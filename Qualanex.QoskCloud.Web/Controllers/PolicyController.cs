﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyController.cs">
///   Copyright (c) 2014 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Linq;
   using System.Collections.Generic;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Resources;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   public class PolicyController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objPolicySearch"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult GetpharmacyDetails(MLPolicyProfileSearch objPolicySearch)
      {
         var objGridModel = new RootObject();

         try
         {
            objGridModel.Viewpolicyrows = PolicyDataAccess.GetPolicypharmacyDetails(objPolicySearch);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(objGridModel, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult Index()
      {
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult CreatePolicy()
      {
         var objResPolicy = new ResponsePolicy();

         this.ViewBag.profileTypeDetails = PolicyDataAccess.GetTypeDetails();
         this.ViewBag.GroupTypeDetails = PolicyDataAccess.GetProfileGroup();

         return this.View("CreatePolicy", objResPolicy);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Profile details like Name, Address, city, state based on selected
      ///   profile from ProfileType and GropType in policy screen.
      /// </summary>
      /// <param name="checkedValues"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult BindProfileDetailsBasedOnSelectedProfile(string checkedValues)
      {
         var json = new JsonResult();

         try
         {
            json.Data = PolicyDataAccess.GetProfileDetailsBasedOnProfileType(checkedValues);
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind Manufacture, Repackager, Wholesaler, Manufacture Group based on
      ///   selected radio button from GUI.
      /// </summary>
      /// <param name="Ptype"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult BindManufactureRepackagerWholesaler(string Ptype)
      {
         var json = new JsonResult();

         try
         {
            json.Data = PolicyDataAccess.GetProfileListByType(Ptype);
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get lot details based on selected product from product grid.
      /// </summary>
      /// <param name="checkedProductIDValues"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult BindLotBasedOnSelectedProduct(string checkedProductIDValues)
      {
         var json = new JsonResult();

         try
         {
            json.Data = PolicyDataAccess.GetLotBasedOnProductID(checkedProductIDValues);
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="checkedManufactureprofile"></param>
      /// <param name="searchType"></param>
      /// <param name="MWRType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult NDCLOTProductsDetailsBasedOnSelectedManufacture(string checkedManufactureprofile, string searchType, string MWRType, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);

            // ...
            //=============================================================================
            if (searchType.Equals(Constants.PolicyController_NDC))
            {
               var data = PolicyDataAccess.GetNDCProductWithAssociatedLotForExistingPolicy(checkedManufactureprofile, MWRType, 0, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

               return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
            }
            // ...
            //=============================================================================
            else if (searchType.Equals(Constants.PolicyController_NDCLOT))
            {
               var data = PolicyDataAccess.GetNDCLOTProductWithAssociatedLot(checkedManufactureprofile, MWRType, 0, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

               return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
            }

         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="checkedManufactureprofile"></param>
      /// <param name="searchType"></param>
      /// <param name="MWRType"></param>
      /// <param name="ProductID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult NDCLOTProductsDetailsBasedOnSelectedManufactureDetails(int checkedManufactureprofile, string searchType, string MWRType, long ProductID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);

            switch (searchType)
            {
               // ...
               //=============================================================================
               case Constants.PolicyController_NDC:
                  {
                     var data = PolicyDataAccess.GetNDCProductWithAssociatedLotForExistingPolicyDetails(checkedManufactureprofile, 0, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }

               // ...
               //=============================================================================
               case Constants.PolicyController_NDCLOT:
                  {
                     var data = PolicyDataAccess.GetNDCLOTProductWithAssociatedLotDetails(checkedManufactureprofile, 0, ProductID, MWRType, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="PolicyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult RedirectToPolicyforEDIT(long PolicyID)
      {
         try
         {
            this.ViewBag.Update = "Update";

            return this.View("EditPolicyRule", PolicyDataAccess.GetPolicyBasedOnPolicyID(PolicyID));
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.View("EditPolicyRule");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="requestData"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult UpdatePolicyDetails(ResponsePolicy requestData)
      {
         try
         {
            requestData.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = PolicyDataAccess.UpdatePolicy(requestData);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = PolicyResource.Update_Fail, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(PolicyResource.Update_Fail);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="policyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult DeleteSelectedPolicy(string policyCode)
      {
         try
         {
            switch (PolicyDataAccess.DeletePolicy(policyCode, this.User.Identity.Name).Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = PolicyResource.OK_Message, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = PolicyResource.Fails, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(new { Result = PolicyResource.Fails, VersionChanged = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="policyProfileID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult DeletePolicyProfile(int policyProfileID)
      {
         try
         {
            PolicyDataAccess.DeletePolicyProfile(policyProfileID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            this.View("Error");
         }

         return this.RedirectToAction("Index");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileType"></param>
      /// <param name="policyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetPolicyProfileCodes(string profileType, long policyID)
      {
         var json = new JsonResult();

         try
         {
            if (!string.IsNullOrWhiteSpace(profileType) && policyID > 0)
            {
               var lst = new ManufactureNameList { lstManufacturer = PolicyDataAccess.GetPolicyProfileCode(profileType, policyID) };

               json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
               json.Data = lst.lstManufacturer.OrderBy(p => p.Name).ToList();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="policyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetAllPolicyAppliedForProfiles(long policyID)
      {
         var json = new JsonResult();

         try
         {
            if (policyID > 0)
            {
               var lst = new ManufactureNameList { lstManufacturer = PolicyDataAccess.GetAllPolicyForProfiles(policyID) };

               if (lst.lstManufacturer != null)
               {
                  json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                  json.Data = lst.lstManufacturer.OrderBy(p => p.Name).ToList();
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      /////****************************************************************************
      ///// <summary>
      /////   Bind the Manufacture, Wholesaler, Repackager, Wholesaler and Manufacture
      /////   Group based on selected policy type like published and custom.
      ///// </summary>
      ///// <param name="Ptype"></param>
      ///// <param name="policyID"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public JsonResult BindManufactureRepackagerWholesalerforStepOne(string Ptype, long policyID)
      //{
      //   var json = new JsonResult();

      //   try
      //   {
      //      json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
      //      json.Data = PolicyDataAccess.GetProfileListByType(Ptype, policyID);
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //   }

      //   return json;
      //}

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profilegroup"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult GetProfileAndGroupType(string profilegroup)
      {
         var json = new JsonResult();

         try
         {
            switch (profilegroup)
            {
               // ...
               //=============================================================================
               case Constants.Profile:
                  json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                  json.Data = PolicyDataAccess.GetTypeDetails();
                  break;

               // ...
               //=============================================================================
               case Constants.Group:
                  json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                  json.Data = PolicyDataAccess.GetProfileGroup();
                  break;

               // ...
               //=============================================================================
               case Constants.PublishedProfileType:
                  json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                  json.Data = PolicyDataAccess.GetPublishProfileType();
                  break;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create policy for published and customer specific.
      /// </summary>
      /// <param name="requestData"></param>
      /// <param name="pharmacylist"></param>
      /// <param name="checkedProductWithLot"></param>
      /// <param name="checkedProductNDC"></param>
      /// <param name="WMProfileType"></param>
      /// <param name="WMRProfileCode"></param>
      /// <param name="PolicyType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public JsonResult SavePolicyDetailsNew(ResponsePolicy requestData, string pharmacylist, string checkedProductWithLot, string checkedProductNDC, string WMProfileType, string WMRProfileCode, string PolicyType)
      {
         var json = new JsonResult();

         try
         {
            requestData.ModifiedBy = this.User.Identity.Name;

            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return this.Json(PolicyDataAccess.SavePolicyCustomerSpecificAndPublishedPolicy(requestData, WMRProfileCode, pharmacylist, checkedProductWithLot, checkedProductNDC, WMProfileType, PolicyType)
                                 ? PolicyResource.Save_Successfully
                                 : PolicyResource.Save_Fails);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(PolicyResource.Save_Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the policy existence based on selection of pharmacy, manufacturer
      ///   and NDC Lot.
      /// </summary>
      /// <param name="WMRProfileCode"></param>
      /// <param name="Pharmacy"></param>
      /// <param name="ProductIDNDC"></param>
      /// <param name="ProductNDCWithLOt"></param>
      /// <param name="PolicyNamePublishedCustom"></param>
      /// <param name="policyTYpe"></param>
      /// <param name="SelectionType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult ISExistPolicy(string WMRProfileCode, string Pharmacy, string ProductIDNDC, string ProductNDCWithLOt, string PolicyNamePublishedCustom, string policyTYpe, string SelectionType)
      {
         var pharmacyProfileCode = 0;
         var WmProfileCode = 0;

         if (!string.IsNullOrEmpty(Pharmacy) && !Pharmacy.Contains(Constants.PolicyController_Select))
         {
            pharmacyProfileCode = Convert.ToInt32(Pharmacy);
         }

         if (!string.IsNullOrEmpty(WMRProfileCode) && !WMRProfileCode.Contains(Constants.PolicyController_Select))
         {
            WmProfileCode = Convert.ToInt32(WMRProfileCode);
         }

         switch (PolicyNamePublishedCustom)
         {
            // ...
            //=============================================================================
            case Constants.CustomerSpecific:
               if (PolicyDataAccess.CheckPolicyExistanceForCustomerSpecific(WmProfileCode, pharmacyProfileCode, ProductIDNDC, ProductNDCWithLOt, policyTYpe, SelectionType))
               {
                  return this.Json("Policy Exist", JsonRequestBehavior.AllowGet);
               }

               break;

            // ...
            //=============================================================================
            case Constants.PublishedPolicy:
               if (PolicyDataAccess.CheckPolicyExistanceForPublished(WmProfileCode, ProductIDNDC, ProductNDCWithLOt, policyTYpe))
               {
                  return this.Json("Policy Exist", JsonRequestBehavior.AllowGet);
               }

               break;
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Show the created policy for pharmacy.
      /// </summary>
      /// <param name="PolicyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult ViewEditPolicy(string PolicyID)
      {
         return View("ViewEditPolicy", PolicyDataAccess.GetPolicyDetails(Convert.ToInt64(PolicyID)));
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ObjPolicyPartialGrid"></param>
      /// <param name="ManufactureName"></param>
      /// <param name="Type"></param>
      /// <param name="CurrentServerPageNo"></param>
      /// <param name="PolicyType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      // [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult GetPolicyDetails(PolicyPartialGrid ObjPolicyPartialGrid, string ManufactureName, string Type, int CurrentServerPageNo, string PolicyType, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            if (!string.IsNullOrEmpty(ManufactureName))
            {
               ObjPolicyPartialGrid = PolicyDataAccess.GetPolicyDetails(Convert.ToInt32(ManufactureName), Type, PolicyType, this.Request.QueryString);
            }

            var orders = ObjPolicyPartialGrid.lstPolicy.Skip(pagesize * pagenum).Take(pagesize);

            if (!string.IsNullOrEmpty(sortorder))
            {
               orders = sortorder == Constants.PolicyController_asc
                           ? orders.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                           : orders.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
            }

            return this.Json(new { TotalRows = ObjPolicyPartialGrid.lstPolicy.Count, Rows = orders }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="query"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string BuildQuery(System.Collections.Specialized.NameValueCollection query)
      {
         var filtersCount = int.Parse(query.GetValues(Constants.PolicyController_filterscount)[0]);
         var tmpDataField = "";
         var tmpFilterOperator = "";
         var where = "";

         if (filtersCount > 0)
         {
            where = Constants.PolicyController_WHERE;
         }

         for (var i = 0; i < filtersCount; i += 1)
         {
            var filterValue = query.GetValues(Constants.PolicyController_filtervalue + i)[0];
            var filterCondition = query.GetValues(Constants.PolicyController_filtercondition + i)[0];
            var filterDataField = query.GetValues(Constants.PolicyController_filterdatafield + i)[0];
            var filterOperator = query.GetValues(Constants.PolicyController_filteroperator + i)[0];

            // TODO
            //=============================================================================
            // I'm leaving this logic "as is" for now because I'd like to trace into it
            // actually see the results and verify that changing the logic will not break
            // the function.  However, reviewing this section below you'll see that the
            // first 'if' statement is always true and therefore the remaining logic is
            // never executed.
            //-----------------------------------------------------------------------------

            if (string.IsNullOrEmpty(tmpDataField))
            {
               tmpDataField = filterDataField;
            }
            else if (tmpDataField != filterDataField)
            {
               where += Constants.PolicyController_ANDWITHBREKET;
            }
            else if (tmpDataField == filterDataField)
            {
               if (string.IsNullOrEmpty(tmpFilterOperator))
               {
                  where += Constants.PolicyController_ANDWITHSPACE;
               }
               else
               {
                  where += Constants.PolicyController_OR;
               }
            }

            // Build the "WHERE" clause depending on the filter's condition, value and datafield.
            where += this.GetFilterCondition(filterCondition, filterDataField, filterValue);

            if (i == filtersCount - 1)
            {
               where += Constants.PolicyController_BREKET;
            }

            tmpFilterOperator = filterOperator;
            tmpDataField = filterDataField;
         }

         return where;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="filterCondition"></param>
      /// <param name="filterDataField"></param>
      /// <param name="filterValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private string GetFilterCondition(string filterCondition, string filterDataField, string filterValue)
      {
         switch (filterCondition)
         {
            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EMPTY:
            case Constants.PolicyController_NOT_NULL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKE;

            // ...
            //=============================================================================
            case Constants.PolicyController_EMPTY:
            case Constants.PolicyController_NULL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKE;

            // ...
            //=============================================================================
            case Constants.PolicyController_CONTAINS_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_CONTAINS:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_DOES_NOT_CONTAIN_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKEPERCENT + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_DOES_NOT_CONTAIN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKEPERCENT + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_EQUAL_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_EQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_EQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EQUAL_CASE_SENSITIVE:
               return Constants.PolicyController_BINARY + filterDataField + Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_GREATER_THAN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_GRATERTHENWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_LESS_THAN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LESSTHENWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_GREATER_THAN_OR_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_GRATERTHENEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_LESS_THAN_OR_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LESSTHENEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_STARTS_WITH_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEWITHCOMMA + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_STARTS_WITH:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEWITHCOMMA + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_ENDS_WITH_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_COMMA + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_ENDS_WITH:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_COMMA;
         }

         return "";
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="checkedprofile"></param>
      /// <param name="SelectionType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult PharmacyDetailsBasedonProfileAndGroupTypeforCreatePolicyScreen(string checkedprofile, string SelectionType, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);

            switch (SelectionType)
            {
               // ...
               //=============================================================================
               case Constants.ProfileType:
                  {
                     var data = PolicyDataAccess.GetPharmacyForCreatePolicy(checkedprofile, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }

               // ...
               //=============================================================================
               case Constants.GroupType:
                  {
                     var data = PolicyDataAccess.GetAllGroupMemberBasedOnGroup(SelectionType, checkedprofile, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }
            }

         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         //return this.PartialView("_PharmacyDetailsAssociatewithProfileType");
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="checkedManufactureprofile"></param>
      /// <param name="searchType"></param>
      /// <param name="MWRType"></param>
      /// <param name="Policyid"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult NDCLOTProductsDetailsBasedOnSelectedManufactureForView(string checkedManufactureprofile, string searchType, string MWRType, long Policyid, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);

            switch (searchType)
            {
               // ...
               //=============================================================================
               case Constants.PolicyController_NDC:
                  {
                     var data = PolicyDataAccess.GetNDCProductWithAssociatedLotForExistingPolicy(checkedManufactureprofile, MWRType, Convert.ToInt64(Policyid), sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }

               // ...
               //=============================================================================
               case Constants.PolicyController_NDCLOT:
                  {
                     var data = PolicyDataAccess.GetNDCLOTProductWithAssociatedLotForExistingPolicy(checkedManufactureprofile, MWRType, Convert.ToInt64(Policyid), sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

                     return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
                  }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="checkedManufactureprofile"></param>
      /// <param name="Policyid"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult NDCLOTProductsDetailsBasedOnSelectedManufactureForViewDetails(int checkedManufactureprofile, long Policyid, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);
            var data = PolicyDataAccess.GetNDCProductWithAssociatedLotForExistingPolicyDetails(checkedManufactureprofile, Convert.ToInt64(Policyid), sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

            return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="lotNumber"></param>
      /// <param name="checkedManufactureprofile"></param>
      /// <param name="Policyid"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult NDCLOTProductsDetailsBasedOnSelectedNDCForViewDetails1(long productID, string lotNumber, int checkedManufactureprofile, long Policyid, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRows = 0;
            var buildquery = this.BuildQuery(this.Request.QueryString);
            var data = PolicyDataAccess.GetNDCLOTProductWithAssociatedLotForExistingPolicyDetails(productID, lotNumber, checkedManufactureprofile, Convert.ToInt64(Policyid), sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRows);

            return this.Json(new { TotalRows = totalRows, Rows = data }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

   }
}