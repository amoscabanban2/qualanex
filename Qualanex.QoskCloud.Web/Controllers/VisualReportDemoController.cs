﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Text;

using Korzh.EasyQuery.Db;
using Korzh.EasyQuery.Services;
using Korzh.EasyQuery.Services.Db;
using Korzh.EasyQuery.Mvc;
using Korzh.Utils.Db;
using Qualanex.QoskCloud.Web.Model;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Qualanex.QoskCloud.Utility;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using Qualanex.QoskCloud.Web.Areas.Report.Common;

using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.Web.Controllers
{
   public class VisualReportDemoController : Controller
   {

      private EqServiceProviderDb eqService;

      public VisualReportDemoController()
      {
         eqService = new EqServiceProviderDb();
         eqService.DefaultModelName = "model2";

         eqService.SessionGetter = key => Session[key];
         eqService.SessionSetter = (key, value) => Session[key] = value;
         eqService.StoreQueryInSession = true;

         eqService.Formats.SetDefaultFormats(FormatType.MsSqlServer);
         eqService.Formats.UseSchema = true;

         string dataPath = System.Web.HttpContext.Current.Server.MapPath("~/Model");
         eqService.DataPath = dataPath;

           // get connection string from configuration manager
         eqService.Connection = new SqlConnection( System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
         if (eqService.Connection.State != ConnectionState.Open)
         {
            eqService.Connection.Open();
         }
         //+ System.IO.Path.Combine(dataPath, "Model1.edmx"));
         //"ConnectionString='metadata=res://*/Models.Model1.csdl|res://*/Models.Model1.ssdl|res://*/Models.Model1.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=localhost;initial catalog=QoskCloud;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;'");
         //new SqlCeConnection("Data Source=AMORAN5810;Initial catalog=QoskCloud;Integrated security=True;");
         //new SqlCeConnection("Data Source=" + System.IO.Path.Combine(dataPath, "Model1.edmx"));


      }

      public ActionResult Index()
      {
         //var query = eqService.GetQuery();
         //ViewBag.QueryJson = query.SaveToDictionary().ToJson();
         ViewBag.Message = TempData["Message"];
         var vm = new Qualanex.QoskCloud.Web.Model.VisualReport()
         {
            rowsCount = 0
         };
         return View("Index", vm);
      }


      //reserved for future version
      //protected override void OnActionExecuting(ActionExecutingContext filterContext) {
      //    base.OnActionExecuting(filterContext);

      //    var prms  = filterContext.HttpContext.Request.Params;
      //    if (prms.Get("modelName") != null && prms.Get("modelId") == null)
      //        filterContext.ActionParameters["modelId"] = prms["modelName"];
      //    if (prms.Get("queryName") != null && prms.Get("queryId") == null)
      //        filterContext.ActionParameters["queryId"] = prms["queryName"];
      //}

      /// <summary>
      /// Creates a <see cref="T:System.Web.Mvc.JsonResult" /> object that serializes the specified object to JavaScript Object Notation (JSON) format using the content type, content encoding, and the JSON request behavior.
      /// </summary>
      /// <remarks>We override this method to set MaxJsonLength property to the maximum possible value</remarks>
      /// <param name="data">The JavaScript object graph to serialize.</param>
      /// <param name="contentType">The content type (MIME type).</param>
      /// <param name="contentEncoding">The content encoding.</param>
      /// <param name="behavior">The JSON request behavior</param>
      /// <returns>The result object that serializes the specified object to JSON format.</returns>
      protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
      {
         // every time when you execute query or want to drag and drop from entities to columns 
         // do some changes, you are are serializing it
         return new JsonResult
         {
            Data = data,
            ContentType = contentType,
            ContentEncoding = contentEncoding,
            JsonRequestBehavior = behavior,
            MaxJsonLength = int.MaxValue
         };
      }

      // the sample for loading into Grid,
      // Get rows, columns as object and count
      public ActionResult EasyQuery(string entityRow, int rowCount, string entityCol)
      {
         var vm = new Qualanex.QoskCloud.Web.Model.VisualReport();
         //System.Web.Helpers.Json.Decode<Dictionary<string, object>>(entity);
         var cols = (IEnumerable<object>)System.Web.Helpers.Json.Decode<object>(entityCol);
         var rows = (IEnumerable<object>)System.Web.Helpers.Json.Decode<object>(entityRow);
        
         vm.rows = rows.ToList();
         vm.cols = cols.ToList();
         vm.lst = entityRow;
         vm.rowsCount = rowCount;

         return PartialView("_EasyQuery", vm);
      }

      // exporting the report as pdf file.
      [HttpPost]
      [ValidateInput(false)]
      public FileResult Export(string GridHtml)
      {
         using (MemoryStream stream = new System.IO.MemoryStream())
         {
            StringReader sr = new StringReader(GridHtml);
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
            pdfDoc.Open();
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            pdfDoc.Close();
            return File(stream.ToArray(), "application/pdf", "Report_"+ DateTime.Now.ToLocalTime() +".pdf");
         }
      }

      #region public actions
      /// <summary>
      /// Gets the model by its name
      /// </summary>
      /// <param name="modelName">The name.</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult GetModel(string modelName)
      {
         var model = eqService.GetModel(modelName);
         return Json(model.SaveToDictionary());
      }

      /// <summary>
      /// Gets the query by its name
      /// </summary>
      /// <param name="queryName">The name.</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult GetQuery(string queryId)
      {
         var query = eqService.GetQuery(queryId);
         return Json(query.SaveToDictionary());
      }

      /// <summary>
      /// Saves the query.
      /// </summary>
      /// <param name="queryJson">The JSON representation of the query .</param>
      /// <param name="queryName">Query name.</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SaveQuery(string queryJson, string queryName)
      {
         eqService.SaveQueryDict(queryJson.ToDictionary(), queryName);
         Dictionary<string, object> dict = new Dictionary<string, object>();
         dict.Add("result", "OK");
         return Json(dict);
      }

      [HttpGet]
      [ValidateAntiForgeryToken]
      public JsonResult GetQueryList(string modelName)
      {
         var queries = eqService.GetQueryList(modelName);
         return Json(queries, JsonRequestBehavior.AllowGet);
      }

      /// <summary>
      /// It's called when it's necessary to synchronize query on client side with its server-side copy.
      /// Additionally this action can be used to return a generated SQL statement (or several statements) as JSON string
      /// </summary>
      /// <param name="queryJson">The JSON representation of the query .</param>
      /// <param name="optionsJson">The additional parameters which can be passed to this method to adjust query statement generation.</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SyncQuery(string queryJson, string optionsJson)
      {
         var query = eqService.SyncQueryDict(queryJson.ToDictionary());
         var statement = eqService.BuildQuery(query, optionsJson.ToDictionary());
         Dictionary<string, object> dict = new Dictionary<string, object>();
         dict.Add("statement", statement);
         return Json(dict);
      }

      /// <summary>
      /// This action returns a custom list by different list request options (list name).
      /// </summary>
      /// <param name="options">List request options - an instance of <see cref="ListRequestOptions"/> type.</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult GetList(ListRequestOptions options)
      {
         return Json(eqService.GetList(options));
      }

      /// <summary>
      /// Executes the query passed as JSON string and returns the result record set (again as JSON).
      /// </summary>
      /// <param name="queryJson">The JSON representation of the query.</param>
      /// <param name="optionsJson">Different options in JSON format</param>
      /// <returns></returns>
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult ExecuteQuery(string queryJson, string optionsJson)
      {
         Dictionary<string, object> resultDict = new Dictionary<string, object>();
         var query = eqService.LoadQueryDict(queryJson.ToDictionary());
         //we can set different options before SQL generation:
         //query.Options.SelectDistinct = true;
         //query.Options.SelectTop = "100";
         //query.Options.LimitClause = "20";

         var sql = eqService.BuildQuery(query, optionsJson.ToDictionary());

         var resultSet = eqService.GetDataSetBySql(sql);

         var resultSetDict = eqService.DataSetToDictionary(resultSet, optionsJson.ToDictionary());


         resultDict.Add("statement", sql);
         resultDict.Add("resultSet", resultSetDict);
         resultDict.Add("resultCount", resultSet.Tables[0].Rows.Count + " record(s) found");


         return Json(resultDict);
      }


      private void ErrorResponse(string msg)
      {
         Response.StatusCode = 400;
         Response.Write(msg);
         Response.Output.Flush();
      }

          // Export Current file to excel
      [HttpGet]
      public void ExportToFileExcel()
      {
         Response.Clear();

         var query = eqService.GetQuery();

         if (!query.IsEmpty)
         {
            var sql = eqService.BuildQuery(query);
            eqService.Paging.Enabled = false;
            DataSet dataset = eqService.GetDataSetBySql(sql);
            if (dataset != null)
            {
               Response.ContentType = "application/vnd.ms-excel";
               Response.AddHeader("Content-Disposition",
                   string.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode("report.xls")));
               DbExport.ExportToExcelHtml(dataset, Response.Output, HtmlFormats.Default);
            }
            else
               ErrorResponse("Empty dataset");
         }
         else
            ErrorResponse("Empty query");

      }

      // Execute File to CSV
      [HttpGet]
      public void ExportToFileCsv()
      {
         Response.Clear();
         var query = eqService.GetQuery();

         if (!query.IsEmpty)
         {
            var sql = eqService.BuildQuery(query);
            eqService.Paging.Enabled = false;
            DataSet dataset = eqService.GetDataSetBySql(sql);
            if (dataset != null)
            {
               Response.ContentType = "text/csv";
               Response.AddHeader("Content-Disposition",
                   string.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncode("report.csv")));
               DbExport.ExportToCsv(dataset, Response.Output, CsvFormats.Default);
            }
            else
               ErrorResponse("Empty dataset");
         }
         else
            ErrorResponse("Empty query");

      }

      // Save Query
      [HttpGet]
      public FileResult GetCurrentQuery(string queryFile)
      {          
         var query = eqService.GetQuery();
         FileContentResult result = new FileContentResult(Encoding.UTF8.GetBytes(query.SaveToString()), "Content-disposition: attachment;");
         string fileName = this.HttpContext.User.Identity.Name;
         result.FileDownloadName = fileName+ ".xml";
         
         return result;

         // For uploading file into azure
         // as .SQL
         ////blob.UploadFromFile(blob.Uri.ToString(), FileMode.Create);
         //string foldename = Server.MapPath("\\Views\\VisualReportDemo\\");
         //string flName = fileName + "_ProductDetail_NewWebJobReport_Active" + ".sql";
         //string pathString = foldename + flName;

         //using (FileStream fs = System.IO.File.Create(pathString))
         //{
         //   for (byte i = 0; i < 100; i++)
         //   {
         //      fs.WriteByte(i);
         //   }
         //}
         ////queryFile = queryFile.Replace(System.Environment.NewLine, string.Empty);
         //System.IO.File.WriteAllText(pathString, queryFile);


         //var connectString = Utility.Common.ConfigurationManager.GetConnectionString(Constants.AzureAccount_KioskSync);

         //Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

         //CloudBlobClient client = account.CreateCloudBlobClient();

         //CloudBlobContainer sampleContainer = client.GetContainerReference(Constants.AzureContainer_GeneratedReports);
         //sampleContainer.CreateIfNotExists();

         //sampleContainer.SetPermissions(new BlobContainerPermissions()
         //{
         //   PublicAccess = BlobContainerPublicAccessType.Container
         //});
         //CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(System.IO.Path.GetFileName(pathString));

         //using (Stream file = System.IO.File.OpenRead(pathString))
         //{
         //   blob.UploadFromStream(file);
         //}
         //if (System.IO.File.Exists(pathString))
         //{
         //   System.IO.File.Delete(pathString);
         //}


      }

      [HttpPost]
      public ActionResult LoadQueryFromFile(HttpPostedFileBase queryFile)
      {
         if (queryFile != null && queryFile.ContentLength > 0)
            try
            {
               var query = eqService.GetQuery();
               query.LoadFromStream(queryFile.InputStream);
               eqService.SyncQuery(query);
            }
            catch (Exception ex)
            {
               TempData["Message"] = "ERROR:" + ex.Message.ToString();
            }
         else
         {
            TempData["Message"] = "You have not specified a file.";
         }

         return RedirectToAction("Index");
      }


      #endregion

   }
}