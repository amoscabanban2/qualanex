﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="HealthCheckController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System;
   using System.Web.Mvc;
   using System.Net;

   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class HealthCheckController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [AllowAnonymous]
      public ActionResult Index()
      {
         try
         {
            // TODO: Run a simple check to ensure database is available
            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
         }
         catch (Exception ex)
         {
            Logger.Error("Exception in basic health check: {0}", ex.Message);
            return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
         }
      }

   }
}