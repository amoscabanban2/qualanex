﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="QoskController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public interface IQoskController
   {
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   [QoskAuthorizeAttribute(Roles = Constants.UserRoleAdmin)]
   public class QoskController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Index()
      {
         var dict = CommonFunctions.GetState();

         this.ViewBag.Buckets = dict.Select(item => new SelectListItem
         {
            Value = item.Key.ToString(),
            Text = item.Value
         });

         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult RegisterQosk()
      {
         var dict = CommonFunctions.GetState();

         this.ViewBag.Buckets = dict.Select(item => new SelectListItem
         {
            Value = item.Key.ToString(),
            Text = item.Value
         });

         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public ActionResult RegisterQosk(QoskRequest objQoskRequest)
      {
         Response objclsResponse = null;

         if (this.ModelState.IsValid)
         {
            objclsResponse = QoskDataAccess.RegisterQosk(objQoskRequest);
         }

         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="QoskID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult EditQosk(string QoskID)
      {
         var dict = CommonFunctions.GetState();

         this.ViewBag.Buckets = dict.Select(item => new SelectListItem
         {
            Value = item.Key.ToString(),
            Text = item.Value
         });

         return this.View(QoskDataAccess.GetQoskDetailsByID(QoskID));
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public ActionResult UpdateQosk(QoskRequest objQoskRequest)
      {
         Response objclsResponse = null;

         if (this.ModelState.IsValid)
         {
            objclsResponse = QoskDataAccess.UpdateQosk(objQoskRequest);
         }

         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <param name="sortDataField"></param>
      /// <param name="sortOrder"></param>
      /// <param name="pageSize"></param>
      /// <param name="pageNum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetQoskDetails(QoskRequest objQoskRequest, string sortDataField, string sortOrder, int pageSize, int pageNum)
      {
         return this.Json(QoskDataAccess.GetQoskDetails(objQoskRequest), JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Creates a pseudo-random password containing the number of character
      ///   classes defined by complexity, where:
      ///            2 = alpha,
      ///            3 = alpha + num,
      ///            4 = alpha + num + special.
      /// </summary>
      /// <param name="length"></param>
      /// <param name="complexity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GeneratePassword(int length, int complexity)
      {
         using (var csp = new System.Security.Cryptography.RNGCryptoServiceProvider())
         {
            // Define the possible character classes where complexity defines the number
            // of classes to include in the final output.
            ///////////////////////////////////////////////////////////////////////////////
            char[][] classes =
            {
               Constants.QoskController_Small_AtoZ.ToCharArray(),
               Constants.QoskController_Capital_AtoZ.ToCharArray(),
               Constants.QoskController_Number.ToCharArray(),
               Constants.QoskController_Pattern.ToCharArray(),
            };

            complexity = Math.Max(1, Math.Min(classes.Length, complexity));

            if (length < complexity)
            {
               throw new ArgumentOutOfRangeException(Constants.QoskController_Length);
            }

            // Since we are taking a random number 0-255 and modulo that by the number of
            // characters, characters that appear earlier in this array will receive a
            // heavier weight. To counter this we will then reorder the array randomly.
            // This should prevent any specific character class from receiving a priority
            // based on it's order.
            ///////////////////////////////////////////////////////////////////////////////
            var allchars = classes.Take(complexity).SelectMany(c => c).ToArray();
            var bytes = new byte[allchars.Length];

            csp.GetBytes(bytes);

            for (var i = 0; i < allchars.Length; i++)
            {
               var tmp = allchars[i];

               allchars[i] = allchars[bytes[i] % allchars.Length];
               allchars[bytes[i] % allchars.Length] = tmp;
            }

            // Create the random values to select the characters
            ///////////////////////////////////////////////////////////////////////////////
            Array.Resize(ref bytes, length);
            var result = new char[length];

            while (true)
            {
               csp.GetBytes(bytes);

               // Obtain the character of the class for each random byte
               for (var i = 0; i < length; i++)
               {
                  result[i] = allchars[bytes[i] % allchars.Length];
               }

               // Verify that it does not start or end with whitespace
               if (char.IsWhiteSpace(result[0]) || char.IsWhiteSpace(result[(length - 1) % length]))
               {
                  continue;
               }

               var testResult = new string(result);

               // Verify that all character classes are represented
               if (0 != classes.Take(complexity).Count(c => testResult.IndexOfAny(c) < 0))
               {
                  continue;
               }

               return this.Json(testResult, JsonRequestBehavior.AllowGet);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="terms"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetProfile(string terms)
      {
         var objProfile = QoskDataAccess.GetPharmacyProfilesUsingAutoComplete(terms)
            .Select(n => new SelectListItem
            {
               Text = n.Name,
               Value = Convert.ToString(n.ProfileCode)
            });

         return this.Json(objProfile, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="stateskey"></param>
      /// <param name="name"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CreateKioskID(string stateskey, string name)
      {
         return this.Json(QoskDataAccess.GetQoskID(name, stateskey), JsonRequestBehavior.AllowGet);
      }

   }
}
