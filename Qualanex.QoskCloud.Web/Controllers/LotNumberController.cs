﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="LotNumberController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Resources;
   using Qualanex.QoskCloud.Web.Areas.Product;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   public class LotNumberController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Index method with search parameters.
      /// </summary>
      /// <param name="fc"></param>
      /// <param name="ddProfileName"></param>
      /// <param name="txtNDC"></param>
      /// <param name="txtDescription"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult Index(FormCollection fc, string ddProfileName, string txtNDC, string txtDescription)
      {
         var isSearch = false;

         try
         {
            var lot = new LotNumbersReturns();
            var objLotNumberSearch = new LotNumberSearch
            {
               UserID = this.GetUserID(),
               UserTypeName = this.GetUserType()
            };

            if (!string.IsNullOrWhiteSpace(ddProfileName))
            {
               isSearch = true;
               objLotNumberSearch.ProfileCode = Convert.ToInt32(ddProfileName);
            }

            if (!string.IsNullOrWhiteSpace(txtNDC))
            {
               isSearch = true;
               objLotNumberSearch.NDC = Convert.ToInt64(txtNDC);
            }

            if (!string.IsNullOrWhiteSpace(txtDescription))
            {
               isSearch = true;
               objLotNumberSearch.Description = Convert.ToString(txtDescription);
            }

            if (isSearch)
            {
               this.GetCurrentUserData(ref objLotNumberSearch);
               lot.listLotNumbers = LotNumbersDataAccess.GetAllLotNumbers(objLotNumberSearch);
            }
            else
            {
               lot.listLotNumbers = Enumerable.Empty<LotNumbersDataEntity>();
            }

            return this.View("Index", lot);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref LotNumberSearch objUserSearch)
      {
         var userReturnData = ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData;

         objUserSearch.UserID = userReturnData.UserID ?? 0;
         objUserSearch.UserTypeCode = userReturnData.UserTypeCode;
         objUserSearch.UserProfileCode = userReturnData.ProfileCode ?? 0;
         objUserSearch.RollupProfileCode = userReturnData.RollupProfileCode ?? 0;
         objUserSearch.ProfileType = userReturnData.UserProfileType;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create Lot Number.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Create()
      {
         var objLotNumberViewModel = new LotNumberViewModel
         {
            RepackagerProfileList = this.GetRepakagerProfileCode().Select(n => new SelectListItem
            {
               Text = n.Name.ToString(),
               Value = n.ProfileCode.ToString()
            }),

            ProductProfileList = this.GetProductIDWithName().Select(n => new SelectListItem
            {
               Text = n.ProductName.ToString(),
               Value = Convert.ToString(n.ProductID)
            }),

            IsPopupCreateLotNumber = false
         };

         return this.View("Create", objLotNumberViewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Create Lot Number.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Create(LotNumberViewModel model)
      {
         try
         {
            model.LotNumbersData.CreatedBy = this.User.Identity.Name;

            this.TempData["Lotmessage"] = LotNumbersDataAccess.SaveLotNumber(model.LotNumbersData)
                                             ? LotNumber.Lot_Create_Message
                                             : LotNumber.Lot_Failed_Message;

            return this.RedirectToAction("Create");
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.View();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Edit methods based on lotNumberID.
      /// </summary>
      /// <param name="LotNumberID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult Edit(long LotNumberID)
      {
         try
         {
            this.FillDropDown();

            return this.View("Edit", LotNumbersDataAccess.GetLotNumberByID(LotNumberID));
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///****************************************************************************
      private void FillDropDown()
      {
         this.ViewBag.PoductID = this.GetProductIDWithName().Select(n => new SelectListItem
         {
            Text = n.ProductName.ToString(),
            Value = Convert.ToString(n.ProductID)
         });

         this.ViewBag.ProfileName = this.GetManufactureWithCode().Select(n => new SelectListItem
         {
            Text = n.Name.ToString(),
            Value = n.ProfileCode.ToString()
         });

         this.ViewBag.RepackagerProfileCode = this.GetRepakagerProfileCode().Select(n => new SelectListItem
         {
            Text = n.Name.ToString(),
            Value = n.ProfileCode.ToString()
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Update the Lot Number on web API.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult Edit(LotNumbersDataEntity model)
      {
         try
         {
            model.ModifiedBy = this.User.Identity.Name;
            var objclsResponse = LotNumbersDataAccess.UpdateLotNumbers(model);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Web.Status.OK:
                  return this.Json(new {Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Web.Status.VersionChange:
                  return this.Json(new {Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Web.Status.Deleted:
                  return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Web.Status.Fail:
                  return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public IEnumerable<ProfilesName> GetRepakagerProfileCode()
      {
         try
         {
            return LotNumbersDataAccess.GetReapackagerProfileName();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public IEnumerable<ProductIDWithName> GetProductIDWithName()
      {
         try
         {
            return LotNumbersDataAccess.GetProductIDWithName();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="terms"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetProductIDWithName(string terms)
      {
         try
         {
            var objproducts = LotNumbersDataAccess.GetProductIDWithName(terms)
               .Select(n => new SelectListItem
               {
                  Text = n.ProductName,
                  Value = Convert.ToString(n.ProductID)
               });

            return this.Json(objproducts, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(null, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public IEnumerable<ProfilesName> GetManufactureWithCode()
      {
         try
         {
            return ProfilesDataAccess.GetManufactureByType();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ICollection<RecallDDLWithName> GetReacallID(long productID)
      {
         try
         {
            return LotNumbersDataAccess.GetRecallID(productID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult RecallIDs(string productID)
      {
         var selectedProfductId = 0L;

         if (!string.IsNullOrWhiteSpace(productID))
         {
            selectedProfductId = Convert.ToInt64(productID);
         }

         return new JsonResult
         {
            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            Data = this.GetReacallID(selectedProfductId).ToList()
         };
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult DiverseLotNumberList(long productID)
      {
         var objDiverseLotNumbersDetails = this.GetDivestedLotNumber(productID);

         if (objDiverseLotNumbersDetails != null)
         {
            return this.Json(objDiverseLotNumbersDetails, JsonRequestBehavior.AllowGet);
         }

         return this.RedirectToAction("Index", "LotNumber");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      private DiverseLotNumbersDetails GetDivestedLotNumber(long productID)
      {
         try
         {
            return LotNumbersDataAccess.GetDivestedLotNumber(productID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="LotIDS"></param>
      /// <param name="ProductID"></param>
      /// <param name="updatestatus"></param>
      /// <param name="DivestType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult SaveDiverseLotNumber(int ProfileCode, string LotIDS, long ProductID, bool updatestatus, int DivestType)
      {
         try
         {
            var divestedLotNumbers = new ChangeDiverseLotNumbers
            {
               DiverseOwnshipProfileCode = ProfileCode,
               LotIDs = LotIDS,
               ProductID = ProductID,
               Updatestatus = updatestatus
            };

            switch (DivestType)
            {
               // ...
               //=============================================================================
               case 1:
                  return this.Json(LotNumbersDataAccess.DiversionOfProductWithLot(divestedLotNumbers), JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case 2:
                  return this.Json(LotNumbersDataAccess.DivestProductforALLLOT(divestedLotNumbers), JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private long GetUserID()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserID ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult LotNumberSample()
      {
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CreateSearchPanel()
      {
         var objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.LotNumberController_LotSearchPanel, 0, false);

         this.ViewData["ControlListingForPopUp"] = objListing;

         var controlListing = new DynamicControlEntity
         {
            Controls = objListing.SubscribedColumn.Select(this.CreateDynamicControlBasedOnValue).ToList()
         };

         return controlListing.Controls.Count > 0
                     ? this.PartialView("_SearchPanel", controlListing)
                     : this.PartialView("_SearchPanel");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="details"></param>
      /// <returns></returns>
      ///****************************************************************************
      private ControlViewModel CreateDynamicControlBasedOnValue(ColumnDetails details)
      {
         ControlViewModel obj = null;

         switch (details.ColumnType.ToLower())
         {
            // ...
            //=============================================================================
            case Constants.LotNumberController_bit:
               obj = new CheckBoxViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  Value = false,
                  AssociateTableName = details.TableName,
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

            // ...
            //=============================================================================
            case Constants.LotNumberController_datetime1:
               obj = new DateTimeViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  Value = null,
                  AssociateTableName = details.TableName,
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

            // ...
            //=============================================================================
            case Constants.LotNumberController_select:
               if (string.Compare(details.ColumnName, Constants.LotNumberController_RXorOTC, true) == 0)
               {
                  obj = new DropDownListViewModel
                  {
                     Visible = true,
                     Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                     Name = details.ColumnName,
                     AssociateTableName = details.TableName,
                     DataType = details.ColumnType,
                     OperatorType = details.FilterType,
                     Values = new SelectList(
                        new[]
                        {
                           new { Value = Constants.Model_ProfilesDataAccess_Zero, Text = Constants.ReportController_Select },
                           new { Value = Constants.ReportController_True, Text = Constants.ReportController_Rprescription },
                           new { Value = Constants.ReportController_False, Text = Constants.ReportController_OTC }
                        },
                        Constants.ReportController_Value, Constants.ReportController_Text, Constants.Model_ProfilesDataAccess_Zero)
                  };
               }
               else
               {
                  obj = new DropDownListViewModel
                  {
                     Visible = true,
                     Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                     Name = details.ColumnName,
                     AssociateTableName = details.TableName,
                     OperatorType = details.FilterType,
                     Values = this.BindRespectiveTable(details.MappedTable)
                  };
               }

               break;

            // ...
            //=============================================================================
            default:
               obj = new TextBoxViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  AssociateTableName = details.TableName,
                  Value = " ",
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

         }

         return obj;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CreateColumnListingPopUp()
      {
         ColumnListEntity objListing = null;

         if (this.ViewData["ControlListingForPopUp"] == null)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.LotNumberController_LotSearchPanel, 0, false);
         }

         return objListing != null
                     ? this.PartialView("_ColumnListPanel", objListing)
                     : this.PartialView("_ColumnListPanel");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public PartialViewResult BindColumnListing()
      {
         return this.PartialView("_ColumnGridListPanel", ManageLayoutConfig.GetColumnListingByLayout(Constants.LotNumberController_LotListing, 0, true));
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="searchList"></param>
      /// <param name="layoutID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult SaveCustomSearchDetailsByUserName(string searchList, int layoutID)
      {
         var objList = new ColumnListEntity { SubscribedColumn = new List<ColumnDetails>() };
         var columnList = searchList.Split(',');
         var userID = ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserID ?? 0L;

         if (columnList.Length > 0)
         {
            foreach (var objColumn in columnList.Where(objColumn => !string.IsNullOrEmpty(objColumn)))
            {
               objList.SubscribedColumn.Add(new ColumnDetails
                                                {
                                                   ColumnId = Convert.ToInt32(objColumn),
                                                   LayoutId = layoutID,    // For Testing Only
                                                   UserId = userID         // For Testing Only
                                                });
            }

            if (objList.SubscribedColumn != null)
            {
               return this.Json(ManageLayoutConfig.SaveSubscribedColumn(objList, userID, layoutID)
                              ? LotNumber.Custom_Save_Success
                              : LotNumber.UnDefined);
            }
         }

         return this.Json(LotNumber.UnDefined);
      }


      static List<ControlViewModel> objdsad = null;

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult BindJQXGridLayoutForServerSidePaging(string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var objFilterManipulation = new FilterManipulation(objdsad.GroupBy(u => u.AssociateTableName).Select(grp => grp.ToList()).ToList());

         var objRequestGetProduct = new RequestGetProduct
         {
            UserID = this.GetUserID(),
            UserTypeName = this.GetUserType()
         };

         var userSearch = new UserSearch();
         this.GetCurrentUserData(ref userSearch);
         int total = 0;

         var objProductEntity = new ProductsEntity
         {
            lstManufactureProduct = ProductDataAccess.ListAllLotWithProductssforserver(objRequestGetProduct,
               objFilterManipulation.ProductsFilterObj,
               objFilterManipulation.LotNumbersFilterObj,
               pagesize * pagenum,
               pagesize,
               this.Request.QueryString,
               "LOT",
               sortdatafield,
               sortorder,
               out total)
         };

         return this.Json(new { TotalRows = total, Rows = objProductEntity.lstManufactureProduct }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="lstobjdsad"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult GetColumnsList(List<ControlViewModel> lstobjdsad)
      {
         objdsad = lstobjdsad;

         var objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.LotNumberController_ProductListing, 0, true);

         var objlstJqxGridColumn = (from m in objListing.SubscribedColumn
                                    orderby m.OrderNumber ascending
                                    select new Column
                                    {
                                       text = string.IsNullOrEmpty(m.DisplayText) ? m.ColumnName : m.DisplayText.Trim(),
                                       datafield = m.ColumnName,
                                       columnsheight = Constants.Model_UsersDataAccess_User_Hundred,
                                       width = m.ColumnName == Constants.LotNumberController_CaseSize1
                                               || m.ColumnName == Constants.LotNumberController_CaseSize2
                                               || m.ColumnName == Constants.LotNumberController_CaseSize3
                                               || m.ColumnName == Constants.LotNumberController_Strength
                                               || m.ColumnName == Constants.LotNumberController_UnitDose
                                               || m.ColumnName == Constants.LotNumberController_RXorOTC
                                               || m.ColumnName == Constants.LotNumberController_Refrigerated
                                               || m.ColumnName == Constants.LotNumberController_DivestedBYLot
                                               || m.ColumnName == Constants.LotNumberController_IsRepackager
                                               || m.ColumnName == Constants.LotNumberController_ControlNumber
                                               || m.ColumnName == Constants.LotNumberController_DosageCode
                                               || m.ColumnName == Constants.LotNumberController_UnitOfMeasure
                                               || m.ColumnName == Constants.LotNumberController_RecallID
                                          ? "90"
                                          : "130"
                                    }).ToList();

         return this.Json(objlstJqxGridColumn, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref UserSearch objUserSearch)
      {
         ReturnLoginUserEntity objReturnLoginUserEntity = null;
         objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
         objUserSearch.UserID = objReturnLoginUserEntity.UserReturnData.UserID ?? 0;
         objUserSearch.UserTypeCode = objReturnLoginUserEntity.UserReturnData.UserTypeCode;
         objUserSearch.ProfileType = objReturnLoginUserEntity.UserReturnData.UserProfileType;
         objUserSearch.ProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
         objUserSearch.UserTypeName = this.GetUserType();
         objUserSearch.RollupProfileCode = objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType()
      {
         try
         {
            return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserTypeCode == "ADMN"
                           ? Constants.HomeController_ADMIN
                           : Constants.HomeController_OTHERS;
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="MappedTable"></param>
      /// <returns></returns>
      ///****************************************************************************
      private SelectList BindRespectiveTable(string MappedTable)
      {
         switch (MappedTable)
         {
            case Constants.LotNumberController_Profiles:
            {

               var lstProfile = PolicyDataAccess.GetProfileListByType(Constants.LotNumberController_ManufacturerRepackager);

               lstProfile.Insert(0, new ProfilesName
               {
                  ProfileCode = 0,
                  Name = "Select"

               });

               var result = (from m in lstProfile
                             select new
                             {
                                value = m.ProfileCode,
                                text = m.Name
                             }).AsEnumerable();

               return new SelectList(result, Constants.LotNumberController_value, Constants.LotNumberController_text);
            }
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Show view page and also send the ID.
      /// </summary>
      /// <param name="prodID"></param>
      /// <param name="ProductID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ChangeTrackerDetails(long prodID, long ProductID)
      {
         return this.RedirectToAction("GoToChangeTrackerPage", "Product", new { prodID = prodID, ProductID = ProductID, PageIdentification = "L" });
      }

      ///****************************************************************************
      /// <summary>
      ///   This allows expiration date to populate from an entered lot number when
      ///   available.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="lotNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetLotExpiration(int productId, string lotNumber)
      {
         if (string.IsNullOrWhiteSpace(lotNumber))
         {
            return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
         }

         var lot = LotNumbersDataAccess.GetLots(productId, lotNumber).FirstOrDefault();
         return this.Json(lot, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   If we have an NA lot and the NDC has potential to be part of a recall, then get the corresponding recall information
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="debitMemoNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetNaLotRecallId(int productId, string debitMemoNumber)
      {
         if (string.IsNullOrWhiteSpace(debitMemoNumber))
         {
            return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
         }

         var recall = UpaDataAccess.GetRecallRelation(productId);

         foreach (var r in recall)
         {
            if (r.NAStartsWith != null && debitMemoNumber.ToLower().StartsWith(r.NAStartsWith.ToLower()))
            {
               return this.Json(r.NALot, JsonRequestBehavior.AllowGet);
            }
         }

         return this.Json("", JsonRequestBehavior.AllowGet);
      }
   }
}