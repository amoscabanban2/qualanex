﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using System.Web;
   using System.Web.Mvc;

   using Microsoft.AspNet.Identity;
   using Microsoft.AspNet.Identity.EntityFramework;
   using Microsoft.AspNet.Identity.Owin;
   using Microsoft.Owin.Security;

   using Newtonsoft.Json;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.QoskCloud.Web.Model;
   using Qualanex.QoskCloud.Web.Resources;
   using Qualanex.QoskCloud.Web.Areas.Report.Models;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   enum UsersRoles
   {
      ADMN,
      CUSR,
      RUSR,
      SUSR
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UserController : Controller, IUserProfileController
   {

      private ApplicationSignInManager _signInManager;
      private ApplicationUserManager _userManager;
      private ApplicationRoleManager _roleManager;

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public UserController()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userManager"></param>
      /// <param name="signInManager"></param>
      /// <param name="roleManager"></param>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public UserController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
      {
         this.UserManager = userManager;
         this.SignInManager = signInManager;
         this.RoleManager = roleManager;
      }

      /// <summary>
      ///     Generates a robots.txt file
      /// </summary>
      /// <returns></returns>
      [AllowAnonymous, OutputCache(Duration = 60 * 60 * 24 * 7)]
      public ContentResult Robots()
      {
         var stringBuilder = new StringBuilder();
         stringBuilder.AppendLine("user-agent: *");
         stringBuilder.AppendLine("disallow: /");

         return this.Content(stringBuilder.ToString(), "text/plain", Encoding.UTF8);
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      private IAuthenticationManager AuthenticationManager => this.HttpContext.GetOwinContext().Authentication;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ApplicationSignInManager SignInManager
      {
         get { return this._signInManager ?? this.HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
         private set { this._signInManager = value; }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ApplicationUserManager UserManager
      {
         get { return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
         private set { this._userManager = value; }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ApplicationRoleManager RoleManager
      {
         get { return this._roleManager ?? this.HttpContext.GetOwinContext().Get<ApplicationRoleManager>(); }
         private set { this._roleManager = value; }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      static IEnumerable<UserType> UserType { get; set; }


      ///****************************************************************************
      /// <summary>
      ///   Get user index page.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public ActionResult Home()
      {
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="page"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult PlaceHolder(string page = null)
      {
         this.ViewBag.Page = page;
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Get user index page.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public ActionResult Index()
      {
         try
         {
            var objUsersEntity = UsersDataAccess.GetUserDefaultData();

            objUsersEntity.UserTypeCode = this.GetUserTypeCode();
            UserType = objUsersEntity.UserType;
            this.ViewBag.UserType = new SelectList(UserType, Utility.Constants.UserController_UserTypeCode, Utility.Constants.UserController_UserTypeName);
            return this.View(objUsersEntity);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get User SearchResult in index page.
      /// </summary>
      /// <param name="ddUserType">Filter by User Type</param>
      /// <param name="txtUserName">Filter by User Name</param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public JsonResult UserSearchResult(string ddUserType, string txtUserName, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            var objUserSearch = new UserSearch
            {
               UserName = txtUserName,
               UserSearchTypeCode = this.SearchParameters(ddUserType)
            };

            if (ddUserType != null)
            {
               objUserSearch.UserTypeCode = this.GetUserTypeCode();
            }

            this.GetCurrentUserData(ref objUserSearch);

            var objUsersEntity = UsersDataAccess.GetUserData(objUserSearch, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);
            objUsersEntity.UserTypeCode = this.GetUserTypeCode();

            var result = new
            {
               TotalRows = totalRecord,
               Rows = objUsersEntity.UsersReturnData
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(null, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set default Search Parameter in parameter.
      /// </summary>
      /// <param name="seachparameters"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string SearchParameters(string seachparameters)
      {
         if (string.IsNullOrWhiteSpace(seachparameters))
         {
            return Utility.Constants.Model_UsersDataAccess_User_MinusOne;
         }

         return seachparameters;
      }

      ///****************************************************************************
      /// <summary>
      ///   User login screen and set remember me option in Cookies.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [AllowAnonymous]
      public ActionResult Login(string returnUrl)
      {
         if (!string.IsNullOrWhiteSpace(this.HttpContext.User.Identity.Name)
          && ValidateUserName(this.HttpContext.User.Identity.Name) == "Fails")
         {
            if (System.Web.HttpContext.Current.Session[Utility.Constants.ManageLayoutConfig_UserDataWithLink] == null)
            {
               SetUserCache(this.HttpContext.User.Identity.Name);
            }
            return this.RedirectToAction("Home", "User");
         }

         var objEncrypt = new MD5Encryption();
         var objLogin = new LoginViewModel();

         if (this.Request.Cookies[Utility.Constants.UserController_EnteredUserName] != null)
         {
            objLogin.UserName = objEncrypt.Decrypt(this.Request.Cookies[Utility.Constants.UserController_EnteredUserName].Value);
            objLogin.RememberMe = true;
         }
         else
         {
            objLogin.RememberMe = false;
         }

         this.ViewBag.ReturnUrl = returnUrl;
         return this.View("Login", objLogin);
      }

      internal static void SetUserCache(string userName)
      {
         var userreturnData = UsersDataAccess.ValidateUserLogin(userName);
         var objReturnLoginUserData = new ReturnLoginUserEntity();
         objReturnLoginUserData.UserReturnData = userreturnData;
         objReturnLoginUserData.UserLeftLinks = UsersDataAccess.GetLeftLinksData(1);
         objReturnLoginUserData.ReportApplicationUrl =
            Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.ReportWebsiteUrl);
         objReturnLoginUserData.AdminApplicationUrl =
            Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl);
         objReturnLoginUserData.ReportsNameLinks = ReportDataAccess.GetAllReport().ToList();
         QoskSessionHelper.SetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink,
            objReturnLoginUserData);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Logout screen and clear current cache.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult Logout(string ReturnUrl = "")
      {
         ReturnUrl = ""; // never return to previous location, but maintaining standard code 
         System.Web.HttpContext.Current.Cache.Remove(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         // Signout from report web library website too
         this.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

         return this.RedirectToAction("Login", "User", new { ReturnUrl = ReturnUrl });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <param name="returnUrl"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [AllowAnonymous]
      public ActionResult LoginCheck(LoginViewModel model, string returnUrl)
      {
         if (model.IsTemporaryPassword)
         {
            return this.RedirectToAction("ChangePassword", "User");
         }

         //// Always ignore returnUrl, leaving commented code here for future consideration
         //if (!string.IsNullOrEmpty(returnUrl))
         //{
         //   if (returnUrl.ToLower().Contains(Utility.Constants.ReturnUrl_Content_Report))
         //   {
         //      //Replace "report" by "Home" from return URL, as it will redirect to home first and then to report.
         //      returnUrl = returnUrl.ToLower().Replace(Utility.Constants.ReturnUrl_Content_Report, Utility.Constants.ReturnUrl_Content_Home);
         //      return new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.ReportWebsiteUrl) + returnUrl.Trim());
         //   }

         //   return this.RedirectToLocal(returnUrl.Trim());
         //}

         return this.RedirectToAction("Home", "User", new { area = string.Empty });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <param name="returnUrl"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [AllowAnonymous]
      public async Task<ActionResult> ValidateUser(LoginViewModel model, string returnUrl)
      {
         try
         {
            var objReturnLoginUserData = new ReturnLoginUserEntity();

            if (!this.ModelState.IsValid)
            {
               this.ModelState.AddModelError(Utility.Constants.UserController_Password, Utility.Constants.UserController_IncorrectCredential);
               return this.Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);
            }

            var result = await this.SignInManager.PasswordSignInAsync(model.UserName, model.Password, false, shouldLockout: false);

            switch (result)
            {
               // ...
               //=============================================================================
               case SignInStatus.Success:
                  {
                     var objEncrypt = new MD5Encryption();
                     var selectedpassword = objEncrypt.EncryptPassword(model.Password);
                     var userreturnData = UsersDataAccess.ValidateUserLogin(model.UserName, selectedpassword);

                     if (userreturnData != null)
                     {
                        if (model.RememberMe)
                        {
                           var username = objEncrypt.Encrypt(model.UserName.Trim());

                           Response.Cookies.Clear();
                           Response.Cookies[Utility.Constants.UserController_EnteredUserName].Value = username;
                           Response.Cookies[Utility.Constants.UserController_EnteredUserName].Expires = DateTime.UtcNow.AddDays(30);
                        }
                        else
                        {
                           Response.Cookies[Utility.Constants.UserController_EnteredUserName].Expires = DateTime.UtcNow.AddDays(-1);
                           Response.Cookies.Clear();
                        }

                        objReturnLoginUserData.UserReturnData = userreturnData;
                        objReturnLoginUserData.UserLeftLinks = UsersDataAccess.GetLeftLinksData(1);
                        objReturnLoginUserData.ReportApplicationUrl = Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.ReportWebsiteUrl);
                        objReturnLoginUserData.AdminApplicationUrl = Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl);
                        objReturnLoginUserData.ReportsNameLinks = ReportDataAccess.GetAllReport().ToList();

                        QoskSessionHelper.SetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink, objReturnLoginUserData);

                        return this.Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);
                     }

                     this.ModelState.AddModelError(Utility.Constants.UserController_Password, Utility.Constants.UserController_IncorrectCredential);
                     return this.Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);
                  }

               // ...
               //=============================================================================
               case SignInStatus.LockedOut:
                  return this.View("Lockout");

               // ...
               //=============================================================================
               case SignInStatus.RequiresVerification:
                  return this.RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

               // ...
               //=============================================================================
               case SignInStatus.Failure:
               default:
                  this.ModelState.AddModelError(Utility.Constants.UserController_Password, Utility.Constants.UserController_IncorrectCredential);
                  return this.Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error("Failed to verify password", ex);
            return this.View("Login", model);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="returnUrl"></param>
      /// <returns></returns>
      ///****************************************************************************
      private ActionResult RedirectToLocal(string returnUrl)
      {
         if (this.Url.IsLocalUrl(returnUrl))
         {
            return this.Redirect(returnUrl);
         }

         return this.RedirectToAction("Home", "User");
      }

      ///****************************************************************************
      /// <summary>
      ///   Change Password screen.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      public ActionResult ChangePassword()
      {
         return this.View("ResetPassword");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ChangePasswordViewModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public ActionResult ChangePassword(ChangeUserPassword ChangePasswordViewModel)
      {
         try
         {
            var userinformation = JsonConvert.DeserializeObject<UserRegistrationRequest>(ChangePasswordViewModel.UserInfo);
            ChangePasswordViewModel.UserID = userinformation.UserID ?? 0;
            var objEncrypt = new MD5Encryption();
            var password = ChangePasswordViewModel.NewPassword;
            ChangePasswordViewModel.NewPassword = objEncrypt.EncryptPassword(ChangePasswordViewModel.NewPassword);
            ChangePasswordViewModel.OldPassword = objEncrypt.EncryptPassword(ChangePasswordViewModel.OldPassword);
            ChangePasswordViewModel.ConfirmPassword = "";

            if (UsersDataAccess.ChangePassword(ChangePasswordViewModel))
            {
               var user = this.UserManager.FindById(this.User.Identity.GetUserId());
               var code = this.UserManager.GeneratePasswordResetToken(user.Id);
               var result = this.UserManager.ResetPassword(user.Id, code, password);

               if (result.Succeeded)
               {
                  return this.RedirectToAction("Index", "Home");
               }
            }

            return this.View("ResetPassword");
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Login", "User");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="temppassword"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public string ValidateTempPassword(string temppassword)
      {
         try
         {
            var objEncrypt = new MD5Encryption();
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

            var objChangeUserPassword = new ChangeUserPassword
            {
               UserID = objReturnLoginUserEntity.UserReturnData.UserID ?? 0,
               NewPassword = objEncrypt.EncryptPassword(temppassword)
            };

            return UsersDataAccess.ValidateUserForgotPassword(objChangeUserPassword)
                     ? Utility.Constants.UserController_SuccessResult
                     : Utility.Constants.UserController_UnsuccessResult;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return Utility.Constants.UserController_Fail;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult UserRegister(string message)
      {
         try
         {
            var objUserRegistrationRequest = new UserRegistrationRequest { UserTypeCode = this.GetUserTypeCode() };
            var profileGroups = GroupDataAccess.GetMutipleGroup(this.GetUserRollupProfileCode());

            objUserRegistrationRequest.MLProfileGroup = profileGroups;
            objUserRegistrationRequest.ProfileCode = this.GetUserRollupProfileCode();
            objUserRegistrationRequest.MLUserGroups = new List<MLGroup>();

            this.PopulateAssignedUserProfiles(objUserRegistrationRequest);
            this.ViewBag.UserType = new SelectList(UserType, Utility.Constants.UserController_UserTypeCode, Utility.Constants.UserController_UserTypeName);
            this.ViewBag.UserSucessmessage = message;

            return this.View("UserRegister", objUserRegistrationRequest);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.View();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult View1()
      {
         try
         {
            var profileGroups = GroupDataAccess.GetMutipleGroup(this.GetUserRollupProfileCode());
            var objUserRegistrationRequest = new UserRegistrationRequest
            {
               UserTypeCode = this.GetUserTypeCode(),
               MLProfileGroup = profileGroups,
               ProfileCode = this.GetUserRollupProfileCode(),
               MLUserGroups = new List<MLGroup>()
            };

            this.PopulateAssignedUserProfiles(objUserRegistrationRequest);
            this.ViewBag.UserType = new SelectList(UserType, Utility.Constants.UserController_UserTypeCode, Utility.Constants.UserController_UserTypeName);

            return this.View("View1", objUserRegistrationRequest);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.View();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserRegistrationRequest"></param>
      ///****************************************************************************
      private void PopulateAssignedUserProfiles(UserRegistrationRequest objUserRegistrationRequest)
      {
         try
         {
            var objProfileSeach = new ProfileSeach { ProfileCode = this.GetUserProfileCode() };
            var allUserProfiles = ProfilesDataAccess.GetMutipleProfileNameWithCode(objProfileSeach.ProfileCode);
            var userProfiles = new HashSet<int>(objUserRegistrationRequest.MLProfileGroup.Select(c => c.GroupProfileCode));
            var viewModel = allUserProfiles.Select(usprl => new AssignedUserProfilesData
            {
               profileCode = usprl.ProfileCode,
               ProfileName = usprl.ProfileName.Trim(),
               Assigned = userProfiles.Contains(usprl.ProfileCode)
            }).ToList();

            this.ViewBag.UserProfiles = viewModel;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref UserSearch objUserSearch)
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         objUserSearch.UserID = objReturnLoginUserEntity.UserReturnData.UserID ?? 0;
         objUserSearch.UserTypeCode = objReturnLoginUserEntity.UserReturnData.UserTypeCode;
         objUserSearch.ProfileType = objReturnLoginUserEntity.UserReturnData.UserProfileType;
         objUserSearch.ProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
         objUserSearch.UserTypeName = this.GetUserType(objReturnLoginUserEntity.UserReturnData.UserTypeCode);
         objUserSearch.RollupProfileCode = objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserTypeCode()
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         return objReturnLoginUserEntity.UserReturnData.UserTypeCode;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userTypeCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType(string userTypeCode)
      {
         return userTypeCode == "ADMN"
                  ? Utility.Constants.HomeController_ADMIN
                  : Utility.Constants.HomeController_OTHERS;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetUserProfileCode()
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         return objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetUserRollupProfileCode()
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         return objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> UserRegister(UserRegistrationRequest model)
      {
         try
         {
            if (this.ModelState.IsValid)
            {
               var iuserId = QoskSessionHelper.GetSessionData(Utility.Constants.UserController_SelectedProfileID);

               if (iuserId != null && (int)iuserId > 0)
               {
                  model.ProfileCode = (int)iuserId;
               }

               var selectedgroupprofile = model.SelectedgroupProfiles?.Split(',');

               if (selectedgroupprofile != null)
               {
                  model.MLProfileGroup = new List<AssignedProfileGroup>();

                  foreach (var selecedprofileCodeRegion in selectedgroupprofile.Select(userProfile => userProfile.Split('_')))
                  {
                     model.MLProfileGroup.Add(new AssignedProfileGroup
                     {
                        ProfileGroupID = int.Parse(selecedprofileCodeRegion[0]),
                        GroupProfileCode = int.Parse(selecedprofileCodeRegion[1]),
                        Region = Convert.ToString(selecedprofileCodeRegion[2])
                     });
                  }
               }

               var selectedallgroups = model.Selectedgroups?.Split(',');

               if (selectedallgroups != null)
               {
                  model.MLUserGroups = new List<MLGroup>();

                  foreach (var selecedCodeRegion in selectedallgroups.Select(userProfileGrp => userProfileGrp.Split('_')))
                  {
                     model.MLUserGroups.Add(new MLGroup
                     {
                        ProfileCode = int.Parse(selecedCodeRegion[0]),
                        ProfileGroupID = int.Parse(selecedCodeRegion[1])
                     });
                  }
               }

               var objEncrypt = new MD5Encryption();
               var userPassword = objEncrypt.EncryptPassword(model.Password);

               var user = new ApplicationUser
               {
                  UserName = model.UserName,
                  Email = model.Email,
                  PhoneNumber = model.PhoneNumber,
                  FirstName = model.FirstName,
                  LastName = model.LastName,
                  CreatedBy = model.UserName,
                  CreatedDate = DateTime.UtcNow,
                  Password = userPassword,
                  FaxNumber = model.FaxNumber,
                  UserTypeCode = model.UserTypeCode,
                  ProfileCode = model.ProfileCode,
                  ModifiedDate = DateTime.UtcNow,
                  IsDeleted = false
               };
               var result = await this.UserManager.CreateAsync(user, model.Password);

               if (result.Succeeded)
               {
                  var userSelectRole = string.Empty;

                  switch (model.UserTypeCode)
                  {
                     // ...
                     //=============================================================================
                     case "SITE":
                        userSelectRole = Utility.Constants.UserController_User;
                        break;

                     // ...
                     //=============================================================================
                     case "CORP":
                        userSelectRole = Utility.Constants.UserController_CUser;
                        break;

                     // ...
                     //=============================================================================
                     case "REGN":
                        userSelectRole = Utility.Constants.UserController_RUser;
                        break;

                     // ...
                     //=============================================================================
                     case "ADMN":
                        userSelectRole = Utility.Constants.UserController_Admin;
                        break;
                  }

                  this.AddUserToRole(user.Id, userSelectRole);

                  // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                  // Send an email with this link
                  UsersDataAccess.UserRegistration(model, user.Id);
                  this.ViewBag.UserSucessmessage = Utility.Constants.UserController_Success;

                  return this.RedirectToAction("UserRegister", new { message = this.ViewBag.UserSucessmessage });
               }

               this.ViewBag.UserSucessmessage = Utility.Constants.UserController_FailResult;
               return this.View("UserRegister", model);
            }

            return this.View(model);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.RedirectToAction("UserRegister", "User", new { message = Utility.Constants.UserController_FailResult });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="name"></param>
      /// <returns></returns>
      ///****************************************************************************
      public bool CreateRole(string name)
      {
         using (var applicationDbContext = new ApplicationDbContext())
         {
            using (var roleStore = new RoleStore<ApplicationRole>(applicationDbContext))
            {
               using (var roleManager = new RoleManager<ApplicationRole>(roleStore))
               {
                  // var role = new IdentityRole(name);
                  var idResult = roleManager.Create(new ApplicationRole(name, Utility.Constants.UserController_Admin));
                  return idResult.Succeeded;
               }
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Add Role to user.
      /// </summary>
      /// <param name="userId"></param>
      /// <param name="roleName"></param>
      /// <returns></returns>
      ///****************************************************************************
      private bool AddUserToRole(string userId, string roleName)
      {
         var idResult = this.UserManager.AddToRole(userId, roleName);
         return idResult.Succeeded;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User Information.
      /// </summary>
      /// <param name="usersVM"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult UpdateUserData(UserRegistrationRequest model)
      {
         try
         {
            model.ModifiedBy = this.User.Identity.Name;
            var objclsResponse = UsersDataAccess.UpdateUser(model);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User Information.
      /// </summary>
      /// <param name="usersVM"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public ActionResult Edit(int? id)
      {
         try
         {
            if (!id.HasValue)
            {
               var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);
               if (objReturnLoginUserEntity.UserReturnData.UserID != null) id = (int)objReturnLoginUserEntity.UserReturnData.UserID;
            }
            var objUserData = UsersDataAccess.GetUserByUserId(id.Value);
            var usertypelist = UsersDataAccess.GetUserDefaultData().UserType;

            this.ViewBag.UserType = usertypelist.Select(n => new SelectListItem
            {
               Text = n.UserTypeName,
               Value = n.UserTypeCode
            });

            this.PopulateAssignedUserProfiles(objUserData);

            var profileGroups = GroupDataAccess.GetMutipleGroup(objUserData.ProfileCode ?? 0);
            var userProfileGrps = new HashSet<int>(objUserData.MLUserGroups.Select(c => c.ProfileGroupID));
            var viewGroupModel = profileGroups.Select(usprl => new AssignedProfileGroup
            {
               ProfileGroupID = usprl.ProfileGroupID,
               GroupName = usprl.GroupName,
               Assigned = userProfileGrps.Contains(usprl.ProfileGroupID)
            }).ToList();

            objUserData.MLProfileGroup = viewGroupModel;
            return this.View("Edit", objUserData);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User Information.
      /// </summary>
      /// <param name="usersVM"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public ActionResult Setting(int? id)
      {
         try
         {
            if (!id.HasValue)
            {
               var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);
               if (objReturnLoginUserEntity.UserReturnData.UserID != null) id = (int)objReturnLoginUserEntity.UserReturnData.UserID;
            }
            var objUserData = UsersDataAccess.GetUserByUserId(id.Value);
            var usertypelist = UsersDataAccess.GetUserDefaultData().UserType;

            this.ViewBag.UserType = usertypelist.Select(n => new SelectListItem
            {
               Text = n.UserTypeName,
               Value = n.UserTypeCode
            });

            this.PopulateAssignedUserProfiles(objUserData);

            var profileGroups = GroupDataAccess.GetMutipleGroup(objUserData.ProfileCode ?? 0);
            var userProfileGrps = new HashSet<int>(objUserData.MLUserGroups.Select(c => c.ProfileGroupID));
            var viewGroupModel = profileGroups.Select(usprl => new AssignedProfileGroup
            {
               ProfileGroupID = usprl.ProfileGroupID,
               GroupName = usprl.GroupName,
               Assigned = userProfileGrps.Contains(usprl.ProfileGroupID)
            }).ToList();

            objUserData.MLProfileGroup = viewGroupModel;
            return this.View("Setting", objUserData);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public JsonResult Edit(UserRegistrationRequest model)
      {
         try
         {
            var selectedgroupprofile = model.SelectedgroupProfiles?.Split(',').Distinct();

            if (selectedgroupprofile != null)
            {
               model.MLProfileGroup = new List<AssignedProfileGroup>();

               foreach (var selecedprofileCodeRegion in selectedgroupprofile.Select(userProfile => userProfile.Split('_')))
               {
                  model.MLProfileGroup.Add(new AssignedProfileGroup
                  {
                     ProfileGroupID = int.Parse(selecedprofileCodeRegion[0]),
                     GroupProfileCode = int.Parse(selecedprofileCodeRegion[1]),
                     Region = Convert.ToString(selecedprofileCodeRegion[2])
                  });
               }
            }

            var selectedallgroups = model.Selectedgroups?.Split(',').Distinct();

            if (selectedallgroups != null)
            {
               model.MLUserGroups = new List<MLGroup>();

               foreach (var selecedprofileCodeRegion in selectedallgroups.Select(userProfileGrp => userProfileGrp.Split('_')))
               {
                  model.MLUserGroups.Add(new MLGroup
                  {
                     ProfileCode = int.Parse(selecedprofileCodeRegion[0]),
                     ProfileGroupID = int.Parse(selecedprofileCodeRegion[1])
                  });
               }
            }

            model.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = UsersDataAccess.UpdateUser(model);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete User Information.
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public JsonResult Delete(int id)
      {
         try
         {
            var objclsResponse = UsersDataAccess.DeleteUser(id, this.User.Identity.Name);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = QoskCloudWebResource.Deleted, Status = true }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = QoskCloudWebResource.Deleted, Status = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = QoskCloudWebResource.DeletedFails, Status = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.DeletedFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Validate User EmailAddress and send Email to user.
      /// </summary>
      /// <param name="userEmail"></param>
      /// <param name="pageName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [AllowAnonymous]
      public string ValidateUserEmailAddress(string userEmail, string pageName)
      {
         try
         {
            if (!string.IsNullOrWhiteSpace(userEmail))
            {
               var user = this.UserManager.FindByEmail(userEmail);

               if (user != null)
               {
                  if (pageName.Equals(Utility.Constants.UserController_ForgotPassword))
                  {
                     var NtwrkCardinalUserName = string.Empty;
                     var NtwrkCardinalPassowrd = string.Empty;
                     var CorporateSMTP = string.Empty;

                     if (AppSettingInformation.lstQoskCloudAppInfo.Count > 0)
                     {
                        NtwrkCardinalUserName = AppSettingInformation.lstQoskCloudAppInfo.Where(x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialUserName)).FirstOrDefault().Value.ToString();
                        NtwrkCardinalPassowrd = AppSettingInformation.lstQoskCloudAppInfo.Where(x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialPassword)).FirstOrDefault().Value.ToString();
                        CorporateSMTP = AppSettingInformation.lstQoskCloudAppInfo.Where(x => x.keyName.Equals(Utility.Constants.UserController_CorporateSMTP)).FirstOrDefault().Value.ToString();
                     }

                     // generate new password
                     var generatenewPassword = SendUserMail.AutogenratePassword();
                     // Message Body
                     var msgBody = this.EmailFormat(generatenewPassword);
                     // Send USer
                     var ToUserEmail = new List<string> { userEmail };

                     if (SendUserMail.SendEmail(ToUserEmail, "", null, Utility.Constants.UserController_NewPassword, msgBody, NtwrkCardinalUserName, NtwrkCardinalPassowrd, CorporateSMTP))
                     {
                        var objEncrypt = new MD5Encryption();
                        var encryptPass = objEncrypt.EncryptPassword(generatenewPassword);
                        var code = this.UserManager.GeneratePasswordResetToken(user.Id);
                        var result = this.UserManager.ResetPassword(user.Id, code, generatenewPassword);

                        if (result.Succeeded)
                        {
                           var OldPass = string.Empty;

                           if (UsersDataAccess.UpdateUserPasword(user.Id, encryptPass, out OldPass))
                           {
                              return Utility.Constants.UserController_Ok;
                           }

                           this.UserManager.ResetPassword(user.Id, code, OldPass);

                           if (result.Succeeded)
                           {
                              return Utility.Constants.UserController_Fails;
                           }
                        }
                     }
                     else
                     {
                        return Utility.Constants.UserController_MailNotSent;
                     }
                  }
                  else if (pageName.Equals(Utility.Constants.UserController_ValidEmail))
                  {
                     return Utility.Constants.UserController_Fails;
                  }
               }
               else
               {
                  if (pageName.Equals(Utility.Constants.UserController_ValidEmail))
                  {
                     return Utility.Constants.UserController_Ok;
                  }

                  if (pageName.Equals(Utility.Constants.UserController_ForgotPassword))
                  {
                     return Utility.Constants.UserController_Fails;
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return Utility.Constants.UserController_Fails;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="newPassord"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string EmailFormat(string newPassord)
      {
         var sb = new System.Text.StringBuilder();

         sb.Append(Utility.Constants.EmailFormat_OpenTable);
         sb.AppendFormat(Utility.Constants.EmailFormat_OpenTableRaw);
         sb.AppendFormat(Utility.Constants.EmailFormat_EndTableRaw, newPassord);
         sb.AppendFormat(Utility.Constants.EmailFormat_OpenTD);
         sb.AppendFormat(Utility.Constants.EmailFormat_EndTD);
         sb.AppendFormat(Utility.Constants.EmailFormat_QualanexQosk);
         sb.Append(Utility.Constants.EmailFormat_EndTable);

         return sb.ToString();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public string ValidateUserName(string userName)
      {
         try
         {
            if (!string.IsNullOrWhiteSpace(userName))
            {
               if (!(UsersDataAccess.ValidateUserName(userName) > 0))
               {
                  return Utility.Constants.UserController_Ok;
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return Utility.Constants.UserController_Fails;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetUserTypes(string profileType)
      {
         try
         {
            var result = new JsonResult();

            if (!string.IsNullOrWhiteSpace(profileType))
            {
               result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
               result.Data = UsersDataAccess.GetUserTypes(profileType).ToList();
            }

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetProfilesGroup(string profileCode)
      {
         try
         {
            var selectGroupProfile = -1;

            if (!string.IsNullOrEmpty(profileCode))
            {
               selectGroupProfile = Convert.ToInt32(profileCode);
            }

            var objGroupProfiles = GroupDataAccess.GetMutipleGroup(selectGroupProfile);

            return this.PartialView("_GroupProfiles", objGroupProfiles);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.PartialView("_GroupProfiles");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetGroupByProfileCodes(int profileCode)
      {
         try
         {
            var grouplist = GroupDataAccess.GetProfileGroup(profileCode);
            var result = new JsonResult
            {
               Data = grouplist.Where(p => p.ProfileGroupID != -1).ToList(),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="userID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult EditProfileGroup(int profileCode, long userID)
      {
         try
         {
            var grouplist = GroupDataAccess.EditProfileGroup(profileCode, userID);
            var result = new JsonResult
            {
               Data = grouplist.Where(p => p.ProfileGroupID != -1).ToList(),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="selectGrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult UserGroupProfiles(string profileCode, string selectGrpID, string selectedRegionCode, string userID)
      {
         try
         {
            var selectedUserID = 0L;

            if (!string.IsNullOrEmpty(userID))
            {
               selectedUserID = Convert.ToInt64(userID);
            }

            var lstGrpProfiles = GroupDataAccess.GetGroupProfiles(profileCode, selectGrpID, selectedRegionCode, selectedUserID);

            return this.PartialView("_ProfileGroups", lstGrpProfiles);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.PartialView("_ProfileGroups");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetRegionCode(string profileCode)
      {
         var selectedProfileCode = -1;

         try
         {
            if (!string.IsNullOrEmpty(profileCode))
            {
               selectedProfileCode = Convert.ToInt32(profileCode);
            }

            var result = new JsonResult
            {
               JsonRequestBehavior = JsonRequestBehavior.AllowGet,
               Data = ProfilesDataAccess.GetRegionCodes(selectedProfileCode).ToList()
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [AllowAnonymous]
      public ActionResult SampleView()
      {
         try
         {
            this.ViewBag.Test = UsersDataAccess.ConnectionTest();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            this.ViewBag.Test = string.Empty;
         }

         return this.View("SampleView");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="selectGrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public JsonResult UserGroupProfilestest(string profileCode, string selectGrpID, string selectedRegionCode, string userID)
      {
         var selectedUserID = 0L;
         List<MLAssignedGroupProfilestest> lstGrpProfiles = null;

         try
         {
            if (!string.IsNullOrEmpty(userID))
            {
               selectedUserID = Convert.ToInt64(userID);
            }

            lstGrpProfiles = GroupDataAccess.GetGroupProfileNested(profileCode, selectGrpID, selectedRegionCode, selectedUserID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(lstGrpProfiles, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="query"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private string BuildQuery(System.Collections.Specialized.NameValueCollection query)
      {
         var filtersCount = int.Parse(query.GetValues(Utility.Constants.PolicyController_filterscount)[0]);

         var tmpDataField = "";
         var tmpFilterOperator = "";
         var where = "";

         if (filtersCount > 0)
         {
            where = Utility.Constants.PolicyController_WHERE;
         }

         for (var i = 0; i < filtersCount; i += 1)
         {
            var filterValue = query.GetValues(Utility.Constants.PolicyController_filtervalue + i)[0];
            var filterCondition = query.GetValues(Utility.Constants.PolicyController_filtercondition + i)[0];
            var filterDataField = query.GetValues(Utility.Constants.PolicyController_filterdatafield + i)[0];
            var filterOperator = query.GetValues(Utility.Constants.PolicyController_filteroperator + i)[0];

            if (string.IsNullOrEmpty(tmpDataField))
            {
               tmpDataField = filterDataField;
            }
            else if (tmpDataField != filterDataField)
            {
               where += Utility.Constants.PolicyController_ANDWITHBREKET;
            }
            else if (tmpDataField == filterDataField)
            {
               if (string.IsNullOrEmpty(tmpFilterOperator))
               {
                  where += Utility.Constants.PolicyController_ANDWITHSPACE;
               }
               else
               {
                  where += Utility.Constants.PolicyController_OR;
               }
            }

            where += this.GetFilterCondition(filterCondition, filterDataField, filterValue);

            if (i == filtersCount - 1)
            {
               where += Utility.Constants.PolicyController_BREKET;
            }

            tmpFilterOperator = filterOperator;
            tmpDataField = filterDataField;
         }

         return where;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="filterCondition"></param>
      /// <param name="filterDataField"></param>
      /// <param name="filterValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private string GetFilterCondition(string filterCondition, string filterDataField, string filterValue)
      {
         switch (filterCondition)
         {
            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EMPTY:
            case Utility.Constants.PolicyController_NOT_NULL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKE;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EMPTY:
            case Utility.Constants.PolicyController_NULL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKE;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_CONTAINS_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_CONTAINS:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_DOES_NOT_CONTAIN_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_DOES_NOT_CONTAIN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EQUAL_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_EQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_EQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EQUAL_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_BINARY + filterDataField + Utility.Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_GREATER_THAN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_GRATERTHENWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_LESS_THAN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LESSTHENWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_GREATER_THAN_OR_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_GRATERTHENEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_LESS_THAN_OR_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LESSTHENEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_STARTS_WITH_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEWITHCOMMA + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_STARTS_WITH:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEWITHCOMMA + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_ENDS_WITH_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_COMMA + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_ENDS_WITH:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_COMMA;
         }

         return "";
      }

      ///****************************************************************************
      /// <summary>
      ///   Get GetUserGroupProfilesData based on profile code and group ID.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="selectGrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetUserGroupProfilesData(string profileCode, string selectGrpID, string selectedRegionCode, string userID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var query = this.Request.QueryString;
         var buildquery = this.BuildQuery(query);

         var selectedUserID = 0L;
         var lstGrpProfiles = new List<AssignedProfileGroup>();

         try
         {
            var totalRecord = 0;

            if (!string.IsNullOrEmpty(userID))
            {
               selectedUserID = Convert.ToInt64(userID);
            }

            lstGrpProfiles = GroupDataAccess.GetUserGroupProfilesData(profileCode, selectGrpID, selectedRegionCode, selectedUserID, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            var result = new
            {
               TotalRows = totalRecord,
               Rows = lstGrpProfiles
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(lstGrpProfiles, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get UserGroupProfileDetails based on selected profileCode and selected
      ///   group ID.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="selectgrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult UserGroupProfileDetails(string profileCode, string selectgrpID, string selectedRegionCode, string userID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var query = this.Request.QueryString;
         var buildquery = this.BuildQuery(query);

         var selectedUserID = 0L;
         var selectedProfileCode = -1;
         var selectedGroupID = -1;

         List<AssignedProfileGroup> lstGrpProfiles = null;

         try
         {
            var totalRecord = 0;

            if (!string.IsNullOrEmpty(userID))
            {
               selectedUserID = Convert.ToInt64(userID);
            }

            if (!string.IsNullOrEmpty(profileCode))
            {
               selectedProfileCode = Convert.ToInt32(profileCode);
            }

            if (!string.IsNullOrEmpty(selectgrpID))
            {
               selectedGroupID = Convert.ToInt32(selectgrpID);
            }

            lstGrpProfiles = GroupDataAccess.UserGroupProfileDetails(selectedProfileCode, selectedGroupID, selectedRegionCode, selectedUserID, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            var result = new
            {
               TotalRows = totalRecord,
               Rows = lstGrpProfiles
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(lstGrpProfiles, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User Information.
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QualanexQoskGlobalFilterAttribute]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public ActionResult UserDetail(int? userId)
      {
         try
         {
            if (!userId.HasValue)
            {
               var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

               if (objReturnLoginUserEntity.UserReturnData.UserID != null)
               {
                  userId = (int)objReturnLoginUserEntity.UserReturnData.UserID;
               }
            }

            var objUserData = UsersDataAccess.GetUserByUserId(userId.Value);
            var usertypelist = UsersDataAccess.GetUserDefaultData().UserType;

            this.ViewBag.UserType = usertypelist.Select(n => new SelectListItem
            {
               Text = n.UserTypeName,
               Value = n.UserTypeCode
            });

            this.PopulateAssignedUserProfiles(objUserData);

            var profileGroups = GroupDataAccess.GetMutipleGroup(objUserData.ProfileCode ?? 0);
            var userProfileGrps = new HashSet<int>(objUserData.MLUserGroups.Select(c => c.ProfileGroupID));
            var viewGroupModel = profileGroups.Select(usprl => new AssignedProfileGroup
            {
               ProfileGroupID = usprl.ProfileGroupID,
               GroupName = usprl.GroupName,
               Assigned = userProfileGrps.Contains(usprl.ProfileGroupID)
            }).ToList();

            objUserData.MLProfileGroup = viewGroupModel;
            return this.View("_UserDetail", objUserData);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult GetSessionTimeout()
      {
         return this.Json(this.Session.Timeout, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }
   }
}