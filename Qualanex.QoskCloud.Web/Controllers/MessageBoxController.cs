﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using Newtonsoft.Json;

using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Web.Common;
using Qualanex.QoskCloud.Web.Controllers;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.Resources;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.QoskCloud;
using Qualanex.Qosk.Library.Model.DBModel;

using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
using System.Data.SqlClient;
using System.Data.Entity.Validation;
using System.IO;

namespace Qualanex.QoskCloud.Web.Controllers
{
   public class MessageBoxController : Controller
   {
      // GET: MessageBox
      public ActionResult Index()
      {
         return View();
      }
      public ActionResult BindColumnListing(string strId)
      {
         ViewBag.Id = strId;
         return PartialView("_MessageBox");
      }
      public ActionResult BindCustomButton(string strId)
      {
         ViewBag.Id = strId;
         return PartialView("_MessageBox");
      }
      public ActionResult Help()
      {
         return PartialView("_Help");
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Username and password validation and add as a valid user.
      /// </summary>
      /// <param name="userName"></param>
      /// <param name="password"></param>
      /// <param name="notes"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CheckUsernameAndPassword(string userName, string password, string notes)
      {
         if (string.IsNullOrWhiteSpace(userName))
         {
            return this.Json(new { Success = false, UserName = "", Error = "Please enter a username." }, JsonRequestBehavior.AllowGet);
         }
         
         if (string.IsNullOrWhiteSpace(password))
         {
            return this.Json(new { Success = false, UserName = "", Error = "Please enter a password." }, JsonRequestBehavior.AllowGet);
         }

         var objEncrypt = new MD5Encryption();
         var selectedPassword = objEncrypt.EncryptPassword(password);
         var userReturnData = ValidateUserLogin(userName, selectedPassword);

         if (userReturnData == null)
         {
            return Json(new { Success = false, UserName = "", Error = "Invalid username/password combination." }, JsonRequestBehavior.AllowGet);
         }

         if (userReturnData.UserName == this.User.Identity.Name
            && userReturnData.UserID == CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId()))
         {
            return Json(new { Success = false, UserName = "", Error = "Cannot verify using logged-in user." }, JsonRequestBehavior.AllowGet);
         }

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var url = HttpContext.Request.UrlReferrer.Segments;
            var action = url[url.Length - 1].Split('/')[0];
            var controller = url[url.Length - 2].Split('/')[0];
            var area = url.Length > 3 ? url[url.Length - 3].Split('/')[0] : "";
            var routeValue = url.Length > 4 ? url[url.Length - 4].Split('/')[0] : "";

            if (!String.IsNullOrWhiteSpace(HttpContext.Request.UrlReferrer.Query))
            {
               action =  HttpContext.Request.UrlReferrer.Query;
               controller = url[url.Length - 1].Split('/')[0];
               area = url[url.Length - 2].Split('/')[0];
               routeValue = url.Length > 3 ? url[url.Length - 3].Split('/')[0] : "";
            }
            try
            {
               var code = (from appCode in cloudEntities.AppDict
                           where appCode.Action == action
                           && appCode.Controller == controller
                           && appCode.Area == area
                           && appCode.RouteValue.ToString() == routeValue
                           && !appCode.IsDeleted
                           select new { appCode.Code, appCode.Description }).FirstOrDefault();

               if (string.IsNullOrWhiteSpace(code.Code))
               {
                  return this.Json(new { Success = false, UserName = userName, Error = "Application missing in AppDict." }, JsonRequestBehavior.AllowGet);
               }

               if (!HasPrivileges((long)userReturnData.UserID, code.Code))
               {
                  return this.Json(new { Success = false, UserName = userName, Error = "User lacks verification privileges." }, JsonRequestBehavior.AllowGet);
               }

               var verification = new VerificationHist()
               {
                  AppCode = code.Code,
                  UserID = userReturnData.Name,
                  //SeqNo = code.SeqNo,
                  CreatedBy = userReturnData.UserName,
                  CreatedDate = DateTime.UtcNow,
                  IsDeleted = false,
                  Version = 1,
                  Notes = notes
               };

               cloudEntities.VerificationHist.Add(verification);
               cloudEntities.SaveChanges();

               return this.Json(new { Success = true, UserName = userReturnData.UserName, Error = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
               Logger.Error(ex);
               return this.Json(new { Success = false, UserName = userName, Error = "Error!" }, JsonRequestBehavior.AllowGet);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Validate User Information.
      /// </summary>
      /// <param name="userName"></param>
      /// <param name="userPassword"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static UserRegistrationRequest ValidateUserLogin(string userName, string userPassword)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (from user in cloudEntities.User
                    join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                    join profileType in cloudEntities.ProfileTypeDict on profile.ProfileTypeCode equals profileType.Code
                    join userType in cloudEntities.UserTypeDict on user.UserTypeCode equals userType.Code
                    where user.UserName.Equals(userName.Trim()) && user.Password.Equals(userPassword)
                    select new UserRegistrationRequest
                    {
                       Name = user.Id,
                       UserName = user.UserName,
                       UserID = user.UserID
                    }).SingleOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Validate User for Privileges.
      /// </summary>
      /// <param name="userId"></param>
      /// <param name="appCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public bool HasPrivileges(long userId, string appCode)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var t = (from user in cloudEntities.User
                     join userRoles in cloudEntities.UserRole on user.Id.ToLower() equals userRoles.UserId.ToLower()
                     join roles in cloudEntities.Role on userRoles.RoleId.ToLower() equals roles.RoleId.ToLower()
                     where !user.IsDeleted && !userRoles.IsDeleted && !roles.IsDeleted
                           && roles.Name == appCode && user.UserID == userId
                     select roles).SingleOrDefault();

            return t != null;
         }
         
         return true;
      }

      public ActionResult CheckCurrentUserValidation()
      {
         using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var url = HttpContext.Request.UrlReferrer.Segments;
            var action = url[url.Length - 1].Split('/')[0];
            var controller = url[url.Length - 2].Split('/')[0];
            var area = url.Length > 3 ? url[url.Length - 3].Split('/')[0] : "";
            var routeValue = url.Length > 4 ? url[url.Length - 4].Split('/')[0] : "";
            var code = (from appCode in cloudEntities.AppDict
                        where appCode.Action == action
                        && appCode.Controller == controller
                        && appCode.Area == area
                        && appCode.RouteValue == routeValue
                        select new { appCode.Code, appCode.Description }).FirstOrDefault();
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            return this.Json(HasPrivileges(UserId, code.Code), JsonRequestBehavior.AllowGet);
         }

      }
      public ActionResult FormTesting()
      {
         var viewModel = new Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtViewModel
         {
            UPAMgmtModels = Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.GetUPAMgmtEntities(false),
            ActionMenu = Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.GetActionMenu()
         };

         if (viewModel.UPAMgmtModels.Any())
         {
            viewModel.SelectedUPAMgmtModel = viewModel.UPAMgmtModels[0];
         }
         return this.PartialView("_FormTesting", viewModel);
         //return Json(viewModel,JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save UPAConfiguration changes.
      /// </summary>
      /// <param name="fieldValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult checkValidationField(string fieldValue)
      {
         var blnCheck = Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.CheckCode(fieldValue);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save UPASummary changes.
      /// </summary>
      /// <param name="bodyData"></param>
      /// <param name="code"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SaveUpaSummaryDetail(string bodyData, string code)
      {
         var curUser = this.HttpContext.User.Identity.Name;
         var blnCheck = code == "0"
                        ? Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.NewUpaSummary(curUser, bodyData)
                        : Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.UpdateUpaSummary(curUser, bodyData, code);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck,
            Code = code == "0"
                        ? Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.GetOneValue(bodyData, "ID_UPACODE")
                        : code
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isEnable)
      {
         var viewModel = new Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtViewModel
         {
            UPAMgmtModels = Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.GetUPAMgmtEntities(isEnable),
            ActionMenu = Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels.UPAMgmtModel.GetActionMenu()
         };

         if (viewModel.UPAMgmtModels.Any())
         {
            viewModel.SelectedUPAMgmtModel = viewModel.UPAMgmtModels[0];
         }

         return this.PartialView("_MessageboxUPAMgmtSummary", viewModel);
      }
   }
}