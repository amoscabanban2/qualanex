﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="IQoskController.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Web;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System.Web.Mvc;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public interface IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      ActionResult OnChangeUserProfileCode(int profileCode);
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UserProfileController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static bool OnChangeUserProfileCode(HttpContextBase httpContext, int profileCode)
      {
         var result = false;

         try
         {
            if (!string.IsNullOrWhiteSpace(httpContext.User.Identity.Name) && !httpContext.User.Identity.Name.StartsWith("TEST", true, CultureInfo.CurrentCulture))
            {
               if (UsersDataAccess.SaveUserProfile(profileCode, UsersDataAccess.ValidateUserName(httpContext.User.Identity.Name)))
               {
                  var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

                  result = objReturnLoginUserEntity.UserReturnData != null;

                  if (result)
                  {
                     objReturnLoginUserEntity.UserReturnData.ProfileCode = profileCode;
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }
   }

}

