﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Qualanex.QoskCloud.Web.Areas.Product.Models;
using Qualanex.QoskCloud.Web.Model;

namespace Qualanex.QoskCloud.Web.Controllers
{
   public class RelationControlDemoController : Controller
   {
      // GET: RelationControl
      public ActionResult Index()

      {
         var lr = new LotResults();

         var r = new RelationControl()
         {
            Rows = new List<object>(){
              new LotResults()
                    {
                        ExpirationDate = DateTime.UtcNow,
                        LotNumber = "relation",
                        ProductId = 1,
                        RecallId = 1234,
                        ExpNa = false,
                        LotNa = true
                    },
                    new LotResults()
                    {
                        ExpirationDate = DateTime.UtcNow,
                        LotNumber = "a",
                        ProductId = 2,
                        RecallId = 1234,
                        ExpNa = false,
                        LotNa = false
                    },
                    new LotResults()
                    {
                        ExpirationDate = DateTime.UtcNow,
                        LotNumber = "b",
                        ProductId = 3,
                        RecallId = 1234,
                        ExpNa = false,
                        LotNa = true
                    },
                    new LotResults()
                    {
                        ExpirationDate = DateTime.UtcNow,
                        LotNumber = "relation",
                        ProductId = 4,
                        RecallId = 1234,
                        ExpNa = false,
                        LotNa = true
                    },
                },
            Options = new RelationControlOptions()
            {
               AddMethod = this.Url.Action(nameof(this.AddRow)),
               UpdateMethod = this.Url.Action(nameof(this.AddRow)),
               SelectMethod = "relationDemoSelect",
               DeleteMethod = "relationDemoDelete",
               InitialReadOnly = false,
               ShowRowControls = false,
               SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>() { { nameof(lr.LotNumber), new List<SelectListItem>() { new SelectListItem() { Text = "lot 1234", Value = "relation" }, new SelectListItem() { Text = "lot a", Value = "a" }, new SelectListItem() { Text = "lot b", Value = "b" } } } },
               HiddenProperties = new List<string>() { nameof(lr.ExpirationDate) },
               ReadOnlyProperties = new List<string>() { nameof(lr.ProductId) },
               RowProperty = new LotResults(),
               CustomControls = new List<CustomControl>()
               {
                  new CustomControl() { EditView = "_CustomColumnMiddle", ReadOnlyView = "_CustomColumnMiddle", AddView = "_CustomColumnMiddle"},
                  new CustomControl { EditView = "_CustomColumn", ReadOnlyView = "_CustomColumn", AddView = "_CustomColumn" }
               }
            },

         };


         return View(r);
      }


      [ValidateAntiForgeryToken]
      public ActionResult AddRow(LotResults lot)
      {
         if (lot.RecallId == null)
         {
            return this.Json(new { error = "RecallId cannot be null" });
         }
         if (lot.ProductId == 0)
         {
            var rnd = new Random();
            lot.ProductId = rnd.Next(4, 5000000);
         }
         if (lot.LotNumber == "b")
         {
            lot.ExpNa = true;
            lot.LotNa = false;
            lot.LotNumber = "relation";
         }
         return this.Json(lot);
      }
   }
}