﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="CommonController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System.Web.Mvc;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class CommonController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult LogedINUserDetails()
      {
         return this.Json(new { UserID = 0, UserName = this.User.Identity.Name }, JsonRequestBehavior.AllowGet);
      }



      public ActionResult DeviceTest()
      {
         return this.View();
      }
   }
}