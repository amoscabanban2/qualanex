﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="QoskController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Threading.Tasks;
   using System.Web.Mvc;

   using Microsoft.AspNet.Identity;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Resources;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   public class ProfileController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public ActionResult Index()
      {
         try
         {
            var objProfileRequestEntity = new ProfileRequestEntity();

            if (this.ViewBag.Type == null)
            {
               this.ViewBag.Type = ProfilesDataAccess.ListProfileType().Select(n => new SelectListItem
               {
                  Text = n.Type,
                  Value = n.Type.ToString()
               }).Distinct().GroupBy(x => x.Text).Select(y => y.First());
            }

            return this.View(objProfileRequestEntity);
         }
         catch (Exception ex)
         {
            throw;
            //Logger.Error(ex.Message);
            //return this.RedirectToAction("Home","User");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ddProfileName"></param>
      /// <param name="ddType"></param>
      /// <param name="txtCity"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public JsonResult BindProfileGrid(string ddProfileName, string ddType, string txtCity, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            var objProfileRequestEntity = new ProfileRequestEntity();
            var objProfileSeach = new ProfileSeach
            {
               ProfileCode = !string.IsNullOrEmpty(ddProfileName) ? Convert.ToInt32(ddProfileName) : 0,
               SerachProfileType = !string.IsNullOrEmpty(ddType) ? ddType : "",
               City = !string.IsNullOrEmpty(txtCity) ? txtCity : ""
            };

            this.GetCurrentUserData(ref objProfileSeach);
            objProfileRequestEntity.profilesLists = ProfilesDataAccess.ListSearchAllProfiles(query, objProfileSeach, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            var result = new
            {
               TotalRows = totalRecord,
               Rows = objProfileRequestEntity.profilesLists
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(null, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="filterCondition"></param>
      /// <param name="filterDataField"></param>
      /// <param name="filterValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private string GetFilterCondition(string filterCondition, string filterDataField, string filterValue)
      {
         switch (filterCondition)
         {
            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EMPTY:
            case Utility.Constants.PolicyController_NOT_NULL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKE;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EMPTY:
            case Utility.Constants.PolicyController_NULL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKE;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_CONTAINS_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_CONTAINS:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_DOES_NOT_CONTAIN_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_DOES_NOT_CONTAIN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTLIKEPERCENT + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EQUAL_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_EQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_EQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EQUAL_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_BINARY + filterDataField + Utility.Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_NOT_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_GREATER_THAN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_GRATERTHENWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_LESS_THAN:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LESSTHENWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_GREATER_THAN_OR_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_GRATERTHENEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_LESS_THAN_OR_EQUAL:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LESSTHENEQUALWITHCOMMA + filterValue + Utility.Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_STARTS_WITH_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEWITHCOMMA + filterValue + Utility.Constants.PolicyController_PERCENT + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_STARTS_WITH:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEWITHCOMMA + filterValue + Utility.Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_ENDS_WITH_CASE_SENSITIVE:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_COMMA + Utility.Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Utility.Constants.PolicyController_ENDS_WITH:
               return Utility.Constants.PolicyController_Space + filterDataField + Utility.Constants.PolicyController_LIKEPERCENT + filterValue + Utility.Constants.PolicyController_COMMA;
         }

         return "";
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="query"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private string BuildQuery(System.Collections.Specialized.NameValueCollection query)
      {
         var where = "";

         if (query != null)
         {
            var filtersCount = int.Parse(query.GetValues(Utility.Constants.PolicyController_filterscount)[0]);

            var tmpDataField = "";
            var tmpFilterOperator = "";

            if (filtersCount > 0)
            {
               where = Utility.Constants.PolicyController_WHERE;
            }

            for (var i = 0; i < filtersCount; i += 1)
            {
               var filterValue = query.GetValues(Utility.Constants.PolicyController_filtervalue + i)[0];
               var filterCondition = query.GetValues(Utility.Constants.PolicyController_filtercondition + i)[0];
               var filterDataField = query.GetValues(Utility.Constants.PolicyController_filterdatafield + i)[0];
               var filterOperator = query.GetValues(Utility.Constants.PolicyController_filteroperator + i)[0];

               if (string.IsNullOrEmpty(tmpDataField))
               {
                  tmpDataField = filterDataField;
               }
               else if (tmpDataField != filterDataField)
               {
                  where += Utility.Constants.PolicyController_ANDWITHBREKET;
               }
               else if (tmpDataField == filterDataField)
               {
                  if (string.IsNullOrEmpty(tmpFilterOperator))
                  {
                     where += Utility.Constants.PolicyController_ANDWITHSPACE;
                  }
                  else
                  {
                     where += Utility.Constants.PolicyController_OR;
                  }
               }

               // Build the "WHERE" clause depending on the filter's condition, value and datafield.
               where += this.GetFilterCondition(filterCondition, filterDataField, filterValue);

               if (i == filtersCount - 1)
               {
                  where += Utility.Constants.PolicyController_BREKET;
               }

               tmpFilterOperator = filterOperator;
               tmpDataField = filterDataField;
            }
         }

         return where;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult Create()
      {
         try
         {
            var objProfileRequest = new ProfileRequest();

            if (this.ViewBag.ManufacturerName == null)
            {
               objProfileRequest.lstmanufacturer = PolicyDataAccess.GetProfileListByType(Utility.Constants.ProductController_Manufacturer).ToList();

               this.ViewBag.ManufacturerName = objProfileRequest.lstmanufacturer.Select(n => new SelectListItem
               {
                  Text = n.Name,
                  Value = n.ProfileCode.ToString()
               });
            }

            var Type = ProfilesDataAccess.ListProfileType().Where(p => p.Type != Utility.Constants.ProfileController_ToBeDeleted).ToList().Select(n => new SelectListItem
            {
               Text = n.Type,
               Value = n.Type.ToString()
            }).Distinct();

            this.ViewBag.ProfileType = Type.GroupBy(x => x.Text).Select(y => y.First());

            this.ViewBag.AccountProfileType = Type.Where(p => p.Text != Utility.Constants.ProductController_Manufacturer && p.Text != Utility.Constants.ProfileController_ManufacturerWPT).ToList();
            this.ViewBag.AccountProfileCode = ProfilesDataAccess.GetAccountManufactureRepackagerType()
                                                .Select(n => new SelectListItem
                                                   {
                                                      Text = n.Name + Utility.Constants.ProfileController_LeftBreket + n.ProfileCode + Utility.Constants.ProfileController_RightBreket,
                                                      Value = n.ProfileCode.ToString().Trim()
                                                   }).Distinct();

            return this.View("Create", objProfileRequest);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult Create(ProfileRequest model)
      {
         try
         {
            if (this.ModelState.IsValid)
            {
               model.ModifiedBy = model.CreatedBy = this.GetUserName();

               if (ProfilesDataAccess.RecordProfile(model))
               {
                  this.ViewBag.ProfileSucessmessage = Resources.Profile.Profile_Create_Message;
                  return this.RedirectToAction("Create");
               }

               this.ViewBag.ProfileSucessmessage = Resources.Profile.Save_Fails;
               return this.View("Create", model);
            }

            return this.RedirectToAction("Create");
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Create");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userTypeID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult ProfileCodes(string userTypeID)
      {
         try
         {
            var result = new JsonResult();

            if (!string.IsNullOrWhiteSpace(userTypeID))
            {
               var lst = new ManufactureNameList();
               var objProfileSeach = new ProfileSeach();

               this.GetCurrentUserData(ref objProfileSeach);
               lst.lstManufacturer = ProfilesDataAccess.GetProfileCode(userTypeID, objProfileSeach);

               result.Data = lst.lstManufacturer.OrderBy(p => p.Name).ToList();
               result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            }

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult Profiles(string profileType)
      {
         try
         {
            var result = new JsonResult();

            if (!string.IsNullOrWhiteSpace(profileType))
            {
               result.Data = profileType.Contains(Utility.Constants.ProfileController_Grp)
                                    ? ProfilesDataAccess.GetProfilesByGroupType(profileType).ToList()
                                    : ProfilesDataAccess.GetProfilesByType(profileType).ToList();

               result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            }

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetGroupTypes(string groupType)
      {
         try
         {
            var result = new JsonResult();

            if (!string.IsNullOrWhiteSpace(groupType))
            {
               result.Data = ProfilesDataAccess.GetProfilesByGroupType(groupType).ToList();
               result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            }

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public ActionResult Edit(int id)
      {
         try
         {
            var objProfileRequest = ProfilesDataAccess.GetProfile(id, this.GetCurrentUserRollupProfileCode());

            // Temp data for Type
            if (this.ViewBag.Type == null)
            {
               var Type = ProfilesDataAccess.ListProfileType().Select(n => new SelectListItem
               {
                  Text = n.Type,
                  Value = n.Type.ToString()
               }).Distinct();

               this.ViewBag.ProfileType = Type;
               this.ViewBag.AccountProfileType = Type.Where(p => p.Text != Utility.Constants.ProductController_Manufacturer && p.Text != Utility.Constants.ProfileController_ManufacturerWPT).ToList();
               this.ViewBag.Type = Type;
            }
            else
            {
               var newType = (IEnumerable<SelectListItem>)this.ViewBag.Type;

               this.ViewBag.ProfileType = newType;
               this.ViewBag.AccountProfileType = newType.Where(p => p.Text != Utility.Constants.ProductController_Manufacturer && p.Text != Utility.Constants.ProfileController_ManufacturerWPT).ToList();
            }

            // Temp data for  Profile Account Type
            if (this.ViewBag.AccountType == null)
            {
               var manufactureRepacakagerdata = ProfilesDataAccess.GetAccountManufactureRepackagerType().Select(n => new SelectListItem
               {
                  Text = n.Name + Utility.Constants.ProfileController_LeftBreket + n.ProfileCode + Utility.Constants.ProfileController_RightBreket,
                  Value = n.ProfileCode.ToString().Trim()
               }).Distinct();

               this.ViewBag.AccountProfileCode = manufactureRepacakagerdata;
               this.ViewBag.AccountType = manufactureRepacakagerdata;
            }
            else
            {
               this.ViewBag.AccountProfileCode = (IEnumerable<SelectListItem>)ViewBag.AccountType;
            }

            if (this.ViewBag.ManufacturerName == null)
            {
               objProfileRequest.lstmanufacturer = PolicyDataAccess.GetProfileListByType(Utility.Constants.ProductController_Manufacturer).ToList();

               this.ViewBag.ManufacturerName = objProfileRequest.lstmanufacturer.Select(n => new SelectListItem
               {
                  Text = n.Name,
                  Value = n.ProfileCode.ToString()
               });
            }

            return this.View("Edit", objProfileRequest);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public JsonResult Delete(int id)
      {
         try
         {
            var objclsResponse = ProfilesDataAccess.DeleteProfile(id, this.User.Identity.Name);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = QoskCloudWebResource.DeletedSuccessfully, Status = true}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = QoskCloudWebResource.Deleted, Status = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = QoskCloudWebResource.DeletedFails, Status = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.DeletedFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public JsonResult UpdateProfileFirstSection(ProfileRequest objProfileRequest)
      {
         try
         {
            objProfileRequest.SectionNumber = 1;
            objProfileRequest.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = ProfilesDataAccess.UpdateProfile(objProfileRequest);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public JsonResult UpdateProfileSecondSection(ProfileRequest objProfileRequest)
      {
         try
         {
            objProfileRequest.SectionNumber = 2;
            objProfileRequest.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = ProfilesDataAccess.UpdateProfile(objProfileRequest);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public JsonResult UpdateProfileFourthSection(ProfileRequest objProfileRequest)
      {
         try
         {
            objProfileRequest.SectionNumber = 4;
            objProfileRequest.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = ProfilesDataAccess.UpdateProfile(objProfileRequest);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.LotNumberController_AllUser)]
      public JsonResult UpdateProfileThirdSection(ProfileRequest objProfileRequest)
      {
         try
         {
            objProfileRequest.SectionNumber = 3;
            objProfileRequest.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = ProfilesDataAccess.UpdateProfile(objProfileRequest);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateSuccessfully, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = QoskCloudWebResource.ProfileVersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = QoskCloudWebResource.Deleted, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = QoskCloudWebResource.UpdateFails, UpdatedVersion = objclsResponse.UpdateVersion, VersionChanged = false}, JsonRequestBehavior.AllowGet);

            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return this.Json(QoskCloudWebResource.UpdateFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserName()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserName;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private long GetUserID()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserID ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserTypeCode == "ADMN"
                     ? Utility.Constants.HomeController_ADMIN
                     : Utility.Constants.HomeController_OTHERS;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetCurrentUserProfileCode()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.ProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetCurrentUserRollupProfileCode()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult CreateGroup()
      {
         try
         {
            return this.PartialView("_CreateGroup", new MLGroup());
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupName"></param>
      /// <param name="userProfileCode"></param>
      /// <param name="grpType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public string CreateGroup(string groupName, string userProfileCode, string grpType)
      {
         try
         {
            return GroupDataAccess.CreateGroup(groupName, Convert.ToInt32(userProfileCode), grpType, this.User.Identity.GetUserName()) > 0
                           ? Utility.Constants.ProfileController_Sucesss
                           : Utility.Constants.ProfileController_Fail;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profile"></param>
      /// <param name="groupList"></param>
      /// <param name="GroupTypeName"></param>
      /// <param name="GroupName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public async Task<ActionResult> GroupProfile(string profile, string groupList, string GroupTypeName, string GroupName)
      {
         try
         {
            var groupProfiles = GroupDataAccess.GetGroupProfileCode(this.GetCurrentUserRollupProfileCode(), groupList, profile, GroupName, GroupTypeName);

            await Task.Run(() => this.GetMasterGroupData(ref groupProfiles));

            return this.View("GroupProfile", groupProfiles);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="Profile"></param>
      /// <param name="groupList"></param>
      /// <param name="GroupTypeName"></param>
      /// <param name="GroupName"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public JsonResult GetGroupProfile(string Profile, string groupList, string GroupTypeName, string GroupName, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            if (GroupName.Equals(Utility.Constants.ProfileController_SelectGroup))
            {
               GroupName = "";
            }

            var groupProfiles = GroupDataAccess.GetPartialGroupProfile(this.GetCurrentUserRollupProfileCode(), groupList, Profile, GroupName, GroupTypeName, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);
            var result = new
            {
               TotalRows = totalRecord,
               Rows = groupProfiles
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult ShowSelectedProfileCode(int profileCode, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            var groupProfiles = GroupDataAccess.ShowProfileGroupData(profileCode, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            var result = new
            {
               TotalRows = totalRecord,
               Rows = groupProfiles
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public ActionResult GetGroupProfileDetails(string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            var groupProfiles = GroupDataAccess.GetPartialGroupProfileDetails(sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            var result = new
            {
               TotalRows = totalRecord,
               Rows = groupProfiles
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objMLGroupProfiles"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public ActionResult GetFilterGroupProfile(MLGroupProfiles objMLGroupProfiles)
      {
         try
         {
            return this.PartialView("_GroupGroupData", GroupDataAccess.GetFilterPartialGroupProfile(this.GetCurrentUserRollupProfileCode(), objMLGroupProfiles));
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="GroupIDS"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public string DeleteGroup(string GroupIDS)
      {
         try
         {
            if (GroupDataAccess.DeleteGroups(GroupIDS))
            {
               this.ViewBag.DeleteGroupSuccessMessage = Resources.Profile.Group_delete_message;
               return Utility.Constants.ProfileController_Sucesss;
            }

            this.ViewBag.DeleteGroupSuccessMessage = Resources.Profile.Group_Delete_Fails;
            return Utility.Constants.ProfileController_Fail;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="GroupId"></param>
      /// <param name="ProfileIDS"></param>
      /// <param name="GroupName"></param>
      /// <param name="Profile"></param>
      /// <param name="groupType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public string UpdateGroupProfiles(string GroupId, string ProfileIDS, string GroupName, string Profile, string groupType)
      {
         try
         {
            var isUpdated = "unknown";
            return GroupDataAccess.UpdateGroupProfiles(GroupId, ProfileIDS, this.GetCurrentUserProfileCode(), GroupName, Profile, groupType) != -1 ? isUpdated.ToString() : Utility.Constants.ProfileController_Unsuccess;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return Utility.Constants.ProfileController_Fail;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileGroupID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult GroupType(int profileGroupID)
      {
         try
         {
            var groupProfileType = GroupDataAccess.GetGroupType(profileGroupID);

            var result = new JsonResult
            {
               Data = groupProfileType,
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileSeach"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref ProfileSeach objProfileSeach)
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         objProfileSeach.UserID = objReturnLoginUserEntity.UserReturnData.UserID ?? 0;
         objProfileSeach.UserTypeCode = objReturnLoginUserEntity.UserReturnData.UserTypeCode;
         objProfileSeach.ProfileType = objReturnLoginUserEntity.UserReturnData.UserProfileType;
         objProfileSeach.UserProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
         objProfileSeach.UserTypeName = this.GetUserType(objReturnLoginUserEntity.UserReturnData.UserTypeCode);
         objProfileSeach.RollupProfileCode = objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userTypeCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType(string userTypeCode)
      {
         return userTypeCode == "ADMN"
               ? Utility.Constants.HomeController_ADMIN
               : Utility.Constants.HomeController_OTHERS;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objGroupProfileViewModel"></param>
      ///****************************************************************************
      private void GetMasterGroupData(ref GroupProfileViewModel objGroupProfileViewModel)
      {
         ICollection<ProfileRequest> profileData = null;

         try
         {
            var objGroup = objGroupProfileViewModel.GroupData.Select(n => new SelectListItem
            {
               Text = n.GroupName.ToString(),
               Value = n.ProfileGroupID.ToString()
            }).Distinct();

            objGroupProfileViewModel.GroupList = objGroup;

            if (this.ViewBag.City == null)
            {
               profileData = ProfilesDataAccess.GetProfilesMasterData();

               var objProfileCity = profileData.Where(n => n.City != null)
                  .Select(n => new SelectListItem
                  {
                     Text = n.City.ToString(),
                     Value = Convert.ToString(n.City)
                  });

               objProfileCity = objProfileCity.GroupBy(x => x.Text).Select(y => y.First());
               objGroupProfileViewModel.GroupProfileCity = objProfileCity;

               this.ViewBag.City = objProfileCity;
            }
            else
            {
               objGroupProfileViewModel.GroupProfileCity = (IEnumerable<SelectListItem>)this.ViewBag.City;
            }

            if (this.ViewBag.Region == null)
            {

               var objProfileRegion = profileData
                  .Where(n => n.RegionCode != null)
                  .Select(n => new SelectListItem
                  {
                     Text = (string.IsNullOrEmpty(n.RegionCode)) ? Utility.Constants.ProfileController_NA : n.RegionCode,
                     Value = (string.IsNullOrEmpty(n.RegionCode)) ? " " : n.RegionCode
                  });

               objProfileRegion = objProfileRegion.GroupBy(x => x.Text).Select(y => y.First());
               objGroupProfileViewModel.GroupProfileReason = objProfileRegion;

               this.ViewBag.Region = objProfileRegion;
            }
            else
            {
               objGroupProfileViewModel.GroupProfileReason = (IEnumerable<SelectListItem>)this.ViewBag.Region;
            }

            if (this.ViewBag.State == null)
            {

               var objProfileState = profileData
                  .Where(n => n.State != null)
                  .Select(n => new SelectListItem
                  {
                     Text = n.State,
                     Value = Convert.ToString(n.State)
                  });

               objProfileState = objProfileState.GroupBy(x => x.Text).Select(y => y.First());
               objGroupProfileViewModel.GroupProfileState = objProfileState;

               this.ViewBag.State = objProfileState;
            }
            else
            {
               objGroupProfileViewModel.GroupProfileState = (IEnumerable<SelectListItem>)this.ViewBag.State;
            }

            if (this.ViewBag.ProfileType == null)
            {
               var objGroupProfileType = profileData?.Select(n => new SelectListItem
               {
                  Text = n.Type.ToString(),
                  Value = Convert.ToString(n.Type)
               }).Distinct();

               objGroupProfileType = objGroupProfileType?.GroupBy(x => x.Text).Select(y => y.First());
               objGroupProfileViewModel.GroupProfileType = objGroupProfileType;

               this.ViewBag.ProfileType = objGroupProfileType;
            }
            else
            {
               objGroupProfileViewModel.GroupProfileType = (IEnumerable<SelectListItem>)this.ViewBag.ProfileType;
            }

            if (this.ViewBag.Profile == null)
            {
               var profilelist = ProfilesDataAccess.GetProfiles();

               var objProfile = profilelist
                  .Select(n => new SelectListItem
                  {
                     Text = n.Name,
                     Value = Convert.ToString(n.ProfileCode)
                  });

               objGroupProfileViewModel.Profile = objProfile;
               objGroupProfileViewModel.GroupDataEntity = new MLGroup { ProductProfileList = objProfile };

               this.ViewBag.Profile = objProfile;
            }
            else
            {
               var list = (IEnumerable<SelectListItem>)this.ViewBag.Profile;

               objGroupProfileViewModel.Profile = list;
               objGroupProfileViewModel.GroupDataEntity = new MLGroup { ProductProfileList = list };
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="terms"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetProfile(string terms)
      {
         var objProfile = ProfilesDataAccess.GetProfilesUsingAutoComplete(terms)
            .Select(n => new SelectListItem
            {
               Text = n.Name,
               Value = Convert.ToString(n.ProfileCode)
            });

         return this.Json(objProfile, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetGroupByProfileCode(int profileCode)
      {
         try
         {
            var result = new JsonResult
            {
               Data = GroupDataAccess.GetProfileGroup(profileCode).ToList(),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult GetGroupByProfileCodes(int profileCode)
      {
         try
         {
            var result = new JsonResult
            {
               Data = GroupDataAccess.GetProfileGroup(profileCode).Where(p => p.ProfileGroupID != -1).ToList(),
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public string GetAccountNumber(int manufactureCode, string accountType, int profileCode, int groupId)
      {
         try
         {
            return ProfilesDataAccess.GetAccountNumber(manufactureCode, accountType, profileCode, groupId);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return string.Empty;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <param name="accountNumber"></param>
      /// <param name="isupdate"></param>
      /// <param name="type"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AdminCuser)]
      public bool SaveUpdateAccountNumber(int manufactureCode, string accountType, int profileCode, int groupId, string accountNumber, bool isupdate, string type)
      {
         try
         {
            return ProfilesDataAccess.SaveUpdateAccountNumber(manufactureCode, accountType, profileCode, groupId, accountNumber, System.Web.HttpContext.Current.User.Identity.Name, isupdate, type.Trim());
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return false;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <param name="accountNumber"></param>
      /// <param name="type"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public JsonResult BindAccountGrid(int manufactureCode, string accountType, int profileCode, string groupId, string accountNumber, string type, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         try
         {
            var totalRecord = 0;

            var query = this.Request.QueryString;
            var buildquery = this.BuildQuery(query);

            var objProfileRequestEntity = new AccountProfileRequestEntity();
            var objProfileRequestData = ProfilesDataAccess.SearchAccountPrifile(manufactureCode, accountType, profileCode, groupId, accountNumber, type, sortdatafield, sortorder, pagesize, pagenum, buildquery, out totalRecord);

            objProfileRequestEntity.AccountProfileLists = (List<ProfileAccountResponse>)objProfileRequestData;

            var result = new
            {
               TotalRows = totalRecord,
               Rows = objProfileRequestEntity.AccountProfileLists
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(null, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profile"></param>
      /// <param name="groupName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Utility.Constants.Role_AllUser)]
      public JsonResult ValiadateProfileGroupName(string profile, string groupName)
      {
         try
         {
            var result = new JsonResult
            {
               Data = string.IsNullOrEmpty(GroupDataAccess.ValiadateProfileGroupName(profile, groupName))
                              ? Utility.Constants.ProfileController_NotExist
                              : Utility.Constants.ProfileController_Exist,
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return result;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ProfileAccount(ProfileRequest objProfileRequest)
      {
         return this.View(objProfileRequest);
      }

   }
}
