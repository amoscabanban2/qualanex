﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ReportController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [QualanexQoskGlobalFilterAttribute]
   [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
   public class ReportController : Controller
   {

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Index()
      {
         return this.View();
      }
   }
}
