﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage.Blob;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Common;

namespace Qualanex.QoskCloud.Web.Controllers
{
   [Authorize(Roles = "QLUser")]
   public class WebcamController : Controller
   {
      /// <summary>
      /// index for a demo page to test camera functionality
      /// </summary>
      /// <returns></returns>
      // GET: Webcam
      public ActionResult WebcamDemo()
      {
         return this.View();
      }

      /// <summary>
      /// Uploads a captured image from a client to an azure blob
      /// </summary>
      /// <param name="file">bit stream for a captured image</param>
      /// <returns>the uri to access the captured image from azure storage</returns>
      [ValidateAntiForgeryToken]
      public ActionResult Capture(string file)
      {
         try
         {
            byte[] bytes = Convert.FromBase64String(file.Replace("data:image/jpeg;base64,", string.Empty));

            if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
            {
               var uri = @"/ProductImages/webcam-uploads";
               var filePath = uri + @"/" + Guid.NewGuid() + ".jpg";
               var savePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + filePath;
               System.IO.File.WriteAllBytes(savePath, bytes);
               return this.Json(new { imageUri = filePath });
            }

            var connectString = Utility.Common.ConfigurationManager.GetConnectionString(Constants.AzureAccount_Image);

            Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer sampleContainer = client.GetContainerReference("webcam-uploads");

            CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(Guid.NewGuid() + ".jpg");

            blob.UploadFromByteArray(bytes, 0, bytes.Length);

            //prepare SharedAccessBlobPolicy class to generate the temporary image for the side and top images
            var sasConstraints = new SharedAccessBlobPolicy
            {
               SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(15),
               Permissions = "Read".ToLower() == Constants.FileAccess_Methods_Write
                  ? SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List
                  : SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
            };

            var tempString = blob.Uri.AbsoluteUri + blob.GetSharedAccessSignature(sasConstraints);

            return this.Json(new { imageUri = blob.Uri.ToString(), tempLink = tempString });
         }
         catch (Exception e)
         {
            //return blank json result so image capture failure message fires
            return this.Json(new {  });
         }
      }

      /// <summary>
      /// Uploads a locally captured video to azure blob storage
      /// </summary>
      /// <returns>uri to access the video from azure</returns>
      [ValidateAntiForgeryToken]
      public ActionResult UploadVideo()
      {
         var connectString = ConfigurationManager.GetConnectionString(Constants.AzureAccount_Video);

         Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

         CloudBlobClient client = account.CreateCloudBlobClient();

         CloudBlobContainer sampleContainer = client.GetContainerReference("webcam-uploads");

         var video = this.Request.Files[0];
         CloudBlockBlob videoBlob = sampleContainer.GetBlockBlobReference(Guid.NewGuid() + ".webm");
         videoBlob.UploadFromStream(video.InputStream);

         return this.Json(new { videoUri = videoBlob.Uri.ToString() });
      }

      /// <summary>
      /// deletes the blob referenced in the url
      /// </summary>
      /// <param name="uri"></param>
      /// <returns></returns>
      [ValidateAntiForgeryToken]
      public ActionResult DeleteImage(string uri)
      {
         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {
            try
            {
               var filePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + uri;
               System.IO.File.Delete(filePath);
            }
            catch (Exception e)
            {
               //do nothing
            }
            return this.Json(new { success = true });
         }
         var blob = GetBlobFromWebcamUri(uri);

         blob.Delete();

         return this.Json(new { success = true });
      }

      /// <summary>
      /// Find the blob reference given a uri string
      /// </summary>
      /// <param name="uri">the uri of a blob in the qoskimages storage account</param>
      /// <returns>blob</returns>
      internal static CloudBlockBlob GetBlobFromWebcamUri(string uri)
      {
         var uriParts = uri.Split('/');
         var connectString = ConfigurationManager.GetConnectionString(Constants.AzureAccount_Image);

         Microsoft.WindowsAzure.Storage.CloudStorageAccount account =
             Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

         CloudBlobClient client = account.CreateCloudBlobClient();

         CloudBlobContainer sampleContainer = client.GetContainerReference("webcam-uploads");

         CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(uriParts.Last());
         return blob;
      }


   }
}