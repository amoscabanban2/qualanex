﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ErrorController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Controllers
{
   ///============================================================================
   /// <summary>
   ///   Manage Error Controller.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ErrorController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ViewResult Error()
      {
         return this.View("Error");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ViewResult NotFound()
      {
         this.Response.StatusCode = 404; // You may want to set this to 200
         return this.View("NotFound");
      }
   }
}