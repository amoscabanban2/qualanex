﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="HomeController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   public class HomeController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Index methods in Home user home page.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult Index()
      {
         return this.View();
      }


        ///****************************************************************************
        /// <summary>
        ///   Bind the Manufacture with their profile code.
        /// </summary>
        /// <returns></returns>
        ///****************************************************************************
        public ActionResult ManufactureDDL()
      {
         if (this.TempData[Constants.HomeController_Manufacture] == null)
         {
            var objUserSearch = new UserSearch();

            this.GetCurrentUserData(ref objUserSearch);

            this.TempData[Constants.HomeController_Manufacture] = ProfilesDataAccess.GetManufactureByUserID(objUserSearch).lstManufacturer.Select(n => new SelectListItem
            {
               Text = n.Name,
               Value = n.ProfileCode.ToString()
            });
         }

         this.TempData.Keep(Constants.HomeController_Manufacture);

         return this.PartialView("ManufactureDDL");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [TraceFilter]
      private long GetUserID()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserID ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserTypeCode()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserTypeCode;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.UserTypeCode == "ADMN"
                     ? Constants.HomeController_ADMIN
                     : Constants.HomeController_OTHERS;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetUserProfileCode()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.ProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private int GetUserRollupProfileCode()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref UserSearch objUserSearch)
      {
         var userReturnData = ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData;

         objUserSearch.UserID = userReturnData.UserID ?? 0;
         objUserSearch.UserTypeCode = userReturnData.UserTypeCode;
         objUserSearch.ProfileType = userReturnData.UserProfileType;
         objUserSearch.ProfileCode = userReturnData.ProfileCode ?? 0;
         objUserSearch.UserTypeName = this.GetUserType(userReturnData.UserTypeCode);
         objUserSearch.RollupProfileCode = userReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userTypeCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType(string userTypeCode)
      {
         return userTypeCode == "ADMN"
                     ? Constants.HomeController_ADMIN
                     : Constants.HomeController_OTHERS;
      }

   }
}