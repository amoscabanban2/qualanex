﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProductController.cs">
///   Copyright (c) 2014 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Web.Mvc;

   using Newtonsoft.Json;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model;
   using Qualanex.QoskCloud.Web.Resources;

   using Qualanex.Qosk.Library.Model.DBModel;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize]
   [QualanexQoskGlobalFilterAttribute]
   public class ProductController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Index Action Used to View the Search and Product Details.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult Index()
      {
         this.ViewBag.PageNo = 0;
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="prodID"></param>
      /// <param name="NDCUPCWithDashes"></param>
      /// <param name="Description"></param>
      /// <param name="PageIdentification"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.UserRoleAdmin)]
      public ActionResult ChangeTrackerDetails(long prodID, string NDCUPCWithDashes, string Description, string PageIdentification)
      {
         var objSearch = new ChangeTrackerSearch
         {
            ID = prodID,
            Description = Description,
            PageIdentification = PageIdentification
         };

         if (string.IsNullOrEmpty(PageIdentification))
         {
            objSearch.NDCUPCWithDashes = NDCUPCWithDashes;
         }

         return this.View("ChangeTrackerDetails", objSearch);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="prodID"></param>
      /// <param name="NDCUPCWithDashes"></param>
      /// <param name="NDC"></param>
      /// <param name="UPC"></param>
      /// <param name="Strength"></param>
      /// <param name="Description"></param>
      /// <param name="ProfileTypeName"></param>
      /// <param name="ManufactureRepackagerName"></param>
      /// <param name="PageIdentification"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.UserRoleAdmin)]
      public ActionResult ChangeTrackerDetails2(long prodID, string NDCUPCWithDashes, long NDC, long UPC, string Strength, string Description, string ProfileTypeName, string ManufactureRepackagerName, string PageIdentification)
      {
         var objSearch = new ChangeTrackerSearch
         {
            ID = prodID,
            Strength = Strength,
            ProfileTypeName = ProfileTypeName,
            ManufactureRepackagerName = ManufactureRepackagerName,
            Description = Description,
            PageIdentification = PageIdentification
         };

         if (string.IsNullOrEmpty(PageIdentification))
         {
            objSearch.NDCUPCWithDashes = NDCUPCWithDashes;
            objSearch.NDC = NDC;
            objSearch.UPC = UPC;
         }

         return this.View("ChangeTrackerDetails2", objSearch);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="prodID"></param>
      /// <param name="ProductID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.UserRoleAdmin)]
      public ActionResult GoToChangeTrackerPage(long prodID, long ProductID)
      {
         var objSearch = ProductDataAccess.GetProductNDCDescription(ProductID);

         objSearch.PageIdentification = Constants.Model_PolicyDataAccess_L;
         objSearch.ID = prodID;

         return this.View("ChangeTrackerDetails", objSearch);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProduct"></param>
      ///****************************************************************************
      private void AssignRespectiveColumnToPopup(ref ProductsEntity objProduct)
      {
         objProduct.ControlListing = new DynamicControlEntity();
         objProduct.ColumnListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.ProductController_ProductSearchPanel, 0, false);
         objProduct.ControlListing.Controls = objProduct.ColumnListing.SubscribedColumn.Select(this.CreateDynamicControlBasedOnValue).ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///   CreateSearchPanel methods used to create search panel dynamically.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public PartialViewResult CreateSearchPanel()
      {
         var objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.ProductController_ProductSearchPanel, 0, false);

         this.ViewData["ControlListingForPopUp"] = objListing;

         var controlListing = new DynamicControlEntity
         {
            Controls = objListing.SubscribedColumn.Select(this.CreateDynamicControlBasedOnValue).ToList()
         };

         return controlListing.Controls.Count > 0
                     ? this.PartialView("_SearchPanel", controlListing)
                     : this.PartialView("_SearchPanel");
      }

      ///****************************************************************************
      /// <summary>
      ///   Get column list save by specific user and show into popup.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CreateColumnListingPopUp()
      {
         ColumnListEntity objListing = null;

         if (this.ViewData["ControlListingForPopUp"] == null)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.ProductController_ProductSearchPanel, 0, false);
         }

         return objListing != null
                     ? this.PartialView("_ColumnListPanel", objListing)
                     : this.PartialView("_ColumnListPanel");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult CreateDivestedPopup()
      {
         var objProduct = new ProductsML { lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Manufacturer).ToList() };
         var result = objProduct.lstManufacturer.Select(n => new SelectListItem
         {
            Text = n.Name,
            Value = n.ProfileCode.ToString()
         });

         return this.Json(result, JsonRequestBehavior.AllowGet);
      }

      static List<ControlViewModel> _objdsad;

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      //[HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult BindJQXGridLayoutForServerSidePaging(string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var objFilterManipulation = new FilterManipulation(_objdsad.GroupBy(u => u.AssociateTableName).Select(grp => grp.ToList()).ToList());

         var objProductEntity = new ProductsEntity();
         var objUserSearch = new UserSearch();
         this.GetCurrentUserData(ref objUserSearch);
         var total = 0;

         objProductEntity.lstManufactureProduct = ProductDataAccess.ListAllProductssforserver(new RequestGetProduct { UserID = this.GetUserID(), UserTypeName = this.GetUserType() },
                                                                                               objFilterManipulation.ProductsFilterObj,
                                                                                                objFilterManipulation.LotNumbersFilterObj,
                                                                                                 pagesize * pagenum,
                                                                                                  pagesize,
                                                                                                   this.Request.QueryString,
                                                                                                    "Product",
                                                                                                     sortdatafield,
                                                                                                      sortorder,
                                                                                                       out total);

         return this.Json(new { TotalRows = total, Rows = objProductEntity.lstManufactureProduct }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="lstobjdsad"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult GetColumnsList(List<ControlViewModel> lstobjdsad)
      {
         _objdsad = lstobjdsad;

         var objProductEntity = new ProductsEntity();
         var objListing = ManageLayoutConfig.GetColumnListingByLayout(Constants.ProductController_ProductListing, 0, true);

         objProductEntity.GridColumnListing = objListing;

         var objlstJqxGridColumn = (from m in objListing.SubscribedColumn
                                     orderby m.OrderNumber ascending
                                      select new Column
                                          {
                                             text = string.IsNullOrEmpty(m.DisplayText) ? m.ColumnName : m.DisplayText.Trim(),
                                             datafield = m.ColumnName,
                                             width = m.ColumnName == Constants.LotNumberController_CaseSize1 || m.ColumnName == Constants.LotNumberController_CaseSize2 || m.ColumnName == Constants.LotNumberController_CaseSize3 || m.ColumnName == Constants.LotNumberController_Strength || m.ColumnName == Constants.LotNumberController_UnitDose || m.ColumnName == Constants.LotNumberController_RXorOTC || m.ColumnName == Constants.LotNumberController_Refrigerated || m.ColumnName == Constants.LotNumberController_DivestedBYLot || m.ColumnName == Constants.LotNumberController_IsRepackager || m.ColumnName == Constants.LotNumberController_ControlNumber || m.ColumnName == Constants.LotNumberController_DosageCode || m.ColumnName == Constants.LotNumberController_UnitOfMeasure || m.ColumnName == Constants.LotNumberController_RecallID ? "90" : "130"
                                          }).ToList();

         return this.Json(objlstJqxGridColumn, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind the grid column list into grid popup for showing the details into
      ///   grid.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public PartialViewResult BindColumnListing()
      {
         return this.PartialView("_ColumnGridListPanel", ManageLayoutConfig.GetColumnListingByLayout(Constants.ProductController_ProductListing, 0, true));
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind dynamic control based on data type into index search page.
      /// </summary>
      /// <param name="details"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      private ControlViewModel CreateDynamicControlBasedOnValue(ColumnDetails details)
      {
         ControlViewModel obj;

         switch (details.ColumnType.ToLower())
         {
            // ...
            //=============================================================================
            case Constants.LotNumberController_bit:
               obj = new CheckBoxViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  Value = false,
                  AssociateTableName = details.TableName,
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

            // ...
            //=============================================================================
            case Constants.LotNumberController_datetime1:
               obj = new DateTimeViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  Value = null,
                  AssociateTableName = details.TableName,
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

            // ...
            //=============================================================================
            case Constants.LotNumberController_string:
            case Constants.LotNumberController_bigint:
            case Constants.LotNumberController_int:
            case Constants.LotNumberController_decimal:
            default:
               obj = new TextBoxViewModel
               {
                  Visible = true,
                  Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                  Name = details.ColumnName,
                  AssociateTableName = details.TableName,
                  Value = " ",
                  DataType = details.ColumnType,
                  OperatorType = details.FilterType
               };

               break;

            // ...
            //=============================================================================
            case Constants.LotNumberController_select:
               if (string.Compare(details.ColumnName, Constants.LotNumberController_RXorOTC, true) == 0)
               {
                  obj = new DropDownListViewModel
                  {
                     Visible = true,
                     Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                     Name = details.ColumnName,
                     AssociateTableName = details.TableName,

                     DataType = details.ColumnType,
                     OperatorType = details.FilterType,
                     Values = new SelectList(
                        new[]
                        {
                           new {Value = Constants.Model_ProfilesDataAccess_Zero, Text = Constants.ReportController_Select},
                           new {Value = Constants.ReportController_True, Text = Constants.ReportController_Rprescription},
                           new {Value = Constants.ReportController_False, Text = Constants.ReportController_OTC},
                        }, Constants.ReportController_Value, Constants.ReportController_Text, Constants.Model_ProfilesDataAccess_Zero
                        )
                  };
               }
               else
               {
                  obj = new DropDownListViewModel
                  {
                     Visible = true,
                     Label = string.IsNullOrEmpty(details.DisplayText) ? details.ColumnName : details.DisplayText,
                     Name = details.ColumnName,
                     AssociateTableName = details.TableName,
                     OperatorType = details.FilterType,
                     Values = this.BindRespectiveTable(details.MappedTable)
                  };
               }

               break;
         }

         return obj;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public ActionResult ProductDetails()
      {
         var objPrdML = new ProductsML();

         try
         {
            // Checking Viewbag null status
            if (this.ViewBag.DosageLIst == null)
            {
               objPrdML.lstProductDosageType = ProductDataAccess.GetDosageList();

               this.ViewBag.DosageLIst = objPrdML.lstProductDosageType.Select(n => new SelectListItem
               {
                  Text = n.Description,
                  Value = n.Code
               });
            }

            // Checking Viewbag null status for wholesaler number
            if (this.ViewBag.WholesalerNumberBag == null)
            {
               objPrdML.lstWholesaler = ProductDataAccess.GetWholesalerNumber();

               this.ViewBag.WholesalerNumberBag = objPrdML.lstWholesaler.Select(n => new SelectListItem
               {
                  Text = n.Name,
                  Value = n.WholesalerAccountNumber
               });
            }

            if (this.ViewBag.ManufacturerName == null)
            {
               objPrdML.lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.LotNumberController_ManufacturerRepackager).ToList();

               this.ViewBag.ManufacturerName = objPrdML.lstManufacturer.Select(n => new SelectListItem
               {
                  Text = n.Name + Constants.ProductController_BREKET_LEFTWITHSPACE + n.ProfileCode.ToString() + Constants.ProductController_BREKET_RIGHTWITHSPACE,
                  Value = n.ProfileCode.ToString()
               });
            }

            if (this.ViewBag.WholesalerName == null)
            {
               objPrdML.lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Wholesaler).ToList();

               this.ViewBag.WholesalerName = objPrdML.lstManufacturer.Select(n => new SelectListItem
               {
                  Text = n.Name,
                  Value = n.ProfileCode.ToString()
               });
            }

            return this.View("ProductDetails", objPrdML);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.View("ProductDetails", objPrdML);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProductML"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult ProductDetails([Bind(Exclude = Constants.ProductController_ProductID)] ProductsML objProductML)
      {
         try
         {
            if (this.ModelState.IsValid)
            {
               var _prodID = 0L;

               if (ProductDataAccess.SaveProduct(objProductML, this.User.Identity.Name, out _prodID))
               {
                  this.ViewBag.message = Resources.Product.Save_Successfully;

                  return this.RedirectToAction("EditProduct", new { prodId = _prodID, mode = Constants.ProductController_Edit });
               }

               this.ViewBag.message = Resources.Product.Save_Fails;
            }
            return this.RedirectToAction("ProductDetails");
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.RedirectToAction("ProductDetails");
      }

      ///****************************************************************************
      /// <summary>
      ///   Access WebAPI for Delete Product that mean deactivate product.
      /// </summary>
      /// <param name="prodId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.UserRoleAdmin)]
      public ActionResult DeleteProduct(long prodId)
      {
         try
         {
            var objclsResponse = ProductDataAccess.DeleteProduct(prodId, this.User.Identity.Name);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = PolicyResource.OK_Message, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = PolicyResource.VersionChanged, VersionChanged = true}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = PolicyResource.Fails, Message = objclsResponse.Message, VersionChanged = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.Json(Resources.Product.Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the Edit Page.
      /// </summary>
      /// <param name="prodId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult EditProduct(long prodId)
      {
         try
         {
            if (this.Request.QueryString.Count > 0 && this.Request.QueryString[Constants.ProductController_Mode] == Constants.ProductController_Edit)
            {
               var productEdiResult = ProductDataAccess.GetProductByID(prodId);

               // Checking Viewbag null status for wholesaler number
               if (this.ViewBag.DosageLIst == null)
               {
                  productEdiResult.lstProductDosageType = ProductDataAccess.GetDosageList();

                  // Assigning data into viewbag for drop-down binding in productDetails View
                  this.ViewBag.DosageLIst = productEdiResult.lstProductDosageType.Select(n => new SelectListItem
                  {
                     Text = n.Description,
                     Value = n.Code
                  });
               }

               var objLotNumberViewModel = new LotNumberViewModel();
               var objLotNumbersDataEntity = new LotNumbersDataEntity
               {
                  ProductID = productEdiResult.ProductID
               };

               objLotNumberViewModel.RepackagerProfileList = this.GetRepakagerProfileCode().Select(n => new SelectListItem
               {
                  Text = n.Name.ToString(),
                  Value = n.ProfileCode.ToString()
               });

               objLotNumberViewModel.ProductProfileList = this.GetProductIDWithName().Select(n => new SelectListItem
               {
                  Text = n.ProductName.ToString(),
                  Value = Convert.ToString(n.ProductID)
               });

               objLotNumberViewModel.ProductRecallList = this.GetReacallID(productEdiResult.ProductID).Select(n => new SelectListItem
               {
                  Text = n.ProductName.ToString(),
                  Value = Convert.ToString(n.RecallID)
               });

               objLotNumberViewModel.IsPopupCreateLotNumber = true;
               objLotNumberViewModel.LotNumbersData = objLotNumbersDataEntity;
               productEdiResult.objLotNumbersDataEntity = objLotNumberViewModel;

               if (this.ViewBag.WholesalerName == null)
               {
                  var lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Wholesaler).ToList();

                  productEdiResult.lstManufacturer = lstManufacturer;

                  this.ViewBag.WholesalerName = productEdiResult.lstManufacturer.Select(n => new SelectListItem
                  {
                     Text = n.Name,
                     Value = n.ProfileCode.ToString()
                  });
               }

               if (this.ViewBag.ManufacturerName == null)
               {
                  var lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.LotNumberController_ManufacturerRepackager);

                  productEdiResult.lstManufacturer = lstManufacturer.ToList();

                  this.ViewBag.ManufacturerName = productEdiResult.lstManufacturer.Select(n => new SelectListItem
                  {
                     Text = n.Name + Constants.ProductController_BREKET_LEFTWITHSPACE + n.ProfileCode.ToString() + Constants.ProductController_BREKET_RIGHTWITHSPACE,
                     Value = n.ProfileCode.ToString()
                  });
               }

               var lstAzureConfigDetails = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.ProductController_AzureStorage);
               productEdiResult.LocalMedia = Convert.ToBoolean(lstAzureConfigDetails.Where(x => x.keyName.Equals(Constants.ProductController_LocalMedia)).FirstOrDefault().Value);

               if (!productEdiResult.LocalMedia)
               {
                  this.DownloadReturnMediaImages(productEdiResult);
               }

               this.ViewBag.MyStates = JsonConvert.SerializeObject(productEdiResult);
               return this.View("ProductDetails", productEdiResult);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.View("ProductDetails");
      }

      ///****************************************************************************
      /// <summary>
      ///   Pass model data to WebAPI for Addition on product table.
      /// </summary>
      /// <param name="profileID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult LotForDivested(int profileID)
      {
         return this.PartialView("_ProductLotDetails");
      }

      ///****************************************************************************
      /// <summary>
      ///   Get manufacture profile.
      /// </summary>
      /// <param name="profileID"></param>
      ///****************************************************************************
      public void GetSelectedManufacture(int profileID)
      {
         try
         {
            QoskSessionHelper.SetSessionData(Constants.ProductController_SelectedProfileID, profileID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the View data using JqueryAjax and Assign data to ProductMLs Model
      ///   and Pass model to WebAPI.
      /// </summary>
      /// <param name="objPrddescription"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public JsonResult UpdateProductDescriptionSection(ProductsML objPrddescription)
      {
         try
         {
            var objclsResponse = ProductDataAccess.UpdateProduct(objPrddescription, this.User.Identity.Name);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new {Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new {Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.Json(Resources.Product.Update_Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Call MVC business logic methods to Save Custom search details into
      ///   database using WebAPI.
      /// </summary>
      /// <param name="SearchList"></param>
      /// <param name="LayoutId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public JsonResult SaveCustomSearchDetailsByUserName(string SearchList, int LayoutId)
      {
         var objList = new ColumnListEntity { SubscribedColumn = new List<ColumnDetails>() };
         var sColumnList = SearchList.Split(',');

         if (sColumnList != null)
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
            var UserReturnData = objReturnLoginUserEntity.UserReturnData;
            var UserID = UserReturnData.UserID ?? 0L;

            foreach (var objColumn in sColumnList)
            {
               if (!string.IsNullOrEmpty(objColumn))
               {
                  var objColumnDetails = new ColumnDetails
                  {
                     ColumnId = Convert.ToInt32(objColumn),
                     LayoutId = LayoutId,
                     UserId = UserID
                  };

                  // For Testing Only
                  objList.SubscribedColumn.Add(objColumnDetails);
               }
            }

            if (objList.SubscribedColumn != null)
            {
               return this.Json(ManageLayoutConfig.SaveSubscribedColumn(objList, UserID, LayoutId)
                        ? Resources.Product.Custom_Save_Success
                        : Resources.Product.UnDefined);
            }
         }

         return this.Json(Resources.Product.UnDefined);
      }

      ///****************************************************************************
      /// <summary>
      ///   Add image Details into database.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult AddProductImageUsingAjax()
      {
         var flag = false;

         try
         {
            var newPath = string.Empty;
            var fileName = string.Empty;
            var contentFileName = string.Empty;
            var localMedia = false;
            var file = this.Request.Files[Constants.ProductController_Contentfile];
            var containerfile = this.Request.Files[Constants.ProductController_Productimgfile];

            if (containerfile != null)
            {
               fileName = Path.GetFileName(containerfile.FileName);
            }

            if (file != null)
            {
               contentFileName = Path.GetFileName(file.FileName);
            }

            var objProduct = new ProductsML
            {
               ProfileCode = Convert.ToInt32(this.Request.Form[Constants.ProductController_ProfileCode]),
               NDCUPCWithDashesforImageTab = this.Request.Form[Constants.ProductController_NDCUPCWithDashes],
               DescriptionforImageTab = this.Request.Form[Constants.ProductController_Description],
               PackagingOrContainerType = this.Request.Form[Constants.ProductController_ContainerType],
               Image = fileName,
               ContentImage = contentFileName,
               CreatedBy = this.User.Identity.Name
            };

            var resultID = ProductDataAccess.InsertProductImage(objProduct);
            var lstAzureConfigDetails = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.ProductController_AzureStorage);

            if (lstAzureConfigDetails.Count > 0)
            {
               localMedia = Convert.ToBoolean(lstAzureConfigDetails.Where(x => x.keyName.Equals(Constants.ProductController_LocalMedia)).FirstOrDefault().Value);
            }

            if (localMedia)
            {
               if (resultID.ProductImageDetailID > 0)
               {
                  switch (objProduct.PackagingOrContainerType)
                  {
                     // ...
                     //=============================================================================
                     case Constants.ProductController_Original:
                        newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_OriginalPtah), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + fileName);
                        break;

                     // ...
                     //=============================================================================
                     case Constants.ProductController_Repackager:
                        newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_RepackagerPath), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + fileName);
                        break;
                  }
               }

               if (!string.IsNullOrWhiteSpace(contentFileName))
               {
                  file.SaveAs(Path.Combine(this.Server.MapPath(Constants.ProductController_ContentPath), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + contentFileName));
                  flag = true;
               }

               if (!string.IsNullOrWhiteSpace(newPath))
               {
                  containerfile.SaveAs(newPath);
                  flag = true;
               }

               return this.Json(flag
                                  ? Resources.Product.Save_Successfully
                                  : Resources.Product.Save_Fails);
            }

            return this.Json(this.UploadImage(resultID, objProduct, containerfile, file, lstAzureConfigDetails)
                                 ? Resources.Product.Save_Successfully
                                 : Resources.Product.Save_Fails);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(Resources.Product.Save_Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save the product image when user is in editing mode.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult AddNewProductPhotoUsingAjax()
      {
         var flag = false;

         try
         {
            var newPath = string.Empty;
            var fileName = string.Empty;
            var contentFileName = string.Empty;
            var localMedia = false;
            var file = this.Request.Files[Constants.ProductController_Contentfile];
            var containerfile = this.Request.Files[Constants.ProductController_Productimgfile];

            if (containerfile == null && file == null)
            {
               return this.Json("Please browse file and then click on save");
            }

            if (containerfile != null)
            {
               fileName = Path.GetFileName(containerfile.FileName);
            }

            if (file != null)
            {
               contentFileName = Path.GetFileName(file.FileName);
            }

            var objProduct = new ProductsML
            {
               NDCUPCWithDashesforImageTab = this.Request.Form[Constants.ProductController_NDCUPCWithDashes],
               DescriptionforImageTab = this.Request.Form[Constants.ProductController_Description],
               PackagingOrContainerType = this.Request.Form[Constants.ProductController_ContainerType],
               ProductID = Convert.ToInt32(this.Request.Form[Constants.ProductController_ProductID]),
               ProfileCode = Convert.ToInt32(this.Request.Form[Constants.ProductController_ProfileCode]),
               Image = fileName,
               ContentImage = contentFileName,
               CreatedBy = this.User.Identity.Name
            };

            var resultID = ProductDataAccess.InsertProductImage(objProduct);
            var lstAzureConfigDetails = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.ProductController_AzureStorage);

            if (lstAzureConfigDetails.Count > 0)
            {
               localMedia = Convert.ToBoolean(lstAzureConfigDetails.Where(x => x.keyName.Equals(Constants.ProductController_LocalMedia)).FirstOrDefault().Value);
            }

            if (localMedia)
            {
               if (resultID.ProductImageDetailID > 0)
               {
                  switch (objProduct.PackagingOrContainerType)
                  {
                     // ...
                     //=============================================================================
                     case Constants.ProductController_Original:
                        newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_OriginalPtah), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + fileName);
                        break;

                     // ...
                     //=============================================================================
                     case Constants.ProductController_Repackager:
                        newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_RepackagerPath), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + fileName);
                        break;
                  }
               }

               if (!string.IsNullOrWhiteSpace(contentFileName))
               {
                  file.SaveAs(Path.Combine(this.Server.MapPath(Constants.ProductController_ContentPath), +resultID.ProductImageDetailID + Constants.ProductController_Underscore + contentFileName));
                  flag = true;
               }

               if (!string.IsNullOrWhiteSpace(newPath))
               {
                  containerfile.SaveAs(newPath);
                  flag = true;
               }

               if (flag)
                  return this.Json(Resources.Product.Save_Successfully);
            }
            else
            {
               return this.Json(this.UploadImage(resultID, objProduct, containerfile, file, lstAzureConfigDetails)
                                       ? Resources.Product.Save_Successfully
                                       : Resources.Product.Save_Fails);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(Resources.Product.Save_Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="resultID"></param>
      /// <param name="objProduct"></param>
      /// <param name="containerfile"></param>
      /// <param name="contentfile"></param>
      /// <param name="lstAmazonPassword"></param>
      /// <returns></returns>
      ///****************************************************************************
      private bool UploadImage(ImageDetails resultID, ProductsML objProduct, System.Web.HttpPostedFileBase containerfile, System.Web.HttpPostedFileBase contentfile, List<AppConfigurationSetting> lstAmazonPassword)
      {
         var account = string.Empty;
         var accessKey = string.Empty;
         var mediaContainer = string.Empty;
         var newPath = string.Empty;
         var flag = false;

         // ...
         ///////////////////////////////////////////////////////////////////////////////
         if (lstAmazonPassword.Count > 0)
         {
            account = lstAmazonPassword.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_Account))?.Value;
            accessKey = lstAmazonPassword.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AccessKey))?.Value;
            mediaContainer = lstAmazonPassword.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AzureMediaName))?.Value;
         }

         // ...
         ///////////////////////////////////////////////////////////////////////////////
         if (resultID.ProductImageDetailID > 0)
         {
            switch (objProduct.PackagingOrContainerType)
            {
               // ...
               //=============================================================================
               case Constants.ProductController_Original:
                  newPath = Constants.productOriginal + resultID.ProductImageDetailID + Constants.ProductController_Underscore + objProduct.Image;
                  break;

               // ...
               //=============================================================================
               case Constants.ProductController_Repackager:
                  newPath = Constants.productRepackager + resultID.ProductImageDetailID + Constants.ProductController_Underscore + objProduct.Image;
                  break;
            }
         }

         // ...
         ///////////////////////////////////////////////////////////////////////////////
         using (var objAzure = new AzureStorageCommunication(account, accessKey))
         {
            if (!string.IsNullOrWhiteSpace(objProduct.ContentImage))
            {
               objAzure.UploadFile(mediaContainer, contentfile, Constants.productContent + resultID.ProductImageDetailID + "_" + objProduct.ContentImage);
               flag = true;
            }

            if (!string.IsNullOrWhiteSpace(newPath))
            {
               objAzure.UploadFile(mediaContainer, containerfile, newPath);
               flag = true;
            }
         }

         return flag;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProductML"></param>
      ///****************************************************************************
      public void DownloadReturnMediaImages(ProductsML objProductML)
      {
         foreach (var item in objProductML.lstProductImage)
         {
            switch (objProductML.PackagingOrContainerType)
            {
               // ...
               //=============================================================================
               case Constants.ProductController_Original:
                  var originalImage = Constants.productOriginal + item.Image;
                  item.AzureContainer = Utility.Common.BlobResource.GetSASUrl(Constants.ProductController_Qoskkiosksync, Constants.ProductController_Returnmedia, originalImage, Constants.ProductController_Read, 75);
                  break;

               // ...
               //=============================================================================
               case Constants.ProductController_Repackager:
                  var repackagerImage = Constants.productRepackager + item.Image;
                  item.AzureContainer = Utility.Common.BlobResource.GetSASUrl(Constants.ProductController_Qoskkiosksync, Constants.ProductController_Returnmedia, repackagerImage, Constants.ProductController_Read, 75);
                  break;
            }
            item.AzureContent = Utility.Common.BlobResource.GetSASUrl(Constants.ProductController_Qoskkiosksync, Constants.ProductController_Returnmedia, Constants.productContent + item.ContentImage, Constants.ProductController_Read, 75);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult UpdateIndividualContentImage()
      {
         var flag = false;
         var contentNewPath = string.Empty;
         var newPath = string.Empty;
         var fileName = string.Empty;
         var contentFileName = string.Empty;
         var localMedia = false;

         try
         {
            var file = this.Request.Files[Constants.ProductController_Contentfile];
            var containerfile = this.Request.Files[Constants.ProductController_productcontainerimgfile];

            if (containerfile == null && file == null)
            {
               return this.Json(Resources.Product.BrowseAlertMessage);
            }

            if (containerfile != null)
            {
               fileName = Path.GetFileName(containerfile.FileName);
            }

            if (file != null)
            {
               contentFileName = Path.GetFileName(file.FileName);
            }

            var objProduct = new ProductsML
            {
               PackagingOrContainerType = this.Request.Form[Constants.ProductController_ContainerType],
               HiddenProductImageDetailID = Convert.ToInt64(this.Request.Form[Constants.ProductController_HiddenProductImageDetailID]),
               ProductID = Convert.ToInt32(this.Request.Form[Constants.ProductController_ProductID]),
               Image = fileName,
               ContentImage = contentFileName,
               Version = Convert.ToInt64(this.Request.Form[Constants.ProductController_Version]),
               ModifiedBy = this.User.Identity.Name
            };

            var objclsResponse = ProductDataAccess.UpdateProductImage(objProduct);
            var lstAzureConfigDetails = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.ProductController_AzureStorage);

            if (lstAzureConfigDetails.Count > 0)
            {
               localMedia = Convert.ToBoolean(lstAzureConfigDetails.Where(x => x.keyName.Equals(Constants.ProductController_LocalMedia)).FirstOrDefault()?.Value);
            }

            if (localMedia)
            {
               switch (objclsResponse.Status)
               {
                  // ...
                  //=============================================================================
                  case Status.OK:
                  {
                     switch (objProduct.PackagingOrContainerType)
                     {
                        // ...
                        //=============================================================================
                        case Constants.ProductController_Original:
                           if (!string.IsNullOrWhiteSpace(fileName))
                           {
                              newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_OriginalPtah), +objProduct.HiddenProductImageDetailID + Constants.ProductController_Underscore + fileName);
                           }

                           break;

                        // ...
                        //=============================================================================
                        case Constants.ProductController_Repackager:
                           if (!string.IsNullOrWhiteSpace(fileName))
                           {
                              newPath = Path.Combine(this.Server.MapPath(Constants.ProductController_RepackagerPath), +objProduct.HiddenProductImageDetailID + Constants.ProductController_Underscore + fileName);
                           }

                           break;
                     }

                     if (!string.IsNullOrWhiteSpace(contentFileName))
                     {
                        contentNewPath = Path.Combine(this.Server.MapPath(Constants.ProductController_ContentPath), +objProduct.HiddenProductImageDetailID + Constants.ProductController_Underscore + contentFileName);
                     }

                     if (!string.IsNullOrWhiteSpace(contentNewPath))
                     {
                        file.SaveAs(contentNewPath);
                        flag = true;
                     }

                     if (!string.IsNullOrWhiteSpace(newPath))
                     {
                        containerfile.SaveAs(newPath);
                        flag = true;
                     }

                     if (flag)
                     {
                        return this.Json(new {Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion}, JsonRequestBehavior.AllowGet);
                     }

                     break;
                  }

                  // ...
                  //=============================================================================
                  case Status.VersionChange:
                     return this.Json(new {Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse}, JsonRequestBehavior.AllowGet);

                  // ...
                  //=============================================================================
                  case Status.Deleted:
                     return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false}, JsonRequestBehavior.AllowGet);

                  // ...
                  //=============================================================================
                  case Status.Fail:
                     return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);

               }
            }
            else
            {
               var azureStoragelocation = string.Empty;
               var oldContentpath = string.Empty;
               var account = string.Empty;
               var accessKey = string.Empty;
               var mediaContainer = string.Empty;
               var oldpath = string.Empty;
               var objOldProductImageDetail = (ProductImage)objclsResponse.response;

               if (lstAzureConfigDetails.Count > 0)
               {
                  account = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_Account))?.Value;
                  accessKey = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AccessKey))?.Value;
                  mediaContainer = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AzureMediaName))?.Value;
               }

               switch (objclsResponse.Status)
               {
                  // ...
                  //=============================================================================
                  case Status.OK:
                  {
                     switch (objProduct.PackagingOrContainerType)
                     {
                        // ...
                        //=============================================================================
                        case Constants.ProductController_Original:
                           if (!string.IsNullOrWhiteSpace(fileName))
                           {
                              newPath = Constants.productOriginal + objProduct.HiddenProductImageDetailID + Constants.ProductController_Underscore + objProduct.Image;
                              oldpath = Constants.productOriginal + objOldProductImageDetail.FileName;
                           }

                           break;

                        // ...
                        //=============================================================================
                        case Constants.ProductController_Repackager:
                           if (!string.IsNullOrWhiteSpace(fileName))
                           {
                              newPath = Constants.productRepackager + objProduct.HiddenProductImageDetailID + Constants.ProductController_Underscore + objProduct.Image;
                              oldpath = Constants.productRepackager + objOldProductImageDetail.FileName;
                           }

                           break;
                     }

                     if (!string.IsNullOrWhiteSpace(objProduct.ContentImage))
                     {
                        var objAzureStorageCommunication = new AzureStorageCommunication(account, accessKey);

                        azureStoragelocation = Constants.productContent + objProduct.HiddenProductImageDetailID + "_" + objProduct.ContentImage;
                        // TODO: Fix commented code
                        //oldContentpath = Constants.productContent + objOldProductImageDetail.ContentFileName;

                        if (objAzureStorageCommunication.DeleteImagefromAzureblob(mediaContainer, oldContentpath))
                        {
                           if (objAzureStorageCommunication.UploadFile(mediaContainer, file, azureStoragelocation))
                           {
                              flag = true;
                           }
                        }
                     }

                     if (!string.IsNullOrWhiteSpace(newPath))
                     {
                        var objAzureStorageCommunication = new AzureStorageCommunication(account, accessKey);

                        objAzureStorageCommunication.DeleteImagefromAzureblob(mediaContainer, oldpath);
                        objAzureStorageCommunication.UploadFile(mediaContainer, containerfile, newPath);

                        flag = true;
                     }

                     if (flag)
                     {
                        return this.Json(new {Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion}, JsonRequestBehavior.AllowGet);
                     }

                     break;
                  }

                  // ...
                  //=============================================================================
                  case Status.VersionChange:
                     return this.Json(new {Result = PolicyResource.VersionChanged, VersionChanged = true}, JsonRequestBehavior.AllowGet);

                  // ...
                  //=============================================================================
                  case Status.Deleted:
                     return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false}, JsonRequestBehavior.AllowGet);

                  // ...
                  //=============================================================================
                  case Status.Fail:
                     return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);
               }
            }

         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(new {Result = PolicyResource.Update_Fail, VersionChanged = false}, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ProductImageDetailID"></param>
      /// <param name="OriginalRpk"></param>
      /// <param name="Content"></param>
      /// <param name="OriginalRpkImag"></param>
      /// <param name="ContentImg"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult DeleteProductPhoto(long ProductImageDetailID, string OriginalRpk, string Content, string OriginalRpkImag, string ContentImg)
      {
         var deleteStatus = false;

         if (ProductDataAccess.DeleteProductImage(ProductImageDetailID, OriginalRpk, Content))
         {
            var lstAzureConfigDetails = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.ProductController_AzureStorage);

            if (lstAzureConfigDetails.Count > 0)
            {
               var localMedia = Convert.ToBoolean(lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_LocalMedia))?.Value);

               if (!localMedia)
               {
                  var account = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_Account))?.Value;
                  var accessKey = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AccessKey))?.Value;
                  var mediaContainer = lstAzureConfigDetails.FirstOrDefault(x => x.keyName.Equals(Constants.ProductController_AzureMediaName))?.Value;
                  var objAzureStorageCommunication = new AzureStorageCommunication(account, accessKey);

                  switch (OriginalRpk)
                  {
                     // ...
                     //=============================================================================
                     case Constants.ProductController_Original:
                        deleteStatus = objAzureStorageCommunication.DeleteImagefromAzureblob(mediaContainer, Constants.productOriginal + OriginalRpkImag);
                        break;

                     // ...
                     //=============================================================================
                     case Constants.ProductController_Repackager:
                        deleteStatus = objAzureStorageCommunication.DeleteImagefromAzureblob(mediaContainer, Constants.productRepackager + OriginalRpkImag);
                        break;
                  }

                  if (!string.IsNullOrWhiteSpace(Content))
                  {
                     deleteStatus = objAzureStorageCommunication.DeleteImagefromAzureblob(mediaContainer, Constants.productContent + ContentImg);
                  }

                  return this.Json(deleteStatus
                                       ? Resources.Product.Product_Image_Deleted
                                       : Resources.Product.Delete_Fails, JsonRequestBehavior.AllowGet);
               }

               return this.Json(Resources.Product.Product_Image_Deleted, JsonRequestBehavior.AllowGet);
            }
         }
         else
         {
            return this.Json(Resources.Product.Product_Image_Deleted, JsonRequestBehavior.AllowGet);
         }

         return this.Json(Resources.Product.Delete_Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private long GetUserID()
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         return objReturnLoginUserEntity.UserReturnData.UserID ?? 0L;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType()
      {
         try
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

            return objReturnLoginUserEntity.UserReturnData.UserTypeCode == "ADMN"
                        ? UserTypes.ADMIN.ToString()
                        : UserTypes.OTHERS.ToString();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get divested lot details based on product ID.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      private DiverseLotNumbersDetails GetDiverseLotNumber(long productID)
      {
         try
         {
            return LotNumbersDataAccess.GetDivestedLotNumber(productID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public List<ProductIDWithName> GetProductIDWithName()
      {
         try
         {
            return LotNumbersDataAccess.GetProductIDWithName();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public List<ProfilesName> GetRepakagerProfileCode()
      {
         try
         {
            return LotNumbersDataAccess.GetReapackagerProfileName();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public List<RecallDDLWithName> GetReacallID(long productID)
      {
         try
         {
            return LotNumbersDataAccess.GetRecallID(productID);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch"></param>
      ///****************************************************************************
      private void GetCurrentUserData(ref UserSearch objUserSearch)
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         objUserSearch.UserID = objReturnLoginUserEntity.UserReturnData.UserID ?? 0;
         objUserSearch.UserTypeCode = objReturnLoginUserEntity.UserReturnData.UserTypeCode;
         objUserSearch.ProfileType = objReturnLoginUserEntity.UserReturnData.UserProfileType;
         objUserSearch.ProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
         objUserSearch.UserTypeName = this.GetUserType(objReturnLoginUserEntity.UserReturnData.UserTypeCode);
         objUserSearch.RollupProfileCode = objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userTypeCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string GetUserType(string userTypeCode)
      {
         return userTypeCode == "ADMN"
                     ? Constants.HomeController_ADMIN
                     : Constants.HomeController_OTHERS;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create Lot Number Post methods.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult CreateLotNumber(long productID)
      {
         try
         {
            var model = new LotNumberViewModel();

            LotNumbersDataAccess.SaveLotNumber(model.LotNumbersData);

            return this.PartialView("_CreateLotNumber", model);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Called when we have to create any new lot for any product.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Create_LotNumber(string productID)
      {
         try
         {
            var productEdiResult = ProductDataAccess.GetProductByID(Convert.ToInt64(productID));

            productEdiResult.lstManufacturer = new List<ProfilesName>
            {
               new ProfilesName()
               {
                  ProfileCode = productEdiResult.ProfileCode,
                  Name = productEdiResult.ManufactureRepackagerName
               }
            };

            if (productEdiResult.DivestedByLot == true)
            {
               productEdiResult.lstManufacturer.Add(new ProfilesName()
               {
                  ProfileCode = productEdiResult.DiverseManufacturerProfileCode,
                  Name = productEdiResult.DiverseManufacturerName
               });
            }

            var objLotNumberViewModel = new LotNumberViewModel
            {
               ProductDetails = productEdiResult,
               RepackagerProfileList = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Repackager).Select(n => new SelectListItem
               {
                  Text = n.Name.ToString(),
                  Value = n.ProfileCode.ToString()
               })
            };

            return this.PartialView("_CreateLotNumber", objLotNumberViewModel);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Edit_LotNumber(string productID)
      {
         try
         {
            var objLotNumberViewModel = new LotNumberViewModel();
            var productEdiResult = ProductDataAccess.GetProductByID(Convert.ToInt64(productID));

            productEdiResult.lstManufacturer = new List<ProfilesName>
            {
               new ProfilesName()
               {
                  ProfileCode = productEdiResult.ProfileCode,
                  Name = productEdiResult.ManufactureRepackagerName
               }
            };

            if (productEdiResult.DivestedByLot == true)
            {
               productEdiResult.lstManufacturer.Add(new ProfilesName()
               {
                  ProfileCode = productEdiResult.DiverseManufacturerProfileCode,
                  Name = productEdiResult.DiverseManufacturerName
               });
            }

            objLotNumberViewModel.ProductDetails = productEdiResult;
            objLotNumberViewModel.RepackagerProfileList = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Repackager).Select(n => new SelectListItem
            {
               Text = n.Name.ToString(),
               Value = n.ProfileCode.ToString()
            });

            objLotNumberViewModel.IsPopupCreateLotNumber = true;

            return this.PartialView("_EditLotNumber", objLotNumberViewModel);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.RedirectToAction("Index");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check NDC With Dashes exist or not in product table.
      /// </summary>
      /// <param name="NDCUPCWithDashes"></param>
      /// <param name="PrdID"></param>
      /// <param name="inputType"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult CheckExistanceNDCUPCWithDashes(string NDCUPCWithDashes, long PrdID, string inputType)
      {
         try
         {
            if (!string.IsNullOrWhiteSpace(NDCUPCWithDashes) && !string.IsNullOrEmpty(inputType))
            {
               var profilecode = 0;
               var description = ProductDataAccess.CheckNDCUPCWithDashesExist(NDCUPCWithDashes, PrdID, Convert.ToInt32(inputType), out profilecode);

               if (!string.IsNullOrEmpty(description))
               {
                  var result = new { data = Resources.Product.OK_message, data2 = description, data3 = profilecode };
                  return this.Json(result, JsonRequestBehavior.AllowGet);
               }
            }
            else if (!string.IsNullOrWhiteSpace(NDCUPCWithDashes) && NDCUPCWithDashes.Contains("-"))
            {
               var profilecode = 0;
               var description = ProductDataAccess.CheckNDCUPCWithDashesExist(NDCUPCWithDashes, PrdID, out profilecode);

               if (!string.IsNullOrEmpty(description))
               {
                  var result = new {data = Resources.Product.OK_message, data2 = description, data3 = profilecode};
                  return this.Json(result, JsonRequestBehavior.AllowGet);
               }
            }
            else if (Convert.ToInt64(NDCUPCWithDashes) > 0)
            {
               var description = ProductDataAccess.CheckNDCExist(Convert.ToInt64(NDCUPCWithDashes), PrdID);

               if (!string.IsNullOrEmpty(description))
               {
                  var result = new {data = Resources.Product.OK_message, data2 = description};
                  return this.Json(result, JsonRequestBehavior.AllowGet);
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(new {data = Resources.Product.Fails}, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="MappedTable"></param>
      /// <returns></returns>
      ///****************************************************************************
      private SelectList BindRespectiveTable(string MappedTable)
      {
         SelectList objSelectlst = null;

         switch (MappedTable)
         {
            case Constants.ProductController_Profiles:
            {
               var lstProfile = PolicyDataAccess.GetProfileListByType(Constants.LotNumberController_ManufacturerRepackager);

               lstProfile.Insert(0, (new ProfilesName
               {
                  ProfileCode = 0,
                  Name = Constants.ProductController_Select
               }));

               var result = (from m in lstProfile
                             select new
                             {
                                value = m.ProfileCode,
                                text = m.Name
                             }).AsEnumerable();

               objSelectlst = new SelectList(result, Constants.ProductController_Value, Constants.ProductController_Text, result);
               break;
            }
         }

         return objSelectlst;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the change details based on selected table,column and from
      ///   and to date.
      /// </summary>
      /// <param name="ObjChangeTackerSearch"></param>
      /// <param name="FromRange"></param>
      /// <param name="ToRange"></param>
      /// <param name="ProdID"></param>
      /// <param name="TableNM"></param>
      /// <param name="ColumnNM"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult GetChangeProductDetails(ChangeTrackerSearch ObjChangeTackerSearch, DateTime? FromRange, DateTime? ToRange, long ProdID, string TableNM, string ColumnNM)
      {
         var objGridModel = new RootObject();

         try
         {
            // Call data access methods for getting change details
            ObjChangeTackerSearch = ProductDataAccess.GetChangeTrackerDetails(FromRange, ToRange, ProdID, TableNM, ColumnNM);
            objGridModel.changeTrack = ObjChangeTackerSearch.lstChangeTrackentity;

            return this.Json(objGridModel, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return this.Json(objGridModel, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the change details based on selected table, column and from
      ///   and to date.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="version"></param>
      /// <param name="pageType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult GetChangeTrackerDetails2(long productId, long version, string pageType, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var total = 0;
         ChangeTrackerSearch objChangeTrackerSearch = null;

         try
         {
            // Call data access methods for getting change details
            objChangeTrackerSearch = ProductDataAccess.GetChangeTrackerDetails2(productId.ToString(), version, pageType);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
         var resultNEw = new
         {
            TotalRows = total,
            Rows = objChangeTrackerSearch.lstChangeTrackentity
         };

         return this.Json(resultNEw, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="FromRange"></param>
      /// <param name="ToRange"></param>
      /// <param name="ColumnNM"></param>
      /// <param name="UserNM"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AllUser)]
      public JsonResult BindProductAudit(long productId, string FromRange, string ToRange, string ColumnNM, string UserNM, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         DateTime fromDateTime = new DateTime(), toDateTime = new DateTime();

         if (string.IsNullOrEmpty(FromRange))
         {
            fromDateTime = DateTime.MinValue;
         }

         if (string.IsNullOrEmpty(ToRange))
         {
            toDateTime = DateTime.UtcNow;
         }

         var skip = pagesize * pagenum;
         var take = pagesize;
         var objProductEntity = new ProductsEntity();
         var total = 0;

         objProductEntity.lstManufactureProduct = ProductDataAccess.BindProductAudit(productId, fromDateTime, toDateTime, ColumnNM, UserNM, skip, take, sortdatafield, sortorder, out total).ToList();

         var orders = objProductEntity.lstManufactureProduct;
         var resultNEw = new
         {
            TotalRows = total,
            Rows = orders
         };

         return this.Json(resultNEw, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all the column list from database base on selected table from
      ///   change tracker details.
      /// </summary>
      /// <param name="TableName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult BindColumnListBaedonSelectedtable(string TableName)
      {
         var json = new JsonResult();

         try
         {
            // Call the data access methods for getting column list.
            var lstColumns = CommonDataAccessLayer.GetColumnListOftable(TableName);

            // Set the json behavior for get request.
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            // Assign column details to json data
            json.Data = lstColumns;
            return json;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all the column list from database base on selected table from
      ///   change tracker details.
      /// </summary>
      /// <param name="TableName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult BindColumnListAvailableUsers(long productId)
      {
         var json = new JsonResult();

         try
         {
            // Call the data access methods for getting column list.
            var lstColumns = ProductDataAccess.BindColumnListAvailableUsers(productId);

            // Set the json behavior for get request.
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            // Assign column details to json data
            json.Data = lstColumns;

            return json;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get NDC and Wholesaler number Details.
      /// </summary>
      /// <param name="objProductML"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult GetNDCforWholesaler(AddWholesalerNumber objProductML, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var result = ProductDataAccess.GetAllProductForWholesalerNumber(objProductML);
         var total = result.Count();
         var orders = result.Skip(pagesize * pagenum).Take(pagesize);

         if (!string.IsNullOrEmpty(sortorder))
         {
            orders = sortorder == Constants.Model_GroupDataAccess_Asc
                        ? orders.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                        : orders.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
         }

         var resultNEw = new
         {
            TotalRows = total,
            Rows = orders
         };

         return this.Json(resultNEw, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Wholesaler details when user click on edit.
      /// </summary>
      /// <param name="WNID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetWholesalerDetailsByWholesalerID(long WNID)
      {
         var objAddWholesalerNumber = new AddWholesalerNumber();
         var lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Wholesaler).ToList();

         objAddWholesalerNumber = ProductDataAccess.GetWholesalerNumberByID(WNID);
         objAddWholesalerNumber.WholesalerProfileList = new SelectList(lstManufacturer, Constants.ProductController_ProfileCode, Constants.ProductController_Name);

         return this.PartialView("_AddWholesalerNumber", objAddWholesalerNumber);
      }

      ///****************************************************************************
      /// <summary>
      ///   Load partial view.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult LoadPartialView()
      {
         var objAddWholesalerNumber = new AddWholesalerNumber();
         var lstManufacturer = PolicyDataAccess.GetProfileListByType(Constants.ProductController_Wholesaler).ToList();

         objAddWholesalerNumber.WholesalerProfileList = new SelectList(lstManufacturer, Constants.ProductController_ProfileCode, Constants.ProductController_Name);

         return this.PartialView("_AddWholesalerNumber", objAddWholesalerNumber);
      }

      ///****************************************************************************
      /// <summary>
      ///   Insert and update wholesaler number for specific NDC.
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult InsertWholesalerNumber(AddWholesalerNumber objAddWholesalerNumber)
      {
         var objclsResponse = ProductDataAccess.AddWholesalerNumberToDB(objAddWholesalerNumber, this.User.Identity.Name);

         return this.Json(new { data = objclsResponse.Status, result = objclsResponse.response }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult UpdateWholesalerNumber(AddWholesalerNumber objAddWholesalerNumber)
      {
         var objclsResponse = ProductDataAccess.EditWholesalerNumberToDB(objAddWholesalerNumber, this.User.Identity.Name);

         switch (objclsResponse.Status)
         {
            // ...
            //=============================================================================
            case Status.OK:
               return this.Json(new { Result = PolicyResource.Update_Successfully, VersionChanged = false, objclsResponse.UpdateVersion }, JsonRequestBehavior.AllowGet);

            // ...
            //=============================================================================
            case Status.VersionChange:
               return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse }, JsonRequestBehavior.AllowGet);

            // ...
            //=============================================================================
            case Status.Deleted:
               return this.Json(new { Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

            // ...
            //=============================================================================
            case Status.Fail:
               return this.Json(new { Result = PolicyResource.Update_Fail, VersionChanged = false }, JsonRequestBehavior.AllowGet);

         }

         return this.Json(new { Result = PolicyResource.Update_Fail, VersionChanged = false }, JsonRequestBehavior.AllowGet);

      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="wholesalerId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      [HttpPost]
      public ActionResult DeleteWholesaler(long wholesalerId)
      {
         try
         {
            var objclsResponse = ProductDataAccess.DeleteWholesalerNumber(wholesalerId, this.User.Identity.Name);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = PolicyResource.OK_Message, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = PolicyResource.Fails, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            Console.WriteLine(ex.Message);
         }

         return this.Json(Resources.Product.Fails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the existence of wholesaler number.
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult CheckExistanceofWholesalerNumber(AddWholesalerNumber objAddWholesalerNumber)
      {
         return this.Json(new { data = ProductDataAccess.CheckWholesalerExist(objAddWholesalerNumber).Status }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all lot for specific product return as a JSON result.
      /// </summary>
      /// <param name="ProductID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult GetLotNumberByProductID(long ProductID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var result = ProductDataAccess.GetLotByProductID(ProductID);
         var total = result.Count;
         var orders = result.Skip(pagesize * pagenum).Take(pagesize);

         if (!string.IsNullOrEmpty(sortorder))
         {
            orders = sortorder == Constants.ProductController_Asc
                           ? orders.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                           : orders.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
         }

         return this.Json(new { TotalRows = total, Rows = orders }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Return JSON result of wholesaler number for specific to product.
      /// </summary>
      /// <param name="ProductID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult GetWholesalerByProductID(long ProductID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var total = 0;
         var orders = ProductDataAccess.GetWholesalerNumberByID(ProductID, sortdatafield, sortorder, pagesize, pagenum, out total);

         return this.Json(new { TotalRows = total, Rows = orders }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get and return result of policy specific to product.
      /// </summary>
      /// <param name="ProductID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public JsonResult GetPolicyByProductID(long ProductID)
      {
         return this.Json(new { Rows = ProductDataAccess.GetPolicyByID(ProductID) }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="searchvalue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult GetSearchType(string searchvalue)
      {
         return this.Json(ProductDataAccess.GetSearchResultStatus(searchvalue), JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="policyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetPolicyRuleToLoadPartialView(long policyID)
      {
         var objResponsePolicy = PolicyDataAccess.GetPolicyBasedOnPolicyID(policyID);

         return this.PartialView("_ProductPolicyDetails", objResponsePolicy);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="lotNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public JsonResult CheckExistanceofLotNumber(long productId, string lotNumber)
      {
         return this.Json(new { data = ProductDataAccess.CheckExistanceOfLotNumber(productId, lotNumber).Status }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="grpID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult PharmacyDetailsBasedGroup(int grpID, string sortdatafield, string sortorder, int pagesize, int pagenum)
      {
         var lstGroupDetails = ProductDataAccess.GroupProfileDetailsByGroupID(grpID);

         return this.Json(new { TotalRows = lstGroupDetails.Count, Rows = lstGroupDetails }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Create Lot Number Post methods.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Create(LotNumbersDataEntity model)
      {
         try
         {
            model.CreatedBy = this.User.Identity.Name;

            return this.Json(LotNumbersDataAccess.SaveLotNumber(model)
                                 ? new { Result = PolicyResource.Save_Successfully }
                                 : new { Result = PolicyResource.Save_Fails }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception)
         {
            return this.View();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminRuser)]
      public ActionResult PolicyDetailsByProduct()
      {
         return this.PartialView("_PolicyDetailsByProduct");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.LotNumberController_AllUser)]
      public ActionResult Edit(LotNumbersDataEntity model)
      {
         try
         {
            model.ModifiedBy = this.User.Identity.Name;

            var objclsResponse = LotNumbersDataAccess.UpdateLotNumbers(model);

            switch (objclsResponse.Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = PolicyResource.Update_Successfully, VersionChanged = false, UpdateVersion = objclsResponse.UpdateVersion }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true, Updatedresult = objclsResponse }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new {Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = PolicyResource.Update_Fail, VersionChanged = false }, JsonRequestBehavior.AllowGet);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(LotNumber.Fails, JsonRequestBehavior.AllowGet);
         }

         return this.Json(new { Result = PolicyResource.Update_Fail, VersionChanged = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="LotNumberID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult Delete(long LotNumberID)
      {
         try
         {
            switch (LotNumbersDataAccess.DeleteLotNumbers(LotNumberID, this.User.Identity.Name).Status)
            {
               // ...
               //=============================================================================
               case Status.OK:
                  return this.Json(new { Result = PolicyResource.OK_Message, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.VersionChange:
                  return this.Json(new { Result = PolicyResource.VersionChanged, VersionChanged = true }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Deleted:
                  return this.Json(new { Result = PolicyResource.Deleted, VersionChanged = false }, JsonRequestBehavior.AllowGet);

               // ...
               //=============================================================================
               case Status.Fail:
                  return this.Json(new { Result = PolicyResource.Fails, VersionChanged = false }, JsonRequestBehavior.AllowGet);

            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(LotNumber.Fails, JsonRequestBehavior.AllowGet);
         }

         return this.Json(new { Result = LotNumber.Fails, VersionChanged = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="pType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetWholesaler(string pType)
      {
         var lstManufacturer = PolicyDataAccess.GetProfileListByType(pType).ToList();

         lstManufacturer.Select(n => new SelectListItem
         {
            Text = n.Name,
            Value = n.ProfileCode.ToString()
         });

         return this.Json(lstManufacturer, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profilecode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult CheckPublishedPolicyExistance(int profilecode)
      {
         return this.Json(ProductDataAccess.CheckPublishedPolicyStatus(Constants.Manufacturer, profilecode), JsonRequestBehavior.AllowGet);
      }
   }
}