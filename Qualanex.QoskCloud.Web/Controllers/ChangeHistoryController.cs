﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ChangeHistoryController.cs">
///   Copyright (c) 2014 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Controllers
{
   using System;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Manage Change History Set.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ChangeHistoryController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult ChangeHistory()
      {
         return this.View();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="prodID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult ChangeTrackerDetails(long prodID)
      {
         return this.View("ChangeTrackerDetails", new ChangeTrackerSearch { ID = prodID });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ObjChangeTackerSearch"></param>
      /// <param name="FromRange"></param>
      /// <param name="ToRange"></param>
      /// <param name="ProdID"></param>
      /// <param name="TableNM"></param>
      /// <param name="ColumnNM"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public ActionResult GetChangeProductDetails(ChangeTrackerSearch ObjChangeTackerSearch, DateTime? FromRange, DateTime? ToRange, long ProdID, string TableNM, string ColumnNM)
      {
         var objGridModel = new RootObject();

         try
         {
            ObjChangeTackerSearch = ProductDataAccess.GetChangeTrackerDetails(FromRange, ToRange, ProdID, TableNM, ColumnNM);
            objGridModel.changeTrack = ObjChangeTackerSearch.lstChangeTrackentity;

            return this.Json(objGridModel, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return this.Json(objGridModel, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="TableName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute(Roles = Constants.Role_AdminCuser)]
      public JsonResult BindColumnListBaedonSelectedtable(string TableName)
      {
         var json = new JsonResult();

         try
         {
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            json.Data = CommonDataAccessLayer.GetColumnListOftable(TableName);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return json;
      }
   }
}
