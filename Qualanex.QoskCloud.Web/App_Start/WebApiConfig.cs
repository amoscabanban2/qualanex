﻿using Qualanex.QoskCloud.Utility;
using System.Web.Http;

namespace Qualanex.QoskCloud.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "QoskApi",
                routeTemplate: "Qosk/api/{controller}/{id}",
                defaults: new {  id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "QoskApi2",
                routeTemplate: "api/Qosk/{controller}/{id}",
                defaults: new { controller = "QoskApi", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: Constants.WebApiConfig_Url,
                routeTemplate: Constants.WebApiConfig_RouteTemplate,
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}
