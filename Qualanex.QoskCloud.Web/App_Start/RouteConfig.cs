﻿using Qualanex.QoskCloud.Utility;
using System.Web.Mvc;
using System.Web.Routing;

namespace Qualanex.QoskCloud.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection
            routes)
        {
            routes.IgnoreRoute(Constants.RouteConfig_DefaulUrl);  
            routes.MapRoute("Robots.txt", "robots.txt", new {controller="User", action = "Robots"});         
            routes.MapRoute(
                name: Constants.RouteConfig_DefaulUrl_Name,
                url: Constants.RouteConfig_DefaulUrl_Url,
                defaults: new { controller = Constants.RouteConfig_DefaulUrl_Controller, action = Constants.RouteConfig_DefaulUrl_Action, id = UrlParameter.Optional }
            );

            //Add the following line of code
            routes.MapMvcAttributeRoutes();
        }


    }
}
