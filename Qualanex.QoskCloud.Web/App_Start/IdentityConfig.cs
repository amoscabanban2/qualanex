﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Qualanex.QoskCloud.Web.Model;

namespace Qualanex.QoskCloud.Web
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            // Credentials:
            var credentialUserName = Qualanex.QoskCloud.Utility.Constants.EmailService_credentialUserName;
            var sentFrom = Qualanex.QoskCloud.Utility.Constants.EmailService_sentFrom;
            var pwd = Qualanex.QoskCloud.Utility.Constants.EmailService_pwd;

            // Configure the client:
            using (System.Net.Mail.SmtpClient client =
                new System.Net.Mail.SmtpClient(Qualanex.QoskCloud.Utility.Constants.EmailService_SmtpClient))
            {

                client.Port = 587;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;

                // Create the credentials:
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential(credentialUserName, pwd);

                client.EnableSsl = true;
                client.Credentials = credentials;

                // Create the message:
                using (var mail =
                     new System.Net.Mail.MailMessage(sentFrom, message.Destination))
                {

                    mail.Subject = message.Subject;
                    mail.Body = message.Body;

                    // Send:
                    client.SendMailAsync(mail);
                }
            }

            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider(Qualanex.QoskCloud.Utility.Constants.EmailService_PhoneCode, new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = Qualanex.QoskCloud.Utility.Constants.EmailService_MessageAndBodyFormat
            });
            manager.RegisterTwoFactorProvider(Qualanex.QoskCloud.Utility.Constants.EmailService_EmailCode, new EmailTokenProvider<ApplicationUser>
            {
                Subject = Qualanex.QoskCloud.Utility.Constants.EmailService_SecurityCode,
                BodyFormat = Qualanex.QoskCloud.Utility.Constants.EmailService_MessageAndBodyFormat
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create(Qualanex.QoskCloud.Utility.Constants.EmailService_Identity));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));
        }
    }
}
