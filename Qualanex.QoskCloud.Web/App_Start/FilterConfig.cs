﻿using Qualanex.QoskCloud.Web.Common;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web
{
    public class FilterConfig
    {
        /// <summary>
        /// RegisterGlobalFilters
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            if (filters != null)
            {
                filters.Add(new ExceptionFilterAttribute());
                filters.Add(new TraceFilterAttribute());
                filters.Add(new QoskAuthorizeAttribute());
#if !DEBUG
                filters.Add(new RequireHttpsAttribute());
#endif                 
            }
                
        }
       
    }
}
