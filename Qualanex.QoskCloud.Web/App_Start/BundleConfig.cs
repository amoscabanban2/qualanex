﻿using System.Web.Optimization;

namespace Qualanex.QoskCloud.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/jquery.cookie.js",
                        "~/Scripts/jquery.validationEngine-en.js",
                        "~/Scripts/jquery.validationEngine.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/reports").Include(
                "~/Scripts/kendo.mobile.min.js",
                "~/Scripts/kendo.web.min.js",
                "~/Areas/Report/ReportViewer/js/telerikReportViewer-9.2.15.1105.min.js"
                //"~/Areas/Report/ReportViewer/js/telerikReportViewer.kendo-13.0.19.116.min.js",
                //"~/Areas/Report/ReportViewer/js/telerikReportViewer-13.0.19.116.min.js"                
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jspatch.js",
                      "~/Scripts/custom-grid.js",
                      "~/Scripts/ProductJs.js",
                      "~/Scripts/jquery.dragtable.js",
                      "~/Scripts/jquery.easytabs.js",
                      "~/Scripts/jquery.maskedinput.js",
                      "~/Scripts/featherlight.min.js",
                      "~/Scripts/DataTables/jquery.dataTables.min.js",
                      "~/js/InfiniteScroll.js"
            //"~/Scripts/ProductDetailsJS.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/qosk").Include(
                    "~/js/CommonJS.js",
                    "~/js/Constants.js",
                    "~/js/Message.js",
                    "~/Scripts/jqx-all.js"

                    ));

            bundles.Add(new ScriptBundle("~/bundles/qosk/common").Include(
                "~/js/QoskCommon.js",
                "~/js/QoskForm.js",
                "~/js/QoskActionbar.js",
                "~/js/QoskWorksheet.js",
                "~/js/QoskSegment.DataTables.js",
                "~/Areas/Sidebar/js/Sidebar.js",
                "~/js/RelationControl.js",
                "~/js/MessageBox.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/slick").Include(
                "~/Scripts/Slick/slick.js" //http://kenwheeler.github.io/slick/
                ));

            bundles.Add(new ScriptBundle("~/bundles/qosk/induction").Include(
                "~/js/QoskArea.Product.js",
                "~/js/QoskTab.Notifications.js",
                "~/js/QoskTab.Policy.js",
                "~/js/QoskTab.Search.js",
                "~/js/QoskSegment.ContainerType.js",
                "~/js/QoskSegment.Finalize.js",
                "~/js/QoskSegment.LotExp.js",
                "~/js/QoskSegment.PolicyStatus.js",
                "~/js/QoskSegment.BinDesignation.js",
                "~/js/QoskSegment.ContainerImage.js",
                "~/js/QoskSegment.QuantityWeightCondition.js",
                "~/js/QoskSegment.ForeignContent.js",
                "~/js/Webcam.js",
                "~/Scripts/DYMO.Label.Framework.latest.js"
                ));

         bundles.Add(new ScriptBundle("~/bundles/qosk/designation").Include(
                "~/js/QoskSegment.BinDesignation.js",
                "~/js/QoskSegment.Binventory.js"
                ));

         bundles.Add(new ScriptBundle("~/bundles/qosk/binventory").Include(
                "~/js/QoskSegment.AuditInventory.js",
                "~/js/QoskSegment.Binventory.js"
                ));

         bundles.Add(new ScriptBundle("~/bundles/qosk/detail").Include(
                "~/js/QoskTab.Search.js", 
                "~/js/QoskArea.Product.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Site.css",
                      "~/Content/Style.css",
                      "~/Content/font-awesome.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/dragtable.css",
                      "~/css/validationEngine.jquery.css",
                      "~/Content/992.css",
                      "~/Content/bootstrap.min.css", 
                      "~/Css/jquery.tagsinput.css",
                      "~/Content/jqx.base.css",
                      "~/Content/Slick/slick.css",
                      "~/Content/Slick/slick-theme.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/custom").Include(
                      "~/Content/main-style.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                     "~/Content/themes/base/jquery.ui.core.css",
                     "~/Content/themes/base/jquery.ui.resizable.css",
                     "~/Content/themes/base/jquery.ui.selectable.css",
                     "~/Content/themes/base/jquery.ui.accordion.css",
                     "~/Content/themes/base/jquery.ui.autocomplete.css",
                     "~/Content/themes/base/jquery.ui.button.css",
                     "~/Content/themes/base/jquery.ui.dialog.css",
                     "~/Content/themes/base/jquery.ui.slider.css",
                     "~/Content/themes/base/jquery.ui.tabs.css",
                     "~/Content/themes/base/jquery.ui.datepicker.css",
                     "~/Content/themes/base/jquery.ui.progressbar.css",
                     "~/Content/themes/base/jquery.ui.theme.css"
                     ));

            bundles.Add(new StyleBundle("~/Bundles/Qosk/Css").Include(
                "~/Css/QoskPillPanel.css",
                "~/Content/DataTables/css/jquery.dataTables.min.css",
                "~/Css/QoskCommon.css",
                "~/Css/QoskDashboard.css",
                "~/Css/QoskLogin.css",
                "~/Css/QoskPage.css",
                "~/Css/QoskActionbar.css",
                "~/Css/QoskFilter.css",
                "~/Css/QoskForm.css",
                "~/Css/QoskApplet.css",
                "~/Css/QoskSegment.css",
                "~/Css/QoskWorkbook.css",
                "~/Css/QoskKeyboard.css",
                "~/Css/QoskSegment.WtQty.css",
                "~/Css/QoskSegment.DockDoor.css",
                "~/Css/QoskSegment.DataTables.css",
                "~/Css/QoskSegment.Staging.css",
                "~/Areas/Product/Css/qosk.css",
                "~/Css/featherlight.min.css",
                "~/Css/QoskMessagebox.css"
                ));

            bundles.Add(new StyleBundle("~/Areas/Report/Css/All").Include(
                "~/Css/kendo.blueopal.min.css",
                "~/Css/kendo.common.min.css",
                "~/Areas/Report/CSS/font-awesome.min.css",
                "~/Areas/Report/ReportViewer/styles/telerikReportViewer-9.2.15.1105.css"
                //"~/Areas/Report/CSS/telerikReportViewer-css.css"
                ));
         bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                "~/Scripts/dropzone/dropzone.js"));

         bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                  "~/Scripts/dropzone/dropzone.css"));

#if DEBUG
         BundleTable.EnableOptimizations = false;
#else
        BundleTable.EnableOptimizations = true;
#endif

        }
    }
}
