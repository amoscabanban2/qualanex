﻿$(function()
{
   //*****************************************************************************************
   //*
   //* LineReceiving.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the LineReceiving partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************
   
   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).ready(function()
   {
      table = window.dtHeaders("#searchGrid", '"order": [[1, "asc"]]', [], []);
      initialState();
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Readonly mode and select first row of grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function initialState()
   {
      setEditMode(false);
   }
});
