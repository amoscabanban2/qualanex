﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="LineReceivingController.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.LineReceiving.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.LineReceiving.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class LineReceivingController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult LineReceivingView()
      {
         this.ModelState.Clear();

         var viewModel = new LineReceivingViewModel
         {
            ActionMenu = LineReceivingModel.GetActionMenu()
         };

         return this.View(viewModel);
      }

   }
}
