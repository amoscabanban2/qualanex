///-----------------------------------------------------------------------------------------------
/// <copyright file="LineReceivingViewModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.LineReceiving.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Common;
   using Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class LineReceivingViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to pull only the first Line Receiving record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public LineReceivingModel SelectedReceivingModel { get; set; } = new LineReceivingModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Line Receiving list for the Line Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<LineReceivingModel> ReceivingDetails { get; set; } = new List<LineReceivingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }

}