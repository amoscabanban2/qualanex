﻿//*****************************************************************************
//*
//* GroupMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Provides functionality for the GroupMgmt partial class.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------


   //#############################################################################
   // Global functions
   //#############################################################################


   //#############################################################################
   // Global event handlers
   //#############################################################################

   var groupId;
   var table;
   var validate = "Click";
   var cancel = false;
   var txtChanges = false;
   var valid = false;
   var baseURL = "/GroupMgmt";
   var active = "";
   var keyPressVal = 0;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function()
   {
      console.log("GroupMgmt::ready()");

      // Initialize the state of the Actionbar
      ///////////////////////////////////////////////////////////////////////////////
      window.setEditMode(false);

      window.enableActionbarControl("#CMD_REFRESH", true);
      window.enableActionbarControl("#CMD_PRINT", true);

      $(".QoskSummary tbody tr:eq(1)").click();
   });

   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Refresh grid when click on refresh button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function()
   {
      if (!getEditMode() && $("a.active").attr("href") === "#tabs-summary")
      {
         var url = "ViewDataRequest";
         var isDelete = true;
         var attSrc = $("#CMD_RETIRED").attr("src");

         if (attSrc.indexOf("HideRetired(1)") >= 0)
         {
            isDelete = false;
         }

         $.ajax({
            type: "POST",
            url: url,
            data:
               {
                  isDeleted: isDelete, __RequestVerificationToken:
                     $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
               },
            success: function(data)
            {
               var $target = $("#MgmtSummary");
               var $newHtml = $(data);

               $target.replaceWith($newHtml);
               table = window.dtHeaders("#searchGrid", "", [], []);
               $("#searchGrid tbody tr:eq(1)").click();
            }
         });

         return true;
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Add New Group detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function()
   {
      if (!getEditMode())
      {
         if ($("a.active").attr("href") === "#tabs-summary")
         {
            active = "summary";
            groupId = 0;
            $("#GroupType").prop("required", true);
            enableActionbarControl("#CMD_RETIRED", false);
            enableActionbarControl("#CMD_REFRESH", false);
            setEditMode(true);

            $(".formBody").find("input[type=Text]").val("");
            $("#GroupType").val("0");
            $("#Status").val("0");

            if ($("#searchGrid tr.selected").hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }
            if (parseInt($("#searchGrid_paginate .paginate_button.current").attr("data-dt-idx")) > 1)
            {
               $("#searchGrid_paginate .paginate_button[data-dt-idx=1]").click();
            }
            $("#searchGrid tbody tr.hidden").removeClass("hidden");
            $("#searchGrid tbody").find("tr:first").addClass("selected");
         }
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Edit Group detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function()
   {
      if (!getEditMode())
      {
         if (groupId > 0)
         {
            if ($("a.active").attr("href") === "#tabs-association")
            {
               enableActionbarControl("#CMD_RETIRED", false);
               enableActionbarControl("#CMD_REFRESH", false);
               setEditMode(true);
               active = "association";
               $(".relationControl").enableRelationEdit();
               $("#GroupMgmtAssociation table tbody tr td input[name='ProfileName']")
                  .each(function()
                  {
                     if ($(this).val().length > 0)
                     {
                        $("#GroupMgmtAssociation #profileAssociationList option[value='" + $(this).val() + "']").val("");
                     }
                  });
               $(".formMax input").each(function()
               {
                  if ($(this).hasClass("editable"))
                  {
                     $(this).prop("readonly", true);
                  }
               });
               $(".formMax select").each(function()
               {
                  if ($(this).hasClass("editable"))
                  {
                     $(this).prop("disabled", true);
                  }
               });
            }
            else if ($("a.active").attr("href") === "#tabs-summary")
            {
               $("#GroupType").prop("required", true);
               enableActionbarControl("#CMD_RETIRED", false);
               enableActionbarControl("#CMD_REFRESH", false);
               setEditMode(true);
               active = "summary";
               groupId = $("#searchGrid tr.selected").closest("tr").attr("MgmtEntity-id");
            }
         }

         return true;
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save Group detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function()
   {
      if (active === "summary")
      {
         if (getEditMode() && !$(".formMax input").hasClass("inputError")
            && !$(".formMax select").hasClass("inputError"))
         {
            if (!window.getFormChanged())
            {
               validate = "Click";
               return false;
            } else
            {
               if (window.getFormChanged() && !$(".formBody").filter(":input").hasClass("inputError"))
               {
                  var gpName = $("#GroupName").val();

                  if (gpName.length === 0)
                  {
                     setColorAndClass("#FFFFC0CB", "#GroupName", "inputError");
                     enableActionbarControl("#CMD_SAVE", false);
                     return false;
                  }

                  var groupCode = $("#GroupType").val();

                  if (groupCode === null || groupCode === "")
                  {
                     setColorAndClass("#FFFFC0CB", "#GroupType", "inputError");
                     enableActionbarControl("#CMD_SAVE", false);
                     validate = "Click";
                     return false;
                  }

                  var profileName = $(".profileCodeField")[0].value;
                  var profileCode = $("#profileList option[value='" + profileName + "']")
                     .attr("data-field-name");

                  if ((profileCode === undefined || profileCode.length === 0))
                  {
                     profileName = "";
                  }

                  validate = "";
                  messageObject = [{
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to save the changes?</h3>"
                  }];
                  btnObjects = [{
                     "id": "btn_group_saving",
                     "name": "Yes",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center"
                  }, {
                     "id": "btn_group_no",
                     "name": "No",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center"
                  }
                  ];

                  titleobjects = [{
                     "title": "Confirm"
                  }];
                  $(this).addMessageButton(btnObjects, messageObject);
                  $(this).showMessageBox(titleobjects);
               }

               return false;
            }
         }
      }
      else if (active === "association")
      {
         if (!window.getFormChanged())
         {
            return false;
         }

         var profileEntity = [];

         enableActionbarControl("#CMD_SAVE", false);
         $("#GroupMgmtAssociation table tbody tr.updated," +
                       "table tbody tr.relationEditRow")
                       .each(function()
                       {
                          if ($(this).find("input[name=PrimaryProfileCode]").val() !== undefined
                             && $(this).find("input[name=PrimaryProfileCode]").val() !== "")
                          {
                             var trObjects = {
                                "GroupID": groupId,
                                "PrimaryProfileCode": $(this).find("input[name=PrimaryProfileCode]").val(),
                             };
                             var bln = false;
                             for (var i = 0; i < profileEntity.length; i++)
                             {
                                if (profileEntity[i].PrimaryProfileCode ===
                                   $(this).find("input[name=PrimaryProfileCode]").val())
                                {
                                   bln = true;
                                }
                             }
                             if (!bln)
                             {
                                profileEntity.push(trObjects);
                             }
                          }
                       });
         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            url: baseURL + "/GroupMgmt/SaveToProfile",
            data: "profileEntity=" + JSON.stringify(profileEntity)
                  + "&groupId=" + groupId
                  + "&isDeleted=" + false
                  + getAntiForgeryToken(),
            success: function(data)
            {
               if (data.Success)
               {
                  setEditMode(false);
                  active = "";
                  $(".relationControl").disableRelationEdit();
                  getProfileByGroup(groupId);
                  enableActionbarControl("#CMD_REFRESH", true);
                  enableActionbarControl("#CMD_RETIRED", true);
               }
               else
               {
                  enableActionbarControl("#CMD_SAVE", false);
               }
            }
         });

      }

      return true;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_group_saving", function()
   {
      if (validate === "Click")
      {
         return false;
      }

      if ($("#GroupName").val().length === 0)
      {
         return false;
      }

      $(this).closeMessageBox();
      var profileName = $(".profileCodeField")[0].value;
      var profileCode = $("#profileList option[value='" + profileName + "']")
         .attr("data-field-name");
      var bodyData = getBodyData(profileCode === undefined
         ? ""
         : profileCode,
         $("#GroupType").val() === undefined
         ? ""
         : $("#GroupType").val());

      if (validate === "" && groupId !== undefined)
      {
         var isDelete = true;
         var attSrc = $("#CMD_RETIRED").attr("src");

         if (attSrc.indexOf("HideRetired(0)") >= 0)
         {
            isDelete = false;
         }

         $.ajax({
            type: "POST",
            url: baseURL + "/GroupMgmt/SaveGroupDetail",
            data: "groupID=" + groupId + "&bodyData=" + bodyData + "&isDeleted=" +
               isDelete + getAntiForgeryToken(),
            datatype: "JSON",
            success: function(data)
            {
               if (data.Success && validate === "")
               {
                  groupId = data.max > 0
                     ? data.max
                     : groupId = 0;
                  validate = "Click";
                  setEditMode(false);
                  $(".formBody .selectEx").removeAttr("required")
                  active = "";
                  clearTextBoxes();
                  refreshGrid(groupId);
                  enableActionbarControl("#CMD_REFRESH", true);
                  enableActionbarControl("#CMD_RETIRED", true);
                  $(".formBody .inputEx,.selectEx")
                     .removeClass("editable")
                     .addClass("editable");
               }

               validate = "Click";
            },
            error: function(data)
            {
               showError("The controller call fails on save group request.");
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_group_no", function()
   {
      $(this).closeMessageBox();
      validate = "Click";
   });
   
   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if (window.getFormChanged())
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
               }];
               btnObjects = [{
                  "id": "btn_group_cancel",
                  "name": "Yes",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }, {
                  "id": "btn_group_cancel_no",
                  "name": "No",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
               ];

               titleobjects = [{
                  "title": "Confirm Cancellation"
               }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
               return false;
            }
            else
            {
               document.getElementById("profileList").innerHTML = "";
               $(this).closeMessageBox();
               $(".formBody").find("input[type=Text]").val("");
               if ($("#searchGrid tbody").find("tr:eq(1)").hasClass("selected")
                  && !getEditMode())
               {
                  $("#searchGrid tbody").find("tr:eq(1)").remove();
               }

               clearTextBoxes();
               $(".formMax .inputEx,.selectEx").removeClass("inputError");
               setEditMode(false);
               if (groupId === 0)
               {
                  $("#searchGrid tr[MgmtEntity-id=0]").addClass("hidden");
                  $("#searchGrid tr:eq(2)").trigger("click");
               }
               else
               {
                  $("#searchGrid tr[MgmtEntity-id=" + groupId + "]").trigger("click");
               }
               enableActionbarControl("#CMD_REFRESH", true);
               enableActionbarControl("#CMD_RETIRED", true);
               enableActionbarControl("#CMD_SAVE", false);
               $("#GroupType").removeAttr("required");
               active = "";
            }

         }
         else if (active === "association")
         {
            enableActionbarControl("#CMD_REFRESH", true);
            enableActionbarControl("#CMD_RETIRED", true);
            if (groupId > 0)
            {
               window.getFormChanged()
                        ? getProfileByGroup(groupId)
                        : $(".relationControl").disableRelationEdit();
            }
            document.getElementById("profileAssociationList").innerHTML = "";
            setEditMode(false);
            active = "";
         }

      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_group_cancel", function()
   {
      $("#GroupType").removeAttr("required");
      document.getElementById("profileList").innerHTML = "";
      setEditMode(false);
      cancel = true;

      if (getEditMode() && validate === "")
      {
         return false;
      }

      $(this).closeMessageBox();
      clearTextBoxes();
      $(".formMax .inputEx,.selectEx").removeClass("inputError");
      refreshGrid(groupId);
      enableActionbarControl("#CMD_REFRESH", true);
      enableActionbarControl("#CMD_RETIRED", true);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Print icon click action.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function()
   {
      $(this).setErrorMessage("The print icon click action is under development", "Error Message", "ok");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   View/Hide retired record(s) Group detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRetired = function()
   {
      if (!getEditMode())
      {
         if ($("a.active").attr("href") === "#tabs-summary")
         {
            var isDeleted = true;
            var attSrc = $("#CMD_RETIRED").attr("src");
            var $newSrc = attSrc.replace("HideRetired(1)", "ShowRetired(1)");

            if (attSrc.indexOf("HideRetired(1)") < 0)
            {
               $newSrc = attSrc.replace("ShowRetired(1)", "HideRetired(1)");
               isDeleted = false;
            }

            $("#CMD_RETIRED").attr("src", $newSrc);
            $("#CMD_RETIRED").show();

            var url = "ViewDataRequest";            

            $.ajax({
               type: "POST",
               url: url, // Controllers Function GroupView_Hide
               data: "isDeleted=" + isDeleted + getAntiForgeryToken(),
               success: function(data)
               {
                  var $targetId = $("#MgmtSummary");
                  var $newHtml = $(data);
                  $targetId.replaceWith($newHtml);                 
                  $("#searchGrid tbody").find("tr:eq(1)")
                    .unbind("click").click();
                  if (!$("#searchGrid").hasClass("dataTable"))
                  {
                     table = window.dtHeaders("#searchGrid", '"order": [[1, "asc"]]', [], []);
                  }
               },
               error: function()
               {
                  showError("The controller call fails on view data request.");
               }
            });
            return true;
         }
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function(saveAndvalidate, field, data)
   {
      //console.log(saveAndvalidate + ":" + field + ":" + data);

      //console.log($("#" + field).prop("tagName") + "::" + field);

      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        $("#" + field).val(data);
                     }

                     //console.log("TEXT::" + field);

               }

               break;
            }

         case "SELECT":
            {
               //console.log("SELECT::" + field + "::" + data);
               $("#" + field).val(data);
            }
      }

      if (saveAndvalidate)
      {
      }
      else
      {
         //console.log($("#" + field).attr("type"));


      }
   }





   //#############################################################################
   //#   Input Changes
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check Profile code is exist.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkFieldDb()
   {
      if (valid && !txtChanges)
      {
         return false;
      }

      var gpName = $("#GroupName").val();

      if (gpName.length === 0)
      {
         setColorAndClass("#FFFFC0CB", "#GroupName", "inputError");
         enableActionbarControl("#CMD_SAVE", false);
         return false;
      }

      var url = "Check_With_DB";

      $.ajax({
         type: "POST",
         url: url,
         datatype: "JSON",
         data: "groupId=" + groupId + "&groupName=" + gpName +
            getAntiForgeryToken(),
         success: function(data)
         {
            if (data.Success)
            {
               valid = true;
               txtChanges = false;

               if ($("#GroupName").hasClass("inputError"))
               {
                  removeColorAndClass("#ECDD9D", "#GroupName", "inputError");
               }

               if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
               {
                  enableActionbarControl("#CMD_SAVE", true);
               }

               return false;
            }
            else
            {
               valid = false;
               validate = "Click";
               $(this).closeMessageBox();
               setColorAndClass("#FFFFC0CB", "#" + data.Input, "inputError");
               enableActionbarControl("#CMD_SAVE", false);
               return false;
            }
         }
      });

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", ".inputEx", function()
   {
      if (active === "summary")
      {
         var id = $(this).attr("id");
         var value = $(this).val();

         updateDataTable(value, id, groupId);

         if ($(this).attr("id") === "ProfileName")
         {
            keyPressVal++
            addDataListOptions("profileList", $(this).val(), keyPressVal);
         }
      }
      else if (active === "association")
      {
         keyPressVal++;
         if ($("#GroupMgmtAssociation #profileAssociationList option[value='"
                              + $(this).val() + "']").attr("data-field-name") !== undefined)
         {
            $(this).closest("tr").find("td input[name=PrimaryProfileCode]").val(
               $("#GroupMgmtAssociation #profileAssociationList option[value='"
                              + $(this).val() + "']").attr("data-field-name"));
            $(this).css("background-color", "#ECDD9D");
            return false;
         }
         else
         {
            $(this).closest("tr").find("td input[name=PrimaryProfileCode]").val("");
         }

         addDataListOptions("profileAssociationList", $(this).val(), keyPressVal);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set data from grid to inputs.
   //*
   //* Parameters:
   //*   inputId - Description not available.
   //*   value   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function setData(inputId, value)
   {
      $(inputId).val(value);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set the Edit mode flag.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".inputEx", function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if ($(this).closest("input").attr("id") === "GroupName")
            {
               txtChanges = true;
            }
         }
         else if (active === "association")
         {
            $(this).removeAttr("placeholder");
            $(this).css("text-align", "left");
            var inputOldValue = $(this).closest("tr").find("input[name=PrimaryProfileCode]").val();
            var profileCode = $("#GroupMgmtAssociation #profileAssociationList option[value='" + $(this).val() + "']").attr("data-field-name");
            if (inputOldValue !== "" && inputOldValue !== undefined)
            {
               $("#GroupMgmtAssociation #profileAssociationList option[data-field-name=" + inputOldValue + "]").show();
               $("#GroupMgmtAssociation #profileAssociationList option[data-field-name='" + inputOldValue + "']").attr("readonly", false);
               $("#profileAssociationList option[data-field-name=" + inputOldValue + "]")
                  .val($("#profileAssociationList option[data-field-name=" + inputOldValue + "]")
                  .attr("data-field-value"));
            }

            if (profileCode !== undefined && $(this).val().length > 0)
            {
               $(this).closest("tr").find("input[name=PrimaryProfileCode]").val(profileCode);
               $("#GroupMgmtAssociation #profileAssociationList option[data-field-name='" + profileCode + "']").hide();
               $("#GroupMgmtAssociation #profileAssociationList option[data-field-name='" + profileCode + "']").attr("readonly", true);
               $("#profileAssociationList option[data-field-name=" + profileCode + "]").val("");
               $(this).removeAttr("style");
            }
            else
            {
               $(this).closest("tr").find("input[name=PrimaryProfileCode]").val("");
               $(this).css("background-color", "#e9967a");
            }

            document.getElementById("profileAssociationList").innerHTML = "";

            if (!window.getFormChanged())
            {
               window.setFormChanged(true);
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Leave the Group Name textbox.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("blur", "#GroupName", function()
   {
      if (getEditMode())
      {
         checkFieldDb();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set the Edit mode flag on selectEx.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".selectEx", function()
   {
      if (getEditMode())
      {
         window.setFormChanged(true);

         if ($(this).closest("select").attr("id") === "GroupType")
         {
            removeColorAndClass("#ECDD9D", "#GroupType", "inputError");

            if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
            {
               enableActionbarControl("#CMD_SAVE", true);
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange ProfileCode.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".profileCodeField", function(e)
   {
      if (getEditMode() && !window.getFormChanged())
      {
         $("#profileList option").each(function()
         {
            if ($(this).val() === $(e.target).val())
            {
               if (active === "summary")
               {
                  $(this).attr("data-field-name");
                  updateDataTable($(this).attr("data-field-name")
                     , "ProfileName"
                     , groupId);
               }
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange ProfileCode.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#GroupMgmtAssociation .profileCodeAccosiationField", function(e)
   {
      if (getEditMode() && !window.getFormChanged())
      {
         $("#profileAssociationList option").each(function()
         {
            if ($(this).val() === $(e.target).val())
            {
               $(this).attr("readonly", true);
               $(this).hide();
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange GoupList.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#GroupType", function()
   {
      if (getEditMode())
      {
         updateDataTable($(this).find("option[value=" + $(this).val() + "]")
            .attr("data-field-name"), "GroupType", groupId);
      }
   });

   //#############################################################################
   //#   Grid Changes/Events
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set data from input  to Grid.
   //*
   //* Parameters:
   //*   inValue - Description not available.
   //*   inField - Description not available.
   //*   gpId    - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function updateDataTable(inValue, inField, gpId)
   {
      var row = $("#searchGrid tbody").find("tr[MgmtEntity-id=0]").closest("tr");

      if (gpId > 0)
      {
         row = $("#MgmtSummary").find("#searchGrid [MgmtEntity-id=" + gpId + "]");
      }

      if (inField === "GroupName")
      {
         row.find("td:eq(2)").html(inValue);
      }

      if (inField === "GroupType")
      {
         row.find("td:eq(3)").html(inValue);
      }

      if (inField === "ProfileName")
      {
         row.find("td:eq(4)").html(inValue);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Refresh grid after save or cancel.
   //*
   //* Parameters:
   //*   grpId - We Get Group ID and send it to this function for selecting the
   //*           specific row.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function refreshGrid(grpId)
   {
      var url = "ViewDataRequest";

      if (validate === "")
      {
         return false;
      }

      if ($("#searchGrid tr").length < 3
         && $("#searchGrid_paginate span a").length === 1)
      {
         groupId = 0;
      }

      var isDelete = true;
      var attSrc = $("#CMD_RETIRED").attr("src");

      if (attSrc.indexOf("HideRetired(0)") >= 0)
      {
         isDelete = false;
      }

      $.ajax({
         type: "POST",
         url: url,
         data:
            {
               isDeleted: isDelete, __RequestVerificationToken:
                  $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
            },
         success: function(data)
         {
            var $target = $("#MgmtSummary");
            var $newHtml = $(data);

            $target.replaceWith($newHtml);
            table = window.dtHeaders("#searchGrid", "", [], []);

            if (!table.$("tr.selected").hasClass("selected"))
            {
               var i = 0;

               if (grpId > 0)
               {
                  $newHtml.find("#searchGrid [MgmtEntity-id='"
                                + grpId + "']").click();
               } else
               {
                  $newHtml.find("#searchGrid tbody").find("tr:eq(1)").click();
               }
            }

            if (!$("#searchGrid tr.selected").hasClass("selected"))
            {
               $("#GroupType").val("0");
               $("#Status").val("0");
            }
         }
      });

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle when a Group's detail is selected form the table.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("click", ".GroupDetail-div-click", function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if ($("#CMD_NEW").attr("src").indexOf("(0)") > 0
               || $("#CMD_EDIT").attr("src").indexOf("(0)") > 0)
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to Save the change?</h3>"
               }];
               btnObjects = [{
                  "id": "CMD_SAVE",
                  "name": "Save",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center;margin-right:5px"
               },
               {
                  "id": "btn_Continue",
                  "name": "Continue",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center;margin-right:5px",
                  "function": "onclick='$(this).closeMessageBox();'"
               },
               {
                  "id": "btn_Discard_Save",
                  "name": "Discard",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }];

               titleobjects = [{ "title": "Confirmation" }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);

               return false;
            };

            ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);

            return false;
         }
      }
      else
      {
         table = table === undefined ? window.dtHeaders("#searchGrid", "", [], []) : table;

         if (!cancel)
         {
            if (!$(this).hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }

            //$(this).closest("tr").find("td").each(function()
            //{
            //   if ($(this).attr("exchange") === "GroupName")
            //   {
            //      setData("#ID_GROUPNAME", $(this).attr("exchange-value"));
            //   }
            //   else if ($(this).attr("exchange") === "ProfileName")
            //   {
            //      setData("#ID_PROFILECODE", $(this).attr("exchange-value"));
            //   }
            //   else if ($(this).attr("exchange") === "GroupType")
            //   {
            //      setData("#ID_GROUPTYPE", $(this).attr("exchange-value"));
            //   }
            //   else if ($(this).attr("exchange") === "Status")
            //   {
            //      if ($(this).attr("exchange-value") === "Enabled")
            //      {
            //         $("#ID_STATUS").val("0");
            //      }
            //      else
            //      {
            //         $("#ID_STATUS").val("1");
            //      }
            //   }
            //});

            $(this).closest("tr").addClass("selected");
            groupId = $(this).closest("tr").attr("MgmtEntity-id");
         }
         else
         {
            if (groupId === 0)
            {
               if (!$(this).hasClass("selected"))
               {
                  $("#searchGrid tr.selected").removeClass("selected");
               }

               if ($("#searchGrid tr").length > 2)
               {
                  groupId = $("#searchGrid tr:eq(2)").closest("tr").attr("MgmtEntity-id");
               }
            }

            if (groupId > 0)
            {
               //$("#searchGrid tr[MgmtEntity-id=" + groupId + "]").closest("tr")
               //   .find("td")
               //   .each(function()
               //   {
               //      if ($(this).attr("exchange") === "GroupName")
               //      {
               //         setData("#ID_GROUPNAME", $(this).attr("exchange-value"));
               //      }
               //      else if ($(this).attr("exchange") === "ProfileName")
               //      {
               //         setData("#ID_PROFILECODE", $(this).attr("exchange-value"));
               //      }
               //      else if ($(this).attr("exchange") === "GroupType")
               //      {
               //         setData("#ID_GROUPTYPE", $(this).attr("exchange-value"));
               //      }
               //      else if ($(this).attr("exchange") === "Status")
               //      {
               //         if ($(this).attr("exchange-value") === "Enabled")
               //         {
               //            $("#ID_STATUS").val("0");
               //         }
               //         else
               //         {
               //            $("#ID_STATUS").val("1");
               //         }
               //      }
               //   });

               if (!$("#searchGrid tr[MgmtEntity-id=" + groupId + "]")
                  .closest("tr")
                  .hasClass("selected"))
               {
                  $("#searchGrid tr[MgmtEntity-id=" + groupId + "]")
                     .closest("tr")
                     .addClass("selected");
               }
            }
            cancel = false;
         }
         if (groupId > 0 && $("a.active").attr("href") === "#tabs-association")
         {
            $("#GroupMgmtAssociation").removeAttr("style");
            getProfileByGroup(groupId);
         }
      }

      return false;
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   Discard .
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_Discard_Save", function()
   {
      if (!getEditMode())
      {
         return false;
      }
      active = "";
      cancel = true;

      $(this).closeMessageBox();
      $(".formBody").find("input[type=Text]").val("");

      if ($("#searchGrid tbody").find("tr:eq(1)").hasClass("selected")
         && !getEditMode())
      {
         $("#searchGrid tbody").find("tr:eq(1)").remove();
      }

      clearTextBoxes();

      $(".formMax .inputEx,.selectEx").removeClass("inputError");
      //$(".formMax .inputEx,.selectEx").removeAttr("style");
      setEditMode(false);
      refreshGrid(groupId);
      enableActionbarControl("#CMD_REFRESH", true);
      enableActionbarControl("#CMD_RETIRED", true);
   });   

   //#############################################################################
   //#   Controls
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get All FormBody data to the string list for controller input.
   //*
   //* Parameters:
   //*   profileCode - Description not available.
   //*   groupCode   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function getBodyData(profileCode, groupCode)
   {
      var bodyData = null;

      $(".formMax *").filter(":input").each(function()
      {
         if ($(this).attr("id") === "ProfileName")
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + profileCode + '",';
         }
         else if ($(this).attr("id") === "GroupType")
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + groupCode + '",';
         }
         else
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + $(this).val() + '",';
         }
      });

      return bodyData;
   };

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clearTextBoxes()
   {
      $(".formMax .inputEx").val("");
      $(".formMax .selectEx").val("0");
      $(".formMax .inputEx,.selectEx").removeAttr("style");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Select a Form by ID, serialize the content, clean it, and send it back
   //*   as an object
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   (function($)
   {
      $.fn.serializeObject = function()
      {
         var self = this,
             json = {},
             pushCounters = {},
             patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
             };

         this.build = function(base, key, value)
         {
            base[key] = value;
            return base;
         };

         this.push_counter = function(key)
         {
            if (pushCounters[key] === undefined)
            {
               pushCounters[key] = 0;
            }
            return pushCounters[key]++;
         };

         $.each($(this).serializeArray(), function()
         {
            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
               return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverseKey = this.name;

            while ((k = keys.pop()) !== undefined)
            {
               // adjust reverse_key
               reverseKey = reverseKey.replace(new RegExp("\\[" + k + "\\]$"), "");

               // push
               if (k.match(patterns.push))
               {
                  merge = self.build([], self.push_counter(reverseKey), merge);
               }
                  // fixed
               else if (k.match(patterns.fixed))
               {
                  merge = self.build([], k, merge);
               }
                  // named
               else if (k.match(patterns.named))
               {
                  merge = self.build({}, k, merge);
               }
            }

            json = $.extend(true, json, merge);
         });

         return json;
      };
   })(jQuery);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on each tab in GroupMgmtView.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".tabContainer a", function()
   {
      if ($("a.active").attr("href") === "#tabs-association")
      {
         if (!getEditMode())
         {
            if (groupId > 0)
            {
               getProfileByGroup();
            }
         }
         else
         {
            if (active === "summary")
            {
               $("#GroupMgmtAssociation").css("display", "none");
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   function for get Profile of each group.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getProfileByGroup()
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + "/GroupMgmt/ProfileEntity",
         data: "groupId=" + groupId + "&isDeleted=" + false + getAntiForgeryToken(),
         success: function(data)
         {
            var $target = $("#GroupMgmtAssociation");
            var $newHtml = $(data);
            $newHtml.find("tr.blank").find("input[name=ProfileName]")
               .attr("placeholder", "- - - - - - - - - Add New Profile - - - - - - - - -")
               .css("text-align", "center");
            var $profileInput = $newHtml.find("input[name='ProfileName']");
            $profileInput.attr("maxlength", "50");
            $profileInput.addClass("profileCodeAccosiationField inputEx editable");
            $profileInput.attr("list", "profileAssociationList");
            $newHtml.find("thead tr th").each(function()
            {
               if ($(this).html() === "Profile Name")
               {
                  $(this).html("Profiles");
               }
            });
            $newHtml.find("thead tr th:eq(6)").html("Profiles");
            $target.replaceWith($newHtml);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   function for get delete each profile.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "input.deleteGroupRelation", function()
   {
      if (!$(this).closest("tr").hasClass("blank"))
      {
         $("#GroupMgmtAssociation #profileAssociationList option[data-field-value='"
            + $(this).closest("tr").find("td input[name=ProfileName]").val() + "']")
            .val($(this).closest("tr").find("td input[name=ProfileName]").val());
         $(this).closest("tr").find("td input[name=ProfileName]").val();
         $(this).deleteRelationRow();
         if (!window.getFormChanged())
         {
            window.setFormChanged(true);
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   search profile name by input.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function addDataListOptions(dataListId, strData, keyPress)
   {
      document.getElementById(dataListId).innerHTML = "";
      if (strData !== "")
      {
         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "strData=" + strData + "&keyPressValue=" + keyPress + "&browser=" + navigator.userAgent + getAntiForgeryToken(),
            url: baseURL + "/GroupMgmt/GetProfileName",
            success: function(jsonData)
            {
               if (jsonData.keyPressValue !== keyPressVal)
               {
                  return false;
               }

               var itemLists = [];
               var options = "";

               if (active === "association")
               {
                  $("#GroupMgmtAssociation table tbody tr td input[name='ProfileName']").each(function()
                  {
                     if ($(this).val().length > 0)
                     {
                        itemLists.push({ "PrimaryProfileCode": $(this).closest("tr").find("td input[name='PrimaryProfileCode']").val() });
                     }
                  });
               }

               for (var jsIndx = 0; jsIndx < jsonData.profileEntity.length; jsIndx++)
               {
                  if (itemLists.length === 0)
                  {
                     options += "<option data-field-name='" + jsonData.profileEntity[jsIndx].PrimaryProfileCode +
                     "' value='" + jsonData.profileEntity[jsIndx].ProfileName + "'> " + jsonData.profileEntity[jsIndx].ProfileName + "</option>";
                  }
                  else
                  {
                     var bln = false;

                     for (var ind = 0; ind < itemLists.length; ind++)
                     {
                        if (String(jsonData.profileEntity[jsIndx].PrimaryProfileCode) === itemLists[ind].PrimaryProfileCode)
                        {
                           bln = true;
                        }
                     }

                     if (bln === false)
                     {
                        options += "<option data-field-name='" + jsonData.profileEntity[jsIndx].PrimaryProfileCode +
                     "' value='" + jsonData.profileEntity[jsIndx].ProfileName + "'> " + jsonData.profileEntity[jsIndx].ProfileName + "</option>";
                     }
                  }
               }

               document.getElementById(dataListId).innerHTML = options;
            }
         });
      }
   }

});
