///-----------------------------------------------------------------------------------------------
/// <copyright file="GroupMgmtModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GroupMgmt.Models
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   using Microsoft.Ajax.Utilities;

   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GroupMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public GroupMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~GroupMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GetGroupDetails(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.Group
                       join profileGrpRel in cloudEntities.Profile on groups.PrimaryProfileCode equals profileGrpRel.ProfileCode
                       join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                       where (groups.IsDeleted == false || groups.IsDeleted == isDeleted)
                       select new GroupEntity
                       {
                          GroupID = groups.GroupID,
                          GroupName = groups.Name,
                          GroupType = groups.GrpTypeCode,
                          PrimaryProfileCode = profileGrpRel.ProfileCode,
                          isDeleted = groups.IsDeleted,
                          Status = groups.IsDeleted
                                       ? "Retired"
                                       : "Enabled",
                          ProfileName = profileGrpRel.Name,
                          Description = groupTypeDict.Description
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GetProfileList(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from profile in cloudEntities.Profile
                       where (profile.IsDeleted == isDeleted)
                       select new GroupEntity
                       {
                          PrimaryProfileCode = profile.ProfileCode,
                          ProfileName = profile.Name
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Profile details by GroupId.
      /// </summary>
      /// <param name="groupId"></param>
      /// <param name="isDelete"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GetProfileList(int groupId, bool isDelete)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from profilesGrp in cloudEntities.ProfileGrpRel
                       join profiles in cloudEntities.Profile on profilesGrp.ProfileCode equals profiles.ProfileCode
                       where (profilesGrp.IsDeleted == isDelete && profilesGrp.GrpID == groupId)
                       select new GroupEntity
                       {
                          GroupID = profilesGrp.GrpID,
                          PrimaryProfileCode = profilesGrp.ProfileCode,
                          ProfileName = profiles.Name,
                          Version = profilesGrp.Version,
                          isDeleted = profilesGrp.IsDeleted
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get profileName by input.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <param name="strData"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GetProfileList(bool isDeleted, string strData, string browser)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return browser.Contains("Edge")
                  ? (from profile in cloudEntities.Profile
                     where (profile.IsDeleted == isDeleted) && profile.Name.Contains(strData)
                     select new GroupEntity
                     {
                        PrimaryProfileCode = profile.ProfileCode,
                        ProfileName = profile.Name
                     }).ToList()
                  : (from profile in cloudEntities.Profile
                     where (profile.IsDeleted == isDeleted) && profile.Name.Contains(strData)
                     select new GroupEntity
                     {
                        PrimaryProfileCode = profile.ProfileCode,
                        ProfileName = profile.Name
                     }).Take(40).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GetGroupTypeList(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.GroupTypeDict
                       where (groups.IsDeleted == isDeleted) && groups.Code != "UNKN"
                       select new GroupEntity()
                       {
                          GroupType = groups.Code,
                          GroupName = groups.Description
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool NewGroup(string curUser, string bodyData, bool isDeleted)
      {
         var status = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var group = new Group
               {
                  Name = GetOneValue(bodyData, "GroupName").ToString().Trim(),
                  GrpTypeCode = GetOneValue(bodyData, "GroupType").ToString().Trim(),
                  Version = 1,
                  IsDeleted = GetOneValue(bodyData, "Status") != "Enabled",
                  EffectiveStartDate = DateTime.UtcNow,
                  EffectiveEndDate = DateTime.UtcNow.AddDays(90),
                  CreatedBy = curUser,
                  PrimaryProfileCode = string.IsNullOrWhiteSpace(GetOneValue(bodyData, "ProfileName"))
                                          ? 0
                                          : Convert.ToInt32(GetOneValue(bodyData, "ProfileName"))
               };

               cloudEntities.Group.Add(group);
               cloudEntities.SaveChanges();
               status = true;
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetGroupId(string curUserName)
      {
         try
         {
            var profileCode = GetProfileCode(curUserName);

            using (var cloudEntities = new QoskCloud())
            {
               return (from profileGroupRel in cloudEntities.ProfileGroupRel
                       where profileGroupRel.ProfileCode == profileCode
                       select profileGroupRel.ProfileGroupID).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetNewGroupId()
      {
         var cloudEntities = new QoskCloud();

         return cloudEntities.Group.Max(t => t.GroupID) + 1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetGroupType(string curUserName)
      {
         try
         {
            var groupId = GetGroupId(curUserName);

            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.Group
                       where groups.GroupID == groupId
                       select groups.GrpTypeCode).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetGroupVersion(string curUserName)
      {
         try
         {
            var groupId = GetGroupId(curUserName);

            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.Group
                       where groups.GroupID == groupId
                       select groups.Version).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupName"></param>
      /// /// <param name="groupId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckGroupName(string groupName, int groupId)
      {
         var cloudEntities = new QoskCloud();

         var count = groupId == 0
                           ? cloudEntities.Group.Count(t => t.Name == groupName)
                           : cloudEntities.Group.Count(t => t.Name == groupName && t.GroupID != groupId);

         return count > 0
                     ? groupName
                     : null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckGroupType(string groupType)
      {
         var cloudEntities = new QoskCloud();
         var groupCount = cloudEntities.GroupTypeDict.Count(t => t.Code == groupType);

         return groupCount > 0
                        ? groupType
                        : null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int CheckProfileCode(string profileCode)
      {
         var chkCode = 0;

         try
         {
            var cloudEntities = new QoskCloud();

            chkCode = profileCode == "0"
                              ? 0
                              : Convert.ToInt32(profileCode);

            var gCount = cloudEntities.Profile.Count(t => t.ProfileCode == chkCode);

            if (gCount > 0)
            {
               chkCode = gCount;
            }
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }

         return chkCode;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetProfileCode(string curUserName)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from user in cloudEntities.User
                       where user.UserName == curUserName
                       select user.ProfileCode.Value).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetProfileName(int profileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from profile in cloudEntities.Profile
                       where profile.ProfileCode == profileCode
                       select profile.Name).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = "";
         char[] spComma = { ',' };
         char[] spColon = { ':' };
         if (inStr != null)
         {
            foreach (var onePair in from oneField in inStr.Replace("\"", "").Split(spComma)
                                    select oneField.Split(spColon)
                                    into onePair
                                    let id = onePair[0].Trim()
                                    where id.Length > 0
                                    where id.Contains(chkStr)
                                    select onePair)
            {
               return onePair[1].Trim();
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="groupId"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool EditGroup(string curUser, int groupId, string bodyData, bool isDeleted)
      {
         var status = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var existGroup = (from groups in cloudEntities.Group
                                 where groups.GroupID == groupId
                                 select groups).SingleOrDefault();

               if (existGroup != null)
               {
                  existGroup.PrimaryProfileCode = string.IsNullOrWhiteSpace(GetOneValue(bodyData, "ProfileName"))
                                                            ? 0
                                                            : Convert.ToInt32(GetOneValue(bodyData, "ProfileName"));
                  existGroup.Name = GetOneValue(bodyData, "GroupName");
                  existGroup.GrpTypeCode = GetOneValue(bodyData, "GroupType");
                  existGroup.ModifiedBy = curUser;
                  existGroup.IsDeleted = GetOneValue(bodyData, "Status") != "Enabled";

                  try
                  {
                     cloudEntities.SaveChanges();
                     status = true;
                  }
                  catch (Exception ex)
                  {
                     Utility.Logger.Error(ex);
                  }
               }
            }

         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetMax(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (cloudEntities.Group.Where(c => c.IsDeleted == isDeleted).Max(c => c.GroupID));
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetMin(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (cloudEntities.Group.Any(c => c.IsDeleted == isDeleted) ? cloudEntities.Group.Where(c => c.IsDeleted == isDeleted).Min(c => c.GroupID) : 0);
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> UpdateGroupDetail(long userId)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.Group
                       join profileGroupRel in cloudEntities.ProfileGroupRel on groups.GroupID equals profileGroupRel.ProfileGroupID
                       join profile in cloudEntities.Profile on profileGroupRel.ProfileCode equals profile.ProfileCode
                       join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                       where groups.IsDeleted == false
                       select new GroupEntity
                       {
                          GroupID = groups.GroupID,
                          GroupName = groups.Name,
                          GroupType = groups.GrpTypeCode,
                          PrimaryProfileCode = profileGroupRel.ProfileCode,
                          isDeleted = groups.IsDeleted,
                          Status = groups.IsDeleted
                                       ? "Retired"
                                       : "Enabled",
                          ProfileName = profile.Name,
                          Description = groupTypeDict.Description
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GroupEntity> GroupDetails_Hide()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from groups in cloudEntities.Group
                       join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                       where groups.IsDeleted == false
                       select new GroupEntity
                       {
                          GroupID = groups.GroupID,
                          GroupName = groups.Name,
                          GroupType = groups.GrpTypeCode,
                          PrimaryProfileCode = (int)groups.PrimaryProfileCode,
                          isDeleted = groups.IsDeleted,
                          Status = groups.IsDeleted
                                       ? "Retired"
                                       : "Enabled",
                          Description = groupTypeDict.Description
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Save profiles for each group to database.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="itemList"></param>
      /// <param name="groupId"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SetProfile(string curUser, List<GroupEntity> itemList, int groupId, bool isDeleted)
      {
         var status = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               try
               {
                  var deleteProfiles = cloudEntities.ProfileGrpRel.Where(c => c.GrpID == groupId && c.IsDeleted == isDeleted).ToList();

                  if (deleteProfiles.Count > 0)
                  {
                     cloudEntities.ProfileGrpRel.RemoveRange(deleteProfiles);
                  }

                  foreach (var profiles in itemList.Select(items => new ProfileGrpRel
                  {
                     Version = 1,
                     IsDeleted = isDeleted,
                     GrpID = groupId,
                     ProfileCode = items.PrimaryProfileCode,
                     CreatedBy = curUser,
                     EffectiveStartDate = DateTime.UtcNow,
                     EffectiveEndDate = DateTime.UtcNow.AddDays(90)
                  }))
                  {
                     cloudEntities.ProfileGrpRel.Add(profiles);
                  }

                  cloudEntities.SaveChanges();
                  status = true;
               }
               catch (Exception ex)
               {
                  Utility.Logger.Error(ex);
               }
            }

         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         var actionMenu = new ActionMenuModel
         {
            Name = "Select One",
            Links = new List<ActionMenuEntry>
            {
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 1",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 2",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 3",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 4",
                  Url = "#"
               }
            }
         };

         return actionMenu;
      }

      #endregion
   }
}
