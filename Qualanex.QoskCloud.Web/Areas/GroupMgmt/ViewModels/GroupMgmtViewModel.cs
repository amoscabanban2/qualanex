///-----------------------------------------------------------------------------------------------
/// <copyright file="GroupMgmtViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GroupMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.GroupMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GroupMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "ADMIN_GRPMGMT";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first group record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public GroupEntity SelectedEntity { get; set; } = new GroupEntity();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Group list for the Group detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GroupEntity> Entitys { get; set; } = new List<GroupEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the List of profile.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GroupEntity> ProfileLists { get; set; } = new List<GroupEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the List of group type dicts.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GroupEntity> GroupTypeLists { get; set; } = new List<GroupEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl GroupRelationControl { get; set; } = new RelationControl();

   }
}
