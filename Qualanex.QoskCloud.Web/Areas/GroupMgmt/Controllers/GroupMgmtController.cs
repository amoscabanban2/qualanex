﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="GroupMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GroupMgmt.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.GroupMgmt.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.GroupMgmt.Models;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_GRPMGMT")]
   public class GroupMgmtController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GroupMgmtView()
      {
         this.ModelState.Clear();

         var viewModel = new GroupMgmtViewModel
         {
            Entitys = GroupMgmtModel.GetGroupDetails(false),
            ActionMenu = GroupMgmtModel.GetActionMenu(),
            ProfileLists = GroupMgmtModel.GetProfileList(false),
            GroupTypeLists = GroupMgmtModel.GetGroupTypeList(false)
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupDetails"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateEditDetail(string groupDetails)
      {
         this.ModelState.Clear();

         var viewModel = new GroupMgmtViewModel
         {
            SelectedEntity = System.Web.Helpers.Json.Decode<GroupEntity>(groupDetails),
            ProfileLists = GroupMgmtModel.GetProfileList(false),
            GroupTypeLists = GroupMgmtModel.GetGroupTypeList(false)
         };

         return this.PartialView("_GroupMgmtForm", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupDetails"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateGroupDetail(string groupDetails)
      {
         this.ModelState.Clear();

         var viewModel = new GroupMgmtViewModel
         {
            SelectedEntity = System.Web.Helpers.Json.Decode<GroupEntity>(groupDetails),
            ProfileLists = GroupMgmtModel.GetProfileList(false),
            GroupTypeLists = GroupMgmtModel.GetGroupTypeList(false)
         };

         return this.PartialView("_GroupMgmtForm", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new GroupMgmtViewModel
         {
            Entitys = GroupMgmtModel.GetGroupDetails(isDeleted),
            ActionMenu = GroupMgmtModel.GetActionMenu(),
            ProfileLists = GroupMgmtModel.GetProfileList(false),
            GroupTypeLists = GroupMgmtModel.GetGroupTypeList(false),
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.PartialView("_GroupMgmtSummary", viewModel);
      }




      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }






      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupId"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveGroupDetail(int groupId, string bodyData, bool isDeleted)
      {
         var curUser = this.HttpContext.User.Identity.Name;
         var max = groupId;
         var checkValue = GroupMgmtModel.GetOneValue(bodyData, "GroupName");
         var checkResult = GroupMgmtModel.CheckGroupName(checkValue, groupId);

         if (!string.IsNullOrWhiteSpace(checkResult))
         {
            return this.Json(new
            {
               Success = false,
               Error = true,
               max = 0
            }, JsonRequestBehavior.AllowGet);
         }

         checkValue = GroupMgmtModel.GetOneValue(bodyData, "ProfileName");

         if (!string.IsNullOrWhiteSpace(checkValue))
         {
            var pCode = GroupMgmtModel.CheckProfileCode(checkValue);

            if (pCode == 0)
            {
               var errorMessage = "The profile code '" + checkValue +
                  "' does not exist. Please try another profile code.";

               if (!string.IsNullOrWhiteSpace(errorMessage))
               {
                  return this.Json(new
                  {
                     Success = false,
                     Error = errorMessage,
                     max = groupId
                  }, JsonRequestBehavior.AllowGet);
               }
            }
         }

         var blnCheck = groupId == 0
                            ? GroupMgmtModel.NewGroup(curUser, bodyData, isDeleted)
                            : GroupMgmtModel.EditGroup(curUser, groupId, bodyData, isDeleted);

         if (GroupMgmtModel.GetOneValue(bodyData, "Status") == "Enabled")
         {
            if (groupId == 0)
            {
               max = GroupMgmtModel.GetMax(false);
            }
         }
         else
         {
            if (groupId > 0)
            {
               if (!isDeleted)
               {
                  max = GroupMgmtModel.GetMin(false);
               }
            }
            else
            {
               max = isDeleted
                        ? GroupMgmtModel.GetMax(true)
                        : GroupMgmtModel.GetMin(false);
            }
         }

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck,
            max = max
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Group Type exist in DB or not.
      /// </summary>
      /// <param name="groupId"></param>
      /// <param name="groupName"></param>
      /// <returns>
      ///   Error message if Group Type does not exist in the DB, otherwise an
      ///   empty string.
      /// </returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult Check_With_DB(int groupId, string groupName)
      {
         var input = "";

         try
         {
            var chkResult = GroupMgmtModel.CheckGroupName(groupName, groupId);

            if (!string.IsNullOrWhiteSpace(chkResult))
            {
               var errorMessage = "The group name '" + groupName +
                  "' has been taken. Please try another group name.";

               input = "GroupName";

               if (!string.IsNullOrWhiteSpace(errorMessage))
               {
                  return this.Json(new
                  {
                     Success = false,
                     Error = errorMessage,
                     Input = input
                  }, JsonRequestBehavior.AllowGet);
               }
            }

            return this.Json(new
            {
               Success = true,
               Error = "",
               Input = ""
            }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            return this.Json(new
            {
               Success = false,
               Error = ex.Message,
               Input = input
            }, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Profile entities by group id.
      /// </summary>
      /// <param name="groupId"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ProfileEntity(int groupId, bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new GroupMgmtViewModel
         {
            GroupRelationControl = new Model.RelationControl
            {
               Options = new Model.RelationControlOptions
               {
                  AddMethod = this.Url.Action(nameof(GroupEntity)),
                  ShowRowControls = false,
                  HiddenProperties =
                  {
                     nameof(GroupEntity.Description),nameof(GroupEntity.GroupCode),
                     nameof(GroupEntity.GroupID), nameof(GroupEntity.GroupName),
                     nameof(GroupEntity.GroupType),nameof(GroupEntity.IsDisposed),
                     nameof(GroupEntity.PrimaryProfileCode),nameof(GroupEntity.Status),
                     nameof(GroupEntity.Version),nameof(GroupEntity.isDeleted)
                  },
                  UpdateMethod = "console.log('not yet implemented');",
                  InitialReadOnly = true,
                  RowProperty = new GroupEntity(),
                  CustomControls = new List<Model.CustomControl>
                  {
                     new Model.CustomControl
                     {
                        EditView = "_GroupCustomColumn",
                        AddView = "_GroupCustomColumn",
                        ReadOnlyView = ""
                     }
                  }
               },
               Rows = GroupMgmtModel.GetProfileList(groupId, isDeleted)
            }
         };

         return this.PartialView("_GroupMgmtAssociation", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   save profile for each groups.
      /// </summary>
      /// <param name="profileEntity"></param>
      /// <param name="groupId"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveToProfile(string profileEntity, int groupId, bool isDeleted)
      {
         var blnCheck = GroupMgmtModel.SetProfile(
            this.HttpContext.User.Identity.Name
            , System.Web.Helpers.Json.Decode<List<GroupEntity>>(profileEntity)
            , groupId
            , isDeleted);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   GetProfileName by input.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetProfileName(string strData, int keyPressValue, string browser)
      {
         var profileEntity = string.IsNullOrWhiteSpace(strData)
                              ? new List<GroupEntity>()
                              : GroupMgmtModel.GetProfileList(false, strData, browser);

         var jsonResult = this.Json(new { profileEntity = profileEntity, keyPressValue = keyPressValue }, JsonRequestBehavior.AllowGet);
         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

   }
}

