///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UserMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.UserMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UserMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "ADMIN_USER";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Policy record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UserEntity SelectedEntity { get; set; } = new UserEntity();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Policy list for the Policy detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<UserEntity> Entitys { get; set; } = new List<UserEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate all profiles for users to be assigned to
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<UserEntity> ProfileList { get; set; } = new List<UserEntity>();
   }
}