﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserMgmtModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UserMgmt.Models
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Linq;
   using System.Runtime.Caching;
   using System.Text.RegularExpressions;

   using Microsoft.Ajax.Utilities;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   using System.Web.Mvc;

   using Microsoft.AspNet.Identity;
   using Microsoft.AspNet.Identity.Owin;

   using Logger = Qualanex.Qosk.Library.Common.Logger.Logger;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UserMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public UserMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~UserMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="deletedCode"></param>  0=false, 1=true, 2=all, -1=don't bother with the 'deleted' logic
      /// <param name="userId"></param> -1=none, else use passed in UserID
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> RetrieveAccountDetails(int deletedCode, long userId = -1)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               List<UserEntity> resultSet = (from user in cloudEntities.User
                                             join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                                             join userTypeDict in cloudEntities.UserTypeDict on user.UserTypeCode equals userTypeDict.Code
                                             where (deletedCode.Equals(0) && user.IsDeleted.Equals(false))       // If deleted flag is false, only bring back records not marked false.
                                                   || (deletedCode.Equals(1) && user.IsDeleted.Equals(true))   // If deleted flag is true,  only bring back records marked false.
                                                   || (deletedCode.Equals(2))                                  // If deleted flag is set to all, bring back all, don't filter on any other columns of data.
                                                   || (deletedCode.Equals(-1) && userId > 0 && user.UserID.Equals(userId))   // If deleted flag is set to bypass, and there is a valid positive UserID, bring back all records associated with that UserID.
                                             select new UserEntity()
                                             {
                                                FirstName = user.FirstName,
                                                LastName = user.LastName,
                                                UserName = user.UserName,
                                                Email = user.Email,
                                                VoiceNumber = user.VoiceNumber,
                                                PhoneNumber = user.PhoneNumber,
                                                Extension = user.Extension,
                                                SMSNumber = user.SMSNumber,
                                                FaxNumber = user.FaxNumber,
                                                ProfileCode = user.ProfileCode.ToString(),
                                                ProfileName = profile.Name,
                                                UserID = user.UserID,
                                                AcctStatus = "",
                                                IsDeleted = user.IsDeleted,
                                                IsEnabled = user.IsEnabled,
                                                IsLocked = user.IsLocked
                                             }).ToList();

               return resultSet;
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> GetEntitys(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               List<UserEntity> resultSet = new List<UserEntity>();
               // If isDeleted parameter is true,  bring back all records. (All records = code of 2)
               // If isDeleted parameter is false, only bring back records that have the Deleted flag set to false. (False flag = code of 0)
               resultSet = RetrieveAccountDetails((isDeleted ? 2 : 0));

               // Format the data that needs it for this process.
               foreach (UserEntity x in resultSet)
               {
                  x.PhoneNumber = FormatPhoneNumber(x.PhoneNumber);
                  x.SMSNumber = FormatPhoneNumber(x.SMSNumber);
                  x.FaxNumber = FormatPhoneNumber(x.FaxNumber);
                  x.AcctStatus = GetAccountStatus(x.IsDeleted, x.IsEnabled, x.IsLocked);
               }
               return resultSet;
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool GetUserName(string userName)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         try
         {
            if (!cloudEntities.User.Any(u => u.UserName == userName))
            {
               return true;
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <param name="enteredUserName"></param>
      /// <param name="userID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> AddEditUserAccount(string curUser, string bodyData, bool isDeleted, bool enteredUserName, long userID = -1)
      {
         //bool addMode = (userID <= 0);   // Add mode if UserID <= 0, else update given data for this userID.


         //MD5Encryption objEncrypt = new MD5Encryption();
         //string status = GetOneValue(bodyData, "ID_STATUS");

         //using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   try
         //   {
         //      // If this UserID doesn't exist in the User table, we need to add a new user with this info, else that user exits so we edit its info.
         //      // Needed just in case an invalid (but still positive integer) value was passed in.
         //      if (userID > 0)
         //      {
         //         addMode = (((from user in cloudEntities.User where user.UserID == userID select user).SingleOrDefault()) == null);
         //      }

         //      // If we're adding a record, add a blank record and make note of the UserID that gets created.
         //      if (addMode)
         //      {
         //         userID = GetNewUserID();
         //         var newUser = new User
         //         {
         //            AccessFailedCount = 0,
         //            PrimaryBiometric = null,
         //            SecondaryBiometric = null,
         //            CreatedBy = curUser,
         //            CreatedDate = DateTime.UtcNow,
         //            EffectiveEndDate = DateTime.UtcNow.AddDays(90),
         //            EffectiveStartDate = DateTime.UtcNow.AddDays(1),
         //            EmailConfirmed = false,
         //            Id = Guid.NewGuid().ToString().ToUpper(),
         //            IsTemporaryPassword = true,
         //            Password = objEncrypt.Encrypt("password"),
         //            PasswordHash = objEncrypt.GetHashCode().ToString(),
         //            SecurityStamp = "",
         //            UserID = userID,
         //            PhoneNumberConfirmed = false,
         //            TwoFactorEnabled = false,
         //            LockoutEndDateUtc = null,
         //            LockoutEnabled = true,
         //            Version = 0,

         //            // EF had issues with certain values being NULL when a new record was created, so rather than put in those
         //            // various fields with legit data, I put in all the "Edit/Update" fields to make maintenance easier.
         //            // Just remember to keep this section updated with the below "else" logic for the "Edit Mode" section.
         //            Email = GetOneValue(bodyData, "ID_EMAIL"),
         //            IsEnabled = GetStatusFieldValue("isEnabled", status),
         //            IsDeleted = GetStatusFieldValue("isDeleted", status),
         //            IsLocked = GetStatusFieldValue("isLocked", status),
         //            FaxNumber = GetOneValue(bodyData, "ID_FAXNO"),
         //            FirstName = GetOneValue(bodyData, "ID_FIRSTNAME"),
         //            LastName = GetOneValue(bodyData, "ID_LASTNAME"),
         //            ModifiedBy = curUser,
         //            ModifiedDate = DateTime.UtcNow,
         //            VoiceNumber = GetOneValue(bodyData, "ID_VOICENO"),
         //            Extension = GetOneValue(bodyData, "ID_EXTENTION"),
         //            SMSNumber = GetOneValue(bodyData, "ID_SMSNO"),
         //            ProfileCode = GetProfileCode(curUser),
         //            UserName = GetUserName(GetOneValue(bodyData, "ID_USERNAME"), enteredUserName),
         //            UserTypeCode = GetUserTypeCode(curUser),
         //            PhoneNumber = GetOneValue(bodyData, "ID_VOICENO"),
         //         };

         //         userID = newUser.UserID;       // If we added this record, replace passed in value with this value, if not in edit mode, passed in value won't be altered.
         //         cloudEntities.User.Add(newUser);
         //         cloudEntities.SaveChanges();
         //      }

         //      // else we're in edit/update mode, get the userID and update it's values
         //      else
         //      {   // Get the record for this UserID
         //         var existUser = (from user in cloudEntities.User where user.UserID == userID select user).SingleOrDefault();

         //         if (existUser != null)
         //         {
         //            // Do this logic for either adding or editing a user.
         //            ////existUser.ProfileCode = GetProfileCode(curUser);
         //            //existUser.UserName = GetUserName(GetOneValue(bodyData, "ID_USERNAME"), enteredUserName);
         //            ////existUser.UserTypeCode = GetUserTypeCode(curUser);
         //            //existUser.UserID = userID;
         //            //existUser.UserName = GetOneValue(bodyData, "ID_USERNAME");

         //            existUser.FirstName = GetOneValue(bodyData, "ID_FIRSTNAME");
         //            existUser.LastName = GetOneValue(bodyData, "ID_LASTNAME");
         //            existUser.FaxNumber = GetOneValue(bodyData, "ID_FAXNO");
         //            existUser.Email = GetOneValue(bodyData, "ID_EMAIL");
         //            existUser.VoiceNumber = GetOneValue(bodyData, "ID_VOICENO");
         //            existUser.Extension = GetOneValue(bodyData, "ID_EXTENTION");
         //            existUser.SMSNumber = GetOneValue(bodyData, "ID_SMSNO");
         //            existUser.ModifiedBy = curUser;
         //            existUser.ModifiedDate = DateTime.UtcNow;
         //            existUser.IsEnabled = GetStatusFieldValue("isEnabled", status);
         //            existUser.IsDeleted = GetStatusFieldValue("isDeleted", status);
         //            existUser.IsLocked = GetStatusFieldValue("isLocked", status);

         //            cloudEntities.SaveChanges();
         //         };
         //      }
         //   }
         //   catch (Exception ex)
         //   {
         //      Log.Write(LogMask.Error, ex.ToString());
         //   }
         //}
         return GetEntitys(isDeleted);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <param name="enteredUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> NewUserAccount(string curUser, string bodyData, bool isDeleted, bool enteredUserName)
      {
         try
         {
            List<UserEntity> result = AddEditUserAccount(curUser, bodyData, isDeleted, enteredUserName);
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return GetEntitys(isDeleted);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get next available user ID from the User table
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static long GetNewUserID()
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();
         return cloudEntities.User.Max(t => t.UserID) + 1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetUserTypeCode(string curUserName)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from user in cloudEntities.User
                       where user.UserName == curUserName
                       select user.UserTypeCode).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get isEnabled, isDeleted, or isLocked value from Status on view
      /// </summary>
      /// <param name="Status"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool GetStatusFieldValue(string checkField, string Status)
      {
         switch (checkField)
         {
            case "isEnabled":
               return Status == "Enabled" || Status == "Locked";
            case "isDeleted":
               return Status == "Retired";
            case "isLocked":
               return Status == "Locked";
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetProfileCode(string curUserName)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from user in cloudEntities.User
                       where user.UserName == curUserName
                       select user.ProfileCode.Value).SingleOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return -1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = "";
         char[] SP_Comma = { ',' };
         char[] SP_Colon = { ':' };

         foreach (var oneField in inStr.Replace("\"", "").Split(SP_Comma))
         {
            var onePair = oneField.Split(SP_Colon);
            var id = onePair[0].Trim();

            if (id.Length > 0)
            {
               var value = onePair[1].Trim();

               if (id == chkStr)
               {
                  if (id == "ID_VOICENO" || id == "ID_SMSNO" || id == "ID_FAXNO") value = UnformattedPhone(value);
                  return value;
               }
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves a list of all profiles
      /// Results are cached.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> GetProfiles(string strData = null)
      {
         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               //var lstAllData = (from profile in entities.Profile
               //                  where !profile.IsDeleted
               //                  select new UserEntity()
               //                  {
               //                     FullProfileName = profile.Name.Trim() + " [" + profile.ProfileCode.ToString() + "] " + profile.City.Trim() + ", " + profile.State.Trim(),
               //                     ListProfileCode = profile.ProfileCode
               //                  }).OrderBy(r => r).ToList();

               var sqlquery = "SELECT Name + ' [' + CAST(ProfileCode AS varchar) + '] ' + City + ', ' + State AS FullProfileName, ProfileCode AS ListProfileCode FROM Profile WHERE IsDeleted = 0 ORDER BY FullProfileName";

               var lstAllData = entities.Database.SqlQuery<UserEntity>(sqlquery).ToList();

               var lstItems = new List<UserEntity>();

               if (!string.IsNullOrWhiteSpace(strData))
               {
                  foreach (var items in lstAllData)
                  {
                     var reslt = Regex.Replace(items.FullProfileName, @"[^a-zA-Z0-9]", string.Empty);

                     if (items.FullProfileName.ToLower().Contains(strData.ToLower()))
                     {
                        lstItems.Add(items);
                     }
                  }
               }
               else
               {
                  lstItems.AddRange(lstAllData);
               }

               return lstItems;
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Given a formatted phone number, remove the formatting characters and return just the phone number characters.
      /// </summary>
      /// <param name="formattedPhone"></param>>
      /// <returns></returns>
      ///****************************************************************************
      public static string UnformattedPhone(string formattedPhone)
      {
         return formattedPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim();
      }

      ///****************************************************************************
      /// <summary>
      ///   Given an unformatted phone number, add the standard U.S. formatting
      ///     structure to the given characters only if it's 10 characters long.
      /// </summary>
      /// <param name="UnformattedPhoneNumber"></param>>
      /// <returns></returns>
      ///****************************************************************************
      public static string FormatPhoneNumber(string UnformattedPhoneNumber)
      {
         // Check the data to make sure it's in the correct format.
         UnformattedPhoneNumber = (UnformattedPhoneNumber.IsNullOrWhiteSpace() ? "" : UnformattedPhoneNumber.Trim());
         return UnformattedPhoneNumber.Length != 10 ? UnformattedPhoneNumber : "(" + UnformattedPhoneNumber.Substring(0, 3) + ") " + UnformattedPhoneNumber.Substring(3, 3) + "-" + UnformattedPhoneNumber.Substring(6, 4);
      }


      /////****************************************************************************
      ///// <summary>
      /////   Writes the error information out to the Error log
      ///// </summary>
      ///// <returns></returns>
      /////****************************************************************************
      //public static void WriteToErrorLog(LogMask errLogMask, Exception ex)
      //{
      //    Log.Write(errLogMask, ex.ToString());
      //}

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="aum"></param>
      /// <param name="password"></param>
      /// <returns>true - valid password (up to standards)</returns>
      /// <returns>false - invalid password (missing at least one of the criteria)</returns>
      ///****************************************************************************
      public static bool CheckPassword(ApplicationUserManager aum, string password)
      {
         try
         {
            var x = aum.PasswordValidator.ValidateAsync(password).Result;
            return x.Succeeded;
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check if the email entered is associated with another user
      /// </summary>
      /// <param name="email"></param>
      /// <param name="username"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckEmail(string email, string username)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         try
         {
            if (!cloudEntities.User.Any(u => u.Email == email && (username == "" || u.UserName != username)))
            {
               return true;
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="userId"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> EditUserAccount(string curUser, long userId, string bodyData, bool isDeleted)
      {
         try
         {
            //List<UserEntity> result = AddEditUserAccount(curUser, bodyData, isDeleted, false, userId);
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }
         return GetEntitys(isDeleted);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> UpdateAccountDetail(long userId)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return RetrieveAccountDetails(-1, userId);
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UserEntity> AccountDetails_Hide()
      {
         try
         {
            return RetrieveAccountDetails(1);
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }
         return null;
      }


      ///****************************************************************************
      /// <summary>
      ///   Get the Account Status based on the Deleted/Enabled/Locked flags.
      /// </summary>
      /// <param name="isDeleted"></param>>
      /// <param name="isEnabled"></param>>
      /// <param name="isLocked"></param>>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetAccountStatus(bool isDeleted, bool isEnabled, bool isLocked)
      {
         return (isDeleted ? "Retired" : (isEnabled ? (isLocked ? "Locked" : "Enabled") : "Disabled"));
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         var actionMenu = new ActionMenuModel
         {
            Name = "Select One",
            Links = new List<ActionMenuEntry>
            {
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 1",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 2",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 3",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 4",
                  Url = "#"
               }
            }
         };

         return actionMenu;
      }

      #endregion
   }
}
