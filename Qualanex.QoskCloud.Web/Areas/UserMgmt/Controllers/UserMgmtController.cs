﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UserMgmt.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Threading.Tasks;
   using System.Web;
   using System.Web.Mvc;

   using Microsoft.AspNet.Identity;
   using Microsoft.AspNet.Identity.Owin;

   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Areas.UserMgmt.Models;
   using Qualanex.QoskCloud.Web.Areas.UserMgmt.ViewModels;

   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_USER")]
   public class UserMgmtController : Controller
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public async Task<ActionResult> UserMgmtView()
      {
         this.ModelState.Clear();

         var viewModel = new UserMgmtViewModel
         {
            Entitys = UserMgmtModel.GetEntitys(true),
            ActionMenu = UserMgmtModel.GetActionMenu(),
            ProfileList = UserMgmtModel.GetProfiles()
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         //var aum = this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

         //var userX = await aum.FindByNameAsync("ghernandez");
         ////var userY = await aum.FindByEmailAsync("dezell@qualanex.com");

         ////var Id = "37d120db-050d-4b6c-a5a1-fcea45cc6d5c";
         //var Id = userX.Id; //user.Id;

         //var user2 = await aum.FindByIdAsync(Id);
         //var logins = user2.Logins;
         //var rolesForUser = await aum.GetRolesAsync(Id);

         //using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   using (var dbTran = cloudEntities.Database.BeginTransaction())
         //   {

         //      foreach (var login in logins.ToList())
         //      {
         //         await aum.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
         //      }

         //      if (rolesForUser.Count() > 0)
         //      {
         //         foreach (var item in rolesForUser.ToList())
         //         {
         //            // item should be the name of the role
         //            var result3 = await aum.RemoveFromRoleAsync(user2.Id, item);
         //         }
         //      }

         //      await aum.DeleteAsync(user2);
         //      dbTran.Commit();
         //   }
         //}

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new UserMgmtViewModel
         {
            Entitys = UserMgmtModel.GetEntitys(isDeleted),
            ActionMenu = UserMgmtModel.GetActionMenu()
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.PartialView("_UserMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityClick(string entity)
      {
         this.ModelState.Clear();

         return this.PartialView("_UserMgmtForm", System.Web.Helpers.Json.Decode<UserEntity>(entity));
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userID"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <param name="enteredUserName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public async Task<ActionResult> SaveAccountDetail(long userID, string bodyData, bool isDeleted, bool enteredUserName)
      {
         var viewModel = new UserMgmtViewModel();
         var curUser = this.HttpContext.User.Identity.Name;

         var aum = this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

         //#############################################################################################
         //#############################################################################################
         //var userX = await aum.FindByNameAsync(GetOneValue(bodyData, "UserName"));
         //var Id = userX.Id;

         //var user2 = await aum.FindByIdAsync(Id);
         //var logins = user2.Logins;
         //var rolesForUser = await aum.GetRolesAsync(Id);

         //using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   using (var dbTran = cloudEntities.Database.BeginTransaction())
         //   {

         //      foreach (var login in logins.ToList())
         //      {
         //         await aum.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
         //      }

         //      if (rolesForUser.Count() > 0)
         //      {
         //         foreach (var item in rolesForUser.ToList())
         //         {
         //            // item should be the name of the role
         //            var result3 = await aum.RemoveFromRoleAsync(user2.Id, item);
         //         }
         //      }

         //      await aum.DeleteAsync(user2);
         //      dbTran.Commit();
         //   }
         //}


         //#############################################################################################
         //#############################################################################################
         var objEncrypt = new MD5Encryption();
         var pwd = GetOneValue(bodyData, "Password");
         var status = GetOneValue(bodyData, "AcctStatus");
         var profileCode = 0;
         int.TryParse(GetOneValue(bodyData, "ProfileCode"), out profileCode);
         var result = new IdentityResult();

         ApplicationUser user;

         //userID == 0 means we're registering a new user
         //any other ID is an already-registered user, and should just be an update as opposed to a save
         if (userID == 0)
         {
            user = new ApplicationUser
            {
               UserID = UserMgmtModel.GetNewUserID(),//Convert.ToInt64(GetOneValue(bodyData, "UserID")),
               UserName = GetOneValue(bodyData, "UserName"),
               Email = GetOneValue(bodyData, "Email"),
               PhoneNumber = GetOneValue(bodyData, "PhoneNumber"),
               FirstName = GetOneValue(bodyData, "FirstName"),
               LastName = GetOneValue(bodyData, "LastName"),
               CreatedBy = "dev",
               CreatedDate = DateTime.UtcNow,
               Password = objEncrypt.EncryptPassword(pwd),
               FaxNumber = GetOneValue(bodyData, "FaxNumber"),
               Extension = GetOneValue(bodyData, "Extension"),
               SmsNumber = GetOneValue(bodyData, "SMSNumber"),
               UserTypeCode = "CORP",
               ProfileCode = profileCode,
               ModifiedDate = DateTime.UtcNow,
               IsEnabled = UserMgmtModel.GetStatusFieldValue("isEnabled", status),
               IsLocked = UserMgmtModel.GetStatusFieldValue("isLocked", status),
               IsDeleted = UserMgmtModel.GetStatusFieldValue("isDeleted", status)
            };

            result = await aum.CreateAsync(user, pwd);
         }
         else
         {
            user = await aum.FindByNameAsync(GetOneValue(bodyData, "UserName"));

            //only update password if user enters
            //by default, we do not display the password on the form, so it's left blank intentionally upon an edit
            if (!string.IsNullOrWhiteSpace(pwd))
            {
               var x = aum.PasswordValidator.ValidateAsync(pwd).Result;

               user.Password = objEncrypt.EncryptPassword(pwd);
               user.PasswordHash = aum.PasswordHasher.HashPassword(pwd);
            }

            //user.UserID = Convert.ToInt64(GetOneValue(bodyData, "UserID"));
            //user.UserName = GetOneValue(bodyData, "UserName");
            user.Email = GetOneValue(bodyData, "Email");
            user.PhoneNumber = GetOneValue(bodyData, "PhoneNumber");
            user.FirstName = GetOneValue(bodyData, "FirstName");
            user.LastName = GetOneValue(bodyData, "LastName");
            user.FaxNumber = GetOneValue(bodyData, "FaxNumber");
            user.Extension = GetOneValue(bodyData, "Extension");
            user.SmsNumber = GetOneValue(bodyData, "SMSNumber");
            user.UserTypeCode = "CORP";
            user.ProfileCode = profileCode;
            user.ModifiedDate = DateTime.UtcNow;
            user.IsEnabled = UserMgmtModel.GetStatusFieldValue("isEnabled", status);
            user.IsLocked = UserMgmtModel.GetStatusFieldValue("isLocked", status);
            user.IsDeleted = UserMgmtModel.GetStatusFieldValue("isDeleted", status);

            result = await aum.UpdateAsync(user);
         }

         if (result.Succeeded)
         {
            var idResultA = aum.AddToRole(user.Id, "Admin");
            var idResultB = aum.AddToRole(user.Id, "QLUser");
         }

         //viewModel.Entitys = userID == 0
         //    ? UserMgmtModel.NewUserAccount(curUser, bodyData, isDeleted, enteredUserName)
         //    : UserMgmtModel.EditUserAccount(curUser, userID, bodyData, isDeleted);

         viewModel.ActionMenu = UserMgmtModel.GetActionMenu();

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         //return this.PartialView("_UserMgmtSummary", viewModel);

         var errorListStr = result.Errors.ToList();
         string[] errorLists = null;

         if (errorListStr.Count > 0)
         {
            errorLists = errorListStr[0].Split('.');
         }

         var jsonResult = this.Json(new
         {
            success = result.Succeeded,
            error = !result.Succeeded,
            errorList = errorLists
            //Staging = (!string.IsNullOrWhiteSpace(blnCheck.TrackingID) || viewModel.ValidateTrkNoAndRA.Count > 0)
            //               ? this.ConvertViewToString("_UserMgmtSummary", viewModel)
            //               : ""
         }, JsonRequestBehavior.AllowGet);

         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check to verify that UserName does not exist in DB.
      /// </summary>
      /// <param name="username"></param>
      /// <returns>Error message, otherwise empty string</returns>
      ///****************************************************************************
      public ActionResult UserCheck(string username)
      {
         var validUserName = UserMgmtModel.GetUserName(username);
         var jsonResult = new { success = validUserName, error = !validUserName };
         return this.Json(jsonResult, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check to verify the password entered is valid and up-to-standards
      /// </summary>
      /// <param name="password"></param>
      /// <returns>Error message, otherwise empty string</returns>
      ///****************************************************************************
      public ActionResult PasswordCheck(string password)
      {
         var aum = this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
         var validPassword = UserMgmtModel.CheckPassword(aum, password);
         var jsonResult = new { success = validPassword, error = !validPassword };
         return this.Json(jsonResult, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check if the email address entered is already associated with a user
      /// </summary>
      /// <param name="email"></param>
      /// <param name="username"></param>
      /// <returns>Error message, otherwise empty string</returns>
      ///****************************************************************************
      public ActionResult EmailCheck(string email, string username)
      {
         var validEmail = UserMgmtModel.CheckEmail(email, username);
         var jsonResult = new { success = validEmail, error = !validEmail };
         return this.Json(jsonResult, JsonRequestBehavior.AllowGet);
      }

      private string ConvertViewToString(string viewName, object model)
      {
         this.ViewData.Model = model;
         using (StringWriter writer = new StringWriter())
         {
            ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
            ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, this.ViewData, new TempDataDictionary(), writer);
            vResult.View.Render(vContext, writer);
            return writer.ToString();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = "";
         char[] SP_Comma = { ',' };
         char[] SP_Colon = { ':' };

         foreach (var oneField in inStr.Replace("\"", "").Split(SP_Comma))
         {
            var onePair = oneField.Split(SP_Colon);
            var id = onePair[0].Trim();

            if (id.Length > 0)
            {
               var value = onePair[1].Trim();

               if (id == chkStr)
               {
                  return value;
               }
            }
         }

         return outValue;
      }




   }
}
