﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserMgmtAreaRegistration.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UserMgmt
{
    using System.Web.Mvc;

    ///============================================================================
    /// <summary>
    ///   Summary not available.
    /// </summary>
    ///----------------------------------------------------------------------------
    public class UserMgmtAreaRegistration : AreaRegistration
    {
        ///////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///   Summary not available.
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////
        public override string AreaName => "UserMgmt";

        ///****************************************************************************
        /// <summary>
        ///   Summary not available.
        /// </summary>
        /// <param name="context"></param>
        ///****************************************************************************
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UserMgmt_default",
                "UserMgmt/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
