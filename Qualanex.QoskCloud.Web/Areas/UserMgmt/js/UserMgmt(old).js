﻿//*****************************************************************************
//*
//* UserMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*     Summary not available.
//*
//* Notes:
//*     None.
//*
//*****************************************************************************

//=============================================================================
// Global definitions
//-----------------------------------------------------------------------------
var editMode;
var textChanged;
var userID;
var table;
var tableDG;
var enteredUsername = false;
var validUser;
var active = "";
var valid = false;

//*****************************************************************************
//*
//* Summary:
//*   Page load functions.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
//$(document).ready(function () {
//   table = $("#searchGrid").DataTable();
//   initialState();
//});

$(document).ready(function()
{
   table = window.dtHeaders("#searchGrid", '"order": [[1, "asc"]]', [], []);
   tableDG = $("#searchGrid").DataTable();

   tableDG.scrollable = true;
   initialState();
});


//*****************************************************************************
//*
//* Summary:
//*   Action on Keypress on VoiceNumber, SMSNO, and FaxNo
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("keypress", "#ID_VOICENO, #ID_EXTENTION, #ID_SMSNO, #ID_FAXNO", function(e)
{
   validateKey(ECS_NUMERIC, e);
});

//*****************************************************************************
//*
//* Summary:
//*   If the any key is pressed in the UserName field (and we're in edit
//*   mode), set the enteredUserName flag to true which will later cause the
//*   validation logic to run on this value.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   N/A
//*
//*****************************************************************************
$(document).on("keypress", "#ID_USERNAME", function(e)
{
   //enteredUsername = true;
   if (editMode)
   {
      enteredUsername = true;
   }
});

//*****************************************************************************
//*
//* Summary:
//*   Action on Paste on voicenumbe, SMSNO, and FaxNo
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("paste", "#ID_VOICENO, #ID_SMSNO, #ID_FAXNO", function(e)
{
   setTimeout(function()
   {
      var pasteData = e.originalEvent.clipboardData.getData("text");

      pasteData = pasteData.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
      pasteData = keepOnlyNumbers(pasteData);

      if ($.isNumeric(pasteData)) { $(this).val(pasteData.substring(0, 10)); }
   }, 10);
});

//*****************************************************************************
//*
//* Summary:
//*   Logic that executes when the focus goes to either VoiceNumber, SMSNO or FaxNo field
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   None
//*
//*****************************************************************************
$(document).on("focus", "#ID_VOICENO, #ID_SMSNO, #ID_FAXNO", function(e)
{
   var str = keepOnlyNumbers($(this).val());
   setTimeout(function()
   {
      if (str) { $(this).val(str.substring(0, 10)); }
   }, 10);
});


//*****************************************************************************
//*
//* Summary:
//*   Set voicenumber, SMS and fax with format (xxx) xxx-xxx
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("focusout", "#ID_VOICENO, #ID_SMSNO, #ID_FAXNO", function(e)
{
   var unformattedNumber = keepOnlyNumbers($(this).val());                 // Get the phone number and strip it to just the numbers.
   var formattedNumber = FormatStringAsPhoneNumber(unformattedNumber);     // If the phone number is of the appropriate length, format it, else keep it unformatted.

   $(this).val(formattedNumber);                                           // Display the number, formatted or not.
   if ($(this).attr("id") === "ID_VOICENO")
   {
      updateDataTable(unformattedNumber, $(this).attr("id"), userID);      // Update the Data Table with the unformatted number
   }
});

//*****************************************************************************
//*
//* Summary:
//*   Summary not available.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("input", ".inputEx", function()
{
   var id = $(this).attr("id");
   var value = $(this).val();

   updateDataTable(value, id, userID);
});

//*****************************************************************************
//*
//* Summary:
//*   Summary not available.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   if username is empty then try to get username from lastname and first name.
//*
//*****************************************************************************
$(document).on("focusout", "#ID_USERNAME, #ID_FIRSTNAME, #ID_LASTNAME", function()
{
   //    if (!editMode) {
   if ($(this).messageBoxStatus())
   {
      return false;
   }
   var lname = $("#ID_LASTNAME").val().trim();
   if ($("#ID_USERNAME").val().trim() === "" && lname !== "") { $("#ID_USERNAME").val(getUserName(lname, "ID_LASTNAME")); }
   var $url = "SaveAccountDetail";

   var bodyData = getBodyData();

   //if (!editMode)
   if (editMode)
   {              // Add Mode check username
      $.ajax({
         type: "GET",
         url: $url.replace("SaveAccountDetail", "UserCheck"),
         data: "bodyData=" + bodyData + "&enteredUserName=" + enteredUsername,
         success: function(data)
         {
            if (data.length > 0)
            {
               var messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + data + "</h3>"
               }];
               var btnObjects = [{
                  "id": "btn_continue",
                  "name": "Continue",
                  "function": "onclick='btnValidateUsers([{" + bodyData + "}])'",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }];

               var titleobjects = [{ "title": "Invalid Entry" }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            } else
            {
               validUser = true;
            }
         },
         error: function(data)
         {
            showError("The controller call Fails on user name checking request.");
         }
      });
   }
   //   }
});

function btnValidateUsers(bodyData)
{
   $(this).closeMessageBox();
   resetNewModeValue(bodyData[0]);
   $("#ID_USERNAME").focus();
   textChanged = false;
   validUser = false;
}
//*****************************************************************************
//*
//* Summary:
//*   Formats the passed in value in standard US phone number format if it has only 10 consecutive digits.
//*
//* Parameters:
//*   inValue - Unformatted phone number value
//*
//* Returns:
//*   Formatted phone number if input value is correct, otherwise returns original passed in value minus any other non-numeric characters.
//*
//*****************************************************************************
function FormatStringAsPhoneNumber(inValue)
{
   inValue = keepOnlyNumbers(inValue);
   if (10 === inValue.length)
   {
      inValue = "(" + inValue.substring(0, 3) + ") " + inValue.substring(3, 6) + "-" + inValue.substring(6, 10);
   }
   return inValue;
}

//*****************************************************************************
//*
//* Summary:
//*   Summary not available.
//*
//* Parameters:
//*   inValue - Description not available.
//*   inField - Description not available.
//*   userID  - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function updateDataTable(inValue, inField, userId)
{
   var row = $("#searchGrid tbody").find("tr:first");

   if (inField === "ID_VOICENO") { inValue = FormatStringAsPhoneNumber(inValue); }     // Remove the formatting if the phone number is not yet 10 numbers.

   if (userId > 0) { row = $("#MgmtSummary").find("#searchGrid [data-row-id=" + userId + "]"); }

   switch (inField)
   {
      case "ID_FIRSTNAME":
         row.find("td:eq(0)").html(inValue);
         break;

      case "ID_LASTNAME":
         row.find("td:eq(1)").html(inValue);
         break;

      case "ID_USERNAME":
         row.find("td:eq(2)").html(inValue);
         break;

      case "ID_EMAIL":
         row.find("td:eq(3)").html(inValue);
         break;

      case "ID_VOICENO":
         row.find("td:eq(4)").html(inValue);
         break;

      case "ID_STATUS":
         row.find("td:eq(5)").html(inValue);
         break;
   }
}

//*****************************************************************************
//*
//* Summary:
//*   Builds the default UserName from the first 5 characters of the Last Name
//*   plus the first character of the First Name
//*
//* Parameters:
//*   inValue - The value in the Last Name field on the screen
//*   inField - The name/ID of the "Last Name" field on the screen.
//*
//* Returns:
//*   Description Get username base of lastname and firstname ignore space, ', / and - in the name
//*
//*****************************************************************************
function getUserName(inValue, inField)
{
   var uname = "";
   var iname = inValue.replace(" ", "").replace("'", "").replace("/", "").replace("-", "");

   if (inField === "ID_LASTNAME")
   {
      uname = ((iname.length < 5) ? iname : iname.substring(0, 5));
      var fname = $("#ID_FIRSTNAME").val().trim();
      if (fname !== "") { uname = uname + fname.substring(0, 1); }
   }

   return uname.toLowerCase();
}

//*****************************************************************************
//*
//* Summary:
//*   Refresh User detail procedure.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", "#CMD_REFRESH", function(e)
{
   // Refresh the data with current view/hide option
   var isDeleted = false;
   var $AttSrc = $("#CMD_RETIRED").attr("src");

   if ($AttSrc.indexOf("ShowRetired(1)") >= 0) { isDeleted = true; }

   var $url = "ViewDataRequest";
   var $TargetID = $("#MgmtSummary");

   $.ajax({
      type: "GET",
      url: $url,
      data: { 'isDeleted': isDeleted },
      success: function(data)
      {
         $TargetID.html(data);
         table = $("#searchGrid").DataTable();

         if (!table.$("tr.selected").hasClass("selected"))
         {
            $TargetID.find("#searchGrid tbody").find("tr:first").click();
         }

      },
      error: function(data)
      {
         showError("The controller call Fails on refresh view data request.");

      }
   });
});


//$(document).on("click", "#CMD_REFRESH", function ()
//{
//    if (!getEditMode() && $("a.active").attr("href") === "#tabs-summary") {
//        var url = "ViewDataRequest";
//        var isDelete = true;
//        var attSrc = $("#CMD_RETIRED").attr("src");

//        if (attSrc.indexOf("HideRetired(1)") >= 0) { isDelete = false; }

//        $.ajax({
//            type: "POST",
//            url: url,
//            data:
//            {
//                isDeleted: isDelete, __RequestVerificationToken:
//                    $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
//            },
//            success: function (data) {
//                var $target = $("#searchGrid");
//                var $newHtml = $(data);

//                $target.replaceWith($newHtml);
//                table = $("#searchGrid").DataTable();
//                $("#searchGrid tbody tr:eq(1)").click();
//            }
//        });
//        return true;
//    }
//    return false;
//});


//*****************************************************************************
//*
//* Summary:
//*   Add New Account detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(document).on("click", "#CMD_NEW", function()
{
   setEditMode_UserName(true, true);

   $(".formMax").find("input").val("");
   $("#ID_STATUS").val("Enabled");

   if ($(".formCtrl").attr("src").indexOf("Expand") > 0) { $(".formCtrl").click(); }

   enteredUsername = false;
   //editMode = false;
   textChanged = false;
   userID = 0;                 // For New User when call controller

   // Add New Line on first Row in table
   var cols = $("#searchGrid tbody tr:first td").length;
   var newRow = '<tr class="Entity-div-click"><td>&nbsp;</td>';

   for (var i = 1; i < cols; i++)
   {
      newRow += "<td></td>";
   }
   newRow += "</tr>";

   $("#searchGrid tbody tr:first").before(newRow);     // Add New Row at first of table
   if (table.$("tr.selected").hasClass("selected")) { table.$("tr.selected").removeClass("selected"); }
   $("#searchGrid tbody").find("tr:first").addClass("selected");

   disableEnableToolbarAfterAddNewAndEdit();
});

//*****************************************************************************
//*
//* Summary:
//*   Edit User Account detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(".fa-edit").parent().click(function(e)
{
   e.preventDefault();
   $(this).closest("tr").find("input").removeAttr("readonly");
});

//*****************************************************************************
//*
//* Summary:
//*   Sets various variables and settings when the app goes to/from edit/add mode.
//*
//* Parameters:
//*   inEditMode      - boolean : (True) when setting up for going into add/edit mode, (False) when coming out of Add/Edit mode.
//*   enableUserName  - boolean : The UserName field only gets set when the User gets created (True), all other times when we don't want the UserName edited we set it to (False).
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function setEditMode_UserName(inEditMode, enableUserName)
{
   setEditMode(inEditMode);         // In "QOSKForm.js"
   if (inEditMode === false)        // If we're not in edit mode...
   {
      enableUserName = false;       // ... UserName can't be in edited. (Put this check in, just in case someone calls this function incorrectly.)
      enteredUsername = false;      // ... set this to false so other logic won't get executed.
   }

   $("#ID_USERNAME").prop("readonly", !enableUserName);
   $("#ID_USERNAME").prop("disabled", !enableUserName);

   document.getElementById("MgmtSummary").style.opacity = (inEditMode ? "0.5" : "1.0");
   document.getElementById("MgmtSummary").readonly = inEditMode;
   document.getElementById("MgmtSummary").disabled = inEditMode;
}


$(document).on("click", "#CMD_EDIT", function()
{
   setEditMode_UserName(true, false);
   //if ($(".formCtrl").attr("src").indexOf("Expand") > 0) { $(".formCtrl").click(); }

   if (getEditMode())
   {
      if (userID > 0)
      {
         if ($("a.active").attr("href") === "#tabs-association")
         {
            resetToolbarControl("#CMD_RETIRED", false);
            resetToolbarControl("#CMD_REFRESH", false);
            setEditMode_UserName(true, false);

            active = "association";
            $(".relationControl").enableRelationEdit();
            //$("#UserMgmtAssociation table tbody tr td input[name='ProfileName']")
            //    .each(function () {
            //        if ($(this).val().length > 0) {
            //            $("#UserMgmtAssociation #profileAssociationList option[value='" + $(this).val() + "']").val('');
            //        }
            //    });
            $(".formMax input").each(function()
            {
               if ($(this).hasClass("editable")) { $(this).prop("readonly", true); }
            });
            $(".formMax select").each(function()
            {
               if ($(this).hasClass("editable")) { $(this).prop("disabled", true); }
            });
         }
         else if ($("a.active").attr("href") === "#tabs-summary")
         {
            $("#ID_USERTYPE").prop("required", true);
            resetToolbarControl("#CMD_RETIRED", false);
            resetToolbarControl("#CMD_REFRESH", false);
            setEditMode_UserName(true, false);

            active = "summary";
            var i = 0;
            var blnClick = false;

            valid = true;

            $("#searchGrid tr").filter(function()
            {
               if ($(this).hasClass("selected"))
               {
                  userID = $("#searchGrid tr.selected").closest("tr").attr("data-row-id");
                  blnClick = true;
                  return false;
               }
            });

            if (!blnClick)
            {
               i++;
               $("#searchGrid_paginate span a")[0].click();
               $("#searchGrid_paginate span a").each(function()
               {
                  $("#searchGrid tr").filter(function()
                  {
                     if ($(this).hasClass("selected"))
                     {
                        userID = $("#searchGrid tr.selected").closest("tr").attr("data-row-id");
                        blnClick = true;
                        return false;
                     }
                  });

                  if (!blnClick) { $("#searchGrid_next").click(); }
               });

               if (!blnClick)
               {
                  for (var j = i; j < $("a.paginate_button[data-dt-idx=" + i + "] ").text() ; j++)
                  {
                     $("#searchGrid tr").filter(function()
                     {
                        if ($(this).hasClass("selected"))
                        {
                           userID = $("#searchGrid tr.selected").closest("tr").attr("data-row-id");
                           blnClick = true;
                           return false;
                        }
                     });

                     if (!blnClick) { $("#searchGrid_next").click(); }
                  }
               }
            }
         }
      }
      disableEnableToolbarAfterAddNewAndEdit();
      return true;
   }
   disableEnableToolbarAfterAddNewAndEdit();
   return false;
});


//*****************************************************************************
//*
//* Summary:
//*   Save Account detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", "#CMD_SAVE", function()
{
   var fieldCheck = checkFields();
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   if (fieldCheck !== "")
   {
      messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + fieldCheck + "</h3>"
      }];
      btnObjects = [{
         "id": "btn_continue",
         "name": "Continue",
         "function": "onclick='$(this).closeMessageBox();'",
         "class": "btnErrorMessage",
         "style": "margin:0 auto;text-align:center"
      }];

      titleobjects = [{ "title": "Invalid Entry" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
      return;                  // Check with Error return;
   }

   if (editMode && !textChanged)
   {     // Edit mode with No Changes Detected
      $("#CMD_CANCEL").click();
   } else
   {                // Input Correct Mode to call controller      // Add or Edit mode to save
      if (!editMode && !validUser)
      {
         var title = $("#ID_USERNAME").val() + " has been previously used.";
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + title + "</h3>"
         }];
         btnObjects = [{
            "id": "btn_continue",
            "name": "Continue",
            "function": "onclick='$(this).closeMessageBox(); setEditMode_UserName(false, editMode);$('#CMD_REFRESH').click();'",
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:center"
         }];

         titleobjects = [{ "title": "Invalid Entry" }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);



         //setMessagBox(title, "Continue", "", "", "", "");
         //$("#confirmAlert").show();
         //$("#btcontinue, .alert-close").click(function ()
         //{
         //   setEditMode_UserName(false, editMode);
         //   //$("#ID_USERNAME").val("");
         //   $("#confirmAlert").hide();
         //   $("#CMD_REFRESH").click();
         //   return;
         //});
         //$("#CMD_REFRESH").click();
         return;
      }

      setEditMode_UserName(false, false);

      var isDeleted = true;
      var $AttSrc = $("#CMD_RETIRED").attr("src");
      if ($AttSrc.indexOf("HideRetired(1)") >= 0) { isDeleted = false; }

      var $url = "SaveAccountDetail";
      var $TargetID = $("#MgmtSummary");
      var bodyData = getBodyData();

      $.ajax({
         type: "GET",
         url: $url,    // Controllers Function UserAccountView_Hide
         data: "userID=" + userID + "&bodyData=" + bodyData + "&isDeleted=" + isDeleted + "&enteredUserName=" + enteredUsername,
         success: function(data)
         {
            $TargetID.html(data);
            table = $("#searchGrid").DataTable();
            if (!table.$("tr.selected").hasClass("selected")) { $TargetID.find("#searchGrid tbody").find("tr:first").click(); }
         },
         error: function(data)
         {
            showError("The controller call Fails on save data request.");
         }
      });

      resetToolbarControl("#CMD_REFRESH", true);

      textChanged = false;
      setEditMode_UserName(false, editMode);

      actionBar_Initial();

      resetToolbarControl("#CMD_REFRESH", true);
      $("#CMD_REFRESH").click();
   }
});

//*****************************************************************************
//*
//* Summary:
//*   Get All FormBody data to the string list for controller input.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function getBodyData()
{
   var bodyData = "";

   $(".formMax *").filter(":input").each(function()
   {
      bodyData = bodyData + '"' + $(this).attr("id") + '" : "' + $(this).val() + '",';
   });

   return bodyData;
}


//*****************************************************************************
//*
//* Summary:
//*   Reset form body data when UserCheck() fails.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function resetNewModeValue(bodyData)
{
   var allFields = String(bodyData).split(",");

   for (var i = 1; i < allFields.length; i++)
   {
      var oneField = String(allFields[i]).split(":");
      setField(oneField[0], oneField[1]);
   }
}

//*****************************************************************************
//*
//* Summary:
//*   reset one field data
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function setField(id, value)
{
   $(".formMax *").filter(":input").each(function()
   {
      if ($(this).attr("id") === id) { $(this).val(value); }
   });
}

//*****************************************************************************
//*
//* Summary:
//*   Check all input fields before when Save icon clicked.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function checkFields()
{
   if (!editMode)
   {
      if (!validateField(ECS_USERNAME, $("#ID_USERNAME").val()))
      {
         return "Username must be at least 4 characters (beginning with a letter)";
      }
   }

   if ($("#ID_FIRSTNAME").val() !== "" && !validateField(ECS_NAME, $("#ID_FIRSTNAME").val()))
   {
      //return "The first name must:\n\r   - Begin with a letter.\n\r   - Cannot contain special characters.";
      return "The first name must begin with a letter followed by only letters or numbers.";
   }

   if ($("#ID_LASTNAME").val() !== "" && !validateField(ECS_NAME, $("#ID_LASTNAME").val()))
   {
      //return "The last name must:\n\r   - Begin with a letter.\n\r   - Cannot contain special characters.";
      return "The last name must begin with a letter followed by only letters or numbers.";
   }

   if ($("#ID_SMSNO").val() !== "" && !validateField(ECS_USPHONE, $("#ID_SMSNO").val()))
   {
      return "The SMS phone number format is not correct.";
   }

   if ($("#ID_VOICENO").val() !== "" && !validateField(ECS_USPHONE, $("#ID_VOICENO").val()))
   {
      return "The voice phone number format is not correct.";
   }

   if ($("#ID_EXTENTION").val() !== "" && !validateField(ECS_NUMERIC, $("#ID_EXTENTION").val()))
   {
      return "The phone extension format is not correct.";
   }

   if ($("#ID_FAXNO").val() !== "" && !validateField(ECS_USPHONE, $("#ID_FAXNO").val()))
   {
      return "The fax phone number format is not correct.";
   }

   if (!validateField(ECS_EMAIL, $("#ID_EMAIL").val()))
   {
      return "The email address format is not correct.";
   }

   return "";
}

//*****************************************************************************
//*
//* Summary:
//*   Cancel User detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//* Notes:
//*   Removed the "No" logic as per request in DE402. (Kept it commented-out
//*   just in case it needs to be re-activated in the future.).  DE402 found
//*   an issues with this "No" logic, so if it does get re-activated, that
//*   issue needs to be resolved.
//*
//*****************************************************************************
$(document).on("click", "#CMD_CANCEL", function()
{
   if (textChanged)
   {
      var title = "You have pending changes. Would you like to cancel?";
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + title + "</h3>"
      }];
      var btnObjects = [{
         "id": "btn_yes_cancel",
         "name": "Yes",
         "function": "onclick='cancelUsersAction();'",
         "class": "btnErrorMessage",
         "style": "margin:0 auto;text-align:center"
      },
      {
         "id": "btn_no_cancel",
         "name": "No",
         "function": "onclick='cancelUsrsActionNoClick();'",
         "class": "btnErrorMessage",
         "style": "margin:0 auto;text-align:center"
      }
      ];

      var titleobjects = [{ "title": "Confirm Cancellation" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);

   }
   else
   {
      $("#CMD_REFRESH").prop("disabled", false);
      $("#CMD_REFRESH").click();
      textChanged = false;
      setEditMode_UserName(false, editMode);
      actionBar_Initial();
   }
});

function cancelUsersAction()
{
   setEditMode_UserName(false, editMode);

   // Remove first row if cancel on New
   $(this).closeMessageBox();
   if ($("#searchGrid tbody").find("tr:first").hasClass("selected") && editMode === true)
   {
      $("#searchGrid tbody").find("tr:first").remove();
   }

   if (editMode) { showFormBody($("#MgmtSummary"), userID); }

   $("#CMD_REFRESH").prop("disabled", false);
   $("#CMD_REFRESH").click();
   textChanged = false;
   setEditMode_UserName(false, editMode);
   actionBar_Initial();
}

function cancelUsrsActionNoClick()
{
   resetToolbarControl("#CMD_SAVE", true);
   resetToolbarControl("#CMD_CANCEL", true);
   $(this).closeMessageBox();
}
//*****************************************************************************
//*
//* Summary:
//*   View/Hide retired record(s) Account detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", "#CMD_RETIRED", function()
{
   var isDeleted = true;
   var $AttSrc = $("#CMD_RETIRED").attr("src");
   var $newSrc = "";

   if ($AttSrc.indexOf("HideRetired(1)") >= 0)
   {
      $newSrc = $AttSrc.replace("HideRetired(1)", "ShowRetired(1)");
      isDeleted = true;
   } else
   {
      $newSrc = $AttSrc.replace("ShowRetired(1)", "HideRetired(1)");
      isDeleted = false;
   }
   $("#CMD_RETIRED").attr("src", $newSrc);
   $("#CMD_RETIRED").show();

   var $url = "ViewDataRequest";
   var $TargetID = $("#MgmtSummary");

   $.ajax({
      type: "GET",
      url: $url,
      data: { 'isDeleted': isDeleted },
      success: function(data)
      {
         $TargetID.html(data);
         table = $("#searchGrid").DataTable();

         if (!table.$("tr.selected").hasClass("selected"))
         {
            $TargetID.find("#searchGrid tbody").find("tr:first").click();
         }
      },
      error: function(data)
      {
         showError("The controller call Fails on retired button click view data request.");
      }
   });
});

//*****************************************************************************
//*
//* Summary:
//*   Filter icon click action.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", "#CMD_FILTER", function()
{
   var title = "Filter icon click action is under development";

   var messageObject = [{
      "id": "lbl_messages",
      "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + title + "</h3>"
   }];
   var btnObjects = [{
      "id": "btn_ok",
      "name": "Ok",
      "function": "onclick='$(this).closeMessageBox();'",
      "class": "btnErrorMessage",
      "style": "margin:0 auto;text-align:center"
   }
   ];

   var titleobjects = [{ "title": "Confirm Filtering" }];
   $(this).addMessageButton(btnObjects, messageObject);
   $(this).showMessageBox(titleobjects);
});

//*****************************************************************************
//*
//* Summary:
//*   Print icon click action.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", "#CMD_PRINT", function()
{
   var title = "Print icon click action is under development";

   var messageObject = [{
      "id": "lbl_messages",
      "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + title + "</h3>"
   }];
   var btnObjects = [{
      "id": "btn_ok",
      "name": "Ok",
      "function": "onclick='$(this).closeMessageBox();'",
      "class": "btnErrorMessage",
      "style": "margin:0 auto;text-align:center"
   }
   ];

   var titleobjects = [{ "title": "Confirm Printing" }];
   $(this).addMessageButton(btnObjects, messageObject);
   $(this).showMessageBox(titleobjects);
});

//*****************************************************************************
//*
//* Summary:
//*   Update User Account detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$("#UpdateAccountRecord").on("click", function()
{
   //TODO: Place focus on a field then start to tab through it to make changes.

   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
   $("#btdeletenok").val(" Yes ");
   $("#btdeletenCancel").val(" No ");
   $("#btdeletenOKCancel").hide();
   $("#confirmAlert").show();
   $("#btdeletenCancel,#btnok,.alert-close").click(function()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });

   $("#confirmAlert").show();
   $(".alert-popup-body").show();
   $(".alert-popup").animate({ "top": "40%" });
   $("#ClearAccountForm").hide();
   $("#UpdateAccountRecord").hide();
});

//*****************************************************************************
//*
//* Summary:
//*   Set the Initial state of the active tool buttons.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function initialState()
{
   //enteredUsername = false;
   textChanged = false;
   setEditMode_UserName(false, false);
   actionBar_Initial();

   if (!$("#searchGrid tbody").hasClass("selected")) { $("#searchGrid tbody").find("tr:first").click(); }
}

//*****************************************************************************
//*
//* Summary:
//*   Enables/Disables (resets) the a toolbar control to the status given.
//*
//* Parameters:
//*   controlID - The control to enable/disable.
//*   enabled - boolean (T/F) value to determine whether to enable (true) or disable (false) this control.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
window.resetToolbarControl = function(controlId, enabled)
{
   if (controlId === "#CMD_CANCEL") { $(controlId).css("color", enabled ? "darkred" : "#3b8aBd"); }

   $(controlId).attr("src", $(controlId).attr("src").replace((enabled ? "(0)" : "(1)"), (enabled ? "(1)" : "(0)")));
   $(controlId).css("pointer-events", (enabled ? "auto" : "none"));
   $(controlId).prop("disabled", !enabled);
   $(controlId).removeClass(enabled ? "lighttxt" : "darktext");
   $(controlId).addClass(enabled ? "darktxt" : "lighttext");
};

//*****************************************************************************
//*
//* Summary:
//*   Enable/Disable toolbar control value after click.
//*   (disable Refresh, disable New, disable Edit, enable Save, enable Cancel, disable Retired, disable Print)
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function disableEnableToolbarAfterAddNewAndEdit()
{
   actionBarReset(false, false, false, true, true, false, false, false);
}

//*****************************************************************************
//*
//* Summary:
//*   Handle when a Account's detail is selected form the table.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", ".entity-div-click", function(e)
{

   //userID = $(this).closest("tr").attr("data-row-id");         // For keep row selection when refresh request

   //if ($("#CMD_NEW").attr("src").indexOf("(0)") > 0 || $("#CMD_EDIT").attr("src").indexOf("(0)") > 0) {   // On Edit /Add mode no selection allow
   //   var title = "Do you want to save the change?";
   //   setMessagBox(title, "Continue", "", "", "Save", "Discard");

   //   $("#confirmAlert").show();

   //   // Error message "Discard" button action
   //   $("#btdiscard,.alert-close").click(function () {
   //      $("#confirmAlert").hide();

   //      if ($("#searchGrid tbody").find("tr:first").hasClass("selected") && editMode === false) {
   //         $("#searchGrid tbody").find("tr:first").remove();
   //      }

   //      actionBar_Initial();

   //      //$("#CMD_NEW").attr("src").replace("(0)", "(1)");
   //      //$("#CMD_EDIT").attr("src").replace("(0)", "(1)");

   //      showFormBody($("#MgmtSummary"), userID);
   //      userID = null;
   //      return;
   //   });

   //   // Error message "Save" button action
   //   $("#btsave").click(function () {
   //      $("#btsave").unbind("click");
   //      $("#confirmAlert").hide();
   //      $("#CMD_SAVE").click();

   //      return;
   //   });

   //   // Error message "Continue" button action
   //   $("#btcontinue").click(function () {
   //       setEditMode_UserName(false, editMode);
   //      $("#btcontinue").unbind("click");
   //      $("#confirmAlert").hide();

   //      return;
   //   });
   //   return true;
   //};

   //getEditMode();
   if (!getEditMode())
   {
      userID = $(this).closest("tr").attr("data-row-id");         // For keep row selection when refresh request
      showFormBody($("#MgmtSummary"), userID);
      ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);
      actionBar_Initial();                                      //Disable Save & Cancel after Row click and Enable New & Edit
   }

   return false;
});

//*****************************************************************************
//*
//* Summary:
//*   Take a Form Id then return the content of the Form as an Object.
//*
//* Parameters:
//*   formID - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function getFormData(formId)
{
   var formData = $(formId).serializeObject();
   return JSON.stringify(formData);
}

//*****************************************************************************
//*
//* Summary:
//*   Get Anti Forgery token from the Form.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function getAntiForgeryToken()
{
   var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
   return "&" + $form.serialize();
}

//*****************************************************************************
//*
//* Summary:
//*   Select a Form by ID, serialize the content, clean it, and send it back  as an object
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
(function($)
{
   $.fn.serializeObject = function()
   {

      var self = this,
          json = {},
          pushCounters = {},
          patterns = {
             "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
             "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
             "push": /^$/,
             "fixed": /^\d+$/,
             "named": /^[a-zA-Z0-9_]+$/
          };

      this.build = function(base, key, value)
      {
         base[key] = value;
         return base;
      };

      this.push_counter = function(key)
      {
         if (pushCounters[key] === undefined)
         {
            pushCounters[key] = 0;
         }
         return pushCounters[key]++;
      };

      $.each($(this).serializeArray(), function()
      {
         if (!patterns.validate.test(this.name)) { return; }        // skip invalid keys

         var k,
             keys = this.name.match(patterns.key),
             merge = this.value,
             reverseKey = this.name;

         while ((k = keys.pop()) !== undefined)
         {
            reverseKey = reverseKey.replace(new RegExp("\\[" + k + "\\]$"), "");        // adjust reverse_key

            if (k.match(patterns.push))
            {
               merge = self.build([], self.push_counter(reverseKey), merge);        // push
            }
            else if (k.match(patterns.fixed))
            {
               merge = self.build([], k, merge);        // fixed
            }
            else if (k.match(patterns.named))
            {
               merge = self.build({}, k, merge);        // named
            }
         }

         json = $.extend(true, json, merge);
      });

      return json;
   };
})(jQuery);

//*****************************************************************************
//*
//* Summary:
//*   Set the Edit mode flag.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("change", ".inputEx", function()
{
   if (editMode) { textChanged = true; }
});

//*****************************************************************************
//*
//* Summary:
//*   Set the Edit mode flag on selectEx.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("change", ".selectEx", function()
{
   if (editMode) { textChanged = true; }
});

//*****************************************************************************
//*
//* Summary:
//*   Initializes the actionBar controls to their initial values.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function actionBar_Initial()
{
   actionBarReset(true, true, true, false, false, true, true, true);
}

//*****************************************************************************
//*
//* Summary:
//*   Resets the actionBar controls to the passed in parameters.
//*
//* Parameters:
//*   boolean values to determine if each control should be enabled (true) or disabled (false).
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function actionBarReset(enableRefresh, enableNew, enableEdit, enableSave, enableCancel, enablePrint, enableRetired, enableFilter)
{
   resetToolbarControl("#CMD_REFRESH", enableRefresh);
   resetToolbarControl("#CMD_NEW", enableNew);
   resetToolbarControl("#CMD_EDIT", enableEdit);
   resetToolbarControl("#CMD_SAVE", enableSave);
   resetToolbarControl("#CMD_CANCEL", enableCancel);
   resetToolbarControl("#CMD_PRINT", enablePrint);
   resetToolbarControl("#CMD_RETIRED", enableRetired);
   //resetToolbarControl("#CMD_FILTER", enableFilter);     // Placeholder.
}


//*****************************************************************************
//*
//* Summary:
//*   Summary not available.
//*
//* Parameters:
//*   Summary - Description not available.
//*   UserID  - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function showFormBody($Summary, userID)
{
   if (userID === null) { return; }

   var $div = $Summary.find("#searchGrid [data-row-id=" + userID + "]");  // Find Row

   if (!$div.hasClass("selected"))
   {
      table.$("tr.selected").removeClass("selected");
      $div.addClass("selected");
   }

   var tokenData = $div.attr("MgmtDetail-data") + getAntiForgeryToken();
   var options = {
      async: true,
      url: $div.attr("MgmtDetail-action"),
      type: $div.attr("MgmtDetail-method"),
      data: tokenData,
      success: function(data)
      {
         $(".formMax").html(data);
         $(".formMin").html(data);
         var status = $(".selectEx").attr("value");
         $(".selectEx").val(status).prop("selected", true);
         if ($(".formCtrl").attr("src").indexOf("Expand") > 0) { $(".formCtrl").click(); }
      }
   };
   try
   {
      $.ajax(options);
   } catch (e)
   {
      throw e;
   }
}

//*****************************************************************************
//*
//* Summary:
//*   Setup MessageBox Three Buttons value and visibility.
//*
//* Parameters:
//*   title      - The message(title) to display in the message box.
//*   btContinue - Word to diaplay for the continue button (usually "Continue")
//*   btYes      - Word to diaplay for the yes button (usually "Yes")
//*   btNo       - Word to diaplay for the no button (usually "No")
//*   btSave     - Word to diaplay for the save button (usually "Save")
//*   btDiscard  - Word to diaplay for the discard button (usually "Discard")
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function setMessagBox(title, btContinue, btYes, btNo, btSave, btDiscard)
{
   $("#confirmAlert").hide();
   $("#alertdeletemessageid").text(title);

   setButton($("#btcontinue"), btContinue);
   setButton($("#btyes"), btYes);
   setButton($("#btno"), btNo);
   setButton($("#btsave"), btSave);
   setButton($("#btdiscard"), btDiscard);
}

//*****************************************************************************
//*
//* Summary:
//*   Summary not available.
//*
//* Parameters:
//*   btID  - Description not available.
//*   value - Description not available.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function setButton(btId, value)
{
   btId.val("");
   btId.hide();

   if (value !== "")
   {
      btId.val(value);
      btId.show();
   }
}

//*****************************************************************************
//*
//* Summary:
//*   Takes in a string of characters and returns a string with only the numeric values.
//*
//* Parameters:
//*   str - The character string to remove all non-numbers from.
//*
//* Returns:
//*   A string that has had all non-numerics removed.
//*
//*****************************************************************************
function keepOnlyNumbers(str)
{
   //return str.replace(/\D/g, "");

   var resultStr = "";
   for (var x = 0; x < str.length; x++)
   {
      if (str[x] >= "0" && str[x] <= "9") { resultStr += str[x]; }
   }
   return resultStr;
}
