//*****************************************************************************
//*
//* UserMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------
   var editMode = false; //flag to see if we're in edit mode (NOT ADD NEW MODE)
   var enteredUsername = false;
   var keyPressVal = 0;
   var userId = 0;
   var username = "";
   var summaryDt = null; //holds the Data Table for Semmary tab
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];

   //#############################################################################
   // Global functions
   //#############################################################################


   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      console.log("UserMgmt::ready()");

      // Initialize the state of the Actionbar
      ///////////////////////////////////////////////////////////////////////////////
      window.setEditMode(false);

      window.enableActionbarControl("#CMD_REFRESH", true);
      window.enableActionbarControl("#CMD_PRINT", true);

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      summaryDt = $(".QoskSummary").DataTable();
      $(".QoskSummary tbody tr:eq(1)").click();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Input event on all fields in the form
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", ".inputEx", function ()
   {
      var value = $(this).val();
      var attrId = $(this).attr("id");
      var isRequired = $(this).attr("required");

      if (attrId === "ProfileCode")
      {

      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Focus out event on all fields in the form
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusout", ".inputEx", function ()
   {
      if (!getEditMode())
      {
         return false;
      }

      var value = $(this).val();
      var attrId = $(this).attr("id");
      var isRequired = $(this).attr("required");

      if (attrId === "Password")
      {
         checkPassword(value);
      }
      else if (attrId === "UserName")
      {
         checkUsername(value);
      }
      else if (attrId === "Email")
      {
         checkEmail(value);
      }

      return true;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   focus out event on the profile dropdown menu
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusout", "#ProfileCode", function (e)
   {
      if (getEditMode() && getFormChanged())
      {
         if ($(e.target).attr("disabled") !== undefined || $(e.target).attr("readonly") !== undefined)
         {
            return false;
         }

         var profileRd = "";
         var profileName = "";
         var i = 0;
         var ij = 0;
         var profileRdIj = "";
         var profileNmIj = "";
         var strData = $(e.target).val();

         //don't search through the list if input is empty
         if (strData === "")
         {
            return false;
         }

         if ($("#profileList option").length === 0)
         {
            //addDataListOptions("profileList", strData, keyPressVal++);
         }

         $("#profileList option").each(function ()
         {
            if (CompareTextboxes(strData, $(this).val()))
            {
               profileRd = $(this).attr("data-field-name");
               profileName = $(this).val();
               i++;
            }

            if (CompareTextboxes($(this).val(), strData))
            {
               profileRdIj = $(this).attr("data-field-name");
               profileNmIj = $(this).val();
               ij++;
            }
         });

         if (profileRd !== "" && (i === 1 || ij === 1))
         {
            $('#profileList option[data-field-name="' + (ij === 1 ? profileRdIj : profileRd) + '"]').closest("option").click();
            removeColorAndClass("#ECDD9D", "#ProfileCode", "inputError");

            if (!$(".formMax .inputEx,selectEx").hasClass("inputError") && editMode)
            {
               //enableActionbarControl("#CMD_SAVE", true);
            }
         }
         else
         {
            profileName = "";
            profileRd = "0";
            setColorAndClass("rgb(243, 104, 104)", "#ProfileCode", "inputError");
            //enableActionbarControl("#CMD_SAVE", false);
            //$(this).closest("tr").find("td input[name=CarrierID]").val('');
            $(this).val(strData);
         }
         //updateDataTable(ij === 1 ? carrierRdIj : carrierRd , "CarrierID" , trckId);
         //updateDataTable(ij === 1 ? carrierNmIj : carrierName , "Carrier" , trckId);
         if ((ij === 1 ? profileNmIj : profileName) !== "")
         {
            $("#ProfileCode").val(ij === 1 ? profileNmIj : profileName);
         }

         $("#ProfileCode").attr("data-field-value", ij === 1 ? profileRdIj : profileRd);
         i = 0;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   ProfileList dropdown click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#profileList option", function (e)
   {
      if ($(e.target).val() !== undefined)
      {
         //$("#Profile").removeAttr("style");

         //updateDataTable($(e.target).attr("data-field-name"), "CarrierID", trckId);
         //updateDataTable($(e.target).val(), "Carrier", trckId);

         $("#ProfileCode").val($(e.target).val());
         $("#ProfileCode").attr("data-field-value", $(e.target).attr("data-field-name"));
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Input event on all fields in the form
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keydown", ".inputEx", function (e)
   {
      var value = $(this).val();
      var attrId = $(this).attr("id");
      var isRequired = $(this).attr("required");

      if (attrId === "Password")
      {
         if (e.keyCode === 32) //don't allow spaces
         {
            return false;
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   check username to see if it's available
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkUsername(user)
   {
      if (getEditMode() && getFormChanged())
      {
         if ($("#UserName").attr("readonly") !== undefined)
         {
            return false;
         }

         if (user === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#UserName", "inputError");
            return false;
         }

         $.ajax({
            cache: false,
            type: "GET",
            url: "UserCheck", // Controllers Function UserAccountView_Hide
            data: "username=" + user,
            async: true,
            success: function (data)
            {
               if (data.success)
               {
                  removeColorAndClass("#ECDD9D", "#UserName", "inputError");
               }
               else
               {
                  setColorAndClass("rgb(243, 104, 104)", "#UserName", "inputError");

                  messageObject = [
                     {
                        "id": "lbl_messages",
                        "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>Username already exists</h3>"
                     }
                  ];

                  btnObjects = [
                     {
                        "id": "btn_ok_error",
                        "name": "Ok",
                        "class": "btnErrorMessage",
                        "style": "margin:0 auto;text-align:center",
                        "function": "onclick='$(this).closeMessageBox();'"
                     }
                  ];

                  titleobjects = [
                     {
                        "title": "Invalid Entry"
                     }
                  ];

                  $(this).addMessageButton(btnObjects, messageObject);
                  $(this).showMessageBox(titleobjects);
               }

               return data.success;
            },
            error: function (data)
            {
               setColorAndClass("rgb(243, 104, 104)", "#UserName", "inputError");

               messageObject = [
                  {
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on username check request.</h3>"
                  }
               ];

               btnObjects = [
                  {
                     "id": "btn_ok_error",
                     "name": "Ok",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center",
                     "function": "onclick='$(this).closeMessageBox();'"
                  }
               ];

               titleobjects = [
                  {
                     "title": "Invalid Entry"
                  }
               ];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);

               return false;
            }
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   check password entered against our password standards
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkPassword(password)
   {
      //new user requires password; check password always
      //existing users only check password when input is entered
      if (password === "" && userId === 0)
      {
         setColorAndClass("rgb(243, 104, 104)", "#Password", "inputError");
         return false;
      }
      else if (password === "" && userId > 0)
      {
         return false;
      }

      $.ajax({
         cache: false,
         type: "GET",
         url: "PasswordCheck", // Controllers Function UserAccountView_Hide
         data: "password=" + password,
         async: true,
         success: function (data)
         {
            data.success ? removeColorAndClass("#ECDD9D", "#Password", "inputError") : setColorAndClass("rgb(243, 104, 104)", "#Password", "inputError");
            return data.success;
         },
         error: function (data)
         {
            setColorAndClass("rgb(243, 104, 104)", "#Password", "inputError");

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on user password check request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);

            return false;
         }
      });

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   check email entered to see if it's already being used
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkEmail(email)
   {
      if (email === "" || !EFS_EMAIL.test(email))
      {
         setColorAndClass("rgb(243, 104, 104)", "#Email", "inputError");
         return false;
      }

      $.ajax({
         cache: false,
         type: "GET",
         url: "EmailCheck", // Controllers Function UserAccountView_Hide
         data: "email=" + email + "&username=" + username,
         async: true,
         success: function (data)
         {
            if (data.success)
            {
               removeColorAndClass("#ECDD9D", "#Email", "inputError");
            }
            else
            {
               setColorAndClass("rgb(243, 104, 104)", "#Email", "inputError");

               messageObject = [
                  {
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>This email is already associated with an account.</h3>"
                  }
               ];

               btnObjects = [
                  {
                     "id": "btn_ok_error",
                     "name": "Ok",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center",
                     "function": "onclick='$(this).closeMessageBox();'"
                  }
               ];

               titleobjects = [
                  {
                     "title": "Invalid Entry"
                  }
               ];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);

               return false;
            }

            return data.success;
         },
         error: function (data)
         {
            setColorAndClass("rgb(243, 104, 104)", "#Email", "inputError");

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on user email check request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);

            return false;
         }
      });

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save function
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function saveUser()
   {
      console.log("Save under development");

      if (!getEditMode())
      {
         return false;
      }

      //go through each input and check for required fields
      $(".formMax *").filter(":input").each(function ()
      {
         var attrId = $(this).attr("id");

         if ($(this).attr("required") !== undefined && $(this).val() === "" && attrId !== "Password")
         {
            setColorAndClass("rgb(243, 104, 104)", "#" + attrId, "inputError");
         }
         else if ($(this).val() === "" && attrId === "Password" && userId === 0)
         {
            setColorAndClass("rgb(243, 104, 104)", "#" + attrId, "inputError");
         }
      });

      if ($(".formEx .inputEx").hasClass("inputError"))
      {
         return false;
      }

      //no point in making a server-side call to save an item that has not been edited
      if (!getFormChanged())
      {
         OnQnexCancel();
         return false;
      }

      var bodyData = getBodyData();

      $.ajax({
         type: "GET",
         url: "SaveAccountDetail",    // Controllers Function UserAccountView_Hide
         data: "userID=" + userId + "&bodyData=" + bodyData + "&isDeleted=false&enteredUserName=false",
         success: function (data)
         {
            if (data.success)
            {
               window.setEditMode(false);
               clearTextBoxes();
            }
            else
            {
               setColorAndClass("rgb(243, 104, 104)", "#Email", "inputError");
               var parseHtml = "";

               for (var i = 0; i < data.errorList.length; i++)
               {
                  if (data.errorList[i] !== "")
                  {
                     parseHtml += "<h3 style='margin:0 auto;text-align:center;width:100%'>" + (i + 1) + ". " + data.errorList[i] + "</h3>";
                  }
               }

               messageObject = [
                  {
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The following error(s) occured:</h3>" + parseHtml
                  }
               ];

               btnObjects = [
                  {
                     "id": "btn_ok_error",
                     "name": "Ok",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center",
                     "function": "onclick='$(this).closeMessageBox();'"
                  }
               ];

               titleobjects = [
                  {
                     "title": "Invalid Entry"
                  }
               ];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         },
         error: function (data)
         {
            messageObject = [
               {
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on user save request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cancel user entry/edit
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function cancelSave()
   {
      window.setEditMode(false);
      editMode = false;
      $(".formEx .inputEx").val("");
      clearTextBoxes();
      $("#searchGrid tr.selected").removeClass("selected");

      if (userId === 0)
      {
         $("#searchGrid tr:eq(2)").trigger("click");
      }
      else
      {
         $("#searchGrid tr[MgmtEntity-id=" + userId + "]").closest("tr").find("td[exchange='UserName'][exchange-value='" + username + "']").closest("tr").trigger("click");
      }

      console.log("Cancel under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get User List To Fill the Summary tab
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getUserList()
   {
      var url = "ViewDataRequest";
      $.ajax({
         type: "POST",
         url: url,
         data: "isDeleted=" + true,
         async: false,
         success: function (data)
         {
            var $target = $("#MgmtSummary");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            window.dtHeaders(".QoskSummary", '"order": [[1, "asc"]]', [], []);
            summaryDt = $(".QoskSummary").DataTable();
            $(".QoskSummary tbody tr:eq(1)").click();
         },
         error: function (data)
         {
            showError("The controller call failed on refresh view data request.");

         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary grid click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#searchGrid tr.entity-div-click", function (e)
   {
      if (getEditMode())
      {
         return false;
      }

      summaryDt.$("tr.selected").removeClass("selected");
      $(this).addClass("selected");
      $("#ProfileCode").val($(this).find("td[exchange='ProfileCode']").attr("exchange-value"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_cancel_saving", function ()
   {
      $(this).closeMessageBox();
      cancelSave();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clear Textboxes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clearTextBoxes()
   {
      $(".formMax .inputEx").val("");
      $(".formMax .selectEx").val($(".formMax .selectEx option:first").val());
      $(".formMax .inputEx,.selectEx").removeAttr("style").removeClass("inputError");
   }

   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      console.log("Refresh under development");

      if (!getEditMode())
      {
         getUserList();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Add New button click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function ()
   {
      if (getEditMode())
      {
         return false;
      }

      OnQnexRefresh();
      window.setEditMode(true);
      clearTextBoxes();
      userId = 0;
      username = "";
      $("#UserID").prop("readonly", true).val(userId);// User ID will be auto-assigned by system
      $("#ProfileCode").attr("data-field-value", "");
      $("#FirstName").focus();
      console.log("New under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Edit button click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      if (getEditMode())
      {
         return false;
      }

      window.setEditMode(true);
      editMode = true;
      userId = $("#searchGrid tr.selected").closest("tr").attr("MgmtEntity-id");
      username = $("#searchGrid tr.selected").closest("tr").find("td[exchange='UserName']").attr("exchange-value");
      $("#UserName, #UserID").prop("readonly", true); // Once a UserName and a User ID have been issued, we can no longer change them
      $("#FirstName").focus();
      console.log("Edit under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save button click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function ()
   {
      if (getEditMode())
      {
         setTimeout(function ()
         {
            saveUser();
         }, 100);
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cancel button click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function ()
   {
      if (!getEditMode())
      {
         return false;
      }

      if (getFormChanged())
      {
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
         }];

         btnObjects = [{
            "id": "btn_cancel_saving",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:center"
         }, {
            "id": "btn_no",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:center",
            "function": "onclick='$(this).closeMessageBox();'"
         }
         ];

         titleobjects = [{
            "title": "Confirm Cancellation"
         }];

         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
      }
      else
      {
         cancelSave();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
      console.log("Print under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRetired = function ()
   {
      console.log("Retired under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   saveAndvalidate - ???.
   //*   field           - ???.
   //*   data            - ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      //console.log("UserMgmt::UpdateData(" + saveAndvalidate + ", " + field + ", " + data + ")");

      console.log($("#" + field).prop("tagName") + "::" + field);

      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        if (field === "ProfileCode")
                        {
                           $("#" + field).attr("data-field-value", data);
                        }
                        else
                        {
                           $("#" + field).val(data);
                        }
                     }

                  //console.log("TEXT::" + field);
               }

               break;
            }

         case "SELECT":
            {
               //console.log("SELECT::" + field + "::" + data);
               $("#" + field).val(data);
            }
      }

      if (saveAndvalidate)
      {
      }
      else
      {
         //console.log($("#" + field).attr("type"));


      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get All FormBody data to the string list for controller input.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function getBodyData()
   {
      var bodyData = "";

      $(".formMax *").filter(":input").each(function ()
      {
         bodyData += '"' + $(this).attr("id") + '" : "' + ($(this).attr("id") === "ProfileCode" ? $(this).attr("data-field-value") : $(this).val()) + '",';
      });

      console.log(bodyData);
      return bodyData;
   }

});