///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyMgmtViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.PolicyMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.PolicyMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class PolicyMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "ADMIN_POLICY";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Policy record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PolicyEntity SelectedEntity { get; set; } = new PolicyEntity();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Policy list for the Policy detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<PolicyEntity> Entitys { get; set; } = new List<PolicyEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }
}
