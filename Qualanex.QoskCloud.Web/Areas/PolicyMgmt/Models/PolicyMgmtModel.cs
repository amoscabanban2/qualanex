﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyMgmtModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.PolicyMgmt.Models
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Common;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class PolicyMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public PolicyMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~PolicyMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyEntity> GetPolicyEntities(bool isDeleted)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (from Policy in cloudEntities.Policy
                    join profile in cloudEntities.Profile on Policy.ProfileCode equals profile.ProfileCode
                    where Policy.IsDeleted == true || Policy.IsDeleted == isDeleted
                    select new PolicyEntity()
                    {
                       PolicyCode = Policy.PolicyCode,
                       ProfileName = profile.Name,
                       EffectiveStartDate = Policy.EffectiveStartDate.ToString(),
                       EffectiveEndDate = Policy.EffectiveEndDate.ToString(),
                       DiscountPercent = Policy.DiscountPercent.ToString(),
                       DaysToReturnInErr = Policy.NumberOfDaysToReturnProductReceivedInError.ToString(),
                       Months_O_Return = Policy.NumberOfMonthsBeforeExpirationReturnableOpened.ToString(),
                       Months_S_Return = Policy.NumberOfMonthsBeforeExpirationReturnableSealed.ToString(),
                       BrkSealRrturnable = Policy.BrokenSealReturnable.ToString(),
                       PartialsRrturnable = Policy.PartialsReturnable.ToString(),
                       PreVialsReturnable = Policy.PrescriptionVialsReturnable.ToString(),
                       RepackagerCreditable = Policy.RepackagerCreditable.ToString(),
                       RepackagedIntoUnitDoseReturnable = Policy.RepackagedIntoUnitDoseReturnable.ToString(),
                       IsDeleted = Policy.IsDeleted.ToString()
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         var actionMenu = new ActionMenuModel
         {
            Name = "Select One",
            Links = new List<ActionMenuEntry>
            {
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 1",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 2",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 3",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 4",
                  Url = "#"
               }
            }
         };

         return actionMenu;
      }

      #endregion
   }
}
