﻿//*****************************************************************************
//*
//* PolicyMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

//=============================================================================
// Global definitions
//-----------------------------------------------------------------------------
var editmode = false;
var textchanged = false;
var PloicyID;
var table;

//*****************************************************************************
//*
//* Summary:
//*   Page load functions.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(document).ready(function ()
{
   table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);
   InitialState();
});

//*****************************************************************************
//*
//* Summary:
//*   Refresh Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_refresh").click(function ()
{
   // Refresh the data with current view/hide option
   var $isEnable = true;
   var $AttSrc = $("#btn_toolbar_hide").children().attr("src");

   if ($AttSrc.indexOf("Hidden(1)") >= 0)
   {
      $isEnable = false;
   }

   var $url = $("#btn_toolbar_hide").attr("data-method");
   var $TargetID = $("#UserGridDetail");

   $.ajax({
      type: "GET",
      url: $url,
      data: { 'isEnable': $isEnable },
      success: function (data)
      {
         var $target = $("#UserGridDetail");
         var $newHtml = $(data);

         $target.replaceWith($newHtml);
         $newHtml.find("#searchGrid [data-row-id='" + PloicyID + "']").click();
         table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);

         if (!table.$("tr.selected").hasClass("selected"))
         {
            $newHtml.find("#searchGrid tbody").find("tr:first").click();
         }
      },
      error: function (data)
      {
         alert("Fail");
      }
   });
});

//*****************************************************************************
//*
//* Summary:
//*   Add New Policy procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_new").click(function ()
{
   $("#alertmessageid").text("New click");
   editmode = true;
   DisableEnableToolbarAfterAddNewAndEdit();
   alert("Add New User Action:=" + $(this).attr("data-method"));

   //$.ajax({
   //   type: "POST",
   //   url: $(this).attr("data-method"),
   //   data: {
   //      'FirstName': "",
   //      'LastName': "",
   //      'Email': "",
   //      'PhoneNumber': "",
   //      'FaxNumber': "",
   //      'ProfileCode': "",
   //      'ProfileName': ""
   //   },
   //   success: function(data)
   //   {
   //      alert("Success");
   //   },
   //   error: function(data)
   //   {
   //      alert("Fail");
   //   }
   //});
});

//*****************************************************************************
//*
//* Summary:
//*   Edit Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_edit").click(function ()
{
   $("#alertmessageid").text("edit click");
   editmode = true;
   DisableEnableToolbarAfterAddNewAndEdit();
   alert("Edit User Action:=" + $(this).attr("data-method"));

   //$.ajax({
   //   type: "POST",
   //   url: $(this).attr("data-method"),
   //   data: {
   //      'FirstName': "",
   //      'LastName': "",
   //      'Email': "",
   //      'PhoneNumber': "",
   //      'FaxNumber': "",
   //      'ProfileCode': "",
   //      'ProfileName': ""
   //   },
   //   success: function(data)
   //   {
   //      alert("Success");
   //   },
   //   error: function(data)
   //   {
   //      alert("Fail");
   //   }
   //});
});

//*****************************************************************************
//*
//* Summary:
//*   Save Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_save").click(function ()
{
   if (editmode && !textchanged)
   {
      // Error message "OK" button action
      $("#btdeletenok").hide();
      $("#btdeletenCancel").hide();
      $("#btdeletenOKCancel").show();
      $("#alertdeletemessageid").text("No Changes Detected");
      $("#confirmAlert").show();

      $("#btdeletenOKCancel,.alert-close").click(function ()
      {
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         return;
      });

      alert("Save User Action:=" + $(this).attr("data-method"));

   }
   else
   {
      location.reload();
      InitialState();
   }
});

//*****************************************************************************
//*
//* Summary:
//*   Cancel Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_cancel").click(function ()
{
   if (textchanged)
   {
      $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
      $("#confirmAlert").show();

      // Error message "No" or "Close Windows" button action
      $("#btdeletenCancel, #btnok,.alert-close").click(function ()
      {
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         return;
      });

      // Error message "Yes" button action
      $("#btdeletenok").click(function ()
      {
         $("#btdeletenok").unbind("click");
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         location.reload();
         InitialState();
         return;
      });

   }
   else
   {
      InitialState();
      return;
   }
});

//*****************************************************************************
//*
//* Summary:
//*   Filter icon click action.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_filter").click(function ()
{
   alert("Filter icon Click Action under develop");
});

//*****************************************************************************
//*
//* Summary:
//*   Print icon click action.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_print").click(function ()
{
   alert("Print icon Click Action under develop");
});

//*****************************************************************************
//*
//* Summary:
//*   Update Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#UpdatePolicyRecord").on("click", function ()
{
   //TODO: place focus on a field then start to tab through it to make changes.
   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");

   $("#btdeletenCancel,#btnok,.alert-close").click(function ()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });

   $("#confirmAlert").show();
   $(".alert-popup-body").show();
   $(".alert-popup").animate({ "top": "40%" });
   $("#ClearPolicyForm").hide();
   $("#UpdatePolicyRecord").hide();
});

//*****************************************************************************
//*
//* Summary:
//*   View disabled records/Hide disabled records Policy detail procedure.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btn_toolbar_hide").click(function ()
{
   var isEnable = true;
   var $AttSrc = $("#btn_toolbar_hide").children().attr("src");
   var $newTit = "";
   var $newSrc = "";

   if ($AttSrc.indexOf("Hidden(1)") >= 0)
   {
      $newTit = "Show Disabled Records";
      $newSrc = $AttSrc.replace("Hidden(1)", "Hidden(0)");
      isEnable = true;
   }
   else
   {
      $newTit = "Hide Disabled Records";
      $newSrc = $AttSrc.replace("Hidden(0)", "Hidden(1)");
      isEnable = false;
   }

   $("#btn_toolbar_hide").attr("title", $newTit);
   $("#btn_toolbar_hide").children().attr("src", $newSrc);
   $("#btn_toolbar_hide").show();

   var $url = $("#btn_toolbar_hide").attr("data-method");
   var $TargetID = $("#UserGridDetail");

   $.ajax({
      type: "GET",
      url: $url,
      data: {
         'isEnable': isEnable
      },
      success: function (data)
      {
         var $target = $("#UserGridDetail");
         var $newHtml = $(data);
         $target.replaceWith($newHtml);
         $newHtml.find("#searchGrid tbody").find("tr:first").click();
         table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);
      },
      error: function (data)
      {
         alert("Fail");
      }
   });
});

//*****************************************************************************
//*
//* Summary:
//*   Save changes popup functionality.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(".alert-close, #btnAlertStay,.close-btn").click(function ()
{
   $(".alert-popup-body").hide();
   $(".alert-popup").animate({ "top": "40%" });
   $(".model-popup").hide();
});

//*****************************************************************************
//*
//* Summary:
//*   Set the Initial state of the active tool buttons.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
function InitialState()
{
   editmode = false;
   textchanged = false;
   EnableToolbarControl("#btn_toolbar_refresh");
   EnableToolbarControl("#btn_toolbar_new");
   EnableToolbarControl("#btn_toolbar_edit");
   //EnableToolbarControl("#btn_toolbar_hide");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");

   if (!$("#searchGrid tbody").hasClass("selected"))
   {
      $("#searchGrid tbody").find("tr:first").click();
   }
}

//*****************************************************************************
//*
//* Summary:
//*   Enable toolbar control.
//*
//* Parameters:
//*   controlID - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
window.EnableToolbarControl = function (controlID)
{
   if (controlID === "#btn_toolbar_cancel")
   {
      $(controlID).css("color", "darkred");
   }

   $(controlID).children().attr("src", $(controlID).children().attr("src").replace("(0)", "(1)"));
   $(controlID).css("pointer-events", "auto");
   $(controlID).removeClass("lighttxt");
   $(controlID).addClass("darktxt");
};

//*****************************************************************************
//*
//* Summary:
//*   Disable toolbar control.
//*
//* Parameters:
//*   controlID - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
window.DisableToolbarControl = function (controlID)
{
   if (controlID === "#btn_toolbar_cancel")
   {
      $(controlID).css("color", "#3b8aBd");
   }

   $(controlID).children().attr("src", $(controlID).children().attr("src").replace("(1)", "(0)"));
   $(controlID).css("pointer-events", "none");
   $(controlID).removeClass("darktxt");
   $(controlID).addClass("lighttxt");
};

//*****************************************************************************
//*
//* Summary:
//*   Disable enable toolbar control value after click.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function DisableEnableToolbarAfterAddNewAndEdit()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   //DisableToolbarControl("#btn_toolbar_hide");
   EnableToolbarControl("#btn_toolbar_save");
   EnableToolbarControl("#btn_toolbar_cancel");
}

//*****************************************************************************
//*
//* Summary:
//*   Handler when a Policy's detail is selected form the table:.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", ".entity-div-click", function (e)
{
   PloicyID = $(this).closest("tr").attr("data-row-id");         // For keep row selection when refresh request

   var $div = $(this);
   var tokenData = $div.attr("PolicyMgmtDetail-data") + getAntiForgeryToken();
   var options = {
      async: true,
      url: $div.attr("PolicyMgmtDetail-action"),
      type: $div.attr("PolicyMgmtDetail-method"),
      data: tokenData,
      success: function (data)
      {
         var $target = $($div.attr("PolicyMgmtDetail-target"));
         var $newHtml = $(data);

         $target.replaceWith($newHtml);

         $newHtml.find(".toCollapse").slideCollapse();
      }
   };

   try
   {
      $.ajax(options);
   }
   catch (e)
   {
      throw e;
   }

   ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);

   // Single row Selection on table in multiple page
   if ($(this).hasClass("selected"))
   {
      $(this).removeClass("selected");
   }
   else
   {
      $("#searchGrid tr.selected").removeClass("selected");
      $(this).addClass("selected");
   }

   // Disable Save & Cancel after Row clicke and Enable New & Edit
   EnableToolbarControl("#btn_toolbar_new");
   EnableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");

   return false;
});

//*****************************************************************************
//*
//* Summary:
//*   Take a Form Id then return the content of the Form as an Object.
//*
//* Parameters:
//*   formId - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function GetFormData(formId)
{
   var formData = $(formId).serializeObject();
   return JSON.stringify(formData);
}

//*****************************************************************************
//*
//* Summary:
//*   Get Anti Forgery token from the Form.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
function getAntiForgeryToken()
{
   var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
   return "&" + $form.serialize();
}

//*****************************************************************************
//*
//* Summary:
//*   SerializeObject Function to select a Form by ID, serialize the content,
//*   clean it, and send it back as an object.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
(function ($)
{
   $.fn.serializeObject = function ()
   {

      var self = this,
         json = {},
         push_counters = {},
         patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
         };

      this.build = function (base, key, value)
      {
         base[key] = value;
         return base;
      };

      this.push_counter = function (key)
      {
         if (push_counters[key] === undefined)
         {
            push_counters[key] = 0;
         }
         return push_counters[key]++;
      };

      $.each($(this).serializeArray(), function ()
      {

         // skip invalid keys
         if (!patterns.validate.test(this.name))
         {
            return;
         }

         var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

         while ((k = keys.pop()) !== undefined)
         {

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), "");

            // push
            if (k.match(patterns.push))
            {
               merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // fixed
            else if (k.match(patterns.fixed))
            {
               merge = self.build([], k, merge);
            }

            // named
            else if (k.match(patterns.named))
            {
               merge = self.build({}, k, merge);
            }
         }

         json = $.extend(true, json, merge);
      });
      return json;
   };
})(jQuery);
