﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.PolicyMgmt.Controllers
{
   using System;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.QoskCloud.Web.Areas.PolicyMgmt.Models;
   using Qualanex.QoskCloud.Web.Areas.PolicyMgmt.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_POLICY")]
   public class PolicyMgmtController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult PolicyMgmtView()
      {
         var viewModel = new PolicyMgmtViewModel
         {
            Entitys = PolicyMgmtModel.GetPolicyEntities(true),
            ActionMenu = PolicyMgmtModel.GetActionMenu()
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnEntityClick(string entity)
      {
         var model = System.Web.Helpers.Json.Decode<PolicyEntity>(entity);

         return this.PartialView("_PolicyMgmtForm", model);
      }




      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }






      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted">Show all records</param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         var viewModel = new PolicyMgmtViewModel
         {
            Entitys = PolicyMgmtModel.GetPolicyEntities(isDeleted),
            ActionMenu = PolicyMgmtModel.GetActionMenu()
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.PartialView("_PolicyMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

   }
}
