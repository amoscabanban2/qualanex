﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProfileMgmtAreaRegistration.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.ProfileMgmt
{
   using System.Web.Mvc;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ProfileMgmtAreaRegistration : AreaRegistration
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AreaName => "ProfileMgmt";

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="context"></param>
      ///****************************************************************************
      public override void RegisterArea(AreaRegistrationContext context)
      {
         context.MapRoute(
             "ProfileMgmt_default",
             "ProfileMgmt/{controller}/{action}/{id}",
             new { action = "Index", id = UrlParameter.Optional }
         );
      }
   }
}
