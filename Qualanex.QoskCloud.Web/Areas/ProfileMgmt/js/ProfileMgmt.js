﻿//*****************************************************************************
//*
//* ProfileMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

//=============================================================================
// Global definitions
//-----------------------------------------------------------------------------
var editmode = false;
var textchanged = false;
var ProfileID;
var table;

//*****************************************************************************
//*
//* Summary:
//*   Page load functions.
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(document).ready(function()
{
   table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);
   InitialState();
});

//Refresh Profile detail procedure
$(document).on("click", "#btn_toolbar_refresh", function(e)
{
   //Refresh the data with current view/hide option
   var $isEnable = true;
   var $AttSrc = $("#btn_toolbar_hide").children().attr("src");
   if ($AttSrc.indexOf("Hidden(1)") >= 0)
   {
      $isEnable = false;
   }
   var $url = $("#btn_toolbar_hide").attr("data-method");
   var $TargetID = $("#ProfileGridSummary");

   $.ajax({
      type: "GET",
      url: $url,    //Controllers Function UserAccountView_Hide
      data: {
         'isEnable': $isEnable
      },
      success: function(data)
      {
         var $target = $("#ProfileGridSummary");
         var $newHtml = $(data);
         $target.replaceWith($newHtml);
         $newHtml.find("#searchGrid [data-row-id='" + ProfileID + "']").click();
         table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);
         if (!table.$("tr.selected").hasClass("selected"))
         {
            $newHtml.find("#searchGrid tbody").find("tr:first").click();
         }
      },
      error: function(data)
      {
         alert("Fail");
      }
   });
});

//Add New Profile procedure
$("#btn_toolbar_new").click(function()
{
   //Something New Need Add.
   $("#alertmessageid").text("New click");
   editmode = true;

   DisableEnableToolbarAfterAddNewAndEdit();
   alert("Add New User Action:=" + $(this).attr("data-method"));
   /*
       $.ajax({
           type: "POST",
           url: $(this).attr("data-method"),    //Controllers Function AddNewAccount
           data: {
               'FirstName' :"",
               'LastName' :"", 
               'Email' :"", 
               'PhoneNumber' :"",
               'FaxNumber' :"", 
               'ProfileCode' :"", 
               'ProfileName' :""
           },
           success: function (data) {
               alert("Success");
           },
           error: function (data) {
               alert("Fail");
           }
       })
   */
});

//Edit Profile detail procedure
$("#btn_toolbar_edit").click(function()
{
   $("#alertmessageid").text("edit click");
   editmode = true;

   DisableEnableToolbarAfterAddNewAndEdit();
   alert("Edit User Action:=" + $(this).attr("data-method"));
   /*
   $.ajax({
       type: "POST",
       url: $(this).attr("data-method"),    //Controllers Function EditAccount
       data: {
           'FirstName' :"",
           'LastName' :"", 
           'Email' :"", 
           'PhoneNumber' :"",
           'FaxNumber' :"", 
           'ProfileCode' :"", 
           'ProfileName' :""
       },
       success: function (data) {
           alert("Success");
       },
       error: function (data) {
           alert("Fail");
       }
   })
   */
});

//Save Profile detail procedure
$("#btn_toolbar_save").click(function()
{
   if (editmode && !textchanged)
   {
      // Error message "OK" button action
      $("#btdeletenok").hide();
      $("#btdeletenCancel").hide();
      $("#btdeletenOKCancel").show();
      $("#alertdeletemessageid").text("No Changes Detected");
      $("#confirmAlert").show();
      $("#btdeletenOKCancel,.alert-close").click(function()
      {
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         return;
      });
      alert("Save User Action:=" + $(this).attr("data-method"));

   } else
   {
      location.reload();
      InitialState();
   }
});

//Cancel Profile detail procedure
$("#btn_toolbar_cancel").click(function()
{

   if (textchanged)
   {
      $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
      $("#confirmAlert").show();

      // Error message "No" or "Close Windows" button action
      $("#btdeletenCancel, #btnok,.alert-close").click(function()
      {
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         return;
      });

      // Error message "Yes" button action
      $("#btdeletenok").click(function()
      {
         $("#btdeletenok").unbind("click");
         $("#confirmAlert").hide();
         $(".alert-popup").animate({ "top": "40%" });
         location.reload();
         InitialState();
         return;
      });

   } else
   {
      InitialState();
      return;
   }
});

//View disabled records/Hide disabled records Profile detail procedure
$("#btn_toolbar_hide").click(function()
{
   var isEnable = true;
   var $AttSrc = $("#btn_toolbar_hide").children().attr("src");
   var $newTit = "";
   var $newSrc = "";

   if ($AttSrc.indexOf("Hidden(1)") >= 0)
   {
      $newTit = "Show Disabled Records";
      $newSrc = $AttSrc.replace("Hidden(1)", "Hidden(0)");
      isEnable = true;
   } else
   {
      $newTit = "Hide Disabled Records";
      $newSrc = $AttSrc.replace("Hidden(0)", "Hidden(1)");
      isEnable = false;
   }
   $("#btn_toolbar_hide").attr("title", $newTit);
   $("#btn_toolbar_hide").children().attr("src", $newSrc);
   $("#btn_toolbar_hide").show();

   var $url = $("#btn_toolbar_hide").attr("data-method");
   var $TargetID = $("#ProfileGridSummary");
   $.ajax({
      type: "GET",
      url: $url,    //Controllers Function UserAccountView_Hide
      data: {
         'isEnable': isEnable
      },
      success: function(data)
      {
         var $target = $("#ProfileGridSummary");
         var $newHtml = $(data);
         $target.replaceWith($newHtml);
         $newHtml.find("#searchGrid tbody").find("tr:first").click();
         table = window.dtHeaders("#searchGrid", "order: [[1, 'asc']]", [], []);
      },
      error: function(data)
      {
         alert("Fail");
      }
   });
});

//Filter icon click action
$("#btn_toolbar_filter").click(function()
{
   alert("Filter icon Click Action under develop");
});

//Print icon click action
$("#btn_toolbar_print").click(function()
{
   alert("Print icon Click Action under develop");
});

//Update User detail procedure
$("#UpdateUserRecord").on("click", function()
{

   //ToDO: place focus on a feild then start to tab through it to make changes.
   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
   $("#btdeletenCancel,#btnok,.alert-close").click(function()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });
   $("#confirmAlert").show();//alert("Update");
   $(".alert-popup-body").show();
   $(".alert-popup").animate({ "top": "40%" });
   $("#ClearUserForm").hide();
   $("#UpdateUserRecord").hide();
});

//Update Group detail procedure
$("#UpdateGroupRecord").on("click", function()
{

   //ToDO: place focus on a feild then start to tab through it to make changes.
   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
   $("#btdeletenCancel,#btnok,.alert-close").click(function()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });
   $("#confirmAlert").show();//alert("Update");
   $(".alert-popup-body").show();
   $(".alert-popup").animate({ "top": "40%" });
   $("#ClearUserForm").hide();
   $("#UpdateUserRecord").hide();
});

//Update Group detail procedure
$("#UpdateProfileRecord").on("click", function()
{

   //ToDO: place focus on a feild then start to tab through it to make changes.
   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
   $("#btdeletenCancel,#btnok,.alert-close").click(function()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });
   $("#confirmAlert").show();//alert("Update");
   $(".alert-popup-body").show();
   $(".alert-popup").animate({ "top": "40%" });
   $("#ClearUserForm").hide();
   $("#UpdateUserRecord").hide();
});
//Use for Save changes poup functionality
$(".alert-close, #btnAlertStay,.close-btn").click(function()
{
   $(".alert-popup-body").hide();
   $(".alert-popup").animate({ "top": "40%" });

   $(".model-popup").hide();
});

//Set the Initial state of the active tool buttons
function InitialState()
{
   editmode = false;
   textchanged = false;
   EnableToolbarControl("#btn_toolbar_refresh");
   EnableToolbarControl("#btn_toolbar_new");
   EnableToolbarControl("#btn_toolbar_edit");
   //EnableToolbarControl("#btn_toolbar_hide");
   //    EnableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   if (!$("#searchGrid tbody").hasClass("selected"))
   {
      $("#searchGrid tbody").find("tr:first").click();
   }
}

//Function for enable toolbar control
window.EnableToolbarControl = function EnableToolbarControl(controlID)
{
   if (controlID === "#btn_toolbar_cancel")
   {
      $(controlID).css("color", "darkred");
   }
   $(controlID).children().attr("src", $(controlID).children().attr("src").replace("0", "1"));
   $(controlID).css("pointer-events", "auto");
   $(controlID).removeClass("lighttxt");
   $(controlID).addClass("darktxt");
};

//Function for disable toolbar control
window.DisableToolbarControl = function DisableToolbarControl(controlID)
{
   if (controlID === "#btn_toolbar_cancel")
   {
      $(controlID).css("color", "#3b8aBd");
   }
   $(controlID).children().attr("src", $(controlID).children().attr("src").replace("1", "0"));
   $(controlID).css("pointer-events", "none");
   $(controlID).removeClass("darktxt");
   $(controlID).addClass("lighttxt");
};

//Functions for disable enable toolbar control value after click
function DisableEnableToolbarAfterAddNewAndEdit()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   //DisableToolbarControl("#btn_toolbar_hide");
   //    DisableToolbarControl("#btn_toolbar_delete");
   EnableToolbarControl("#btn_toolbar_save");
   EnableToolbarControl("#btn_toolbar_cancel");
}

//*****************************************************************************
//*
//* Summary:
//*   Event handler for Summary Entity click.
//*
//* Parameters:
//*   e - Description not available.
//*
//* Returns:
//*   Description not available.
//*
//*****************************************************************************
$(document).on("click", ".entity-div-click", function(e)
{
   PloicyID = $(this).closest("tr").attr("data-row-id");

   var $div = $(this);
   var tokenData = $div.attr("profileDetail-data") + getAntiForgeryToken();
   var options = {
      async: true,
      url: $div.attr("profileDetail-action"),
      type: $div.attr("profileDetail-method"),
      data: tokenData,
      success: function(data)
      {
         var $target = $($div.attr("profileDetail-target"));
         var $newHtml = $(data);

         $target.replaceWith($newHtml);

         $newHtml.find(".toCollapse").slideCollapse();
      }
   };

   try
   {
      $.ajax(options);
   } catch (e)
   {
      throw e;
   }

   ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);

   //Single row Selection on table in multiple page
   if ($(this).hasClass("selected"))
   {
      $(this).removeClass("selected");
   }
   else
   {
      $("#searchGrid tr.selected").removeClass("selected");
      $(this).addClass("selected");
   }

   //Disable Save & Cancel after Row clicke and Enable New & Edit
   EnableToolbarControl("#btn_toolbar_new");
   EnableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");

   return false;
});

//Take a Form Id then return the content of the Form as an Object 
function GetFormData(formId)
{
   var formData = $(formId).serializeObject();
   return JSON.stringify(formData);
}

//Get Anti Forgery token from the Form
function getAntiForgeryToken()
{
   var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
   return "&" + $form.serialize();
}

//SerializeObject Function to select a Form by ID, serialize the content, clean it, and send it back as an object
(function($)
{
   $.fn.serializeObject = function()
   {

      var self = this,
          json = {},
          push_counters = {},
          patterns = {
             "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
             "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
             "push": /^$/,
             "fixed": /^\d+$/,
             "named": /^[a-zA-Z0-9_]+$/
          };


      this.build = function(base, key, value)
      {
         base[key] = value;
         return base;
      };

      this.push_counter = function(key)
      {
         if (push_counters[key] === undefined)
         {
            push_counters[key] = 0;
         }
         return push_counters[key]++;
      };

      $.each($(this).serializeArray(), function()
      {

         // skip invalid keys
         if (!patterns.validate.test(this.name))
         {
            return;
         }

         var k,
             keys = this.name.match(patterns.key),
             merge = this.value,
             reverse_key = this.name;

         while ((k = keys.pop()) !== undefined)
         {

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), "");

            // push
            if (k.match(patterns.push))
            {
               merge = self.build([], self.push_counter(reverse_key), merge);
            }

               // fixed
            else if (k.match(patterns.fixed))
            {
               merge = self.build([], k, merge);
            }

               // named
            else if (k.match(patterns.named))
            {
               merge = self.build({}, k, merge);
            }
         }

         json = $.extend(true, json, merge);
      });
      return json;
   };
})(jQuery);
