﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProfileMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.ProfileMgmt.Controllers
{
   using System;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.QoskCloud.Web.Areas.ProfileMgmt.Models;
   using Qualanex.QoskCloud.Web.Areas.ProfileMgmt.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_PROFILE")]
   public class ProfileMgmtController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ProfileMgmtView()
      {
         this.ModelState.Clear();

         var viewModel = new ProfileMgmtViewModel
         {
            ProfileModels = ProfileMgmtModel.GetProfileEntities(true),
            ActionMenu = ProfileMgmtModel.GetActionMenu()
         };

         if (viewModel.ProfileModels.Any())
         {
            viewModel.SelectedEntity = viewModel.ProfileModels[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnEntityClick(string entity)
      {
         this.ModelState.Clear();

         return this.PartialView("_ProfileMgmtForm", System.Web.Helpers.Json.Decode<ProfileEntity>(entity));
      }




      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }






      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted">Show all records</param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new ProfileMgmtViewModel
         {
            ProfileModels = ProfileMgmtModel.GetProfileEntities(isDeleted),
            ActionMenu = ProfileMgmtModel.GetActionMenu()
         };

         if (viewModel.ProfileModels.Any())
         {
            viewModel.SelectedEntity = viewModel.ProfileModels[0];
         }

         return this.PartialView("_ProfileMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

   }
}
