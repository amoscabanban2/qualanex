﻿$(function ()
{
   //*****************************************************************************************
   //*
   //* WrhsSort.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the Final Sort partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************
   var baseURL = "/WrhsSort";
   var table;
   var binItemId = "";
   var stationId = "";
   var scannedGaylord = [];
   var requestedGaylord = [];
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   var location = "";
   var cage = ["RED_BIN", "YLW_BIN", "BLU_BIN", "BLK_BIN"];
   var floor = ["RED_BIN", "YLW_BIN"];

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   var action;
   $(document).ready(function ()
   {
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
      {
         stationId = data;
      });
      stationId = stationId === "" ? getCookie("MachineName") : stationId;
      sidebar.init();
      getBinventoryTabDetails(sidebar.getInboundContainer().containerId);
      sidebar.beforeClose(function (d)
      {
         return $(d).find("input[name='Attribute']").val() === "I"
            ? ($(".searchGrid_WrhsSrtDetails-div-click").length === $(".searchGrid_WrhsSrtDetails-div-click.done").length)
            : true;
      });
      sidebar.beforeOpen(function (d, s)
      {
         location = location === "" ? getMachineLocation().Location : location;

         //do not try to open container in sidebar if the PC is not registered
         if (location === "" || location === undefined)
         {
            return "PC Registration Not Found. Please Contact Administrator.";
         }

         var tp = getValidateType(d).WrhsContr.Type;
         return s === "I"
            ? tp === null
               ? "Container is not in the correct type"
               : (location === "3" || location === "4" || location === "5")
                  ? cage.includes(tp)
                     ? ""
                     : "Container is not in the correct location"
                  : location === "0"
                     ? floor.includes(tp)
                        ? ""
                        : "Container is not in the correct location"
                     : "Container is not in the correct location"
            : "";
      });
      sidebar.beforeRequest(function (container)
      {
         if ($(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='isForeignContainer']").attr("exchange-value") === "true")
         {
            container.profileCode = 0;
         }
      });

      sidebar.onRequest(function () { updateRequestedField(); });
      sidebar.onBinOpen(function () { getBinventoryTabDetails(sidebar.getInboundContainer().containerId); });
      sidebar.onBinClose(function () { getBinventoryTabDetails(""); });
      sidebar.onBinPause(function () { getBinventoryTabDetails(""); });
      window.addInventoryMethod(function ()
      {
         var container = sidebar.getInboundContainer();
         getBinventoryTabDetails(container !== undefined ? container.containerId : "");
      });
      window.addSelectMethod(function (e)
      {
         if ($(e).closest("tr").hasClass("done"))
         {
            $("#NDC").attr({
               "readonly": "readonly", "disabled": "disabled"
            });

            $("#ItemGUID").focus();
         }
         else
         {
            $("#ItemGUID,#OutboundContainerID").removeAttr("readonly").removeAttr("disabled");
         }

         $("#OutboundContainerID").removeAttr("readonly").removeAttr("disabled");
         $("#OutboundContainerID").focus();
         return true;
      });
      window.addUnselectMethod(function (e)
      {
         $("#NDC").attr({ "readonly": "readonly", "disabled": "disabled" });
         $("#OutboundContainerID").attr({ "readonly": "readonly", "disabled": "disabled" });
         $("#FinalSortBody .inputEx").val("");
         $("#FinalSortBody label").text("");
         $("#WrhsContrType").text("Bin");
         $("#ItemGUID").val("").closest("input").focus();
         return true;
      });

      window.addClickGrid_ControlMethod(function (e)
      {
         if ($(e).closest("tr").hasClass("MgmtEntity"))
         {
            $(e).closest("tr").removeClass("MgmtEntity");
            return false;
         }

         action = 0;
         return true;
      });
      //$("#searchGrid_WrhsSrtDetails tbody").find("tr:eq(0)").click();
   });

   function getCookie(cname)
   {
      var name = cname + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++)
      {
         var c = ca[i];

         while (c.charAt(0) === ' ')
         {
            c = c.substring(1);
         }

         if (c.indexOf(name) === 0)
         {
            return c.substring(name.length, c.length);
         }
      }

      return "";
   }

   function getMachineLocation()
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "machineName=" + stationId + getAntiForgeryToken(),
         async: false,
         url: baseURL + baseURL + "/GetMachineLocation",
         success: function (data)
         {
         }
      });

      return request.responseJSON;
   }
   function getValidateType(containerId)
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "containerId=" + containerId + getAntiForgeryToken(),
         async: false,
         url: baseURL + baseURL + "/GetContainerType",
         success: function (data)
         {
         }
      });

      return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   saveAndValidate      - Description not available.
   //*   field                - Description not available.
   //*   data                 - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        //if (field === "ItemGUID")
                        //{
                        //   $("#ItemGuid").val(data);
                        //}
                        $("#" + field).val(data);
                     }
               }

               break;
            }

         case "SELECT":
            {
               $("#" + field).val(data);
               break;
            }

         case "LABEL":
            {
               //console.log("SELECT::" + field + "::" + data);
               if (field !== "TimeProcessing" && field !== "StationId")
               {
                  $("#" + field).text(data);
               }
               break;
            }
      }
   }

   function updateRequestedField()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
      {
         $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='OutboundContainerID']").html("Requested");
         $(".searchGrid_WrhsSrtDetails-div-click.selected").addClass("Requested");
         requestedGaylord.push({ "itemGuid": $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").attr("exchange-value") });
         //$(".searchGrid_WrhsSrtDetails-div-click.selected").click();
      }
   }

   action = 0;
   setInterval(function ()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected") && action === 0
         && $(".searchGrid_WrhsSrtDetails-div-click.selected").not(".done").length > 0)
      {
         var name = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='Destination']").attr("exchange-value");
         name = name.indexOf("-") > 0 ? name.replace("-", " Dash ") : name;
         $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + name, {}, function () { });
      }
   }, 5000);

   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Recalls and Hazards associated with bin Id.
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getBinventoryTabDetails(binId)
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "binId=" + binId + "&stationId=" + stationId + "&location=" + location + getAntiForgeryToken(),
         url: baseURL + baseURL + "/GetBinventoryDetails",
         async: false,
         success: function (data)
         {
            var $target = $("#WrhsSortgDetailsForm");
            var $newHtml = $(data);

            if (scannedGaylord.length > 0)
            {
               for (var items = 0; items < scannedGaylord.length; items++)
               {
                  var itemGuid = scannedGaylord[items].ItemGuid;
                  $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='ItemGUID'][exchange-value='" + scannedGaylord[items].ItemGuid + "']").closest("tr").addClass("done").closest("tr").css("text-decoration", "line-through");
               }
            }

            if (requestedGaylord.length > 0)
            {
               $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='ItemGUID']").each(function ()
               {
                  if (requestedGaylord.find(c => c.itemGuid === $(this).attr("exchange-value")) !== undefined)
                  {
                     $(this).closest("tr").addClass("Requested");
                     $(this).closest("tr").find("td[exchange='OutboundContainerID']").html("Requested");
                  }
               });
            }

            $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='Sorted']").closest("td").each(function ()
            {
               if (parseInt($(this).attr("exchange-value")) === 1)
               {
                  $(this).closest("tr").addClass("done");
                  $(this).closest("tr").css("text-decoration", "line-through");
               }
            });

            if ($newHtml.find(".searchGrid_WrhsSrtDetails-div-click.done").length === $newHtml.find(".searchGrid_WrhsSrtDetails-div-click").length)
            {
               $newHtml.find("input#Reconcile_Close").val("Close");
            }

            $(".formMax input[type=text]").val("");
            $target.replaceWith($newHtml);
            $("#ItemGUID").removeAttr('disabled').removeAttr("readonly");
            binItemId = binId;
            $("#ItemCount").text(0 + " of " + $(".searchGrid_WrhsSrtDetails-div-click").length);
            $("#Station").text(stationId);
            $("#Destination").text("");
            $("#ItemGUID").focus();
         }
      });
   }

   var checkOpenContainerGhost = function (itemGuid, category, type)
   {
      var container = category === "" ? sidebar.getContainerByType(type) : sidebar.getContainerByType(type, category);

      if (!container.isOpen())
      {
         container.open($("#OutboundContainerID").val(), function (data)
         {
            if (data.success)
            {
               putContainer();
            }
            else
            {
               $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + data.error, {}, function () { });
               action = 0;
            }
         });
      }
      else
      {
         putContainer();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      var binId = binItemId.length > 0
         ? binItemId
         : $(".searchGrid_WrhsSrtDetails-div-click").length > 0
            ? $(".searchGrid_WrhsSrtDetails-div-click:first td[exchange='InboundBoxID']").closest("td").attr("exchange-value")
            : "";

      if (binId === "" || binId === undefined)
      {
         return false;
      }

      if (sidebar.containers.find(c => c.attribute === 'I' && c.isOpen()) === undefined)
      {
         return false;
      }

      if (requestedGaylord.length > 0)
      {
         requestedGaylord = [];
      }

      $("#FinalSortBody .inputEx").val("");
      $("#FinalSortBody label").text("");
      $("#OutboundContainerID").attr({
         "readonly": "readonly", "disabled": "disabled"
      });
      getBinventoryTabDetails(binId);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_CLEAR", function ()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click").length > 0 && $(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
      {
         //$(".searchGrid_WrhsSrtDetails-div-click.selected").closest("tr").click();
         binventoryRowClick($(".searchGrid_WrhsSrtDetails-div-click.selected").closest("tr"));
         $("#OutboundContainerID").attr({ "readonly": "readonly", "disabled": "disabled" });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SUBMIT", function ()
   {
      if (sidebar.containers.find(c => c.attribute === "I" && c.isOpen()) === undefined)
      {
         return false;
      }

      if ($("#OutboundContainerID").val() !== "" && $("#ItemGUID").length > 0 && $(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
      {
         //keyup event checks for any "done" binventory items - aka already sorted
         //trigger the keyup event
         var ex = jQuery.Event("keyup");
         ex.keyCode = 13;
         ex.key = "Enter";
         $("#OutboundContainerID").trigger(ex);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set focus on the outbound container ID textbox after pop-up messages are closed (gaylord request, scanning an invalid gaylord)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_continue_container_label, #btn_continue", function ()
   {
      $("#OutboundContainerID").focus().select();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "input.inputEx", function (e)
   {
      if (e.keyCode === 13)
      {
         if ($(this).attr("id") === "OutboundContainerID")
         {
            if (sidebar.containers.find(c => c.attribute === 'I' && c.isOpen()) === undefined)
            {
               return false;
            }

            if ($(this).val() !== "")
            {
               if ($("#ItemGUID").length > 0 && $(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
               {
                  action = 1;
                  var itmGUID = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").closest("td").attr("exchange-value");
                  var $row = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID'][exchange-value='" + itmGUID + "']").closest("tr");

                  if ($row.hasClass("done"))
                  {
                     if ($row.find("td[exchange='OutboundContainerID']").attr("exchange-value").toUpperCase() === $(this).val().toUpperCase())
                     {
                        var speech = $row.find("td[exchange='Destination']").attr("exchange-value");
                        speech = speech.indexOf("-") > 0 ? speech.replace("-", " Dash ") : speech;
                        $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + speech, {}, function () { });
                        return false;
                     }
                  }

                  checkOpenContainer();
               }
            }
         }
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Pop-up shows up when a mis-sorted item was placed in the correct container
   //*   After pop-up is closed, focus on the outbound container field (see QoskSegment.Binventory.js)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_missort_continue", function ()
   {
      $("#OutboundContainerID").focus();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get open gaylord for item (if exists) or request one if there isn't one open
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#searchGrid_WrhsSrtDetails tr.MgmtEntity", function ()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click.selected.done").length > 0)
      {
         return false;
      }

      var type = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='DestinationType']").attr("exchange-value");

      if (type.includes("GAYLRD"))
      {
         var profileCode = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='MfgProfileCode']").attr("exchange-value");
         var wasteProfileId = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='WasteProfileId']").attr("exchange-value");
         var recallId = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='RecallID']").attr("exchange-value");
         var ctrlNbr = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='CtrlNumber']").attr("exchange-value");

         if ($(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='SpecialHandling']").attr("exchange-value") !== "True" || (recallId !== null && recallId !== 0 && recallId !== undefined))
         {
            profileCode = 0;
         }

         //This is done more so for C345 items, otherwise each of those controls would get a unique ctrl number (C3-only gaylord, for example)
         switch (ctrlNbr)
         {
            case "0":
               ctrlNbr = 0;
               break;
            case "2":
               ctrlNbr = 2;
               break;
            case "3":
            case "4":
            case "5":
               ctrlNbr = 345;
               break;
         }

         //grab new container based on info passed, show the container in sidebar
         var container = sidebar.getGaylord(ctrlNbr, profileCode, recallId, wasteProfileId, type);
         container.show();
      }
   });

   var checkOpenContainer = function ()
   {
      var type = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='DestinationType']").attr("exchange-value");
      var category = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='WasteProfileId']").attr("exchange-value");
      var itemIndex = $(".searchGrid_WrhsSrtDetails-div-click.selected").index();
      var containerId = $("#OutboundContainerID").val();
      var container = sidebar.getContainerByType(type, category, containerId);

      if ($("#OutboundContainerID").attr("disabled") !== undefined && $("#OutboundContainerID").attr("disabled") !== null)
      {
         return;
      }

      $("#OutboundContainerID").disable();

      if (!container.isOpen())
      {
         container.open(containerId, function (data)
         {
            if (data.success)
            {
               //bin open methods get new binventory list, which means we lose the selected class
               //gaylord open methods do not exist here, which means we don't get a new list, and thus the selected class is maintained
               if (!$(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
               {
                  binventoryRowClick($(".searchGrid_WrhsSrtDetails-div-click").eq(itemIndex));
               }

               $("#OutboundContainerID").val(containerId);
               putContainer();
            }
            else
            {
               $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + data.error, {}, function () { });
               sidebar.getContainerByType(type, category).show();
               action = 0;

               //do not allow user to request a bin container, only gaylords
               if (!type.includes("GAYLRD"))
               {
                  $("#Sidebar [name=containerRequest]").disable();
               }

               $("#OutboundContainerID").enable();
            }
         });
      }
      else
      {
         putContainer();
      }
   }
   window.checkOpenContainer = checkOpenContainer;

   var putContainer = function ()
   {
      var item = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").closest("td").attr("exchange-value");
      var type = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='DestinationType']").attr("exchange-value");
      var category = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='WasteProfileId']").attr("exchange-value");
      var container = sidebar.getContainerByType(type, category, $("#OutboundContainerID").val());

      container.put(item, function (data)
      {
         if (data.success)
         {
            action = 0;

            if ($(".searchGrid_WrhsSrtDetails-div-click.selected").hasClass("Requested"))
            {
               requestedGaylord.splice(requestedGaylord.findIndex(c => c.itemGuid === $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").attr("exchange-value")), 1);
               $(".searchGrid_WrhsSrtDetails-div-click.selected").removeClass("Requested");
            }

            getBinventoryTabDetails(binItemId);
            $("#FinalSortBody .inputEx").val("");
            $("#FinalSortBody label").text("");
            $("#OutboundContainerID").attr({ "readonly": "readonly", "disabled": "disabled" });
            $("#ItemGUID").focus();
         }
         else
         {
            //$(this).setErrorMessage(data.error, "Error Message", "ok");
            $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + data.error, {}, function () { });
            action = 0;
            $("#OutboundContainerID").enable();
         }
      });
   }
});