///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsSortViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels
{
   using System.Collections.Generic;

   using Model;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsSortViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_SORT";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Sorting record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsSortModel SelectedSortingModel { get; set; } = new WrhsSortModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Sorting list for the Sorting detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<BinventoryDataAccess> SortingDetails { get; set; } = new List<BinventoryDataAccess>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public SideBarItems sidebarOptions { get; set; } = new SideBarItems();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the sidebar object list.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<Dictionary<string, IEnumerable<object>>> sidebarObjects { get; set; } = new List<Dictionary<string, IEnumerable<object>>>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the Model List.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<string> ModelList { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the Gaylord list.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsSortModel> GaylordList { get; set; } = new List<WrhsSortModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UpaViewModel UPAControl { get; set; }
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class SideBarItems
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<object> ModelList { get; set; } = new List<object>();
   }

}