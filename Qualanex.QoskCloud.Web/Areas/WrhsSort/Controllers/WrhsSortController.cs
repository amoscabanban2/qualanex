﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsSortController.cs">
///   Copyright (c) 2017-2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;
   using System.Web;
   using Entity;
   using Product;
   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_SORT")]
   public class WrhsSortController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Final Sort View.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsSortView()
      {
         this.ModelState.Clear();
         var objReturnLoginUserEntity =
            (ReturnLoginUserEntity)
            QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

         int userProfileCode = -99;

         if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
         {
            userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         var viewModel = new WrhsSortViewModel
         {
            ActionMenu = WrhsSortModel.GetActionMenu(),
            UPAControl = new Web.ViewModels.UpaViewModel
            {
               UpaApplication = UpaDataAccess.GetUpaModel("WAREHS_SORT",
                                                           objReturnLoginUserEntity.UserReturnData.UserID,
                                                            userProfileCode),
               Binventory = new Web.ViewModels.BinventoryViewModel
               {
                  Options = new Web.ViewModels.BinventoryOptions()
                  {
                     RowProperty = new BinventoryDataAccess(),
                  },
                  Rows = new List<BinventoryDataAccess>()
               }
            }
         };

         var pageViewId = Guid.NewGuid();
         QoskSessionHelper.SetSessionData(Utility.Constants.UpaApplicationKey + pageViewId, viewModel.UPAControl.UpaApplication);

         this.ViewBag.pageViewId = pageViewId;

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get BinventoryDetails.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetBinventoryDetails(string binId, string stationId, string location)
      {
         this.ModelState.Clear();
         stationId= string.IsNullOrWhiteSpace(stationId)
                           ? Sidebar.SidebarCommon.GetMachineName(this.Request)
                           : stationId;
         var items = string.IsNullOrWhiteSpace(binId)
                        ? new List<BinventoryDataAccess>()
                        : BinventoryDataAccess.GetBinventoryList(binId, stationId, ProductCommon.GetUserState(), "WAREHS_SORT");
         //WrhsSortModel.GetItemList(binId, this.User.Identity.Name, stationId, ProductCommon.GetUserState(), location);         
         var viewModel = new WrhsSortViewModel
         {
            ActionMenu = WrhsSortModel.GetActionMenu(),
            SortingDetails = items,
            UPAControl = new Web.ViewModels.UpaViewModel
            {
               Binventory = new Web.ViewModels.BinventoryViewModel
               {
                  Options = new Web.ViewModels.BinventoryOptions()
                  {
                     RowProperty = new BinventoryDataAccess(),
                  },
                  Rows = items
               }
            }
         };

         viewModel.ModelList = viewModel.sidebarObjects.Select(c => c.Keys.FirstOrDefault()).Distinct().ToList();

         return this.PartialView("_WrhsSortBinProcessing", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get machine Location.
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetMachineLocation(string machineName)
      {
         machineName = string.IsNullOrWhiteSpace(machineName) ? Sidebar.SidebarCommon.GetMachineName(this.Request) : machineName;
         var strCheck = WrhsSortModel.GetMachineLocation(machineName);

         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(strCheck),
            Error = string.IsNullOrWhiteSpace(strCheck),
            Location = strCheck
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Get container type.
      /// </summary>
      /// <param name="containerId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetContainerType(string containerId)
      {
         var strCheck = WrhsSortModel.GetContainerType(containerId);
         return this.Json(new
         {
            Success = strCheck != null,
            Error = strCheck == null,
            WrhsContr = strCheck
         });
      }

   }
}
