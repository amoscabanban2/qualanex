﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsContrMgmtViewModel.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsContrMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_CONTR";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first RoleConfig record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsContrEntity SelectedEntity { get; set; } = new WrhsContrEntity();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RoleConfig list for the RoleConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsContrEntity> Entities { get; set; } = new List<WrhsContrEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }
}
