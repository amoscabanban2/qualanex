﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsContrMgmtController.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Models;
   using Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_CONTR")]
   public class WrhsContrMgmtController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsContrMgmtView()
      {
         var viewModel = new WrhsContrMgmtViewModel
         {
            Entities = WrhsContrMgmtModel.GetEntities(false),
            ActionMenu = WrhsContrMgmtModel.GetActionMenu()
         };

         if (viewModel.Entities.Any())
         {
            viewModel.SelectedEntity = viewModel.Entities[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }

   }
}
