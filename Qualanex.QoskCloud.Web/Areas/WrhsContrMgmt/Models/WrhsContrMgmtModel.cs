///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsContrMgmtModel.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Models
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   using Qualanex.QoskCloud.Web.Common;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.QoskCloud.Utility;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsContrMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public WrhsContrMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~WrhsContrMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsContrEntity> GetEntities(bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from entity in cloudEntities.WrhsContr
                       select new WrhsContrEntity
                       {
                          WrhsContrID = entity.WrhsContrID,
                          Type = entity.Type
                          //Status = entity.Status
                       }).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = string.Empty;
         char[] spComma = { ',' };
         char[] spColon = { ':' };

         if (inStr != null)
         {
            foreach (var onePair in from oneField in inStr.Replace("\"", string.Empty).Split(spComma)
                                    select oneField.Split(spColon) into onePair
                                    let id = onePair[0].Trim()
                                    where id.Length > 0
                                    where id == chkStr
                                    select onePair)
            {
               return onePair[1].Trim();
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="contrWasteProfileID"></param>
      /// <param name="ctrlNo"></param>
      /// <param name="profileCode"></param>
      /// <param name="typeRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CreateGaylord(string contrWasteProfileID, int ctrlNo, int profileCode, string typeRequest = "")
      {
         var result = string.Empty;

         try
         {
            var contrCaption = string.Empty;
            var contrType = string.Empty;
            var contrMfg = string.Empty;
            var contrRCRA = string.Empty;
            var contrDate = DateTime.Today;

            using (var cloudEntities = new QoskCloud())
            {
               // ...
               ///////////////////////////////////////////////////////////////////////////////
               var cntrApproval = (from entity in cloudEntities.WasteStreamProfile
                                   where entity.WasteProfileID == contrWasteProfileID
                                   select entity.ApprovalCode).FirstOrDefault();

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               var cntrSeverity = (from wcpr in cloudEntities.WasteCodeProfileRel
                                   join wcd in cloudEntities.WasteCodeDict on wcpr.WasteCode equals wcd.Code
                                   where wcpr.WasteStreamProfileID == contrWasteProfileID
                                   select wcd.SeverityOrder).FirstOrDefault();

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               var contrCtrl = ctrlNo == 2
                                 ? "低TRLNO=C%20II"
                                 : ctrlNo == 345
                                    ? "低TRLNO=C%0AIII%20IV%20V"
                                    : string.Empty;

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               contrMfg = profileCode == 0
                                 ? "Multiple%20Manufacturers"
                                 : (from entity in cloudEntities.Profile
                                    where entity.ProfileCode == profileCode
                                    select entity.Name).FirstOrDefault();

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               if (cntrSeverity > 3)
               {
                  contrCaption = "Non-Hazardous%20Waste";
                  contrType = string.IsNullOrEmpty(typeRequest)
                     ? ctrlNo == 2           // Control 2 => BLK_GAYLRD
                        ? Constants.BlackGaylord
                        : ctrlNo == 345      // Control 3,4 or 5 => ORG_GAYLRD
                           ? Constants.OrangeGaylord
                           : profileCode != 0
                              ? Constants.SmallGaylord
                              : Constants.LargeGaylord
                     : typeRequest;
               }
               else
               {
                  contrCaption = "Hazardous%20Waste";
                  contrType = string.IsNullOrEmpty(typeRequest)
                     ? Constants.WasteGaylord
                     : typeRequest;

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  var rcra = (from entity in cloudEntities.WasteCodeProfileRel
                              where entity.WasteStreamProfileID == contrWasteProfileID
                              select new
                              {
                                 entity.WasteCode
                              }).ToList();

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  if (rcra.Count > 1)
                  {
                     contrRCRA = "刪CRA=RCRA%20Codes:%0A";

                     foreach (var x in rcra.Select((value, i) => new { i, value }))
                     {
                        var sep = x.i == rcra.Count - 1
                           ? string.Empty
                           : (x.i + 1) % 7 == 0
                              ? "%0A"
                              : ",";

                        contrRCRA += $"{x.value.WasteCode}{sep}";
                     }
                  }
               }

               result = $"CONTRID={CreateDBContainer(contrType, contrWasteProfileID, string.Empty, ctrlNo, profileCode, contrDate)}低APTION={contrCaption}{contrCtrl}你PPRVLNO={cntrApproval}冶FG={contrMfg}伶ATEOPEN={contrDate:dd-MMM-yyyy}{contrRCRA}";
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="recallID"></param>
      /// <param name="contrWasteProfileID"></param>
      /// <param name="ctrlNo"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CreateGaylord(string recallID, string contrWasteProfileID, int ctrlNo)
      {
         var result = string.Empty;

         try
         {
            var contrCaption = string.Empty;
            var contrNDC = string.Empty;
            var contrLotNo = string.Empty;
            var contrDate = DateTime.Today;

            using (var cloudEntities = new QoskCloud())
            {
               var recallNo = Convert.ToInt64(recallID);

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               var recallRec = (from entityR in cloudEntities.Recall
                                join entityP in cloudEntities.Profile on entityR.ManufacturerProfileCode equals entityP.ProfileCode
                                where entityR.RecallID.Equals(recallNo) && entityR.IsDeleted.Equals(false)
                                select new
                                {
                                   entityR.RecallNumber,
                                   entityP.ProfileCode,
                                   entityP.Name
                                }).FirstOrDefault();

               // Is this a real recall?
               if (recallRec != null)
               {
                  contrCaption = $"Recall%20Number:%20{recallRec.RecallNumber}";

                  var recallProd = (from entityRPL in cloudEntities.RecallProductRel
                                    join entityP in cloudEntities.Product on entityRPL.ProductID equals entityP.ProductID
                                    where entityRPL.RecallID.Equals(recallNo)
                                    select new
                                    {
                                       entityP.ProductID,
                                       entityP.NDC,
                                       entityP.UPC,
                                       entityP.NDCUPCWithDashes,
                                       entityRPL.AllLots
                                    }).ToList();

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  var cntrApproval = (from entity in cloudEntities.WasteStreamProfile
                                      where entity.WasteProfileID.Equals(contrWasteProfileID)
                                      select entity.ApprovalCode).FirstOrDefault();

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  var contrCtrl = ctrlNo == 2
                     ? "低TRLNO=C%20II"
                     : ctrlNo == 345
                        ? "低TRLNO=C%0AIII%20IV%20V"
                        : string.Empty;

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  contrNDC = "冷DC=";

                  foreach (var x in recallProd.Select((value, i) => new { i, value }))
                  {
                     var sep = x.i == recallProd.Count - 1
                        ? string.Empty
                        : (x.i + 1) % 3 == 0
                           ? "%0A"
                           : ",";

                     contrNDC += $"{x.value.NDCUPCWithDashes}{sep}";
                  }

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  contrLotNo = recallProd.Any(x => x.AllLots)
                     ? "兵OTNO=Multiple%20Lots"
                     : string.Empty;

                  if (string.IsNullOrEmpty(contrLotNo))
                  {
                     var lotList = (from r in recallProd from s in (from entity in cloudEntities.Lot where entity.ProductID.Equals(r.ProductID) && entity.RecallID == recallNo && entity.IsDeleted.Equals(false) select new { entity.LotNumber }).ToList() select s.LotNumber).ToList();

                     if (lotList.Count > 10)
                     {
                        contrLotNo = "兵OTNO=Multiple%20Lots";
                     }
                     else
                     {
                        contrLotNo = "兵OTNO=";

                        foreach (var x in lotList.Select((value, i) => new { i, value }))
                        {
                           var sep = x.i == lotList.Count - 1
                              ? string.Empty
                              : (x.i + 1) % 2 == 0
                                 ? "%0A"
                                 : ",";

                           contrLotNo += $"{x.value}{sep}";
                        }
                     }
                  }

                  // ...
                  ///////////////////////////////////////////////////////////////////////////////
                  result = $"CONTRID={CreateDBContainer(Constants.RecallGaylord, contrWasteProfileID, recallID, ctrlNo, 0, contrDate)}低APTION={contrCaption}{contrCtrl}你PPRVLNO={cntrApproval}冶FG={recallRec.Name}伶ATEOPEN={contrDate:dd-MMM-yyyy}{contrNDC}{contrLotNo}";
               }
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="contrType"></param>
      /// <param name="contrWasteProfileID"></param>
      /// <param name="contrAssocID"></param>
      /// <param name="ctrlNo"></param>
      /// <param name="profileCode"></param>
      /// <param name="contrDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string CreateDBContainer(string contrType, string contrWasteProfileID, string contrAssocID, int ctrlNo, int profileCode, DateTime contrDate)
      {
         var contrID = string.Empty;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               // Get the database ID for this new container
               ///////////////////////////////////////////////////////////////////////////////
               while (string.IsNullOrEmpty(contrID))
               {
                  var tempID = Methods.NextId(DBBase.QIPS);

                  var tempRec = (from entity in cloudEntities.WrhsContr
                                 where entity.WrhsContrID.Equals(tempID)
                                 select entity.WrhsContrID).FirstOrDefault();

                  contrID = string.IsNullOrEmpty(tempRec) ? tempID : string.Empty;
               }

               // ...
               ///////////////////////////////////////////////////////////////////////////////
               var wrhsContr = new WrhsContr
               {
                  WrhsContrID = contrID,
                  WasteProfileID = contrWasteProfileID,
                  AssocID = contrAssocID,
                  Type = contrType,
                  CtrlNo = ctrlNo,
                  ProfileCode = profileCode,
                  IsShareable = true,
                  CreatedBy = "web",
                  CreatedDate = contrDate
               };

               cloudEntities.WrhsContr.Add(wrhsContr);
               cloudEntities.SaveChanges();

               var wrhsContrStatusHist = new WrhsContrStatusHist
               {
                  WrhsContrID = contrID,
                  WrhsContrAttribCode = "O",
                  WrhsInstID = "QNEX00000000",
                  AppCode = "WAREHS_CONTR",
                  StatusCode = "A",
                  CreatedBy = "web",
                  CreatedDate = contrDate
               };

               cloudEntities.WrhsContrStatusHist.Add(wrhsContrStatusHist);
               cloudEntities.SaveChanges();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return contrID;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the waste stream profile ID based on the control number (non-haz only)
      /// </summary>
      /// <returns>
      ///   WasteStreamProfileID
      /// </returns>
      ///****************************************************************************
      public static string GetWasteStreamProfileId(int ctrlNumber)
      {
         var wasteStream = "";


         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               wasteStream = (from wcpr in cloudEntities.WasteCodeProfileRel
                              where wcpr.WasteCode == (ctrlNumber == 0 ? "Q999" : "Q998")
                              select wcpr.WasteStreamProfileID).First();
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }
         }


         return wasteStream;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }
}
