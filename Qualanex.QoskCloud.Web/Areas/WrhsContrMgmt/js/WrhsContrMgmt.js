﻿//*****************************************************************************
//*
//* WrhsContrMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------


   //#############################################################################
   // Global functions
   //#############################################################################


   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function()
   {
      console.log("WrhsContrMgmt::ready()");

      // Initialize the state of the Actionbar
      ///////////////////////////////////////////////////////////////////////////////
      window.setEditMode(false);

      window.enableActionbarControl("#CMD_REFRESH", true);
      window.enableActionbarControl("#CMD_PRINT", true);

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      $("input").attr("autocomplete", "off");
   });

   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function()
   {
      console.log("Refresh under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function()
   {
      console.log("New under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function()
   {
      // Once a UserName has been issued, we can no longer change it
      $("#ID_USERNAME").prop("readonly", true);

      console.log("Edit under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function()
   {
      console.log("Save under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function()
   {
      console.log("Cancel under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function()
   {
      console.log("Print under development");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRetired = function()
   {
      console.log("Retired under development");
   }

});
