﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UpaDataAccess.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   This class implements database queries and updates for use in UPA applications or other
/// applications requiring product related queries.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------


namespace Qualanex.QoskCloud.Web.Areas.Product
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;
   using System.Runtime.Caching;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;

   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
   using Qualanex.QoskCloud.Entity;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.StoredSql;
   using Qualanex.Qosk.Library.Model.UPAModel;
   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Sidebar;
   using Qualanex.QoskCloud.Web.Areas.WrhsCtrl2.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   using Logger = Qualanex.Qosk.Library.Common.Logger.Logger;
   using ItemNonCreditRel = Qualanex.Qosk.Library.Model.DBModel.ItemNonCreditRel;
   using ProductNotification = Qualanex.QoskCloud.Web.Areas.Product.Models.ProductNotification;
   using WasteCode = Qualanex.QoskCloud.Web.Areas.Product.Models.WasteCode;
   using System.Data.SqlClient;
   using System.Data.SqlTypes;
   using System.Data;
   using Antlr.Runtime.Misc;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   internal class UpaDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   Provides the upa application model 
      /// </summary>
      /// <param name="appCode">The application code</param>
      /// <param name="userId">The logged in user loading the application</param>
      /// <param name="userProfile">the profile code for the user loading the application</param>
      /// <returns></returns>
      ///****************************************************************************
      internal static UPAApplication GetUpaModel(string appCode, long? userId, int? userProfile)
      {
         var dbModel = new DBModel("name=QoskCloud");
         var model = new UPAApplication();

         return model.Fetch(ref dbModel, userId.ToString(), userProfile.ToString(), appCode); ;
      }

      ///****************************************************************************
      /// <summary>
      ///   Query the database for search results given entered information.
      /// </summary>
      /// <param name="request">model containing the request criteria</param>
      /// <param name="getImprint">
      ///   Specifies whether imprint and image groups should be included. Set false
      ///   for faster search execution.
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      internal static SearchResult GetSearch(SearchRequest request, bool getImprint = true)
      {
         if (request == null || request.IsDefault())
         {
            return new SearchResult()
            {
               ResultView = ResultView.List,
               Message = "Perform a search to see results"
            };
         }

         Guid itemGuid;

         if (Guid.TryParseExact(request.Product.ScannedBarcode, "D", out itemGuid))
         {
            var item = GetItem(itemGuid);

            if (item != null)
            {
               request.Product.ProductId = item.ProductID;
               var relations = SidebarCommon.GetContainerItemRel(itemGuid);

               if (relations.Any(r => r.Container.Type == Constants.WhiteSortBin && r.ContainerRel.IsActive))
               {
                  var auditBin = relations.First(r => r.Container.Type == Constants.WhiteSortBin && r.ContainerRel.IsActive).Container.WrhsContrID;
                  var msg = request.AppCode == "WAREHS_AUDIT"
                     ? "Item GUIDs cannot be scanned in this application. Please scan the NDC/UPC, or a 2D Barcode if applicable."
                     : "This item is in an Audit Bin and cannot be re-inducted. Return the item to Audit Bin " + auditBin + ".";

                  return new SearchResult()
                  {
                     ResultView = ResultView.List,
                     Message = msg
                  };
               }
            }
         }

         //temporarily removing until confirmed that we don't need it; Rob's Stored Procedure below also does NDC/UPC parse 11.29.18
         //if (request.Product.ProductId == 0 && request.Product.Ndc != null && request.Product.Ndc.Value != 0)
         //{
         //   GetProductId(request);

         //   //The NDC/UPC searched for does not exist, so no results are possible
         //   if (request.Product.ProductId == 0)
         //   {
         //      return new SearchResult { ResultView = ResultView.List };
         //   }
         //}

         var storedProcedureName = "FindProduct";
         var result = new SearchResult { ResultView = ResultView.List };

         List<SqlParameter> parameters = new List<SqlParameter>()
         {
            new SqlParameter() { ParameterName = "@NDC", SqlDbType = SqlDbType.BigInt, Value = request.Product.Ndc ?? SqlInt64.Null  },
            new SqlParameter() { ParameterName = "@ProductId", SqlDbType = SqlDbType.Int, Value = (request.Product.ProductId != 0) ? request.Product.ProductId : SqlInt32.Null },
            new SqlParameter() { ParameterName = "@ProfileCode", SqlDbType = SqlDbType.Int, Value = (request.Product.ProfileCode != 0) ? request.Product.ProfileCode : SqlInt32.Null },
            new SqlParameter() { ParameterName = "@Brand", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Brand ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Manufacturer", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Manufacturer ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Color", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Color ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Obverse", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Obverse ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Reverse", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Reverse ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Score", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Score ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Shape", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Shape ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@SerialNumber", SqlDbType = SqlDbType.NVarChar, Value = request.AppCode != "WAREHS_AUDIT" ? request.Product.SerialNumber ?? (object) DBNull.Value : (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@LotNumber", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Lot.LotNumber ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@UPC", SqlDbType = SqlDbType.BigInt, Value = request.Product.Upc ?? SqlInt64.Null }
         };

         DataSet output = SqlStoredProcedure.Execute(storedProcedureName, parameters);
         result.Products = new List<ProductResult>();

         //if something goes wrong in the stored procedure it will return a null dataset object and we should return
         if (output == null)
         {
            return result;
         }

         DataTable productReturn = output.Tables[0];
         DataTable imprintReturn = output.Tables[1];
         DataTable imageReturn = output.Tables[2];

         foreach (DataRow dr in productReturn.Rows)
         {
            if (dr["Message"] != DBNull.Value)
            {
               return new SearchResult()
               {
                  ResultView = ResultView.List,
                  Message = dr["Message"].ToString()
               };
            }

            //LotResults myLot; //get Lot info (2D Barcode was scanned) -- we might not need this after all, commenting out for now until SP is finalized
            var productId = Convert.ToInt32(dr["ProductID"]);
            long? rollupNdc; //get rollup NDC
            int? rollupProductId = Convert.ToBoolean(dr["NDCInnerPack"]) && dr["RollupProductID"] != DBNull.Value
               ? Convert.ToInt32(dr["RollupProductID"])
               : productId;

            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               //no point to make DB call if we're processing an outer, since the rollup NDC will equal the "outer" NDC
               rollupNdc = productId == rollupProductId
                  ? dr["NDC"] == DBNull.Value
                     ? (long?)null
                     : Convert.ToInt64(dr["NDC"])
                  : (from product in entities.Product where product.ProductID == rollupProductId select product.NDC).FirstOrDefault();

               //we might not need this after all, commenting out for now until SP is finalized
               //myLot = (from lot in entities.Lot
               //         where lot.LotNumber == request.Lot && lot.ProductID == productId && !lot.IsDeleted
               //         select new LotResults()
               //         {
               //            LotNumber = lot.LotNumber,
               //            ExpirationDate = lot.ExpirationDate,
               //            LotNa = false,
               //            ExpNa = false
               //         }).FirstOrDefault();
            }

            var workingProduct = new ProductDetail
            {
               ProductId = productId,
               RollupProductId = rollupProductId,
               ProfileCode = Convert.ToInt32(dr["ProfileCode"]),
               Ndc = dr["NDC"] == DBNull.Value ? (long?)null : Convert.ToInt64(dr["NDC"]),
               NDCUPCWithDashes = dr["NDCUPCWithDashes"].ToString(),
               Upc = dr["UPC"] == DBNull.Value ? (long?)null : Convert.ToInt64(dr["UPC"]),
               PackageSize = dr["PackageSize"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PackageSize"]),
               UnitsPerPackage = dr["UnitsPerPackage"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["UnitsPerPackage"]),
               UnitOfMeasure = dr["UnitOfMeasure"].ToString(),
               DosageCode = dr["DosageCode"].ToString(),
               Brand = dr["Description"].ToString(),
               SpecialHandling = dr["SpecialHandling"] == DBNull.Value ? false : Convert.ToBoolean(dr["SpecialHandling"]),
               RollupNdc = rollupNdc,
               Manufacturer = dr["Name"].ToString(),
               Generic = dr["GenericName"].ToString(),
               Strength = dr["Strength"].ToString(),
               CtrlNumber = dr["ControlNumber"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ControlNumber"]),
               Rx = Convert.ToBoolean(dr["RXorOTC"]) == true ? "RX" : "OTC",
               Lot = null,//myLot, we might not need this after all, commenting out for now until SP is finalized
               SerialNumber = request.Product.SerialNumber,
               ScannedBarcode = request.Product.ScannedBarcode,
               CaseQty = request.CaseQty,
               ContainerWeight = dr["ContainerWeight"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["ContainerWeight"]),
               FullContainerWeight = dr["FullContainerWeightWithContents"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["FullContainerWeightWithContents"]),
               DosageImageCode = dr["DosageCode"].ToString(),
               NDCInnerPack = dr["NDCInnerPack"] == DBNull.Value ? false : Convert.ToBoolean(dr["NDCInnerPack"])
            };

            var workingImage = new List<ImageResult>();
            foreach (DataRow drImage in imageReturn.Rows)
            {
               if (drImage["ProductID"] != DBNull.Value && Convert.ToInt32(drImage["ProductID"]) == productId)
               {
                  workingImage.Add(new ImageResult
                  {
                     ProductId = Convert.ToInt32(drImage["ProductID"]),
                     EffectiveEndDate = drImage["EffectiveEndDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImage["EffectiveEndDate"]),
                     EffectiveStartDate = drImage["EffectiveStartDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImage["EffectiveStartDate"]),
                     ImageType = drImage["ImageTypeCode"].ToString(),
                     FileName = drImage["FileName"].ToString(),
                     AltText = dr["Description"].ToString()
                  });
               }
            }

            var workingImprint = new ListStack<ImprintResult>();
            foreach (DataRow drImprint in imprintReturn.Rows)
            {
               if (drImprint["ProductID"] != DBNull.Value && Convert.ToInt32(drImprint["ProductID"]) == productId)
               {
                  workingImprint.Add(new ImprintResult
                  {
                     ProductId = Convert.ToInt32(drImprint["ProductID"]),
                     EffectiveStartDate = drImprint["EffectiveStartDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImprint["EffectiveStartDate"]),
                     EffectiveEndDate = drImprint["EffectiveEndDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImprint["EffectiveEndDate"]),
                     Description = drImprint["Description"].ToString(),
                     BasicDescription = drImprint["BasicDescription"].ToString(),
                     ImprintCode = drImprint["ImprintCode"].ToString()
                  });
               }
            }

            result.Products.Add(new ProductResult
            {
               ProductDetail = workingProduct,
               ImprintGroups = new List<ImprintGroup>()
               {
                  new ImprintGroup()
                  {
                     ImprintResults = workingImprint,
                     ImageResults = workingImage
                  }
               }
            });
         }

         foreach (var product in result.Products)
         {
            product.SortImprintGroups();

            if (product.ProductDetail.Lot == null)
            {
               product.ProductDetail.Lot = new LotResults()
               {
                  ExpirationDate = request.ExpirationDate,
                  LotNumber = request.Lot?.ToUpper(),
                  ExpNa = !request.ExpirationDate.HasValue,
                  LotNa = request.Lot.IsNullOrWhiteSpace()
               };
            }
         }

         return result;
      }

      /// <summary>
      /// Database lookup to retrieve a product specific auditrate
      /// Results are cached
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      internal static int GetProductAuditRate(int productId)
      {
         //create a unique key for using memcache
         var key = $"GetProductAuditRate|{productId}";
         //check if there is a value in memory cache already, cast to return type
         var model = MemoryCache.Default[key] as int?;
         if (model != null)
         {
            return model.Value;
         }
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               model = (from product in cloudEntities.Product
                        where product.ProductID == productId
                        select product.AuditRate).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }
         model = model ?? 0;
         //cache result for future lookups
         Common.Methods.SetCache(key, model);
         return model.Value;
      }

      /// <summary>
      /// Looks up an item from the guid
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      public static Item GetItem(Guid itemGuid)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from item in cloudEntities.Item
                       where item.ItemGUID == itemGuid
                       select item).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Paged product search with returns in image format.
      /// </summary>
      /// <param name="request">model containing the search criteria</param>
      /// <param name="page">Which page of results to retrieve</param>
      /// <param name="pageSize">Specify the number of results per page</param>
      /// <returns></returns>
      ///****************************************************************************
      internal static IPagedList<ProductResult> GetImageSearch(SearchRequest request, int page, int pageSize = 10)
      {
         Guid itemGuid;

         if (Guid.TryParseExact(request.Product.ScannedBarcode, "D", out itemGuid))
         {
            var item = GetItem(itemGuid);

            if (item != null)
            {
               request.Product.ProductId = item.ProductID;
               var relations = SidebarCommon.GetContainerItemRel(itemGuid);

               if (relations.Any(r => r.Container.Type == Constants.WhiteSortBin && r.ContainerRel.IsActive))
               {
                  var auditBin = relations.First(r => r.Container.Type == Constants.WhiteSortBin && r.ContainerRel.IsActive).Container.WrhsContrID;
                  var msg = request.AppCode == "WAREHS_AUDIT"
                     ? "Item GUIDs cannot be scanned in this application. Please scan the NDC/UPC, or a 2D Barcode if applicable."
                     : "This item is in an Audit Bin and cannot be re-inducted. Return the item to Audit Bin " + auditBin + ".";

                  var list = new ProductResult
                  {
                     ProductDetail = null,
                     ImprintGroups = new List<ImprintGroup>(),
                     Message = msg
                  };

                  return new List<ProductResult> { list }.ToPagedList(page, pageSize);
               }
            }
         }

         //temporarily removing until confirmed that we don't need it; Rob's Stored Procedure below also does NDC/UPC parse 11.29.18
         //if (request.Product.ProductId == 0 && request.Product.Ndc != null && request.Product.Ndc.Value != 0)
         //{
         //   GetProductId(request);

         //   //The NDC/UPC searched for does not exist, so no results are possible
         //   if (request.Product.ProductId == 0)
         //   {
         //      return null;
         //   }
         //}

         var storedProcedureName = "FindProduct";
         var result = new List<ProductResult>();

         List<SqlParameter> parameters = new List<SqlParameter>()
         {
            new SqlParameter() { ParameterName = "@NDC", SqlDbType = SqlDbType.BigInt, Value = request.Product.Ndc ?? SqlInt64.Null  },
            new SqlParameter() { ParameterName = "@ProductId", SqlDbType = SqlDbType.Int, Value = (request.Product.ProductId != 0) ? request.Product.ProductId : SqlInt32.Null },
            new SqlParameter() { ParameterName = "@ProfileCode", SqlDbType = SqlDbType.Int, Value = (request.Product.ProfileCode != 0) ? request.Product.ProfileCode : SqlInt32.Null },
            new SqlParameter() { ParameterName = "@Brand", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Brand ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Manufacturer", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Manufacturer ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Color", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Color ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Obverse", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Obverse ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Reverse", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Reverse ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Score", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Score ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@Shape", SqlDbType = SqlDbType.NVarChar, Value = request.Imprint.Shape ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@SerialNumber", SqlDbType = SqlDbType.NVarChar, Value = request.AppCode != "WAREHS_AUDIT" ? request.Product.SerialNumber ?? (object) DBNull.Value : (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@LotNumber", SqlDbType = SqlDbType.NVarChar, Value = request.Product.Lot.LotNumber ?? (object) DBNull.Value },
            new SqlParameter() { ParameterName = "@UPC", SqlDbType = SqlDbType.BigInt, Value = request.Product.Upc ?? SqlInt64.Null }
         };

         DataSet output = SqlStoredProcedure.Execute(storedProcedureName, parameters);

         //if something goes wrong in the stored procedure it will return a null dataset object and we should return
         if (output == null)
         {
            return result.ToPagedList(page, pageSize);
         }

         DataTable productReturn = output.Tables[0];
         DataTable imprintReturn = output.Tables[1];
         DataTable imageReturn = output.Tables[2];

         foreach (DataRow dr in productReturn.Rows)
         {
            if (dr["Message"] != DBNull.Value)
            {
               var list = new ProductResult
               {
                  ProductDetail = null,
                  ImprintGroups = new List<ImprintGroup>(),
                  Message = dr["Message"].ToString()
               };

               return new List<ProductResult> { list }.ToPagedList(page, pageSize);
            }

            //LotResults myLot; //get Lot info (2D Barcode was scanned) -- we might not need this after all, commenting out for now until SP is finalized
            var productId = Convert.ToInt32(dr["ProductID"]);
            long? rollupNdc; //get rollup NDC
            int? rollupProductId = Convert.ToBoolean(dr["NDCInnerPack"]) && dr["RollupProductID"] != DBNull.Value
               ? Convert.ToInt32(dr["RollupProductID"])
               : productId;

            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               //no point to make DB call if we're processing an outer, since the rollup NDC will equal the "outer" NDC
               rollupNdc = productId == rollupProductId
                  ? dr["NDC"] == DBNull.Value
                     ? (long?)null
                     : Convert.ToInt64(dr["NDC"])
                  : (from product in entities.Product where product.ProductID == rollupProductId select product.NDC).FirstOrDefault();

               //we might not need this after all, commenting out for now until SP is finalized
               //myLot = (from lot in entities.Lot
               //         where lot.LotNumber == request.Lot && lot.ProductID == productId && !lot.IsDeleted
               //         select new LotResults()
               //         {
               //            LotNumber = lot.LotNumber,
               //            ExpirationDate = lot.ExpirationDate,
               //            LotNa = false,
               //            ExpNa = false
               //         }).FirstOrDefault();
            }

            var workingProduct = new ProductDetail
            {
               ProductId = productId,
               RollupProductId = rollupProductId,
               ProfileCode = Convert.ToInt32(dr["ProfileCode"]),
               Ndc = dr["NDC"] == DBNull.Value ? (long?)null : Convert.ToInt64(dr["NDC"]),
               NDCUPCWithDashes = dr["NDCUPCWithDashes"].ToString(),
               Upc = dr["UPC"] == DBNull.Value ? (long?)null : Convert.ToInt64(dr["UPC"]),
               PackageSize = dr["PackageSize"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PackageSize"]),
               UnitsPerPackage = dr["UnitsPerPackage"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["UnitsPerPackage"]),
               UnitOfMeasure = dr["UnitOfMeasure"].ToString(),
               DosageCode = dr["DosageCode"].ToString(),
               Brand = dr["Description"].ToString(),
               SpecialHandling = dr["SpecialHandling"] == DBNull.Value ? false : Convert.ToBoolean(dr["SpecialHandling"]),
               RollupNdc = rollupNdc,
               Manufacturer = dr["Name"].ToString(),
               Generic = dr["GenericName"].ToString(),
               Strength = dr["Strength"].ToString(),
               CtrlNumber = dr["ControlNumber"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ControlNumber"]),
               Rx = Convert.ToBoolean(dr["RXorOTC"]) == true ? "RX" : "OTC",
               Lot = null,//myLot, we might not need this after all, commenting out for now until SP is finalized
               SerialNumber = request.Product.SerialNumber,
               ScannedBarcode = request.Product.ScannedBarcode,
               CaseQty = request.CaseQty,
               ContainerWeight = dr["ContainerWeight"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["ContainerWeight"]),
               FullContainerWeight = dr["FullContainerWeightWithContents"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["FullContainerWeightWithContents"]),
               DosageImageCode = dr["DosageCode"].ToString(),
               NDCInnerPack = dr["NDCInnerPack"] == DBNull.Value ? false : Convert.ToBoolean(dr["NDCInnerPack"])
            };

            var workingImage = new List<ImageResult>();
            foreach (DataRow drImage in imageReturn.Rows)
            {
               if (drImage["ProductID"] != DBNull.Value && Convert.ToInt32(drImage["ProductID"]) == productId)
               {
                  workingImage.Add(new ImageResult
                  {
                     ProductId = Convert.ToInt32(drImage["ProductID"]),
                     EffectiveEndDate = drImage["EffectiveEndDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImage["EffectiveEndDate"]),
                     EffectiveStartDate = drImage["EffectiveStartDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImage["EffectiveStartDate"]),
                     ImageType = drImage["ImageTypeCode"].ToString(),
                     FileName = drImage["FileName"].ToString(),
                     AltText = dr["Description"].ToString()
                  });
               }
            }

            //the old EF did not include imprint in its return statement; temporarily commenting out
            //var workingImprint = new ListStack<ImprintResult>();
            //foreach (DataRow drImprint in imprintReturn.Rows)
            //{
            //   if (drImprint["ProductID"] != DBNull.Value && Convert.ToInt32(drImprint["ProductID"]) == productId)
            //   {
            //      workingImprint.Add(new ImprintResult
            //      {
            //         ProductId = Convert.ToInt32(drImprint["ProductID"]),
            //         EffectiveStartDate = drImprint["EffectiveStartDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImprint["EffectiveStartDate"]),
            //         EffectiveEndDate = drImprint["EffectiveEndDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(drImprint["EffectiveEndDate"]),
            //         Description = drImprint["Description"].ToString(),
            //         BasicDescription = drImprint["BasicDescription"].ToString(),
            //         ImprintCode = drImprint["ImprintCode"].ToString()
            //      });
            //   }
            //}

            var prdctRes = new ProductResult
            {
               ProductDetail = workingProduct,
               ImprintGroups = new List<ImprintGroup>()
               {
                  new ImprintGroup()
                  {
                     //ImprintResults = workingImprint,
                     ImageResults = workingImage
                  }
               },
               Message = null
            };

            result.Add(prdctRes);
         }

         foreach (var product in result)
         {
            if (product.ProductDetail.Lot == null)
            {
               product.ProductDetail.Lot = new LotResults()
               {
                  ExpirationDate = request.ExpirationDate,
                  LotNumber = request.Lot,
                  ExpNa = !request.ExpirationDate.HasValue,
                  LotNa = request.Lot.IsNullOrWhiteSpace()
               };
            }
         }

         return result.OrderBy(r => r.ProductDetail.Brand).ToPagedList(page, pageSize);

         //OLD CODE AS OF 11.27.18
         //IPagedList<ProductResult> resultsTwo = null;

         //var manufacturerTypeCode = "MFGR";

         //try
         //{
         //   using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //   {
         //      resultsTwo = (from product in entities.Product
         //                join mfrProfile in entities.Profile on new { profileCode = product.ProfileCode, profileTypeCode = manufacturerTypeCode } equals
         //                   new { profileCode = mfrProfile.ProfileCode, profileTypeCode = mfrProfile.ProfileTypeCode }
         //                join dosage in entities.DosageDict on product.DosageCode equals dosage.Code
         //                join image in entities.ProductImage on product.ProductID equals image.ProductID into i
         //                from image in i.DefaultIfEmpty()
         //                where
         //                   (request.Product.ProductId == 0 || request.Product.ProductId == product.ProductID)
         //                   && (request.Product.Manufacturer == null || request.Product.Manufacturer == string.Empty
         //                       || mfrProfile.Name.Contains(request.Product.Manufacturer.Trim()))
         //                   && (request.Product.Brand == null || request.Product.Brand == string.Empty
         //                       || product.Description.Contains(request.Product.Brand.Trim())
         //                       || product.GenericName.Contains(request.Product.Brand.Trim()))
         //                   && ((request.Imprint.Obverse == null || request.Imprint.Obverse == string.Empty)
         //                       || (from imprint in entities.Imprint
         //                           where
         //                              imprint.ProductID == product.ProductID
         //                              && imprint.ImprintCode == "Imprint"
         //                              && imprint.Description.Replace(" ", "").Contains(request.Imprint.Obverse.Trim().Replace(" ", ""))
         //                           select 1).Any())
         //                   && ((request.Imprint.Reverse == null || request.Imprint.Reverse == string.Empty)
         //                       || (from imprint in entities.Imprint
         //                           where
         //                              imprint.ProductID == product.ProductID
         //                              && imprint.ImprintCode == "Imprint"
         //                              && imprint.Description.Replace(" ", "").Contains(request.Imprint.Reverse.Trim().Replace(" ", ""))
         //                           select 1).Any())
         //                   && ((request.Imprint.Color == null || request.Imprint.Color == string.Empty)
         //                       || (from imprint in entities.Imprint
         //                           where
         //                              imprint.ProductID == product.ProductID
         //                              && (imprint.ImprintCode == "color")
         //                              && imprint.BasicDescription.Contains(request.Imprint.Color.Trim())
         //                           select 1).Any())
         //                   && ((request.Imprint.Shape == null || request.Imprint.Shape == string.Empty)
         //                       || (from imprint in entities.Imprint
         //                           where
         //                              imprint.ProductID == product.ProductID
         //                              && (imprint.ImprintCode == "shape")
         //                              && imprint.BasicDescription.Contains(request.Imprint.Shape.Trim())
         //                           select 1).Any())
         //                   && ((request.Imprint.Score == null || request.Imprint.Score == string.Empty)
         //                       || (from imprint in entities.Imprint
         //                           where
         //                              imprint.ProductID == product.ProductID
         //                              && (imprint.ImprintCode == "score")
         //                              && imprint.Description.Contains(request.Imprint.Score.Trim())
         //                           select 1).Any()) && !product.IsDeleted
         //                   && (image.FileName == null || !image.IsDeleted)
         //                select new ProductResult
         //                {
         //                   ProductDetail =
         //                      new ProductDetail()
         //                      {
         //                         NDCUPCWithDashes = product.NDCUPCWithDashes,
         //                         Brand = product.Description,
         //                         ProductId = product.ProductID,
         //                         Ndc = product.NDC,
         //                         DosageCode = dosage.Description,
         //                         SpecialHandling = mfrProfile.SpecialHandling,
         //                         UnitOfMeasure = product.UnitOfMeasure,
         //                         Manufacturer = mfrProfile.Name,
         //                         ProfileCode = product.ProfileCode,
         //                         Generic = product.GenericName,
         //                         UnitsPerPackage = product.UnitsPerPackage,
         //                         PackageSize = product.PackageSize,
         //                         Strength = product.Strength,
         //                         RollupProductId = product.RollupProductID ?? product.ProductID,
         //                         RollupNdc = product.RollupProductID != null ? (from prd in entities.Product where prd.ProductID == product.RollupProductID select prd.NDC).FirstOrDefault() : product.NDC,
         //                         CtrlNumber = product.ControlNumber,
         //                         Rx = product.RXorOTC ? "RX" : "OTC",
         //                         Upc = product.UPC,
         //                         Lot = (from lot in entities.Lot
         //                                where lot.LotNumber == request.Lot
         //                                      && lot.ProductID == product.ProductID
         //                                      && !lot.IsDeleted
         //                                select new LotResults()
         //                                {
         //                                   LotNumber = lot.LotNumber,
         //                                   ExpirationDate = lot.ExpirationDate,
         //                                   LotNa = false,
         //                                   ExpNa = false
         //                                }).FirstOrDefault(),
         //                         SerialNumber = request.Product.SerialNumber,
         //                         ScannedBarcode = request.Product.ScannedBarcode,
         //                         CaseQty = request.CaseQty,
         //                         ContainerWeight = product.ContainerWeight,
         //                         FullContainerWeight = product.FullContainerWeightWithContents,
         //                         DosageImageCode = (from dpr in entities.DosagePackageRel
         //                                            where product.DosageCode == dpr.DosageCode
         //                                            select dpr.DosageImageCode).FirstOrDefault()
         //                      },
         //                   ImprintGroups =
         //                      new List<ImprintGroup>()
         //                      {
         //                         new ImprintGroup()
         //                         {
         //                            ImageResults =
         //                               new List<ImageResult>()
         //                               {
         //                                  new ImageResult()
         //                                  {
         //                                     ImageType = image.ImageTypeCode ?? null,
         //                                     FileName = image.FileName ?? null,
         //                                     AltText = product.Description ?? null
         //                                  }
         //                               }
         //                         }
         //                      }
         //                }).OrderBy(r => r.ProductDetail.Brand)
         //         .ToPagedList(page, pageSize);

         //      foreach (var product in resultsTwo)
         //      {
         //         if (product.ProductDetail.Lot == null)
         //         {
         //            product.ProductDetail.Lot = new LotResults()
         //            {
         //               ExpirationDate = request.ExpirationDate,
         //               LotNumber = request.Lot,
         //               ExpNa = !request.ExpirationDate.HasValue,
         //               LotNa = request.Lot.IsNullOrWhiteSpace()
         //            };
         //         }
         //      }

         //   }
         //}
         //catch (Exception ex)
         //{
         //   Logger.Error(ex);
         //   throw;
         //}

         //return resultsTwo;
      }

      /// <summary>
      /// Determines if a control 2 item matches an entry in the control 2 form
      /// and update the control 2 to indicate the item was received
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="userName">the username of the logged in user, used to record the database update</param>
      /// <returns></returns>
      internal static bool IsForm222Match(PolicyForm policyForm, string userName)
      {
         try
         {
            var sealedOpenCase = policyForm.ContainerType.Split('|')[1];
            if (sealedOpenCase == ContainerType.Loose.ToString() || sealedOpenCase == ContainerType.Open.ToString())
            {
               sealedOpenCase = "Opened";
            }

            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {

               var itemRel = cloudEntities.Item222Rel.FirstOrDefault(x => x.ItemGUID == policyForm.ItemGuid && !x.IsDeleted);

               //these have to be here because the detail query below doesn't allow itemRel to be used in a join unless it's an enumerable
               var reprocess = itemRel != null;
               var itemRelDea = itemRel?.DEAFormNumber;
               var itemRelLineNo = itemRel?.LineNumber;
               var itemRelQty = itemRel?.AppliedQty;

               //get the item's form-line pairing from form222Detail
               var undoDetail = itemRel != null
                  ? cloudEntities.Form222Detail.FirstOrDefault(x => x.DEAFormNumber == itemRel.DEAFormNumber && x.LineNumber == itemRel.LineNumber)
                  : null;

               var detail = (from form222Detail in cloudEntities.Form222Detail
                             join form222 in cloudEntities.Form222 on form222Detail.DEAFormNumber equals form222.DEAFormNumber
                             where form222Detail.NDC == policyForm.Product.Ndc
                             && form222.Form222SourceID == policyForm.ReturnInformation.ReturnAuthorization.RtrnAuthNumber
                             && form222.Status != "Closed" && form222.Status != "Voided"
                             && form222Detail.SealedOpenCase == sealedOpenCase
                             && form222Detail.PackageSize == (sealedOpenCase == "Opened" ? policyForm.Quantity : form222Detail.PackageSize)
                             && (form222Detail.QuantityReceived == null || (form222Detail.QuantityReceived < form222Detail.PackageSize * form222Detail.NbrofPackages)
                                 || (reprocess && form222Detail.DEAFormNumber == itemRelDea && form222Detail.LineNumber == itemRelLineNo && form222Detail.QuantityReceived == itemRelQty))
                             select form222Detail).FirstOrDefault();

               //no exact matches found
               if (detail == null)
               {
                  //we have an item222Rel entry with the given Item GUID that we need to delete
                  //we have to do this here because the return false implies the item will go into a white bin
                  //which means we won't get a chance to delete the corresponding item222Rel entries later on in the process
                  if (itemRel != null)
                  {
                     //Before deleting the item222Rel entry, we have to subtract the Quantity from the line
                     //that had a previous match. This is to ensure we have a correct quantity received from the customer.
                     if (undoDetail != null)
                     {
                        undoDetail.QuantityReceived -= itemRel.AppliedQty;
                        undoDetail.QuantityReceived = undoDetail.QuantityReceived == 0 ? null : undoDetail.QuantityReceived;
                        undoDetail.ReceivedDate = undoDetail.QuantityReceived == null ? (DateTime?)null : DateTime.UtcNow;
                        undoDetail.ModifiedDate = DateTime.UtcNow;
                     }

                     cloudEntities.Item222Rel.Remove(itemRel);
                     cloudEntities.SaveChanges();
                  }
                  
                  return false;
               }

               return true;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /// <summary>
      /// If an item is going to a black bin, save the item into Item222Rel and update the Form222Detail table accordingly
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="itemGuid">itemGuid being processed
      /// <param name="userName">the username of the logged in user, used to record the database update</param>
      /// <returns></returns>
      internal static bool Form222MatchItem(PolicyForm policyForm, Guid itemGuid, string userName)
      {
         try
         {
            var sealedOpenCase = policyForm.ContainerType.Split('|')[1];
            if (sealedOpenCase == ContainerType.Loose.ToString() || sealedOpenCase == ContainerType.Open.ToString())
            {
               sealedOpenCase = "Opened";
            }

            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {

               var itemRel = cloudEntities.Item222Rel.FirstOrDefault(x => x.ItemGUID == itemGuid && !x.IsDeleted);

               //these have to be here because the detail query below doesn't allow itemRel to be used in a join unless it's an enumerable
               var reprocess = itemRel != null;
               var itemRelDea = itemRel?.DEAFormNumber;
               var itemRelLineNo = itemRel?.LineNumber;
               var itemRelQty = itemRel?.AppliedQty;

               //get the item's form-line pairing from form222Detail
               var undoDetail = itemRel != null
                  ? cloudEntities.Form222Detail.FirstOrDefault(x => x.DEAFormNumber == itemRel.DEAFormNumber && x.LineNumber == itemRel.LineNumber)
                  : null;

               var detail = (from form222Detail in cloudEntities.Form222Detail
                             join form222 in cloudEntities.Form222 on form222Detail.DEAFormNumber equals form222.DEAFormNumber
                             where form222Detail.NDC == policyForm.Product.Ndc
                             && form222.Form222SourceID == policyForm.ReturnInformation.ReturnAuthorization.RtrnAuthNumber
                             && form222.Status != "Closed" && form222.Status != "Voided"
                             && form222Detail.SealedOpenCase == sealedOpenCase
                             && form222Detail.PackageSize == (sealedOpenCase == "Opened" ? policyForm.Quantity : form222Detail.PackageSize)
                             && (form222Detail.QuantityReceived == null || (form222Detail.QuantityReceived < form222Detail.PackageSize * form222Detail.NbrofPackages)
                                 || (reprocess && form222Detail.DEAFormNumber == itemRelDea && form222Detail.LineNumber == itemRelLineNo && form222Detail.QuantityReceived == itemRelQty))
                             select form222Detail).FirstOrDefault();

               //no exact matches found
               //should be caught during the bin designation process, but this is just a failsafe
               if (detail == null)
               {
                  //we have an item222Rel entry with the given Item GUID that we need to delete
                  if (itemRel != null)
                  {
                     //Before deleting the item222Rel entry, we have to subtract the Quantity from the line
                     //that had a previous match. This is to ensure we have a correct quantity received from the customer.
                     if (undoDetail != null)
                     {
                        undoDetail.QuantityReceived -= itemRel.AppliedQty;
                        undoDetail.QuantityReceived = undoDetail.QuantityReceived <= 0 ? null : undoDetail.QuantityReceived;
                        undoDetail.ReceivedDate = undoDetail.QuantityReceived == null ? (DateTime?)null : DateTime.UtcNow;
                        undoDetail.ModifiedDate = DateTime.UtcNow;
                     }

                     //delete the item222Rel entry
                     cloudEntities.Item222Rel.Remove(itemRel);
                     cloudEntities.SaveChanges();
                  }

                  return false;
               }

               //undoDetail is a form222detail entry based on the item's form and line
               if (itemRel != null && undoDetail != null)
               {
                  //if the matched form-line pairing equals that of the item's already existing form-line pairing then we do not need to do a new calculation
                  if (detail.DEAFormNumber == undoDetail.DEAFormNumber && detail.LineNumber == undoDetail.LineNumber)
                  {
                     return true;
                  }

                  //if we get here, them it means the reprocessed item previously matched to one form-line pairing has been matched up against another form-line pair
                  //that means we have to undo the QuantityReceived and the item222Rel table entry for the item on its previous pairing
                  undoDetail.QuantityReceived -= itemRel.AppliedQty;
                  undoDetail.QuantityReceived = undoDetail.QuantityReceived <= 0 ? null : undoDetail.QuantityReceived;
                  undoDetail.ReceivedDate = undoDetail.QuantityReceived == null ? (DateTime?) null : DateTime.UtcNow;
                  undoDetail.ModifiedDate = DateTime.UtcNow;
                  undoDetail.ModifiedBy = userName;
                  cloudEntities.Item222Rel.Remove(itemRel);
                  cloudEntities.SaveChanges();
               }

               //new entry into item222Rel, GUID-Form-Line combination
               var newItemRel = new WrhsCtrl2Model
               {
                  GUID = itemGuid,
                  DEA = detail.DEAFormNumber.ToString(),
                  LineNo = detail.LineNumber,
                  Approved = false,
                  AppliedQty = sealedOpenCase == "Opened" ? policyForm.Quantity : detail.PackageSize,
                  Match = 0
               };

               WrhsCtrl2Model.Form222ReconcileItem(newItemRel, userName);

               //update Form222Detail accordingly
               detail.QuantityReceived = (detail.QuantityReceived ?? 0) + (sealedOpenCase == "Opened" ? policyForm.Quantity : detail.PackageSize);
               detail.ReceivedDate = DateTime.UtcNow;
               detail.ModifiedDate = DateTime.UtcNow;
               detail.ModifiedBy = userName;
               cloudEntities.SaveChanges();
               return true;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /// <summary>
      /// Returns a recall record for a given Id
      /// </summary>
      /// <param name="recallId"></param>
      /// <returns></returns>
      internal static Recall GetRecallByID(long? recallId)
      {
         Recall result = null;
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               result = (
                  from recall in cloudEntities.Recall
                  where recall.RecallID == recallId
                        && !recall.IsDeleted
                  select recall).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
         return result;
      }

      /// <summary>
      /// Sets the productId in the given SearchRequest object if a matching upc or ndc is found
      /// </summary>
      /// <param name="request"></param>
      private static void GetProductId(SearchRequest request)
      {
         long ndc = 0;
         long upc = 0;

         var dbModel = new Qualanex.Qosk.Library.Model.DBModel.DBModel("name=QoskCloud");

         request.Product.ProductId = Qualanex.Qosk.Library.Model.ProductModel.ProductModel.IsExistUPC(
            ref dbModel,
            request.Product.Ndc.ToString(),
            out ndc);

         if (request.Product.ProductId == 0)
         {
            request.Product.ProductId =
               Qualanex.Qosk.Library.Model.ProductModel.ProductModel.IsExistNDC(ref dbModel, request.Product.Ndc.ToString(),
                  out upc);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves a list of lots.
      /// </summary>
      /// <param name="productId">The product whose lots are retrieved</param>
      /// <param name="lotNumber">Optional, a specific lot number to retrieve</param>
      /// <returns></returns>
      ///****************************************************************************
      internal static List<LotResults> GetLots(long productId, string lotNumber = null)
      {
         var result = new List<LotResults>();

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               result = new List<LotResults>(
                  from lot in cloudEntities.Lot
                  where lot.ProductID == productId
                        && (lotNumber == null || lotNumber == String.Empty || lot.LotNumber == lotNumber)
                        && !lot.IsDeleted
                  select
                     new LotResults()
                     {
                        ExpirationDate = lot.ExpirationDate,
                        LotNumber = lot.LotNumber,
                        ProductId = lot.ProductID,
                        RecallId = lot.RecallID
                     });
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Given an NDC and a two letter state code, find all relevant disposal
      ///   information.
      /// </summary>
      /// <param name="ndc"></param>
      /// <param name="state">
      ///   Returns all available states if null or missing.
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      internal static WasteResult GetDisposal(long ndc, string state = null)
      {
         var disposalResult = GetProductWasteInfo(ndc, state);

         if (disposalResult?.StateWasteCodes != null)
         {
            disposalResult.StateWasteCodes = disposalResult.StateWasteCodes.Where(r => r.StateCode == state).ToList();
         }

         return disposalResult;
      }


      ///****************************************************************************
      /// <summary>
      ///   Uses the current user profile and current date to determine which
      ///   notifications and recalls need to be to shown.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      internal static Notification GetNotificationsAndRecalls(int userProfileCode)
      {
         Notification notification = new Notification()
         {
            ProductReminders = new List<List<ProductNotification>>(),
            Recalls = new List<RecallResult>()
         };

         List<LotResults> lots;
         List<Acknlowedgement> acknowledgements;

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               notification.ProductReminders = (from notificationGroup in cloudEntities.ProductNotification
                                                where notificationGroup.UserProfileCode == userProfileCode
                                                group notificationGroup by notificationGroup.GroupGUID into groupG
                                                select (from productNotification in cloudEntities.ProductNotification
                                                        join product in cloudEntities.Product on productNotification.ProductID equals product.ProductID
                                                        join mfrProfile in cloudEntities.Profile on new { profileCode = product.ProfileCode, profileTypeCode = "MFGR" } equals
                                                           new { profileCode = mfrProfile.ProfileCode, profileTypeCode = mfrProfile.ProfileTypeCode }
                                                           //where filtered by appropriate criterion here
                                                        where productNotification.UserProfileCode == userProfileCode
                                                        && productNotification.GroupGUID == groupG.Key
                                                        select
                                                           new ProductNotification()
                                                           {
                                                              ProductDetail =
                                                                 new ProductDetail()
                                                                 {
                                                                    ProductId = productNotification.ProductID ?? 0,
                                                                    NDCUPCWithDashes = product.NDCUPCWithDashes,
                                                                    Upc = product.UPC,
                                                                    Generic = product.GenericName,
                                                                    Brand = product.Description,
                                                                    Manufacturer = mfrProfile.Name,
                                                                    ProfileCode = product.ProfileCode,
                                                                    Lot =
                                                                       new LotResults()
                                                                       {
                                                                          LotNumber = productNotification.LotNumber,
                                                                          ExpirationDate = productNotification.ExpirationDate
                                                                       }
                                                                 },
                                                              Lot = new LotResults()
                                                              {
                                                                 LotNumber = productNotification.LotNumber,
                                                                 ExpirationDate = productNotification.ExpirationDate
                                                              },
                                                              FutureIndateOpen = productNotification.FutureIndateOpen,
                                                              FutureIndateSealed = productNotification.FutureIndateSealed,
                                                              ContainerTypeAndCondition = productNotification.ContainerTypeName,
                                                              EndDate = productNotification.EndDate,
                                                              NotificationId = productNotification.NotificationID,
                                                              Status = productNotification.Status,
                                                              CaseSize = productNotification.CaseSize,
                                                              GroupGuid = productNotification.GroupGUID
                                                           }).OrderBy(n => n.FutureIndateSealed ?? n.FutureIndateOpen ?? DateTime.MaxValue)
                                                   .ToList())
                                                   .OrderBy(ng => ng.FirstOrDefault().FutureIndateSealed ?? ng.FirstOrDefault().FutureIndateOpen ?? DateTime.MaxValue)
                                                   .ToList();

               notification.Recalls = new List<RecallResult>(
                  from recall in cloudEntities.Recall
                  join lot in cloudEntities.Lot on recall.RecallID equals lot.RecallID.Value
                  join product in cloudEntities.Product on lot.ProductID equals product.ProductID
                  where !recall.IsDeleted
                        && !lot.IsDeleted
                        && !product.IsDeleted
                  select
                     new RecallResult
                     {
                        RecallId = recall.RecallID,
                        RecallEndDate = recall.RecallEndDate,
                        CreditEndDate = recall.CreditEndDate,
                        RecallClass = recall.RecallClass,
                        RecallStartDate = recall.RecallStartDate,
                        ReimbursementProvider = recall.ReimbusementProvider,
                        ProductDetail =
                           new ProductDetail()
                           {
                              ProductId = product.ProductID,
                              Brand = product.Description,
                              ProfileCode = product.ProfileCode,
                              NDCUPCWithDashes = product.NDCUPCWithDashes,
                              Ndc = product.NDC,
                              Upc = product.UPC,
                              UnitOfMeasure = product.UnitOfMeasure
                           }
                     }).DistinctBy(r => r.ProductDetail.Ndc.ToString() + "|" + r.RecallId.ToString()) //distinct combination of product and recall
                  .ToList();

               lots = new List<LotResults>(from lot in cloudEntities.Lot
                                           where lot.RecallID.HasValue
                                                 && !lot.IsDeleted
                                           select new LotResults()
                                           {
                                              ProductId = lot.ProductID,
                                              ExpirationDate = lot.ExpirationDate,
                                              LotNumber = lot.LotNumber,
                                              RecallId = lot.RecallID
                                           }).ToList();
               var query = "IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES "
               + " WHERE TABLE_NAME = N'RecallAcknowledgement') "
               + " BEGIN "
               + " select cast('true' as bit) as Acknowledged, RecallId, AcknowledgedDate, AcknowledgedBy from RecallAcknowledgement where ProfileCode = " + userProfileCode
               + " END";

               acknowledgements = cloudEntities.Database.SqlQuery<Acknlowedgement>(query).ToList();

               foreach (var recall in notification.Recalls)
               {
                  // ReSharper disable once PossibleInvalidOperationException The query above guarantees that lot.recallid will have a value
                  recall.Lots = lots.FindAll(r => r.RecallId.Value == recall.RecallId && r.ProductId == recall.ProductDetail.ProductId)
                     .OrderBy(r => r.LotNumber).ToList();
                  recall.Acknowledgement = acknowledgements.FindLast(r => r.RecallId == recall.RecallId) ?? new Acknlowedgement();
               }

               foreach (var group in notification.ProductReminders)
               {
                  foreach (var productNotification in group)
                  {
                     var container = ProductCommon.SeperateContainerTypeAndCondition(productNotification.ContainerTypeAndCondition);
                     productNotification.PackageType = container.Package;
                     productNotification.ContainerType = container.Type;
                     productNotification.ContainerCondition = container.Condition;
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            throw;
         }

         return notification;
      }

      ///****************************************************************************
      /// <summary>
      ///   Insert a recall acknowledgment record.
      /// </summary>
      /// <param name="recallId"></param>
      /// <param name="userName"></param>
      /// <param name="userProfileCode"></param>
      ///****************************************************************************
      internal static void SaveRecallAcknowledgement(long recallId, string userName, int userProfileCode)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var query = "IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES "
               + " WHERE TABLE_NAME = N'RecallAcknowledgement') "
               + " BEGIN "
               + $" insert into RecallAcknowledgement (RecallId, ProfileCode, AcknowledgedBy, AcknowledgedDate, CreatedBy, CreatedDate) values({recallId},{userProfileCode},'{userName}',getdate(),'{userName}', getdate());"
            + " END";

            cloudEntities.Database.ExecuteSqlCommand(query);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns a list of Product notifications for a given list of notification ids.
      /// </summary>
      /// <param name="notificationIds"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductNotification> GetNotifications(List<long> notificationIds)
      {
         try
         {
            using (var entites = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var notifications = entites.ProductNotification.Where(p => notificationIds.Contains(p.NotificationID))
                  .Select(n => new ProductNotification
                  {
                     ContainerTypeAndCondition = n.ContainerTypeName,
                     CaseSize = n.CaseSize,
                     EndDate = n.EndDate,
                     FutureIndateOpen = n.FutureIndateOpen,
                     FutureIndateSealed = n.FutureIndateSealed,
                     GroupGuid = n.GroupGUID,
                     Lot = new LotResults()
                     {
                        ExpirationDate = n.ExpirationDate,
                        LotNumber = n.LotNumber
                     },
                     NotificationId = n.NotificationID,
                     Status = n.Status,
                     ProductDetail = new ProductDetail()
                     {
                        ProductId = n.ProductID ?? 0
                     }

                  }).ToList();

               foreach (var productNotification in notifications)
               {
                  var container = ProductCommon.SeperateContainerTypeAndCondition(productNotification.ContainerTypeAndCondition);
                  productNotification.PackageType = container.Package;
                  productNotification.ContainerType = container.Type;
                  productNotification.ContainerCondition = container.Condition;
               }

               return notifications;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            return null;
         }
      }

      /// ****************************************************************************
      ///  <summary>
      ///    Retrieve product details by productID.
      ///    
      ///  </summary>
      ///  <param name="productId"></param>
      /// <param name="getImprints">If true, imprints will be retrieved, note that this may take significantly more time
      /// results are cached</param>
      /// <returns></returns>
      /// ****************************************************************************
      public static ProductResult GetProduct(long productId, bool getImprints = false)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            if (productId == 0)
            {
               return new ProductResult() { ProductDetail = new ProductDetail() };
            }

            var key = $"GetProduct|{productId}|{getImprints}";
            var result = MemoryCache.Default[key] as ProductResult;

            if (result != null)
            {
               return result;
            }

            result = new ProductResult();

            try
            {
               var query = "select distinct NDCUPCWithDashes as NDCUPCWithDashes, p.Description as Brand, " +
                              "p.ProductID as ProductId, p.RollupProductID as RollupProductId, p.NDC as Ndc, d.Description as DosageCode, " +
                              "p.UnitOfMeasure as UnitOfMeasure, m.Name as Manufacturer, " +
                              "p.GenericName as GenericName, p.UnitsPerPackage as UnitsPerPackage, " +
                              "p.PackageSize as PackageSize, p.Strength as Strength, " +
                              "case when ISNULL(p.NDCInnerPack,0) = 1 then p.InnerContainerTypeName else null end as InnerContainerTypeName, " +
                              "ISNULL(p.NDCInnerPack,0) as NDCInnerPack, " +
                              "cast(p.ControlNumber as int) as CtrlNumber, p.ProfileCode as ProfileCode, " +
                              "CASE RXorOTC WHEN 0 THEN 'OTC' WHEN 1 THEN 'RX' END AS Rx, " +
                              "p.UPC as Upc, p.ForceQuantityCount as ForceQuantityCount, " +
                              "p.ForceQuantityCountNotes as ForceQuantityCountNotes, " +
                              "p.IndividualCountWeight as IndividualCountWeight, " +
                              "p.ContainerWeight as ContainerWeight, p.FullContainerWeightWithContents as FullContainerWeight, " +
                              " p.UnitDose as UnitDose, dpr.DosageImageCode as DosageImageCode, " +
                              "m.SpecialHandling as SpecialHandling, " +
                              "p.AlwaysSortToQuarantine as AlwaysSortToQuarantine " +
                              "from Product p " +
                              "join DosageDict d on p.DosageCode = d.Code and d.IsDeleted = 0 " +
                              "join DosagePackageRel dpr on p.DosageCode = dpr.DosageCode " +
                              "join Profile m on p.ProfileCode = m.ProfileCode and m.IsDeleted = 0 " +
                              "where p.IsDeleted = 0 and p.ProductID = " + productId +
                              " and (dpr.PackageCode is null or dpr.PackageCode = p.PackageCode); ";

               result.ProductDetail = cloudEntities.Database.SqlQuery<ProductDetail>(query).First();
               result.ProductDetail.CaseSize =
                  cloudEntities.Database.SqlQuery<int>(
                     "select CaseSize from ProductCaseSize where IsDeleted = 0 and ProductID = " +
                     result.ProductDetail.ProductId + ";").ToList();

               //if we're processing an inner, process the quantity/weights as if they were an outer
               //this is so we get the 3 quantity boxes even if we scan the inner NDC
               if (result.ProductDetail.NDCInnerPack)
               {
                  var innerOuterQuery = (from p in cloudEntities.Product
                                         where !p.IsDeleted && p.ProductID == result.ProductDetail.RollupProductId
                                         select p).First();

                  result.ProductDetail.IndividualCountWeight = innerOuterQuery.IndividualCountWeight;
                  result.ProductDetail.FullContainerWeight = innerOuterQuery.FullContainerWeightWithContents;
                  result.ProductDetail.ContainerWeight = innerOuterQuery.ContainerWeight;
                  result.ProductDetail.PackageSize = innerOuterQuery.PackageSize;
                  result.ProductDetail.UnitsPerPackage = innerOuterQuery.UnitsPerPackage;
               }

               if (getImprints)
               {
                  result.ImprintGroups =
                     new List<ImprintGroup>()
                     {
                        new ImprintGroup()
                        {
                           ImprintResults = (from imp in cloudEntities.Imprint
                                             join impDict in cloudEntities.ImprintDict on
                                             imp.ImprintCode equals impDict.Code
                                             where imp.ProductID == result.ProductDetail.ProductId
                                                   && !imp.IsDeleted
                                             select
                                             new ImprintResult()
                                             {
                                                EffectiveStartDate = imp.EffectiveStartDate,
                                                Description = imp.Description,
                                                BasicDescription = imp.BasicDescription,
                                                EffectiveEndDate = imp.EffectiveEndDate,
                                                ImprintCode = imp.ImprintCode
                                             }).ToList(),
                           ImageResults = (from image in cloudEntities.ProductImage
                                           where image.ProductID == result.ProductDetail.ProductId
                                                 && !image.IsDeleted
                                           select
                                           new ImageResult()
                                           {
                                              EffectiveEndDate = image.EffectiveEndDate,
                                              EffectiveStartDate = image.EffectiveStartDate,
                                              ImageType = image.ImageTypeCode,
                                              FileName = image.FileName,
                                              AltText = result.ProductDetail.Brand
                                           }).Distinct().ToList()
                        }
                     };

                  result.SortImprintGroups();
               }

            }
            catch (Exception ex)
            {
               Logger.Error(ex.Message);
            }

            result.ProductDetail = result.ProductDetail ?? new ProductDetail();
            Common.Methods.SetCache(key, result);
            return result;
         }
      }

      /// ****************************************************************************
      ///  <summary>
      ///    Saves a product notification record for items that are returned to stock.
      ///  </summary>
      ///  <param name="policyForm">reflects the input values for the item being inducted</param>
      ///  <param name="logon">the logged in username</param>
      /// <param name="userProfileCode">the logged in user's profilecode</param>
      /// <returns></returns>
      /// ****************************************************************************
      public static bool SaveNotification(PolicyForm policyForm, string logon, int userProfileCode)
      {
         DateTime? inDateOpen = null;
         DateTime? inDateSealed = null;
         CloudBlockBlob sideImageBlob = null;
         CloudBlockBlob topImageBlob = null;
         CloudBlockBlob contentImageBlob = null;

         if (!policyForm.SideImageUri.IsNullOrWhiteSpace())
         {
            sideImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.SideImageUri);
         }

         if (!policyForm.TopImageUri.IsNullOrWhiteSpace())
         {
            topImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.TopImageUri);
         }

         if (!policyForm.ContentImageUri.IsNullOrWhiteSpace())
         {
            contentImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.ContentImageUri);
         }

         if (policyForm.SelectedLot.ExpirationDate.HasValue)
         {
            var policy = policyForm.PolicyResult.PolicyModel.ProductPolicy.Policy;

            if (policy.NumberOfMonthsBeforeExpirationReturnableOpened != null)
            {
               inDateOpen =
                  policyForm.SelectedLot.ExpirationDate.Value.AddMonths(
                     -policy.NumberOfMonthsBeforeExpirationReturnableOpened.Value
                     + (policy.ExpiresOnFirstOfCurrentMonth ?? false ? 0 : 1));
            }

            if (policy.NumberOfMonthsBeforeExpirationReturnableSealed != null)
            {
               inDateSealed =
                  policyForm.SelectedLot.ExpirationDate.Value.AddMonths(
                     -policy.NumberOfMonthsBeforeExpirationReturnableSealed.Value
                     + (policy.ExpiresOnFirstOfCurrentMonth ?? false ? 0 : 1));
            }
         }

         var groupGuid = Guid.NewGuid();
         decimal notificationQty = 1;
         var containerType = policyForm.ContainerType.Split('|')[1];

         if (policyForm.Quantity > 1 && (containerType == ContainerType.Case.ToString() ||
                                         containerType == ContainerType.Sealed.ToString()))
         {
            notificationQty = policyForm.Quantity;
         }

         policyForm.ContainerType += "|" + policyForm.ContainerCondition;

         try
         {
            using (var entites = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               for (var i = 0; i < notificationQty; i++)
               {
                  var notification = new Qualanex.Qosk.Library.Model.DBModel.ProductNotification()
                  {
                     ExpirationDate = policyForm.SelectedLot.ExpirationDate,
                     LotNumber = policyForm.SelectedLot.LotNumber,
                     ProductID = policyForm.ProductId,
                     CreatedBy = logon,
                     FutureIndateOpen = inDateOpen,
                     FutureIndateSealed = inDateSealed,
                     CreatedDate = DateTime.UtcNow,
                     UserProfileCode = userProfileCode,
                     ContainerTypeName = policyForm.ContainerType,
                     CaseSize = policyForm.SelectedCaseSize,
                     Status = NotificationStatus.Pending.ToString(),
                     GroupGUID = groupGuid,
                     QoskMachineID = "           "
                  };

                  entites.ProductNotification.Add(notification);


                  if (sideImageBlob != null)
                  {
                     var appendFileName = "_ReturnedToStock_1_SideImage.jpg";
                     ProductCommon.CopyBlobToItemContainer(groupGuid, appendFileName, sideImageBlob, Constants.AzureAccount_Image);
                  }

                  if (topImageBlob != null)
                  {
                     var appendFileName = "_ReturnedToStock_3_TopImage.jpg";
                     ProductCommon.CopyBlobToItemContainer(groupGuid, appendFileName, topImageBlob, Constants.AzureAccount_Image);
                  }

                  if (contentImageBlob != null)
                  {
                     var appendFileName = "_ReturnedToStock_3_ContentImage.jpg";
                     ProductCommon.CopyBlobToItemContainer(groupGuid, appendFileName, contentImageBlob, Constants.AzureAccount_Image);
                  }
               }

                //delete the original images once they've been moved
                //remove this and setup an async purge of the webcam-uploads blob directory in the future

                //commented out 2.15.19 as we're running into items with missing images in their respective blob storage locations
                //ProductCommon.DeleteImage(sideImageBlob);
                //ProductCommon.DeleteImage(topImageBlob);
                //ProductCommon.DeleteImage(contentImageBlob);

                return (entites.SaveChanges() == 1);
            }
         }
         catch (Exception ex)
         {
            throw;
            //   Logger.Error(ex);
            //   return false;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Updates a list of product notification IDs to the required status
      /// </summary>
      /// <param name="notificationIds"></param>
      /// <param name="status"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool UpdateNotifications(List<long> notificationIds, NotificationStatus status)
      {
         try
         {
            using (var entites = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               entites.ProductNotification.Where(n => notificationIds.Contains(n.NotificationID))
                  .ForEach(n =>
               {
                  n.Status = status.ToString();
                  n.ModifiedDate = DateTime.UtcNow;
               });
               entites.SaveChanges();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            return false;
         }

         return true;
      }

      /// ****************************************************************************
      ///  <summary>
      ///    Saves or updates item records and associated records, such as green bin reasons
      /// non-credit reasons, and waste stream.
      ///  </summary>
      ///  <param name="itemGuid">the identifier for the item being saved or updated</param>
      ///  <param name="controlNumber">the item's control number</param>
      ///  <param name="state">the user profile's current state (IL, WA, CA, etc)</param>
      ///  <param name="quantity">the quantity value for the item being saved</param>
      ///  <param name="logOn">the logged in username</param>
      ///  <param name="userProfileCode">the logged in user's profile code</param>
      ///  <param name="form">reflects the input values for the item being inducted</param>
      ///  <param name="wasteCodes">waste codes associated with this item</param>
      ///  <param name="updateItemRecord">if true, an existing item will be updated with the given data</param>
      ///  <param name="appCode">the app recorded as performing the update</param>
      ///  <param name="qoskId">ID Tied to the workstation where the item was processed; default to 1 if PC is not registered</param>
      /// <returns></returns>
      /// ****************************************************************************
      public static Guid SaveItem(Guid itemGuid, byte controlNumber, string state, decimal quantity, string logOn, PolicyForm form, List<WasteCode> wasteCodes, bool updateItemRecord, string appCode, int userProfileCode = -99, int qoskId = 1)
      {
         try
         {
            var wasteStreams = GetWasteStreams(form.Product.NDCInnerPack ? (long)form.RollupProductId : form.ProductId, ProductCommon.GetUserState()).FirstOrDefault()?.WasteStreamCode;
            var incomingConditionList = form.ContainerCondition.Split('|').ToList();

            //determine if we have a re-processed item i.e. GUID scanned in the NDC/UPC field
            //the form's ScannedBarCode field will always be null when processing an item in Green Bin; hence, the appcode check
            var parseItemGuid = new Guid();
            var parsedGuid = Guid.TryParseExact(form.ScannedBarCode, "D", out parseItemGuid) || appCode == "WAREHS_GRNBIN";

            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var wasteStreamProfileId = !string.IsNullOrEmpty(wasteStreams) ? (from wspr in entities.WasteStreamProfileRel where wspr.WasteStreamCode == wasteStreams select wspr.WasteStreamProfileID).SingleOrDefault() : null;

               //retrieve any existing item for this guid
               var oldItem = (from it in entities.Item where it.ItemGUID == itemGuid select it).SingleOrDefault();
               var item = new Qualanex.Qosk.Library.Model.DBModel.Item()
               {
                  ItemGUID = itemGuid,
                  AppCode = appCode,
                  QoskID = qoskId,
                  LotNumber = form.SelectedLot.LotNumber,
                  ConditionCode = "UNKN",
                  ContainerTypeName = form.ContainerType,
                  ExpirationDate = form.SelectedLot.ExpirationDate,
                  ProductID = form.ProductId,
                  Quantity = quantity,
                  ContainerWeight = form.ProductWeight ?? 0,
                  ContentsWeight = form.ContentWeight ?? 0,
                  CaseSize = form.SelectedCaseSize ?? 0,
                  CreatedBy = logOn,
                  CreatedDate = DateTime.UtcNow,
                  Comments = form.ProductComment,
                  IsCredit = form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.CREDIT && !form.IsForeignContainer,
                  Returnable = form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.CREDIT && !form.IsForeignContainer,
                  PolicyCode = form.PolicyResult.PolicyModel.ProductPolicy.Policy?.PolicyCode ?? "Unknown",
                  PolicyVersion = 1,
                  ControlNumber = controlNumber,
                  Shippable = true,
                  RecallID = form.SelectedLot.RecallId ?? ProductCommon.GetRecallId(form.Product.NDCInnerPack ? (int)form.RollupProductId : form.ProductId, form.SelectedLot, form.ReturnInformation?.Tracking?.RcvdDate),
                  ItemStateCode = state,
                  SerialNumber = parsedGuid ? oldItem?.SerialNumber : form.SerialNumber,
                  ScannedBarcode = parsedGuid ? oldItem?.ScannedBarcode : form.ScannedBarCode, //do not place the item GUID as the scanned barcode (re-process only)
                  TrackingID = form.ReturnInformation?.Tracking?.TrackingID,
                  ProfileCode = form.Product.ProfileCode,
                  CreditDispositionCode = form.PolicyResult.PolicyModel.CreditDisposition.ToString(),
                  UserProfileCode = userProfileCode,
                  Disposition = form.PolicyResult.Disposition,
                  RollupProductID = form.Product.NDCInnerPack ? form.RollupProductId : (int?)null,
                  WasteStreamProfileID = wasteStreamProfileId
               };

               SaveGreenBinReason(itemGuid, form.GreenBinReason, form.GreenBinOverrideReason, logOn);

               if (form.IsForeignContainer)
               {
                  item.NonReturnableReason = "FOREIGNCNTNR";
                  item.NonReturnableReasonDetail = "Foreign containers are not eligible for credit";
               }
               else if ((form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.RTN4AGING && item.Disposition == "Aging") || form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.AGINGNOSTK)
               {
                  item.IsCredit = true;
                  item.Returnable = true;
                  item.NonReturnableReason = null;
                  item.NonReturnableReasonDetail = null;
               }
               else if (form.PolicyResult.PolicyModel.Exceptions.Count > 0)
               {
                  item.NonReturnableReason = "";
                  item.NonReturnableReasonDetail = "";

                  foreach (var exception in form.PolicyResult.PolicyModel.Exceptions)
                  {
                     item.NonReturnableReason += item.NonReturnableReason.Length != 0 ? "|" : "";
                     item.NonReturnableReason += exception.Violation.ToString();

                     item.NonReturnableReasonDetail += item.NonReturnableReasonDetail.Length != 0 ? "|" : "";
                     item.NonReturnableReasonDetail += exception.Exception;
                  }
               }

               if (updateItemRecord)
               {
                  oldItem.QoskID = qoskId;
                  oldItem.AppCode = appCode;
                  oldItem.LotNumber = form.SelectedLot.LotNumber;
                  oldItem.ConditionCode = "UNKN";
                  oldItem.ContainerTypeName = form.ContainerType;
                  oldItem.ExpirationDate = form.SelectedLot.ExpirationDate;
                  oldItem.ProductID = form.ProductId;
                  oldItem.Quantity = quantity;
                  oldItem.ContainerWeight = form.ProductWeight ?? 0;
                  oldItem.ContentsWeight = form.ContentWeight ?? 0;
                  oldItem.CaseSize = form.SelectedCaseSize ?? 0;
                  oldItem.ModifiedBy = logOn;
                  oldItem.Comments = form.ProductComment;
                  oldItem.IsCredit = form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.CREDIT && !form.IsForeignContainer;
                  oldItem.Returnable = form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.CREDIT && !form.IsForeignContainer;
                  oldItem.PolicyCode = form.PolicyResult.PolicyModel.ProductPolicy.Policy?.PolicyCode ?? "Unknown";
                  oldItem.PolicyVersion = 1;
                  oldItem.ControlNumber = controlNumber;
                  oldItem.Shippable = true;
                  oldItem.RecallID = form.SelectedLot.RecallId ?? ProductCommon.GetRecallId(form.Product.NDCInnerPack ? (int)form.RollupProductId : form.ProductId, form.SelectedLot, form.ReturnInformation?.Tracking?.RcvdDate);
                  oldItem.ItemStateCode = state;
                  oldItem.SerialNumber = parsedGuid ? oldItem.SerialNumber : form.SerialNumber;
                  oldItem.ScannedBarcode = parsedGuid ? oldItem.ScannedBarcode : form.ScannedBarCode; //do not place the item GUID as the scanned barcode
                  oldItem.ProfileCode = form.Product.ProfileCode;
                  oldItem.CreditDispositionCode = form.PolicyResult.PolicyModel.CreditDisposition.ToString();
                  oldItem.UserProfileCode = userProfileCode;
                  oldItem.Disposition = form.PolicyResult.Disposition;
                  oldItem.RollupProductID = form.Product.NDCInnerPack ? form.RollupProductId : (int?)null;
                  oldItem.WasteStreamProfileID = wasteStreamProfileId;

                  if (form.IsForeignContainer)
                  {
                     oldItem.NonReturnableReason = "FOREIGNCNTNR";
                     oldItem.NonReturnableReasonDetail = "Foreign containers are not eligible for credit";
                  }
                  else if ((form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.RTN4AGING && item.Disposition == "Aging") || form.PolicyResult.PolicyModel.CreditDisposition == CreditDisposition.AGINGNOSTK)
                  {
                     oldItem.IsCredit = true;
                     oldItem.Returnable = true;
                     oldItem.NonReturnableReason = null;
                     oldItem.NonReturnableReasonDetail = null;
                  }
                  else if (form.PolicyResult.PolicyModel.Exceptions.Count == 0)
                  {
                     oldItem.NonReturnableReason = null;
                     oldItem.NonReturnableReasonDetail = null;
                  }
                  else if (form.PolicyResult.PolicyModel.Exceptions.Count > 0)
                  {
                     oldItem.NonReturnableReason = "";
                     oldItem.NonReturnableReasonDetail = "";

                     foreach (var exception in form.PolicyResult.PolicyModel.Exceptions)
                     {
                        oldItem.NonReturnableReason += oldItem.NonReturnableReason.Length != 0 ? "|" : "";
                        oldItem.NonReturnableReason += exception.Violation.ToString();

                        oldItem.NonReturnableReasonDetail += oldItem.NonReturnableReasonDetail.Length != 0 ? "|" : "";
                        oldItem.NonReturnableReasonDetail += exception.Exception;
                     }
                  }

                  UpdateItemConditions(incomingConditionList, itemGuid, logOn, appCode);
                  UpdateItemConditions(incomingConditionList, itemGuid, logOn, "MASTER");
               }
               else
               {
                  //if an item with this guid already exists and updates are not being performed, there is nothing left to do
                  if (form.ItemGuid == itemGuid && oldItem != null)
                  {
                     using (var updateEntity = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
                     {
                        //saving audit items in Green Bin application
                        if (form.BinDesignation == Constants.WhiteSortBin && controlNumber != 2 && appCode != "WAREHS_AUDIT")
                        {
                           try
                           {
                              var auditRecord = (from audit in updateEntity.AuditPending where audit.ItemGUID == item.ItemGUID select audit).FirstOrDefault();
                              var auditItem = AuditFromItem(item);

                              if (auditRecord == null)
                              {
                                 updateEntity.AuditPending.Add(auditItem);
                              }
                              else
                              {
                                 auditRecord.AppCode = auditItem.AppCode;
                                 auditRecord.CreatedDate = auditItem.CreatedDate;
                                 auditRecord.Status = auditItem.Status;
                                 auditRecord.LotNumber = auditItem.LotNumber;
                                 auditRecord.Quantity = auditItem.Quantity;
                                 auditRecord.ExpirationDate = auditItem.ExpirationDate;
                                 auditRecord.CaseSize = auditItem.CaseSize;
                                 auditRecord.ItemGUID = auditItem.ItemGUID;
                                 auditRecord.QoskID = auditItem.QoskID;
                                 auditRecord.ContainerWeight = auditItem.ContainerWeight;
                                 auditRecord.ProfileCode = auditItem.ProfileCode;
                                 auditRecord.SerialNumber = auditItem.SerialNumber;
                                 auditRecord.IsDeleted = auditItem.IsDeleted;
                                 auditRecord.ContainerTypeName = auditItem.ContainerTypeName;
                                 auditRecord.CreatedBy = auditItem.CreatedBy;
                                 auditRecord.Version = auditItem.Version;
                                 auditRecord.ProductID = auditItem.ProductID;
                                 auditRecord.ItemStateCode = auditItem.ItemStateCode;
                                 auditRecord.ConditionCode = "UNKN";
                                 auditRecord.ModifiedDate = auditItem.ModifiedDate;
                                 auditRecord.EffectiveEndDate = auditItem.EffectiveEndDate;
                                 auditRecord.EffectiveStartDate = auditItem.EffectiveStartDate;
                                 auditRecord.ModifiedBy = auditItem.ModifiedBy;
                                 auditRecord.Attempt = auditItem.Attempt;
                                 auditRecord.BatchId = auditItem.BatchId;
                                 auditRecord.Comments = auditItem.Comments;
                                 auditRecord.ContentsWeight = auditItem.ContentsWeight;
                                 auditRecord.ControlNumber = auditItem.ControlNumber;
                                 auditRecord.DebitMemoUnitReturnValue = auditItem.DebitMemoUnitReturnValue;
                                 auditRecord.DivestedManufacturerProfileCode = auditItem.DivestedManufacturerProfileCode;
                                 auditRecord.ExternalInterfaceDate = auditItem.ExternalInterfaceDate;
                                 auditRecord.ForeginProductColorSide1 = auditItem.ForeginProductColorSide1;
                                 auditRecord.ForeginProductColorSide2 = auditItem.ForeginProductColorSide2;
                                 auditRecord.ForeginProductImprint = auditItem.ForeginProductImprint;
                                 auditRecord.IsCredit = auditItem.IsCredit;
                                 auditRecord.ItemUnitPrice = auditItem.ItemUnitPrice;
                                 auditRecord.ItemUnitPriceNoFees = auditItem.ItemUnitPriceNoFees;
                                 auditRecord.ItemValue = auditItem.ItemValue;
                                 auditRecord.ManufacturerPriceOverride = auditItem.ManufacturerPriceOverride;
                                 auditRecord.NonReturnableReason = auditItem.NonReturnableReason;
                                 auditRecord.NonReturnableReasonDetail = auditItem.NonReturnableReasonDetail;
                                 auditRecord.PartialReturnThresholdPecentage = auditItem.PartialReturnThresholdPecentage;
                                 auditRecord.PolicyCode = auditItem.PolicyCode;
                                 auditRecord.PolicyVersion = auditItem.PolicyVersion;
                                 auditRecord.RecallID = auditItem.RecallID;
                                 auditRecord.RepackagerProfileCode = auditItem.RepackagerProfileCode;
                                 auditRecord.ReturnAuthorizationsItemsMatchAccuracy = auditItem.ReturnAuthorizationsItemsMatchAccuracy;
                                 auditRecord.Returnable = auditItem.Returnable;
                                 auditRecord.ReturnableItemLowestPriceValue = auditItem.ReturnableItemLowestPriceValue;
                                 auditRecord.ReturnableItemLowestPriceValueNoFees = auditItem.ReturnableItemLowestPriceValueNoFees;
                                 auditRecord.ReturnableItemValue = auditItem.ReturnableItemValue;
                                 auditRecord.ScannedBarcode = auditItem.ScannedBarcode;
                                 auditRecord.SecondaryConditionID = auditItem.SecondaryConditionID;
                                 auditRecord.Shippable = auditItem.Shippable;
                                 auditRecord.ThirdConditionID = auditItem.ThirdConditionID;
                                 auditRecord.TrackingID = auditItem.TrackingID;
                                 auditRecord.ValidityState = auditItem.ValidityState;

                                 UpdateItemConditions(incomingConditionList, itemGuid, logOn, appCode);
                                 UpdateItemConditions(incomingConditionList, itemGuid, logOn, "MASTER");
                              }

                              updateEntity.SaveChanges();
                           }
                           catch (Exception ex)
                           {
                              Logger.Error(ex.InnerException);
                           }
                        }

                        if (form.BinDesignation == Constants.BlackSortBin)
                        {
                           Form222MatchItem(form, itemGuid, logOn);
                        }

                        //update the individual item fields based on the completed green bin segments
                        if (appCode == "WAREHS_GRNBIN")
                        {
                           //update the RecallID field in Item table, if needed
                           if (item.RecallID != oldItem.RecallID)
                           {
                              try
                              {
                                 var updateRecallId = (from it in updateEntity.Item where it.ItemGUID == itemGuid select it).SingleOrDefault();
                                 updateRecallId.RecallID = item.RecallID;
                                 updateEntity.SaveChanges();
                              }
                              catch (Exception ex)
                              {
                                 Logger.Error(ex.InnerException);
                              }
                           }

                           //update the WasteStreamProfileID field in Item table, if needed
                           if (item.WasteStreamProfileID != oldItem.WasteStreamProfileID)
                           {
                              try
                              {
                                 var updateWasteStream = (from it in updateEntity.Item where it.ItemGUID == itemGuid select it).SingleOrDefault();
                                 updateWasteStream.WasteStreamProfileID = item.WasteStreamProfileID;
                                 updateEntity.SaveChanges();

                                 //if the item went to a green bin for a missing waste stream, then it never received an entry into the ItemWasteCode table
                                 foreach (var waste in wasteCodes)
                                 {
                                    //check for duplicates ItemWasteCode records
                                    var iwcEntity = updateEntity.ItemWasteCode.FirstOrDefault(c => c.ItemGUID == itemGuid && c.WasteCode == waste.Code && c.StateCode == "");

                                    if (iwcEntity == null)
                                    {
                                       var iwc = new ItemWasteCode
                                       {
                                          CreatedDate = DateTime.UtcNow,
                                          CreatedBy = logOn,
                                          ItemGUID = itemGuid,
                                          Version = 1,
                                          IsDeleted = false,
                                          WasteCode = waste.Code,
                                          StateCode = ""
                                       };

                                       updateEntity.ItemWasteCode.Add(iwc);
                                       updateEntity.SaveChanges();
                                    }
                                 }
                              }
                              catch (Exception ex)
                              {
                                 Logger.Error(ex.InnerException);
                              }
                           }
                        }
                     }

                     return item.ItemGUID;
                  }

                  entities.Item.Add(item);

                  UpdateItemConditions(incomingConditionList, itemGuid, logOn, appCode);
                  UpdateItemConditions(incomingConditionList, itemGuid, logOn, "MASTER");
               }

               if (form.BinDesignation == Constants.WhiteSortBin && controlNumber != 2 && !IsInAudit(item.ItemGUID))
               {
                  entities.AuditPending.Add(AuditFromItem(item));
               }

               entities.SaveChanges();

               //update the dea forms if a C2 item is going into a black bin
               if (form.BinDesignation == Constants.BlackSortBin)
               {
                  Form222MatchItem(form, itemGuid, logOn);
               }

               //=============================================================================
               // Associate policy violation(s)
               //-----------------------------------------------------------------------------

               // Save each policy violation
               //foreach (var returnReason in form.PolicyResult.PolicyModel.Exceptions.Select(
               //   violation => new ItemNonCreditRel
               //   {
               //      ItemGUID = itemGuid,
               //      AppCode = appCode,
               //      Version = 1,
               //      IsDeleted = false,
               //      Description = violation.Exception.ToString()
               //   }))
               //{
               //   entities.ItemNonCreditRel.Add(returnReason);
               //   entities.SaveChanges();
               //}

               //=============================================================================
               // Associate waste classification(s)
               //-----------------------------------------------------------------------------

               // Save each of the waste classifications
               //foreach (var waste in wasteCodes.Select(
               //   classification => new Qualanex.Qosk.Library.Model.DBModel.ItemWasteCode
               //   {
               //      CreatedDate = DateTime.UtcNow,
               //      CreatedBy = logOn,
               //      ItemGUID = itemGuid,
               //      Version = 1,
               //      IsDeleted = false,
               //      WasteCode = classification.Code,
               //      StateCode = classification.StateCode
               //   }))
               //{
               //   entities.ItemWasteCode.Add(waste);
               //   entities.SaveChanges();
               //}

               //Save each of the waste classifications
               foreach (var waste in wasteCodes)
               {
                  //check for duplicates ItemWasteCode records
                  var iwcEntity = entities.ItemWasteCode.FirstOrDefault(classification =>
                     classification.ItemGUID == itemGuid && classification.WasteCode == waste.Code && classification.StateCode == waste.StateCode);

                  if (iwcEntity != null)
                  {
                     continue;
                  }

                  var iwc = new ItemWasteCode
                  {
                     CreatedDate = DateTime.UtcNow,
                     CreatedBy = logOn,
                     ItemGUID = itemGuid,
                     Version = 1,
                     IsDeleted = false,
                     WasteCode = waste.Code,
                     StateCode = waste.StateCode
                  };

                  entities.ItemWasteCode.Add(iwc);
                  entities.SaveChanges();
               }


               // TODO: fixed commented code
               //reasonOfReturn = new Reason_Return()
               //{
               //   ItemID = item.ItemID,
               //   QoskID = qoskID,
               //   ReasonOfReturnType = reasonType,
               //   ReasonOfReturnMemo = reasonComment,
               //   ProfileCode = Convert.ToInt32(profileCode),
               //   IsActive = ConstantAndEnum.UserStatus_Active,
               //   CreatedBy = AddProductViewModel.LogOn,
               //   CreatedDate = DateTime.UtcNow
               //};

               //entities.Reason_Return.Add(reasonOfReturn);

               return item.ItemGUID;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
               foreach (var validationError in validationErrors.ValidationErrors)
               {
                  Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                     validationErrors.Entry.Entity.GetType().FullName,
                     validationError.PropertyName,
                     validationError.ErrorMessage);
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return Guid.Empty;
      }

      /// <summary>
      /// Helper method copying all fields from an item entity to an identical auditpending entity
      /// </summary>
      /// <param name="item"></param>
      /// <returns></returns>
      private static AuditPending AuditFromItem(Item item)
      {
         return new AuditPending()
         {
            AppCode = item.AppCode,
            CreatedDate = item.CreatedDate,
            Status = item.Status,
            LotNumber = item.LotNumber,
            Quantity = item.Quantity,
            ExpirationDate = item.ExpirationDate,
            CaseSize = item.CaseSize,
            ItemGUID = item.ItemGUID,
            QoskID = item.QoskID,
            ContainerWeight = item.ContainerWeight,
            ProfileCode = item.ProfileCode,
            SerialNumber = item.SerialNumber,
            IsDeleted = item.IsDeleted,
            ContainerTypeName = item.ContainerTypeName,
            CreatedBy = item.CreatedBy,
            Version = item.Version,
            ProductID = item.ProductID,
            ItemStateCode = item.ItemStateCode,
            ConditionCode = item.ConditionCode,
            ModifiedDate = item.ModifiedDate,
            EffectiveEndDate = item.EffectiveEndDate,
            EffectiveStartDate = item.EffectiveStartDate,
            ModifiedBy = item.ModifiedBy,
            Attempt = item.Attempt,
            BatchId = item.BatchId,
            Comments = item.Comments,
            ContentsWeight = item.ContentsWeight,
            ControlNumber = item.ControlNumber,
            DebitMemoUnitReturnValue = item.DebitMemoUnitReturnValue,
            DivestedManufacturerProfileCode = item.DivestedManufacturerProfileCode,
            ExternalInterfaceDate = item.ExternalInterfaceDate,
            ForeginProductColorSide1 = item.ForeginProductColorSide1,
            ForeginProductColorSide2 = item.ForeginProductColorSide2,
            ForeginProductImprint = item.ForeginProductImprint,
            IsCredit = item.IsCredit,
            ItemUnitPrice = item.ItemUnitPrice,
            ItemUnitPriceNoFees = item.ItemUnitPriceNoFees,
            ItemValue = item.ItemValue,
            ManufacturerPriceOverride = item.ManufacturerPriceOverride,
            NonReturnableReason = item.NonReturnableReason,
            NonReturnableReasonDetail = item.NonReturnableReasonDetail,
            PartialReturnThresholdPecentage = item.PartialReturnThresholdPecentage,
            PolicyCode = item.PolicyCode,
            PolicyVersion = item.PolicyVersion,
            RecallID = item.RecallID,
            RepackagerProfileCode = item.RepackagerProfileCode,
            ReturnAuthorizationsItemsMatchAccuracy = item.ReturnAuthorizationsItemsMatchAccuracy,
            Returnable = item.Returnable,
            ReturnableItemLowestPriceValue = item.ReturnableItemLowestPriceValue,
            ReturnableItemLowestPriceValueNoFees = item.ReturnableItemLowestPriceValueNoFees,
            ReturnableItemValue = item.ReturnableItemValue,
            ScannedBarcode = item.ScannedBarcode,
            SecondaryConditionID = item.SecondaryConditionID,
            Shippable = item.Shippable,
            ThirdConditionID = item.ThirdConditionID,
            TrackingID = item.TrackingID,
            ValidityState = item.ValidityState
         };
      }

      /// <summary>
      /// Saves and updates the reasons for sorting an item to a green bin
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <param name="reasons">the system generated reasons</param>
      /// <param name="overrideReasons">the user selected reasons</param>
      /// <param name="userName">the currently logged in user</param>
      public static void SaveGreenBinReason(Guid itemGuid, GreenBinReason reasons, GreenBinReason overrideReasons, string userName)
      {

         using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var guid = itemGuid.ToString();

            if (reasons.HasFlag(GreenBinReason.ContainerImage))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductContainerImage,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons.HasFlag(GreenBinReason.ContentImage))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductContentImage,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons.HasFlag(GreenBinReason.LotExpiration))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_LotExpiration,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons.HasFlag(GreenBinReason.Weight))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductWeight,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons.HasFlag(GreenBinReason.WasteStream))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductWaste,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons.HasFlag(GreenBinReason.Quarantine))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_Quarantine,
                  SValue = Constants.Property_SValue_Required
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.ContainerImage))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductContainerImage,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.ContentImage))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductContentImage,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.LotExpiration))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_LotExpiration,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.Weight))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductWeight,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.WasteStream))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_ProductWaste,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (overrideReasons.HasFlag(GreenBinReason.Quarantine))
            {
               var property = new PropertyValueRel()
               {
                  CreatedBy = userName,
                  CreatedDate = DateTime.UtcNow,
                  RelationCode = guid,
                  PropertyCode = Constants.Property_Code_Quarantine,
                  SValue = Constants.Property_SValue_ManualOverride
               };

               var existingPvr = CheckPropertyValueRel(property, entities);

               if (existingPvr == null)
               {
                  entities.PropertyValueRel.Add(property);
               }
               else
               {
                  if (existingPvr.SValue != Constants.Property_SValue_Required)
                  {
                     existingPvr.Version++;
                  }

                  existingPvr.ModifiedBy = property.CreatedBy;
                  existingPvr.ModifiedDate = property.CreatedDate;
                  existingPvr.SValue = property.SValue;
               }
            }

            if (reasons != 0 || overrideReasons != 0)
            {
               entities.SaveChanges();
            }
         }
      }


      /// <summary>
      /// Retrieves an audit rate for a user, defaults to 6
      /// Results are cached
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      internal static int GetUserAuditRate(long userId)
      {

         var key = $"GetUserAuditRate|{userId}";
         var model = MemoryCache.Default[key] as int?;

         if (model != null)
         {
            return model.Value;
         }

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var auditRateModel = (from profile in cloudEntities.UserProfiles where profile.UserID == userId select profile).FirstOrDefault();
               model = auditRateModel?.AuditRate;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         model = model ?? 2; //If there's no user audit rate set, we default the audit percentage rate to 2%
         Common.Methods.SetCache(key, model);
         return model.Value;
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves the hazard level for a product in a state.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="state"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal static int GetHazardClass(long productID, string state)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               if ((from productWasteCode in cloudEntities.ProductWasteCode
                    join wasteCodeDict in cloudEntities.WasteCodeDict on productWasteCode.WasteCode equals
                       wasteCodeDict.Code
                    where
                       productWasteCode.ProductID == productID &&
                       productWasteCode.StateCode == state &&
                       wasteCodeDict.SeverityOrder < 4
                    select wasteCodeDict.StateCode).Any())
               {
                  return 2;
               }

               if ((from productWasteCode in cloudEntities.ProductWasteCode
                    join wasteCodeDict in cloudEntities.WasteCodeDict on productWasteCode.WasteCode equals
                       wasteCodeDict.Code
                    where
                       productWasteCode.ProductID == productID &&
                       productWasteCode.StateCode == string.Empty &&
                       wasteCodeDict.SeverityOrder < 4
                    select wasteCodeDict.StateCode).Any())
               {
                  return 3;
               }

               return 0;

            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }
         return 0;
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves applicable waste codes for a product.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="state"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal static List<WasteCode> GetMinimalWasteCodes(long productId, string state)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var wasteCodes = (from productWasteCode in cloudEntities.ProductWasteCode
                                 join wasteCodeDict in cloudEntities.WasteCodeDict on productWasteCode.WasteCode equals wasteCodeDict.Code
                                 where productWasteCode.ProductID == productId && (productWasteCode.StateCode == state || productWasteCode.StateCode == string.Empty)
                                 select new WasteCode()
                                 {
                                    Description = wasteCodeDict.Description,
                                    Code = wasteCodeDict.Code,
                                    StateCode = wasteCodeDict.StateCode,
                                    SeverityOrder = wasteCodeDict.SeverityOrder
                                 }).ToList();

               return wasteCodes;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves the policy status of an item from the policy engine.
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="app">get the current application code</param>
      /// <returns></returns>
      ///****************************************************************************
      internal static PolicyResult GetPolicyResult(PolicyForm policyForm, string app = null)
      {
         int? profileCode = 0;

         try
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);

            if (objReturnLoginUserEntity == null)
            {
               return new PolicyResult() { LogOut = true };
            }

            profileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode;
         }
         catch (Exception ex)
         {
            throw;
         }
         if (app == null)
         {
            app = QoskSessionHelper.GetReferringPage().Split('=')[1];
         }
         if (policyForm.ProductId == 0)
         {
            return new PolicyResult();
         }
         if (policyForm.IsForeignContainer)
         {
            var foreignModel = new PolicyResult()
            {
               PolicyModel = new PolicyModel(),
               PolicyInDate = PolicyInDate.OUTDATE,
               CreditDisposition = CreditDisposition.DESTRUCTION,
               PolicyStatus = "",
               ProductId = policyForm.ProductId,
               LabelUrl = "/Areas/Product/Images/sample_label.jpg"
            };
         }

         var packageContainer = new string[] { PackageType.Unknown.ToString(), ContainerType.Unknown.ToString() };

         if (policyForm?.ContainerType != null)
         {
            var splitTypes = policyForm.ContainerType.Split('|');
            for (var i = 0; i < splitTypes.Length; i++)
            {
               packageContainer[i] = splitTypes[i];
            }
         }

         //BEGIN DEBUGGING
         //note that we're using the ri variable below for some debugging. This can be simplified once the debugging ends
         ReturnInformation ri = null;
         Logger.Info($"About to call GetRaForItem from GetPolicyResult");
         ri = SidebarCommon.GetRaForItem(policyForm.ItemGuid);
         Logger.Info($"Receiving response from GetRaForItem");

         if (ri == null)
         {
            Logger.Info($"GetRaForItem returned null; about to call GetRaForTote from GetPolicyResult");
            ri = SidebarCommon.GetRaForTote(policyForm.InboundContainer);
            Logger.Info($"Receiving response from GetRaForTote");
         }

         var ra = ri;//SidebarCommon.GetRaForItem(policyForm.ItemGuid) ?? SidebarCommon.GetRaForTote(policyForm.InboundContainer);
         //END OF DEBUGGING

         List<string> toCondition = new List<string>();
         if (string.IsNullOrEmpty(policyForm.ContainerCondition))
         {
            toCondition.Add("Unknown");
         }
         else
         {
            toCondition = policyForm.ContainerCondition.Split('|').ToList();
         }

         GetContainerSettings(policyForm, app == "PHARMA_DECISION" ? policyForm.Product.ProfileCode : ra?.DebitMemo?.MfrProfileCode ?? 0);

         var policyResult = new PolicyModel
         {
            ToContainerCondition = toCondition,
            ExpirationDate = policyForm.SelectedLot.ExpirationDate,
            LotNumber = policyForm.SelectedLot.LotNumber,
            ProductID = policyForm.ProductId,
            CustProfileCode = app == "PHARMA_DECISION" ? (int)profileCode : ra?.DebitMemo?.ProfileCode ?? 0,
            ToContainerType = packageContainer[1],
            ToPackageType = packageContainer[0],
         };

         var dbModel = new DBModel("name=QoskCloud");

         //Method is found in QoskLibrary
         policyResult.Initialize(ref dbModel);

         var model = new PolicyResult()
         {
            PolicyModel = policyResult,
            PolicyInDate = policyResult.PolicyInDate,
            CreditDisposition = policyResult.CreditDisposition,
            PolicyStatus = policyResult.Disposition,
            ProductId = policyForm.ProductId,
            LabelUrl = "/Areas/Product/Images/sample_label.jpg"
         };

         // Convert a list of object to a list of strings
         // TODO: Add override on exception class in QoskLibrary to do this for us
         foreach (var exception in policyResult.Exceptions)
         {
            model.Violations.Add(exception.Exception);
         }

         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns all lots tied to an NDC.
      /// </summary>
      /// <param name="NDC"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<LotInfo> GetLotNumber(long NDC)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();
         var lotList = new List<LotInfo>();

         try
         {
            lotList = (from lot in cloudEntities.Lot
                       join product in cloudEntities.Product on lot.ProductID equals product.ProductID
                       where product.NDC == NDC
                             && !lot.IsDeleted
                             && !product.IsDeleted
                       select new LotInfo
                       {
                          LotNumber = lot.LotNumber,
                          ExpirationDate = lot.ExpirationDate,
                          Recall = lot.RecallID.HasValue
                       }).Distinct().OrderBy(pp => pp.LotNumber).ToList();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return lotList;
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns the profile's container settings
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void GetContainerSettings(PolicyForm model, int profileCode)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         try
         {
            var contrSettingProfile = (from p in cloudEntities.Profile where p.ProfileCode == profileCode && !p.IsDeleted select p).SingleOrDefault();
            model.ContrSetting = contrSettingProfile.ContainerSetting & (int)ContainerSetting.ShowNoOuterPkg;
            model.FreeGoodsSetting = contrSettingProfile.ContainerSetting & (int)ContainerSetting.ShowFreeGoods;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="state"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WasteResult GetProductWasteInfo(long productID, string state)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();
         var productWasteList = new WasteResult();

         try
         {
            //confirm it's a real NDC or UPC
            var NDCUPC = (from product in cloudEntities.Product
                          where (product.ProductID == productID)
                          select new { product.NDC, product.UPC }).SingleOrDefault();
            // if not, don't bother
            if (NDCUPC != null)
            {
               productWasteList.Ndc = NDCUPC.NDC ?? NDCUPC.UPC;
               productWasteList.FederalWasteCodes = GetWasteCodes(productID, "");
               productWasteList.StateWasteCodes = GetWasteCodes(productID, state);
               productWasteList.WasteStreams = (from productWaste in cloudEntities.ProductWasteCode
                                                from wasteStream in cloudEntities.WasteStreamDict
                                                   .Where(x => x.Code == productWaste.WasteStreamCode)
                                                from wasteCode in cloudEntities.WasteCodeDict
                                                   .Where(x => x.Code == productWaste.WasteCode)
                                                from stateDict in cloudEntities.StateDict
                                                   .Where(x => x.Code == productWaste.StateCode).DefaultIfEmpty()
                                                where
                                                   (productWaste.ProductID == productID) &&
                                                   (productWaste.StateCode == state || productWaste.StateCode == "")
                                                   && !productWaste.IsDeleted
                                                   && !wasteStream.IsDeleted
                                                   && !wasteCode.IsDeleted
                                                   && !stateDict.IsDeleted
                                                orderby productWaste.StateCode descending
                                                select new WasteStream
                                                {
                                                   State = stateDict.Description,
                                                   Description = wasteStream.Description,
                                                   IsHazardous = wasteCode.SeverityOrder <= 3,
                                                   //CostPerPound = wasteStreamProfile.CostPerPound,
                                                   //MinimumPoundsPerPallet = wasteStreamProfile.MinimumPoundsPerPallet,
                                                   //MinimumPoundsPerDrum = wasteStreamProfile.MinimumPoundsPerDrum,
                                                   //MinimumPoundsPerBox = wasteStreamProfile.MinimumPoundsPerBox
                                                }).ToList();
            }

         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return productWasteList;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="state"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductWasteStream> GetWasteStreams(long productID, string state = null)
      {
         var key = $"ProductWasteStream|{productID}|{state}";
         var model = MemoryCache.Default[key] as List<ProductWasteStream>;

         if (model == null)
         {

            try
            {
               var emptyState = state.IsNullOrWhiteSpace();
               using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
               {
                  model = (from wasteStream in entities.ProductWasteStream
                           where wasteStream.ProductID == productID
                                 && (emptyState
                                     || state == wasteStream.StateCode
                                     || wasteStream.StateCode == "")
                                 && wasteStream.WasteStreamCode != "PHME" && wasteStream.WasteStreamCode != "UNKN"
                           select wasteStream).OrderBy(x => x.SeverityOrder).ToList();
                  Common.Methods.SetCache(key, model);
               }
            }
            catch (Exception e)
            {
               Logger.Error(e);
               throw;
            }
         }

         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="state"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static List<WasteCode> GetWasteCodes(long productID, string state = null)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         try
         {
            if (null != state)
            {
               var query = (from productWaste in cloudEntities.ProductWasteCode.Where(x => x.StateCode == state)
                            from wasteCodeDict in cloudEntities.WasteCodeDict
                               .Where(x => x.Code == productWaste.WasteCode && x.StateCode == state).DefaultIfEmpty()
                            from hazardEmptyDict in cloudEntities.HazardEmptyDict
                               .Where(x => x.Code == productWaste.HazardEmptyCode).DefaultIfEmpty()
                            from hazardFullPartialDict in cloudEntities.HazardFullPartialDict
                               .Where(x => x.Code == productWaste.HazardFullPartialCode).DefaultIfEmpty()
                            from hazardAddInfoDict in cloudEntities.HazardAddInfoDict
                               .Where(x => x.Code == productWaste.HazardAddInfoCode).DefaultIfEmpty()
                            from hazardReasonDict in cloudEntities.HazardReasonDict
                               .Where(
                                  x =>
                                     x.Code == productWaste.ReasonCode && x.Additional == "" &&
                                     x.StateCode == productWaste.StateCode).DefaultIfEmpty()
                            from hazardReasonDictAdditional in cloudEntities.HazardReasonDict
                               .Where(
                                  x => x.Code == productWaste.AddReasonCode && x.Additional == "A" && x.StateCode == "")
                               .DefaultIfEmpty()
                            from hazardStateSumDict in cloudEntities.HazardStateSumDict
                               .Where(x => x.Code == productWaste.StateCode).DefaultIfEmpty()
                            from dotWasteStreamDict in cloudEntities.DOTWasteStreamDict
                               .Where(x => x.Code == wasteCodeDict.DOTWasteStreamCode).DefaultIfEmpty()
                            from hazardTypeDict in cloudEntities.HazardTypeDict
                               .Where(x => x.Code == wasteCodeDict.HazardTypeCode).DefaultIfEmpty()
                            from stateDict in cloudEntities.StateDict
                               .Where(x => x.Code == productWaste.StateCode).DefaultIfEmpty()
                            where
                               (productWaste.ProductID == productID) && productWaste.StateCode == state
                            select new WasteCode
                            {
                               Code = productWaste.WasteCode,
                               Description = wasteCodeDict.Description,
                               StateCode = productWaste.StateCode,
                               State = stateDict.Description,
                               ActiveIngredient = wasteCodeDict.ActiveIngredient,
                               SeverityOrder = wasteCodeDict.SeverityOrder,
                               DotWasteStreamDescription = dotWasteStreamDict.Description,
                               DotSchedule0 = dotWasteStreamDict.Schedule0,
                               DotSchedule2 = dotWasteStreamDict.Schedule2,
                               DotSchedule345 = dotWasteStreamDict.Schedule345,
                               HazardType = hazardTypeDict.Description,
                               HazardEmptyDescription = hazardEmptyDict.Description,
                               HazardEmptyContent = hazardEmptyDict.Content,
                               HazardEmptyContentAddInfo = hazardEmptyDict.ContentAdditionalInfo,
                               HazardEmptyShippingLabel = hazardEmptyDict.ShippingLabel,
                               HazardFullPartialDescription = hazardFullPartialDict.Description,
                               HazardFullPartialContent = hazardFullPartialDict.Content,
                               HazardFullPartialContentAddInfo = hazardFullPartialDict.ContentAdditionalInfo,
                               HazardFullPartialShippingLabel = hazardFullPartialDict.ShippingLabel,
                               HazardAddInfo = hazardAddInfoDict.Description,
                               HazardAddInfoPage = productWaste.AddInfoPage,
                               AddInfoSdsUrl = new QoskHref
                               {
                                  Text = productWaste.AddInfoSDS,
                                  Href = productWaste.AddInfoSDSUrl
                               },
                               ReasonDescription = hazardReasonDict.Description,
                               ReasonDetail = hazardReasonDict.ReasonDetail,
                               ReasonUrls = (from hru in cloudEntities.HazardReasonUrl
                                             join pw in cloudEntities.ProductWasteCode on hru.ReasonCode equals pw.ReasonCode
                                             where (pw.ProductID == productID) && pw.StateCode == state
                                             select new QoskHref
                                             {
                                                Text = hru.UrlTitle,
                                                Href = hru.Url
                                             }).Distinct().ToList(),
                               AddReasonDescription = hazardReasonDictAdditional.Description,
                               AddReasonUrls = (from hru in cloudEntities.HazardReasonUrl
                                                join pw in cloudEntities.ProductWasteCode on hru.ReasonCode equals pw.AddReasonCode
                                                where (pw.ProductID == productID) && pw.StateCode == state
                                                select new QoskHref
                                                {
                                                   Text = hru.UrlTitle,
                                                   Href = hru.Url
                                                }).Distinct().ToList(),
                               AddReasonDetail = hazardReasonDictAdditional.ReasonDetail,
                               AddReasonHighlight = hazardReasonDictAdditional.HightlightFlag == "Y",
                               AddReasonText = productWaste.AddReasonText,
                               Highlights = productWaste.Highlights,
                               MsdsRsnWeb = productWaste.MsdsRsnWeb,
                               MsdsMfgWeb = productWaste.MsdsMfgWeb,
                               MsdsNotReq = productWaste.MsdsNotReqFlag == "Y",
                               PossibleSyringe = productWaste.PossibleSyringeFlag == "Y",
                               MsdsComment = new QoskHref
                               {
                                  Text = productWaste.MsdsComment,
                                  Href = productWaste.MsdsCommentUrl
                               },
                               StateSumDescription = hazardStateSumDict.StateSummary,
                               StateSum = new QoskHref
                               {
                                  Text = hazardStateSumDict.Description,
                                  Href = hazardStateSumDict.StateSummaryUrl
                               },
                               StateSums = (from hss in cloudEntities.HazardStateSumUrl
                                            join pw in cloudEntities.ProductWasteCode on hss.StateCode equals pw.StateCode
                                            where (pw.ProductID == productID) && pw.StateCode == state
                                            select new QoskHref
                                            {
                                               Text = hss.UrlTitle,
                                               Href = hss.Url
                                            }).Distinct().ToList()
                            });

               var wasteCodes = query.ToList();


               foreach (var wasteCode in wasteCodes)
               {
                  if (string.IsNullOrWhiteSpace(wasteCode.MsdsRsnWeb) && !string.IsNullOrWhiteSpace(wasteCode.AddInfoSdsUrl.Text)
                      && !string.IsNullOrEmpty(wasteCode.AddInfoSdsUrl.Href))
                  {
                     wasteCode.AddInfoSdsUrl.Text = "SDS provided by " + wasteCode.MsdsMfgWeb;

                  }
                  else if (!string.IsNullOrWhiteSpace(wasteCode.MsdsComment.Text)
                           && !string.IsNullOrEmpty(wasteCode.MsdsComment.Href))
                  {
                     wasteCode.MsdsComment.Text = "SDS provided by " + wasteCode.MsdsComment.Text;
                  }

                  if (string.IsNullOrWhiteSpace(wasteCode.AddInfoSdsUrl.Text))
                  {
                     wasteCode.AddInfoSdsUrl.Text = "For additional information, click here.";
                  }
               }

               return wasteCodes;
            }

            return new List<WasteCode>();
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return new List<WasteCode>();
      }

      ///****************************************************************************
      /// <summary>
      ///   Check for duplicate green bin reasons before saving
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static PropertyValueRel CheckPropertyValueRel(PropertyValueRel property, Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud entities = null)
      {
         try
         {
            if (entities == null)
            {
               entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();
            }

            var modelPvr = (from pvr in entities.PropertyValueRel where pvr.RelationCode == property.RelationCode && pvr.PropertyCode == property.PropertyCode select pvr).FirstOrDefault();
            return modelPvr;

         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieves a list of all manufacturers
      /// Results are cached.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      internal static List<string> GetManufacturers()
      {
         const string key = "ManufacturersDropDown";
         var model = MemoryCache.Default[key] as List<string>;

         if (model == null)
         {
            try
            {
               using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
               {
                  model = (from profile in entities.Profile
                           where profile.ProfileTypeCode == "MFGR" && !profile.IsDeleted
                           select profile.Name).OrderBy(r => r).ToList();
               }

               Common.Methods.SetCache(key, model);
            }
            catch (Exception ex)
            {
               Logger.Error(ex);
            }
         }

         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<int> GetCaseSizes(long productId)
      {
         List<int> caseSizes = new List<int>();

         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               caseSizes = (from productCaseSize in entities.ProductCaseSize
                            where productCaseSize.ProductID == productId && !productCaseSize.IsDeleted
                            select productCaseSize.CaseSize).ToList();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         return caseSizes;
      }

      public static Qosk GetQosk(string machineId)
      {
         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from qosk in entities.Qosk
                       where qosk.MachineID == machineId && !qosk.IsDeleted
                       select qosk).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            throw;
         }
      }

      public string GetDosageImageCode(string dosageCode)
      {
         var result = "UNKN";

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               result = (
                  from dpr in cloudEntities.DosagePackageRel
                  where dpr.DosageCode == dosageCode
                  select dpr.DosageImageCode).First();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }

      public static List<RecallProductRel> GetRecallRelation(int productId)
      {
         var result = new List<RecallProductRel>();

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               result = new List<RecallProductRel>(
                  from recall in cloudEntities.RecallProductRel
                  where recall.ProductID == productId
                  select recall);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }

      public static bool GetIsContractedMfg(int profileCode)
      {
         var result = false;

         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var isexist = (
                   from profile in entities.Profile
                   where profile.ProfileCode == profileCode && profile.ProfileTypeCode == "MFGR" && profile.Status == "Active" && profile.ProfileCode != 0
                   select profile.ProfileCode).Count();
               result = isexist > 0;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine if the user profile allows weight-based counting
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool GetIsWtCount(int profileCode)
      {
         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from profile in entities.Profile where profile.ProfileCode == profileCode && !profile.IsDeleted select profile.WeightCounting ?? false).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get location of the machine the user is at
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetMachineLocation(string machineName)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var loc = (from qsk in cloudEntities.Qosk where qsk.MachineID == machineName select qsk).FirstOrDefault();
            return loc?.LocationRoute.ToString() ?? "";
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine if this item already has an audit pending record
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool IsInAudit(Guid? itemGuid)
      {
         if (itemGuid == null)
         {
            return false;
         }

         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from audit in entities.AuditPending where audit.ItemGUID == itemGuid select audit.ItemGUID).Any();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update the item entries in ItemConditionRel
      /// </summary>
      /// <param name="newConditions"></param>
      /// <param name="itemGuid"></param>
      /// <param name="logOn"></param>
      /// <param name="appCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void UpdateItemConditions(List<string> newConditions, Guid itemGuid, string logOn, string appCode)
      {
         try
         {
            using (var entities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var oldConditions = (from conditionRel in entities.ItemConditionRel
                                    where conditionRel.ItemGUID == itemGuid
                                    && conditionRel.AppCode == appCode
                                    select conditionRel);

               if (oldConditions.Any())
               {
                  foreach (var itemConditionRel in oldConditions)
                  {
                     if (!newConditions.Contains(itemConditionRel.ConditionCode) && itemConditionRel.IsDeleted != true)
                     {
                        itemConditionRel.IsDeleted = true;
                        itemConditionRel.ModifiedBy = logOn;
                        itemConditionRel.Version++;
                        itemConditionRel.ModifiedDate = DateTime.UtcNow;
                     }
                     else if (newConditions.Contains(itemConditionRel.ConditionCode) && itemConditionRel.IsDeleted == true)
                     {
                        itemConditionRel.IsDeleted = false;
                        itemConditionRel.ModifiedBy = logOn;
                        itemConditionRel.Version++;
                        itemConditionRel.ModifiedDate = DateTime.UtcNow;
                     }
                  }
               }
               List<ItemConditionRel> conditionRelList = new List<ItemConditionRel>();
               foreach (var newItemConditionRel in newConditions)
               {
                  if (!oldConditions.Any(c => c.ConditionCode == newItemConditionRel))
                  {
                     conditionRelList.Add(new ItemConditionRel()
                     {
                        ConditionCode = newItemConditionRel,
                        ItemGUID = itemGuid,
                        Version = 1,
                        AppCode = appCode,
                        CreatedBy = logOn,
                        CreatedDate = DateTime.UtcNow
                     });
                  }
               }
               entities.ItemConditionRel.AddRange(conditionRelList);

               entities.SaveChanges();
            }
         }
         catch (Exception e)
         {
            throw e;
         }
      }
   }
}
