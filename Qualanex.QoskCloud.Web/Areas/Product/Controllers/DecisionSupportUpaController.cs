﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="DecisionSupportUpaController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   UpaController file collecting primarily Decision Support related functionality.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
using Qualanex.QoskCloud.Web.Areas.Sidebar;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Drawing;
   using System.IO;
   using System.Linq;
   using System.Net;
   using System.Web.Mvc;
   using System.Web.WebPages;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Controllers;

   using WasteCode = Models.WasteCode;

   //=============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "QLUser")]
   public partial class UpaController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult DecisionSupport()
      {
         return this.Index("PHARMA_DECISION");
      }

      ///****************************************************************************
      /// <summary>
      ///   Updates notifications to the selected statuses.
      /// </summary>
      /// <param name="productId">product Id not currently used</param>
      /// <param name="returnIds">comma separated list of notificationIds to be returned</param>
      /// <param name="dispenseIds">comma separated list of notificationIds marked as dispensed</param>
      /// <param name="removeIds">comma separated list of notificationIds marked as removed</param>
      /// <param name="transferIds">comma separated list of notificationIds marked as transferred</param>
      /// <param name="undoIds"></param>
      /// <returns>partial view of the notification tab</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateNotificationStatus(int productId, string returnIds, string dispenseIds, string removeIds, string transferIds, string undoIds)
      {
         var updateOptions = UpdateOptions.notifications;
         var returnList = ProductCommon.StringToLongList(returnIds);
         var dispenseList = ProductCommon.StringToLongList(dispenseIds);
         var removeList = ProductCommon.StringToLongList(removeIds);
         var transferList = ProductCommon.StringToLongList(transferIds);
         var undoList = ProductCommon.StringToLongList(undoIds);

         if (dispenseList.Any())
         {
            UpaDataAccess.UpdateNotifications(dispenseList, NotificationStatus.Dispensed);
         }
         if (removeList.Any())
         {
            UpaDataAccess.UpdateNotifications(removeList, NotificationStatus.DuplicateRemoved);
         }
         if (transferList.Any())
         {
            UpaDataAccess.UpdateNotifications(transferList, NotificationStatus.StoreTransfer);
         }
         if (undoList.Any())
         {
            UpaDataAccess.UpdateNotifications(undoList, NotificationStatus.Pending);
         }

         return this.UpdateMultiple(updateOptions, productId: productId, notificationIds: returnList);
      }
      
      ///****************************************************************************
      /// <summary>
      ///   Updates the waste tab for a product.
      /// </summary>
      /// <param name="productId">product id to look up the waste for</param>
      /// <param name="tab">the upa application appcode for the waste tab</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateWaste(long productId, string tab)
      {
         if (this.GetApplicationFromSession() == null)
         {
            return this.Json("");
         }

         var model = ProductCommon.PrepareWasteUpdate(productId);
         this.ViewBag.Tab = this.GetApplicationFromSession().childObj.Values.First(t => t.ObjectID == tab);

         return this.PartialView("_Tab", this.GetModelWithApplication(new UpaViewModel() { WasteResult = model }));
      }


      ///****************************************************************************
      /// <summary>
      ///   Ajax method allowing a notification to be created.
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="pageViewId">unique value used to find the UPA Application in the cache</param>
      /// <returns>Updated notification tab partial view</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveNotification(PolicyForm policyForm, string pageViewId = "")
      {
         var objReturnLoginUserEntity =
            (ReturnLoginUserEntity)
            GetUserLoginData(HttpContext);
         string logOn;
         int userProfileCode = -99;

         if (objReturnLoginUserEntity == null)
         {
            //policyForm.PolicyFinalize.LogOut = true;
            //return this.PartialView("_PrintLabel", policyForm);
            logOn = "Unknown";
         }
         else
         {
            logOn = objReturnLoginUserEntity.UserReturnData.UserName;

            if (objReturnLoginUserEntity.UserReturnData.ProfileCode != null)
               userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         if (UpaDataAccess.SaveNotification(policyForm, logOn, userProfileCode))
         {
            var model = UpaDataAccess.GetNotificationsAndRecalls(userProfileCode);
            return this.PartialView("_Tab", this.GetModelWithApplication(new UpaViewModel() { Notification = model }));
         }

         return new HttpStatusCodeResult(500, "An error occurred while saving the notification, please try again");
      }


      /// ****************************************************************************
      ///  <summary>
      ///    Marks the recall as acknowledged for the logged on user.
      ///  </summary>
      /// <param name="recallId">the id of the recall to mark acknowledged</param>
      /// <returns></returns>
      /// ****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult AcknowledgeRecall(long recallId)
      {
         var objReturnLoginUserEntity =
            (ReturnLoginUserEntity)
            GetUserLoginData(HttpContext);
         string logOn;
         int userProfileCode = -99;

         if (objReturnLoginUserEntity == null)
         {
            logOn = "Unknown";
            return new HttpStatusCodeResult(500, "An error occurred while saving the update, please try again");
         }
         else
         {
            logOn = objReturnLoginUserEntity.UserReturnData.UserName;

            if (objReturnLoginUserEntity.UserReturnData.ProfileCode != null)
               userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         UpaDataAccess.SaveRecallAcknowledgement(recallId, logOn, userProfileCode);

         return new HttpStatusCodeResult(200, "Update successful");
      }



   }
}