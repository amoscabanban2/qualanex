﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SearchUpaController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   UpaController file collecting primarily Search related functionality.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
using Qualanex.QoskCloud.Web.Areas.Sidebar;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Drawing;
   using System.IO;
   using System.Linq;
   using System.Net;
   using System.Web.Mvc;
   using System.Web.WebPages;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Controllers;

   using WasteCode = Models.WasteCode;

   //=============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "QLUser")]
   public partial class UpaController
   {
      ///****************************************************************************
      /// <summary>
      ///   Returns search results from a submission on the product detail form.
      /// </summary>
      /// <param name="productDetail">product detail model used as the search criteria</param>
      /// <param name="tab">the object Id of the Tab from the UPA Application</param>
      /// <param name="page">Optional parameter for pagination, not currently implemented</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateSearch(ProductDetailViewModel productDetail, string tab, int page = 1)
      {

         var model = this.GetModelWithApplication();

         if (model.UpaApplication == null)
         {
            return this.Json("");
         }

         //the primary input field of the form can accept various barcode formats as well as typed NDC and UPCs
         try
         {
            ProductCommon.ParseNdcInput(productDetail);
         }
         catch (Exception ex)
         {
            var search = new SearchResult()
            {
               ResultView = ResultView.List,
               Message = "ERROR: " + ex.Message + "<script> $(\"#tab-container\").easytabs(\"select\", \"#tabs-SearchResults\")</script>"
            };

            switch (productDetail.Request.ResultView)
            {
               case ResultView.Image:
                  model.SearchResults.ResultView = ResultView.Image;
                  break;

               case ResultView.List:
                  model.SearchResults.ResultView = ResultView.List;
                  break;

               default:
                  throw new ArgumentOutOfRangeException();
            }

            model.SearchResults = search;
            this.ViewBag.Tab = this.GetApplicationFromSession().childObj.Values.First(t => t.ObjectID == tab);
            return this.PartialView("_Tab", model);
         }

         if (model.UpaApplication.HasObjectId("resultViewImg") && !model.UpaApplication.HasObjectId("resultViewList")) // default to image results if no other result function is present
         {
            productDetail.Request.ResultView = ResultView.Image;
         }
         else if (!model.UpaApplication.HasObjectId("resultViewImg")) // default to list results if no result functions are present
         {
            productDetail.Request.ResultView = ResultView.List;
         }

         if (productDetail.Request.ResultView != ResultView.Image && model.UpaApplication.HasObjectId("resultViewImg") && !productDetail.Request.Imprint.IsDefault())
         {
            var imprint = productDetail.Request.Imprint;

            productDetail.Request.Imprint = new ImprintRequest();

            if (productDetail.Request.IsDefault())
            {
               productDetail.Request.ResultView = ResultView.Image;
            }

            productDetail.Request.Imprint = imprint;
         }

         switch (productDetail.Request.ResultView)
         {
            case ResultView.Image:
               model.SearchResults.ResultView = ResultView.Image;
               model.SearchResults.ImageSearchResults = this.PrepareImageSearch(1, productDetail.Request, string.Empty);
               break;

            case ResultView.List:
               model.SearchResults.ResultView = ResultView.List;
               model.SearchResults = ProductCommon.PrepareSearchUpdate(productDetail);
               break;

            default:
               throw new ArgumentOutOfRangeException();
         }

         this.ViewBag.Tab = this.GetApplicationFromSession().childObj.Values.First(t => t.ObjectID == tab);

         //return a partial view using the segments from the current UPA Application
         return this.PartialView("_Tab", model);
      }

      ///****************************************************************************
      /// <summary>
      ///   AJAX Method for getting a page of image results after an initial search is completed
      /// </summary>
      /// <param name="page">Page of image results</param>
      /// <param name="request">SearchRequest object used as the search criteria</param>
      /// <param name="sortOrder">Designates the field to sort by, not currently used</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateImageSearch(int page, SearchRequest request, string sortOrder)
      {
         var model = this.GetModelWithApplication();

         if (model.UpaApplication == null)
         {
            return this.Json("");
         }

         this.ViewBag.SearchDetail = request;

         model.SearchResults.ResultView = ResultView.Image;
         model.SearchResults.ImageSearchResults = this.PrepareImageSearch(page, request, sortOrder);

         return this.PartialView("_SearchImageResult", model);
      }

      ///****************************************************************************
      /// <summary>
      ///   Builds the model for a page of product search image results.
      /// </summary>
      /// <param name="page">indicates the page of search results</param>
      /// <param name="request">SearchRequest object used as the search criteria</param>
      /// <param name="sortOrder">Designates the field to sort by, not currently used</param>
      /// <returns></returns>
      ///****************************************************************************
      private IPagedList<ProductResult> PrepareImageSearch(int page, SearchRequest request, string sortOrder)
      {
         this.ViewBag.SearchDetail = request;

         var model = UpaDataAccess.GetImageSearch(request, page, 10);

         if (request.IsDefault())
         {
            this.ViewBag.Message = "Perform a search to see results";
         }
         else if (model == null || !model.Any())
         {
            this.ViewBag.Message = "The requested search has no results<script> $(\"#tab-container\").easytabs(\"select\", \"#tabs-SearchResults\")</script>";
         }
         else if (model.Any(m => !string.IsNullOrEmpty(m.Message)))
         {
            //we set model to null so that we do not confuse the HTML thinking that there are rows existing in the object
            this.ViewBag.Message = model.First(m => !string.IsNullOrEmpty(m.Message)).Message;
            model = null;
         }
         else
         {
            this.ViewBag.Message = string.Empty;
         }

         return model;
      }
   }
}