﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UpaController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   This is a controller to be used in all UPA applications by default,
///   
/// </summary>
/// <remarks>
///   Because it is used in several applications covering many domains, the UpaController class 
///   has been split into several files in an attempt to keep domain specific methods together
///   in smaller files without complicating functionality.
/// 
///   UPA tabs and segments are rendered through index.cshtml, tab.cshtml and the respective segment views
///   UPA functions are used in a variety of ways, usually by calling "upaApplication.HasObjectId("[function objectId")"
///   to check its availability in an application. Some segments may use functions to reference additional partial 
///   views (see _SegmentContainerType.cshtml) 
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
using Qualanex.QoskCloud.Web.Areas.Sidebar;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Drawing;
   using System.IO;
   using System.Linq;
   using System.Net;
    using System.Web;
    using System.Web.Mvc;
   using System.Web.WebPages;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Controllers;

   using WasteCode = Models.WasteCode;

   //=============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   /// Users must be logged in and have an entry matching the "QLUser" role in 
   /// the UserRole table. 
   /// This decoration should probably be removed or replaced once UPA 
   /// role and security is implemented for access control.
   [Authorize(Roles = "QLUser")]
   public partial class UpaController : Controller, IQoskController, IUserProfileController
   {
      private UPAApplication _upaApplication;

      ///****************************************************************************
      /// <summary>
      ///   Loads the default page given a UPA app code
      /// </summary>
      /// <param name="app">The appcode matching the Code column in the AppDict Column</param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Index(string app)
      {
         //user is required to get the UPA mode, if it's null, this user's session has expired, so logout
         var objReturnLoginUserEntity = GetUserLoginData(HttpContext);

         if (objReturnLoginUserEntity == null)
         {
            return this.Redirect("LogOut");
         }

         int userProfileCode = -99;

         if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
         {
            userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         // The upaApplication object retreived from the QoskLibrary contains the definitions for what tabs,
         // segments, and functions are included in this application
         this._upaApplication = UpaDataAccess.GetUpaModel(app, objReturnLoginUserEntity.UserReturnData.UserID, userProfileCode);

         //pageViewId is used to differentiate the cached version of the application between various users
         //since planned updates to UPA will include different functions/segments for an app based on the User
         //The pageViewId is inserted into the layout and included in every json request using "getAntiforgeryToken()"
         var pageViewId = Guid.NewGuid();

         //The application is cached since depending on the complexity, the initial call can take a significant amount of itme
         QoskSessionHelper.SetSessionData(Constants.UpaApplicationKey + pageViewId, this._upaApplication);
         this.ViewBag.pageViewId = pageViewId;

         //set up the model
         SearchRequest request = new SearchRequest { Product = new ProductDetail() };

         var model = new UpaViewModel
         {
            WasteResult = new WasteResult(),
            UpaApplication = this._upaApplication,
            SearchResults = new SearchResult()
            {
               Message = "Perform a search to see results",
               Products = new List<ProductResult>()

            }
         };
         model.ProductDetail = this.PrepareDetailUpdate();
         //notifications can be pulled by user without any input, so if included, get 
         //the data for the initial page load
         if (this._upaApplication.HasObjectId("_SegmentReminders") ||
             this._upaApplication.HasObjectId("_SegmentRecalls"))
         {
            model.Notification = UpaDataAccess.GetNotificationsAndRecalls(userProfileCode);
         }
         //Upa applications with a binventory segment need the proper models instantiated
         if (this._upaApplication.HasObjectId("_SegmentBinventory"))
         {
            model.Binventory = new Web.ViewModels.BinventoryViewModel
            {
               Options = new Web.ViewModels.BinventoryOptions()
               {
                  RowProperty = new BinventoryDataAccess(),
               },
               Rows = new List<BinventoryDataAccess>()
            };
         }
         var machineName = SidebarCommon.GetMachineName(this.Request);
         model.CurrentInbound = SidebarCommon.GetCurrentInboundTote(machineName, model.UpaApplication.Code);
         //Much of the heavy lifting in putting a UPA application together is done in
         //"Index.cshtml" and "_Tab.cshtml"
         return this.View("Index", model);
      }

      ///****************************************************************************
      /// <summary>
      ///   This method is used to update multiple tabs simultaneously
      /// </summary>
      /// <param name="options">An enum specifying which tabs to update</param>
      /// <param name="ndc"></param>
      /// <param name="productId"></param>
      /// <param name="expDate">expiration date</param>
      /// <param name="impDate">imprint group date</param>
      /// <param name="lotNumber"></param>
      /// <param name="productDetail"></param>
      /// <param name="notificationIds">selected notifications</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateMultiple(UpdateOptions options, long ndc = 0, int productId = 0, DateTime? expDate = null, DateTime? impDate = null, string lotNumber = null, ProductDetailViewModel productDetail = null, List<long> notificationIds = null)
      {
         var model = this.GetModelWithApplication();
         var machineName = SidebarCommon.GetMachineName(this.Request);

         if (model.UpaApplication == null)
         {
            return this.Json("");
         }

         if (options.HasFlag(UpdateOptions.policy))
         {
            model.PolicyForm = PreparePolicyUpdate(productId, expDate, lotNumber, machineName: machineName, notificationIds: notificationIds);
         }

         if (options.HasFlag(UpdateOptions.productDetail))
         {
            model.ProductDetail = this.PrepareDetailUpdate(new long(), productId, impDate, expDate, lotNumber, notificationIds: notificationIds);
         }

         if (options.HasFlag(UpdateOptions.waste))
         {
            model.WasteResult = ProductCommon.PrepareWasteUpdate(ndc);
         }

         if (options.HasFlag(UpdateOptions.search))
         {
            model.SearchResults = ProductCommon.PrepareSearchUpdate(productDetail);
         }

         if (options.HasFlag(UpdateOptions.notifications) &&
          (this._upaApplication.HasObjectId("_SegmentReminders") || this._upaApplication.HasObjectId("_SegmentRecalls")))
         {
            var objReturnLoginUserEntity = GetUserLoginData(HttpContext);
            int userProfileCode = -99;

            if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
            {
               userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
            }

            model.Notification = UpaDataAccess.GetNotificationsAndRecalls(userProfileCode);

            if (notificationIds != null)
            {
               model.Notification.ReturnStartedIds = notificationIds;
            }
         }

         model.CurrentInbound = SidebarCommon.GetCurrentInboundTote(machineName, model.UpaApplication.Code);
         return this.View("Index", model);
      }

      /// ****************************************************************************
      ///  <summary>
      ///    Allows AJAX calls to update the product detail 
      ///    form at the top of UPA applications.
      ///  </summary>
      ///  <param name="rollupNdc">NDC to be shown. This is required</param>
      ///  <param name="rollupProductId"></param>
      ///  <param name="impDate">A date for an imprint group</param>
      ///  <param name="expDate">Expiration date</param>
      ///  <param name="lotNumber"></param>
      ///  <param name="notificationIds">Notification IDs tied to the selected product. Required to show images from previous induction</param>
      /// <returns></returns>
      /// ****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateDetail(long? rollupNdc = null, int? rollupProductId = null, DateTime? impDate = null, DateTime? expDate = null, string lotNumber = null, string notificationIds = null)
      {
         if (this.GetApplicationFromSession() == null)
         {
            return this.Json("");
         }

         var notificationList = ProductCommon.StringToLongList(notificationIds);

         var productDetail = this.PrepareDetailUpdate(rollupNdc, rollupProductId, impDate, expDate, lotNumber, notificationList);

         return this.PartialView("_ProductDetail", this.GetModelWithApplication(new UpaViewModel() { ProductDetail = productDetail }));
      }

      /// ****************************************************************************
      ///  <summary>
      ///    This method has the logic for creating the product detail model to be used
      ///    when serving a view.
      ///  </summary>
      ///  <param name="ndc"></param>
      ///  <param name="productId"></param>
      ///  <param name="impDate">imprint group date</param>
      ///  <param name="expDate">expiration date</param>
      ///  <param name="lotNumber"></param>
      /// <param name="notificationIds">Notification IDs tied to the selected product. Required to show images from previous induction</param>
      /// <returns></returns>
      /// ****************************************************************************
      private ProductDetailViewModel PrepareDetailUpdate(long? ndc = null, int? productId = null, DateTime? impDate = null, DateTime? expDate = null, string lotNumber = null, List<long> notificationIds = null)
      {
         //get the details to populate the form if a product has been selected
         var result = UpaDataAccess.GetSearch(new SearchRequest
         {
            AppCode = this._upaApplication.Code,
            Product = new ProductDetail
            {
               Ndc = ndc,
               ProductId = productId ?? 0
            },
            Lot = lotNumber,
            ExpirationDate = expDate
         });


         var upaApplication = this.GetApplicationFromSession();

         //Default result view is a list, unless only the image result function is available in this application
         var defaultResultView = ResultView.List;
         if (!upaApplication.HasObjectId("resultViewList") && upaApplication.HasObjectId("resultViewImg"))
         {
            defaultResultView = ResultView.Image;
         }

         if (!result.Any())
         {
            var model = new ProductDetailViewModel { Request = new SearchRequest { ResultView = defaultResultView } };
            ProductCommon.PopulateDropDownLists(model);
            return model;
         }

         var product = result.Products.First();
         ImprintGroup imprintGroup;
         var images = new List<ImageResult>();
         var imprint = new ImprintRequest();

         //if the product is selected from a return notification, get any images captured from previous induction to include in the carousel
         if (notificationIds != null && notificationIds.Any())
         {
            var notifications = UpaDataAccess.GetNotifications(notificationIds);
            var imageList = AzureCommonUtility.DownloadImagesVideosFromAzure(notifications.First().GroupGuid.ToString()).OrderBy(i => i.ContentSasURI);
            images.AddRange(imageList.Select(image => new ImageResult
            {
               Url = image.ContentSasURI,
               AltText = "Previously Captured Image"
            }));
         }

         //select the imprint information to show if multiple groups are available
         if (product.ImprintGroups.Any())
         {
            //if no imprint date is specified, just use the latest imprints
            if (impDate == null)
            {
               imprintGroup = product.ImprintGroups.OrderByDescending(r => r.EffectiveStartDate).First();

               foreach (var imp in product.ImprintGroups)
               {
                  images.AddRange(imp.ImageResults);
               }
            }
            else //use the imprints where the effective dates include the imprint date
            {
               imprintGroup =
                  product.ImprintGroups.First(
                     r => r.EffectiveStartDate <= impDate && r.EffectiveEndDate > impDate)
                  ?? product.ImprintGroups.OrderByDescending(r => r.EffectiveStartDate).First();

               images = imprintGroup.ImageResults;
            }

            imprint = new ImprintRequest
            {
               Score = imprintGroup.ImprintResults.FirstOrDefault(r => r.ImprintCode.Trim() == "score")?.Description.Trim(),
               Color = imprintGroup.ImprintResults.FirstOrDefault(r => r.ImprintCode.Trim() == "color")?.BasicDescription.Trim(),
               Shape = imprintGroup.ImprintResults.FirstOrDefault(r => r.ImprintCode.Trim() == "shape")?.BasicDescription.Trim(),
               Obverse = imprintGroup.ImprintResults.FirstOrDefault(r => r.ImprintCode.Trim() == "Imprint")?.Description,
               Reverse = imprintGroup.ImprintResults.FirstOrDefault(r => r.ImprintCode.Trim() == "Imprint" && r.Description != imprintGroup.ImprintResults.FirstOrDefault(r2 => r2.ImprintCode.Trim() == "Imprint")?.Description)?.Description,
               ImageUrl = imprintGroup.ImageResults.FirstOrDefault()?.Url ?? Constants.Default_Product_Image_Url,
               Images = images.Distinct().ToList()
            };
         }

         var productDetail = new ProductDetailViewModel
         {
            Request = new SearchRequest
            {
               ResultView = defaultResultView,
               Product = product.ProductDetail,
               Lot = product.ProductDetail.Lot.LotNumber,
               ExpirationDate = product.ProductDetail.Lot.ExpirationDate,
               Imprint = imprint
            }
         };

         if (!product.ProductDetail.NDCUPCWithDashes.IsNullOrWhiteSpace())
         {
            productDetail.NDCUPCWithDashes = product.ProductDetail.NDCUPCWithDashes;
         }
         else if (product.ProductDetail.Ndc != null && product.ProductDetail.Ndc.Value != 0)
         {
            productDetail.NDCUPCWithDashes = product.ProductDetail.Ndc.ToString();
         }
         else if (product.ProductDetail.Upc != null && product.ProductDetail.Upc.Value != 0)
         {
            productDetail.NDCUPCWithDashes = product.ProductDetail.Upc.ToString();
         }

         ProductCommon.PopulateDropDownLists(productDetail);

         return productDetail;
      }

      ///****************************************************************************
      /// <summary>
      ///   Helper method to generate an empty model, or update a model with the UpaApplication.
      /// </summary>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      private UpaViewModel GetModelWithApplication(UpaViewModel model = null)
      {
         if (model == null)
         {
            model = new UpaViewModel();
         }

         model.UpaApplication = this.GetApplicationFromSession();
         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Helper method to retrieve the model from the session if it is not already
      ///   loaded into the instance variable.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private UPAApplication GetApplicationFromSession()
      {
         return this._upaApplication ?? (this._upaApplication = (UPAApplication)QoskSessionHelper.GetSessionData(Constants.UpaApplicationKey + this.Request.Params["pageViewId"]));
      }

      ///****************************************************************************
      /// <summary>
      ///   Used to update the profile, currently changes the state of the account.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

      /// <summary>
      /// Returns the login user info, refreshing the cache if necessary
      /// </summary>
      /// <returns></returns>
      public static ReturnLoginUserEntity GetUserLoginData(HttpContextBase httpContext)
      {
         var objReturnLoginUserEntity =
            (ReturnLoginUserEntity)
            QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
         if (objReturnLoginUserEntity == null)
         {
            UserController.SetUserCache(httpContext.User.Identity.Name);
            objReturnLoginUserEntity =
               (ReturnLoginUserEntity)
               QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
         }
         return objReturnLoginUserEntity;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get machine Location.
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetMachineLocation(string machineName)
      {
         machineName = string.IsNullOrWhiteSpace(machineName) ? SidebarCommon.GetMachineName(this.Request) : machineName;
         var strCheck = UpaDataAccess.GetMachineLocation(machineName);

         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(strCheck),
            Error = string.IsNullOrWhiteSpace(strCheck),
            Location = strCheck
         });
      }
   }
}
