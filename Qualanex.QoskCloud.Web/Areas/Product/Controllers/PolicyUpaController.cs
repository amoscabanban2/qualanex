﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyUpaController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///    UpaController file collecting primarily Policy related functionality.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
using Qualanex.QoskCloud.Web.Areas.Sidebar;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Drawing;
   using System.IO;
   using System.Linq;
   using System.Net;
   using System.Web.Mvc;
   using System.Web.WebPages;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Controllers;

   using WasteCode = Models.WasteCode;

   //=============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "QLUser")]
   public partial class UpaController
   {

      ///****************************************************************************
      /// <summary>
      ///   Updates the policy tab primarily for when the selected product is
      ///   changed.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="tab"></param>
      /// <param name="expDate"></param>
      /// <param name="lotNumber"></param>
      /// <param name="caseQty"></param>
      /// <param name="serial"></param>
      /// <param name="scannedBarcode"></param>
      /// <param name="auditItemGuid"></param>
      /// <param name="notificationIds"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdatePolicy(int productId, string tab, DateTime? expDate = null, string lotNumber = null, int? caseQty = null, string serial = null, string scannedBarcode = null, Guid? auditItemGuid = null, string notificationIds = "")
      {
         if (this.GetApplicationFromSession() == null)
         {
            return this.Json("");
         }

         var machineName = SidebarCommon.GetMachineName(this.Request);
         var model = PreparePolicyUpdate(productId, expDate, lotNumber, caseQty, serial, scannedBarcode, machineName, auditItemGuid, notificationIds: ProductCommon.StringToLongList(notificationIds));
         this.ViewBag.Tab = this.GetApplicationFromSession().childObj.Values.First(t => t.ObjectID == tab);

         return this.PartialView("_Tab", this.GetModelWithApplication(new UpaViewModel() { PolicyForm = model }));
      }

      ///****************************************************************************
      /// <summary>
      ///   Prepares the model for a policy form update.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="expDate"></param>
      /// <param name="lotNumber"></param>
      /// <param name="caseQty"></param>
      /// <param name="serialNumber"></param>
      /// <param name="scannedBarcode"></param>
      /// <param name="machineName"></param>
      /// <param name="auditItemGuid"></param>
      /// <param name="notificationIds"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static PolicyForm PreparePolicyUpdate(int productId, DateTime? expDate = null, string lotNumber = null, int? caseQty = null, string serialNumber = null,
         string scannedBarcode = null, string machineName = null, Guid? auditItemGuid = null, List<long> notificationIds = null)
      {
         //Check if a Guid was scanned, this indicates an item is being re-inducted
         Guid itemGuid;
         Item item = null;
         ContainerItemRelation tote = null;
         ContainerInstance noGuidTote = null;
         var parseGuidString = QoskSessionHelper.GetReferringPage().Split('=')[1] == "WAREHS_AUDIT" ? auditItemGuid.ToString() : scannedBarcode;

         if (Guid.TryParseExact(parseGuidString, "D", out itemGuid))
         {
            //if this is a re-induction, get the original item record and the tote it was inducted from 
            //else get the info for the currently attached inbound tote
            item = UpaDataAccess.GetItem(itemGuid);
            tote = SidebarCommon.GetContainerItemRel(itemGuid).FirstOrDefault(c => Constants.ToteTypes.Contains(c.Container.Type));
         }
         else
         {
            noGuidTote = SidebarCommon.GetCurrentInboundTote(machineName);
         }

         //If the item was selected from a notification, find the notification records, selected lot, and expiration
         var notifications = new List<ProductNotification>();

         if (notificationIds != null && notificationIds.Any())
         {
            notifications = UpaDataAccess.GetNotifications(notificationIds);
            lotNumber = notifications.First().Lot.LotNumber;
            expDate = notifications.First().Lot.ExpirationDate;
         }

         var productDetail = UpaDataAccess.GetProduct(productId).ProductDetail;

         var model = new PolicyForm()
         {
            ProductId = productId,
            RollupProductId = productDetail.RollupProductId,
            Product = productDetail,
            SelectedLot = new LotResults() { LotNumber = lotNumber, ExpirationDate = expDate },
            SerialNumber = serialNumber,
            ScannedBarCode = scannedBarcode,
            NotificationIds = notificationIds ?? new List<long>(),
            Quantity = notifications.Count,
            ContainerType = notifications.Any()
               ? notifications.FirstOrDefault()?.PackageType.ToString() + "|" + notifications.FirstOrDefault()?.ContainerType.ToString()
               : caseQty.HasValue && caseQty > 0
                  ? "Original|Case"
                  : productDetail.InnerContainerTypeName,
            ContainerCondition = notifications.FirstOrDefault()?.ContainerCondition.ToString(),
            SelectedCaseSize = notifications.FirstOrDefault()?.CaseSize ?? (caseQty.HasValue && caseQty > 0 ? caseQty : null),
            ItemGuid = item?.ItemGUID,
            InboundContainer = new ContainerInstance()
            {
               InstanceId = tote?.ContainerRel?.WrhsInstID ?? noGuidTote?.InstanceId,
               ContainerId = tote?.ContainerRel?.WrhsContrID ?? noGuidTote?.ContainerId
            }
         };

         if (model.SelectedLot.ExpirationDate.HasValue)
         {
            model.SelectedExpMonth = model.SelectedLot.ExpirationDate.Value.Month.ToString();
            model.SelectedExpYear = model.SelectedLot.ExpirationDate.Value.Year.ToString();
         }

         var userState = ProductCommon.GetUserState();
         model.HazardClass = UpaDataAccess.GetHazardClass(model.ProductId, userState);

         //get waste stream profile ID of the product (either by searching for the product's Waste Code or by the processed item's waste stream profile ID - if reprocess)
         var wasteStreams = UpaDataAccess.GetWasteStreams(productDetail.NDCInnerPack ? (long)model.RollupProductId : model.ProductId, userState).FirstOrDefault()?.WasteStreamCode;
         var dbModel = new DBModel("name=QoskCloud");
         var wasteStreamProfileId = !string.IsNullOrEmpty(wasteStreams)
            ? (from wspr in dbModel.WasteStreamProfileRel where wspr.WasteStreamCode == wasteStreams select wspr.WasteStreamProfileID).SingleOrDefault()
            : null;

         model.ItemWasteStreamProfileId = item?.WasteStreamProfileID ?? wasteStreamProfileId;

         model.IsRecallPossible = ProductCommon.IsAnyProductRecall(model.ProductId);
         model.PolicyResult = UpaDataAccess.GetPolicyResult(model);
         PopulatePolicyDropDownLists(model);
         model.IsContractedMfg = ProductCommon.IsContractedMfg(model.Product.ProfileCode);
         var sessionProfileCode = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Utility.Constants.ManageLayoutConfig_UserDataWithLink);
         model.IsWeightCounting = model.PolicyResult.PolicyModel != null && ProductCommon.IsWtCount((int)sessionProfileCode.UserReturnData.ProfileCode);
         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Updates the policy results (disposition or designation) given selections made within the policy tab.
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="pageViewId">unique value used to find the UPA Application in the cache</param>
      /// <returns>A Partial view with the designation or disposition result</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdatePolicyResult(PolicyForm policyForm, string pageViewId = "")
      {
         var application = this.GetApplicationFromSession();
         if (application == null)
         {
            return this.Json("");
         }

         SetExpirationFromForm(policyForm);

         //Persist condition-related information and strip leading pipe if a condition has been set 
         policyForm.ContainerCondition = policyForm.ContainerCondition != null && policyForm.ContainerCondition.StartsWith("|")
            ? policyForm.ContainerCondition?.Substring(1)
            : policyForm.ContainerCondition;

         //if this application has a bin designation, return the bin designation result,
         //otherwise evaluate the credit status of the policy to return a disposition result
         if (application.HasObjectId("_SegmentBinDesignation"))
         {
            return this.UpdateInductionResult(policyForm);
         }
         policyForm.PolicyResult = UpaDataAccess.GetPolicyResult(policyForm, application.Code);

         //if (policyForm.HazardClass > 0)
         //{
         //   policyForm.PolicyResult.PolicyStatus = "This product has been classified as hazardous waste and cannot be returned for credit.";
         //}
         return this.PartialView("_PolicyResult", this.GetModelWithApplication(new UpaViewModel() { PolicyForm = policyForm }));
      }


      /// <summary>
      /// Saves an item to the database
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="auditItem">Saves an AuditPending record in addition to the Item</param>
      /// <returns></returns>
      [ValidateAntiForgeryToken]
      public bool SaveItem(PolicyForm policyForm, bool auditItem = false)
      {
         try
         {
            //Persist condition-related information and strip leading pipe if a condition has been set 
            policyForm.ContainerCondition = policyForm.ContainerCondition != null && policyForm.ContainerCondition.StartsWith("|")
               ? policyForm.ContainerCondition?.Substring(1)
               : policyForm.ContainerCondition;

            this.PrepareItemDetails(policyForm, saveItem: true, saveItemAudit: auditItem);
         }
         catch (Exception ex)
         {
            return false;
         }

         return true;
      }



      ///****************************************************************************
      /// <summary>
      ///   Updates the policy finalize segments once a policy designation is selected or acknowledged.
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="policyReturnNotifications">notifications that were selected to begin this induction</param>
      /// <param name="pageViewId">unique value used to find the UPA Application in the cache</param>
      /// <returns>The _policyFinalize view</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdatePolicyFinalize(PolicyForm policyForm, string policyReturnNotifications = "", string pageViewId = "")
      {
         var application = this.GetApplicationFromSession();
         if (application == null)
         {
            return this.Json("");
         }

         policyForm.NotificationIds = ProductCommon.StringToLongList(policyReturnNotifications);

         SetExpirationFromForm(policyForm);

         //Persist condition-related information and strip leading pipe if a condition has been set 
         policyForm.ContainerCondition = policyForm.ContainerCondition != null && policyForm.ContainerCondition.StartsWith("|")
            ? policyForm.ContainerCondition?.Substring(1)
            : policyForm.ContainerCondition;

         var policyResult = UpaDataAccess.GetPolicyResult(policyForm, application.Code);

         if (policyForm.PolicyResult.Disposition == "Stock" && policyForm.SelectedLot.ExpirationDate.HasValue && (policyResult.CreditDisposition == CreditDisposition.QUARANTINEHOLD
                                                                                                                  || policyResult.CreditDisposition == CreditDisposition.DESTRUCTSTOCKRTN
                                                                                                                  || policyResult.CreditDisposition == CreditDisposition.RTN4AGING
                                                                                                                  || policyResult.CreditDisposition == CreditDisposition.CREDITSTOCKRTN))
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)GetUserLoginData(HttpContext);
            string logOn;
            int userProfileCode = -99;

            if (objReturnLoginUserEntity == null)
            {
               //policyForm.PolicyFinalize.LogOut = true;
               //return this.PartialView("_PrintLabel", policyForm);
               logOn = "Unknown";
            }
            else
            {
               logOn = objReturnLoginUserEntity.UserReturnData.UserName;

               if (objReturnLoginUserEntity.UserReturnData.ProfileCode != null)
               {
                  userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
               }
            }

            policyForm.PolicyResult.PolicyModel = policyResult.PolicyModel;
            this.PrepareItemDetails(policyForm);

            //if this UPA Application has the save notification function, generate the notification for later user
            if (this.GetApplicationFromSession().HasObjectId("saveNotification"))
            {
               UpaDataAccess.SaveNotification(policyForm, logOn, userProfileCode);
               policyForm.PolicyFinalize.Message = "You will be notified when this item can be returned for credit";
            }
            else //can't return, can't save a notification, nothing to do here
            {
               policyForm.PolicyFinalize.Message = "No further action is required for this product";
            }
         }
         else if (policyForm.PolicyResult.Disposition == "Stock")
         {
            policyForm.PolicyFinalize.Message = "No further action is required for this product";
         }
         else if (policyForm.PolicyResult.Disposition == "Credit" || policyForm.PolicyResult.Disposition == "Disposal" || policyForm.PolicyResult.Disposition == "Aging")
         {
            this.PrepareItemDetails(policyForm);
         }

         //Update notifications to reflect that an induction has been completed
         if (policyForm.NotificationIds.Any())
         {
            UpaDataAccess.UpdateNotifications(policyForm.NotificationIds, NotificationStatus.Returned);
         }

         return this.PartialView("_PolicyFinalize", this.GetModelWithApplication(new UpaViewModel() { PolicyForm = policyForm }));
      }

      ///****************************************************************************
      /// <summary>
      ///   Generates a _PrintLabel partial view that can communicate with a local dymo printers
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult PrintLabel(PolicyForm policyForm)
      {
         policyForm.PolicyResult = UpaDataAccess.GetPolicyResult(policyForm, this.GetApplicationFromSession().Code);
         this.PrepareItemDetails(policyForm);

         return this.PartialView("_PrintLabel", this.GetModelWithApplication(new UpaViewModel() { PolicyForm = policyForm }));
      }

      /// ****************************************************************************
      ///  <summary>
      ///    Saves or updates item records during induction and bin designation
      ///  </summary>
      ///  <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="saveItem">Optional, defaults to true. If false, new item records will not be created</param>
      /// <param name="saveItemAudit">optional, defaults to false, if true, an AuditPending record will be created</param>
      /// ****************************************************************************
      private void PrepareItemDetails(PolicyForm policyForm, bool saveItem = true, bool saveItemAudit = false)
      {
         var application = this.GetApplicationFromSession();
         string stateCode;
         decimal itemQuantity = policyForm.Quantity;

         //If item images were captured during processing, gather references to the blob storage 
         //so they can be moved to an item specific location
         CloudBlockBlob sideImageBlob = null;
         CloudBlockBlob topImageBlob = null;
         CloudBlockBlob contentImageBlob = null;

         if (!policyForm.SideImageUri.IsNullOrWhiteSpace())
         {
            sideImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.SideImageUri);
         }

         if (!policyForm.TopImageUri.IsNullOrWhiteSpace())
         {
            topImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.TopImageUri);
         }

         if (!policyForm.ContentImageUri.IsNullOrWhiteSpace())
         {
            contentImageBlob = WebcamController.GetBlobFromWebcamUri(policyForm.ContentImageUri);
         }

         SetExpirationFromForm(policyForm);

         var packageContainer = new string[] { PackageType.Unknown.ToString(), ContainerType.Unknown.ToString() };

         if (policyForm?.ContainerType != null)
         {
            packageContainer = policyForm.ContainerType.Split('|');
         }

         PackageType packageType = PackageType.Unknown;
         ContainerType containerType = ContainerType.Unknown;
         Enum.TryParse(packageContainer[0], out packageType);
         Enum.TryParse(packageContainer[1], out containerType);

         ProductResult product = UpaDataAccess.GetSearch(
            new SearchRequest()
            {
               AppCode = application.Code,
               Product = new ProductDetail() { ProductId = policyForm.ProductId },
               ExpirationDate = policyForm.SelectedLot.ExpirationDate,
               Lot = policyForm.SelectedLot.LotNumber
            }).FirstOrDefault();

         //Find the correct stateCode  corresponding to the container type
         switch (containerType)
         {
            case ContainerType.Case:
               stateCode = "C";
               itemQuantity = 1;
               break;

            case ContainerType.Sealed:
               stateCode = "S";
               itemQuantity = 1;
               break;

            case ContainerType.Loose:
            case ContainerType.Open:
               stateCode = "O";
               break;

            default:
               stateCode = "UNKN";
               break;
         }

         if (policyForm.IsForeignContainer)
         {
            policyForm.ContainerCondition = string.IsNullOrWhiteSpace(policyForm.ContainerCondition) ? "ForeignContainer" : $"{policyForm.ContainerCondition}|ForeignContainer";
         }

         if (itemQuantity == 0)
         {
            policyForm.ContainerCondition = string.IsNullOrWhiteSpace(policyForm.ContainerCondition) ? "Empty" : $"{policyForm.ContainerCondition}|Empty";
         }

         if (policyForm.ContainerCondition.IsNullOrWhiteSpace())
         {
            policyForm.ContainerCondition = "Irrelevant";
         }

         //Generate a guid for each item record being created, defaulting to an existing guid if available
         var itemGuids = new List<Barcode>() { new Barcode() { ItemGuid = policyForm.ItemGuid ?? Guid.NewGuid() } };

         //Multiple quantity of sealed/case items will be saved into corresponding multiple item records
         if (containerType == ContainerType.Sealed || containerType == ContainerType.Case)
         {
            for (var i = 1; i < policyForm.Quantity; i++)
            {
               itemGuids.Add(new Barcode() { ItemGuid = Guid.NewGuid() });
            }
         }

         List<WasteCode> wasteCodes = UpaDataAccess.GetMinimalWasteCodes(
            policyForm.Product.NDCInnerPack ? (long)policyForm.RollupProductId : policyForm.ProductId, ProductCommon.GetUserState()).OrderBy(code => code.SeverityOrder).ToList();

         var objReturnLoginUserEntity = (ReturnLoginUserEntity)GetUserLoginData(HttpContext);

         if (objReturnLoginUserEntity == null)
         {
            policyForm.PolicyFinalize.LogOut = true;
            return;
         }

         //get the user's information and apply it towards the item being saved
         var logOn = objReturnLoginUserEntity.UserReturnData.UserName;
         int userProfileCode = -99;

         if (objReturnLoginUserEntity.UserReturnData.ProfileCode != null)
         {
            userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         var qoskInfo = UpaDataAccess.GetQosk(SidebarCommon.GetMachineName(this.Request));

         foreach (var itemGuid in itemGuids)
         {
            if (saveItem)
            {
               policyForm.PolicyResult.PolicyModel = UpaDataAccess.GetPolicyResult(policyForm, application.Code).PolicyModel;

               //in audit, we actually update the item table with the new information on the second call
               //by that time, we lose ReturnInformation, so we have to populate it here again
               if (saveItemAudit)
               {
                  policyForm.ReturnInformation = SidebarCommon.GetRaForItem(itemGuid.ItemGuid);
               }

               var guid = UpaDataAccess.SaveItem(itemGuid.ItemGuid, Convert.ToByte(product.ProductDetail.CtrlNumber), stateCode, itemQuantity, logOn, policyForm, wasteCodes, saveItemAudit, application.Code, userProfileCode, qoskInfo?.QoskID ?? 1);

               //throw exception everywhere but DSS (so that it doesn't return a bin designation without saving the item)
               //for DSS, allow each item to be saved
               if (guid == Guid.Empty && application.Code != "PHARMA_DECISION")
               {
                  throw new Exception();
               }
            }

            //Copy the imageBlobs to item specific directory
            if (sideImageBlob != null)
            {
               var appendFileName = "_SideImage.jpg";
               ProductCommon.CopyBlobToItemContainer(itemGuid.ItemGuid, appendFileName, sideImageBlob, Constants.AzureAccount_Image, true);
            }

            if (topImageBlob != null)
            {
               var appendFileName = "_TopImage.jpg";
               ProductCommon.CopyBlobToItemContainer(itemGuid.ItemGuid, appendFileName, topImageBlob, Constants.AzureAccount_Image, true);
            }

            if (contentImageBlob != null)
            {
               var appendFileName = "_ContentImage.jpg";
               ProductCommon.CopyBlobToItemContainer(itemGuid.ItemGuid, appendFileName, contentImageBlob, Constants.AzureAccount_Image, true);
            }
         }

         policyForm.PolicyFinalize = new PolicyFinalize()
         {
            ProductDetail = product.ProductDetail,
            ItemGuids = itemGuids,
            Quantity = itemQuantity,
            StateCode = stateCode,
            WasteCode = wasteCodes.FirstOrDefault()?.Code,
            WasteDescription = wasteCodes.FirstOrDefault()?.Description
         };

         //delete the original images once they've been moved
         //remove this and setup an async purge of the webcam-uploads blob directory in the future

         //commented out 2.15.19 as we're running into items with missing images in their respective blob storage locations
         //ProductCommon.DeleteImage(sideImageBlob);
         //ProductCommon.DeleteImage(topImageBlob);
         //ProductCommon.DeleteImage(contentImageBlob);
        }

        ///****************************************************************************
        /// <summary>
        ///   Parse the selected expiration month and expiration year into a datetime value
        /// </summary>
        /// <param name="policyForm">reflects the input values for the item being inducted</param>
        ///****************************************************************************
        private static void SetExpirationFromForm(PolicyForm policyForm)
      {
         if (!policyForm.SelectedExpMonth.IsNullOrWhiteSpace() && !policyForm.SelectedExpYear.IsNullOrWhiteSpace())
         {
            policyForm.SelectedLot.ExpirationDate = new DateTime(policyForm.SelectedExpYear.AsInt(), policyForm.SelectedExpMonth.AsInt(), 1);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   This allows expiration date to populate from an entered lot number when
      ///   available.
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetLotExpiration(PolicyForm policyForm)
      {
         if (policyForm.SelectedLot.LotNumber.IsNullOrWhiteSpace())
         {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
         }

         LotResults lot = UpaDataAccess.GetLots(policyForm.ProductId, policyForm.SelectedLot.LotNumber).FirstOrDefault();

         return this.Json(lot, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      ///****************************************************************************
      private static void PopulatePolicyDropDownLists(PolicyForm model)
      {
         model.YearOptions = new List<SelectListItem>() { new SelectListItem { Text = string.Empty, Value = string.Empty } };
         var year = DateTime.UtcNow.Year;

         for (int i = year - 50; i < year + 50; i++)
         {
            model.YearOptions.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
         }

         model.YearOptions.Add(new SelectListItem { Text = "----NA----", Value = " " });

         model.MonthOptions = new List<SelectListItem>()
         {
            new SelectListItem {Text = "-------- Month --------", Value = string.Empty},
            new SelectListItem {Text = "1 - January", Value = "1"},
            new SelectListItem {Text = "2 - February", Value = "2"},
            new SelectListItem {Text = "3 - March", Value = "3"},
            new SelectListItem {Text = "4 - April", Value = "4"},
            new SelectListItem {Text = "5 - May", Value = "5"},
            new SelectListItem {Text = "6 - June", Value = "6"},
            new SelectListItem {Text = "7 - July", Value = "7"},
            new SelectListItem {Text = "8 - August", Value = "8"},
            new SelectListItem {Text = "9 - September", Value = "9"},
            new SelectListItem {Text = "10 - October", Value = "10"},
            new SelectListItem {Text = "11 - November", Value = "11"},
            new SelectListItem {Text = "12 - December", Value = "12"}
         };
      }
   }
}