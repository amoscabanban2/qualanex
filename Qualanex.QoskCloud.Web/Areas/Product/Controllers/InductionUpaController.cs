///-----------------------------------------------------------------------------------------------
/// <copyright file="InductionUpaController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   UpaController file collecting primarily Warehouse Induction related functionality.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
using Qualanex.QoskCloud.Web.Areas.Sidebar;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.Controllers;
using Qualanex.QoskCloud.Web.Areas.WrhsSort.ViewModels;
using Qualanex.QoskCloud.Web.Model;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Drawing;
   using System.IO;
   using System.Linq;
   using System.Net;
   using System.Web.Mvc;
   using System.Web.WebPages;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage.Blob;

   using PagedList;
   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Controllers;

   using WasteCode = Models.WasteCode;

   //=============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "QLUser")]
   public partial class UpaController
   {
      ///****************************************************************************
      /// <summary>
      ///   Shortcut to allow a url of /product/upa/induction for the WAREHS_RETURN appcode.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Induction()
      {
         return this.Index("WAREHS_RETURN");
      }

      /// <summary>
      /// Returns details of RA and tracking for a tote
      /// </summary>
      /// <param name="containerId"></param>
      /// <param name="instanceId"></param>
      /// <returns></returns>
      [ValidateAntiForgeryToken]
      public JsonResult GetRaInformation(string containerId, string instanceId)
      {
         Logger.Info($"GetRaInformation: Calling GetRaForTote from controller");
         var returnInformation = SidebarCommon.GetRaForTote(new ContainerInstance() { InstanceId = instanceId, ContainerId = containerId });
         Logger.Info($"GetRaInformation: GetRaForTote returned response to controller");
         return this.Json(returnInformation);
      }

      /// <summary>
      /// Returns information related to an item's Return Authorization, tracking, debit memo, and the RA's Profile
      /// </summary>
      /// <param name="itemGuid">ID specifying a returned item</param>
      /// <returns>JSON containing records for the item's Return Authorization, tracking, debit memo, and the RA's Profile</returns>
      [ValidateAntiForgeryToken]
      public JsonResult GetRaForItem(Guid itemGuid)
      {
         var returnInformation = SidebarCommon.GetRaForItem(itemGuid);
         return this.Json(returnInformation);
      }


      /// <summary>
      ///   Updates the bin designation segment
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <returns></returns>
      private ActionResult UpdateInductionResult(PolicyForm policyForm)
      {
         UpaDataAccess.GetProduct(policyForm.ProductId, getImprints: true);
         UpaDataAccess.GetWasteStreams(policyForm.ProductId, ProductCommon.GetUserState()); // cache results for future 
         //ReadyForDisposition specifies if all inputs are complete and the designation process should be entered
         if (policyForm.ReadyForDisposition)

         {
            this.GetBinDesignation(policyForm);
         }
         return this.PartialView("_BinResult", this.GetModelWithApplication(new UpaViewModel() { PolicyForm = policyForm }));
      }

      /// <summary>
      /// Determines the correct bin into which the current item should be sorted
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <returns></returns>
      [ValidateAntiForgeryToken]
      public JsonResult GetBinDesignation(PolicyForm policyForm)
      {
         var app = this.GetApplicationFromSession();
         var product = UpaDataAccess.GetProduct(policyForm.ProductId, getImprints: true);
         var wasteStreams = UpaDataAccess.GetWasteStreams(product.ProductDetail.NDCInnerPack ? (long)policyForm.RollupProductId : policyForm.ProductId, ProductCommon.GetUserState());
         LotResults lots = null;

         //BEGIN DEBUGGING
         //note that we're using the ri variable below for some debugging. This can be simplified once the debugging ends
         ReturnInformation ri = null;
         Logger.Info($"About to call GetRaForItem from GetBinDesignation");
         ri = SidebarCommon.GetRaForItem(policyForm.ItemGuid);
         Logger.Info($"Receiving response from GetRaForItem");

         if (ri == null)
         {
            Logger.Info($"GetRaForItem returned null; about to call GetRaForTote from GetBinDesignation");
            ri = SidebarCommon.GetRaForTote(policyForm.InboundContainer);
            Logger.Info($"Receiving response from GetRaForTote");
         }

         policyForm.ReturnInformation = ri;//SidebarCommon.GetRaForItem(policyForm.ItemGuid) ?? SidebarCommon.GetRaForTote(policyForm.InboundContainer);
         //END OF DEBUGGING

         policyForm.Product = product.ProductDetail;
         var appctrl = this.Request.Params["ctrl"];
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)GetUserLoginData(HttpContext);
         var logOn = objReturnLoginUserEntity?.UserReturnData?.UserName;

         try
         {
            //Green bin check is made first because it captures the green bin reasons
            //Green bin reasons are saved into the database upon item save
            //This accounts for green bin reasons in case we don't reach the green bin code (for example, if we get C2 items going into black bin)
            var isGrnBin = InGreenBin(policyForm, wasteStreams, product, out lots);

            if (policyForm.Product.CtrlNumber == 2 && UpaDataAccess.IsForm222Match(policyForm, logOn))
            {
               policyForm.BinDesignation = Constants.BlackSortBin;
               policyForm.BinText = "BLACK";
            }
            else if (policyForm.Product.CtrlNumber == 2)
            {
               policyForm.BinDesignation = Constants.WhiteSortBin;
               policyForm.BinText = "WHITE";
            }
            else if (policyForm.Product.CtrlNumber > 2 && HasOutbound(app, Constants.BlueSortBin) && appctrl == "0")
            {
               policyForm.BinDesignation = Constants.BlueSortBin;
               policyForm.BinText = "BLUE";
            }
            else if (isGrnBin && HasOutbound(app, Constants.GreenSortBin))
            {
               policyForm.BinDesignation = Constants.GreenSortBin;
               policyForm.BinText = "GREEN";
            }
            else if (InWhiteBin(policyForm, product, app.Code) && HasOutbound(app, Constants.WhiteSortBin))
            {
               policyForm.BinDesignation = Constants.WhiteSortBin;
               policyForm.BinText = "WHITE";
            }
            else if (ProductCommon.GetRecallId(product.ProductDetail.NDCInnerPack ? (int)policyForm.RollupProductId : policyForm.ProductId,
                        lots ?? new LotResults() { LotNumber = policyForm.SelectedLot.LotNumber },
                        policyForm.ReturnInformation?.Tracking?.RcvdDate) != null && HasOutbound(app, Constants.RedSortBin))
            {
               policyForm.BinDesignation = Constants.RedSortBin;
               policyForm.BinText = "RED";
            }
            else if (wasteStreams.Any(w => w.SeverityOrder > 0 && w.SeverityOrder <= 3) && HasOutbound(app, Constants.YellowSortBin))
            {
               policyForm.BinDesignation = Constants.YellowSortBin;
               policyForm.BinText = "YELLOW";
            }
            else if (policyForm.Product.CtrlNumber > 2 && HasOutbound(app, Constants.OrangeGaylord))
            {
               if (!policyForm.Product.SpecialHandling || policyForm.IsForeignContainer)
               {
                  policyForm.BinText = "ORANGE";
               }
               else
               {
                  var profile = ProfilesDataAccess.GetProfile(policyForm.Product.ProfileCode, 0);
                  policyForm.BinText = profile.Name + " ORANGE";
               }

               policyForm.BinDesignation = Constants.OrangeGaylord;

            }
            else
            {
               if (!policyForm.Product.SpecialHandling || policyForm.IsForeignContainer)
               {
                  policyForm.BinText = "GAYLORD";
               }
               else
               {
                  var profile = ProfilesDataAccess.GetProfile(policyForm.Product.ProfileCode, 0);
                  policyForm.BinText = profile.Name + " GAYLORD";
               }

               policyForm.BinDesignation = Constants.SmallGaylord;
            }

            this.PrepareItemDetails(policyForm);
            return this.Json(new { bin = policyForm.BinDesignation, itemId = policyForm.PolicyFinalize.ItemGuids.FirstOrDefault()?.ItemGuid });
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            policyForm.ReadyForDisposition = false;
            return this.Json(policyForm);
         }
      }

      /// <summary>
      /// Check if the upa application has access to a type of outbound container
      /// </summary>
      /// <param name="app">UPA Application object</param>
      /// <param name="type">Sidebar Container type</param>
      /// <returns></returns>
      private static bool HasOutbound(UPAApplication app, string type)
      {
         var result = app.childObj["SIDEBAR"].childObj.Values.Any(c => c.ObjectID == type && c.HasObjectId("Outbound"));
         return result;
      }


      /// <summary>
      /// Determines if item qualifies to be sorted into an audit bin
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="product">Product details for the inducted item</param>
      /// <param name="appCode">Get application</param>
      /// <returns></returns>
      private bool InWhiteBin(PolicyForm policyForm, ProductResult product, string appCode = "")
      {
         //web.config or azure application setting "DisableAuditSort" prevents sorting to white bin in all cases if set to "true"
         //If the item was already in a white bin, then do not send it back to white bin - assuming we're not re-processing the item in induction
         if (ConfigurationManager.GetAppSettingValue("DisableAuditSort") == "true" || (policyForm.Product.CtrlNumber != 2 && SidebarCommon.WasItemInWhiteBin(policyForm.ItemGuid) && appCode != "WAREHS_RETURN"))
         {
            return false;
         }

         var auditChance = policyForm.AuditChanceOverride ?? this.GetUserAuditRate();
         var productAuditRate = UpaDataAccess.GetProductAuditRate(policyForm.ProductId);
         decimal? lowTolerance;
         decimal? highTolerance;

         var containerList = new string[] { "Original|Sealed", "Original|Open", "Original|Case", "Prescription|Open", "None|Loose", "UnitDose|Open" };

         //If lot or expiration is set to null on any container types not included in the list, the item will always sort to audit
         if ((policyForm.SelectedLot.LotNumber == null || policyForm.SelectedExpYear == null) && !containerList.Contains(policyForm.ContainerType))
         {
            auditChance = 100;
         }

         //If an open container's item quantity does not match the calculated quantity, always sort to audit
         if ((policyForm.ContainerType.EndsWith("Open") || policyForm.ContainerType.EndsWith("Loose")) && product.ProductDetail.IndividualCountWeight != null && !policyForm.Product.ForceQuantityCount && (policyForm.ContentWeight ?? 0) > 0)
         {
            var expectedQuantity = policyForm.ContentWeight / product.ProductDetail.IndividualCountWeight;

            try
            {
               var percentDifference = Convert.ToInt32(100 * Math.Abs(policyForm.Quantity - (expectedQuantity ?? 0)) / ((policyForm.Quantity + expectedQuantity) * 2));

               if (percentDifference > 5)
               {
                  auditChance = 100;
               }
            }
            catch (Exception ex)
            {
               //do nothing. Unexpected division by zero
            }
         }

         if (policyForm.ContainerType == "Original|Sealed")
         {
            if (policyForm.Product.FullContainerWeight != null)
            {
               lowTolerance = (1 - Constants.WeightTolerance) * policyForm.Product.FullContainerWeight;
               highTolerance = (1 + Constants.WeightTolerance) * policyForm.Product.FullContainerWeight;

               if (policyForm.ProductWeight < lowTolerance || policyForm.ProductWeight > highTolerance)
               {
                  auditChance = 100;
               }
            }
         }
         else if (policyForm.ContainerType == "Original|Open")
         {
            if (policyForm.Product.FullContainerWeight != null)
            {
               highTolerance = (1 + Constants.WeightTolerance) * policyForm.Product.FullContainerWeight;

               if (policyForm.ProductWeight > highTolerance)
               {
                  auditChance = 100;
               }
            }

            if (policyForm.Product.ContainerWeight != null)
            {
               lowTolerance = (1 - Constants.WeightTolerance) * policyForm.Product.ContainerWeight;

               if (policyForm.ProductWeight < lowTolerance)
               {
                  auditChance = 100;
               }
            }
         }

         //re-processing an item in induction sends it to the white bin automatically
         if (!string.IsNullOrWhiteSpace(policyForm.ItemGuid.ToString()) && appCode == "WAREHS_RETURN")
         {
            auditChance = 100;
         }

         //Foreign containers and C2 inductions are never sorted to audit
         if (policyForm.IsForeignContainer || policyForm.Product.CtrlNumber == 2)
         {
            auditChance = -1;
         }

         //roll the dice, if the auditchance calculated above is above the random number, or the productAuditRate is above the random number, sort to white bin
         //the check against -1 is there in case we have to audit a product, but the user audit rate is 0 
         return (new Random().Next(100) < auditChance) || (new Random().Next(100) < productAuditRate && auditChance > -1);
      }

      /// <summary>
      /// Determines if an item qualifies for sorting into green bin
      /// </summary>
      /// <param name="policyForm">reflects the input values for the item being inducted</param>
      /// <param name="wasteStreams">waste streams for the inducted item</param>
      /// <param name="product">product detail for the inducted item</param>
      /// <param name="lot">lot for the inducted item is returned for reuse</param>
      /// <returns></returns>
      private static bool InGreenBin(PolicyForm policyForm, List<ProductWasteStream> wasteStreams, ProductResult product, out LotResults lot)
      {
         var greenBinItems = SidebarCommon.WasItemInGreenBin(policyForm.ProductId);
         var tempStatus = new List<ItemConditionRel>();
         if (greenBinItems.Any())
         {
            tempStatus = SidebarCommon.TempGreenBinConditionFix(greenBinItems.Select(i => i.ItemGUID).ToList());
         }

         //original sealed items with missing information or images are sorted to green bin if a matching item is not already in green bin
         //if (policyForm.ContainerType == "Original|Sealed" && !greenBinItems.Any(i => (i.ConditionCode == "Irrelevant" || i.ConditionCode == "Org") && i.ItemStateCode == "S") && !policyForm.IsForeignContainer)
         if (policyForm.ContainerType == "Original|Sealed" && !greenBinItems.Any(i => i.ItemStateCode == "S") && !policyForm.IsForeignContainer && !tempStatus.Any(x => x.ConditionCode == "Irrelevant"))
         {
            if (policyForm.Product.UnitsPerPackage == 1
                && (policyForm.Product.IndividualCountWeight == null && !policyForm.Product.ForceQuantityCount
                    || (policyForm.Product.DosageImageCode != "P" && policyForm.Product.ContainerWeight == null && !policyForm.Product.ForceQuantityCount)
                    || policyForm.Product.FullContainerWeight == null))
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.Weight;
            }

            if (!product.ImprintGroups.Any(r => r.ImageResults.Any(i => i.ImageType == "CTR")))
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.ContainerImage;
            }

            if (!product.ImprintGroups.Any(r => r.ImageResults.Any(i => i.ImageType == "CNT")))
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.ContentImage;
            }
         }

         //for the lot section only, always check for the missing lots on the OUTER product ID
         //if a product does not have an inner/outer packaging, then we can assume it's an outer as well
         var lotProductId = product.ProductDetail.NDCInnerPack ? policyForm.RollupProductId : policyForm.ProductId;

         //for inner/outer, check if either of the inner/outer were in the green bin for a lot reason
         var lotGreenBin = product.ProductDetail.NDCInnerPack
            ? !SidebarCommon.WasItemInGreenBin((int)policyForm.RollupProductId).Any(i => i.LotNumber == policyForm.SelectedLot.LotNumber && i.ExpirationDate == policyForm.SelectedLot.ExpirationDate) &&
              !greenBinItems.Any(i => i.LotNumber == policyForm.SelectedLot.LotNumber && i.ExpirationDate == policyForm.SelectedLot.ExpirationDate)
            : !greenBinItems.Any(i => i.LotNumber == policyForm.SelectedLot.LotNumber && i.ExpirationDate == policyForm.SelectedLot.ExpirationDate);

         //if lot information is entered but does not exist in the database, sort to greenbin
         lot = UpaDataAccess.GetLots((long)lotProductId, policyForm.SelectedLot.LotNumber).FirstOrDefault(l => l.ProductId == lotProductId && l.LotNumber.ToUpper() == policyForm.SelectedLot.LotNumber?.ToUpper());

         if (!policyForm.IsForeignContainer && lotGreenBin
             && (lot == null && !policyForm.SelectedLot.LotNumber.IsNullOrWhiteSpace()
                 || (lot != null
                     && (!policyForm.SelectedLot.LotNumber.IsNullOrWhiteSpace()
                         && policyForm.SelectedLot.ExpirationDate.HasValue
                         && (policyForm.SelectedLot.ExpirationDate.Value.Year != lot.ExpirationDate?.Year
                             || policyForm.SelectedLot.ExpirationDate.Value.Month != lot.ExpirationDate?.Month
                             )
                         )
                     )
                 )
             )
         {
            policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.LotExpiration;
         }

         //if there are no waste streams, sort to greenbin
         if (!wasteStreams.Any())
         {
            policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.WasteStream;
         }

         if (product.ProductDetail.AlwaysSortToQuarantine == true)
         {
            //if there is an item guid already check to see if this item has been processed before
            //else it is a new item and can't have been processed
            if (!string.IsNullOrEmpty(policyForm.ItemGuid.ToString()))
            {
               var quarantineCandidate = new PropertyValueRel
               {
                  RelationCode = policyForm.ItemGuid.ToString(),
                  PropertyCode = Constants.Property_Code_Quarantine
               };

               var result = UpaDataAccess.CheckPropertyValueRel(quarantineCandidate);
               if (result == null || result.SValue != "C")
               {
                  policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.Quarantine;
               }
            }
            else
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.Quarantine;
            }
         }

         //if no GreenBinReason is specified, don't sort to greenbin
         return policyForm.GreenBinReason != 0;
      }

      /// <summary>
      /// Returns the audit rate for the logged in user
      /// </summary>
      /// <returns>int indicating the rate out of 100 a user should be audited</returns>
      private int GetUserAuditRate()
      {
         var objReturnLoginUserEntity = (ReturnLoginUserEntity)GetUserLoginData(HttpContext);
         long? userId = 0;

         if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
         {
            userId = objReturnLoginUserEntity.UserReturnData.UserID;
         }

         return UpaDataAccess.GetUserAuditRate(userId ?? 2);
      }


      /// <summary>
      /// An AJAX endpoint for updating the _SegmentBinventory view.
      /// </summary>
      /// <param name="binId">the bin used to populate the binventory segment</param>
      /// <returns></returns>
      [ValidateAntiForgeryToken]
      public ActionResult GetBinventoryDetails(string binId)
      {
         this.ModelState.Clear();
         var app = GetApplicationFromSession();
         var stationId = SidebarCommon.GetMachineName(this.Request);
         var userName = GetUserLoginData(HttpContext).UserReturnData.UserName;

         var items = (string.IsNullOrWhiteSpace(binId) || binId == "undefined")
            ? new List<BinventoryDataAccess>()
            : BinventoryDataAccess.GetBinventoryList(binId, stationId, ProductCommon.GetUserState(), app.Code);

         var binventory = new Web.ViewModels.BinventoryViewModel
         {
            Options = new Web.ViewModels.BinventoryOptions()
            {
               RowProperty = new BinventoryDataAccess(),
            },
            Rows = items,
            DoNotAudit = app.Code == "WAREHS_AUDIT" && items.Any(x => x.CreatedBy == userName)
         };

         return this.PartialView("_SegmentBinventory", new UpaViewModel { Binventory = binventory });
      }

      ///**************************************************************************** 
      /// <summary> 
      ///   Find the original bin of the missorted item; determine what to do with it 
      /// </summary> 
      /// <param name="itemGUID"></param> 
      /// <param name="ibcontrId"></param> 
      /// <param name="ibcontrType"></param> 
      /// <returns></returns> 
      ///**************************************************************************** 
      [ValidateAntiForgeryToken]
      public ActionResult GetMissortedItemContr(string itemGUID, string ibcontrId, string ibcontrType)
      {
         var app = GetApplicationFromSession();
         var stationId = SidebarCommon.GetMachineName(this.Request);
         var stateCode = ProductCommon.GetUserState();
         var itemInfo = BinventoryDataAccess.GetItemBinInfo(itemGUID, stationId, stateCode, app.Code);

         // either item wasn't found at all, or item isn't in a container that's in the appropriate state
         if (itemInfo == null)
         {
            return this.Json(new
            {
               success = false,
               error = true,
               item = itemInfo,
               containerText = "",
               appSrc = app.Code
            });
         }

         // get missorted item's container info
         var itemContrId = itemInfo.WrhsContrID;
         var itemContrType = itemInfo.WrhsContrType;
         var contrText = SidebarCommon.GetContainerType(itemContrType).Description;

         // double check the item container ID and type matches the current IB container info
         if (itemContrType == ibcontrType && itemContrId == ibcontrId)
         {
            return this.Json(new
            {
               success = true,
               error = true,
               item = itemInfo,
               containerText = contrText,
               appSrc = app.Code
            });
         }

         // if container type of the missorted item matches the current IB container type, place item into that container 
         if (itemContrType == ibcontrType && itemContrId != ibcontrId)
         {
            var containers = SidebarCommon.GetContainer(ibcontrId);
            containers.Put(itemInfo.ItemGUID);

            return this.Json(new
            {
               success = true,
               error = false,
               item = itemInfo,
               containerText = contrText,
               appSrc = app.Code
            });
         }

         //otherwise, prompt for the item's container type to be pulled up on the sidebar
         return this.Json(new
         {
            success = false,
            error = false,
            item = itemInfo,
            containerText = contrText,
            appSrc = app.Code
         });

      }
   }
}
