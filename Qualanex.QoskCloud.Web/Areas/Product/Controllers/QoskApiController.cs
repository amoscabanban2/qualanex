﻿
namespace Qualanex.QoskCloud.Web.Areas.Product.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Web.Http;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;

   public class QoskApiController : ApiController
   {
      /// <summary>
      /// Gets product information from a SearchRequest
      /// </summary>
      /// <param name="page">For implementing paging</param>
      /// <param name="itemsPerPage">For implementing paging</param>
      /// <param name="manufacturer">Manufacturer from input form</param>
      /// <param name="product">Product name</param>
      /// <param name="ndc">NDC entered with dashes removed</param>
      /// <param name="generic">Generic name</param>
      /// <param name="lot">Lot Number</param>
      /// <param name="obverse">Imprint Text</param>
      /// <param name="reverse">Imprint Text</param>
      /// <param name="score">Score chosen from options</param>
      /// <param name="color">Color chosen from options</param>
      /// <param name="shape">Shape chosen from options</param>
      /// <param name="expirationDate"></param>
      /// <param name="packageSize"></param>
      /// <param name="unitsPerPackage"></param>
      /// <param name="ctrlNumber"></param>
      /// <param name="otc">OTC, RX or null</param>
      /// <param name="dosage"></param>
      /// <returns></returns>
      // GET: api/QoskApi
      [Route("QoskApi")]
      public IEnumerable<ProductResult> Get(int page = 1, int itemsPerPage = 100
          , string manufacturer = "", string product = ""
          , long ndc = -1, string generic = "", string lot = "", string obverse = ""
          , string reverse = ""
          , string score = "", string color = "", string shape = ""
          , string expirationDate = "", int packageSize = -1, int unitsPerPackage = -1
          , int ctrlNumber = -1, string otc = "", string dosage = null)
      {
         SearchRequest request = new SearchRequest
         {
            Product = new ProductDetail()
         };
         if (!string.IsNullOrWhiteSpace(manufacturer))
            request.Product.Manufacturer = manufacturer;
         if (!string.IsNullOrWhiteSpace(product))
            request.Product.Brand = product;
         if (!string.IsNullOrWhiteSpace(generic))
            request.Product.Generic = generic;
         if (!string.IsNullOrWhiteSpace(lot))
            request.Lot = lot;
         if (!string.IsNullOrWhiteSpace(obverse))
            request.Imprint.Obverse = obverse;
         if (!string.IsNullOrWhiteSpace(reverse))
            request.Imprint.Reverse = reverse;
         if (!string.IsNullOrWhiteSpace(score))
            request.Imprint.Score = score;
         if (!string.IsNullOrWhiteSpace(color))
            request.Imprint.Color = color;
         if (!string.IsNullOrWhiteSpace(shape))
            request.Imprint.Shape = shape;
         DateTime outDate;
         if (!string.IsNullOrWhiteSpace(expirationDate) && DateTime.TryParse(expirationDate, out outDate))
            request.ExpirationDate = outDate;
         if (!string.IsNullOrWhiteSpace(otc))
            request.Product.Rx = otc;
         if (ndc > 0)
            request.Product.Ndc = ndc;
         if (packageSize > 0)
            request.Product.PackageSize = packageSize;
         if (unitsPerPackage > 0)
            request.Product.UnitsPerPackage = unitsPerPackage;
         if (ctrlNumber > 0)
            request.Product.CtrlNumber = ctrlNumber;
         if (!string.IsNullOrWhiteSpace(dosage))
            request.Product.DosageCode = dosage;

         return UpaDataAccess.GetSearch(request);
      }
   }
}
