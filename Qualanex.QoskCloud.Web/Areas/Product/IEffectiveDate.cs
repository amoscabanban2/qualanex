﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Web.Areas.Product
{
    public interface IEffectiveDate
    {
        DateTime? EffectiveStartDate { get; set; }
        DateTime? EffectiveEndDate { get; set; }
    }
}
