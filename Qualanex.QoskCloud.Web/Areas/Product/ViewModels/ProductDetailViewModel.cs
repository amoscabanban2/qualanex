﻿
namespace Qualanex.QoskCloud.Web.Areas.Product.ViewModels
{
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Areas.Product.Models;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ProductDetailViewModel
   {
      public string Message { get; set; } = string.Empty;
      public SearchRequest Request { get; set; } = new SearchRequest();

      [DisplayName(@"NDC/UPC")]
      public string NDCUPCWithDashes { get; set; }

      [DisplayName(@"Shape")]
      public List<SelectListItem> ShapeOptions { get; set; } = new List<SelectListItem>();

      [DisplayName(@"Color")]
      public List<SelectListItem> ColorOptions { get; set; } = new List<SelectListItem>();

      [DisplayName(@"Score")]
      public List<SelectListItem> ScoreOptions { get; set; } = new List<SelectListItem>();

      [DisplayName(@"RX or OTC")]
      public List<SelectListItem> RxOptions { get; set; } = new List<SelectListItem>();

      [DisplayName("Form")]
      public List<SelectListItem> DosageOptions { get; set; } = new List<SelectListItem>();

      public List<string> ManufacturerOptions { get; set; } = new List<string>();
   }
}
