﻿
var hidWidth;
var scrollBarWidths = 120;

var widthOfList = function () {
    var itemsWidth = 0;
    $(".scroll-list .radio-inline").each(function () {
        var itemWidth = $(this).outerWidth();
        itemsWidth += itemWidth;
    });
    return itemsWidth;
};
var widthOfItem = function () {
    var itemWidth = $(".scroll-list .radio-inline").first().outerWidth();
       
    return itemWidth;
};
var widthOfHidden = function () {
    return (($(".scroll-wrapper").outerWidth()) - widthOfList() - getLeftPosi()) - scrollBarWidths;
};

var getLeftPosi = function () {
    return $(".scroll-list").position().left;
};

var reAdjust = function () {
    if (($(".scroll-wrapper").outerWidth()) < widthOfList()) {
        //$('.scroller-right').show();
    }
    else {
        //$('.scroller-right').hide();
    }

    if (getLeftPosi() < 0) {
        //$('.scroller-left').show();
    }
    else {
        $('.scroll-item').animate({ left: "-=" + getLeftPosi() + "px" }, 'slow');
        //$('.scroller-left').hide();
    }
    if (($(".scroll-wrapper").outerWidth()) === 0 && widthOfList() === 0) {

        //$('.scroller-right').show();
        //$('.scroller-left').show();
    }
}

reAdjust();

$(window).on('resize', function (e) {
    reAdjust();
});

$('.scroller-left').click(function () {
    if (!$('.scroll-list').is(':animated')) {
        var item = $(".scroll-wrapper").outerWidth();
        var move = 0;
        if (getLeftPosi() + item > 0)
            move = -getLeftPosi();
        else {
            move = item;
        }

        $('.scroll-list')
            .animate({ left: "+=" + move + "px" },
                'fast',
                function() {

                });
    }
});

$('.scroller-right').click(function () {
    if (!$('.scroll-list').is(':animated')) {
        var item = $(".scroll-wrapper").outerWidth();
        var move = 0;
        if (getLeftPosi() - ($(".scroll-wrapper").outerWidth() * 2) < -widthOfList())
            move = (getLeftPosi() - ($(".scroll-wrapper").outerWidth() - widthOfList()));
        else {
            move = item;
        }

        $('.scroll-list')
            .animate({ left: "-=" + move + "px" },
                'fast',
                function() {

                });
    } 
});