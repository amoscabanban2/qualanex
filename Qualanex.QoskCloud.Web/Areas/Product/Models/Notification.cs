﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="Notification.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Models for use in displaying notifications
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.Product.Models
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel.DataAnnotations;

   using Qualanex.Qosk.Library.Model.PolicyModel;
   using Qualanex.Qosk.Library.Common.CodeCorrectness;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public enum NotificationStatus
   {
      Pending,
      Actionable,
      Returned,
      Dispensed,
      StoreTransfer,
      DuplicateRemoved,
      ReturnInProgress,
      Done
   }

   ///============================================================================
   /// <summary>
   ///   Model for the entirety of the notification tab.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class Notification
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public Notification()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~Notification()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing product information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<List<ProductNotification>> ProductReminders { get; set; } = new List<List<ProductNotification>>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing product information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<RecallResult> Recalls { get; set; } = new List<RecallResult>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing product information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<long> ReturnStartedIds { get; set; } = new List<long>();

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Total of product and recall notifications to be shown to user.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public int Count()
      {
         return this.ProductReminders.Count + this.Recalls.Count;
      }

      #endregion
   }

   ///============================================================================
   /// <summary>
   ///   Model of Product notifications for showing to the user.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ProductNotification
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public ProductNotification()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~ProductNotification()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing product information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ProductDetail ProductDetail { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing lot information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public LotResults Lot { get; set; } = new LotResults();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date at which the notification will no longer display.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public DateTime? EndDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date when an open container can be returned for credit.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
      public DateTime? FutureIndateOpen { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date when a closed container can be returned for credit.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
      public DateTime? FutureIndateSealed { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Identifier to be used when clearing or updating notifications.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public long NotificationId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Container type of the product.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ContainerTypeAndCondition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Enum value indicating the container's type.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ContainerType ContainerType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Enum value indicating the Package's type.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PackageType PackageType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Enum value indicating the condition of the container.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ContainerCondition ContainerCondition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Identifier indicating the size of the case for relevant products.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int? CaseSize { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Describes if the notification can or has been handled.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string Status { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   GUID used to link multiple notifications for groups of cases or sealed
      ///   products.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public Guid? GroupGuid { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }

   ///============================================================================
   /// <summary>
   ///   Model of recall information to be displayed.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class RecallResult
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public RecallResult()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~RecallResult()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing product information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ProductDetail ProductDetail { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing lot information to be displayed.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<LotResults> Lots { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date that the recall goes into effect.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
      public DateTime? RecallStartDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date that the recall is no longer in effect.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
      public DateTime? RecallEndDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Date after which credit will not be offered for the recall.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
      public DateTime? CreditEndDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A formatted string specifying the recall class.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string RecallClass { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   String specifying the reimbursement provider.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ReimbursementProvider { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Numeric identifier to be used for updates and clearing.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public long RecallId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Bool indicating that an authorized user has acknowledged this recall
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public Acknlowedgement Acknowledgement { get; set; } = new Acknlowedgement();
      
      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }

   public class Acknlowedgement
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Authorized user has acknowledged this recall
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool Acknowledged { get; set; }


      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Datetime that an authorized user acknowledged this recall
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public DateTime? AcknowledgedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   User that acknowledged this recall
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string AcknowledgedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Recall acknowledged by this
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public long RecallId { get; set; }
   }
}
