﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyForm.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.Qosk.Library.Model.DBModel;

namespace Qualanex.QoskCloud.Web.Areas.Product.Models
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.PolicyModel;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using Sidebar.Models;


   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------

   [Flags]
   public enum GreenBinReason
   {
      ContainerImage = 1 << 0,
      ContentImage = 1 << 1,
      Weight = 1 << 2,
      WasteStream = 1 << 3,
      LotExpiration = 1 << 4,
      Override = 1 << 5,
      Quarantine = 1 << 6
   }


   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class PolicyForm : IDisposable
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public PolicyForm()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~PolicyForm()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A list of lots for the selected product.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<LotResults> AvailableLots { get; set; } = new List<LotResults>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The selected or entered lot information.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public LotResults SelectedLot { get; set; } = new LotResults();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   String of the year selected for the expiration date.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string SelectedExpYear { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   String of the month selected for the expiration date.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string SelectedExpMonth { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Month options for the dropdown.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<SelectListItem> MonthOptions { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Year options for the dropdown.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<SelectListItem> YearOptions { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Numeric identifier of the product being looked up for policy.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int ProductId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Quantity remaining in the package.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public decimal Quantity { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [Range(0, int.MaxValue)]
      public decimal? ProductWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [Range(0, int.MaxValue)]
      public decimal? ContentWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Selected string from Qosk.Library.PolicyModel.ContainerType enum.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ContainerType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Selected string from Qosk.Library.PolicyModel.PackageType enum.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string PackageType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Selected string from Qosk.Library.PolicyModel.ProductType enum.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ProductType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Product Detail to Display appropriate product information throughout
      ///   policy process.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ProductDetail Product { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing the result of the policy lookup.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PolicyResult PolicyResult { get; set; } = new PolicyResult();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Model containing the finalize data for induction.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PolicyFinalize PolicyFinalize { get; set; } = new PolicyFinalize();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Selected string from Qosk.Library.PolicyModel.ContainerCondition enum.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ContainerCondition { get; set; } = "Irrelevant";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   the URI for the uploaded side view image.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string SideImageUri { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The URI for the uploaded top view image.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string TopImageUri { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The URI for the uploaded top view image.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ContentImageUri { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The integer value representing the case size that the user selects.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName("Size")]
      [Range(1, int.MaxValue)]
      public int? SelectedCaseSize { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Message to be displayed when a product selection hasn't been made yet.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string Message { get; set; } = "Select a product to continue";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   String containing the serial number of the container.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string SerialNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A record of the barcode captured when finding the product.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ScannedBarCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Indicator for hazardous material.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int HazardClass { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A boolean indicating whether the necessary segments have been completed
      ///   without error.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool ReadyForDisposition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A list of notifications that are being returned in this induction.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<long> NotificationIds { get; set; } = new List<long>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container type designated by the applicaiton for warehouse induction
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string BinDesignation { get; set; }
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container type designated by the applicaiton for warehouse induction
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string BinText { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   An indicator for why an item is sorted to the green bin
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public GreenBinReason GreenBinReason { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   An indicator for why an item is overridden to the green bin
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public GreenBinReason GreenBinOverrideReason { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container from which an item is inducted
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ContainerInstance InboundContainer { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container from which an item is inducted
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ReturnInformation ReturnInformation { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container from which an item is inducted
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsForeignContainer { get; set; }
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The container from which an item is inducted
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsRecallPossible { get; set; }

      public Guid? ItemGuid { get; set; }
      public string ProductComment { get; set; }
      public string ItemWasteStreamProfileId { get; set; }
      public int? AuditChanceOverride { get; set; }
      public bool IsContractedMfg { get; set; }
      public int ContrSetting { get; set; }
      public int FreeGoodsSetting { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Does this profile force quantity count based on individual weight?
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsWeightCounting { get; set; }
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Numeric identifier of the outer product being looked up for policy.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int? RollupProductId { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }

   ///============================================================================
   /// <summary>
   ///   Model used for displaying results of a policy lookup.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class PolicyResult : IDisposable
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public PolicyResult()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~PolicyResult()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The string representation of the disposition selected by the user.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string Disposition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   A collection of violations to the policy.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<string> Violations { get; set; } = new List<string>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Status of the policy's applicability to the current product and policy
      ///   options.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PolicyInDate PolicyInDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public CreditDisposition CreditDisposition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Status of the Return given the current product and policy options.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string PolicyStatus { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Numeric identifier of the product being looked up.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public long ProductId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   URL containing the barcode label for product return.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string LabelUrl { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public PolicyModel PolicyModel { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Redirects user to logout when true.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool LogOut { get; set; } = false;

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }

   ///============================================================================
   /// <summary>
   ///   Model used for displaying the Finalize step of an induction.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class PolicyFinalize : IDisposable
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public PolicyFinalize()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~PolicyFinalize()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<Barcode> ItemGuids { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ProductDetail ProductDetail { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string WasteCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string WasteDescription { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Redirects user to logout when true.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool LogOut { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string StateCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public decimal Quantity { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string Message { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }

   public class ReturnInformation
   {
      public ReturnAuthorization ReturnAuthorization { get; set; }
      public DebitMemo DebitMemo { get; set; }
      public Tracking Tracking { get; set; }
      public Profile Profile { get; set; }
   }


   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class Barcode : IDisposable
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public Barcode()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~Barcode()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public Guid ItemGuid { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ItemBarcode { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }
}
