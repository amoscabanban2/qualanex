﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskLite.Models
{
    public class DisposalResult
    {
        public long Ndc { get; set; }
        public List<WasteCode> FederalWasteCodes { get; set; }
        public List<WasteCode> StateWasteCodes { get; set; }
        public List<WasteStream> WasteStreams { get; set; }
    }

    public class WasteCode
    {
        public string Description { get; set; }
        public string StateCode { get; set; }
        public string State { get; set; }
        public string ActiveIngredient { get; set; }
        public int SeverityOrder { get; set; }
        public string DotWasteStreamDescription { get; set; }
        public bool? DotSchedule0 { get; set; }
        public bool? DotSchedule2 { get; set; }
        public bool? DotSchedule345 { get; set; }
        public string HazardType { get; set; }
        public string HazardEmptyDescription { get; set; }
        public string HazardEmptyContent { get; set; }
        public string HazardEmptyContentAddInfo { get; set; }
        public string HazardEmptyShippingLabel { get; set; }
        public string HazardFullPartialDescription { get; set; }
        public string HazardFullPartialContent { get; set; }
        public string HazardFullPartialContentAddInfo { get; set; }
        public string HazardFullPartialShippingLabel { get; set; }
        public string HazardAddInfo { get; set; }
        public string HazardAddInfoPage { get; set; }
        public QoskHref AddInfoSdsUrl { get; set; }
        public string ReasonDescription { get; set; }
        public string ReasonDetail { get; set; }
        public List<QoskHref> ReasonUrls { get; set; }
        public string AddReasonDescription { get; set; }
        public string AddReasonDetail { get; set; }
        public List<QoskHref> AddReasonUrls { get; set; }
        public bool? AddReasonHighlight { get; set; }
        public string AddReasonText { get; set; }
        public string Highlights { get; set; }
        public string MsdsRsnWeb { get; set; }
        public string MsdsMfgWeb { get; set; }
        public bool? MsdsNotReq { get; set; }
        public bool? PossibleSyringe { get; set; }
        public QoskHref MsdsComment { get; set; }
        public string MsdsCommentUrl { get; set; }
        public string StateSumDescription { get; set; }
        public QoskHref StateSum { get; set; }
        public List<QoskHref> StateSums { get; set; }
    }
    
    public class WasteStream
    {
        public string Description { get; set; }
        public bool IsHazardous { get; set; }
        public decimal CostPerPound { get; set; }
        public decimal MinimumPoundsPerPallet { get; set; }
        public decimal MinimumPoundsPerDrum { get; set; }
        public decimal MinimumPoundsPerBox { get; set; }

    }
}