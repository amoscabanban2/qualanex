﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProductCommon.cs">
///   Copyright (c) 2014 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Common methods related to product processing.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using System.Web.Hosting;
using Qualanex.Qosk.Library.Model.PolicyModel;

namespace Qualanex.QoskCloud.Web.Areas.Product
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Microsoft.Ajax.Utilities;
   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Blob;

   using Qualanex.Qosk.Library.Model.ProductModel;
   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Utility.Common;
   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Qualanex.QoskCloud.Web.Areas.Product.ViewModels;
   using System.IO;
    using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;

    ///============================================================================
    /// <summary>
    ///   Summary not available.
    /// </summary>
    ///----------------------------------------------------------------------------
    public static class ProductCommon
   {
      #region Constructors

      ///////////////////////////////////////////////////////////////////////////////
      ///   No constructors defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Events

      ///////////////////////////////////////////////////////////////////////////////
      ///   No events defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      ///   No properties defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Disposal

      ///////////////////////////////////////////////////////////////////////////////
      ///   No disposal defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Event Handlers

      ///////////////////////////////////////////////////////////////////////////////
      ///   No event handlers defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Parses 2D barcodes and updates the product detail fields with appropriate values.
      /// </summary>
      /// <param name="productDetail"></param>
      ///****************************************************************************
      internal static void ParseNdcInput(ProductDetailViewModel productDetail)
      {
         if (!String.IsNullOrWhiteSpace(productDetail.NDCUPCWithDashes))
         {
            //we are running into issues where a part of an item GUID might match a 2D barcode key, which results in 
            //either an error or incorrect data (GUIDs as lot numbers, for example); hence, this first GUID check.
            var product = new ProductModel();
            Guid itemGuid;

            if (!Guid.TryParseExact(productDetail.NDCUPCWithDashes, "D", out itemGuid))
            {
               try
               {
                  product = ProductModel.Parse2DBarcode(productDetail.NDCUPCWithDashes);
               }
               catch (Exception ex)
               {
                  Logger.Error(ex);
                  throw;
               }
            }

            productDetail.Request.Product.Ndc = productDetail.Request.Product.Ndc ?? product.NDC;

            if (productDetail.Request.Product.Ndc == null && !productDetail.NDCUPCWithDashes.IsNullOrWhiteSpace())
            {
               long tempNdc;

               if (Int64.TryParse(productDetail.NDCUPCWithDashes.Replace("-", String.Empty), out tempNdc))
               {
                  productDetail.Request.Product.Ndc = tempNdc;
               }
               else
               {
                  productDetail.Request.Product.Ndc = -999;
               }
            }

            productDetail.Request.Product.Upc = productDetail.Request.Product.Upc ?? product.UPC;
            productDetail.Request.ExpirationDate = productDetail.Request.ExpirationDate ?? product.ExpirationDate;
            productDetail.Request.Lot = productDetail.Request.Lot ?? product.LotNumber;
            productDetail.Request.Product.SerialNumber = product.SerialNumber;
            productDetail.Request.Product.ScannedBarcode = productDetail.NDCUPCWithDashes;
            productDetail.Request.CaseQty = productDetail.Request.CaseQty ?? product.CaseQty;
         }
      }

      /// <summary>
      /// Converts a string of comma separated numbers into a List. Useful for receiving arrays from javascript
      /// </summary>
      /// <param name="commaSeperated">string of comma seperated long values</param>
      /// <returns></returns>
      internal static List<long> StringToLongList(string commaSeperated)
      {
         if (commaSeperated.IsNullOrWhiteSpace())
         {
            return new List<long>();
         }
         var stringArr = commaSeperated.Split(',');
         return (from s in stringArr where !s.IsNullOrWhiteSpace() select Int64.Parse(s)).ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///   Gets search results for a product detail.
      /// </summary>
      /// <param name="productDetail"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal static SearchResult PrepareSearchUpdate(ProductDetailViewModel productDetail)
      {
         return UpaDataAccess.GetSearch(productDetail.Request, false);
      }

      ///****************************************************************************
      /// <summary>
      ///   Updates the model for the waste tab.
      /// </summary>
      /// <param name="productID">product for which to look up waste information</param>
      /// <returns></returns>
      ///****************************************************************************
      internal static WasteResult PrepareWasteUpdate(long productID)
      {
         var model = new WasteResult();

         try
         {
            var state = GetUserState();

            if (state == null)
            {
               return new WasteResult
               {
                  LogOut = true
               };
            }

            model = UpaDataAccess.GetProductWasteInfo(productID, state);

            if (model.StateWasteCodes?.FirstOrDefault()?.SeverityOrder < 4)
            {
               model.DisplayClass = "level2";
            }
            else if (model.FederalWasteCodes?.FirstOrDefault()?.SeverityOrder < 4)
            {
               model.DisplayClass = "level3";
            }
            else
            {
               model.DisplayClass = "level1";
            }

         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            throw;
         }

         return model;
      }

      ///****************************************************************************
      /// <summary>
      ///   Gets the State (e.g.: IL, CA, WI) for the logged in user's profile.
      /// </summary>
      /// <returns>State</returns>
      ///****************************************************************************
      internal static string GetUserState()
      {
         var objReturnLoginUserEntity =
            (ReturnLoginUserEntity)
               QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         if (objReturnLoginUserEntity == null)
         {
            return null;
         }

         var profile = ProfilesDataAccess.GetProfile(objReturnLoginUserEntity.UserReturnData.ProfileCode ?? -999,
                                                      objReturnLoginUserEntity.UserReturnData.RollupProfileCode ?? -999);

         return profile.State;
      }

      ///****************************************************************************
      /// <summary>
      ///   Gets values for the dropdown lists on the form.
      /// </summary>
      /// <param name="model"></param>
      ///****************************************************************************
      internal static void PopulateDropDownLists(ProductDetailViewModel model)
      {
         model.ManufacturerOptions = UpaDataAccess.GetManufacturers();

         model.ShapeOptions = new List<SelectListItem>
         {
            new SelectListItem {Text = String.Empty, Value = String.Empty},
            new SelectListItem {Text = "round", Value = "round"},
            new SelectListItem {Text = "rectangle", Value = "rectangle"},
            new SelectListItem {Text = "square", Value = "square"},
            new SelectListItem {Text = "diamond", Value = "diamond"},
            new SelectListItem {Text = "3 sided", Value = "3 sided"},
            new SelectListItem {Text = "oblong", Value = "oblong"},
            new SelectListItem {Text = "5 sided", Value = "5 sided"},
            new SelectListItem {Text = "6 sided", Value = "6 sided"},
            new SelectListItem {Text = "8 sided", Value = "8 sided"},
            new SelectListItem {Text = "other", Value = "other"},
         };

         model.ColorOptions = new List<SelectListItem>
         {
            new SelectListItem {Text = String.Empty, Value = String.Empty},
            new SelectListItem {Text = "tan", Value = "tan"},
            new SelectListItem {Text = "brown", Value = "brown"},
            new SelectListItem {Text = "black", Value = "black"},
            new SelectListItem {Text = "turquoise", Value = "turquoise"},
            new SelectListItem {Text = "lavender", Value = "lavender"},
            new SelectListItem {Text = "gold", Value = "gold"},
            new SelectListItem {Text = "purple", Value = "purple"},
            new SelectListItem {Text = "gray", Value = "gray"},
            new SelectListItem {Text = "off-white", Value = "off-white"},
            new SelectListItem {Text = "yellow", Value = "yellow"},
            new SelectListItem {Text = "peach", Value = "peach"},
            new SelectListItem {Text = "red", Value = "red"},
            new SelectListItem {Text = "blue", Value = "blue"},
            new SelectListItem {Text = "pink", Value = "pink"},
            new SelectListItem {Text = "multi-colored", Value = "multi-colored"},
            new SelectListItem {Text = "orange", Value = "orange"},
            new SelectListItem {Text = "clear", Value = "clear"},
            new SelectListItem {Text = "white", Value = "white"},
            new SelectListItem {Text = "green", Value = "green"},
         };

         model.ScoreOptions = new List<SelectListItem>
         {
            new SelectListItem {Text = String.Empty, Value = String.Empty},
            new SelectListItem {Text = "double-scored", Value = "double-scored"},
            new SelectListItem {Text = "multi-scored", Value = "multi-scored"},
            new SelectListItem {Text = "partially scored", Value = "partially scored"},
            new SelectListItem {Text = "scored", Value = "scored"},
            new SelectListItem {Text = "triple-scored", Value = "triple-scored"},
         };
      }

      /// <summary>
      /// Seperates values into enums from a pipe(|) delimitted string
      /// Returns an anonymous object with enum values Type and Condition
      /// </summary>
      /// <param name="containerTypeAndCondition"></param>
      internal static dynamic SeperateContainerTypeAndCondition(string containerTypeAndCondition)
      {
         var packageType = PackageType.Unknown;
         var containerType = ContainerType.Unknown;
         var containerCondition = ContainerCondition.Unknown;

         var values = (containerTypeAndCondition ?? "").Split('|');
         if (values.Length < 3)
         {
            return new
            {
               Type = ContainerType.Unknown,
               Condition = ContainerCondition.Unknown,
               Package = PackageType.Unknown
            };
         }
         Enum.TryParse(values[0], out packageType);
         Enum.TryParse(values[1], out containerType);
         Enum.TryParse(values[2], out containerCondition);

         return new { Type = containerType, Condition = containerCondition, Package = packageType }; ;
      }

      ///****************************************************************************
      /// <summary>
      ///   Copies blob from its original location to an item specific location.
      /// </summary>
      /// <param name="itemGuid">used to create or find the appropriate directory</param>
      /// <param name="appendFileName">Appended to the file name to differentiate the image</param>
      /// <param name="originalBlob">The file being moved</param>
      /// <param name="accountName">The storage account holding the blobs</param>
      ///****************************************************************************
      public static void CopyBlobToItemContainer(Guid itemGuid, string appendFileName, CloudBlockBlob originalBlob,
         string accountName, bool generateThumbnail = false)
      {

         var guidContinerName = itemGuid.ToString().ToLower();

         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {

            var origFilePath = HostingEnvironment.ApplicationPhysicalPath + @"/ProductImages/webcam-uploads/" + originalBlob.Name;
            var newPath = HostingEnvironment.ApplicationPhysicalPath +
                          @"/ProductImages/saved-images/" + guidContinerName + appendFileName;
            File.Copy(origFilePath, newPath, true);
         }
         else
         {
            var connectString = ConfigurationManager.GetConnectionString(Constants.AzureAccount_Image);

            CloudStorageAccount account = CloudStorageAccount.Parse(connectString);

            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer container = client.GetContainerReference(guidContinerName);

            container.CreateIfNotExists();

            CloudBlockBlob newBlob = container.GetBlockBlobReference(guidContinerName + appendFileName);
            var blobCallbackInfo = new BlobCallbackInfo() { OriginalBlob = originalBlob, NewBlob = newBlob, GuidContainerName = guidContinerName };

            AsyncCallback asyncCallback = generateThumbnail ? new AsyncCallback(GenerateThumbAndDeleteOriginal) : new AsyncCallback(DeleteOriginal);
            newBlob.BeginStartCopy(originalBlob, asyncCallback, blobCallbackInfo);
          }
      }

        private class BlobCallbackInfo
        {
            public CloudBlockBlob OriginalBlob { get; set; }
            public CloudBlockBlob NewBlob { get; set; }
            public string GuidContainerName { get; set; }
        }

        private static void DeleteOriginal(IAsyncResult result)
        {
            // Note: we are not sure how this delete will work since image copy can be called mutiple times, commented out for now
            //var info = (BlobCallbackInfo)result.AsyncState;
            //DeleteImage(info.OriginalBlob);
        }

        private static void GenerateThumbAndDeleteOriginal(IAsyncResult result)
        {
            // Note: we are not sure how this delete will work since image copy can be called mutiple times, commented out for now
            //DeleteOriginal(result);

            var info = (BlobCallbackInfo)result.AsyncState;

            var targetAccountName = "qoskimagesthumbnails";
            var targetConnectString = ConfigurationManager.GetConnectionString(targetAccountName);
            CloudStorageAccount targetStorageAccount = CloudStorageAccount.Parse(targetConnectString);
            CloudBlobClient targetCloudBlobClient = targetStorageAccount.CreateCloudBlobClient();

            CloudBlobContainer targetContainer = ReportingBlobResource.GetThumbnailsContainer(targetCloudBlobClient, info.GuidContainerName);

            HostingEnvironment.QueueBackgroundWorkItem(ct => ReportingBlobResource.GenerateThumbnailForBlob(info.NewBlob, targetContainer, true));
        }

        /// <summary>
        /// Determines if any recalls cover any lot of a given product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static bool IsAnyProductRecall(int productId)
      {
         if (UpaDataAccess.GetRecallRelation(productId).Any())
         {
            return true;
         }
         return UpaDataAccess.GetLots(productId).Any(l => l.RecallId != null);
      }


      /// <summary>
      /// Determines if a manufacturer profile code is Qualanex-contracted
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      public static bool IsContractedMfg(int profileCode)
      {
         return UpaDataAccess.GetIsContractedMfg(profileCode);
      }

      /// <summary>
      /// Determines if a user profile allows for weight-based counting
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      public static bool IsWtCount(int profileCode)
      {
         return UpaDataAccess.GetIsWtCount(profileCode);
      }

      /// <summary>
      ///   Returns a recall ID for a given product and lot
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="lot"></param>
      /// <param name="checkdate">Optional, checks if this date falls within the
      /// effecting recall dates when finding a recallId, defaults to the current date.</param>
      /// <returns>recallId if present, null if no recall meets the criteria</returns>
      public static long? GetRecallId(int productId, LotResults lot, DateTime? checkdate = null)
      {
         checkdate = checkdate ?? DateTime.UtcNow;

         //determine the recall for any items that had the No Lot Number button entered, process it as if the lot was entered as NA
         var naLot = lot?.LotNumber.IsNullOrWhiteSpace() ?? false;

         var recallid = lot?.RecallId ?? ((lot != null && !lot.LotNumber.IsNullOrWhiteSpace()) || naLot
                           ? UpaDataAccess.GetLots(productId, naLot ? "NA" : lot.LotNumber).FirstOrDefault()?.RecallId
                           : null);

         if (recallid != null)
         {
            var recall = UpaDataAccess.GetRecallByID(recallid);

            if (recall != null && checkdate > recall.RecallStartDate && checkdate < recall.RecallEndDate)
            {
               return recall.RecallID;
            }
         }

         //couldn't find a lot-specific recall; check all lots flag
         var recallAllId = UpaDataAccess.GetRecallRelation(productId).FirstOrDefault(r => r.AllLots)?.RecallID;

         if (recallAllId != null)
         {
            var recall = UpaDataAccess.GetRecallByID(recallAllId);

            if (recall != null && checkdate > recall.RecallStartDate && checkdate < recall.RecallEndDate)
            {
               return recall.RecallID;
            }
         }

         return null;

         //// couldn't find a lot-specific recall; check all lots flag
         //var recallAll = UpaDataAccess.GetRecallRelation(productId).ToList();
         //return recallAll.FirstOrDefault(r => r.AllLots)?.RecallID;
      }

      /// <summary>
      /// Deletes a blob, used when images are moved, or retaken
      /// </summary>
      /// <param name="imageBlob"></param>
      public static void DeleteImage(CloudBlockBlob imageBlob)
      {
         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {
            try
            {
               var filePath = HostingEnvironment.ApplicationPhysicalPath +
                              @"/ProductImages/saved-images/" + imageBlob.Name;
               File.Delete(filePath);
            }
            catch (Exception e)
            {
               //do nothing
            }
         }
         else
         {
            try
            {
               imageBlob?.Delete(DeleteSnapshotsOption.IncludeSnapshots);
            }
            catch (Exception e)
            {
               //do nothing
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Uses SAS to get a product image from azure blob storage with limited
      ///   availability.
      /// </summary>
      /// <param name="fileName">String with the format of "{Container}\{FileName}"</param>
      /// <returns>URL With access token to blob resource</returns>
      ///****************************************************************************
      public static string GetProductImageUrl(string fileName)
      {
         string baseURI = String.Empty, sasContainerToken = String.Empty;

         if (fileName.IsNullOrWhiteSpace())
         {
            return @"/images/Defaultimages.jpg"; // TODO: replace with constant/config value
         }

         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {
            baseURI = @"/ProductImages/";
         }
         else
         {
            var containerName = fileName.Split('\\').First();
            var connectString = ConfigurationManager.GetConnectionString(Constants.AzureAccount_Products);

            if (String.IsNullOrEmpty(connectString))
            {
               throw new ApplicationException($"Configuration Setting for {Constants.AzureAccount_Products} not defined!");
            }

            var acct = CloudStorageAccount.Parse(connectString);
            var client = acct.CreateCloudBlobClient();

            baseURI = acct.CreateCloudBlobClient().BaseUri.ToString();

            var container = client.GetContainerReference(containerName);
            var sasConstraints = new SharedAccessBlobPolicy
            {
               SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1),
               Permissions = SharedAccessBlobPermissions.Read
            };

            sasContainerToken = container.GetSharedAccessSignature(sasConstraints);
         }

         return String.Concat(baseURI, fileName, sasContainerToken);
      }

      ///****************************************************************************
      /// <summary>
      ///   Upload files from local storage to blob.
      /// </summary>
      /// <param name="container">the directory in the azure container that will hold the blob</param>
      /// <param name="accountName">the storage account that will hold the blob</param>
      /// <param name="filePath">the current file location</param>
      /// <param name="localPath">if using local images, the path to move the image to</param>
      /// <param name="newFileName">file name after the move</param>
      /// <returns></returns>
      ///****************************************************************************
      public static void UploadFilesfromLocalToBlob(string container, string filePath, string accountName, string localPath, string newFileName)
      {
         try
         {
            if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true" && !String.IsNullOrWhiteSpace(localPath))
            {
               bool isExists = Directory.Exists(localPath);
               if (!isExists)
                  Directory.CreateDirectory(localPath);
               File.Copy(filePath, localPath + newFileName, true);
            }
            else
            {
               var connectString = GetConnectionString(accountName);

               CloudStorageAccount account = CloudStorageAccount.Parse(connectString);

               CloudBlobClient client = account.CreateCloudBlobClient();

               CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());

               sampleContainer.CreateIfNotExists();

               sampleContainer.SetPermissions(new BlobContainerPermissions()
               {
                  PublicAccess = BlobContainerPublicAccessType.Container
               });
               CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(newFileName);

               using (Stream file = File.OpenRead(filePath))
               {
                  blob.UploadFromStream(file);
               }

               if (File.Exists(filePath))
               {
                  File.Delete(filePath);
               }
            }
         }
         catch (Exception ex)
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get connection string for an azure storage account from the configuration.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetConnectionString(string account)
      {
         var connectString = String.Empty;

         connectString = ConfigurationManager.GetConnectionString(account);
         if (String.IsNullOrEmpty(connectString))
            throw new ApplicationException(String.Format(Constants.Azure_ConnectionString_NotDefined_Format, account));
         return connectString;
      }
   }

   #endregion
}
