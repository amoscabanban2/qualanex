﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStnMgmtController.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.Models;
   using Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_STATION")]
   public class WrhsStnMgmtController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsStnMgmtView()
      {
         this.ModelState.Clear();

         var viewModel = new WrhsStnMgmtViewModel
         {
            StationDetails = WrhsStnMgmtModel.GetStationList(false),
            LocationList = WrhsStnMgmtModel.GetLocationList(),
            ActionMenu = WrhsStnMgmtModel.GetActionMenu()
         };

         viewModel.SelectedStationMgmtModel = viewModel.StationDetails[0];

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="qoskEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsStnMgmtSave(string qoskEntity)
      {
         var blnCheck = System.Web.Helpers.Json.Decode<List<WrhsStnMgmtModel>>(qoskEntity)[0].QoskID == 0
                        ? (Dictionary<int, int>)WrhsStnMgmtModel.SaveStation(System.Web.Helpers.Json.Decode<List<WrhsStnMgmtModel>>(qoskEntity)[0], this.HttpContext.User.Identity.Name)
                        : (Dictionary<int, int>)WrhsStnMgmtModel.EditStation(System.Web.Helpers.Json.Decode<List<WrhsStnMgmtModel>>(qoskEntity)[0], this.HttpContext.User.Identity.Name);

         return this.Json(new
         {
            Success = blnCheck.FirstOrDefault().Value,
            Error = blnCheck.FirstOrDefault().Value,
            QoskID = blnCheck.FirstOrDefault().Key
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Used to refresh the Qosk Station Summary Grid
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new WrhsStnMgmtViewModel
         {
            StationDetails = WrhsStnMgmtModel.GetStationList(false)
         };

         return this.PartialView("_WrhsStnMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }
   }
}