﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStnMgmtModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.Models
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Linq;

   using Qualanex.QoskCloud.Web.Common;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStnMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public WrhsStnMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~WrhsStnMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      [DisplayName(@"QoskID")]
      public int QoskID { get; set; }

      [DisplayName(@"Profile Code")]
      public int ProfileCode { get; set; }

      [DisplayName(@"Machine ID")]
      public string MachineID { get; set; }

      [DisplayName(@"Machine S/N")]
      public string SerialNumber { get; set; }

      [DisplayName(@"Is Deleted")]
      public bool IsDeleted { get; set; }

      [DisplayName(@"Location Route")]
      public string LocationRoute { get; set; }

      [DisplayName(@"Location Description")]
      public string LocationDescription { get; set; }

      [DisplayName(@"QoskIDx")]
      public string QoskIDx { get; set; }

      [DisplayName(@"Route Dictionary Code")]
      public string Code { get; set; }

      [DisplayName(@"Route Dictionary Description")]
      public string Description { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Get a list of all non-deleted stations
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStnMgmtModel> GetLocationList()
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from wrhsRd in cloudEntities.WrhsRouteDict
                    where wrhsRd.IsDeleted == false
                    orderby wrhsRd.Code
                    select new WrhsStnMgmtModel
                    {
                       Code = wrhsRd.Code.ToString(),
                       Description = wrhsRd.Description + " - " + wrhsRd.Code
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get a list of all station locations
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStnMgmtModel> GetStationList(bool isDeleted)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var stnList = (from qosk in cloudEntities.Qosk
                           join wrhsRd in cloudEntities.WrhsRouteDict on qosk.LocationRoute equals wrhsRd.Code into routeDict
                           from wrhsRd in routeDict.DefaultIfEmpty()
                           where qosk.IsDeleted == isDeleted
                           orderby qosk.MachineID
                           select new WrhsStnMgmtModel
                           {
                              QoskID = qosk.QoskID,
                              ProfileCode = qosk.ProfileCode,
                              MachineID = qosk.MachineID,
                              SerialNumber = qosk.SerialNumber,
                              IsDeleted = qosk.IsDeleted,
                              LocationDescription = wrhsRd.Description + " - " + qosk.LocationRoute,
                              LocationRoute = qosk.LocationRoute.ToString(),
                              QoskIDx = qosk.QoskIDx
                           }).ToList();
            return stnList;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save new station
      /// </summary>
      /// <param name="stationEntity"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static object SaveStation(WrhsStnMgmtModel stationEntity, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var status = 0;
            var objects = new Dictionary<int, int>();
            objects.Add(0, status);

            try
            {
               var oldStation = (from qosk in cloudEntities.Qosk
                                 where qosk.MachineID == stationEntity.MachineID
                                 && !qosk.IsDeleted
                                 select qosk.QoskIDx).FirstOrDefault();

               if (oldStation == null)
               {

                  var newQosk = new Qosk
                  {
                     ProfileCode = stationEntity.ProfileCode,
                     MachineID = stationEntity.MachineID,
                     SerialNumber = stationEntity.SerialNumber,
                     Version = "1",
                     IsDeleted = false,
                     CreatedBy = curUser,
                     CreatedDate = DateTime.UtcNow,
                     ModifiedBy = curUser,
                     ModifiedDate = DateTime.UtcNow,
                     QoskIDx = Common.Methods.NextId(),
                     LocationRoute = (byte)(stationEntity.LocationRoute == "0" || string.IsNullOrWhiteSpace(stationEntity.LocationRoute)
                                      ? 0
                                      : Convert.ToInt32(stationEntity.LocationRoute))
                  };

                  cloudEntities.Qosk.Add(newQosk);
                  cloudEntities.SaveChanges();

                  //grab the newly-generated QoskID
                  var newQoskID = (from qosk in cloudEntities.Qosk
                                   where qosk.MachineID == stationEntity.MachineID
                                   && !qosk.IsDeleted
                                   select qosk.QoskID).FirstOrDefault();

                  status = 1;
                  objects.Clear();
                  objects.Add(newQoskID, status);
               }
               else
               {
                  objects.Clear();
                  objects.Add(0, -1);
               }
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }

            return objects;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save any station edits currently worked on in the Station management form
      /// </summary>
      /// <param name="stationEntity"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static object EditStation(WrhsStnMgmtModel stationEntity, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var status = 0;
            var objects = new Dictionary<int, int>();
            objects.Add(0, status);

            try
            {
               var existEntity = cloudEntities.Qosk.Where(c => c.QoskID == stationEntity.QoskID && c.IsDeleted == false).SingleOrDefault();
               var dupEntity = cloudEntities.Qosk.Where(c => c.MachineID == stationEntity.MachineID && c.QoskID != stationEntity.QoskID && c.IsDeleted == false).SingleOrDefault();

               if (dupEntity != null)
               {
                  objects.Clear();
                  objects.Add(0, -1);
               }
               else if (existEntity != null)
               {
                  existEntity.MachineID = stationEntity.MachineID;
                  existEntity.SerialNumber = stationEntity.SerialNumber;
                  existEntity.LocationRoute = (byte)(stationEntity.LocationRoute == "0" || string.IsNullOrWhiteSpace(stationEntity.LocationRoute)
                              ? 0
                              : Convert.ToInt32(stationEntity.LocationRoute));
                  existEntity.ModifiedBy = curUser;
                  existEntity.ModifiedDate = DateTime.UtcNow;
                  cloudEntities.SaveChanges();

                  status = 1;
                  objects.Clear();
                  objects.Add(existEntity.QoskID, status);
               }
               else
               {
                  objects.Clear();
                  objects.Add(0, -2);
               }
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }

            return objects;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }
}
