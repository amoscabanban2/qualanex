///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStnMgmtViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.WrhsStnMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStnMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_STATION";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsStnMgmtModel SelectedStationMgmtModel { get; set; } = new WrhsStnMgmtModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStnMgmtModel> StationDetails { get; set; } = new List<WrhsStnMgmtModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStnMgmtModel> LocationList { get; set; } = new List<WrhsStnMgmtModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }
}
