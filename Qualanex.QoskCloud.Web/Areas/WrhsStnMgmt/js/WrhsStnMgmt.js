﻿//*****************************************************************************************
//*
//* WrhsStnMgmt.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Provides functionality for the Station Management Page.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------
   var editMode = false;
   var qoskID = 0;
   var profileCode = 0;
   var baseUrl = "/WrhsStnMgmt";


   //#############################################################################
   // Global functions
   //#############################################################################


   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      initialState();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function initialState()
   {
      setEditMode(false);
      if (!$("#searchGrid tbody tr").hasClass("selected"))
      {
         $("#searchGrid tbody").find("tr:eq(1)").click();
      }
   }


   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      if (!getEditMode())
      {
         refreshStationGrid();
         initialState();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function ()
   {
      if (!getEditMode())
      {
         enableActionbarControl("#CMD_REFRESH", false);
         setEditMode(true);
         editMode = true;
         qoskID = 0;
         $("#MachineID,#SerialNumber").val("");
         $("#LocationDescription").val($("#LocationDescription option:first").val());
         $("#LocationDescription").attr("data-field-value", $("#LocationDescription option[value='" + $("#LocationDescription").val() + "']").closest("option").attr("id"));

         if ($("#searchGrid tr").hasClass("selected"))
         {
            $("#searchGrid tr.selected").removeClass("selected");
         }
      }

   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      if (!getEditMode())
      {
         enableActionbarControl("#CMD_REFRESH", false);
         setEditMode(true);
         editMode = true;
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function ()
   {
      if (getEditMode())
      {
         if ($("#MachineID").val() === undefined || $("#MachineID").val() === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#MachineID", "inputError");
         }
         if ($("#SerialNumber").val() === undefined || $("#SerialNumber").val() === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#SerialNumber", "inputError");
         }

         if ($(".formMax .inputEx,.selectEx").hasClass("inputError"))
         {

            if ($("#SerialNumber").val() === undefined || $("#SerialNumber").val() === "")
            {
               setColorAndClass("rgb(243, 104, 104)", "#SerialNumber", "inputError");
               $("#SerialNumber").focus();
            }
            if ($("#MachineID").val() === undefined || $("#MachineID").val() === "")
            {
               setColorAndClass("rgb(243, 104, 104)", "#MachineID", "inputError");
               $("#MachineID").focus();
            }
            return false;
         }

         var items = [
            {
               "QoskID": qoskID,
               "ProfileCode": 900048,
               "MachineID": $("#MachineID").val(),
               "SerialNumber": $("#SerialNumber").val(),
               "LocationRoute": $("#LocationDescription").attr("data-field-value")
            }
         ];

         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "qoskEntity=" + JSON.stringify(items) + getAntiForgeryToken(),
            url: baseUrl + baseUrl + "/WrhsStnMgmtSave",
            success: function (data)
            {
               if (data.Success === 1)
               {
                  setEditMode(false);
                  editMode = false;
                  $(".formMax .inputEx,.selectEx").removeClass("inputError");
                  $(".formMax .inputEx,.selectEx").removeAttr("style");
                  qoskID = 0;
                  profileCode = 0;
                  enableActionbarControl("#CMD_REFRESH", true);
                  refreshStationGrid();
                  $("#searchGrid tbody").find("tr:eq(1)").click();
               }
               else
               {
                  if (data.Success === -1)
                  {
                     messageObject = [{
                        "id": "lbl_messages",
                        "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>A Station With An Identical ID Already Exists.</h3>"
                     }];
                  }
                  else if (data.Success === -2)
                  {
                     messageObject = [{
                        "id": "lbl_messages",
                        "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> This Station Does Not Exist.</h3>"
                     }];
                  }

                  btnObjects = [{
                     "id": "btn_saveOk",
                     "name": "No",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center",
                     "function": "onclick='$(this).closeMessageBox();'"
                  }
                  ];

                  titleobjects = [{
                     "title": "Station Management Save"
                  }];
                  $(this).addMessageButton(btnObjects, messageObject);
                  $(this).showMessageBox(titleobjects);
                  setColorAndClass("rgb(243, 104, 104)", "#MachineID", "inputError");
                  $("#MachineID").focus();
               }
            }
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function ()
   {
      if (getEditMode())
      {
         if (getFormChanged())
         {
            messageObject = [{
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
            }];
            btnObjects = [{
               "id": "btn_cancel_saving",
               "name": "Yes",
               "class": "btnErrorMessage",
               "style": "margin:0 auto;text-align:center"
            }, {
               "id": "btn_no",
               "name": "No",
               "class": "btnErrorMessage",
               "style": "margin:0 auto;text-align:center",
               "function": "onclick='$(this).closeMessageBox();'"
            }
            ];

            titleobjects = [{
               "title": "Confirm Cancellation"
            }];
            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
         else
         {
            setEditMode(false);
            editMode = false;
            $(".formMax .inputEx,.selectEx").removeClass("inputError");
            $(".formMax .inputEx,.selectEx").removeAttr("style");
            enableActionbarControl("#CMD_REFRESH", true);
            $("#searchGrid tbody").find("tr:eq(1)").click();
         }
      }
   }

   window.UpdateData = function (saveAndValidate, field, data)
   {
      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        $("#" + field).val(data);
                     }
               }
               break;
            }

         case "SELECT":
            {
               if (field === "LocationRoute")
               {
                  $("#" + field).attr("data-field-value", data);
               }
               if (field === "LocationDescription")
               {
                  $("#" + field).val(data);
               }
               break;
            }
      }

      switch (field)
      {
         case "QoskID":
            {
               qoskID = data;
               break;
            }
         case "ProfileCode":
            {
               profileCode = data;
               break;
            }
      }
   }

   $(document).on("change", ".selectEx", function ()
   {
      if ($(this).attr("id") === "LocationDescription")
      {
         $(this).attr("data-field-value", $(this).find("option[value='" + $(this).val() + "']").closest("option").attr("id"));
         $(this).attr("value", $(this).val());
         updateDataTable($(this).val()
            , "LocationDescription"
            , qoskID);
      }
   });

   $(document).on("click", "#btn_cancel_saving", function ()
   {
      setEditMode(false);
      editMode = false;
      enableActionbarControl("#CMD_REFRESH", true);
      refreshStationGrid();
      $(".formMax .inputEx,.selectEx").removeClass("inputError");
      $(".formMax .inputEx,.selectEx").removeAttr("style");
      $(".inputEx").val("");
      $(this).closeMessageBox();
      $("#searchGrid tbody").find("tr:eq(1)").click();
   });

   $(document).on("click", "#registerMe", function ()
   {
      if (getEditMode())
      {
         $.getJSON("https://localhost:8007/Qualanex/http/MachineName",
            {}, function (data)
            {
               console.log("Machine Name: " + data);
               $("#MachineID").val(data);
               removeColorAndClass("#ECDD9D", "#MachineID", "inputError");
               updateDataTable(data
                  , "MachineID"
                  , qoskID);
            });
         $.getJSON("https://localhost:8007/Qualanex/http/MachineSerialNo",
            {}, function (data)
            {
               console.log("Machine S/N: " + data);
               $("#SerialNumber").val(data);
               removeColorAndClass("#ECDD9D", "#SerialNumber", "inputError");
            });

         setFormChanged(true);
      }
   });

   $(document).on("change", ".inputEx", function ()
   {
      if ($(this).attr("id") === "MachineID")
      {
         updateDataTable($(this).val()
            , "MachineID"
            , qoskID);

         if ($("#MachineID").val() === "" || $("#MachineID").val() === undefined)
         {
            setColorAndClass("rgb(243, 104, 104)", "#MachineID", "inputError");
            return false;
         }

         removeColorAndClass("#FFFFF", "#MachineID", "inputError");
      }
      if ($(this).attr("id") === "SerialNumber")
      {
         if ($("#SerialNumber").val() === "" || $("#SerialNumber").val() === undefined)
         {
            setColorAndClass("rgb(243, 104, 104)", "#SerialNumber", "inputError");
            return false;
         }

         removeColorAndClass("#FFFFF", "#SerialNumber", "inputError");
      }

      if ($(this).attr("nextfocus") !== undefined &&
         $(this).attr("nextfocus") !== "")
      {
         $("#" + $(this).attr("nextfocus")).focus();
      }
   });

   $(document).on("focusout", "#MachineID", function (e)
   {
      if (getEditMode())
      {
         if ($("#MachineID").val() === undefined || $("#MachineID").val() === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#MachineID", "inputError");
            return false;
         }
      }

   });

   $(document).on("focusout", "#SerialNumber", function (e)
   {
      if (getEditMode())
      {
         if ($("#SerialNumber").val() === undefined || $("#SerialNumber").val() === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#SerialNumber", "inputError");
            return false;
         }
      }
   });

   $(document).on("keypress", "#MachineID", function (e)
   {
      removeColorAndClass("#ECDD9D", "#MachineID", "inputError");
      if (e.keyCode === 13)
      {
         updateDataTable($(this).val()
            , "MachineID"
            , qoskID);
         if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
         {
            $("#" + $(this).attr("nextfocus")).focus();
         }
      }
   });

   $(document).on("keypress", "#SerialNumber", function (e)
   {
      removeColorAndClass("#ECDD9D", "#SerialNumber", "inputError");
      if (e.keyCode === 13)
      {
         if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
         {
            $("#" + $(this).attr("nextfocus")).focus();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set data from input to Grid.
   //*
   //* Parameters:
   //*   inValue - Description not available.
   //*   inField - Description not available.
   //*   gpId    - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function updateDataTable(inValue, inField, recvId)
   {
      var row = $("#searchGrid tbody").find("tr[MgmtEntity-id=0]").closest("tr");

      if (recvId !== 0)
      {
         row = $("#searchGrid tbody tr.selected").closest("tr");
      }

      if (inField === "MachineID")
      {
         row.find("td:eq(0)").html(inValue);
      }

      else if (inField === "LocationDescription")
      {
         row.find("td:eq(1)").html(inValue);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Refresh qosk Station list
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshStationGrid()
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "isDeleted=" + false + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/ViewDataRequest",
         success: function (data)
         {
            var $target = $("#WrhsStnMgmtGridSummary");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            window.dtHeaders(".QoskSummary", '"order": [[1, "asc"]]', [], []);
         }
      });
   }
});
