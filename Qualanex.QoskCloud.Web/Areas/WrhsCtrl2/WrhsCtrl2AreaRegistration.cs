﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsCtrl2AreaRegistration.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsCtrl2
{
   using System.Web.Mvc;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsCtrl2AreaRegistration : AreaRegistration
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AreaName => "WrhsCtrl2";

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="context"></param>
      ///****************************************************************************
      public override void RegisterArea(AreaRegistrationContext context)
      {
         context.MapRoute(
             "WrhsCtrl2_default",
             "WrhsCtrl2/{controller}/{action}/{id}",
             new { action = "Index", id = UrlParameter.Optional }
         );
      }
   }
}
