///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsCtrl2Model.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsCtrl2.ViewModels
{
   using System;
   using System.IO;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Areas.Sidebar;
   using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Blob;
   using Microsoft.WindowsAzure.Storage.Shared.Protocol;

   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using Model;
   using Utility.Common;
   using Utility;
   using System.Data.SqlClient;
   using System.Data.Entity.Validation;
   using System.Data.Entity.Infrastructure;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsCtrl2Model
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public WrhsCtrl2Model()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~WrhsCtrl2Model()
      {
         this.Dispose(false);
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA Number")]
      public string RA { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA ID")]
      public string RAID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Manufacturer")]
      public string Mfg { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Totes Received")]
      public int? WrhsContrIdR { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Totes Finished")]
      public int? WrhsContrIdF { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Totes In Process")]
      public int? WrhsContrIdI { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Ready")]
      public bool? Ready { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Approved")]
      public bool Approved { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"To")]
      public string To { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Address")]
      public string Address { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Debit Memo")]
      public string DebitMemo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Date")]
      public string Date { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"City & State")]
      public string CityState { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"DEA")]
      public string DEA { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"NDC")]
      public string NDC { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Outer NDC")]
      public string OuterNDC { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"NDC Inner Pack")]
      public bool NdcInnerPack { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"GUID")]
      public Guid? GUID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Unit")]
      public string Unit { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"White Bin")]
      public string WhiteBin { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Black Bin")]
      public string BlackBin { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Control Status")]
      public string CtrlStatus { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA Status")]
      public string RAStatus { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form Status")]
      public string FormStatus { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form Qty.")]
      public decimal? FormQty { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form Qty Remaining")]
      public decimal? FormQtyRemaining { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Line#")]
      public int? LineNo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Qty.")]
      public decimal? ItemQty { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Qty Remaining")]
      public decimal? ItemQtyRemaining { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Applied Qty")]
      public decimal? AppliedQty { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Unit")]
      public string ItemUnit { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Condition")]
      public string Condition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"List of Warehouse Control 2")]
      public List<WrhsCtrl2Model> WrhsCtrl2List { get; set; } = new List<WrhsCtrl2Model>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Orders")]
      public int Orders { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form222Condition")]
      public string F2Condition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Match Rate")]
      public decimal? MatchRate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Product ID")]
      public int ProductID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Match")]
      public int Match { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Row Id")]
      public int RowId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"ParentRowId")]
      public int ParentRowId { get; set; }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Get RA information
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsCtrl2Model GetRaNumberInfo(string raNumber)
      {
         var raInfo = new WrhsCtrl2Model();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var sqlScript = "select distinct raTbl.RtrnAuthID as RAID, convert(nvarchar(max), raTbl.RtrnAuthNumber) as RA, raTbl.RtrnAuthStatusCode as RAStatus, raTbl.RtrnAuthCtrlCode as CtrlStatus " +
                               " from ReturnAuthorization raTbl " +
                               " where convert(nvarchar(max),raTbl.RtrnAuthNumber) = @raNumber";
               raInfo = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlScript, new SqlParameter("@raNumber", raNumber)).FirstOrDefault();

               if (raInfo == null)
               {
                  sqlScript = "select distinct raTbl.RtrnAuthID as RAID,convert(nvarchar(max), raTbl.RtrnAuthNumber) as RA,raTbl.RtrnAuthStatusCode as RAStatus ,raTbl.RtrnAuthCtrlCode as CtrlStatus " +
                              " from ReturnAuthorization raTbl " +
                              " left outer join tracking on raTbl.RtrnAuthID = AssocID " +
                              " left outer join WrhsContrTrackingRel on Tracking.TrackingID = WrhsContrTrackingRel.TrackingID " +
                              " left outer join WrhsContrItemRel on WrhsContrTrackingRel.WrhsContrID = WrhsContrItemRel.WrhsContrID " +
                              " and WrhsContrTrackingRel.WrhsInstID = WrhsContrItemRel.WrhsInstID where convert(nvarchar(max), WrhsContrItemRel.ItemGUID)=@itmGuid ";
                  raInfo = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlScript, new SqlParameter("@itmGuid", raNumber)).FirstOrDefault();
               }
            }
         }
         catch (Exception ex)
         {
            raInfo = null;
            Log.Write(LogMask.Error, ex.ToString());
         }

         return raInfo;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get list of form222 associated with RA
      /// </summary>
      /// <param name="RA"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsCtrl2Model> GetCtrl2Summary()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var sqlQuery = " select Distinct raTbl.RtrnAuthID as RAID, convert(nvarchar, raTbl.RtrnAuthNumber) as RA, " +
                           " dbtMemo.DebitMemoNumber as DebitMemo, mfrProfile.Name as Mfg, " +
                           " (select count(*) from WrhsContrTrackingRel as rthStatus join Tracking rtTrck " +
                           " on rthStatus.TrackingID = rtTrck.TrackingID where rtTrck.AssocID = raTbl.RtrnAuthID " +
                           " and raTbl.IsDeleted = 0 and rthStatus.IsDeleted = 0) as WrhsContrIdR, " +
                           " (select top 1 convert(varchar,rtTrck.StgeDate,101) from WrhsContrTrackingRel as rthStatus " +
                           " join Tracking rtTrck on rthStatus.TrackingID = rtTrck.TrackingID where rtTrck.AssocID = raTbl.RtrnAuthID and " +
                           " raTbl.IsDeleted = 0 and rthStatus.IsDeleted = 0 order by stgeDate desc) as Date " +
                           " from ReturnAuthorization raTbl " +
                           " join DebitMemo dbtMemo on raTbl.DebitMemoID = dbtMemo.DebitMemoID " +
                           " inner join Form222 frm222 on raTbl.RtrnAuthNumber = frm222.Form222SourceID " +
                           " left outer join Profile mfrProfile on dbtMemo.MfrProfileCode = mfrProfile.ProfileCode " +
                           " where raTbl.RtrnAuthCtrlCode = 'CTRL2' and raTbl.RtrnAuthStatusCode not in ('C','CE','CD') and " +
                           " frm222.Status in ('Printed','Closed') and frm222.PrintedDate >= DATEADD(DAY, -30, getdate()) ";
                           //" and not exists(select HasDiscrepancy from Item222Rel where frm222.DEAFormNumber = Item222Rel.DEAFormNumber and HasDiscrepancy=0)";

            var form222Lst = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlQuery).ToList();

            SetAttachedContainersInformation(cloudEntities, form222Lst.Where(c => c.WrhsContrIdR > 0).ToList());

            return form222Lst;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get list of form222 associated with RA and set number of container 
      ///   that associated with RA
      /// </summary>
      /// <param name="cloudEntities"></param>
      /// <param name="form222Lst"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void SetAttachedContainersInformation(QoskCloud cloudEntities, List<WrhsCtrl2Model> form222Lst)
      {
         foreach (var items in form222Lst)
         {
            var lst = (from rthStatus in cloudEntities.WrhsContrTrackingRel
                       join rtTrck in cloudEntities.Tracking on rthStatus.TrackingID equals rtTrck.TrackingID
                       where rtTrck.AssocID == items.RAID
                       && rtTrck.IsDeleted == false
                       && rthStatus.IsDeleted == false
                       orderby rthStatus.CreatedDate descending
                       select rthStatus).ToList();

            foreach (var wrhsCntr in lst)
            {
               var status = cloudEntities.WrhsContrStatusHist
                  .Where(c => c.WrhsContrID == wrhsCntr.WrhsContrID).Select(c => new { c.StatusCode, c.WrhsInstID, c.CreatedDate })
                  .OrderByDescending(c => c.CreatedDate).FirstOrDefault();
               //right now we don't have any other status S,I,A, when we added other statuses
               // when we added pause status we can remove the comments below
               //  right now just simply showing changed instaces
               if (status.WrhsInstID != wrhsCntr.WrhsInstID)//&& status.StatusCode == "A"
               {
                  items.WrhsContrIdF += 1;
               }
               //else 
               //{
               //   var oldStatusInst = status.WrhsInstID;
               //   status = cloudEntities.WrhsContrStatusHist
               //      .Where(c => c.WrhsContrID == wrhsCntr.WrhsContrID
               //      && c.WrhsInstID == wrhsCntr.WrhsInstID)
               //      .Select(c => new { c.StatusCode, c.WrhsInstID, c.CreatedDate })
               //      .OrderByDescending(c => c.CreatedDate).FirstOrDefault();
               //   if (oldStatusInst == wrhsCntr.WrhsInstID
               //      && (status.StatusCode != "S" && status.StatusCode != "I")
               //      || oldStatusInst != wrhsCntr.WrhsInstID)
               //   {
               //      items.WrhsContrIdF += 1;
               //   }
               //}
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get list of form222 details associated with RA
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="raId"></param>
      /// <param name="status"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsCtrl2Model> GetCtrl2Detail(int ra, string raId, int status, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            //always delete any non-approved entries, as we have to recalculate the form each time
            var query = "delete from item222rel where ApprovedDate is null and (itemguid in (select itemguid from wrhscontritemrel where wrhscontrid in " +
                        "(select rthStatus.wrhscontrid from WrhsContrTrackingRel as rthStatus join Tracking rtTrck on rthStatus.TrackingID = rtTrck.TrackingID " +
                        " join ReturnAuthorization raTbl on rtTrck.AssocID = raTbl.RtrnAuthID where raTbl.IsDeleted = 0 and rthStatus.IsDeleted = 0 and raTbl.rtrnauthnumber = " + ra + ")) " +
                        " or deaformnumber in (select deaformnumber from form222 where Form222SourceID = " + ra + "))";
            cloudEntities.Database.ExecuteSqlCommand(query);
            cloudEntities.SaveChanges();

            var sqlMatched = "select case when rl.DEAFormNumber = -1 then '' else convert(nvarchar, rl.DEAFormNumber) end as DEA, " +
                             " case when rl.DEAFormNumber = -1 then 2 else 1 end as Orders, " +
                             " ISNULL(rl.LineNumber, 0) as 'LineNo', case when rl.DEAFormNumber = -1 then convert(nvarchar, prdct2.NDC)  " +
                             " else convert(nvarchar, frm.NDC) end as NDC, " +
                             " cast(case when isnull(frm.SealedOpenCase,'Opened') = 'Opened' and prdct2.ndcinnerpack = 1 and isnull(outerPrdct.unitsperpackage,1) > 1 " +
                             " then frm.OriginalNbrofPackages * (outerPrdct.PackageSize / outerPrdct.UnitsPerPackage) " +
                             " else frm.OriginalNbrofPackages * ISNULL(frm.PackageSize, 0) end as decimal(18,2)) as FormQty, case when " +
                             " convert(nvarchar(max), rl.ItemGUID) = '00000000-0000-0000-0000-000000000000' then (select top(1) " +
                             " UnitOfMeasure from product where NDC = frm.ndc) else prdct2.UnitOfMeasure end as Unit, isnull(frm.SealedOpenCase,'Opened') as F2Condition, " +
                             " case when convert(nvarchar(max), rl.ItemGUID) = '00000000-0000-0000-0000-000000000000' then null else rl.ItemGUID end as GUID, " +
                             " case when itm.ItemStateCode = 'O' then ISNULL(itm.Quantity, 0) else ISNULL(prdct2.PackageSize, 0) " +
                             " end as ItemQty, cnd.Description as Condition, prdct2.UnitOfMeasure as ItemUnit, /*case when " +
                             " (frm.SealedOpenCase = cnd.Description or frm.SealedOpenCase = 'Sealed' and itm.ItemStateCode = 'O') and " +
                             " prdct2.UnitOfMeasure = prdct2.UnitOfMeasure and frm.PackageSize = case when itm.ItemStateCode = 'O' " +
                             " then itm.Quantity else prdct2.PackageSize end then 1 else case when rl.HasDiscrepancy = 0 then 1 else " +
                             " case when rl.ApprovedDate is not null then 4 else 0 end end end as Match,*/ rl.AppliedQty, " +
                             " case when rl.ApprovedDate is not null then cast(1 as bit) else cast(0 as bit) end as Approved " +
                             " from item222rel rl left join Form222Detail frm on rl.DEAFormNumber = frm.DEAFormNumber and rl.LineNumber = frm.LineNumber " +
                             " left outer join item itm on (itm.ItemGUID = rl.ItemGUID) and convert(nvarchar(max), rl.ItemGUID) <> '00000000-0000-0000-0000-000000000000' " +
                             " left join ItemStateDict cnd on itm.ItemStateCode = cnd.Code left join product prdct2 on itm.ProductID = prdct2.ProductID " +
                             " left join product outerPrdct on outerPrdct.productid = prdct2.RollupProductID " +
                             " where rl.IsDeleted = 0 and frm.DEAFormNumber in (select DEAFormNumber from Form222 where Form222SourceID = @ra) or (rl.itemguid in " +
                             " (select ItemGUID from WrhsContrItemRel left join WrhsContrTrackingRel wrhstrRl on WrhsContrItemRel.WrhsContrID = " +
                             " wrhstrRl.WrhsContrID and WrhsContrItemRel.WrhsInstID = wrhstrRl.WrhsInstID where wrhstrRl.TrackingID in (select trackingid from tracking where AssocID = @raId)) " +
                             " and rl.DEAFormNumber = -1) order by orders, NDC, DEA, 'LineNo'";

            var form222MatchedLst = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlMatched
                                                            , new SqlParameter("@ra", ra)
                                                            , new SqlParameter("@raId", raId)).ToList();

            //if (form222MatchedLst.Any())
            //{
            //   var itemguids = form222MatchedLst.Where(c => c.GUID.ToString() != "").GroupBy(c => c.GUID).ToList();

            //   //match up a single item GUID to multiple forms; determine if there's a discrepancy or not
            //   foreach (var guids in itemguids)
            //   {
            //      var matchedForms = form222MatchedLst.Where(c => c.GUID == guids.Key).ToList();

            //      if (matchedForms.Any())
            //      {
            //         var itmsQntity = form222MatchedLst.FirstOrDefault(c => c.GUID == guids.Key).ItemQty;
            //         matchedForms.ForEach(c => c.Match = c.Match == 0
            //            ? (form222MatchedLst.Where(d => d.GUID == guids.Key).Sum(d => d.FormQty) == itmsQntity ? 1 : 0)
            //            : c.Match);
            //      }
            //   }

            //   var formLinePairs = form222MatchedLst.Where(c => c.GUID.ToString() != "" && c.Match == 0).GroupBy(c => new { c.DEA, c.LineNo }).ToList();

            //   //match up a single line to multiple items; determine if there's a discrepancy or not
            //   foreach (var line in formLinePairs)
            //   {
            //      var deaFormNumber = line.Key.DEA;
            //      var lineNumber = line.Key.LineNo;
            //      var matchedForms = form222MatchedLst.Where(c => c.DEA == deaFormNumber && c.LineNo == lineNumber).ToList();

            //      if (matchedForms.Any())
            //      {
            //         var formQty = form222MatchedLst.FirstOrDefault(c => c.DEA == deaFormNumber && c.LineNo == lineNumber).FormQty;
            //         matchedForms.ForEach(c => c.Match = c.Match == 0
            //            ? (form222MatchedLst.Where(d => d.DEA == deaFormNumber && d.LineNo == lineNumber).Sum(d => d.ItemQty) == formQty ? 1 : 0)
            //            : c.Match);
            //      }
            //   }

            //   return form222MatchedLst;
            //}
            //else
            if (true)
            {
               // get information from form 222 table under entered RA
               var sqlQuery = "select distinct convert(nvarchar, frm222.DEAFormNumber) as DEA, frm222Details.LineNumber as 'LineNo', " +
                              " product.UnitOfMeasure as Unit, convert(nvarchar, frm222Details.NDC) as NDC, " +
                              " case Product.NdcInnerPack when 1 then convert(nvarchar, outerPrdct.NDC) else null end as OuterNDC, " +
                              " CAST(CASE WHEN frm222Details.SealedOpenCase = 'Opened' AND product.UnitsPerPackage > 1 " +
                              " THEN (frm222Details.OriginalNbrofPackages * (product.PackageSize / product.UnitsPerPackage)) " +
                              " ELSE (frm222Details.OriginalNbrofPackages * frm222Details.PackageSize) END AS DECIMAL(18,2)) as FormQty, " +
                              " isnull(frm222Details.SealedOpenCase,'Opened') as F2Condition, frm222.Status as FormStatus, " +
                              " case when itemRel.itemGuid = '00000000-0000-0000-0000-000000000000' then null else itemRel.itemGuid end as GUID, product.NDCInnerPack as NdcInnerPack " +
                              " from Form222 frm222 " +
                              " left outer join Form222Detail frm222Details on frm222.DEAFormNumber = frm222Details.DEAFormNumber " +
                              " left outer join Item222Rel itemRel on frm222Details.DEAFormNumber = itemRel.DEAFormNumber and frm222Details.LineNumber = itemRel.LineNumber " +
                              " inner join productlink AS PD ON frm222Details.ProductID = PD.qipsproductid " +
                              " left join product product on PD.ProductID = product.ProductID " +
                              " left join product outerPrdct on outerPrdct.ProductID = product.RollupProductID " +
                              " where frm222.IsDeleted = 0 and frm222.Status in ('Printed','Closed') and frm222Details.IsDeleted=0 and frm222.Form222SourceID = @ra " +
                              " order by NDC, DEA, 'LineNo', F2Condition desc";

               var form222Lst = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlQuery, new SqlParameter("@ra", ra)).ToList();

               if (form222Lst.Any(f => f.FormStatus == "Closed"))
               {
                  var closedForms = form222Lst.Where(f => f.FormStatus == "Closed").Select(f => f.DEA).Distinct().ToList();
                  var openFormQuery = "UPDATE Form222 SET STATUS = 'Printed', ClosedDate = null WHERE DEAFormNumber IN (" + string.Join(",", closedForms) + ")";
                  cloudEntities.Database.ExecuteSqlCommand(openFormQuery);
                  cloudEntities.SaveChanges();
               }

               // get information from item table under entered RA
               var sqlItemQuery = "select itm.ItemGUID as GUID, convert(nvarchar, Product.NDC) as NDC, " +
                                  " case Product.NdcInnerPack when 1 then convert(nvarchar, outerPrdct.NDC) else null end as OuterNDC, Product.NDCInnerPack as NdcInnerPack, " +
                                  " case itm.ItemStateCode when 'O' then itm.Quantity when 'C' then Product.PackageSize * itm.CaseSize else Product.PackageSize end as ItemQty, " +
                                  " cndt.Description as Condition, Product.UnitOfMeasure as ItemUnit " +
                                  " from item itm " +
                                  " join Product on itm.ProductID = Product.ProductID " +
                                  " join ItemStateDict cndt on itm.ItemStateCode = cndt.Code " +
                                  " left join product outerPrdct on outerPrdct.ProductID = Product.RollupProductID " +
                                  " where itm.IsDeleted = 0 and ItemGUID in (select ItemGUID from WrhsContrItemRel where WrhsContrID in " +
                                  " (select WrhsContrID from WrhsContrTrackingRel where TrackingID in " +
                                  " (select trackingid from tracking where AssocID = @raId)) " +
                                  " and WrhsInstID in (select WrhsInstID from WrhsContrTrackingRel where TrackingID in " +
                                  " (select trackingid from tracking where AssocID = @raId)) and IsDeleted = 0) " +
                                  " order by NDC, ItemUnit desc";

               var itemLst = cloudEntities.Database.SqlQuery<WrhsCtrl2Model>(sqlItemQuery, new SqlParameter("@raId", raId)).ToList();
               var usedItems = new List<WrhsCtrl2Model>();

               // Exact Matches
               if (itemLst.Any()) // if we have items we are trying to match with form222 rows
               {
                  // get item quantity and form quantity totals, so we can accurately account for each ML or pill as we're applying items with lines on a DEA Form
                  form222Lst.ForEach(t =>
                  {
                     t.FormQtyRemaining = t.FormQty;
                     t.ItemQty = form222MatchedLst.FirstOrDefault(x => x.GUID.HasValue && t.GUID.HasValue && x.GUID.ToString() == t.GUID.ToString())?.ItemQty;
                  });
                  itemLst.ForEach(t => t.ItemQtyRemaining = t.ItemQty);

                  //grab all approved items and apply the proper quantities that were captured during calculation
                  //this ensures us that we won't get any already-approved quantities as part of a recalculation
                  foreach (var approved in form222MatchedLst)
                  {
                     if (approved.GUID.HasValue)
                     {
                        var matchedForms = form222Lst.Where(x => approved.GUID.ToString() == x.GUID.ToString()).ToList();

                        foreach (var match in matchedForms)
                        {
                           match.Approved = true;

                           if (approved.DEA == match.DEA && approved.LineNo == match.LineNo)
                           {
                              match.AppliedQty = approved.AppliedQty;
                              match.Condition = approved.Condition;
                           }
                        }

                        var matchedItems = itemLst.Where(x => approved.GUID.ToString() == x.GUID.ToString()).ToList();

                        foreach (var match in matchedItems)
                        {
                           match.Approved = true;
                           match.ItemQty = approved.GUID.ToString() == match.GUID.ToString() ? approved.ItemQty : match.ItemQty;
                        }
                     }
                     else
                     {
                        var noItemsRcvd = form222Lst.Where(x => !approved.GUID.HasValue && x.DEA == approved.DEA && x.LineNo == approved.LineNo).ToList();

                        foreach (var match in noItemsRcvd)
                        {
                           match.Approved = true;
                        }
                     }
                  }

                  //Using the AppliedQty from the previous statements, subtract those quantities from the form to get the remaining Qty on the form
                  foreach (var line in form222Lst.Where(x => x.DEA != "-1" && x.LineNo > 0 && x.GUID.HasValue).GroupBy(c => new { c.DEA, c.LineNo }).ToList())
                  {
                     var deaFormNumber = line.Key.DEA;
                     var lineNumber = line.Key.LineNo;
                     var matchedForms = form222Lst.Where(c => c.DEA == deaFormNumber && c.LineNo == lineNumber && c.FormQtyRemaining > 0).ToList();

                     if (matchedForms.Any())
                     {
                        var formQty = matchedForms.FirstOrDefault(c => c.DEA == deaFormNumber && c.LineNo == lineNumber).FormQty;
                        var totalQtyApplied = matchedForms.Sum(c => c.AppliedQty ?? 0);
                        var remainingFormQty = formQty - totalQtyApplied;

                        if (remainingFormQty < 0)
                        {
                           remainingFormQty = 0;
                        }

                        matchedForms.ForEach(c => c.FormQtyRemaining = remainingFormQty);
                     }
                  }

                  //anywhere that we applied a quantity, update the itemQtyRemaining for the item
                  foreach (var item in itemLst)
                  {
                     var matchedForms = form222Lst.Where(x => x.GUID == item.GUID).ToList();
                     item.ItemQtyRemaining -= matchedForms.Sum(y => y.AppliedQty ?? 0);

                     if (matchedForms.Any())
                     {
                        item.DEA = matchedForms[matchedForms.Count - 1].DEA;
                        item.LineNo = matchedForms[matchedForms.Count - 1].LineNo;
                     }

                     if (item.ItemQtyRemaining <= 0 || item.Approved)
                     {
                        item.ItemQtyRemaining = 0;
                        usedItems.Add(item);
                     }
                  }

                  // looking for exact match with 0 discrepancy
                  SingleRowExactMatch(form222Lst, itemLst, usedItems, curUser);
                  var unUsedItems = itemLst.Except(usedItems).ToList();
                  unUsedItems.Clear();

                  //BELOW IS THE OLD CODE OF DOING MATCHING, WHICH WAS INCORRECT
                  //I'M NOT READY TO DELETE IT JUST YET, SO I'M KEEPING IT
                  //ALL CALCULATIONS WILL BE DONE IN SINGLEROWEXACTMATCH
                  // if we have unmatched item
                  if (unUsedItems.Any())
                  {
                     // if we have unmatched item we are trying to do match with one form and multiple items
                     // and looking for matches with 0 discrepancy
                     MultiplePartialMatches(form222Lst, unUsedItems, usedItems, curUser);

                     //if we still have unmatched records
                     if (itemLst.Except(usedItems).Any())
                     {
                        // we are looking for unmatched
                        unUsedItems = itemLst.Except(usedItems).ToList();
                        MultiplePartialNotMatches(form222Lst, unUsedItems, usedItems, curUser);
                     }
                  }
               }

               var itemguids = form222Lst.Where(c => c.GUID.HasValue).GroupBy(c => c.GUID).ToList();

               //match up a single item GUID to multiple forms; determine if there's a discrepancy or not
               foreach (var guids in itemguids)
               {
                  var matchedForms = form222Lst.Where(c => c.GUID == guids.Key).ToList();

                  if (matchedForms.Any())
                  {
                     var itmsQntity = form222Lst.FirstOrDefault(c => c.GUID == guids.Key).ItemQty;
                     matchedForms.ForEach(c => c.Match = c.Match == 0
                        ? (form222Lst.Where(d => d.GUID == guids.Key).Sum(d => d.FormQty) == itmsQntity ? 1 : 0)
                        : c.Match);
                  }
               }

               var formLinePairs = form222Lst.Where(c => c.GUID.HasValue && c.Match == 0).GroupBy(c => new { c.DEA, c.LineNo }).ToList();

               //match up a single line to multiple items; determine if there's a discrepancy or not
               foreach (var line in formLinePairs)
               {
                  var deaFormNumber = line.Key.DEA;
                  var lineNumber = line.Key.LineNo;
                  var matchedForms = form222Lst.Where(c => c.DEA == deaFormNumber && c.LineNo == lineNumber).ToList();

                  if (matchedForms.Any())
                  {
                     var formQty = form222Lst.FirstOrDefault(c => c.DEA == deaFormNumber && c.LineNo == lineNumber).FormQty;
                     matchedForms.ForEach(c => c.Match = c.Match == 0
                        ? (form222Lst.Where(d => d.DEA == deaFormNumber && d.LineNo == lineNumber).Sum(d => d.ItemQty) == formQty ? 1 : 0)
                        : c.Match);
                  }
               }

               //MatchRate - compare the item quantity to the expected form quantity
               //DEA and LineNo - only make those -1 and 0, respectively, if the item in the list does not have an associated form
               form222Lst.ForEach(m =>
               {
                  m.MatchRate = !string.IsNullOrWhiteSpace(m.GUID.ToString())
                     ? (string.IsNullOrWhiteSpace(m.ItemQty.ToString()) ? 0 : m.ItemQty) - (string.IsNullOrWhiteSpace(m.FormQty.ToString()) ? 0 : m.FormQty)
                     : string.IsNullOrWhiteSpace(m.FormQty.ToString())
                        ? 0
                        : m.FormQty * -1;
                  m.DEA = (string.IsNullOrWhiteSpace(m.DEA) || m.DEA == "-1") && (string.IsNullOrWhiteSpace(m.LineNo.ToString()) || m.LineNo == 0) ? "" : m.DEA;
                  m.LineNo = (string.IsNullOrWhiteSpace(m.DEA) || m.DEA == "-1") && (string.IsNullOrWhiteSpace(m.LineNo.ToString()) || m.LineNo == 0) ? 0 : m.LineNo;
               });

               AddItems222Rel(form222Lst, curUser);

               //determine the container in which the item is currently in
               //it can only be either a black or a white bin
               foreach (var itemBin in form222Lst.Where(x => x.GUID.HasValue).Select(x => x.GUID).Distinct().ToList())
               {
                  var binQuery = "SELECT WrhsContrID FROM WrhsContrItemRel WHERE ItemGUID = @guid AND IsActive = 1 AND IsDeleted = 0";
                  var bin = cloudEntities.Database.SqlQuery<string>(binQuery, new SqlParameter("@guid", itemBin.ToString())).First();
                  var guidItems = form222Lst.Where(y => y.GUID.ToString() == itemBin.ToString()).ToList();

                  if (bin.Length == 4 && bin.StartsWith("4"))
                  {
                     guidItems.ForEach(g => g.WhiteBin = bin);
                  }
                  else if (bin.StartsWith("CT"))
                  {
                     guidItems.ForEach(g => g.BlackBin = bin);
                  }
               }

               return form222Lst.OrderBy(c => c.NDC).ThenBy(c => c.DEA).ThenBy(c => c.LineNo).ToList();
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Matching Details - approve any discrepancies, or edit any quantites that were changed
      /// </summary>
      /// <param name="detailList"></param>
      /// <param name="approvedBy"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveMatchingDetails(List<WrhsCtrl2Model> detailList, string approvedBy, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               foreach (var d in detailList)
               {
                  d.DEA = string.IsNullOrWhiteSpace(d.DEA) && d.LineNo == 0 ? "-1" : d.DEA;
                  d.GUID = string.IsNullOrWhiteSpace(d.GUID.ToString())
                     ? Guid.Parse("00000000-0000-0000-0000-000000000000")
                     : d.GUID;
               }

               foreach (var itemList in detailList)
               {
                  var item222Rl = cloudEntities.Item222Rel.FirstOrDefault(c => c.DEAFormNumber.ToString() == itemList.DEA && c.LineNumber == itemList.LineNo && c.ItemGUID == itemList.GUID);

                  if (item222Rl != null && item222Rl.HasDiscrepancy)
                  {
                     if (itemList.Approved && !item222Rl.ApprovedDate.HasValue)
                     {
                        item222Rl.ApprovedBy = approvedBy;
                        item222Rl.ApprovedDate = DateTime.UtcNow;
                     }

                     item222Rl.ModifiedBy = curUser;
                     item222Rl.ModifiedDate = DateTime.UtcNow;
                     item222Rl.HasDiscrepancy = itemList.Match != 0;

                     var item = cloudEntities.Item.FirstOrDefault(c => c.ItemGUID == itemList.GUID);

                     //we will never change quantities on a sealed or case product
                     if (item != null && item.ItemStateCode == "O")
                     {
                        if ((string.IsNullOrWhiteSpace(item.OrigQuantity.ToString()) || item.OrigQuantity == 0) && item.Quantity != itemList.ItemQty)
                        {
                           item.OrigQuantity = item.Quantity;
                        }

                        item.Quantity = (decimal)itemList.ItemQty;
                     }

                     cloudEntities.SaveChanges();
                  }
               }

               return true;
            }
            catch (Exception ex)
            {
               return false;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Add list of items associated with RA to Item222Rel table
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void AddItems222Rel(List<WrhsCtrl2Model> itemList, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               // this is the last part which we are adding all information in first time into item222Rel table
               // DEA of -1 and LineNo of 0 means we have an item that doesn't match up to any forms (i.e. Extra Item)
               //only add the recalculated items; ignore any approved items
               foreach (var itms in itemList.Where(x => !x.Approved).Distinct().ToList())
               {
                  var item222Rl = new Item222Rel
                  {
                     ItemGUID = string.IsNullOrWhiteSpace(itms.GUID.ToString())
                                    ? Guid.Parse("00000000-0000-0000-0000-000000000000")
                                    : (Guid)itms.GUID,
                     DEAFormNumber = string.IsNullOrWhiteSpace(itms.DEA) && itms.LineNo == 0
                                 ? -1
                                 : Convert.ToInt64(itms.DEA),
                     LineNumber = string.IsNullOrWhiteSpace(itms.DEA) && itms.LineNo == 0
                              ? 0
                              : (int)itms.LineNo,
                     Version = 1,
                     AppliedQty = string.IsNullOrWhiteSpace(itms.GUID.ToString()) ? null : itms.AppliedQty,
                     IsDeleted = false,
                     CreatedDate = DateTime.UtcNow,
                     CreatedBy = curUser,
                     HasDiscrepancy = itms.Match != 1
                  };

                  cloudEntities.Item222Rel.Add(item222Rl);
                  cloudEntities.SaveChanges();
               }
            }
            catch (Exception ex)
            {
               throw ex;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Add unmatched items to list which associated with RA
      /// </summary>
      /// <param name="form222Lst"></param>
      /// <param name="itemLst"></param>
      /// <param name="usedItems"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void MultiplePartialNotMatches(List<WrhsCtrl2Model> form222Lst, List<WrhsCtrl2Model> itemLst, List<WrhsCtrl2Model> usedItems, string curUser)
      {
         var itemLists = new List<WrhsCtrl2Model>();
         // like previous functionality again we are grouping by ndc
         var ndcGroups = itemLst.Except(usedItems).GroupBy(c => c.NDC).ToList();
         foreach (var itmLst in ndcGroups)
         {
            var unUsed = itemLst.Except(usedItems).ToList();

            if (!unUsed.Any())
            {
               break;
            }

            var lsts = unUsed.Where(c => c.NDC == itmLst.Key).ToList();
            itemLists.Clear();
            var form222Cnt = form222Lst.Count(c => c.NDC == itmLst.Key
                                                   && !c.GUID.HasValue
                                                   && c.Match == 0);
            if (lsts.Count == form222Cnt || (form222Cnt == 1 && lsts.Count > 1))
            {
               // we are looking for empty guid and not matched
               var itmForm = form222Lst.Where(c => c.NDC == itmLst.Key
                                                   && !c.GUID.HasValue
                                                   && c.Match == 0).OrderBy(c => c.NDC).ToList();

               if (form222Cnt == 1 && lsts.Count > 1)
               {
                  // if we have one form with empty item guid and we have multiple items from item table
                  // with tha same ndc
                  // we are adding this multiple items to that one form
                  // for that we need to repeat form
                  itmForm[0].ItemQty = lsts[0].ItemQty;
                  itmForm[0].GUID = lsts[0].GUID;
                  itmForm[0].ItemUnit = lsts[0].ItemUnit;
                  itmForm[0].Condition = lsts[0].Condition;
                  itmForm[0].Match = lsts.GroupBy(c => c.ItemUnit).Count() == 1 ? 0 : 2;
                  usedItems.Add(lsts[0]);

                  for (int i = 1; i < lsts.Count; i++)
                  {
                     lsts[i].FormQty = itmForm[0].FormQty;
                     lsts[i].LineNo = itmForm[0].LineNo;
                     lsts[i].DEA = itmForm[0].DEA;
                     lsts[i].NDC = itmForm[0].NDC;
                     lsts[i].Unit = itmForm[0].Unit;
                     lsts[i].Match = lsts.GroupBy(c => c.ItemUnit).Count() == 1 ? 0 : 2;
                     lsts[i].Orders = itmForm[0].Orders;
                     form222Lst.Add(lsts[i]);
                     usedItems.Add(lsts[i]);
                     //AddItems222rel(itm_form[i], curUser);
                  }
               }
               else
               {
                  // we don't have exact  matches,
                  //but we found  forms and items which we can doing match with discrepancy
                  for (int i = 0; i < lsts.Count; i++)
                  {
                     itmForm[i].ItemQty = lsts[i].ItemQty;
                     itmForm[i].GUID = lsts[i].GUID;
                     itmForm[i].ItemUnit = lsts[i].ItemUnit;
                     itmForm[i].Condition = lsts[i].Condition;
                     itmForm[i].Match = lsts.GroupBy(c => c.ItemUnit).Count() == 1 ? 0 : 2;
                     usedItems.Add(lsts[i]);
                     //AddItems222rel(itm_form[i], curUser);
                  }
               }
            }
            else
            {
               foreach (var lstItems in lsts)
               {
                  //
                  var itmForm = form222Lst.Where(c => c.NDC == lstItems.NDC
                                                      && !c.GUID.HasValue
                                                      && c.Match == 0
                                                      && c.Unit == lstItems.ItemUnit).OrderBy(c => c.NDC).ToList();

                  // forms with empty guids
                  // and no matter what you have
                  // just adding items to this forms
                  // one by one
                  if (itmForm.Any())
                  {
                     var i = 0;
                     foreach (var items in itmForm)
                     {
                        i++;
                        items.ItemQty = lstItems.ItemQty;
                        items.GUID = lstItems.GUID;
                        items.ItemUnit = lstItems.ItemUnit;
                        items.Condition = lstItems.Condition;
                        items.Match = i == 1 ? 0 : 2;
                     }

                     usedItems.Add(lstItems);
                  }
                  else
                  {
                     // unmatched records
                     form222Lst.Add(lstItems);
                     usedItems.Add(lstItems);
                  }
               }
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Exact Match from one by one records from different tables
      /// </summary>
      /// <param name="form222Lst"></param>
      /// <param name="itemLst"></param>
      /// <param name="usedItems"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void SingleRowExactMatch(List<WrhsCtrl2Model> form222Lst, List<WrhsCtrl2Model> itemLst, List<WrhsCtrl2Model> usedItems, string curUser)
      {
         foreach (var itm in itemLst)
         {
            // Try to get exact the same match from form222 list with ndc,condition,itemQuantity (example: Line 1 expects an open 20, get an item with EXACTLY open of 20)
            var itms = form222Lst.FirstOrDefault(c => c.NDC == itm.NDC
                                                      && c.F2Condition == itm.Condition
                                                      && c.FormQty == itm.ItemQty
                                                      && c.Unit == itm.ItemUnit
                                                      && c.Match == 0);

            if (itms == null)
            {
               continue;
            }

            // if find some, just edit the item quantity and guid and unit and match=1 to know this is matched
            itms.Condition = itm.Condition;
            itms.ItemQty = itm.ItemQty;
            itms.GUID = itm.GUID;
            itms.BlackBin = itm.BlackBin;
            itms.WhiteBin = itm.WhiteBin;
            itms.ItemUnit = itm.ItemUnit;
            itms.Match = 1;

            itms.FormQtyRemaining -= itm.ItemQty;
            itm.ItemQtyRemaining = itm.ItemQty;
            itm.AppliedQty = itm.ItemQty;
            itms.AppliedQty = itm.ItemQty;

            usedItems.Add(itm);
         }

         var remainingItems = itemLst.Except(usedItems).ToList();
         var remainingForms = form222Lst.Where(c => c.Match == 0).ToList();
         var f222 = new List<WrhsCtrl2Model>();
         f222.AddRange(remainingForms.Where(x => x.Approved).ToList());

         //here, we will assign lines to items that have less or equal item quantity than the line quantity allows
         //for example, we can match up an item of 4 and an item of 6 quantity to a line that's asking for 10 or more
         //any items that have a bigger quantity than a form asks for will not be considered here
         foreach (var form in remainingForms)
         {
            var itemLists = new List<WrhsCtrl2Model>();
            var remainingQtyForm = form.FormQtyRemaining;

            if (remainingQtyForm <= 0)
            {
               continue;
            }

            foreach (var item in remainingItems)
            {
               var newItem = new WrhsCtrl2Model();

               /* Here are the following checks below:
                * 1. If both the form NDC and the processed item NDC are "regular" products (no inner-outer) then we expect the NDCs to match
                * 2. If both the form NDC and the processed item NDC are inner packs and have a rollup NDC, then we expect those inner NDCs to match
                * 3. If the form NDC is an outer and the processed item NDC is an inner, then the form NDC should match the item's outer NDC
                * 4. If the form NDC is an inner and the processed item NDC is an outer, then the form's Outer NDC should match the item NDC
                */
               if ((!form.NdcInnerPack && !item.NdcInnerPack && form.NDC != item.NDC) ||
                   (form.NdcInnerPack && item.NdcInnerPack && form.NDC != item.NDC) ||
                   (!form.NdcInnerPack && item.NdcInnerPack && form.NDC != item.OuterNDC) ||
                   (form.NdcInnerPack && !item.NdcInnerPack && form.OuterNDC != item.NDC))
               {
                  continue;
               }

               var itemRemainingQty = item.ItemQtyRemaining;

               if (itemRemainingQty > 0 && remainingQtyForm >= itemRemainingQty)
               {
                  newItem.DEA = form.DEA;
                  newItem.LineNo = form.LineNo;
                  newItem.FormQty = form.FormQty;
                  newItem.NDC = item.OuterNDC ?? item.NDC;
                  newItem.GUID = item.GUID;
                  newItem.BlackBin = item.BlackBin;
                  newItem.WhiteBin = item.WhiteBin;
                  newItem.ItemUnit = item.ItemUnit;
                  newItem.Unit = item.ItemUnit;
                  newItem.Condition = item.Condition;
                  newItem.ItemQtyRemaining = item.ItemQtyRemaining;

                  if (itemRemainingQty > remainingQtyForm && remainingQtyForm > 0)
                  {
                     newItem.ItemQty = item.ItemQty;
                     newItem.AppliedQty = remainingQtyForm;

                     item.DEA = form.DEA;
                     item.LineNo = form.LineNo;
                     form.ItemQty += form.Approved ? 0 : remainingQtyForm;
                     item.AppliedQty = remainingQtyForm;
                     itemRemainingQty -= remainingQtyForm;
                     form.FormQtyRemaining -= remainingQtyForm;
                     remainingQtyForm = 0;
                     item.ItemQtyRemaining = itemRemainingQty;
                     form.ItemUnit = item.ItemUnit;
                     form.Condition = form.Approved ? form.Condition : item.Condition;
                  }
                  else
                  {
                     newItem.ItemQty = item.ItemQty;
                     newItem.AppliedQty = itemRemainingQty;

                     item.DEA = form.DEA;
                     item.LineNo = form.LineNo;
                     form.ItemQty += form.Approved ? 0 : itemRemainingQty;
                     item.AppliedQty = itemRemainingQty;
                     remainingQtyForm -= itemRemainingQty;
                     form.FormQtyRemaining -= itemRemainingQty;
                     itemRemainingQty -= itemRemainingQty;
                     item.ItemQtyRemaining = itemRemainingQty;
                     form.ItemUnit = item.ItemUnit;
                     form.Condition = form.Approved ? form.Condition : item.Condition;
                  }

                  itemLists.Add(item);
                  f222.Add(newItem);
               }
            }

            var x = 0;
            foreach (var itemForm in itemLists)
            {
               x++;

               if (itemForm.ItemQtyRemaining <= 0)
               {
                  usedItems.Add(itemForm);
               }

               if ((itemLists.Count == 1 || x == 1) && !form.Approved)
               {
                  form.ItemQty = itemForm.ItemQty;
                  form.AppliedQty = itemForm.AppliedQty;
                  form.ItemUnit = itemForm.ItemUnit;
                  form.GUID = itemForm.GUID;
                  form.BlackBin = itemForm.BlackBin;
                  form.WhiteBin = itemForm.WhiteBin;
                  form.Condition = itemForm.Condition;
               }
               else if (x > 1 || form.Approved)
               {
                  itemForm.DEA = form.DEA;
                  itemForm.LineNo = form.LineNo;
                  itemForm.NDC = form.NDC;
                  itemForm.FormQty = form.FormQty;
                  itemForm.Unit = form.Unit;
                  form222Lst.Add(itemForm);
               }
            }
         }

         remainingItems = itemLst.Except(usedItems).ToList();
         remainingForms = form222Lst.Where(c => c.Match == 0 && c.FormQtyRemaining > 0).ToList();

         //here we will take a look at items with possible overages
         //for example, it's possible here to match up a single item of 30 qty to a line that is asking for 10 qty (multiple rows to an item)
         foreach (var form in remainingForms)
         {
            var itemLists = new List<WrhsCtrl2Model>();
            var remainingQtyForm = form.FormQtyRemaining;

            if (remainingQtyForm <= 0)
            {
               continue;
            }

            foreach (var item in remainingItems)
            {
               var newItem = new WrhsCtrl2Model();

               /* Here are the following checks below:
                * 1. If both the form NDC and the processed item NDC are "regular" products (no inner-outer) then we expect the NDCs to match
                * 2. If both the form NDC and the processed item NDC are inner packs and have a rollup NDC, then we expect those inner NDCs to match
                * 3. If the form NDC is an outer and the processed item NDC is an inner, then the form NDC should match the item's outer NDC
                * 4. If the form NDC is an inner and the processed item NDC is an outer, then the form's Outer NDC should match the item NDC
                */
               if ((!form.NdcInnerPack && !item.NdcInnerPack && form.NDC != item.NDC) ||
                   (form.NdcInnerPack && item.NdcInnerPack && form.NDC != item.NDC) ||
                   (!form.NdcInnerPack && item.NdcInnerPack && form.NDC != item.OuterNDC) ||
                   (form.NdcInnerPack && !item.NdcInnerPack && form.OuterNDC != item.NDC))
               {
                  continue;
               }

               var itemRemainingQty = item.ItemQtyRemaining;

               if (itemRemainingQty > 0 && remainingQtyForm > 0)
               {
                  newItem.DEA = form.DEA;
                  newItem.LineNo = form.LineNo;
                  newItem.FormQty = form.FormQty;
                  newItem.NDC = item.OuterNDC ?? item.NDC;
                  newItem.GUID = item.GUID;
                  newItem.BlackBin = item.BlackBin;
                  newItem.WhiteBin = item.WhiteBin;
                  newItem.ItemUnit = item.ItemUnit;
                  newItem.Unit = item.ItemUnit;
                  newItem.Condition = item.Condition;
                  newItem.ItemQtyRemaining = item.ItemQtyRemaining;

                  if (itemRemainingQty > remainingQtyForm && remainingQtyForm > 0)
                  {
                     newItem.ItemQty = item.ItemQty;
                     newItem.AppliedQty = remainingQtyForm;

                     item.DEA = form.DEA;
                     item.LineNo = form.LineNo;
                     form.ItemQty += form.Approved ? 0 : remainingQtyForm;
                     item.AppliedQty = remainingQtyForm;
                     itemRemainingQty -= remainingQtyForm;
                     form.FormQtyRemaining -= remainingQtyForm;
                     remainingQtyForm = 0;
                     item.ItemQtyRemaining = itemRemainingQty;
                     form.ItemUnit = item.ItemUnit;
                     form.Condition = form.Approved ? form.Condition : item.Condition;
                  }
                  else
                  {
                     newItem.ItemQty = item.ItemQty;
                     newItem.AppliedQty = itemRemainingQty;

                     item.DEA = form.DEA;
                     item.LineNo = form.LineNo;
                     form.ItemQty += form.Approved ? 0 : itemRemainingQty;
                     item.AppliedQty = itemRemainingQty;
                     remainingQtyForm -= itemRemainingQty;
                     form.FormQtyRemaining -= itemRemainingQty;
                     itemRemainingQty -= itemRemainingQty;
                     item.ItemQtyRemaining = itemRemainingQty;
                     form.ItemUnit = item.ItemUnit;
                     form.Condition = form.Approved ? form.Condition : item.Condition;
                  }

                  itemLists.Add(item);
                  f222.Add(newItem);
               }
            }

            var x = 0;
            foreach (var itemForm in itemLists)
            {
               x++;

               if (itemForm.ItemQtyRemaining <= 0)
               {
                  usedItems.Add(itemForm);
               }

               if ((itemLists.Count == 1 || x == 1) && !form.Approved)
               {
                  form.ItemQty = itemForm.ItemQty;
                  form.AppliedQty = itemForm.AppliedQty;
                  form.ItemUnit = itemForm.ItemUnit;
                  form.GUID = itemForm.GUID;
                  form.BlackBin = itemForm.BlackBin;
                  form.WhiteBin = itemForm.WhiteBin;
                  form.Condition = itemForm.Condition;
               }
               else if (x > 1 || form.Approved)
               {
                  itemForm.DEA = form.DEA;
                  itemForm.LineNo = form.LineNo;
                  itemForm.NDC = form.NDC;
                  itemForm.FormQty = form.FormQty;
                  itemForm.Unit = form.Unit;
                  form222Lst.Add(itemForm);
               }
            }
         }

         var noMatch = form222Lst.Where(x => !f222.Any(y => y.DEA == x.DEA && y.LineNo == x.LineNo)).ToList();
         remainingItems = itemLst.Except(usedItems).ToList().Where(x => !f222.Any(y => y.DEA == x.DEA && y.LineNo == x.LineNo)).ToList();
         noMatch.AddRange(remainingItems);
         f222.AddRange(noMatch);
         form222Lst.Clear();
         form222Lst.AddRange(f222);
         return;

         //OLD REMATCHING CODE
         //DIDN'T WORK PROPERLY, BUT I'M KEEPING IT HERE FOR NOW JUST IN CASE
         //DMB
         // match up a single item against the sum of all lines for a matching NDC
         foreach (var itm in remainingItems)
         {
            // Here we are determine we have one item and different form matches
            // first we are grouping by ndc and unit
            var groups = form222Lst.Where(c => c.NDC == itm.NDC).GroupBy(c => new { c.NDC, c.Unit }).ToList();
            var itemLists = new List<WrhsCtrl2Model>();

            foreach (var frm in groups)
            {
               // we are looking for grouped ndc and unit and 0 matches
               // why 0 matches: because maybe we have match for one item when we tried to
               // get exact matches, we don't need to try to group exact matches with 0 discrepancy
               // and processed before
               var listForm = form222Lst.Where(c => c.Unit == frm.Key.Unit
                                                   && c.NDC == frm.Key.NDC
                                                   && c.Match == 0).ToList();

               //listForm.ForEach(t => t.FormQtyRemaining = listForm.Sum(x => x.FormQty));

               // doing calculation and clear item list when we get match items or move for another group
               // itemLists is list of temporary items that we are using for calculation
               double sumVal = -1;
               itemLists.Clear();

               // Example :  we have item 1 which has 100 Qnty
               // here we are trying to find forms associated with that item
               // if formRow1+formRow2+formRow3 = 100 Qnty => Exact Match
               // if formRow1+formRow2+formRow3 < 100 Qnty => We are trying to match together but with discrepancy
               foreach (var frms in form222Lst)
               {
                  // has fixed itemQauntity and trying to do Sum of formQuantity
                  if (sumVal < Convert.ToDouble(itm.ItemQty) && (sumVal == -1 ? 0 : sumVal + Convert.ToDouble(frms.FormQty)) <= Convert.ToDouble(itm.ItemQty))
                  {
                     sumVal = (sumVal == -1 ? 0 : sumVal) + Convert.ToDouble(frms.FormQty);
                     itemLists.Add(frms);
                  }
                  else if (sumVal > 0)
                  {
                     // trying remove like FIFO from the list and sum with new quantity
                     if (Convert.ToDouble(frms.FormQty) <= Convert.ToDouble(itm.ItemQty))
                     {
                        var itemListExcept = new List<WrhsCtrl2Model>();

                        foreach (var listItems in itemLists)
                        {
                           itemListExcept.Add(listItems);

                           if (Convert.ToDouble(frms.FormQty) + Convert.ToDouble(itemLists.Except(itemListExcept).Sum(c => c.FormQty)) <= Convert.ToDouble(itm.ItemQty))
                           {
                              break;
                           }
                        }

                        if (itemListExcept.Any())
                        {
                           sumVal = (Convert.ToDouble(frms.FormQty) + Convert.ToDouble(itemLists.Except(itemListExcept).Sum(c => c.FormQty)));
                           itemLists = itemLists.Except(itemListExcept).ToList();
                           itemLists.Add(frms);
                        }
                        else
                        {
                           sumVal = Convert.ToDouble(frms.FormQty) <= Convert.ToDouble(itm.ItemQty)
                                             ? Convert.ToDouble(frms.FormQty)
                                             : 0;
                           itemLists.Clear();
                           itemLists.Add(frms);
                        }
                     }
                     else
                     {
                        sumVal = -1;
                        itemLists.Clear();
                     }
                  }
               }

               if (sumVal == Convert.ToDouble(itm.ItemQty) && sumVal == Convert.ToDouble(itemLists.Sum(c => c.FormQty)))
               {
                  var i = 0;
                  if ((itemLists.Count == 1
                     && (itemLists[0].F2Condition == itm.Condition
                     || (itemLists[0].F2Condition == "Sealed" && itm.Condition == "Opened")))
                     || itemLists.Count > 1)
                  {
                     foreach (var items in itemLists)
                     {
                        i++;
                        var frms = form222Lst.FirstOrDefault(c => c.Unit == items.Unit && c.NDC == items.NDC
                                                                                      && c.DEA == items.DEA
                                                                                      && c.Match == 0
                                                                                      && c.LineNo == items.LineNo
                                                                                      && c.FormQty == items.FormQty
                                                                                      && c.F2Condition == items.F2Condition);

                        frms.Match = itemLists.Count > 1 && i > 1 ? 3 : 1;
                        frms.ItemQty = itm.ItemQty;
                        frms.GUID = itm.GUID;
                        frms.ItemUnit = itm.ItemUnit;
                        frms.Condition = itm.Condition;
                     }

                     usedItems.Add(itm);
                  }

                  sumVal = 0;
                  break;
               }
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine if we have a valid C2 Black bin scanned
      /// </summary>
      /// <param name="blackBin"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ContainerInstance CheckBlackBin(string blackBin)
      {
         var protoContainer = SidebarCommon.GetContainer(blackBin);

         if (protoContainer == null)
         {
            throw new Exception("The requested container could not be found");
         }

         if (protoContainer.Type != Utility.Constants.BlackSortBin || protoContainer.TypeGroup != Utility.Constants.Bin)
         {
            throw new Exception($"Container must be a {Utility.Constants.Bin} ({Utility.Constants.BlackSortBin})");
         }

         if (protoContainer.Status == "A")
         {
            throw new Exception($"Container is currently not being used"); //Status = Available i.e. it's currently not being used in processing
         }

         if (protoContainer.Attribute == "I")
         {
            throw new Exception($"Container is currently in Final Sort"); //Attribute = Inbound i.e. it's currently attached or has been attached as an Inbound container in Final Sort
         }

         return protoContainer.ContainerInstance;
      }

      ///****************************************************************************
      /// <summary>
      ///   Move all the C2 items for a given RA from their respective white bins to the scanned black bin
      /// </summary>
      /// <param name="rtrnAuthId"></param>
      /// <param name="blackBin"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<Guid, string> MoveC2Items(string rtrnAuthId, ContainerInstance blackBin, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var movedItems = new Dictionary<Guid, string>();
            //get the list of our items tied to the given RA
            var itemQuery = "select itm.ItemGUID as GUID from item itm where itm.IsDeleted = 0 " +
                            " and ItemGUID in (select ItemGUID from WrhsContrItemRel where WrhsContrID in " +
                            " (select WrhsContrID from WrhsContrTrackingRel where TrackingID in (select trackingid from tracking where AssocID = @raId)) " +
                            " and WrhsInstID in (select WrhsInstID from WrhsContrTrackingRel where TrackingID in (select trackingid from tracking where AssocID = @raId)) " +
                            " and IsDeleted = 0) ";
            var itemLst = cloudEntities.Database.SqlQuery<Guid>(itemQuery, new SqlParameter("@raId", rtrnAuthId)).ToList();
            var context = cloudEntities.Database.BeginTransaction();

            foreach (var item in itemLst)
            {
               try
               {
                  var contrItemRel = cloudEntities.WrhsContrItemRel.FirstOrDefault(x => x.ItemGUID.ToString() == item.ToString() && x.IsActive && !x.IsDeleted && x.WrhsContrID.ToUpper() != blackBin.ContainerId.ToUpper());

                  //get the currently-active bin for the item
                  //if it's in a white bin, move it to the scanned black bin
                  if (contrItemRel != null && !contrItemRel.WrhsContrID.StartsWith("CT"))
                  {
                     //first we update the active white bin to INACTIVE
                     contrItemRel.IsActive = false;
                     contrItemRel.ModifiedDate = DateTime.UtcNow;

                     //then we generate a new entry in the WrhsContrItemRel with the scanned black bin
                     var newContrItemRel = new WrhsContrItemRel
                     {
                        WrhsContrID = blackBin.ContainerId,
                        WrhsInstID = blackBin.InstanceId,
                        ItemGUID = contrItemRel.ItemGUID,
                        IsActive = true,
                        IsDeleted = false,
                        CreatedBy = curUser,
                        CreatedDate = DateTime.UtcNow,
                        Version = 1
                     };

                     cloudEntities.WrhsContrItemRel.Add(newContrItemRel);
                     cloudEntities.SaveChanges();
                     movedItems.Add(contrItemRel.ItemGUID, contrItemRel.WrhsContrID);
                  }
               }
               catch (Exception e)
               {
                  context.Rollback();
                  throw e;
               }
            }

            context.Commit();
            return movedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Complete all forms - attempt to close any forms that have no more discrepancies
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void Form222CompleteAllForms(string raNumber)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var ra = 0;
            int.TryParse(raNumber, out ra);

            var deaForms = cloudEntities.Form222.Where(f => f.Form222Source == "0" && f.Form222SourceID == ra && f.Status == "Printed").ToList();

            foreach (var form in deaForms)
            {
               var formDetails = cloudEntities.Form222Detail.Where(fd => fd.DEAFormNumber == form.DEAFormNumber).ToList();
               var query = "select isnull(FormQty, 0) - isnull(SumProcessedQuantity, 0) from " +
                           " (select CAST(CASE WHEN frm222Details.SealedOpenCase = 'Opened' AND product.UnitsPerPackage > 1 " +
                           " THEN (frm222Details.OriginalNbrofPackages * (product.PackageSize / product.UnitsPerPackage)) " +
                           " ELSE (frm222Details.OriginalNbrofPackages * frm222Details.PackageSize) END AS DECIMAL(18,2)) as FormQty, " +
                           " (select sum(isnull(itemRel.AppliedQty, 0)) from Item222Rel itemRel where frm222Details.DEAFormNumber = itemRel.DEAFormNumber and frm222Details.LineNumber = itemRel.LineNumber) SumProcessedQuantity " +
                           " from Form222Detail frm222Details left outer join Item222Rel itemRel on frm222Details.DEAFormNumber = itemRel.DEAFormNumber and frm222Details.LineNumber = itemRel.LineNumber " +
                           " inner join productlink AS PD ON frm222Details.ProductID = PD.qipsproductid left join product product on PD.ProductID = product.ProductID " +
                           " left join product outerPrdct on outerPrdct.ProductID = product.RollupProductID where frm222Details.DEAFormNumber = @dea) " +
                           " A order by abs(isnull(FormQty, 0) - isnull(SumProcessedQuantity, 0))";

               //this gets all of the lines on a given form, and it should return the last entry
               //if it's a non-zero then it means at least one line still has a discrepancy, and that we cannot close the form yet
               var qtyDifference = cloudEntities.Database.SqlQuery<decimal>(query, new SqlParameter("@dea", form.DEAFormNumber)).LastOrDefault();

               if (qtyDifference == 0)
               {
                  form.Status = "Closed";
                  form.ClosedDate = DateTime.UtcNow;

                  foreach (var fd in formDetails)
                  {
                     var fdQty = cloudEntities.Item222Rel.Where(i => i.DEAFormNumber == fd.DEAFormNumber && i.LineNumber == fd.LineNumber).Sum(q => q.AppliedQty ?? 0);
                     fd.QuantityReceived = fdQty;
                     fd.ReceivedDate = DateTime.UtcNow;
                  }

                  cloudEntities.SaveChanges();
               }
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Multiple row from one list matched with one row from another list
      /// </summary>
      /// <param name="form222Lst"></param>
      /// <param name="itemLst"></param>
      /// <param name="usedItems"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void MultiplePartialMatches(List<WrhsCtrl2Model> form222Lst, List<WrhsCtrl2Model> itemLst, List<WrhsCtrl2Model> usedItems, string curUser)
      {
         // one form and multiple items
         // first thing is important here is grouping by ndc and unit
         var groups = form222Lst.GroupBy(c => new { c.NDC, c.Unit }).ToList();
         var itemLists = new List<WrhsCtrl2Model>();

         foreach (var frm in groups)
         {
            if (!itemLst.Except(usedItems).Any())
            {
               // since we get done with unmatched items we are going to show data
               break;
            }
            // we are looking for grouped ndc and unit and 0 matches
            // why 0 matches: because maybe we have match for one item when we tried to
            // get exact matches, we don't need to try to group exact matches with 0 discrepancy
            // and processed before
            var listForm = form222Lst.Where(c => c.Unit == frm.Key.Unit
                                                 && c.NDC == frm.Key.NDC
                                                 && c.Match == 0).ToList();

            foreach (var frms in listForm)
            {
               double sumVal = 0;
               itemLists.Clear();

               // doing calculation and clear item list when we get match items or move for another group
               // itemLists is list of temporary items that we are using for calculation
               if (!itemLst.Except(usedItems).Any())
               {
                  break;
               }

               foreach (var itmLst in itemLst.Except(usedItems).ToList())
               {
                  // pick items one by one and compare with next quantity
                  // doing this process until get more quantity or equal quantity
                  if (sumVal < Convert.ToDouble(itmLst.ItemQty) && (sumVal + Convert.ToDouble(frms.FormQty)) <= Convert.ToDouble(itmLst.ItemQty))
                  {
                     // since it's lower than form quantity we are doing sum and adding into itemLists
                     sumVal += Convert.ToDouble(frms.FormQty);
                     itemLists.Add(itmLst);
                  }
                  else if (sumVal > 0)
                  {
                     // if formQuantity is lower than current itemQuantity or equal
                     // it could be sumVal > = than current itemQuantity
                     if (Convert.ToDouble(frms.FormQty) <= Convert.ToDouble(itmLst.ItemQty))
                     {
                        var itemListExcept = new List<WrhsCtrl2Model>();

                        // here we are processing itemlists that we added before
                        // we are trying to understand which record is reason to be greater that form quantity
                        // we are cutting the first record from list and trying to see sum form quantity value is lower than 
                        // current item quantity
                        foreach (var listItems in itemLists)
                        {
                           itemListExcept.Add(listItems);
                           if ((Convert.ToDouble(frms.FormQty) + Convert.ToDouble(itemLists.Except(itemListExcept).Sum(c => c.FormQty))) <= Convert.ToDouble(itmLst.ItemQty))
                           {
                              break;
                           }
                        }

                        // we are changing itemlist and adding new quantities from new rows
                        // if we got matches
                        if (itemListExcept.Any())
                        {
                           sumVal = (Convert.ToDouble(frms.FormQty) + Convert.ToDouble(itemLists.Except(itemListExcept).Sum(c => c.FormQty)));
                           itemLists = itemLists.Except(itemListExcept).ToList();
                           itemLists.Add(frms);
                        }
                        else
                        {
                           // if not we just clearing all itemlists and going for another process
                           sumVal = Convert.ToDouble(frms.FormQty) <= Convert.ToDouble(itmLst.ItemQty)
                                             ? Convert.ToDouble(frms.FormQty)
                                             : 0;
                           itemLists.Clear();
                           itemLists.Add(frms);
                        }
                     }
                     else
                     {
                        sumVal = -1;
                        itemLists.Clear();
                     }
                  }
               }

               if (sumVal == Convert.ToDouble(frms.FormQty))
               {
                  // sometimes formquantity is 0 and we don't have any sum value
                  // for avoid to matching in this situation we are comparing with itemLists.Count
                  var i = 0;
                  if ((itemLists.Count == 1
                     && (itemLists[0].Condition == frms.F2Condition
                     || (frms.F2Condition == "Sealed" && itemLists[0].Condition == "Opened")))
                     || itemLists.Count > 0)
                  {
                     // we are looking for same condition,
                     // or if we have sealed condition in form
                     // and also we have opened condition in item,
                     // we can do process ( for more information please look at user story )
                     foreach (var items in itemLists)
                     {
                        i++;
                        // in each process we are adding into usedItems list to know how many unmatched
                        // items we have to continue processing or no
                        usedItems.Add(items);

                        // Match = 3 means we have multiple items for one form
                        if (i > 1)
                        {
                           items.DEA = frms.DEA;
                           items.LineNo = frms.LineNo;
                           items.NDC = frms.NDC;
                           items.FormQty = frms.FormQty;
                           items.Unit = frms.Unit;
                           items.Match = 3;
                           form222Lst.Add(items);
                        }
                        else if (itemLists.Count == 1 || i == 1)
                        {
                           frms.ItemQty = items.ItemQty;
                           frms.ItemUnit = items.ItemUnit;
                           frms.GUID = items.GUID;
                           frms.Condition = items.Condition;
                           frms.Match = 1;
                        }
                     }
                  }
               }

               sumVal = 0;
               break;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   A C2 item matched against a form 222, so we need to add its calculation to the item222Rel table
      /// </summary>
      /// <param name="item"></param>
      /// <param name="user"></param>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static bool Form222ReconcileItem(WrhsCtrl2Model item, string user)
      {
         if (item.GUID == Guid.Empty)
         {
            return false;
         }

         AddItems222Rel(new List<WrhsCtrl2Model> { item }, user);
         return true;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }
}
