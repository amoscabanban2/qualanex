﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsCtrl2Controller.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsCtrl2.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.WrhsCtrl2.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;
   using System.Web;
   using Product;
   using Sidebar;
   using Entity;
   using Utility;
   using System.IO;
   using Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_CTRL2")]
   public class WrhsCtrl2Controller : Controller, IQoskController
   {
      private Qosk.Library.Model.UPAModel.UPAApplication _upaApplication;
      ///****************************************************************************
      /// <summary>
      ///   Warehouse Control 2 View.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsCtrl2View()
      {
         this.ModelState.Clear();
         var objReturnLoginUserEntity =
             (ReturnLoginUserEntity)
             QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         int userProfileCode = -99;

         if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
         {
            userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         this._upaApplication = UpaDataAccess.GetUpaModel("WAREHS_CTRL2",
                                                           objReturnLoginUserEntity.UserReturnData.UserID,
                                                            userProfileCode);

         var pageViewId = Guid.NewGuid();
         QoskSessionHelper.SetSessionData(Constants.UpaApplicationKey + pageViewId, this._upaApplication);

         var viewModel = new UpaViewModel
         {
            UpaApplication = this._upaApplication,
            WrhsCtrl2 = new WrhsCtrl2ViewModel
            {
               ActionMenu = WrhsCtrl2Model.GetActionMenu()
            }
         };

         this.ViewBag.pageViewId = pageViewId;

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Warehouse Control 2 Matching Summary.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetSummaryDetails()
      {
         var viewModel = new UpaViewModel
         {
            WrhsCtrl2 = new WrhsCtrl2ViewModel
            {
               Ctrl2Summary = WrhsCtrl2Model.GetCtrl2Summary()
            }
         };

         return this.PartialView("_WrhsCtrl2Summary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Warehouse Control 2 Matching Details.
      /// </summary>
      /// <param name="raNumber"></param>
      /// <param name="raId"></param>
      /// <param name="status"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetMatchingForm222(int raNumber, string raId, int status)
      {
         this.ModelState.Clear();

         var viewModel = new UpaViewModel
         {
            WrhsCtrl2 = new WrhsCtrl2ViewModel
            {
               ActionMenu = WrhsCtrl2Model.GetActionMenu(),
               Ctrl2Matching = WrhsCtrl2Model.GetCtrl2Detail(raNumber, raId, status, this.User.Identity.Name)
            }
         };

         //For all given forms, if the amount of total rows to be returned is equal to the amount of rows that are either approved or have no discrepancy, it means the forms have been completed
         viewModel.WrhsCtrl2.FormCompleted = viewModel.WrhsCtrl2.Ctrl2Matching.Count(x => x.Approved || x.Match == 1) == viewModel.WrhsCtrl2.Ctrl2Matching.Count;

         ViewBag.status = status;
         var jsonResult = this.Json(new { success = true, matching = GetMatchingHtml(viewModel), error = false });
         jsonResult.MaxJsonLength = int.MaxValue;
         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Adding view model to partial view.
      /// </summary>
      /// <param name="viewModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string GetMatchingHtml(UpaViewModel viewModel)
      {
         return this.ConvertViewToString("_WrhsCtrl2Matching", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Converting View to String .
      /// </summary>
      /// <param name="viewModel"></param>
      /// <param name="model"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string ConvertViewToString(string viewName, object model)
      {
         this.ViewData.Model = model;
         using (StringWriter writer = new StringWriter())
         {
            ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
            ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, this.ViewData, new TempDataDictionary(), writer);
            vResult.View.Render(vContext, writer);
            return writer.ToString();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check RA .
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckRA(string raNumber)
      {
         this.ModelState.Clear();

         var blnCheck = WrhsCtrl2Model.GetRaNumberInfo(raNumber);

         return this.Json(new
         {
            success = blnCheck != null,
            Ctrl = blnCheck == null ? "" : blnCheck.CtrlStatus,
            Status = blnCheck == null ? "" : blnCheck.RAStatus,
            RaID = blnCheck == null ? "" : blnCheck.RAID,
            RaNumber = blnCheck == null ? "" : blnCheck.RA,
            error = blnCheck == null
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Warehouse Control 2 Matching Save .
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="approvedBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveMatching(string itemList, string approvedBy)
      {
         this.ModelState.Clear();

         var details = System.Web.Helpers.Json.Decode<List<WrhsCtrl2Model>>(itemList);
         var blnCheck = WrhsCtrl2Model.SaveMatchingDetails(details, approvedBy, this.User.Identity.Name);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Finish C2 Matching process; move remaining items from white bin to the scanned black bin; update C2 forms
      /// </summary>
      /// <param name="raNumber"></param>
      /// <param name="rtrnAuthId"></param>
      /// <param name="blackBin"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult FinishC2Matching(string raNumber, string rtrnAuthId, string blackBin)
      {
         var message = "";
         var blnCheck = new Dictionary<Guid, string>();
         var exception = false;
         var whiteBinList = new List<string>();

         try
         {
            var contrInstance = WrhsCtrl2Model.CheckBlackBin(blackBin); //first we have to check if the Black Bin scanned is valid

            //then we have to move any white bin items to the scanned black bin
            blnCheck = WrhsCtrl2Model.MoveC2Items(rtrnAuthId, contrInstance, this.User.Identity.Name);

            //get distinct white bins
            foreach (var guid in blnCheck.Keys)
            {
               var whiteBin = blnCheck[guid];

               if (!whiteBinList.Contains(whiteBin))
               {
                  whiteBinList.Add(whiteBin);
               } 
            }

            //finally, see if we're able to close any forms for the given RA
            WrhsCtrl2Model.Form222CompleteAllForms(raNumber);
         }
         catch (Exception ex)
         {
            message = ex.Message;
            exception = true;
         }
         
         return this.Json(new
         {
            success = !exception,
            error = exception,
            errorMsg = message,
            itemCount = blnCheck.Count,
            whiteBins = whiteBinList
         });
      }
   }
}
