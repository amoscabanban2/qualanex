///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsCtrl2ViewModel.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsCtrl2.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsCtrl2ViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_CTRL2";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to pull only the first Control 2 record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsCtrl2Model SelectedCtrl2Model { get; set; } = new WrhsCtrl2Model();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Summary list for the Summary detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsCtrl2Model> Ctrl2Summary { get; set; } = new List<WrhsCtrl2Model>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Matching list for the Matching detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsCtrl2Model> Ctrl2Matching { get; set; } = new List<WrhsCtrl2Model>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Determines whether the form(s) is/are completed for the given RA #
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool FormCompleted { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }
}