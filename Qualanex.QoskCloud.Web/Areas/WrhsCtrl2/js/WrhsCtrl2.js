﻿$(function ()
{
   //*****************************************************************************************
   //*
   //* WrhsCtrl2.js
   //*    Copyright (c) 2018, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the Warehouse Control 2 partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************
   var baseURL = "/WrhsCtrl2";
   var stationId = "";
   var machineLocation = "";
   var validRANumber = "";
   var validRAID = "";
   var $htmlMatching = "";
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   var $rowSummary = "";
   // we are adding approved to this list to send to controller approved list
   var approvedList = [];
   var hiddenList = [];
   var editList = [];
   var inputChnge = false;
   var $rowMatching = "";
   var cancel = false;
   var readyStatus = 0;

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).ready(function ()
   {
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
      {
         stationId = data;
      });

      table = window.dtHeaders("#searchGrid_Summary", "scroller:true", ["sorting"], []);
      initialState();
      $htmlMatching = $("#WrhsCtrl2DetailsForm").html();
      $("#searchGrid_Summary td.dataTables_empty ").html("Please wait while data is loading...");
      $rowMatching = $("#WrhsCtrl2DetailsForm").html();
      OnQnexRefresh();
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set up Basic options in the form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function initialState()
   {
      setEditMode(false);
      $("td#tab-container a[href='#tabs-Summary']").click();
      $("#RA").removeAttr("disabled readonly");
      $("#RA").focus();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   saveAndValidate      - Description not available.
   //*   field                - Description not available.
   //*   data                 - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        $("#" + field).val(data);
                     }
               }

               break;
            }

         case "SELECT":
            {
               $("#" + field).val(data);
            }
      }
   }

   //==============================Start ActionBar================================
   //*****************************************************************************
   //*
   //* Summary:
   //*   Refresh function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      $(".formMax .inputEx").val("");
      $("#wait").show();
      $("#Summary-loading").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + baseURL + "/GetSummaryDetails",
         async: true,
         success: function (data)
         {
            replaceSegments($("#WrhsCtrl2GridSummary"), $(data));
            window.dtHeaders("#searchGrid_Summary", "scroller:true", ["sorting"], []);
            $("#Summary-loading").hide();
            $("#tab-container a[href='#" + $("#WrhsCtrl2GridSummary").closest("div.ql-body").attr("id") + "'] span").html($("#tab-container a[href='#" + $("#WrhsCtrl2GridSummary").closest("div.ql-body").attr("id") + "'] span").html().replace("(0)", "(" + $(data).find(".searchGrid_Summary-div-click").length + ")"));
            $("#wait").hide();
         },
         error: function (data)
         {
            $("#Summary-loading").hide();
            $("#wait").hide();

            messageObject = [{
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on Control II Matching Summary request.</h3>"
            }];

            btnObjects = [{
               "id": "btn_ok_error",
               "name": "Ok",
               "class": "btnErrorMessage",
               "style": "margin:0 auto;text-align:center",
               "function": "onclick='$(this).closeMessageBox();'"
            }];

            titleobjects = [{
               "title": "Invalid Entry"
            }];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Submit function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SUBMIT", function ()
   {
      if ($("#RA").val().length > 0 && !getEditMode())
      {
         searchRaInfo($("#RA").val());
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Edit function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      var guid = "";

      if (readyStatus === 2)
      {
         $(".searchGrid_Matching-div-click").each(function ()
         {
            if (($(this).attr("hidden") === undefined || $(this).attr("hidden") === "")
               && ($.trim($(this).find("td[exchange='GUID']").html()).length > 0
                  && $.trim($(this).find("td[exchange='Matching']").attr("exchange-value")) === "0"
                  && ($(this).find("td[exchange=Condition]").attr("exchange-value") === "Opened")
                  && !$(this).find("td[exchange='Approve']").hasClass("approved"))
            )
            {
               if ($.trim($(this).find("td[exchange='GUID']").attr("exchange-value")) !== guid)
               {
                  guid = $.trim($(this).find("td[exchange='GUID']").attr("exchange-value"));
                  $(this).find(".inputMatching").show();
                  $(this).find("label").hide();
               }
            }
         });

         window.setEditMode(true);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Edit the quantity for a given item
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", ".inputMatching", function (e)
   {
      $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
      inputChnge = true;
      //$(this).val($(this).val().replace(/[^0-9]|\.|/, ''));
      $(this).addClass("inputMatchingChange");
      var fltr = [];

      if ($(this).closest("tr").attr("data-parent-id") === undefined)
      {
         //parent - when changed textbox is from parent we are looking for children, 
         //if this parent has child we need to know how many child we have to add to rowspan
         // here we are looking for Match rate value
         if ($(this).closest("tr").find("td[exchange='MatchRate']").attr("exchange-value") === "0")
         {
            // if that's the 0 it means we have matched, but keep in mind for save it you need to 
            // approve this changed for example the record has child and match rate is 0 but child is not approved,
            // it shouldn't record as approved in database , that's why we have a -1 for that to determine 
            // this is not valid
            fltr = approvedList.filter(c => c.RowId === parseFloat($(this).closest("tr").attr("mgmtentity-id")
               || c.ParentRowId === $(this).closest("tr").attr("mgmtentity-id")));
         }
      }
      else
      {
         // changed textbox is child and here we are giving the parent Id and looking for match rate
         // the same scenario that i mentioned in top
         if ($(".searchGrid_Matching-div-click[mgmtentity-id='" + $(this).closest("tr").attr("data-parent-id") + "']").find("td[exchange='MatchRate']").attr("exchange-value") === "0")
         {
            fltr = approvedList.filter(c => c.RowId === parseFloat($(this).closest("tr").attr("data-parent-id")
               || c.ParentRowId === $(this).closest("tr").attr("data-parent-id")));

         }
      }
      // set -1 to unapproved match rate
      if (fltr.length > 0)
      {
         fltr.forEach(function ()
         {
            Match: -1
         });
      }

      if ($(this).hasClass("approved"))
      {
         // scenario is if you changed approved record,
         // you need to do visible button for approve it again,
         // and also the approved button from left side ,
         // if you click on approved button on left side you will approve all children
         var findIndex = approvedList.findIndex(c => c.RowId === parseFloat($(this).closest("tr").attr("mgmtentity-id"))
            && c.ParentRowId === ($(this).closest("tr").attr("data-parent-id") === undefined
               ? 0
               : parseFloat($(this).closest("tr").attr("data-parent-id"))));
         approvedList.splice(findIndex, 1);

         $(this).closest("tr").find("td[exchange='Approve']").removeClass("approved");
         $(this).closest("tr").find("td[exchange='ApproveAll']").removeClass("approved");
         $(this).closest("tr").find("div").removeClass("approved").addClass("hidden");
         $(this).closest("tr").find("input.ApprovedItem").attr("style", " ");

         if ($(this).closest("tr").attr("data-parent-id") !== undefined)
         {
            $(".searchGrid_Matching-div-click[mgmtentity-id='" + $(this).closest("tr").attr("data-parent-id") + "']").find("td[exchange='ApproveAll'] .ApprovedAllItem").attr("style", " ");
            $(".searchGrid_Matching-div-click[mgmtentity-id='" + $(this).closest("tr").attr("data-parent-id") + "']").find("td[exchange='ApproveAll'] div").addClass("hidden");
         }
         else
         {
            $(this).closest("tr").find("td[exchange='ApproveAll'] .ApprovedAllItem").attr("style", " ");
            $(this).closest("tr").find("td[exchange='ApproveAll'] div").addClass("hidden");
         }
      }

      var rowId = $(this).closest("tr").attr("data-parent-id") === undefined
         ? $(this).closest("tr").attr("MgmtEntity-id")
         : $(this).closest("tr").attr("data-parent-id");

      var newQty = $(this).val();
      var updatedGuid = $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value"));

      //display the Edited checkbox for an item when a quantity is changed
      $("#Edited_" + $(this).closest("tr").attr("MgmtEntity-id")).removeClass("hidden");
      $("#EditedAll_" + rowId).removeClass("hidden");
      $(".ApprovedItem[name='" + $(this).closest("tr").attr("MgmtEntity-id") + "']").hide();
      $(".ApprovedAllItem[name='" + rowId + "']").hide();

      /* Get group form quantities and item quantities */
      var formQty = $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find("td[exchange='FormQnty']").attr("exchange-value") === ""
         || $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find("td[exchange='FormQnty']").attr("exchange-value") === undefined
         ? 0
         : parseFloat($(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find("td[exchange='FormQnty']").attr("exchange-value"));

      var itemQty = $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find(".inputMatching").val() === ""
         || $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find(".inputMatching").val() === undefined
         ? 0
         : parseFloat($(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find(".inputMatching").val());

      /* -------------------------------------------------------------------*/
      // here if we have children under this row, we are giving all form quantities and 
      // item quantities and trying to calculating
      $(".searchGrid_Matching-div-click.matches[data-parent-id='" + rowId + "']").find(".inputMatching").each(function ()
      {
         var guidExchange = $(this).closest("tr").find("td[exchange='GUID']");

         if ($.trim($(guidExchange).attr("exchange-value")).length > 0 && !($(guidExchange).attr("style").indexOf("#000") > 0 || $.trim($(guidExchange).attr("style")).indexOf("rgb(0") > 0))
         {
            if ($.trim($(this).val()).length === 0)
            {
               $(this).val(0);
            }

            itemQty += parseFloat($.trim($(this).val()) === undefined || $.trim($(this).val()) === "" ? 0 : $.trim($(this).val()));
         }

         if ($(this).closest("tr").find("td[exchange='ndc']").attr("style").indexOf("#000") < 0)
         {
            var formQtyExchange = $(this).closest("tr").find("td[exchange='FormQnty']").attr("exchange-value");
            formQty += parseFloat((formQtyExchange === undefined || formQtyExchange === "")
               ? 0
               : formQtyExchange);
         }

         //When we edit a quantity and we're looking at every childin the form-line sub-set, check if we have any items that spam across multiple lines
         //we have to update the quantity on those as well, since they're all separate text boxes
         if (newQty !== undefined && newQty !== null && updatedGuid === $.trim($(guidExchange).attr("exchange-value")))
         {
            $(this).closest("tr").find(".inputMatching").val(newQty);
         }
      });

      // set the match rate
      // td:eq(6) references the 7th <td> element in the Matching table, which happens to be the Match column
      var matchRate = itemQty - formQty;
      if ($(this).closest("tr").find("td:eq(6)").attr("exchange") === "MatchRate")
      {
         // if current change is parent you need to set this rate to column 6 because of using rowspan
         $(this).closest("tr").find("td:eq(6)").html(matchRate > 0 ? "+" + matchRate : matchRate);
         $(this).closest("tr").find("td:eq(6)").attr("exchange-value", matchRate);
      }
      else
      {
         $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find("td:eq(6)").html(matchRate === ""
            ? 0
            : (matchRate > 0
               ? "+" + matchRate
               : matchRate));
         $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").find("td:eq(6)").attr("exchange-value", matchRate);
      }

      if (matchRate === 0)
      {
         // if match rate is 0 means you have match,
         // adding change class to each textboxes that we changed,
         // added green back ground for match rate column
         $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").addClass("Changes").closest("tr").find("td:eq(6)").css("background-color", "Green");
         $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "'] td:eq(6)").html("");

         if ($(".searchGrid_Matching-div-click.matches[data-parent-id='" + rowId + "']").length > 0)
         {
            $(".searchGrid_Matching-div-click.matches[data-parent-id='" + rowId + "']").addClass("Changes");
         }
      }
      else
      {
         // we still have mismatches and we're setting mismatched color 
         $(".searchGrid_Matching-div-click.matches[MgmtEntity-id='" + rowId + "']").removeClass("Changes").closest("tr").find("td:eq(6)").css("background-color", "#cb7474");

         if ($(".searchGrid_Matching-div-click.matches[data-parent-id='" + rowId + "']").length > 0)
         {
            $(".searchGrid_Matching-div-click.matches[data-parent-id='" + rowId + "']").removeClass("Changes");
         }
      }

      // determine if we already have this item in our editList
      var changesIndx = editList.indexOf(approvedList.find(c => c.LineNo === $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value")
         && c.DEA === $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value")
         && c.NDC === $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value")
         && c.GUID === $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value"))
         && c.RowId === parseFloat($(this).closest("tr").attr("mgmtentity-id"))
         && c.ParentRowId === ($(this).closest("tr").attr("data-parent-id") === undefined
            ? 0
            : parseFloat($(this).closest("tr").attr("data-parent-id")))));

      //if we don't have this item in the edited list we are adding into that list
      // editList : is a list of edited items for sending to controller and save information
      if (changesIndx === -1)
      {
         editList.push({
            "GUID": $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value")),
            "DEA": $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value"),
            "LineNo": $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value"),
            "ItemQty": $(this).closest("tr").find(".inputMatching").val() === ""
               ? 0
               : $(this).closest("tr").find(".inputMatching").val(),
            "NDC": $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value"),
            "RowId": parseFloat($(this).closest("tr").attr("mgmtentity-id")),
            "ParentRowId": $(this).closest("tr").attr("data-parent-id") === undefined
               ? 0
               : parseFloat($(this).closest("tr").attr("data-parent-id")),
            "Match": matchRate,
            "Approved": false
         });
      }
      else
      {
         // if we have this approved item into list we are just changing the matching rate and item quantity
         approvedList[changesIndx].ItemQty = $(this).closest("tr").find(".inputMatching").val() === "" ? 0 : $(this).closest("tr").find(".inputMatching").val();
         approvedList[changesIndx].Match = matchRate;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Approved button for each item.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ApprovedItem", function (data)
   {
      //$(this).closest("tr").find("td input.inputMatching").attr("readonly", "readonly");
      $(this).hide();
      inputChnge = true;

      //these are declared here so we don't have to rely on having to call the html each and every single time
      var dataParentId = $(this).closest("tr").attr("data-parent-id");
      var mgmtEntityId = $(this).closest("tr").attr("MgmtEntity-id");

      // when clicking on button we are changing the button to pending checkbox
      $("#Pending_" + mgmtEntityId).removeClass("hidden");
      $(this).closest("tr").find("td[exchange='Approve']").closest("td").addClass("approved");
      $(this).closest("tr").find(".inputMatching").addClass("approved");

      // approved item is in list of approved or no get the parent-id
      var changesIndx = approvedList.indexOf(approvedList.find(c => c.LineNo === $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value")
         && c.DEA === $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value")
         && c.NDC === $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value")
         && c.GUID === $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value"))
         && c.RowId === parseFloat(mgmtEntityId)
         && c.ParentRowId === (dataParentId === undefined
            ? 0
            : parseFloat(dataParentId))));

      // get the matching rate ( rowspan value ) 
      var matchingRate = dataParentId === undefined
         ? (($(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "']").length > 0
            && ($(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "']").length ===
               $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "'] td[exchange='Approve'].approved").length))
            || (($(".searchGrid_Matching-div-click td[exchange='GUID'][exchange-value='" + $(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value") + "']").length - 1)
               === $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "']").length))
            ? parseFloat($(this).closest("tr").find("td[exchange=MatchRate]").attr("exchange-value") === undefined
               ? 0
               : $(this).closest("tr").find("td[exchange=MatchRate]").attr("exchange-value"))
            : parseFloat($(this).closest("tr").find(".inputMatching").val() === "" || $(this).closest("tr").find(".inputMatching").val() === "0"
               ? -1
               : $(this).closest("tr").find(".inputMatching").val())
         : ($(".searchGrid_Matching-div-click[data-parent-id='" + dataParentId + "']").length > 0
            && ($(".searchGrid_Matching-div-click[data-parent-id='" + dataParentId + "']").length ===
               $(".searchGrid_Matching-div-click[data-parent-id='" + dataParentId + "'] td[exchange='Approve'].approved").length)
            && $(".searchGrid_Matching-div-click[MgmtEntity-id='" + dataParentId + "'] td[exchange='Approve']").hasClass("approved"))
            ? parseFloat($(".searchGrid_Matching-div-click[mgmtentity-id='" + dataParentId + "'] td[exchange='MatchRate']").attr("exchange-value") === undefined
               ? 0
               : $(".searchGrid_Matching-div-click[mgmtentity-id='" + dataParentId + "'] td[exchange='MatchRate']").attr("exchange-value"))
            : parseFloat($(this).closest("tr").find(".inputMatching").val() === "" ? 0 : $(this).closest("tr").find(".inputMatching").val());

      //if we don't have this item in approved list we are adding into that list
      // approvedList : is a list of approved items for sending to controller and save information
      if (changesIndx === -1)
      {
         approvedList.push({
            "GUID": $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value")),
            "DEA": $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value"),
            "LineNo": $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value"),
            "ItemQty": $(this).closest("tr").find(".inputMatching").val() === ""
               ? 0
               : $(this).closest("tr").find(".inputMatching").val(),
            "NDC": $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value"),
            "RowId": parseFloat(mgmtEntityId),
            "ParentRowId": dataParentId === undefined
               ? 0
               : parseFloat(dataParentId),
            "Match": matchingRate,
            "Approved": true
         });
      }
      else
      {
         // if we have this approved item into list we are just changing the matching rate and item quantity
         approvedList[changesIndx].ItemQty = $(this).closest("tr").find(".inputMatching").val() === "" ? 0 : $(this).closest("tr").find(".inputMatching").val();
         approvedList[changesIndx].Match = matchingRate;
      }

      //we're disabling the edit quantity textbox for the approved row
      $(this).closest("tr").find(".inputMatching").hide();
      $(this).closest("tr").find("label").show();

      // determine which item approved , if all child items approved and we approved the last unapproved item, 
      // we are changing approved all button to pending
      if (dataParentId !== undefined
         || (dataParentId === undefined &&
            $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "']").length
            === ($(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "'] td[exchange='Approve'].approved").length
               || $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "'] td[exchange='GUID'][exchange-value='"
                  + $(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value") + "']").closest("tr").length)))
      {
         if ($("tr[data-parent-id='" + dataParentId + "']").length === $("tr[data-parent-id='" + dataParentId + "'] td[exchange='Approve'].approved").length
            && $("tr[mgmtentity-id='" + dataParentId + "'] td[exchange='Approve']").hasClass("approved"))
         {
            $("tr[mgmtentity-id='" + dataParentId + "'] td[exchange='ApproveAll'] .ApprovedAllItem").click();
         }
         else if (dataParentId === undefined
            && $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "']").length
            === ($(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "'] td[exchange='Approve'].approved").length
               || $(".searchGrid_Matching-div-click[data-parent-id='" + mgmtEntityId + "'] td[exchange='GUID'][exchange-value='"
                  + $(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value") + "']").closest("tr").length))
         {
            // if we approved all childrens includes parent  and don't have any unapproved item,
            // we are automatically changing button from approved all to pending
            $("tr[mgmtentity-id='" + mgmtEntityId + "'] td[exchange='ApproveAll'] .ApprovedAllItem").click();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Approved All button for each item.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ApprovedAllItem", function (data)
   {
      //$(".searchGrid_Matching-div-click[data-parent-id='" + $(this).closest("tr").attr("MgmtEntity-id") + "'] td input.inputMatching").attr("readonly", "readonly");
      //$(this).closest("tr").find("td input.inputMatching").attr("readonly", "readonly");
      inputChnge = true;
      $(this).closest("tr").find("td[exchange='ApproveAll']").closest("td").addClass("approved");
      $(this).hide();
      $(this).closest("td").find("div#PendingAll_" + $(this).closest("tr").attr("MgmtEntity-id")).removeClass("hidden");
      $(this).closest("tr").find(".inputMatching").addClass("approved");
      var parentItemGuid = $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value"));

      var chngsIndx = approvedList.indexOf(approvedList.find(c => c.LineNo === $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value")
         && c.DEA === $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value")
         && c.NDC === $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value")
         && c.GUID === parentItemGuid
         && c.RowId === parseFloat($(this).closest("tr").attr("MgmtEntity-id"))
         && c.ParentRowId === ($(this).closest("tr").attr("data-parent-id") === undefined
            ? 0
            : parseFloat($(this).closest("tr").attr("data-parent-id")))));

      var matchingRate = $(this).closest("tr").find("td[exchange='MatchRate']").attr("exchange-value") === ""
         || $(this).closest("tr").find("td[exchange='MatchRate']").attr("exchange-value") === undefined
         ? 0
         : parseFloat($(this).closest("tr").find("td[exchange='MatchRate']").attr("exchange-value"));

      if (chngsIndx === -1)
      {
         approvedList.push({
            "GUID": parentItemGuid,
            "DEA": $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value"),
            "LineNo": $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value"),
            "ItemQty": $(this).closest("tr").find(".inputMatching").val() === ""
               ? 0
               : $(this).closest("tr").find(".inputMatching").val(),
            "NDC": $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value"),
            "RowId": parseFloat($(this).closest("tr").attr("mgmtentity-id")),
            "ParentRowId": $(this).closest("tr").attr("data-parent-id") === undefined
               ? 0
               : parseFloat($(this).closest("tr").attr("data-parent-id")),
            "Match": matchingRate,
            "Approved": true
         });
      }
      else
      {
         approvedList[chngsIndx].ItemQty = $(this).closest("tr").find(".inputMatching").val() === "" ? 0 : $(this).closest("tr").find(".inputMatching").val();
         approvedList[chngsIndx].Match = matchingRate;
      }

      $(".searchGrid_Matching-div-click[data-parent-id='" + $(this).closest("tr").attr("MgmtEntity-id") + "']").each(function ()
      {
         // calculating all children's quantities and signed as pending
         if ($.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value")).length > 0)
         {
            chngsIndx = approvedList.indexOf(approvedList.find(c =>
               c.LineNo === $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value")
               && c.DEA === $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value")
               && c.NDC === $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value")
               && c.GUID === $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value"))
               && c.RowId === parseFloat($(this).closest("tr").attr("MgmtEntity-id"))
               && c.ParentRowId === parseFloat($(this).closest("tr").attr("data-parent-id"))));

            if ($(this).find("td[exchange=Approve] input[type=submit]").attr("style").indexOf("none") < 0)
            {
               $("#Pending_" + $(this).closest("tr").attr("MgmtEntity-id")).removeClass("hidden");
               $(".ApprovedItem[name='" + $(this).closest("tr").attr("MgmtEntity-id") + "']").hide();
               $(this).closest("tr").find("td[exchange='Approve']").closest("td").addClass("approved");
            }

            //we're disabling the edit quantity textbox for the approved all row and all its children
            $(this).closest("tr").find(".inputMatching").addClass("approved").hide();
            $(this).closest("tr").find("label").show();

            if (chngsIndx === -1)
            {
               approvedList.push({
                  "GUID": $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value")) === parentItemGuid
                     ? parentItemGuid
                     : $.trim($(this).closest("tr").find("td[exchange='GUID']").attr("exchange-value")),
                  "DEA": $(this).closest("tr").find("td[exchange='dea']").attr("exchange-value"),
                  "LineNo": $(this).closest("tr").find("td[exchange='lineNo']").attr("exchange-value"),
                  "ItemQty": $(this).closest("tr").find(".inputMatching").val() === ""
                     ? 0
                     : $(this).closest("tr").find(".inputMatching").val(),
                  "NDC": $(this).closest("tr").find("td[exchange='ndc']").attr("exchange-value"),
                  "RowId": parseFloat($(this).closest("tr").attr("mgmtentity-id")),
                  "ParentRowId": $(this).closest("tr").attr("data-parent-id") === undefined
                     ? 0
                     : parseFloat($(this).closest("tr").attr("data-parent-id")),
                  "Match": matchingRate,
                  "Approved": true
               });
            }
            else
            {
               approvedList[chngsIndx].ItemQty = $(this).closest("tr").find(".inputMatching").val() === ""
                  ? 0
                  : $(this).closest("tr").find(".inputMatching").val();
               approvedList[chngsIndx].Match = matchingRate;
            }
         }
      });

      //the ApprovedItem check is there to ensure we don't replace the already-approved item with a Pending image
      //when Approve All is clicked, or all remaining items for a form-line combination are approved
      if (parentItemGuid.length > 0 && $(this).closest("tr").find("td[exchange='Approve']").find("#Approved_" + $(this).closest("tr").attr("MgmtEntity-id")).hasClass("hidden"))
      {
         $("#Pending_" + $(this).closest("tr").attr("MgmtEntity-id")).removeClass("hidden");
         $(".ApprovedItem[name='" + $(this).closest("tr").attr("MgmtEntity-id") + "']").hide();
      }

      //we're disabling the edit quantity textbox for the first row
      //on an Approve All button click, we always use the top-most row as a base for all the other items that are part of the Form-Line approval
      $(this).closest("tr").find(".inputMatching").hide();
      $(this).closest("tr").find("label").show();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function ()
   {
      // when page don't have any approved nor edited items, do not attempt to save the form
      if (approvedList.length === 0 && editList.length === 0)
      {
         $(this).setErrorMessage("Please approve and/or make changes before attempting to save", "Error Message", "Continue");
         return false;
      }

      // secondary verification for save information in control 2 matching
      btnObjects = [{
         "id": "btnValidationSubmitApprove",
         "name": "Approve",
         "function": "",
         "class": "btnErrorMessage"
      }, {
         "id": "btnValidationSubmitCancel",
         "name": "Cancel",
         "function": "onclick='$(this).closeMessageBox();'",
         "class": "btnErrorMessage"
      }];

      var verificationMsg = "A supervisor signature is required in order to complete the modifications made to the current matching.<br><br> Supervisor, please press cancel and review the approvals and quantity changes made to the matching tabe. Press Save in order to return here to sign.<br><br> Entering your username and password is a signature-equivalent action and indicates your approval of the changes made to this matching table.";
      $(this).setVerificationMessageBox(verificationMsg, "Supervisor Verification", btnObjects, false, "btnValidationSubmitApprove");

      setTimeout(function ()
      {
         $("#CustomMessageBoxVerification .textboxes input#UserName").focus();
      }, 200);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save After Supervisor's Verification.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnValidationSubmitApprove", function ()
   {
      var validation = $(this).checkForValidation("Supervisor verification for save approved control 2 matching");

      if (validation.Success)
      {
         var id = $("a.active img.actionLoad").attr("id");
         $("#wait").show();
         $("#" + id).show();
         var tempAprList = tempAprList === undefined ? approvedList : tempAprList;
         approvedList = (approvedList.length + hiddenList.length) === approvedList.length ? approvedList : approvedList.concat(hiddenList);
         approvedList = editList.length > 0 ? approvedList.concat(editList) : approvedList;
         $(this).closeMessageBox();

         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "itemList=" + JSON.stringify(approvedList) + "&approvedBy=" + validation.UserName + getAntiForgeryToken(),
            url: baseURL + baseURL + "/SaveMatching",
            success: function (data)
            {
               if (data.Success)
               {
                  // temp approve list is list of approved list
                  // we need this list to determined what list we have and needs to changed pending to approved instead of loading page
                  for (var i = 0; i < tempAprList.length; i++)
                  {
                     $(".searchGrid_Matching-div-click[MgmtEntity-id='" + tempAprList[i].RowId + "'] td[exchange='ItemQty'] input").hide();
                     $(".searchGrid_Matching-div-click[MgmtEntity-id='" + tempAprList[i].RowId + "'] td[exchange='ItemQty'] label").show();

                     if (tempAprList[i].Match !== 0)
                     {
                        $(".searchGrid_Matching-div-click[MgmtEntity-id='" + tempAprList[i].RowId + "'] td[exchange='ItemQty'] label").html(tempAprList[i].ItemQty);
                     }
                  }

                  hiddenList = [];
                  approvedList = [];
                  tempAprList = [];
                  editList = [];

                  if ($(".searchGrid_Matching-div-click").not(".hidden").length === 0)
                  {
                     $(".searchGrid_Summary.selected").remove();
                     $(".formMax .inputEx").val("");
                     //finish
                  }
                  //var $div = $(".searchGrid_Summary-div-click[MgmtEntity-id='" + validRANumber + "']").closest("tr");
                  //$div.remove();
                  //$(".formMax .inputEx").val("");
                  //validRANumber = "";
                  //$("#WrhsCtrl2DetailsForm").html($html_matching);
                  //$("#RA").focus();

                  inputChnge = false;

                  if (getEditMode())
                  {
                     window.setEditMode(false);
                  }

                  //there's a wait.hide() in the asynchronous call below, so SaveMatching does not need a wait.hide() here
                  getMatchingForm222($(".searchGrid_Summary-div-click.selected").find("td[exchange='RA']").attr("exchange-value"), 2);
               }
               else
               {
                  $("#" + id).hide();
                  $("#wait").hide();
               }
               
            },
            error: function (data)
            {
               $("#wait").hide();

               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on Control II Form Save request.</h3>"
               }];

               btnObjects = [{
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }];

               titleobjects = [{
                  "title": "Invalid Entry"
               }];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cancel function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function ()
   {
      if (window.getEditMode() && (approvedList.length > 0 || inputChnge))
      {
         if ($(".tabContainer a.active").attr("href") !== "#tabs-Matching")
         {
            $(".tabContainer a[href='#tabs-Matching']").click();
         }

         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to Cancel the changes?</h3>"
         }];
         btnObjects = [{
            "id": "btnYesCancel",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 7px;text-align:center;width:70px"
         }, {
            "id": "btnNoCancel",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 7px;text-align:center;width:70px",
            "function": "onclick='$(this).closeMessageBox();'"
         }];

         titleobjects = [{
            "title": "Cancel CII Matching Form Edit"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
      }
      else
      {
         cancelMatching();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Print function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function cancelMatching()
   {
      window.setEditMode(false);
      $(this).closeMessageBox();
      readyStatus = 0;
      cancel = true;
      $(".tabContainer a:first").click();
      $(".formMax .inputEx").val("");
      $("#RA").removeAttr("disabled readonly");
      $(".searchGrid_Summary-div-click[MgmtEntity-id='" + validRAID + "']").click();
      hiddenList = [];
      approvedList = [];
      editList = [];
      validRAID = "";
      validRANumber = "";
      $("#WrhsCtrl2DetailsForm").html($rowMatching);
      inputChnge = false;
      $("#RA").focus();

      if ($(".ql-tab.active a").attr("href").indexOf("Summary") > 0)
      {
         enableActionbarControl("#CMD_REFRESH", true);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Print function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
   }
   //========================================End ActionBar========================

   //*****************************************************************************
   //*
   //* Summary:
   //*   keypress RA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".inputEx", function (e)
   {
      if (e.keyCode === 13 && $(this).attr("id") === "RA" && ((window.getEditMode() && !inputChnge) || !window.getEditMode()))
      {
         searchRaInfo($(this).val());
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   search by RA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function searchRaInfo(ra)
   {
      if ($.trim(ra).length === 0)
      {
         return false;
      }

      $("#wait").show();
      $("#Summary-loading").show();

      if ($(".searchGrid_Summary-div-click").hasClass("selected"))
      {
         $(".searchGrid_Summary-div-click.selected").click();
      }

      // searching ra into list of grid if find some highlighting
      // if no searching from database
      var vld = false;
      $(".searchGrid_Summary-div-click td[exchange='RA']").each(function ()
      {
         if ($.trim($(this).attr("exchange-value")) === $.trim(ra))
         {

            $(this).click();
            //table.scroller().scrollToRow($(this).closest("tr").index());
            //$("#searchGrid_Summary").animate({
            //   scrollTop: $("#searchGrid_Summary").scrollTop() + $(".searchGrid_Summary-div-click:eq('" +
            //      $(this).closest("tr").index() + "')").offset().top
            //}, 500);
            //table.row($(this).closest("tr").index()).scrollTo();
            vld = true;
            return false;
         }
      });

      window.setEditMode(false);

      // vld is for know do we have RA in grid's list or no, if this is true it means we have it
      if (!vld)
      {
         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "raNumber=" + $.trim(ra) + getAntiForgeryToken(),
            url: baseURL + baseURL + "/CheckRA",
            async: true,
            success: function (data)
            {
               if (data.Ctrl !== "" && data.Ctrl !== undefined)
               {
                  //get feedback if searched ra is not control 2
                  if (data.Ctrl !== "CTRL2")
                  {
                     $("#wait").hide();
                     $(this).setErrorMessage("The scanned RA should be C2 RA", "Error Message", "Continue");
                  }
                  else
                  {
                     if (data.Status !== "C" && data.Status !== "CE" && data.Status !== "CD")
                     {
                        //click on row if you searched by RA ID
                        if ($(".searchGrid_Summary-div-click[mgmtentity-id='" + data.RaID + "']").length > 0)
                        {
                           $(".searchGrid_Summary-div-click[mgmtentity-id='" + data.RaID + "']").click();
                           $("#Summary-loading").hide();
                           $("#wait").hide();
                           return false;
                        }
                        else
                        {
                           //get feedback if searched ra is control 2 but don't have form222
                           $("#wait").hide();
                           $(this).setErrorMessage("There is no form 222 matching for this RA", "Error Message", "Continue");
                        }
                     }
                     else
                     {
                        //get feedback if searched ra is control 2 but closed
                        $("#wait").hide();
                        $(this).setErrorMessage("The scanned RA is currently closed", "Error Message", "Continue");
                     }
                  }
               }
               else
               {
                  //get feedback if user don't found matches
                  $("#wait").hide();
                  $(this).setErrorMessage("No matches found", "Error Message", "Continue");
               }

               $("#RA").val("");
            },
            error: function (data)
            {
               $("#Summary-loading").hide();
               $("#wait").hide();

               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on Control II Matching request.</h3>"
               }];

               btnObjects = [{
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }];

               titleobjects = [{
                  "title": "Invalid Entry"
               }];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         });
      }

      $("#wait").hide();
      $("#Summary-loading").hide();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on summary grid
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".searchGrid_Summary-div-click", function (e)
   {
      // if we have changes in itemQuantity message box comes up
      if (getEditMode() && inputChnge)
      {
         $rowSummary = $(this).closest("tr");
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to Cancel the changes?</h3>"
         }];
         btnObjects = [{
            "id": "btnYesCancel",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:center"
         }, {
            "id": "btnNoCancel",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:centerqne"
         }];

         titleobjects = [{
            "title": "Confirm"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         return false;
      }

      if ($(e.target).closest("tr").hasClass("MgmtEntity"))
      {
         $(e.target).closest("tr").removeClass("MgmtEntity");
         return false;
      }

      $rowSummary = "";
      $(".searchGrid_Summary-div-click.MgmtEntity").closest("tr").removeClass("MgmtEntity");

      if ($(this).hasClass("selected"))
      {
         validRANumber = "";
         validRAID = "";
         $(this).removeClass("selected");
         $(".formMax .inputEx").val("");
         $("#WrhsCtrl2DetailsForm").html($htmlMatching);
         $("#RA").focus();
      }
      else
      {
         // if we don't have any changes in item quantity we are just clicking new record
         $(this).closest("tr").addClass("MgmtEntity");
         $(".searchGrid_Summary-div-click.selected").removeClass("selected");
         $(".searchGrid_Summary-div-click.MgmtEntity").closest("tr").click();
         validRANumber = $(this).closest("tr").find("td[exchange='RA']").attr("exchange-value");
         validRAID = $(this).closest("tr").attr("MgmtEntity-id");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Switch Tabs
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".tabContainer a", function ()
   {
      if ($(this).attr("href").indexOf("Matching") > 0)
      {
         if (!window.getEditMode())
         {
            if (validRAID === "" && !$(".searchGrid_Summary-div-click").hasClass("selected"))
            {
               return false;
            }
            // get status from ready for doing matching or no
            var rdValue = $(".searchGrid_Summary-div-click.selected").find("td[exchange='Ready']").attr("exchange-value");
            // get status from ready for doing matching or no
            if (parseFloat(rdValue === undefined ? 0 : rdValue) === 0)
            {
               $(this).setErrorMessage("All totes must be processed", "Error Message", "Continue");
               $(".formmax .inputex").val("");
               return false;
            }
            // 0 Means you don't have any associated totes
            // 1 Means you received totes and not completed
            // 2 means you received totes and completed and it's ready for matching
            // why I didn't remove this numbers : because before that regarding our conversation 
            // we had received records but didn't finished  yet we simply showed not editable records
            // we didn't have received records and finished records we showed yellow and white colors from form side
            // if the logic with if ready is there user can go to matching needs to just remove this condition 0,1,2
            // add condition  $(".searchGrid_Summary-div-click.selected")
            //   .find("td[exchange='Ready']")
            // .attr("exchange-value")===1

            getMatchingForm222($(".searchGrid_Summary-div-click.selected").find("td[exchange='RA']").attr("exchange-value"),
               $(".searchGrid_Summary-div-click.selected").find("td[exchange='WrhsContrIdR']").attr("exchange-value") === "0"
                  ? 0
                  : rdValue === "0"
                     ? 1
                     : 2);
         }
      }
      else if ($(this).attr("href").indexOf("Summary") > 0)
      {
         // if you are in edit mode and have changes and switch to summary tab, 
         // messages comes up for cancel
         if (inputChnge)
         {
            if (!cancel)
            {
               OnQnexCancel();
            }
            else
            {
               cancel = true;
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Confirmation to cancel changes
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnYesCancel", function ()
   {
      cancelMatching();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   cancel changes
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnNoCancel", function ()
   {
      $(".tabContainer a[href='#tabs-Matching']").click();
      $(this).closeMessageBox();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click the Finish C2 Matching button
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#matchingContinue", function ()
   {
      if (getEditMode())
      {
         var messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Items have to be moved from the white bin to the black bin. Please scan a black bin to continue." +
               "<br/><br/><input class=\"blackBinId scannerRequired text-box single-line\" style=\"height: 22px;\" type=\"text\" autocomplete=\"off\">" +
               "<br/><br/><input type=\"button\" id=\"btnPut\" value=\"Put\" class=\"btnErrorMessage blackBinDesignation\" style=\"margin:0 auto;text-align:center;width:75px\"></h3>" +
               "<div class='tdMessage' style='padding-left:33px'><table><tr>" +
               "<td style='margin:0 auto;width:100%;color:maroon;font: 16px eras_medium,\"Eras Medium ITC\",sans-serif;'" +
               "id='CIIMatchingError'></td></tr></table></div><span class='scannedBlackBin'></span>" +
               "<script>setTimeout(function(){$('.blackBinId').select();},10);</script>"
         }];
         var titleobjects = [{ "title": "Scan Black Bin" }];
         $(this).addMessageButton([], messageObject);
         $(this).showMessageBox(titleobjects);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click the Put button after scanning the Black Bin
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnPut", function ()
   {
      if ($(".blackBinId").val() === undefined || $(".blackBinId").val() === "")
      {
         $(".blackBinId").select();
         $("#CIIMatchingError").html("Please Scan A Black Bin To Continue");
         return false;
      }

      $("#wait").show();
      var rtrnAuthId = $(".searchGrid_Summary-div-click.selected").attr("mgmtentity-id");
      var blackBin = $(".blackBinId").val().toUpperCase();
      $("#CIIMatchingError").html("");

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "raNumber=" + validRANumber + "&rtrnAuthId=" + rtrnAuthId + "&blackBin=" + blackBin + getAntiForgeryToken(),
         url: baseURL + baseURL + "/FinishC2Matching",
         async: false,
         success: function (data)
         {
            $("#wait").hide();

            if (data.success)
            {
               $(this).closeMessageBox();

               btnObjects = [{
                     "id": "btn_continue_matching",
                     "name": "Finish",
                     "style": "margin:0 10px;text-align:center;width:70px",
                     "function": "",
                     "class": "btnErrorMessage"
               }];

               var message = data.itemCount === 0
                  ? "No items are needed to be moved."
                  : "Please move all remaining White Bin items { " + data.itemCount + " } from White Bin(s) { " + data.whiteBins.toString() + " } to Black Bin { " + blackBin + " }.";

               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>C2 Matching Process Complete. " + message + "</h3>" +
                     "<script>$('.close-button-messageBox').hide()</script>"
               }];
               titleobjects = [{
                  "title": "Finalize C2 Matching"
               }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
            else if (data.error)
            {
               $("#CIIMatchingError").html(data.errorMsg);
               $(".blackBinId").select();
            }
         },
         error: function (data)
         {
            $("#wait").hide();
            $("#CIIMatchingError").html("The controller call failed on Control II Finish Matching request.");
            $(".blackBinId").select();
         }
      });

      return false;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   C2 Matching Finalize Continue button click
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_continue_matching", function ()
   {
      $(this).closeMessageBox();
      setTimeout(function ()
      {
         $("#CMD_CLEAR").click();
      }, 150);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Black Bin textbox key down event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keydown", ".blackBinId", function (e)
   {
      if (e.keyCode === 32) //don't allow spaces
      {
         return false;
      }
      else if (e.keyCode === 13)
      {
         $("#btnPut").click();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Matching form222 using RA
   //*
   //* Parameters:
   //*   list - List of Matching form222 associated with RA
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getMatchingForm222(raNumber, status)
   {
      validRAID = $(".selected").closest("tr").attr("MgmtEntity-id");
      validRANumber = $(".selected").closest("tr").find("td[exchange='RA']").attr("exchange-value");
      readyStatus = status;
      // when status is ready for matching , getting data from form222 form
      //
      $("#wait").show();
      $("#Matching-loading").show();
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "raNumber=" + raNumber + "&raId=" + validRAID + "&status=" + status + getAntiForgeryToken(),
         url: baseURL + baseURL + "/GetMatchingForm222",
         async: true,
         success: function (data)
         {
            enableActionbarControl("#CMD_REFRESH", false);
            replaceSegments($("#WrhsCtrl2DetailsForm"), $(data.matching));
            $("#Matching-loading").hide();
            inputChnge = false;

            if ($("#searchGrid_Matching").length > 0)
            {
               window.dtHeaders("#searchGrid_Matching", "", ["sorting"], []);
               $("#searchGrid_Matching_filter").css("display", "none");
            }

            // get status , if this is 2 it means you can edit it
            if (status === 2)
            {
               hiddenList = [];
               approvedList = [];
               editList = [];

               var ndc = "";
               var dea = "";
               var lineNo = 0;
               var indx = [];// list of grouping by ndc
               var matchRate = 0;
               var addMatches = [];
               var $trs = "";
               var div = [];

               // *******  Re-order Unmatched records to end of the list  ********
               $(".searchGrid_Matching-div-click").not(".hidden").find("td[exchange='ndc'].unmatch").closest("tr").each(function ()
               {
                  var lineNo = $(this).find("td[exchange='lineNo']").attr("exchange-value");

                  if ((lineNo === "0" || lineNo === "" || lineNo === undefined)
                     && $(this).find("td[exchange='ndc']").attr("exchange-value") !== "")
                  {
                     div.push($(this));
                  }
               });

               for (var sk = 0; sk < div.length; sk++)
               {
                  $(div[sk]).remove();
                  $(div[sk]).insertAfter($(".searchGrid_Matching-div-click:last"));
               }

               // **********************************************************
               //*******  add rowspan for visible (un-hidden) records and calculating Matching Rate  **************
               $(".searchGrid_Matching-div-click").not(".hidden").each(function ()
               {
                  var exchangeNdc = $(this).find("td[exchange='ndc']").attr("exchange-value");
                  var exchangeDea = $(this).find("td[exchange='dea']").attr("exchange-value");
                  var exchangeGuid = $(this).find("td[exchange='GUID']").attr("exchange-value");

                  //edits - DMB
                  if ($.trim(exchangeGuid).length === 0)
                  {
                     //the first setRowspan merges any previous lines that would be merged
                     indx = setRowspan(indx);

                     matchRate = $.trim($(this).find("td[exchange='MatchRate']").html());
                     indx.push({
                        "row": $(this).attr("MgmtEntity-id"),
                        "match": matchRate,
                        "itemQty": $.trim($(this).find("td[exchange='ItemQty']").attr("exchange-value")),
                        "formQty": $.trim($(this).find("td[exchange='FormQnty']").attr("exchange-value")),
                        "GUID": $.trim($(this).find("td[exchange='GUID']").html()),
                        "Matching": $.trim($(this).find("td[exchange='Matching']").attr("exchange-value"))
                     });

                     //this second one enters the line with no items received that we just pushed into indx
                     indx = setRowspan(indx);
                  }
                  else
                  {
                     if (ndc.length === 0 ||
                        (ndc.length > 0 && $.trim(exchangeNdc) === $.trim(ndc) &&
                           (
                              ($.trim(exchangeNdc) === "" && $.trim(exchangeDea) === "")
                              ||
                              ($.trim(exchangeNdc) !== "" && $.trim(exchangeDea) !== "")
                           )
                        ))
                     {
                        // get ndc if this is first record or ndc has been changed from another row
                        ndc = exchangeNdc;

                        matchRate = $.trim($(this).find("td[exchange='MatchRate']").html());// $(this).find("td[exchange='MatchRate']").attr("exchange-value");
                        indx.push({
                           "row": $(this).attr("MgmtEntity-id"),
                           "match": matchRate,
                           "itemQty": $.trim($(this).find("td[exchange='ItemQty']").attr("exchange-value")),
                           "formQty": $.trim($(this).find("td[exchange='FormQnty']").attr("exchange-value")),
                           "GUID": $.trim($(this).find("td[exchange='GUID']").html()),
                           "Matching": $.trim($(this).find("td[exchange='Matching']").attr("exchange-value"))
                           //$(this).find("td[exchange='MatchRate']").attr("exchange-value")
                        });

                        if ($(this).next().length === 0)
                        {
                           if (indx.length > 0)
                           {
                              indx = setRowspan(indx);
                           }
                        }
                     }
                     else if ($.trim(ndc) !== $.trim(exchangeNdc) || ($.trim(exchangeDea) === "" && $.trim(ndc) === $.trim(exchangeNdc)))
                     {
                        // calculating old ndc to go for processing in new ndc for multiple rows
                        //and add rowspan for multiple rows
                        if (indx.length > 0)
                        {
                           indx = setRowspan(indx);
                        }

                        ndc = exchangeNdc;
                        matchRate = $.trim($(this).find("td[exchange='MatchRate']").html());
                        indx.push({
                           "row": $(this).attr("MgmtEntity-id"),
                           "match": matchRate,
                           "itemQty": $.trim($(this).find("td[exchange='ItemQty']").attr("exchange-value")),
                           "formQty": $.trim($(this).find("td[exchange='FormQnty']").attr("exchange-value")),
                           "GUID": $.trim($(this).find("td[exchange='GUID']").html()),
                           "Matching": $.trim($(this).find("td[exchange='Matching']").attr("exchange-value"))
                        });
                     }
                  }

                  // if you get last record and don't have any visible records
                  // do calculation and add in rowspan if this is multiple row
                  if ($(this).next().not(".hidden").length === 0 && indx.length > 0)
                  {
                     indx = setRowspan(indx);
                  }
               });

               // adding all hidden items into hidden list
               // hidden items : it mean this is match items and there is no discrepancy
               $(".searchGrid_Matching-div-click.hidden").each(function ()
               {
                  hiddenList.push({
                     "GUID": $.trim($(this).find("td[exchange='GUID']").attr("exchange-value")),
                     "DEA": $(this).find("td[exchange='dea']").attr("exchange-value"),
                     "LineNo": $(this).find("td[exchange='lineNo']").attr("exchange-value"),
                     "ItemQty": $(this).find(".inputMatching").val() === ""
                        ? 0
                        : $(this).find(".inputMatching").val(),
                     "NDC": $(this).find("td[exchange='ndc']").attr("exchange-value"),
                     "RowId": parseFloat($(this).closest("tr").attr("mgmtentity-id")),
                     "ParentRowId": $(this).closest("tr").attr("data-parent-id") === undefined
                        ? 0
                        : parseFloat($(this).closest("tr").attr("data-parent-id")),
                     "Match": 0,
                     "Approved": false
                  });
               });

               $("td[exchange='GUID']").css("width", "100px");
               $("td[exchange='Condition']").css("width", "50px");
               $("td[exchange='ApproveAll']").css("width", "73px");
               $("td[exchange='Approve']").css("width", "73px");

               if (status === 2)
               {
                  $("#CMD_EDIT").click();
               }

               //$rowMatching = $("#WrhsCtrl2DetailsForm").html();
            }

            setTimeout(function()
            {
               $("#wait").hide();
            }, 400);

            //enable the finish C2 matching button if there are no discrepancies, or if all discrepancies have been approved
            if ($("#searchGrid_Matching").attr("formcomplete") !== undefined && $("#searchGrid_Matching").attr("formcomplete").toLowerCase() === "true")
            {
               $("#matchingContinue").enable();
            }
         },
         error: function (data)
         {
            $("#Summary-loading").hide();
            $("#wait").hide();

            messageObject = [{
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on Control II Matching request.</h3>"
            }];

            btnObjects = [{
               "id": "btn_ok_error",
               "name": "Ok",
               "class": "btnErrorMessage",
               "style": "margin:0 auto;text-align:center",
               "function": "onclick='$(this).closeMessageBox();'"
            }];

            titleobjects = [{
               "title": "Invalid Entry"
            }];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Row Span for groups in Match Column
   //*
   //* Parameters:
   //*   indx - List of grouped items
   //*
   //* Returns:
   //*   Empty array.
   //*
   //*****************************************************************************
   function setRowspan(indx)
   {
      //usually occurs when we come across a line that happened to not have a GUID
      if (indx.length === 0)
      {
         return [];
      }

      //get the form and item quantities
      //we only need the first row (index of 0) because we're only going to be displaying the first row of a form/item match, while hiding the others
      var formQnt = parseFloat(indx[0].formQty === "" ? 0 : indx[0].formQty);
      var itemQnty = parseFloat(indx[0].itemQty === "" ? 0 : indx[0].itemQty);

      for (var b = 0; b < indx.length; b++)
      {
         if (indx.length > 1
            && ((indx[b + 1] !== undefined && indx[b].GUID === indx[b + 1].GUID)
               || indx[b - 1] !== undefined && indx[b].GUID === indx[b - 1].GUID))
         {
            formQnt = (b === 0 ? 0 : formQnt) + parseFloat(indx[b].formQty);
         }
         else
         {
            itemQnty = (b === 0 ? 0 : itemQnty) + parseFloat(indx[b].itemQty);
         }
      }

      var newFormQty = 0;
      var newItemQty = 0;
      var guidUsed = [];
      var c = 0;

      for (c = 0; c < indx.length; c++)
      {
         newFormQty = (c === 0 ? 0 : newFormQty) + parseFloat(indx[c].formQty === "" ? "0" : indx[c].formQty);
      }

      for (c = 0; c < indx.length; c++)
      {
         var duplicateGuid = false;
         for (var g = 0; g < guidUsed.length; g++)
         {
            if (guidUsed[g] === indx[c].GUID)
            {
               g = guidUsed.length;
               duplicateGuid = true;
            }
         }

         if (duplicateGuid)
         {
            continue;
         }

         newItemQty = (c === 0 ? 0 : newItemQty) + parseFloat(indx[c].itemQty);
         guidUsed.push(indx[c].GUID);
      }

      //var formQtyDiff = itemQnty - formQnt;
      var formQtyDiff = newItemQty - newFormQty;
      var $matchRateRow = $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='MatchRate']").closest("td");
      $matchRateRow.attr("rowspan", indx.length).html(formQtyDiff > 0 ? "+" + formQtyDiff : formQtyDiff);
      $matchRateRow.attr("exchange-value", formQtyDiff);
      $matchRateRow.css("background-color", formQtyDiff === 0 ? "Green" : "#cb7474");

      var $approveAllObj = $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='ApproveAll']");
      $approveAllObj.closest("td").attr("rowspan", indx.length).html($approveAllObj.html());

      var approvalCount = 0;

      if (indx.length > 1)
      {
         for (var k = 1; k < indx.length; k++)
         {
            var $childRow = $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[k].row + "']");
            $childRow.attr("data-parent-id", indx[0].row);
            $childRow.find("td:eq(6)").remove(); //remove the Match Rate from the given row
            $childRow.attr("data-parent-id", indx[0].row);
            $childRow.find("td:eq(5)").remove(); //remove the Approve button from the given row

            //edits - DMB
            //if the Approve button for the given item is hidden, replace it with Approved checkbox
            if ($childRow.find("td[exchange='Approve']").hasClass("approved"))
            {
               approvalCount++;

               if ($childRow.find("td[exchange='Approve']").attr("style").indexOf("#000") < 0)
               {
                  $childRow.find("#Approved_" + indx[k].row).removeClass("hidden");
               }
            }

            if (approvalCount === indx.length - 1)
            {
               for (var y = 1; y < indx.length; y++)
               {
                  $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[y].row + "']").hide();
               }
            }
         }
      }

      //edits - DMB
      //if the Approve button for the given item is hidden, replace it with Approved checkbox
      if ($(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='Approve']").hasClass("approved"))
      {
         $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='Approve']").find("#Approved_" + indx[0].row).removeClass("hidden");

         if (approvalCount === indx.length - 1)
         {
            $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='ApproveAll']").find("#ApprovedAll_" + indx[0].row).removeClass("hidden");
            $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "'] td[exchange='ApproveAll']").find(".ApprovedAllItem").hide();
            $(".searchGrid_Matching-div-click[MgmtEntity-id='" + indx[0].row + "']").hide();
         }
      }

      return [];
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Matching form222 using RA
   //*
   //* Parameters:
   //*   list - List of Matching form222 associated with RA
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_CLEAR", function ()
   {
      if (!getEditMode() || !inputChnge)
      {
         if ($(".tabContainer a.active").attr("href").indexOf("Summary") < 0)
         {
            $(".tabContainer a:first").click();
         }

         if ($(".searchGrid_Summary-div-click").hasClass("selected"))
         {
            $(".searchGrid_Summary-div-click.selected").click();
         }
         else
         {
            $(".formMax .inputEx").val("");
            $("#WrhsCtrl2DetailsForm").html($htmlMatching);
         }

         enableActionbarControl("#CMD_REFRESH", true);
         $("#RA").focus();
         validRANumber = "";

         if (getEditMode())
         {
            window.setEditMode(false);
         }
      }
   });

   // set up auto focus on RA if RA is empty
   setInterval(function ()
   {
      if ($("#RA").val() !== "")
      {
         return false;
      }
      else
      {
         $("#RA").select();
         $("#RA").focus();
      }
   }, 500);

   //********** added expand collapse functionality for matching but never used
   //*****************************************************************************
   //*
   //* Summary:
   //*   Matching Grid grouping Expand/Collapse
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".matchingGroup", function ()
   {
      var $expandedRow = $(".matchingExpansionRow[data-groupguid=" + $(this).attr("data-groupguid") + "]:visible");

      if ($expandedRow.length > 0)
      {
         removeExpandedRow($expandedRow);
         return;
      }

      var $rows = $($(this).parents(".dataTable").DataTable().rows().nodes());
      var i = $rows.index(this);

      $rows.eq(i).after($(this).find(".matchingExpansionRow").parent().html());
      $(".matchingExpansionRow[data-groupguid=" + $(this).attr("data-groupguid") + "]:visible div:first").slideDown();
   });

   function removeExpandedRow($row)
   {
      if ($row === null)
      {
         $row = $(this);
      }

      $row.find("div:first").slideUp("fast", function ()
      {
         $row.remove();
      });
   }
   //*********************************************************
});