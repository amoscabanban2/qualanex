﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UPAMgmtViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UPAMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "ADMIN_UPAMGMT";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first UPAConfig record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UPAMgmtModel SelectedUPAMgmtModel { get; set; } = new UPAMgmtModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   UPAConfig list for the UPAConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<UPAMgmtModel> UPAMgmtModels { get; set; } = new List<UPAMgmtModel>();


      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   UPAConfig list for dropdown list the UPAConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<UPAMgmtModel> UPAMgmtModelsItems { get; set; } = new List<UPAMgmtModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get UPAConfig and Relation Control data for Tab
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UPAMgmtRelationControl TabRelationObjects { get; set; } = new UPAMgmtRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get UPAConfig and Relation Control data for Segment
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UPAMgmtRelationControl SegmentRelationObjects { get; set; } = new UPAMgmtRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get UPAConfig and Relation Control data for Function
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UPAMgmtRelationControl FunctionRelationObjects { get; set; } = new UPAMgmtRelationControl();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UPAMgmtRelationControl
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl UPAConfigRelationControl { get; set; } = new RelationControl();
   }
}
