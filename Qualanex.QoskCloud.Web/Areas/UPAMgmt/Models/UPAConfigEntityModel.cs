﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UPAEntityModel.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UPAConfigMenu : IDisposable
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      public Dictionary<string, UPAMgmtModel> Menu;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public UPAConfigMenu()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~UPAConfigMenu()
      {
         this.Dispose(false);
      }

      #endregion

      #region Events

      ///////////////////////////////////////////////////////////////////////////////
      ///   No events defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;
                     ;
                  }
                  catch (Exception ex)
                  {
                     ;
                     ;
                     ;
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }
            }
         }
      }

      #endregion

      #region Event Handlers

      ///////////////////////////////////////////////////////////////////////////////
      ///   No event handlers defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="dbModel"></param>
      ///****************************************************************************
      public UPAConfigMenu Fetch(ref QoskCloud dbModel)
      {
         this.Menu = new Dictionary<string, UPAMgmtModel>(this.FetchItem(ref dbModel, new UPAMgmtModel { Code = null }).Submenu);

         return this;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="dbModel"></param>
      /// <param name="item"></param>
      ///****************************************************************************
      protected UPAMgmtModel FetchItem(ref QoskCloud dbModel, UPAMgmtModel item)
      {
         //foreach (var entity in (from appDict in dbModel.AppDict
         //                        where appDict.ParentCode.Equals(item.Code) && !appDict.IsDeleted
         //                        orderby appDict.SeqNo
         //                        select new UPAMgmtModel
         //                        {
         //                           Code = appDict.Code,
         //                           Description = appDict.Description,
         //                           DisplayFormat = appDict.DisplayFormat,
         //                           ParentCode = appDict.ParentCode,
         //                           ObjectID = appDict.ObjectID,
         //                           Tooltip = appDict.Tooltip,
         //                           URN = appDict.URN,
         //                           URITemplate = appDict.URITemplate

         //                        }).ToList())
         //{
         //   item.Submenu.Add(entity.Code, this.FetchItem(ref dbModel, entity));
         //}

         return item;
      }

      #endregion
   }
}