﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UPAMgmtModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------


namespace Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels
{
   using System;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System.Data.Entity.Validation;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;

   using Telerik.Reporting.Barcodes;
   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UPAMgmtModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      public readonly Dictionary<string, UPAMgmtModel> Submenu = new Dictionary<string, UPAMgmtModel>();
      private UPAConfigMenu _menu = new UPAConfigMenu();

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public UPAMgmtModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~UPAMgmtModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Code")]
      public string Code { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Description")]
      public string Description { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Display Format")]
      public string DisplayFormat { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Object ID")]
      public string ObjectID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Parent Code")]
      public string ParentCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tool Tip")]
      public string Tooltip { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"URN")]
      public string URN { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"URI Template")]
      public string URITemplate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Seq No")]
      public int SeqNo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Version")]
      public int Version { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"IsDeleted")]
      public string IsDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Start Date")]
      public string EffectiveStartDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"End Date")]
      public string EffectiveEndDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created By")]
      public string CreatedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created Date")]
      public string CreatedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified By")]
      public string ModifiedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified Date")]
      public string ModifiedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Area")]
      public string Area { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Controller")]
      public string Controller { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Action")]
      public string Action { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Route")]
      public string RouteValue { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////

      [DisplayName(@"Application Code")]
      public string AppCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tab Code")]
      public string TabCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Segment Code")]
      public string SegCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Function Code")]
      public string FuncCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Level")]
      public int Level { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetUPAMgmtEntities(bool isEnable)
      {
         var menuItems = new UPAMgmtModel();
         var listItem = new List<UPAMgmtModel>();
         var lvl = 0;

         using (var cloudEntities = new QoskCloud())
         {
            //var listParents = cloudEntities.AppDict.Where(c => c.IsDeleted == isEnable && (c.ParentCode == null || c.ParentCode == ""))
            //   .OrderBy(c => c.SeqNo)
            //   .Select(upaConfig => new
            //   {
            //      upaConfig.Code,
            //      upaConfig.Description,
            //      upaConfig.URN,
            //      upaConfig.DisplayFormat,
            //      upaConfig.ParentCode,
            //      upaConfig.SeqNo,
            //      upaConfig.Tooltip,
            //      upaConfig.ObjectID,
            //      upaConfig.URITemplate,
            //      upaConfig.Action,
            //      upaConfig.Controller,
            //      upaConfig.Area,
            //      upaConfig.RouteValue
            //   })
            //   .ToList();

            //foreach (var item in listParents)
            //{
            //   listItem.Add(new UPAMgmtModel
            //   {
            //      Code = item.Code,
            //      Description = item.Description,
            //      URN = item.URN,
            //      DisplayFormat = item.DisplayFormat,
            //      ParentCode = item.ParentCode,
            //      SeqNo = (int)item.SeqNo,
            //      Tooltip = item.Tooltip,
            //      ObjectID = item.ObjectID,
            //      URITemplate = item.URITemplate,
            //      Action = item.Action,
            //      Controller = item.Controller,
            //      Area = item.Area,
            //      RouteValue = item.RouteValue,
            //      Level = lvl
            //   });

            //   if (cloudEntities.AppDict.Where(c => c.ParentCode == item.Code).Select(c => c.Code).Any())
            //   {
            //      menuItems.GetChildList(item.Code, cloudEntities, 1, listItem);
            //   }
            //}

            return listItem;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> UpdateUPAMgmtEntity(long userID)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.AppDict
                       //orderby UPAConfig.SeqNo ascending
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.Code,
                       CreatedBy = UPAConfig.CreatedBy,
                       Description = UPAConfig.Description,
                       //DisplayFormat = UPAConfig.DisplayFormat,
                       IsDeleted = UPAConfig.IsDeleted.ToString(),
                       ModifiedBy = UPAConfig.ModifiedBy,
                       ObjectID = UPAConfig.ObjectID,
                       //ParentCode = UPAConfig.ParentCode,
                       //SeqNo = (int)UPAConfig.SeqNo,
                       //Tooltip = UPAConfig.Tooltip,
                       URITemplate = UPAConfig.URITemplate,
                       URN = UPAConfig.URN,
                       Version = UPAConfig.Version
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetTabDictLists(string appCode, bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = cloudEntities.TabDict.Select(c => new { c.Code, c.Description }).ToList();
            var items = cloudEntities.AppTabRel.Where(c => c.AppCode == appCode && c.IsDeleted == isEnable).Select(c => c.TabCode).ToList();
            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "-------- Add New Tab --------",
                  Value = "Add", Disabled = true
               }
            };

            selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
            selectedItems.Where(c => items.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetUPAMgmtTabEntity(bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.TabDict
                    where UPAConfig.IsDeleted == isEnable
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.Code,
                       Description = UPAConfig.Description,
                       DisplayFormat = UPAConfig.DisplayFormat,
                       ObjectID = UPAConfig.ObjectID,
                       Tooltip = UPAConfig.Tooltip
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <param name="tabCode"></param>
      /// <param name="segmentLists"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetSegmentDictLists(string appCode, bool isEnable, string tabCode, List<UPAMgmtModel> segmentLists)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = cloudEntities.SegmentDict.Select(c => new { c.Code, c.Description }).ToList();
            var items = segmentLists?.Where(c => c.AppCode == appCode).Select(c => c.SegCode).ToList() ?? cloudEntities.TabSegRel.Where(c => c.AppCode == appCode && c.IsDeleted == isEnable && c.TabCode == tabCode).Select(c => c.SegCode).ToList();
            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "----- Add New Segment -----",
                  Value = "Add", Disabled = true
               }
            };

            selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
            //selectedItems.Where(c => items.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetSegmentDictLists()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = cloudEntities.SegmentDict.Select(c => new { c.Code, c.Description }).ToList();
            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "----- Add New Segment -----",
                  Value = "Add", Disabled = true
               }
            };

            selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });

            return selectedItems;
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetUPAMgmtSeqEntity(bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.SegmentDict
                    where UPAConfig.IsDeleted == isEnable
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.Code,
                       Description = UPAConfig.Description,
                       DisplayFormat = UPAConfig.DisplayFormat,
                       ObjectID = UPAConfig.ObjectID,
                       Tooltip = UPAConfig.Tooltip
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <param name="tabCode"></param>
      /// <param name="appCode"></param>
      /// <param name="segCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetUPAMgmtFunction(bool isEnable, string appCode, string segCode, string tabCode)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var items = cloudEntities.TabSegRel.Where(c => c.AppCode == appCode && c.SegCode == segCode && c.TabCode == tabCode).Select(c => c.SegCode).ToList();
            var query = cloudEntities.SegFuncRel.Where(c => c.IsDeleted == isEnable && c.AppCode == appCode && items.Contains(c.SegCode)).OrderBy(c => c.SeqNo).Select(UPAConfig => new
            {
               Code = UPAConfig.AppCode,
               CreatedBy = UPAConfig.CreatedBy,
               IsDeleted = UPAConfig.IsDeleted.ToString(),
               ModifiedBy = UPAConfig.ModifiedBy,
               SeqNo = (int)UPAConfig.SeqNo,
               SegCode = UPAConfig.SegCode,
               FuncCode = UPAConfig.FuncCode
            }).ToList();

            return query.Select(t => new UPAMgmtModel
            {
               Code = t.Code,
               CreatedBy = t.CreatedBy,
               IsDeleted = t.IsDeleted.ToString(),
               ModifiedBy = t.ModifiedBy,
               SeqNo = t.SeqNo,
               SegCode = t.SegCode,
               FuncCode = t.FuncCode
            }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <param name="segCode"></param>
      /// <param name="tabCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetFunctionDictsLists(string appCode, bool isEnable, string segCode, string tabCode)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var items = GetUPAMgmtFunction(isEnable, appCode, segCode, tabCode).Select(c => c.FuncCode).ToList();
            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "-------- Delete --------",
                  Value = ""
               }
            };

            var query = cloudEntities.FunctionDict.Select(c => new { c.Code, c.Description }).ToList();

            selectedItems.AddRange(query.Select(i => new SelectListItem { Value = i.Code, Text = i.Description }));
            selectedItems.Where(c => items.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetFunctionDictsLists()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = cloudEntities.FunctionDict.Select(c => new { c.Code, c.Description }).ToList();
            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "----- Add New Function -----",
                  Value = "Add",Disabled = true
               }
            };

            selectedItems.AddRange(query.Select(i => new SelectListItem() { Value = i.Code, Text = i.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetUPAMgmtFunctionEntity(bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.FunctionDict
                    where UPAConfig.IsDeleted == isEnable
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.Code,
                       Description = UPAConfig.Description,
                       ObjectID = UPAConfig.ObjectID,
                       Tooltip = UPAConfig.Tooltip
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetTabList(string appCode, bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.AppTabRel
                    where UPAConfig.IsDeleted == isEnable && UPAConfig.AppCode == appCode
                    orderby UPAConfig.SeqNo
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.TabCode,
                       TabCode = UPAConfig.TabCode,
                       SeqNo = UPAConfig.SeqNo,
                       AppCode = UPAConfig.AppCode,
                       Version = UPAConfig.Version,
                       ModifiedBy = UPAConfig.ModifiedBy
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetSegmentList(string appCode, bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.TabSegRel
                    where UPAConfig.IsDeleted == isEnable && UPAConfig.AppCode == appCode
                    orderby UPAConfig.SeqNo
                    select new UPAMgmtModel()
                    {
                       Code = UPAConfig.SegCode,
                       CreatedBy = UPAConfig.CreatedBy,
                       IsDeleted = UPAConfig.IsDeleted.ToString(),
                       ModifiedBy = UPAConfig.ModifiedBy,
                       SeqNo = (int)UPAConfig.SeqNo,
                       SegCode = UPAConfig.SegCode,
                       TabCode = UPAConfig.TabCode,
                       AppCode = UPAConfig.AppCode
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<UPAMgmtModel> GetFunctionList(string appCode, bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from UPAConfig in cloudEntities.SegFuncRel
                    where UPAConfig.IsDeleted == isEnable && UPAConfig.AppCode == appCode
                    orderby UPAConfig.SeqNo
                    select new UPAMgmtModel
                    {
                       Code = UPAConfig.FuncCode,
                       SegCode = UPAConfig.SegCode,
                       SeqNo = UPAConfig.SeqNo,
                       AppCode = UPAConfig.AppCode,
                       FuncCode = UPAConfig.FuncCode,
                       ModifiedBy = UPAConfig.ModifiedBy,
                       IsDeleted = UPAConfig.IsDeleted.ToString(),
                       CreatedBy = UPAConfig.CreatedBy
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="items"></param>
      /// <param name="dbModel"></param>
      /// <param name="level"></param>
      /// <param name="listItem"></param>
      /// <returns></returns>
      ///****************************************************************************
      public void GetChildList(string items, QoskCloud dbModel, int level, List<UPAMgmtModel> listItem)
      {
         //var lstChild = dbModel.AppDict.Where(c => c.ParentCode == items)
         //   .OrderBy(c => c.SeqNo)
         //     .Select(i => new
         //     {
         //        i.Code,
         //        i.Description,
         //        i.URN,
         //        i.DisplayFormat,
         //        i.ParentCode,
         //        i.SeqNo,
         //        i.Tooltip,
         //        i.ObjectID,
         //        i.URITemplate,
         //        i.Action,
         //        i.Controller,
         //        i.Area,
         //        i.RouteValue,
         //        level
         //     }).ToList();

         //foreach (var i in lstChild)
         //{
         //   listItem.Add(new UPAMgmtModel
         //   {
         //      Code = i.Code,
         //      Description = i.Description,
         //      URN = i.URN,
         //      DisplayFormat = i.DisplayFormat,
         //      ParentCode = i.ParentCode,
         //      SeqNo = (int)i.SeqNo,
         //      Tooltip = i.Tooltip,
         //      ObjectID = i.ObjectID,
         //      URITemplate = i.URITemplate,
         //      Action = i.Action,
         //      Controller = i.Controller,
         //      Area = i.Area,
         //      RouteValue = i.RouteValue,
         //      Level = level
         //   });

         //   if (dbModel.AppDict.Where(c => c.ParentCode == i.Code).Select(c => c.Code).Any())
         //   {
         //      this.GetChildList(i.Code, dbModel, level + 1, listItem);
         //   }
         //}
      }

      ///****************************************************************************
      /// <summary>
      ///   save configuration to database.
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SetConfiguration(List<UPAMgmtModel> itemList, string curUser)
      {
         var status = false;
         var appCode = itemList.Select(c => c.AppCode).FirstOrDefault();
         var newFunctionObjects = itemList.Where(c => c.FuncCode != null && c.SegCode != null).ToList();
         var newSegmentObjects = itemList.Where(c => c.SegCode != null && c.FuncCode == null && c.TabCode != null).ToList();
         var newTabObjects = itemList.Where(c => c.SegCode == null && c.FuncCode == null && c.TabCode != null).ToList();

         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var deleteFunctionObjects = cloudEntities.SegFuncRel.Where(c => c.AppCode == appCode).ToList();

               if (deleteFunctionObjects.Count > 0)
               {
                  cloudEntities.SegFuncRel.RemoveRange(deleteFunctionObjects);
               }

               var deleteSegmentObjects = cloudEntities.TabSegRel.Where(c => c.AppCode == appCode).ToList();

               if (deleteSegmentObjects.Count > 0)
               {
                  cloudEntities.TabSegRel.RemoveRange(deleteSegmentObjects);
               }

               var deleteTabObjects = cloudEntities.AppTabRel.Where(c => c.AppCode == appCode).ToList();

               if (deleteTabObjects.Count > 0)
               {
                  cloudEntities.AppTabRel.RemoveRange(deleteTabObjects);
               }

               cloudEntities.SaveChanges();

               if (newTabObjects.Count > 0)
               {
                  SaveToAppTabTable(newTabObjects, cloudEntities, curUser);
               }

               if (newSegmentObjects.Count > 0)
               {
                  SaveToTabSegTable(newSegmentObjects, cloudEntities, curUser);
               }

               if (newFunctionObjects.Count > 0)
               {
                  SaveToSegFuncTable(newFunctionObjects, cloudEntities, curUser);
               }

               status = true;
            }
            catch (Exception ex)
            {
               Utility.Logger.Error(ex);
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   save to SegFunc Table
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToSegFuncTable(List<UPAMgmtModel> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var segFuncObject = new SegFuncRel
               {
                  AppCode = items.AppCode,
                  SegCode = items.SegCode,
                  FuncCode = items.FuncCode,
                  SeqNo = (byte)(items.Level == -1
                  ? items.SeqNo = ((itemList.Where(c => c.TabCode == items.TabCode &&
                  c.SegCode == items.SegCode).Max(c => (int?)c.SeqNo) ?? 0) + 1)
                  : items.SeqNo),
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser

               };
               db.SegFuncRel.Add(segFuncObject);
            }
            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save to TabSegRel table
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToTabSegTable(List<UPAMgmtModel> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var tabSegObject = new TabSegRel()
               {
                  AppCode = items.AppCode,
                  SegCode = items.SegCode,
                  TabCode = items.TabCode,
                  SeqNo = (byte)(items.Level == -1
                  ? items.SeqNo = ((itemList.Where(c => c.TabCode == items.TabCode)
                  .Max(c => (int?)c.SeqNo) ?? 0) + 1)
                  : items.SeqNo),
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser
               };
               db.TabSegRel.Add(tabSegObject);
            }
            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save to AppTabRel table.
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToAppTabTable(List<UPAMgmtModel> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var appTabObject = new AppTabRel()
               {
                  AppCode = items.AppCode,
                  TabCode = items.TabCode,
                  SeqNo = (byte)(items.Level == -1
                  ? items.SeqNo = ((itemList.Max(c => (int?)c.SeqNo) ?? 0) + 1)
                  : items.SeqNo),
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser
               };
               db.AppTabRel.Add(appTabObject);

            }
            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check validation for Code.
      /// </summary>
      /// <param name="code"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckCode(string code)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         var count = cloudEntities.AppDict.Count(t => t.Code == code);

         return count > 0
                     ? false
                     : true;
      }

      ///****************************************************************************
      /// <summary>
      ///   save new application to app dict table.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool NewUpaSummary(string curUser, string bodyData)
      {
         var status = false;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               var seqNo = GetOneValue(bodyData, "ID_SEQNO") == "0"
                  ? 0
                  : Convert.ToInt32(GetOneValue(bodyData, "ID_SEQNO"));
               if (string.IsNullOrWhiteSpace(GetOneValue(bodyData, "ID_PARENTCODE")))
               {
                  cloudEntities.Database.ExecuteSqlCommand("update AppDict set SeqNo=SeqNo+1 where SeqNo>=" + seqNo +
                                                           " and ((ParentCode IS NULL) OR (ParentCode=''))");
               }
               else
               {
                  cloudEntities.Database.ExecuteSqlCommand("update AppDict set SeqNo=SeqNo+1 where SeqNo>=" + seqNo +
                                                           " and ParentCode='" + GetOneValue(bodyData, "ID_PARENTCODE") +
                                                           "'");
               }

               var appDict = new AppDict
               {
                  Code = GetOneValue(bodyData, "ID_UPACODE").ToString().Trim(),
                  //                  ParentCode = GetOneValue(bodyData, "ID_PARENTCODE"),
                  //                  SeqNo = (byte)(seqNo),
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser,
                  ObjectID = "",
                  //                  DisplayFormat = GetOneValue(bodyData, "ID_DISPLAY"),
                  Description = GetOneValue(bodyData, "ID_DESCR"),
                  URN = GetOneValue(bodyData, "ID_URN"),
                  URITemplate = GetOneValue(bodyData, "ID_URI"),
                  Area = GetOneValue(bodyData, "ID_AREA"),
                  Controller = GetOneValue(bodyData, "ID_CTRLR"),
                  Action = GetOneValue(bodyData, "ID_ACTION"),
                  RouteValue = GetOneValue(bodyData, "ID_ROUTE")
               };

               cloudEntities.AppDict.Add(appDict);
               cloudEntities.SaveChanges();
               status = true;
            }
            catch (Exception ex)
            {
               Utility.Logger.Error(ex);
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   get Max SeqNo by parentCode.
      /// </summary>
      /// <param name="parentCode"></param>
      /// <param name="db"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetMaxSeqNo(string parentCode, QoskCloud db)
      {
         return 0;
         //return (string.IsNullOrWhiteSpace(parentCode)
         //   ? db.AppDict.Where(c => (c.ParentCode == null || c.ParentCode == ""))
         //      .Max(c => (int)c.SeqNo)
         //   : db.AppDict.Where(c => c.ParentCode == parentCode)
         //      .Max(c => (int)c.SeqNo));
      }

      ///****************************************************************************
      /// <summary>
      ///   save new application to app dict table.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="code"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool UpdateUpaSummary(string curUser, string bodyData, string code)
      {
         var status = false;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var existApplication = (from App in cloudEntities.AppDict
                                    where App.Code == code
                                    select App).SingleOrDefault();

            if (existApplication != null)
            {
               existApplication.ModifiedBy = curUser;
               //existApplication.DisplayFormat = GetOneValue(bodyData, "ID_DISPLAY");
               existApplication.Description = GetOneValue(bodyData, "ID_DESCR");
               existApplication.URN = GetOneValue(bodyData, "ID_URN");
               existApplication.URITemplate = GetOneValue(bodyData, "ID_URI");
               existApplication.Area = GetOneValue(bodyData, "ID_AREA");
               existApplication.Controller = GetOneValue(bodyData, "ID_CTRLR");
               existApplication.Action = GetOneValue(bodyData, "ID_ACTION");
               existApplication.RouteValue = GetOneValue(bodyData, "ID_ROUTE");

               try
               {
                  cloudEntities.SaveChanges();
                  status = true;
               }
               catch (Exception ex)
               {
                  Utility.Logger.Error(ex);
               }
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = "";
         char[] spComma = { ',' };
         char[] spColon = { ':' };

         if (inStr != null)
         {
            foreach (var onePair in from oneField in inStr.Replace("\"", "").Split(spComma)
                                    select oneField.Split(spColon)
                                    into onePair
                                    let id = onePair[0].Trim()
                                    where id.Length > 0
                                    where id == chkStr
                                    select onePair)
            {
               return onePair[1].Trim();
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="dbModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public UPAConfigMenu FetchMenu(ref QoskCloud dbModel)
      {
         return this._menu.Fetch(ref dbModel);
      }

      #endregion
   }

}
