﻿$(function()
{
   //*****************************************************************************
   //*
   //* UPAMgmt.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the UPAMgmt partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************

   var upaId;
   var baseURL = "/UPAMgmt";
   var table;
   var editMode = "";
   var $newRow = "";
   var active = "";
   var valid = false;
   var txtChanges = false;
   var parentCode = "";
   var btnIndex = 1;
   var rowIndex = 0;
   var seqNo = 0;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None._
   //*
   //*****************************************************************************
   $(document).ready(function()
   {
      var table = window.dtHeaders("#searchGrid", "", ["sorting"], ["click"]);
      $(document).off("click", ".relationDisplayRow");

      $newRow = $("#searchGrid tbody tr.hidden").closest("tr");
      initialState();
   });

   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Refresh button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_REFRESH").unbind("click").click(function()
   {
      if (getEditMode())
      {
         return false;
      }

      if ($("a.active").attr("href") !== "#tabs-Config" && active === "")
      {
         var url = "ViewDataRequest";

         $.ajax({
            type: "POST",
            url: url,
            data: "isEnable=" + false + getAntiForgeryToken(),
            success: function(data)
            {
               var $target = $("#UPAMgmtGridSummary");
               var $newHtml = $(data);
               $target.replaceWith($newHtml);
               table = window.dtHeaders("#searchGrid", "", ["sorting"], ["click"]);
               if (!table.$("tr.selected").hasClass("selected"))
               {
                  $newHtml.find("#searchGrid tbody").find("tr:eq(1)").click();
               }
               $newHtml.find("#searchGrid thead tr th").removeClass("sorting").off("click");
            }
         });

         return true;
      }

      return false;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on New button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_NEW").unbind("click").click(function()
   {
      if ($("a.active").attr("href") === "#tabs-Config")
      {
         return false;
      }

      if (!getEditMode())
      {
         active = "summary";
         var blnClick = false;
         var i = 0;
         upaId = $("#searchGrid tbody tr.selected td[data-field-name=Code]").attr("data-field-value");
         if (parseInt($("#searchGrid tbody tr.selected")
            .find("td[data-field-name=Level]")
            .attr("data-field-value")) > 0)
         {
            $newRow.find("td[data-field-name=Level]")
               .attr("data-field-value", $("#searchGrid tbody tr.selected")
                  .find("td[data-field-name=Level]")
                  .attr("data-field-value"));
            parentCode = $("#searchGrid tbody tr.selected")
               .find("td[data-field-name=ParentCode]")
               .attr("data-field-value");

            $newRow.find("td[data-field-name=ParentCode]").closest("td")
               .attr("data-field-value", parentCode);
            $newRow.find("td[data-field-name=ParentCode]").closest("td")
            .html(parentCode);
         }

         else
         {
            parentCode = "";
         }
         seqNo = $("#searchGrid tbody tr.selected")
            .find("td[data-field-name=SeqNo]")
            .attr("data-field-value");
         $newRow.find("td[data-field-name=SeqNo]")
               .attr("data-field-value",
               seqNo);

         $newRow.find("td").each(function()
         {
            if ($(this).attr("data-field-name") !== "ParentCode")
            {
               $(this).closest("td").attr("data-field-value", "");
               $(this).html('');
            }
         });
         rowIndex = $("#searchGrid tbody tr.selected").closest("tr").index();
         $("#searchGrid tbody tr.selected").closest("tr").removeClass("selected");
         $newRow.removeClass("hidden").addClass("selected");
         $newRow.insertBefore($("#searchGrid tbody tr:eq(" + rowIndex + ")").closest("tr"));

         enableActionbarControl("#CMD_REFRESH", false);
         setEditMode(true);
         clearTextBoxes();
         return true;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Edit button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_EDIT").unbind("click").click(function()
   {
      if (getEditMode())
      {
         return false;
      }
      enableActionbarControl("#CMD_REFRESH", false);
      setEditMode(true);
      editMode = "Edit";
      if ($("a.active").attr("href") === "#tabs-Config")
      {
         if ($("#tabRelationControl").val() === undefined)
         {
            return false;
         }
         active = "config";
         var tabUnique = $("#tabRelationControl table tr.selected ").attr("data-unique-selection");
         var tabRowId = $("#tabRelationControl table tr.selected ").attr("data-row-id");
         var segmentUnique = $("#segmentRelationControlItems table tr.selected ").attr("data-unique-selection");
         var segmentRowId = $("#segmentRelationControlItems table tr.selected ").attr("data-row-id");
         var functionUnique = $("#functionRelationControlItems table tr.selected ").attr("data-unique-selection");
         var functionRowId = $("#functionRelationControlItems table tr.selected ").attr("data-row-id");
         $(".relationControl").enableRelationEdit();
         $("#tabRelationControl tr.blank select[name='TabCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $("#segmentRelationControlItems tr.blank select[name='SegCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $("#functionRelationControlItems table tr.blank select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $("#tabRelationControl td input[type='number']").css("width", "100%");
         $("#segmentRelationControlItems td input[type='number']").css("width", "100%");
         $("#segmentRelationControlItems td select[name='SegCode']").css("width", "100%");
         $("#functionRelationControlItems td input[type='number']").css("width", "100%");
         $("#functionRelationControlItems td input[type='number']").closest("td").css("width", "34%");
         $("#functionRelationControlItems td select[name='FuncCode']").css("width", "100%");
         $("#tabRelationControl table tr input.image_tab_relation")
            .each(function()
            {
               $(this).attr("data-field-type", "TabCode");
            });
         $("#SegmentRelationControl table tr input.image_tab_relation")
            .each(function()
            {
               $(this).attr("data-field-type", "SegCode");
            });
         setCssToDropDownLists("tabRelationControl", "TabCode", "font-weight", "bold");
         setCssToDropDownLists("SegmentRelationControlItems", "SegCode", "font-weight", "bold");
         setCssToDropDownLists("functionRelationControl", "FuncCode", "font-weight", "bold");
         $("#tabRelationControl table tr ")
            .each(function()
            {
               if ($(this).attr("data-unique-selection") === tabUnique && $(this).attr("data-row-id") === tabRowId && tabRowId !== undefined)
               {
                  $(this).addClass("selected");
               }
            });

         $("input[Type=Text],.inputEx").attr("readonly", "readonly");
         if (!$("#tabRelationControl tr").hasClass("selected"))
         {
            $("#segmentRelationControlItems .relationControl").disableRelationEdit();
         }

         if (!$("#segmentRelationControlItems tr").hasClass("selected"))
         {
            $("#functionRelationControlItems .relationControl").disableRelationEdit();
         }

         setOptionSegment(segmentUnique, segmentRowId);
         setOptionFunction(functionUnique, functionRowId);
      }

      else if ($("a.active").attr("href") === "#tabs-summary")
      {
         active = "summary";
         $("#ID_UPACODE").prop("readonly", true);
         upaId = $("#searchGrid tbody tr.selected td[data-field-name=Code]")
            .attr("data-field-value");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Save button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_SAVE").unbind("click").click(function()
   {
      if (getEditMode())
      {
         if (window.getFormChanged())
         {
            if (active === "config")
            {
               enableActionbarControl("#CMD_SAVE", false);
               enableActionbarControl("#CMD_CANCEL", false);
               var itemLists = [];
               var relationObjects = {};
               var appCode = document.getElementById("ID_UPACODE").value;
               $("#tabRelationControl table tbody tr.relationEditRow,#tabRelationControl table tbody tr.updated").each(function()
               {
                  relationObjects = {
                     "AppCode": appCode,
                     "SeqNo": $(this).find("td input[name=SeqNo]").closest("input").val().length > 0
                        ? $(this).find("td input[name=SeqNo]").closest("input").val() :
                        0,
                     "TabCode": $(this).find("td input[name=Code]").closest("input").val(),
                     "Level": $(this).find("td input[name=SeqNo]").closest("input").val().length > 0
                        ? 0
                        : -1,
                  }
                  itemLists.push(relationObjects);
               });
               relationObjects = {};
               $("#segmentRelationControlItems table tbody tr.relationEditRow,#segmentRelationControlItems table tbody tr.updated").each(function()
               {
                  relationObjects = {
                     "AppCode": appCode,
                     "SeqNo": $(this).find("td input[name=SeqNo]").closest("input").val() > 0
                        ? $(this).find("td input[name=SeqNo]").closest("input").val() :
                        0,
                     "TabCode": $(this).find("td input[name=TabCode]").closest("input").val(),
                     "SegCode": $(this).find("td input[name=Code]").closest("input").val(),
                     "Level": $(this).find("td input[name=SeqNo]").closest("input").val().length > 0
                        ? 0
                        : -1,
                  }
                  itemLists.push(relationObjects);
               });
               relationObjects = {};
               $("#functionRelationControlItems table tbody tr.relationEditRow,#functionRelationControlItems table tbody tr.updated").each(function()
               {
                  relationObjects = {
                     "AppCode": appCode,
                     "SeqNo": $(this).find("td input[name=SeqNo]").closest("input").val() > 0
                        ? $(this).find("td input[name=SeqNo]").closest("input").val() :
                        0,
                     "SegCode": $(this).find("td input[name=SegCode]").closest("input").val(),
                     "FuncCode": $(this).find("td input[name=Code]").closest("input").val(),
                     "Level": $(this).find("td input[name=SeqNo]").closest("input").val().length > 0
                        ? 0
                        : -1,
                  }
                  itemLists.push(relationObjects);
               });
               if (itemLists.length === 0)
               {
                  itemLists.push({ "AppCode": appCode });
               }

               $.ajax({
                  dataType: "json",
                  type: "POST",
                  url: baseURL + "/UPAMgmt/SaveUPAConfigDetail",
                  data: "upaConfigEntity=" + JSON.stringify(itemLists) + getAntiForgeryToken(),
                  success: function(data)
                  {
                     if (data.Success)
                     {
                        $(".relationControl").disableRelationEdit();

                        $(".relationControl table tbody tr").removeClass("selected");
                        enableActionbarControl("#CMD_REFRESH", true);
                        editMode = "";
                        configurationSection(document.getElementById("ID_UPACODE").value);
                        setEditMode(false);
                        active = "";
                     }

                     else
                     {
                        enableActionbarControl("#CMD_SAVE", true);
                        enableActionbarControl("#CMD_CANCEL", true);
                     }
                     return false;
                  },
                  error: function(data)
                  {
                     enableActionbarControl("#CMD_SAVE", true);
                     enableActionbarControl("#CMD_CANCEL", true);
                     showError("The controller call fails on save group request.");
                  }
               });
            }

            else if (active === "summary")
            {
               if ($(".formMax .inputEx").hasClass("inputError"))
               {
                  return false;
               }

               $(".formMax .inputEx").each(function()
               {
                  switch ($(this).closest("input").attr("id"))
                  {
                     case "ID_UPACODE":
                        {
                           if ($(this).val().length === 0)
                           {
                              setColorAndClass("#FFFFC0CB", "#ID_UPACODE", "inputError");
                              enableActionbarControl("#CMD_SAVE", false);
                           }
                           break;
                        }
                     case "ID_DISPLAY":
                        {
                           if ($(this).val().length === 0)
                           {
                              setColorAndClass("#FFFFC0CB", "#ID_DISPLAY", "inputError");
                              enableActionbarControl("#CMD_SAVE", false);
                           }
                           break;
                        }
                  }
               });

               if ($(".formMax .inputEx").hasClass("inputError"))
               {
                  return false;
               }

               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to save the changes?</h3>"
               }];
               btnObjects = [{
                  "id": "btn_upa_saving",
                  "name": "Yes",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }, {
                  "id": "btn_no",
                  "name": "No",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
               ];

               titleobjects = [{
                  "title": "Confirm"
               }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Cancel button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_upa_saving", function()
   {
      $(this).closeMessageBox();
      var bodyData = getBodyData();
      if (upaId === undefined)
      {
         return false;
      }
      if ($("#ID_UPACODE").attr("readonly") === undefined)
      {
         upaId = "0";
         bodyData = bodyData + '"ID_SEQNO" : "' + seqNo + '",';
      }
      bodyData = bodyData + '"ID_PARENTCODE" : "' +
         (parentCode.length > 0
         ? parentCode.toUpperCase()
         : "") +
         '",';

      $.ajax({
         type: "POST",
         url: baseURL + "/UPAMgmt/SaveUpaSummaryDetail",
         data: "bodyData=" + bodyData +
            "&code=" + upaId +
            getAntiForgeryToken(),
         datatype: "JSON",
         success: function(data)
         {
            if (data.Success)
            {
               clearTextBoxes();
               parentCode = "";
               enableActionbarControl("#CMD_REFRESH", true);
               $(".formBody .inputEx")
                  .removeClass("editable")
                  .addClass("editable");
               $("#UPAMgmtGridSummary #searchGrid_length select").removeAttr("disabled");
               active = "";
               setEditMode(false);
               upaId = data.Code;
               refreshGridByCode(data.Code);
               txtChanges = false;
            }
         },
         error: function()
         {
            showError("The controller call fails on save group request.");
         }
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Cancel button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_CANCEL").unbind("click").click(function()
   {
      if (getEditMode())
      {
         editMode = "";
         if (active === "config")
         {
            configurationSection(document.getElementById("ID_UPACODE").value);
            setEditMode(false);
            enableActionbarControl("#CMD_REFRESH", true);
            active = "";
         }

         else if (active === "summary")
         {
            if (window.getFormChanged())
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
               }];
               btnObjects = [{
                  "id": "btn_cancel_upa_saving",
                  "name": "Yes",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }, {
                  "id": "btn_no",
                  "name": "No",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
               ];

               titleobjects = [{
                  "title": "Confirm Cancellation"
               }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }

            else
            {
               if ($("#ID_UPACODE").attr("readonly") !== "readonly")
               {
                  setEditMode(false);
                  window.enableActionbarControl("#CMD_REFRESH", true);
                  $("#UPAMgmtGridSummary #searchGrid_length select").removeAttr("disabled");
                  parentCode = "";
                  $("#searchGrid tbody tr").removeClass("selected");

                  $(".formMax .inputEx").removeAttr("style").removeClass("inputError");
                  if (!$("#searchGrid tbody tr.selected[data-row-id=0]").hasClass("hidden"))
                  {
                     if ($("#searchGrid tbody tr.selected[data-row-id=0]").closest("tr").index() > 0)
                     {
                        $("#searchGrid tbody tr[data-row-id=0]").removeClass("selected").detach();
                     }

                     $newRow.addClass("hidden").addClass("odd");
                     $newRow.find("td").each(function()
                     {
                        $(this).attr("data-field-value", "");
                     });

                  }
                  if (!$("#searchGrid tbody tr").hasClass("selected"))
                  {
                     //$("#searchGrid_paginate span a")[0].click();
                     findRow(upaId, 0);
                     $("#searchGrid tbody tr.selected").click();
                  }
               }

               else
               {
                  initialState();
                  findRow(upaId, 0);
                  $("#searchGrid tbody tr.selected").click();
               }

               txtChanges = false;
               active = "";
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Print button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_cancel_upa_saving", function()
   {
      $("#UPAMgmtGridSummary #searchGrid_length select").removeAttr("disabled");
      parentCode = "";
      setEditMode(false);
      active = "";
      $(".formMax .inputEx").removeAttr("style").removeClass("inputError");
      $(this).closeMessageBox();
      setEditMode(false);
      refreshGridByCode(upaId);
      enableActionbarControl("#CMD_REFRESH", true);
      txtChanges = false;
      $newRow.find("td").each(function()
      {
         $(this).html('');
         $(this).closest("td").attr("data-field-value", "");
      });
      $newRow.addClass("odd").addClass("hidden");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Print button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_PRINT").unbind("click").click(function()
   {
      alert("Print under development");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set the Initial state of the active tool buttons.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function initialState()
   {
      setEditMode(false);
      window.enableActionbarControl("#CMD_REFRESH", true);
      if (!$("#searchGrid tbody tr").hasClass("selected"))
      {
         $("#searchGrid tbody").find("tr:eq(1)").click();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Function to handle when a UPAMgmt's detail is selected form the table.
   //*
   //* Parameters:
   //*   e                - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".UPAMgmtDetail-div-click", function(e)
   {
      if (!getEditMode())
      {
         setClickedRowToTextbox($(this).attr("data-row-id"), $(this).closest("tr"));
         if ($(this).hasClass("selected"))
         {
            $(this).removeClass("selected").addClass("selected");
         }

         else
         {
            table.$("tr.selected").removeClass("selected");
            $(this).addClass("selected");
         }

         if ($("a.active").attr("href") === "#tabs-Config")
         {
            setUpaIdCallConfiguration();
         }
      }

      else
      {
         if (active === "summary")
         {
            if ($("#CMD_NEW").attr("src").indexOf("(0)") > 0
               || $("#CMD_EDIT").attr("src").indexOf("(0)") > 0)
            {
               $("#ID_UPACODE").attr("readonly") === undefined
               ? $(this).setErrorMessage("You are currently adding an application", "Error Message", "Continue")
               : $(this).setErrorMessage("You are currently editing an application", "Error Message", "Continue");
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   SerializeObject Function to select a Form by ID, serialize the content,
   //*   clean it, and send it back as an object.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   (function($)
   {
      $.fn.serializeObject = function()
      {
         var self = this,
            json = {},
            push_counters = {},
            patterns = {
               "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
               "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
               "push": /^$/,
               "fixed": /^\d+$/,
               "named": /^[a-zA-Z0-9_]+$/
            };

         this.build = function(base, key, value)
         {
            base[key] = value;
            return base;
         };

         this.push_counter = function(key)
         {
            if (push_counters[key] === undefined)
            {
               push_counters[key] = 0;
            }

            return push_counters[key]++;
         };

         $.each($(this).serializeArray(), function()
         {
            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
               return;
            }

            var k,
               keys = this.name.match(patterns.key),
               merge = this.value,
               reverse_key = this.name;

            while ((k = keys.pop()) !== undefined)
            {
               // adjust reverse_key
               reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), "");

               // push
               if (k.match(patterns.push))
               {
                  merge = self.build([], self.push_counter(reverse_key), merge);
               }
                  // fixed
               else if (k.match(patterns.fixed))
               {
                  merge = self.build([], k, merge);
               }
                  // named
               else if (k.match(patterns.named))
               {
                  merge = self.build({}, k, merge);
               }
            }

            json = $.extend(true, json, merge);
         });

         return json;
      };
   })(jQuery);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".tabContainer a", function()
   {
      if ($("a.active").attr("href") === "#tabs-Config"
         && $(this).attr("id") === "btn_tab_config")
      {
         $(".ql-body.active").css("overflow", "auto");
         if (!getEditMode())
         {
            setUpaIdCallConfiguration();
         }
         else
         {
            if (active === "summary")
            {
               $("#tabRelationControl,#segmentRelationControlItems,#functionRelationControlItems").css("display", "none");
            }
         }
      }
      else
      {
         $("#dataTables_scrollHeadInner").css("width", "100%");
         $("#dataTables_scrollHeadInner table").css("width", "100%");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Function for Change Configuration Tab.
   //*
   //* Parameters:
   //*   appCode          - AppCode for find tabs with this AppCode.
   //*   rowsNo           - Row Number in tab for selecting.
   //*   rowsSegNo        - Row Number in segment for selecting.
   //*   rowsFuncNo       - Row Number in function for selecting.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function configurationSection(appCode)
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + "/UPAMgmt/UPAConfiguration",
         data: {
            appCode: appCode,
            __RequestVerificationToken: $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
         },
         success: function(data)
         {
            var $target = $("#tabs-Config #UPAMgmtConfiguration");
            var $newHtml = $(data);
            $newHtml.find("table tbody tr.selected").removeClass("selected");
            window.replaceSegments($target, $newHtml, false);
            $newHtml.find("#segmentRelationControlItems table tbody").hide();
            $newHtml.find("#functionRelationControlItems table tbody").hide();
            $newHtml.find("#segmentRelationControlItems table tbody td input.image_tab_relation")
               .each(function()
               {
                  $(this).attr("data-field-type", "SegCode");
               });
            $newHtml.find("option[value='Delete']").css("font-weight", "bold");
            $("#tabRelationControl table tbody tr td[data-field-name='SeqNo']").css("width", "25%");
            $("#segmentRelationControlItems table tbody tr td[data-field-name='SeqNo']").css("width", "25%");
            $("#functionRelationControlItems table tbody tr td[data-field-name='SeqNo']").css("width", "30%");
            if ($("#tabRelationControl table tbody tr.relationDisplayRow:first").length > 0)
            {
               selectFirstRow($("#tabRelationControl table tbody tr.relationDisplayRow:first"), "click");
            }
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change tabCode DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='TabCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var max = 0;
      var changedValue = this.value;
      var tabCode = $(this).closest("tr").find("td input[name=Code]").val();
      var appCode = $('#ID_UPACODE').val();
      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow();
         $("select[name='TabCode'] option").each(function()
         {
            if ($(this).val() === tabCode)
            {
               $(this).show();
            }
         });
      }

      else if (tabCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name=Code]").val(changedValue);
         $("select[name='TabCode'] option")
            .each(function()
            {
               if ($(this).val() === tabCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("tr").find("td input[name=AppCode]").attr("value", appCode);
         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").val(changedValue);
         $(this).closest("tr").find("input[type='image']").attr("name", changedValue);
         $(this).closest("tr").find("input[type='image']").addClass("image_tab_relation");
         $(this).closest("tr").find("input[type='image']").attr("data-field-type", "TabCode");

         if ($(this).closest("tr").find("td input[name=SeqNo]").val().length === 0)
         {
            $("#tabRelationControl table tbody tr.relationEditRow td input[name=SeqNo],tr.updated td input[name=SeqNo]").each(function()
            {
               if ($(this).val().length > 0)
               {
                  max = Math.max(parseInt($(this).val()), max);
               }
            });
            $(this).closest("tr").find("td input[name=SeqNo]").val(max + 1);
         }

      }

      var segmentObj = [];
      $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name='TabCode']," +
            "#segmentRelationControlItems table tbody tr.updated input[name='TabCode']")
         .closest('td').find('input[value="' + tabCode + '"]')
         .each(function()
         {
            segmentObj.push({
               SegCode: $(this).closest("tr").find("td input[name=Code]").val()
            });
            $(this).deleteRelationRow();
         });
      $("#functionRelationControlItems table tbody tr.updated input[name='SegCode']," +
            "#functionRelationControlItems table tbody tr.relationEditRow input[name='SegCode']")
         .closest("td")
         .each(function()
         {
            for (var i = 0; i < segmentObj.length; i++)
            {
               if (segmentObj[i].SegCode === $(this).closest("tr").find("td input[name=SegCode]").val())
               {
                  $(this).deleteRelationRow();
               }
            }

         });
      $("#tabRelationControl table tr").each(function()
      {
         if ($(this).hasClass("selected"))
         {
            $(this).removeClass("selected");
         }
      });
      $(this).closest("tr").addClass("selected");
      if (changedValue === "Delete" || changedValue==="")
      {
         if ($("#tabRelationControl table tbody tr td input[name=AppCode][Value='" +appCode+"']:first").closest("tr").length>0)
         {
            selectFirstRow($("#tabRelationControl table tbody tr td input[name=AppCode][Value='" + appCode + "']:first").closest("tr")
               .find("input.image_tab_relation").closest("input"), "click");
         }
      }
      else
      {
         if ($("#tabRelationControl table tbody tr").hasClass("selected"))
         {
            $(this).closest("tr").find("input[type=image]").click();
         }

         else
         {
            $("#segmentRelationControlItems table tbody tr").removeClass("selected");
            $("#segmentRelationControlItems table tbody").hide();
            $("#functionRelationControlItems table tbody tr").removeClass("selected");
            $("#functionRelationControlItems table tbody").hide();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on seqNo.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#tabRelationControl table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("selected"))
      {
         return false;
      }

      $(e.target).closest("tr").find("input.image_tab_relation").trigger("click");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Sequence number.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#tabRelationControl table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("blank"))
      {
         $(this).val("");
         return false;
      }

      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var $table = $(e.target).closest("table tbody");
      var newValue = $(this).val();
      var i = 0;
      var j = 0;
      $table.find("tr td input[name=SeqNo]").closest("td input").each(function()
      {
         if ($(this).val() === newValue)
         {
            i++;
         }
      });
      if (i > 1)
      {
         $table.find("tr td input[name=SeqNo]").each(function()
         {
            if (parseInt($(this).val()) >= parseInt(newValue))
            {
               var rowVals = $(this).val();
               if ($(e.target).closest("tr").find("td input[name=Code]").val() === $(this).closest("tr").find("td input[name=Code]").val())
               {
                  return;
               }

               $table.find("tr td input[name=SeqNo]").closest("td input")
                  .each(function()
                  {
                     if ($(this).val() === rowVals)
                     {
                        j++;
                     }
                  });

               if (j > 1)
               {
                  $(this).val(parseInt($(this).val()) + 1);
               }
               j = 0;

            }
         });
      }

      sortTable($(this).closest("table").attr("id"),
         $(e.target).closest("tbody").find("tr.relationEditRow").length,
         $(e.target).closest("tbody").find("tr.updated").length,
         $(e.target).closest("tr"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onClick on TabRelationControl tr for addClass Selected and show segments.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#tabRelationControl table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      var tabCode = $("#tabRelationControl table tbody tr[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name=Code]").val();
      if (tabCode === undefined)
      {
         return false;
      }

      $("#segmentRelationControlItems table tbody tr").removeClass('selected');
      $('#functionRelationControlItems table tbody').hide();
      $("#segmentRelationControlItems table tbody tr.relationDisplayRow").hide();
      $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name='TabCode']")
         .each(function()
         {
            if ($(this).closest("input").val() === tabCode)
            {
               var $tr = $(this).closest("tr");
               var rowId = $tr.attr("data-row-id");
               $("#segmentRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" + rowId + "]").closest("tr").show();
            }
         });
      $("#segmentRelationControlItems table tbody").show();
      changeSelection($(this));
      if ($("#segmentRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" +
         $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name=TabCode][value='" + tabCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name=TabCode][value=" + tabCode + "]:first").closest("tr").attr("data-unique-selection")
         + "]").closest("tr").length > 0)
      {
         selectFirstRow($("#segmentRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" +
            $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name=TabCode][value='" + tabCode + "']:first").closest("tr").attr("data-row-id")
            + "][data-unique-selection=" + $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name=TabCode][value=" + tabCode + "]:first").closest("tr").attr("data-unique-selection")
            + "]").closest("tr"), "click");
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onClick on SegRelationControl tr for addClass Selected and show segments.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#segmentRelationControlItems table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      var segmentCode = $("#segmentRelationControlItems table tbody tr[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name=Code]").val();
      segmentCode = (segmentCode === undefined || segmentCode.length === 0)
         ? $(this).find("td:first").find("input").val() :
         segmentCode;
      if (segmentCode === undefined)
      {
         return false;
      }

      $("#functionRelationControlItems table tbody tr").removeClass("selected");
      $("#functionRelationControlItems table tbody tr.relationDisplayRow").hide();
      $("#functionRelationControlItems table tbody tr.relationEditRow td input[name='SegCode']")
         .each(function()
         {
            if ($(this).closest("input").val() === segmentCode)
            {
               var $tr = $(this).closest('tr');
               var rowId = $tr.attr('data-row-id');
               $("#functionRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" + rowId + "]").closest("tr").show();
            }
         });
      $("#functionRelationControlItems table tbody tr td[data-field-name=SeqNo]").closest("td").css("width", "30%");
      $("#functionRelationControlItems table tbody").show();
      changeSelection($(this));
      if ($("#functionRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" +
         $("#functionRelationControlItems table tbody tr.relationEditRow td input[name=SegCode][value='" + segmentCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $("#functionRelationControlItems table tbody tr.relationEditRow td input[name=SegCode][value=" + segmentCode + "]:first").closest("tr").attr("data-unique-selection")
         + "]").closest("tr").length > 0)
      {
         selectFirstRow(
            $("#functionRelationControlItems table tbody tr.relationDisplayRow[data-row-id=" +
            $("#functionRelationControlItems table tbody tr.relationEditRow td input[name=SegCode][value='" + segmentCode + "']:first").closest("tr").attr("data-row-id")
            + "][data-unique-selection=" + $("#functionRelationControlItems table tbody tr.relationEditRow td input[name=SegCode][value=" + segmentCode + "]:first").closest("tr").attr("data-unique-selection")
            + "]").closest("tr"), "");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Segment DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='SegCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var changedValue = this.value;
      var appCode = $("#ID_UPACODE").val();
      var max = 0;
      var segCode = $(this).closest("tr").find("td input[name='Code']").val();
      var tabCode = $(this).closest("tr").find("td input[name='TabCode']").val().length === 0
         ? ($("#tabRelationControl tr").hasClass("selected")
            ? $("#tabRelationControl tr.selected td select[name='TabCode']").val()
            : $(this).find("option:eq(0)").prop("selected", true))
         : $(this).closest("tr").find("td input[name=TabCode]").val();
      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow();
         $("select[name='SegCode'] option").each(function()
         {
            if ($(this).val() === segCode)
            {
               $(this).show();
            }
         });
      }

      else if (segCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name=Code]").attr("value", changedValue);
         $(this).closest("tr").find("td input[name=TabCode]").attr("value", $("#tabRelationControl table tr.selected td input[name=Code]").val());
         $(this).closest("tr").find("td input[name=AppCode]").attr("value", appCode);
         $("select[name='SegCode'] option")
            .each(function()
            {
               if ($(this).val() === segCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").val(tabCode);
         $(this).closest("tr").find("input[type='image']").attr("name", changedValue);
         $(this).closest("tr").find("input[type='image']").addClass("image_tab_relation");
         $(this).closest("tr").find("input[type='image']").attr("data-field-type", "SegCode");
         if ($(this).closest("tr").find("td input[name=SeqNo]").val().length === 0)
         {
            $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name=TabCode]," +
                  "#segmentRelationControlItems table tbody tr.updated td input[name=TabCode]")
               .closest("td").find("input[value='" + tabCode + "']")
               .closest("tr").find("td input[name=SeqNo]")
               .each(function()
               {
                  if ($(this).val().length > 0)
                  {
                     max = Math.max(parseInt($(this).val()), max);
                  }
               });
            $(this).closest("tr").find("td input[name=SeqNo]").val(max + 1);
         }
      }

      $("#functionRelationControlItems table tbody tr td input[name='SegCode']")
         .closest("td").find("input[value='" + segCode + "']")
         .closest("tr")
         .each(function()
         {
            var $tr = $(this).closest("tr");
            var rowId = $tr.attr("data-row-id");
            $("#functionRelationControlItems table tbody tr[data-row-id=" + rowId + "]")
               .each(function()
               {
                  $(this).deleteRelationRow();
               });
         });

      $("#functionRelationControlItems table tbody tr.relationAddRow.updated input[name='SegCode']")
         .closest("td").find(" input[value='" + segCode + "']").closest("tr")
         .each(function()
         {
            $(this).deleteRelationRow();
         });

      $("#segmentRelationControlItems table tr.selected").removeClass("selected");
      if (tabCode === "Add")
      {
         $(this).find("option:eq(0)").prop("selected", true);
         return false;
      }
      if (changedValue === "" || changedValue==="Delete")
      {
         if ($("#tabRelationControl table tbody tr td input[name=Code][Value='" + tabCode + "']").closest("tr").length > 0)
         {
            selectFirstRow($("#tabRelationControl table tbody tr td input[name=Code][Value='" + tabCode + "']")
               .closest("tr").find("input.image_tab_relation").closest("input"), "click");
         }
      }
      else
      {
         $(this).closest("tr").addClass("selected");
         $("#segmentRelationControlItems table tbody tr").hasClass("selected")
            ? $(this).closest("tr").find("input[type=image]").click()
            : ($("#functionRelationControlItems table tbody tr").removeClass("selected"),
               $("#functionRelationControlItems table tbody").hide());
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on seqNo.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#segmentRelationControlItems table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("selected"))
      {
         return false;
      }

      $(e.target).closest("tr").find("input.image_tab_relation").trigger("click");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Sequence number.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#segmentRelationControlItems table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("blank"))
      {
         $(this).val("");
         return false;
      }

      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var $table = $(e.target).closest("table tbody");
      var newValue = $(this).val();
      var i = 0;
      var j = 0;
      var tabCode = $("#tabRelationControl table tbody tr.selected").find("td input[name=Code]").val();
      $table.find("tr td input[name=TabCode]").closest("td").find("input[value='" + tabCode + "']")
         .closest("tr").find("td input[name=SeqNo]").closest("td input")
         .each(function()
         {
            if ($(this).val() === newValue)
            {
               i++;
            }
         });
      if (i > 1)
      {
         $table.find("tr td input[name=TabCode]").closest("td")
            .find("input[value='" + tabCode + "']")
            .closest("tr").find("td input[name=SeqNo]")
            .each(function()
            {
               if (parseInt($(this).val()) >= parseInt(newValue))
               {
                  var rowVals = $(this).val();
                  if ($(e.target).closest("tr").find("td input[name=Code]").val() === $(this).closest("tr").find("td input[name=Code]").val())
                  {
                     return;
                  }

                  $table.find("tr.relationEditRow td input[name=TabCode] , tr.updated td input[name=TabCode]")
                     .closest("td").find("input[value='" + tabCode + "']").closest("tr")
                     .find("td input[name=SeqNo]")
                     .each(function()
                     {
                        if ($(this).val() === rowVals)
                        {
                           j++;
                        }
                     });
                  if (j > 1)
                  {
                     $(this).val(parseInt($(this).val()) + 1);
                  }
                  j = 0;
               }
            });
      }

      sortTable($(this).closest("table").attr("id"),
         $(e.target).closest("tbody").find("tr.relationEditRow").length,
         $(e.target).closest("tbody").find("tr.updated").length,
         $(e.target).closest("tr"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on function row in view mode.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#functionRelationControlItems table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      changeSelection($(this));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on seqNo.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#functionRelationControlItems table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("selected"))
      {
         return false;
      }

      changeSelection($(this));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Function DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='FuncCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var changedValue = this.value;
      var max = 0;
      var appCode = $('#ID_UPACODE').val();
      var segCode = $("#segmentRelationControlItems table tr.selected").find("td input[name=Code]").val();
      var funcCode = $(this).closest("tr").find("td input[name=Code]").val();
      segCode = segCode.length === 0
         ? ($("#segmentRelationControlItems tr").hasClass("selected")
            ? $("#segmentRelationControlItems tr.selected td select[name='SegCode']").val()
            : $(this).find("option:eq(0)").prop("selected", true))
         : segCode;
      if (changedValue === "Delete")
      {
         $(this).deleteRelationRow();

         $("select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === funcCode)
               {
                  $(this).show();
               }
            });
      }

      else if (funcCode !== changedValue)
      {
         $("select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === funcCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("tr").find("td input[name=Code]").attr("value", changedValue);
         $(this).closest('tr').find('td input[name=SegCode]').attr("value", segCode);
         $(this).closest('tr').find('td input[name=AppCode]').attr("value", appCode);
         $(this).closest("select").find("option[value='Delete']").show();
         if ($(this).closest("tr").find("td input[name=SeqNo]").val().length === 0)
         {
            $("#functionRelationControlItems table tbody tr.relationEditRow td input[name=SegCode],tr.updated td input[name=SegCode]")
               .closest("td").find("input[value='" + segCode + "']")
               .closest("tr").find("td input[name=SeqNo]")
               .each(function()
               {
                  if ($(this).val().length > 0)
                  {
                     max = Math.max(parseInt($(this).val()), max);
                  }
               });
            $(this).closest("tr").find("td input[name=SeqNo]").val(max + 1);
         }
      }
      if (changedValue === "Delete" || changedValue==="")
      {
         if ($("#segmentRelationControlItems table tbody tr td input[name=Code][Value='" + segCode + "']").closest("tr").length > 0)
         {
            selectFirstRow($("#segmentRelationControlItems table tbody tr td input[name=Code][Value='" + segCode + "']")
               .closest("tr").find("input.image_tab_relation").closest("input"), "click");
         }
      }
      else
      {
         $("#functionRelationControlItems table tr").removeClass("selected");
         $(this).closest("tr").addClass("selected");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Sequence number.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#functionRelationControlItems table tbody tr input[name='SeqNo']", function(e)
   {
      if ($(e.target).closest("tr").hasClass("blank"))
      {
         $(this).val("");
         return false;
      }

      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var $table = $(e.target).closest("table tbody");
      var newValue = $(this).val();
      var i = 0;
      var j = 0;
      var segCode = $("#segmentRelationControlItems table tbody tr.selected").find("td input[name=Code]").val();
      $table.find("tr td input[name=SegCode]").closest("td")
         .find("input[value='" + segCode + "']").closest("tr")
         .find("td input[name=SeqNo]").closest("td input")
         .each(function()
         {
            if ($(this).val() === newValue)
            {
               i++;
            }
         });
      if (i > 1)
      {
         $table.find("tr td input[name=SegCode]").closest("td")
            .find("input[value='" + segCode + "']").closest("tr")
            .find("td input[name=SeqNo]")
            .each(function()
            {
               if (parseInt($(this).val()) >= parseInt(newValue))
               {
                  var rowVals = $(this).val();
                  if ($(e.target).closest("tr").find("td input[name=Code]").val() === $(this).closest("tr").find("td input[name=Code]").val())
                  {
                     return;
                  }

                  $table.find("tr.relationEditRow td input[name=SegCode] , tr.updated td input[name=SegCode]")
                     .closest("td").find("input[value='" + segCode + "']").closest("tr")
                     .find("td input[name=SeqNo]")
                     .each(function()
                     {
                        if ($(this).val() === rowVals)
                        {
                           j++;
                        }
                     });
                  if (j > 1)
                  {
                     $(this).val(parseInt($(this).val()) + 1);
                  }
                  j = 0;
               }
            });
      }

      sortTable($(this).closest("table").attr("id"),
         $(e.target).closest("tbody").find("tr.relationEditRow").length,
         $(e.target).closest("tbody").find("tr.updated").length,
         $(e.target).closest("tr"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onclick on arrows.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "input.image_tab_relation", function(e)
   {
      var tabCode = $(e.target).attr("value");
      var segCode = $(e.target).attr("name");
      $("#functionRelationControlItems table tbody").hide();
      if ($(e.target).attr("data-field-type") === "TabCode")
      {
         $("#tabRelationControl table tr")
            .each(function()
            {
               if ($(this).hasClass("selected"))
               {
                  $(this).removeClass("selected");
               }
            });
         if (tabCode === undefined)
         {
            $("#segmentRelationControlItems .relationControl").disableRelationEdit();
            $("#segmentRelationControlItems table tbody").hide();
            $(e.target).closest("tr").addClass("selected");
            return false;
         }

         $("#segmentRelationControlItems table tbody").show();
         $("#segmentRelationControlItems .relationControl").enableRelationEdit();
         $(this).closest("tr").addClass("selected");
         $("#segmentRelationControlItems table tbody tr.selected").removeClass('selected');
         setOptionSegment();
      }

      else if ($(e.target).attr("data-field-type") === "SegCode")
      {
         if (!$("#tabRelationControl table tr").hasClass("selected"))
         {
            showError("First you need to choose Tab!!!");
            return false;
         }

         $("#segmentRelationControlItems table tr").each(function()
         {
            if ($(this).hasClass("selected"))
            {
               $(this).removeClass("selected");
            }
         });

         if (segCode === undefined)
         {
            $("#functionRelationControlItems .relationControl").disableRelationEdit();
            $("#functionRelationControlItems table tbody").hide();
            $(e.target).closest("tr").addClass("selected");
            return false;
         }

         $("#functionRelationControlItems .relationControl").enableRelationEdit();
         $(this).closest("tr").addClass("selected");
         setOptionFunction();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set style for dropdowns.
   //*
   //* Parameters:
   //*   id               - id of each drop down.
   //*   name             - name of each dropdown.
   //*   style            - which style we want.
   //*   addStyle         - which style property we want.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setCssToDropDownLists(id, name, style, addStyle)
   {
      $("#" + id + " select[name=" + name + "] option").each(function()
      {
         if ($(this).val() === "Delete")
         {
            $(this).css(style, addStyle);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Load option for each segment.
   //*
   //* Parameters:
   //*   None
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setOptionSegment(segmentUnique, segmentRowId)
   {
      var segmentObjects = [];
      if ($("#tabRelationControl tr").hasClass("selected"))
      {
         var str = $("#tabRelationControl tr.selected").find("td select[name=TabCode]").val();
         $("#segmentRelationControlItems table tbody tr.relationEditRow").hide();
         $("#segmentRelationControlItems table tbody tr.relationEditRow td input[name='TabCode']")
            .each(function()
            {
               if ($(this).closest("input").val() === str)
               {
                  var element = {
                  };
                  element.segCode = $(this).closest("tr").find("td select[name=SegCode]").val();
                  segmentObjects.push(element);
                  $(this).closest("tr").show();
               }

               else
               {
                  $(this).closest("tr").hide();
               }
            });
         $("#segmentRelationControlItems table tbody tr.relationAddRow.updated td input[name='TabCode']")
            .each(function()
            {
               if ($(this).val() !== str)
               {
                  $(this).closest("tr").hide();
               }
            });
         $("#segmentRelationControlItems table tbody tr.relationAddRow.updated td input[name=TabCode]")
            .closest("td").find("input[value='" + str + "']").closest("tr")
            .find("select[name=SegCode] option").each(function()
            {
               var element = {
               };
               element.segCode = $(this).closest("tr").find("td select[name=SegCode]").val();
               segmentObjects.push(element);
               $(this).closest("tr").show();
            });
         $("#segmentRelationControlItems table tbody tr.relationEditRow  td input[name='TabCode']")
            .closest("input[value='" + str + "']").closest("tr")
            .find("select[name=SegCode] option")
            .each(function()
            {
               if ($(this).val() !== "Add")
               {
                  $(this).show();
                  for (var i = 0; i < segmentObjects.length; i++)
                  {
                     if ($(this).val() === segmentObjects[i].segCode && $(this).closest("select").val() !== $(this).val())
                     {
                        $(this).hide();
                     }
                  }
               }

               else
               {
                  $(this).hide();
               }
            });
         $("#segmentRelationControlItems table tbody tr.blank td select[name=SegCode] option").show();
         $("#segmentRelationControlItems table tbody tr.blank td select[name=SegCode] option").each(function()
         {
            if ($(this).val() !== "Add")
            {
               for (var i = 0; i < segmentObjects.length; i++)
               {
                  if ($(this).val() === segmentObjects[i].segCode)
                  {
                     $(this).hide();
                  }
               }
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            }
         });
         if (segmentUnique !== undefined && segmentRowId !== undefined)
         {
            $("#segmentRelationControlItems table tr ")
             .each(function()
             {
                if ($(this).attr("data-unique-selection") === segmentUnique && $(this).attr("data-row-id") === segmentRowId && segmentRowId !== undefined)
                {
                   $(this).addClass("selected");
                }
             });
         }
         else
         {
            if ($("#segmentRelationControlItems tr.relationAddRow.updated td input[name='TabCode'][value='" + str + "']:first")
               .closest("tr").length > 0)
            {
               selectFirstRow($("#segmentRelationControlItems .relationAddRow.updated td input[name='TabCode'][value='" + str + "']:first")
               .closest("tr").find("input.image_tab_relation").closest("input"), "click");
            }
            else if ($("#segmentRelationControlItems tr.relationEditRow td input[name='TabCode'][value='" + str + "']:first")
               .closest("tr").length > 0)
            {
               selectFirstRow($("#segmentRelationControlItems tr.relationEditRow td input[name='TabCode'][value='" + str + "']:first")
               .closest("tr").find("input.image_tab_relation").closest("input"), "click");
            }
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Load option for each function.
   //*
   //* Parameters:
   //*   None
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setOptionFunction(functionUnique, functionRowId)
   {
      var functionObjects = [];
      if ($("#segmentRelationControlItems table tbody tr").hasClass("selected"))
      {
         $("#functionRelationControlItems table tbody").show();
      }

      if ($("#segmentRelationControlItems table tbody tr").hasClass("selected"))
      {
         var segCode = $("#segmentRelationControlItems table tbody tr.selected")
            .find("td select[name=SegCode]").val();
         $("#functionRelationControlItems table tbody tr.relationEditRow").hide();
         $("#functionRelationControlItems table tbody tr.relationEditRow td input[name='SegCode']")
            .each(function()
            {
               if ($(this).closest("input").val() === segCode)
               {
                  var element = {
                  };
                  element.funcCode = $(this).closest("tr")
                     .find("td select[name=FuncCode]").val();
                  functionObjects.push(element);
                  $(this).closest("tr").show();
               }

               else
               {
                  $(this).closest("tr").hide();
               }
            });
         $("#functionRelationControlItems table tbody tr.updated td input[name='SegCode']")
            .each(function()
            {
               if ($(this).val() !== segCode)
               {
                  $(this).closest("tr").hide();
               }
            });
         $("#functionRelationControlItems table tbody tr.updated td input[name='SegCode']")
            .each(function()
            {
               if ($(this).closest("input").val() === segCode)
               {
                  var element = {
                  };
                  element.funcCode = $(this).closest("tr")
                     .find("td select[name=FuncCode]").val();
                  functionObjects.push(element);
                  $(this).closest("tr").show();
               }

               else
               {
                  $(this).closest("tr").hide();
               }
            });
         $("#functionRelationControlItems table tbody tr.relationEditRow  td input[name='SegCode']")
            .closest("td").find("input[value = '" + segCode + "']")
            .closest("tr").find("select[name=FuncCode] option").each(function()
            {
               if ($(this).val() !== "Add")
               {
                  $(this).show();
                  for (var i = 0; i < functionObjects.length; i++)
                  {
                     if ($(this).val() === functionObjects[i].funcCode
                        && $(this).closest("select").val() !== $(this).val())
                     {
                        $(this).hide();
                     }
                  }
               }
            });
         $("#functionRelationControlItems table tbody tr.blank td select[name=FuncCode] option").show();
         $("#functionRelationControlItems table tbody tr.blank td select[name=FuncCode] option")
            .each(function()
            {
               if ($(this).val() !== "Add")
               {
                  for (var i = 0; i < functionObjects.length; i++)
                  {
                     if ($(this).val() === functionObjects[i].funcCode)
                     {
                        $(this).hide();
                     }
                  }
               }
            });
         $("#functionRelationControlItems table tr.blank select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $("#functionRelationControlItems table tbody tr").removeClass("selected");
         if (functionUnique !== undefined && functionRowId !== undefined)
         {
            $("#functionRelationControlItems table tr ")
               .each(function()
               {
                  if ($(this).attr("data-unique-selection") === functionUnique && $(this).attr("data-row-id") === functionRowId && functionRowId !== undefined)
                  {
                     $(this).addClass("selected");
                  }
               });
         }
         else
         {
            if ($("#functionRelationControlItems tr.relationAddRow.updated td input[name='SegCode'][value='" + segCode + "']:first")
               .closest("tr").length > 0)
            {
               selectFirstRow($("#functionRelationControlItems tr.relationAddRow.updated td input[name='SegCode'][value='" + segCode + "']:first")
               .closest("tr"), "");
            }
            else if ($("#functionRelationControlItems tr.relationEditRow td input[name='SegCode'][value='" + segCode + "']:first")
               .closest("tr").length > 0)
            {
               selectFirstRow($("#functionRelationControlItems tr.relationEditRow td input[name='SegCode'][value='" + segCode + "']:first")
               .closest("tr"), "");
            }
         }
      }

      else
      {
         $("#functionRelationControlItems table tbody").hide();
      }


   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   sortTable function for sorting table rows.
   //*
   //* Parameters:
   //*   tableName        - table name for changing row.
   //*   rowLength        - length of rows that has relationEditRow class.
   //*   updatesRowLength - length of rows that has update class.
   //*   rowId            - Row properties for select currently row.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function sortTable(tableName, rowLength, updatesRowLength, rowId)
   {
      var tbl = $("#" + tableName).find("tbody");
      var store = [];
      for (var i = 0; i < rowLength; i++)
      {
         var row = tbl.find("tr.relationEditRow:eq(" + i + ")");
         var seqNo = row.find("td input[name=SeqNo]").val();
         var sortnr = seqNo.length > 0
            ? parseFloat(seqNo)
            : "";
         if (!isNaN(sortnr))
         {
            store.push([sortnr, row]);
         }
      }

      for (var j = 0; j < updatesRowLength; j++)
      {
         var rowUpdate = tbl.find("tr.updated:eq(" + j + ")");
         var seqNoUpdate = rowUpdate.find("td input[name=SeqNo]").val();
         var sortnrUpdate = seqNoUpdate.length > 0
            ? parseFloat(seqNoUpdate)
            : "";
         if (!isNaN(sortnrUpdate))
         {
            store.push([sortnrUpdate, rowUpdate]);
         }
      }

      store.sort(function(x, y)
      {
         return x[0] - y[0];
      });

      tbl.find("tr.relationEditRow").detach();
      var newRow = tbl.find("tr.blank").closest("tr");
      tbl.find("tr.relationAddRow").detach();

      for (var k = 0; k < store.length; k++)
      {
         tbl.append(store[k][1]);
      }

      tbl.append(newRow);
      tbl.find("tr.selected").removeClass("selected");
      rowId.addClass("selected");
      tbl.find("tr.selected").each(function()
      {
         $(this).find("input.image_tab_relation").trigger("click");
      });

      store = null;
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set data from input  to Grid.
   //*
   //* Parameters:
   //*   inValue - Description not available.
   //*   inField - Description not available.
   //*   upaId    - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   function updateDataTable(inValue, inField, upaId)
   {
      var row = $("#searchGrid tbody").find("tr[data-row-id=0]").closest("tr");

      if (upaId !== "0")
      {
         row = $("#UPAMgmtGridSummary").find("#searchGrid [data-row-id=" + upaId + "]");
      }

      if (inField === "ID_UPACODE")
      {
         row.find("td:eq(1)").html(inValue.toUpperCase());
         $("#ID_UPACODE_MIN").val(inValue.toUpperCase());
      }

      if (inField === "ID_DISPLAY")
      {
         row.find("td:eq(2)").html(inValue);
         $("#ID_DISPLAY_MIN").val(inValue);
      }

      if (inField === "ID_DESCR")
      {
         row.find("td:eq(3)").html(inValue);
         $("#ID_DESCR_MIN").val(inValue);
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set data to grid when typing on textboxes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("input", ".inputEx", function()
   {
      var id = $(this).attr("id");
      var value = $(this).val();

      updateDataTable(value,
                      id,
                      ($("#ID_UPACODE").attr("readonly") === undefined
                        ? "0"
                        : upaId));
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   find row in pages.
   //*
   //* Parameters:
   //*   upaIdRow    -  upaIdRow is code for find row.
   //*   row         -  row is the selected row information.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setClickedRowToTextbox(upaIdRow, row)
   {
      table = window.dtHeaders("#searchGrid", "", ["sorting"], ["click"]);
      row.find("td").each(function()
      {
         switch ($(this).attr("data-field-name"))
         {
            case "Code":
               {
                  $("#ID_UPACODE").val($(this).attr("data-field-value"));
                  $("#ID_UPACODE_MIN").val($(this).attr("data-field-value"));
                  break;
               }
            case "Display":
               {
                  $("#ID_DISPLAY").val($(this).attr("data-field-value"));
                  $("#ID_DISPLAY_MIN").val($(this).attr("data-field-value"));
                  break;
               }
            case "Desc":
               {
                  $("#ID_DESCR").val($(this).attr("data-field-value"));
                  $("#ID_DESCR_MIN").val($(this).attr("data-field-value"));
                  break;
               }
            case "Action":
               {
                  $("#ID_ACTION").val($(this).attr("data-field-value"));
                  break;
               }
            case "Controller":
               {
                  $("#ID_CTRLR").val($(this).attr("data-field-value"));
                  break;
               }
            case "Area":
               {
                  $("#ID_AREA").val($(this).attr("data-field-value"));
                  break;
               }
            case "RouteValue":
               {
                  $("#ID_ROUTE").val($(this).attr("data-field-value"));
                  break;
               }
            case "URN":
               {
                  $("#ID_URN").val($(this).attr("data-field-value"));
                  break;
               }
            case "URITemplate":
               {
                  $("#ID_URI").val($(this).attr("data-field-value"));
                  break;
               }
         }
      });
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Setup MessageBox Three Buttons value and visibility.
   //*
   //* Parameters:
   //*   title      - Description not available.
   //*   btContinue - Description not available.
   //*   btYes      - Description not available.
   //*   btNo       - Description not available.
   //*   btSave     - Description not available.
   //*   btDiscard  - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setMessagBox(title, btContinue, btYes, btNo, btSave, btDiscard)
   {
      $("#confirmAlert").hide();
      $("#alertdeletemessageid").text(title);

      setButton($("#btcontinue"), btContinue);
      setButton($("#btyes"), btYes);
      setButton($("#btno"), btNo);
      setButton($("#btsave"), btSave);
      setButton($("#btdiscard"), btDiscard);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   btID  - Description not available.
   //*   value - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setButton(btID, value)
   {
      btID.val("");
      btID.hide();

      if (value !== "")
      {
         btID.val(value);
         btID.show();
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Clear Textboxes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function clearTextBoxes()
   {
      $(".formMax .inputEx").val("");
      $(".formMax .selectEx").val("0");
      $(".formMax .inputEx,.selectEx").removeAttr("style");
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   remove new row.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function removeGridNewRow()
   {
      if (!$("#searchGrid tbody tr.selected[data-row-id=0]").hasClass("hidden"))
      {
         $("#searchGrid tbody tr.selected[data-row-id=0]").detach();
         $newRow.insertBefore($("#searchGrid tbody tr:first").closest("tr"));
         $newRow.addClass("hidden");
         $newRow.find("td").each(function()
         {
            $(this).attr("data-field-value", "");
         });
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   check validation field.
   //*
   //* Parameters:
   //*   fieldName     - name of each field for get validation 
   //*   fieldValue    - value of each field for get validation
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function checkValidationField(fieldName, fieldValue)
   {
      if (valid && !txtChanges)
      {
         return false;
      }
      if (fieldValue.length === 0)
      {
         setColorAndClass("#FFFFC0CB", "#" + fieldName, "inputError");
         enableActionbarControl("#CMD_SAVE", false);
         return false;
      }

      $.ajax(
      {
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "fieldValue=" + fieldValue.toUpperCase() +
            getAntiForgeryToken(),
         url: "checkValidationField",
         success: function(data)
         {
            if (data.Success)
            {
               valid = true;
               txtChanges = false;
               if (!window.getFormChanged())
               {
                  window.setFormChanged(true);
               }

               if ($("#" + fieldName).hasClass("inputError"))
               {
                  removeColorAndClass("#ECDD9D", "#" + fieldName, "inputError");
               }

               if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
               {
                  enableActionbarControl("#CMD_SAVE", true);
               }

               return false;
            }

            else
            {
               valid = false;
               $(this).closeMessageBox();
               setColorAndClass("#FFFFC0CB", "#" + fieldName, "inputError");
               enableActionbarControl("#CMD_SAVE", false);
               return false;
            }
         }
      });
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Leave the Code textbox.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("blur", "#ID_UPACODE", function()
   {
      if (getEditMode() && $("#ID_UPACODE").attr("readonly") === undefined)
      {
         checkValidationField("ID_UPACODE", $("#ID_UPACODE").val());
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   change textboxes value.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".inputEx", function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if ($(this).closest("input").attr("id") === "ID_UPACODE")
            {
               txtChanges = true;
               if (!window.getFormChanged())
               {
                  window.setFormChanged(true);
               }
            }

            else if ($(this).closest("input").attr("id") === "ID_DISPLAY")
            {
               if ($(this).val().length > 0)
               {
                  removeColorAndClass("#ECDD9D", "#ID_DISPLAY", "inputError");
                  if (!window.getFormChanged())
                  {
                     window.setFormChanged(true);
                  }
                  if (!$(".formBody .inputEx").hasClass("inputError"))
                  {
                     enableActionbarControl("#CMD_SAVE", true);
                  }
               }
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   allow put words and underline for Code.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#ID_UPACODE", function(e)
   {
      if (e.which < 48 ||
          (e.which > 57 && e.which < 65) ||
          (e.which > 90 && e.which < 97 && e.which !== 95) ||
          e.which > 122)
      {
         e.preventDefault();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get All FormBody data to the string list for controller input.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function getBodyData()
   {
      var bodyData = "";
      $(".formMax *").filter(":input").each(function()
      {
         bodyData = bodyData + '"' + $(this).attr("id") + '" : "' +
            ($(this).attr("id") === "ID_UPACODE"
               ? $(this).val().toUpperCase()
               : $(this).val())
            + '",';
      });

      return bodyData;
   };

   //*****************************************************************************
   //*
   //* Summary:
   //*   refresh Grid by Code.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshGridByCode(code)
   {
      $.ajax({
         type: "POST",
         url: "ViewDataRequest",
         data: "isEnable=" + false + getAntiForgeryToken(),
         success: function(data)
         {
            var $target = $("#UPAMgmtGridSummary");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            table = window.dtHeaders("#searchGrid", "", ["sorting"], ["click"]);
            if (!table.$("tr.selected").hasClass("selected"))
            {
               var i = 0;
               if (code.length > 0 && code !== "0")
               {
                  findRow(code, i);
               }

               else
               {
                  $newHtml.find("#searchGrid tbody").find("tr:eq(1)").click();
               }
            }

            $newHtml.find("#searchGrid thead tr th").removeClass("sorting").off("click");
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   find row by code .
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function findRow(code, i)
   {
      var blnClick = false;
      $("#searchGrid tbody tr.selected").removeClass("selected");
      $("#searchGrid tbody tr[data-row-id='" + code + "']").click();


   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   click on pagable button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("click", ".paginate_button ", function()
   {
      if (getEditMode())
      {
         if (!$("#searchGrid tbody tr").hasClass("hidden"))
         {
            if (parseInt($(this).html()) === btnIndex)
            {
               if ($("#searchGrid tbody tr:eq(" + rowIndex + ")").closest("tr").hasClass("odd"))
               {
                  if ($newRow.hasClass("even"))
                  {
                     $newRow.removeClass("even").addClass("odd");
                  }
               } else
               {
                  if ($newRow.hasClass("odd"))
                  {
                     $newRow.removeClass("odd").addClass("even");
                  }
               }
               for (var k = rowIndex; k <= $("#searchGrid tbody tr").last().closest("tr").index() ; k++)
               {
                  if ($("#searchGrid tbody tr:eq(" + k + ")").closest("tr").hasClass("odd"))
                  {
                     $("#searchGrid tbody tr:eq(" + k + ")").closest("tr").removeClass("odd").addClass("even");
                  } else
                  {
                     $("#searchGrid tbody tr:eq(" + k + ")").closest("tr").removeClass("even").addClass("odd");
                  }
               }
               $newRow.removeClass("hidden");
               $newRow.insertBefore($("#searchGrid tbody tr:eq(" + rowIndex + ")").closest("tr"));
            }
         }

         if (btnIndex !== 1 && $("#searchGrid_paginate a.paginate_button.current").closest("a").attr("data-dt-idx") === "1")
         {
            if ($("#searchGrid tbody tr.selected").closest("tr").attr("data-row-id") === "0")
            {
               $("#searchGrid tbody tr.selected").addClass("hidden");
            }
         }

         if ($("#ID_UPACODE").attr("readonly") !== "readonly")
         {
            updateDataTable($("#ID_UPACODE").val(), "ID_UPACODE", "0");
            updateDataTable($("#ID_DISPLAY").val(), "ID_DISPLAY", "0");
            updateDataTable($("#ID_DESCR").val(), "ID_DESCR", "0");
         }

         else
         {
            updateDataTable($("#ID_UPACODE").val(), "ID_UPACODE", upaId);
            updateDataTable($("#ID_DISPLAY").val(), "ID_DISPLAY", upaId);
            updateDataTable($("#ID_DESCR").val(), "ID_DESCR", upaId);
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   set upaId after save and cancel or change tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setUpaIdCallConfiguration()
   {
      upaId = $("#ID_UPACODE").val();
      if (upaId !== undefined
         && upaId !== "0"
         && upaId.length > 0)
      {
         configurationSection(upaId);
      }
   }

});