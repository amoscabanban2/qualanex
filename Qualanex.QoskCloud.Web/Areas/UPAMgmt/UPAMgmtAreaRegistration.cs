﻿using System;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.UPAMgmt
{
   public class UPAMgmtAreaRegistration : AreaRegistration
   {
      public override string AreaName
      {
         get
         {
            return "UPAMgmt";
         }
      }

      public override void RegisterArea(AreaRegistrationContext context)
      {
         context.MapRoute(
            "UPAMgmt_default",
            "UPAMgmt/{Controller}/{action}/{id}",
            new { action = "Index", id = UrlParameter.Optional }
         );
      }
   }
}