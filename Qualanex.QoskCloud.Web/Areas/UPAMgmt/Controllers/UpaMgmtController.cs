﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UPAMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.UPAMgmt.Controllers
{
   using System.Linq;
   using System.Web.Mvc;
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.QoskCloud.Web.Areas.UPAMgmt.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_UPAMGMT")]
   public class UPAMgmtController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult UPAMgmtView()
      {
         var viewModel = new UPAMgmtViewModel
         {
            UPAMgmtModels = UPAMgmtModel.GetUPAMgmtEntities(false),
            ActionMenu = UPAMgmtModel.GetActionMenu()
         };

         if (viewModel.UPAMgmtModels.Any())
         {
            viewModel.SelectedUPAMgmtModel = viewModel.UPAMgmtModels[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="UPAMgmtEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateUPAMgmtEntity(string UPAMgmtEntity)
      {
         var model = System.Web.Helpers.Json.Decode<UPAMgmtModel>(UPAMgmtEntity);

         return this.PartialView("_UPAMgmtForm", model);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isEnable)
      {
         var viewModel = new UPAMgmtViewModel
         {
            UPAMgmtModels = UPAMgmtModel.GetUPAMgmtEntities(isEnable),
            ActionMenu = UPAMgmtModel.GetActionMenu()
         };

         if (viewModel.UPAMgmtModels.Any())
         {
            viewModel.SelectedUPAMgmtModel = viewModel.UPAMgmtModels[0];
         }

         return this.PartialView("_UPAMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="appCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UPAConfiguration(string appCode)
      {
         var viewModel = new UPAMgmtViewModel
         {
            TabRelationObjects = new UPAMgmtRelationControl
            {
               UPAConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        { nameof(UPAMgmtModel.TabCode),
                           UPAMgmtModel.GetTabDictLists(appCode, false) }
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(UPAMgmtModel.Level),nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.AppCode), nameof(UPAMgmtModel.Area),
                        nameof(UPAMgmtModel.Code),nameof(UPAMgmtModel.Controller),
                        nameof(UPAMgmtModel.CreatedBy), nameof(UPAMgmtModel.CreatedDate),
                        nameof(UPAMgmtModel.Description), nameof(UPAMgmtModel.DisplayFormat),
                        nameof(UPAMgmtModel.EffectiveEndDate), nameof(UPAMgmtModel.FuncCode),
                        nameof(UPAMgmtModel.EffectiveStartDate), nameof(UPAMgmtModel.IsDeleted),
                        nameof(UPAMgmtModel.IsDisposed), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.ModifiedBy), nameof(UPAMgmtModel.ModifiedDate),
                        nameof(UPAMgmtModel.ObjectID), nameof(UPAMgmtModel.ParentCode),
                        nameof(UPAMgmtModel.RouteValue), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.RouteValue), nameof(UPAMgmtModel.SegCode),
                        nameof(UPAMgmtModel.Tooltip), nameof(UPAMgmtModel.URITemplate),
                        nameof(UPAMgmtModel.URN), nameof(UPAMgmtModel.Version),
                        nameof(UPAMgmtModel.Code)
                     },
                     UpdateMethod = "console.log('not yet implemented');",
                     AddMethod = this.Url.Action(nameof(UPAMgmtModel)),
                     InitialReadOnly = true,
                     RowProperty = new UPAMgmtModel(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView = "_UPAConfigCustomControl",
                           AddView = "_UPAConfigCustomControl",
                           ReadOnlyView = ""
                        }
                     }
                  },
                  Rows = UPAMgmtModel.GetTabList(appCode, false)
               }
            },
            SegmentRelationObjects = new UPAMgmtRelationControl
            {
               UPAConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        { nameof(UPAMgmtModel.SegCode), UPAMgmtModel.GetSegmentDictLists()}
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(UPAMgmtModel.Level), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.AppCode), nameof(UPAMgmtModel.Area),
                        nameof(UPAMgmtModel.Code),nameof(UPAMgmtModel.Controller),
                        nameof(UPAMgmtModel.CreatedBy), nameof(UPAMgmtModel.CreatedDate),
                        nameof(UPAMgmtModel.Description),nameof(UPAMgmtModel.EffectiveEndDate),
                        nameof(UPAMgmtModel.DisplayFormat), nameof(UPAMgmtModel.EffectiveStartDate),
                        nameof(UPAMgmtModel.FuncCode), nameof(UPAMgmtModel.IsDeleted),
                        nameof(UPAMgmtModel.IsDisposed), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.ModifiedBy), nameof(UPAMgmtModel.ModifiedDate),
                        nameof(UPAMgmtModel.ObjectID),nameof(UPAMgmtModel.ParentCode),
                        nameof(UPAMgmtModel.RouteValue), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.RouteValue), nameof(UPAMgmtModel.TabCode),
                        nameof(UPAMgmtModel.Tooltip), nameof(UPAMgmtModel.URITemplate),
                        nameof(UPAMgmtModel.URN), nameof(UPAMgmtModel.Version),
                        nameof(UPAMgmtModel.Code)
                     },
                     UpdateMethod = "console.log('not yet implemented'); ",
                     AddMethod = this.Url.Action(nameof(UPAMgmtModel)),
                     InitialReadOnly = true,
                     RowProperty = new UPAMgmtModel(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView = "_UPAConfigCustomControl",
                           AddView = "_UPAConfigCustomControl",
                           ReadOnlyView = ""
                        }
                     }
                  },
                  Rows = UPAMgmtModel.GetSegmentList(appCode, false)
               }
            },
            FunctionRelationObjects = new UPAMgmtRelationControl
            {
               UPAConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        {
                        nameof(UPAMgmtModel.FuncCode), UPAMgmtModel.GetFunctionDictsLists()
                        }
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(UPAMgmtModel.Level), nameof(UPAMgmtModel.Action),
                        nameof(UPAMgmtModel.AppCode), nameof(UPAMgmtModel.Area),
                        nameof(UPAMgmtModel.Code),nameof(UPAMgmtModel.Controller),
                        nameof(UPAMgmtModel.CreatedBy), nameof(UPAMgmtModel.CreatedDate),
                        nameof(UPAMgmtModel.Description), nameof(UPAMgmtModel.DisplayFormat),
                        nameof(UPAMgmtModel.EffectiveEndDate),nameof(UPAMgmtModel.EffectiveStartDate),
                        nameof(UPAMgmtModel.IsDeleted), nameof(UPAMgmtModel.IsDisposed),
                        nameof(UPAMgmtModel.Action),nameof(UPAMgmtModel.ModifiedBy),
                        nameof(UPAMgmtModel.ModifiedDate), nameof(UPAMgmtModel.ObjectID),
                        nameof(UPAMgmtModel.ParentCode), nameof(UPAMgmtModel.RouteValue),
                        nameof(UPAMgmtModel.Action),nameof(UPAMgmtModel.RouteValue),
                        nameof(UPAMgmtModel.TabCode), nameof(UPAMgmtModel.SegCode),
                        nameof(UPAMgmtModel.Tooltip), nameof(UPAMgmtModel.URITemplate),
                        nameof(UPAMgmtModel.URN), nameof(UPAMgmtModel.Version),
                        nameof(UPAMgmtModel.Code)
                     },
                     UpdateMethod = "console.log('not yet implemented'); ",
                     AddMethod = this.Url.Action(nameof(UPAMgmtModel)),
                     InitialReadOnly = true,
                     RowProperty = new UPAMgmtModel()
                  },
                  Rows = UPAMgmtModel.GetFunctionList(appCode, false)
               }
            }
         };

         return this.PartialView("_UPAConfiguration", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save UPAConfiguration changes.
      /// </summary>
      /// <param name="upaConfigEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SaveUPAConfigDetail(string upaConfigEntity)
      {
         var blnCheck = UPAMgmtModel.SetConfiguration(
            System.Web.Helpers.Json.Decode<List<UPAMgmtModel>>(upaConfigEntity),
            this.HttpContext.User.Identity.Name);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save UPAConfiguration changes.
      /// </summary>
      /// <param name="fieldValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult checkValidationField(string fieldValue)
      {
         var blnCheck = UPAMgmtModel.CheckCode(fieldValue);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save UPASummary changes.
      /// </summary>
      /// <param name="bodyData"></param>
      /// <param name="code"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SaveUpaSummaryDetail(string bodyData, string code)
      {
         var curUser = this.HttpContext.User.Identity.Name;
         var blnCheck = code == "0"
                        ? UPAMgmtModel.NewUpaSummary(curUser, bodyData)
                        : UPAMgmtModel.UpdateUpaSummary(curUser, bodyData, code);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck,
            Code = code == "0"
                        ? UPAMgmtModel.GetOneValue(bodyData, "ID_UPACODE")
                        : code
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

   }
}
