﻿namespace Qualanex.QoskCloud.Web.Areas.Report
{
    /// <summary>
    /// Response: This class is used to get ReportScheduleId and update report criteria based on ReportScheduleId
    /// </summary>
    public class Response
    {
        public object response { get; set; }
        public Status Status { get; set; }
        public string Message { get; set; }
        public long UpdateVersion { get; set; }
        public long ReportScheduleId { get; set; }
    }
    /// <summary>
    /// Status
    /// </summary>
    public enum Status
    {
        OK = 0,
        Fail = 1,
        Rollback = 2,
        VersionChange = 3,
        Deleted = 4
    }
}