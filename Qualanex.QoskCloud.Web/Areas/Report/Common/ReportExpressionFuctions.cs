﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Microsoft.Azure;
using Qualanex.QoskCloud.Utility;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using DetailSection = Telerik.Reporting.Processing.DetailSection;
using TextBox = Telerik.Reporting.Processing.TextBox;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Qualanex.QoskCloud.Web.Areas.Report
{
   /// <summary>
   /// Class is represent functions those are available in expression in report designer.
   /// </summary>
   public class ReportExpressionFuctions : Telerik.Reporting.Report
   {
      /// <summary>
      /// Convert UTC datetime in specified time zone datetime.
      /// </summary>
      /// <param name="rpReportDataTimeZone"></param>
      /// <param name="UtcDate"></param>
      /// <returns></returns>   
      public static DateTime? ConvertUTCToTimeZone(string rpReportDataTimeZone, DateTime? UtcDate)
      {
         return SchedulerFunctions.ConvertUTCToTimeZoneDate(rpReportDataTimeZone, Convert.ToDateTime(UtcDate));
      }

      /// <summary>
      /// Convert date into UTC date from local time.
      /// </summary>
      /// <param name="date"></param>
      /// <returns></returns>
      public static DateTime ConvertDateIntoUTC(DateTime date)
      {
         return date.ToUniversalTime();
      }

      /// <summary>
      /// CreateTxtHeader is used to generate dynamic report
      /// </summary>
      /// <param name="FieldName"></param>
      /// <param name="i"></param>
      /// <returns></returns>
      private Telerik.Reporting.TextBox CreateTxtHeader(string FieldName, int i)
      {
         Telerik.Reporting.TextBox txtHead = new Telerik.Reporting.TextBox();
         txtHead.Style.BackgroundColor = Color.Empty;
         txtHead.Style.BorderStyle.Default = BorderType.Solid;
         txtHead.Style.Font.Bold = true;
         txtHead.CanGrow = true;
         txtHead.Value = FieldName;
         return txtHead;
      }

      /// <summary>
      /// CreateTxtDetail is used to generate dynamic report
      /// </summary>
      /// <param name="FieldName"></param>
      /// <param name="i"></param>
      /// <returns></returns>
      private Telerik.Reporting.TextBox CreateTxtDetail(string FieldName, int i)
      {
         Telerik.Reporting.TextBox txtDetail = new Telerik.Reporting.TextBox();
         txtDetail.Style.BackgroundColor = Color.Empty;
         txtDetail.Style.BorderStyle.Default = BorderType.Solid;
         txtDetail.Style.Font.Bold = false;
         txtDetail.CanGrow = true;
         txtDetail.Value = "=Fields." + FieldName;
         return txtDetail;
      }

      /// <summary>
      /// Generate report based on user specific settings
      /// </summary>
      /// <param name="userid"></param>
      /// <param name="filetype"></param>
      /// <param name="subscribedColumns"></param>
      /// <param name="report"></param>
      /// <param name="panel1"></param>
      /// <param name="detail"></param>
      /// <param name="timeZone"></param>
      public void GenerateReportDefinition(long userid, object filetype, List<ColumnDetails> subscribedColumns, Telerik.Reporting.Report report, Telerik.Reporting.Panel panel1, Telerik.Reporting.DetailSection detail, object timeZone)
      {
         Telerik.Reporting.Report report1 = new Telerik.Reporting.Report();
         int count = subscribedColumns.Count;

         Unit x = Unit.Inch(0);
         Unit y_detail = Unit.Inch(0);
         Unit y_header = Unit.Inch(1);
         double columnWidth = 2;
         double columnMaxHeight = 0;

         var reportTimeZone = (report.ReportParameters["ReportScheduleTimeZone"].Value ?? timeZone).ToString();
         reportTimeZone = (string.IsNullOrEmpty(reportTimeZone) ? timeZone : reportTimeZone).ToString();

         DateTime? end = (DateTime?)(report.ReportParameters["DataEndDate"].Value?.ToString() == "" ? null : report.ReportParameters["DataEndDate"].Value);
         DateTime? start = (DateTime?)(report.ReportParameters["DataStartDate"].Value?.ToString() == "" ? null : report.ReportParameters["DataStartDate"].Value);

         if (end != null && start != null && report.ReportParameters["ReportScheduleTimeZone"].Value != null)
         {
            end = ConvertUTCToTimeZone(reportTimeZone, end);
            start = ConvertUTCToTimeZone(reportTimeZone, start);
         }

         string dataEndDate = end?.ToString("yyyy/MM/dd hh:mm tt") ?? ConvertUTCToTimeZone(reportTimeZone, DateTime.UtcNow).Value.ToString("yyyy/MM/dd hh:mm tt");
         string dataStartDate = start?.ToString("yyyy/MM/dd hh:mm tt - ") ?? "";
         string periodEndDate = "Period End Date: ";
         //if (report.Name == "Return To Stock")
         //{
         periodEndDate += dataEndDate;
         //}
         //else
         //{
         //   periodEndDate += dataStartDate + dataEndDate;
         //}

         Telerik.Reporting.TextBox startDate = new Telerik.Reporting.TextBox();
         startDate.Style.BackgroundColor = Color.Empty;
         startDate.Style.BorderStyle.Default = BorderType.None;
         startDate.Style.Font.Bold = true;
         startDate.CanGrow = false;
         startDate.Value = periodEndDate;
         startDate.Location = new PointU(x, Unit.Inch(0.5));
         startDate.Width = Unit.Inch(6);
         startDate.Height = Unit.Inch(0.2);

         Telerik.Reporting.TextBox endDate = new Telerik.Reporting.TextBox();
         endDate.Style.BackgroundColor = Color.Empty;
         endDate.Style.BorderStyle.Default = BorderType.None;
         endDate.Style.Font.Bold = true;
         endDate.CanGrow = false;
         endDate.Value = periodEndDate;
         endDate.Location = new PointU(x, Unit.Inch(0.5));
         endDate.Width = Unit.Inch(6);
         endDate.Height = Unit.Inch(0.2);

         Telerik.Reporting.TextBox timeZoneBox = new Telerik.Reporting.TextBox();
         timeZoneBox.Style.BackgroundColor = Color.Empty;
         timeZoneBox.Style.BorderStyle.Default = BorderType.None;
         timeZoneBox.Style.Font.Bold = true;
         timeZoneBox.CanGrow = false;
         timeZoneBox.Value = reportTimeZone;
         timeZoneBox.Location = new PointU(x, Unit.Inch(0.7));
         timeZoneBox.Width = Unit.Inch(6);
         timeZoneBox.Height = Unit.Inch(0.2);
         timeZoneBox.Style.Font.Size = Unit.Point(8D);

         Telerik.Reporting.PictureBox prettyHeader = new Telerik.Reporting.PictureBox
         {
            Width = Unit.Inch(3),
            Height = Unit.Inch(0.4),
            Location = new PointU(x, Unit.Inch(0)),
            MimeType = "png",
            Value = getReportLogoURL()//@"Images/logo-inner.png"
         };

         Telerik.Reporting.TextBox headerTitle = new Telerik.Reporting.TextBox();
         headerTitle.Style.BackgroundColor = Color.Empty;
         headerTitle.Style.BorderStyle.Default = BorderType.None;
         headerTitle.Style.Font.Bold = true;
         headerTitle.Style.Font.Size = Unit.Point(16D);
         headerTitle.CanGrow = false;
         headerTitle.Value = report.Name;
         headerTitle.Location = new PointU(Unit.Inch(3), Unit.Inch(0.1));
         headerTitle.Width = Unit.Inch(3);
         headerTitle.Height = Unit.Inch(0.2);

         Telerik.Reporting.ReportItemBase[] headColumnList = new Telerik.Reporting.ReportItem[count + 4];
         Telerik.Reporting.ReportItemBase[] detailColumnList = new Telerik.Reporting.ReportItem[count];

         for (int column = 0; column < count; column++)
         {
            string columnName = subscribedColumns[column].ColumnName;
            string columnDisplayText = subscribedColumns[column].DisplayText;
            columnWidth = columnDisplayText.Length / 11.0;

            if (columnName == Constants.Report_ProductDetail_Parameter_VendorName || columnName == Constants.Report_ProductDetail_Parameter_StoreName)
            {
               columnWidth = 1.5;
            }

            Telerik.Reporting.TextBox header = CreateTxtHeader(columnDisplayText, column);
            header.Location = new Telerik.Reporting.Drawing.PointU(x, y_header);
            header.Name = "textboxHeader" + column;
            Unit s = header.Height;

            Telerik.Reporting.TextBox textBox = CreateTxtDetail(columnName, column);
            textBox.Location = new Telerik.Reporting.Drawing.PointU(x, y_detail);
            textBox.Name = "textboxDetail" + column;

            headColumnList[column] = header;
            detailColumnList[column] = textBox;

            header.Width = Unit.Inch(columnWidth);
            header.Height = Unit.Inch(0.4);
            columnMaxHeight = Math.Max(header.Height.Value, columnMaxHeight);

            textBox.Width = Unit.Inch(columnWidth);
            textBox.Height = Unit.Inch(0.2);
            x += Unit.Inch(columnWidth);
         }

         Telerik.Reporting.ReportHeaderSection reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();

         if (filetype != null && filetype.ToString() == "XLS")
         {
            panel1.Visible = false;
            reportHeaderSection1.Height = new Unit(0.2, UnitType.Inch);
            reportHeaderSection1.Style.BackgroundColor = Color.White;
            detail.Style.BackgroundColor = System.Drawing.Color.White;
         }
         else
         {
            reportHeaderSection1.Height = new Unit(0.6, UnitType.Inch);
            Color headerBlue = ColorTranslator.FromHtml("#90C1D8");

            foreach (var item in headColumnList)
            {
               if (item != null)
               {
                  item.Style.BackgroundColor = headerBlue;
               }
            }
         }
         headColumnList.ToList().ForEach(xe => { if (xe != null) ((Telerik.Reporting.TextBox)xe).Height = Unit.Inch(columnMaxHeight); });
         headColumnList[headColumnList.Count() - 4] = headerTitle;
         headColumnList[headColumnList.Count() - 3] = prettyHeader;
         headColumnList[headColumnList.Count() - 2] = startDate;
         headColumnList[headColumnList.Count() - 1] = timeZoneBox;
         reportHeaderSection1.Items.AddRange(headColumnList);

         //Telerik.Reporting.DetailSection detailSection1 = new Telerik.Reporting.DetailSection();
         //detailSection1.Height = new Unit(0.2, UnitType.Inch);            
         //detailSection1.ItemDataBound += Detail_ItemDataBound;

         var detailSection1 = report.Items.Find(typeof(Telerik.Reporting.DetailSection), true)[0] as Telerik.Reporting.DetailSection;
         detailSection1.Items.AddRange(detailColumnList);

         report.Items.Add(reportHeaderSection1);
         //report.Items.Add(detailSection1);

         panel1.Width = Unit.Inch(3);
         report.Width = x;
         x += Unit.Inch(2);
         report.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
         report.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(x, Telerik.Reporting.Drawing.Unit.Mm(250));
      }

        /// <summary>
        /// GetInchOverCharacter
        /// </summary>
        /// <param name="CharacterSize"></param>
        /// <returns></returns>
        double GetInchOverCharacter(int CharacterSize)
      {
         return (0.05 * CharacterSize);
      }

      /// <summary>
      /// GetInchOverCharacter
      /// </summary>
      /// <param name="CharacterSize"></param>
      /// <returns></returns>
      Unit GetInchForHeightOverCharacter(int CharacterSize)
      {
         if (CharacterSize >= 22)
            return Unit.Inch(((double)CharacterSize / 22) * 0.32);
         else return Unit.Inch(0.2);
      }

      /// <summary>
      /// Detail ItemDataBound
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Detail_ItemDataBound(object sender, EventArgs e)
      {
         var detail = (DetailSection)sender;
         var elementCollection = ElementTreeHelper.GetChildElements(detail).ToList();
         if (elementCollection.Any())
         {
            var maxHeight = 0.25;
            foreach (var x in elementCollection)
            {
               var textX = (TextBox)x;
               var lines = (textX.Value ?? "").ToString().Trim().Length / 20.0 / textX.Width.Value;
               maxHeight = Math.Max(Math.Ceiling(lines) / 4.0, maxHeight);

            }
            elementCollection.ForEach(x =>
            {
               if (x != null) ((TextBox)x).Height = Unit.Inch(maxHeight + 0.25);
            });
         }
      }

      private string getReportLogoURL()
      {
         try
         {
            var connectString = GetConnectionString(Constants.AzureAccount_KioskSync);
            Console.WriteLine("Connection string get");
            Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);
            Console.WriteLine("Account initialized");
            CloudBlobClient client = account.CreateCloudBlobClient();
            Console.WriteLine("Client initialized");
            CloudBlobContainer sampleContainer = client.GetContainerReference("images");
            Console.WriteLine("Container initialized");
            CloudBlockBlob blob = sampleContainer.GetBlockBlobReference("logo-inner.png");
            Console.WriteLine("File path get");
            return blob.Uri.ToString();
         }
         catch
         {
            Console.WriteLine("Exception during blob upload");
            throw;
         }
      }

      /// <summary>
      /// Get Azure connection String.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      private string GetConnectionString(string account)
      {
         var connectString = string.Empty;

         connectString = ConfigurationManager.GetConnectionString(account);
         if (string.IsNullOrEmpty(connectString))
            throw new ApplicationException(string.Format(Constants.Azure_ConnectionString_NotDefined_Format, account));
         return connectString;
      }
   }
}
