﻿namespace Qualanex.QoskCloud.Web.Areas.Report.Common
{
    /// <summary>
    /// AutoCompleteEntity: Entity class for Auto Complete functionality
    /// </summary>
    public class AutoCompleteEntity
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}