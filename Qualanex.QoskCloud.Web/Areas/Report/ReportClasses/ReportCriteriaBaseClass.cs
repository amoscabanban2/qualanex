﻿using System;
namespace Qualanex.QoskCloud.Web.Areas.Report.ReportClasses
{
    /// <summary>
    /// ReportCriteriaBaseClass: Base class for all report criteria classes.
    /// </summary>
    public class ReportCriteriaBaseClass
    {
        public string ReportDataTimeZone { get; set; }
        public string ReportScheduleTimeZone { get; set; }
        public DateTime? DataStartDate { get; set; }
        public DateTime? DataEndDate { get; set; }
    }
}
