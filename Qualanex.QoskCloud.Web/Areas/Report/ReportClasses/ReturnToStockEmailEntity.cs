﻿using Qualanex.QoskCloud.Web.Areas.Report.Models;
namespace Qualanex.QoskCloud.Web.Areas.Report.ReportClasses
{
    /// <summary>
    /// ReturnToStockEmailEntity: Entity class for Return To Stock report.
    /// </summary>
    public class ReturnToStockEmailEntity : ReturnToStockReportEntity
    {
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DEANumber { get; set; }
        public string ContainerTypeName { get; set; }
        public string ItemGuid { get; set; }
        public string FullQty { get; set; }
        public string PartialQty { get; set; }
        public string SealedOpenCase { get; set; }

    }
}