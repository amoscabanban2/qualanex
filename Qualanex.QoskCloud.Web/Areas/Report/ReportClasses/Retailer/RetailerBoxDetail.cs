﻿using System;

namespace Qualanex.QoskCloud.WebReports.ReportClasses.Retailer
{
    /// <summary>
    ///Class for Sample Reports_Retailer_Box Detail
    /// </summary>
    public class RetailerBoxDetail1 : ReportCriteriaBaseClass
    {
        public long? NDCNumber { get; set; }
        public int? StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string VendorName { get; set; }
        public string ProductDescription { get; set; }
        public string LotNumber { get; set; }
        public DateTime? ExpDate { get; set; }
        public decimal? FullQty { get; set; }
        public decimal? PartialQty { get; set; }
        public decimal? UnitPriceBefore { get; set; }
        public decimal? CreditableAmountBeforeMFGDiscount { get; set; }
        public decimal? IndateAmount { get; set; }
        public decimal? StoreTransferAmount { get; set; }
        public decimal? NonCreditableAmount { get; set; }
        public string OutofPolicyCode { get; set; }
        public string OutofpolicyCodeDescription { get; set; }
      
    }
}
