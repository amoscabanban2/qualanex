﻿using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.Web.Areas.Report.ReportClasses
{
    /// <summary>
    /// PharmacyReportEmailEntity: Entity class for Pharmacy Summary report.
    /// </summary>
    public class PharmacyReportEmailEntity : PharmacySummaryEntity
    {
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DEANumber { get; set; }
        public string ContainerTypeName { get; set; }
        public string QoskProcessDate { get; set; }
        public string VendorName { get; set; }
        public string ItemGuid { get; set; }
        public string NDCNumber { get; set; }
        public string LotNumber { get; set; }
        public string ExpDate { get; set; }
        public string ProductDescription { get; set; }
        public string FullQty { get; set; }
        public string PartialQty { get; set; }
        public string SealedOpenCase { get; set; }

    }
}