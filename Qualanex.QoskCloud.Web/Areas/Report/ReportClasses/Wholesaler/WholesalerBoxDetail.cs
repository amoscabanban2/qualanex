﻿namespace Qualanex.QoskCloud.WebReports.ReportClasses.Wholesaler
{
    /// <summary>
    ///Class for Sample Reports_Wholesaler_Box Detail
    /// </summary>
    public class WholesalerBoxDetail : ReportCriteriaBaseClass
    {       
        public long NDCNumber { get; set; }  
    }
}
