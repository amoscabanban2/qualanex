using System;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
    /// <summary>
    /// Summary description for Waste Report.
    /// </summary>
    public partial class WasteReport : Telerik.Reporting.Report
    {
        public WasteReport()
        {
            InitializeComponent();
            this.NeedDataSource += WasteReport_NeedDataSource;
        }
        /// <summary>
        /// WasteReport_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WasteReport_NeedDataSource(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
            long userID = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_WasteReportListPanel, userID);
            if (subscribedColumns.Count > 0)
            {
                ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
                reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail, report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Context = typeof(Qualanex.QoskCloud.Web.Areas.Report.Models.ReportDataContext);
                entityDataSource.ContextMember = "Get_Waste_Report_Details";
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NDCNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NDCNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_Description, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_Description].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_Strength, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_Strength].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_FullQuantity, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_FullQuantity].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_LotNumber, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_LotNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_SealedOpenCase, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_SealedOpenCase].Value);

                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_ItemGuid, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_ItemGuid].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_WasteCode, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_WasteCode].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_WasteStreamProfile, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_WasteStreamProfile].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_PartialQuantity, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_PartialQuantity].Value);

                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_StoreName, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_StoreName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_StoreNumber, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_StoreNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_ExpDate, typeof(DateTime?), report.Parameters[Utility.Constants.WasteReport_Parameter_ExpDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_PackageSize, typeof(decimal?), report.Parameters[Utility.Constants.WasteReport_Parameter_PackageSize].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_ControlNumber, typeof(int?), report.Parameters[Utility.Constants.WasteReport_Parameter_ControlNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_DosageForm, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_DosageForm].Value);
                entityDataSource.Parameters.Add(Utility.Constants.WasteReport_Parameter_PackageForm, typeof(string), report.Parameters[Utility.Constants.WasteReport_Parameter_PackageForm].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_UserID, typeof(long), report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            entityDataSource.Name = "entityDataSource";

                this.DataSource = entityDataSource;
            }
        }
    }
}