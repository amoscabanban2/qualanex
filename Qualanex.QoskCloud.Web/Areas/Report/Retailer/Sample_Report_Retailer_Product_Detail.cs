using System;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;
using Telerik.Reporting.Processing;
using System.Linq;
using Telerik.Reporting.Drawing;

namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
   /// <summary>
   /// Summary description for Product Details.
   /// </summary>
   public partial class Sample_Report_Retailer_Product_Detail : Telerik.Reporting.Report
   {
      public Sample_Report_Retailer_Product_Detail()
      {
         InitializeComponent();
         this.NeedDataSource += Sample_Report_Retailer_Product_Detail_NeedDataSource;
         this.ItemDataBinding += Sample_Report_Retailer_Product_Detail_ItemDataBinding;
      }

        private void Sample_Report_Retailer_Product_Detail_ItemDataBinding(object sender, EventArgs e)
        {
            var report = (sender as Telerik.Reporting.Processing.Report).ItemDefinition as Telerik.Reporting.Report;

            Telerik.Reporting.DetailSection detailSection1 = new Telerik.Reporting.DetailSection();
            detailSection1.Height = new Unit(0.2, UnitType.Inch);
            detailSection1.ItemDataBound += Detail_ItemDataBound;
            report.Items.Add(detailSection1);        
        }

        private void Detail_ItemDataBound(object sender, EventArgs e)
        {
            var detail = (DetailSection)sender;
            var elementCollection = ElementTreeHelper.GetChildElements(detail).ToList();
            if (elementCollection.Any())
            {
                var maxHeight = 0.25;
                foreach (var x in elementCollection)
                {
                    var textX = (TextBox)x;
                    var lines = (textX.Value ?? "").ToString().Trim().Length / 20.0 / textX.Width.Value;
                    maxHeight = Math.Max(Math.Ceiling(lines) / 4.0, maxHeight);

                }
                elementCollection.ForEach(x =>
                {
                    if (x != null) ((TextBox)x).Height = Unit.Inch(maxHeight + 0.25);
                });
            }
        }

        /// <summary>
        /// Sample_Report_Retailer_Product_Detail_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sample_Report_Retailer_Product_Detail_NeedDataSource(object sender, System.EventArgs e)
      {
         Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
         Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
         long userId = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
         CommonDataAccess commonDataAccess = new CommonDataAccess();
         List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_ProductDetailListPanel, userId);

         if (subscribedColumns.Count > 0)
         {
            ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
            reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail, report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);

            entityDataSource.Context = typeof(Qualanex.QoskCloud.Web.Areas.Report.Models.ReportDataContext);
            entityDataSource.ContextMember = "GetRetailer_Product_Details";

            //get all parameters
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NDCNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NDCNumber].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_StoreName, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_StoreName].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_VendorName, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_VendorName].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ProductDescription, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ProductDescription].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_LotNumber, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_LotNumber].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ExpDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ExpDate].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_FullQty, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_FullQty].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_PartialQty, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_PartialQty].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceBefore, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_UnitPriceBefore].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountBeforeMFGDiscount, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountBeforeMFGDiscount].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ReturnStatus, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ReturnStatus].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_OutofpolicyCodeDescription, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_OutofpolicyCodeDescription].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_Strength, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_Strength].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ItemGuid, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ItemGuid].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_QoskProcessDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_QoskProcessDate].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ControlNumber, typeof(int?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ControlNumber].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_PackageSize, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_PackageSize].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceAfter, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_UnitPriceAfter].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountAfter, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountAfter].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_RecalledProduct, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_RecalledProduct].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_DiscontinuedProduct, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_DiscontinuedProduct].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_RXorOTC, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_RXorOTC].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_DosageForm, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_DosageForm].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_PackageForm, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_PackageForm].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_PartialPercentage, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_PartialPercentage].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_StoreNumber, typeof(string), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_StoreNumber].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_ProductDetail_Parameter_ExtendedDiscountPercent, typeof(decimal?), report.Parameters[Utility.Constants.Report_ProductDetail_Parameter_ExtendedDiscountPercent].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_UserID, typeof(long), report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);

            entityDataSource.Name = "entityDataSource";

            this.DataSource = entityDataSource;
         }

      }

   }
}