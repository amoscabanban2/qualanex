using System;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
    /// <summary>
    /// Summary description for NonCreditableSummary.
    /// </summary>
    public partial class NonCreditableSummary : Telerik.Reporting.Report
    {
        public NonCreditableSummary()
        {
            InitializeComponent();
            this.NeedDataSource += NonCreditableSummary_NeedDataSource;
        }
        /// <summary>
        /// NonCreditableSummary_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NonCreditableSummary_NeedDataSource(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
            long userID = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_NonCreditableSummaryListPanel, userID);
            if (subscribedColumns.Count > 0)
            {
                ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
                reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail, report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Context = typeof(Qualanex.QoskCloud.Web.Areas.Report.Models.ReportDataContext);
                entityDataSource.ContextMember = "Get_Non_Creditable_Summary_Details";
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_StoreNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_StoreName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_RegionName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_RegionName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_ManufacturerName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_ManufacturerName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonCode, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonCode].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonDescription, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonDescription].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_UserID, typeof(long), report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);

            entityDataSource.Name = "entityDataSource";

                this.DataSource = entityDataSource;
            }
        }
    }
}