using System;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
    /// <summary>
    /// Summary description for Return To Stock.
    /// </summary>
    public partial class ReturnToStock : Telerik.Reporting.Report
    {
        public ReturnToStock()
        {
            InitializeComponent();
            this.NeedDataSource += ReturnToStock_NeedDataSource;
        }
        /// <summary>
        /// ReturnToStock_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnToStock_NeedDataSource(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
            long userID = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_ReturnToStockListPanel, userID);
            if (subscribedColumns.Count > 0)
            {
                ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
                reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail, report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Context = typeof(Qualanex.QoskCloud.Web.Areas.Report.Models.ReportDataContext);
                entityDataSource.ContextMember = "Get_Return_To_Stock_Details";
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_VendorName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_VendorName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_StoreName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_StoreName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_StoreNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_StoreNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_NDCNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_NDCNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_ProductDescription, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_ProductDescription].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_Strength, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_Strength].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_ControlNumber, typeof(int?), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_ControlNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_RXorOTC, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_RXorOTC].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_DosageForm, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_DosageForm].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_PackageForm, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_PackageForm].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_LotNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_LotNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_ExpDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_ExpDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_QoskProcessDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_QoskProcessDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_DateEligibleForCredit, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_DateEligibleForCredit].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ReturnToStock_OutofpolicyDescription, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReturnToStock_OutofpolicyDescription].Value);
            entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_UserID, typeof(long), report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);

            entityDataSource.Name = "entityDataSource";

                this.DataSource = entityDataSource;
            }
        }
    }
}