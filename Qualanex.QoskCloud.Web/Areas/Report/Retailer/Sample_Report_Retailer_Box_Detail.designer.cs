namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
   partial class Sample_Report_Retailer_Box_Detail
   {
      #region Component Designer generated code
      /// <summary>
      /// Required method for telerik Reporting designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sample_Report_Retailer_Box_Detail));
         Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
         this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
         this.textBox4 = new Telerik.Reporting.TextBox();
         this.textBox6 = new Telerik.Reporting.TextBox();
         this.textBox7 = new Telerik.Reporting.TextBox();
         this.textBox10 = new Telerik.Reporting.TextBox();
         this.textBox11 = new Telerik.Reporting.TextBox();
         this.textBox24 = new Telerik.Reporting.TextBox();
         this.textBox27 = new Telerik.Reporting.TextBox();
         this.textBox28 = new Telerik.Reporting.TextBox();
         this.textBox30 = new Telerik.Reporting.TextBox();
         this.panel1 = new Telerik.Reporting.Panel();
         this.txtReportName = new Telerik.Reporting.TextBox();
         this.txtStartDate = new Telerik.Reporting.TextBox();
         this.txtTimeZoneValue = new Telerik.Reporting.TextBox();
         this.txtEndDate = new Telerik.Reporting.TextBox();
         this.txtStartDateValue = new Telerik.Reporting.TextBox();
         this.txtEndDateValue = new Telerik.Reporting.TextBox();
         this.textTimeZone = new Telerik.Reporting.TextBox();
         this.textBox12 = new Telerik.Reporting.TextBox();
         this.textBox17 = new Telerik.Reporting.TextBox();
         this.textBox8 = new Telerik.Reporting.TextBox();
         this.textBox19 = new Telerik.Reporting.TextBox();
         this.textBox1 = new Telerik.Reporting.TextBox();
         this.textBox15 = new Telerik.Reporting.TextBox();
         this.textBox16 = new Telerik.Reporting.TextBox();
         this.textBox5 = new Telerik.Reporting.TextBox();
         this.textBox18 = new Telerik.Reporting.TextBox();
         this.textBox2 = new Telerik.Reporting.TextBox();
         this.textBox60 = new Telerik.Reporting.TextBox();
         this.textBox13 = new Telerik.Reporting.TextBox();
         this.textBox20 = new Telerik.Reporting.TextBox();
         this.textBox3 = new Telerik.Reporting.TextBox();
         this.textBox21 = new Telerik.Reporting.TextBox();
         this.textBox22 = new Telerik.Reporting.TextBox();
         this.textBox14 = new Telerik.Reporting.TextBox();
         this.textBox9 = new Telerik.Reporting.TextBox();
         this.textBox29 = new Telerik.Reporting.TextBox();
         this.textBox26 = new Telerik.Reporting.TextBox();
         this.detail = new Telerik.Reporting.DetailSection();
         this.textBox48 = new Telerik.Reporting.TextBox();
         this.textBox62 = new Telerik.Reporting.TextBox();
         this.textBox55 = new Telerik.Reporting.TextBox();
         this.txtExpirationDate = new Telerik.Reporting.TextBox();
         this.textBox41 = new Telerik.Reporting.TextBox();
         this.textBox40 = new Telerik.Reporting.TextBox();
         this.textBox37 = new Telerik.Reporting.TextBox();
         this.textBox47 = new Telerik.Reporting.TextBox();
         this.textBox46 = new Telerik.Reporting.TextBox();
         this.textBox45 = new Telerik.Reporting.TextBox();
         this.textBox44 = new Telerik.Reporting.TextBox();
         this.textBox43 = new Telerik.Reporting.TextBox();
         this.textBox42 = new Telerik.Reporting.TextBox();
         this.textBox35 = new Telerik.Reporting.TextBox();
         this.textBox34 = new Telerik.Reporting.TextBox();
         this.textBox33 = new Telerik.Reporting.TextBox();
         this.textBox32 = new Telerik.Reporting.TextBox();
         this.textBox31 = new Telerik.Reporting.TextBox();
         this.textBox39 = new Telerik.Reporting.TextBox();
         this.textBox49 = new Telerik.Reporting.TextBox();
         this.textBox59 = new Telerik.Reporting.TextBox();
         this.textBox56 = new Telerik.Reporting.TextBox();
         this.textBox38 = new Telerik.Reporting.TextBox();
         this.textBox53 = new Telerik.Reporting.TextBox();
         this.textBox51 = new Telerik.Reporting.TextBox();
         this.textBox50 = new Telerik.Reporting.TextBox();
         this.textBox36 = new Telerik.Reporting.TextBox();
         this.textBox57 = new Telerik.Reporting.TextBox();
         this.textBox58 = new Telerik.Reporting.TextBox();
         this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
         this.textBox61 = new Telerik.Reporting.TextBox();
         this.txtReportGeneratedTime = new Telerik.Reporting.TextBox();
         this.entityDataSource1 = new Telerik.Reporting.EntityDataSource();
         ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
         // 
         // pageHeaderSection1
         // 
         this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.50000190734863281D);
         this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox6,
            this.textBox7,
            this.textBox10,
            this.textBox11,
            this.textBox24,
            this.textBox27,
            this.textBox28,
            this.textBox30,
            this.panel1,
            this.textBox12,
            this.textBox17,
            this.textBox8,
            this.textBox19,
            this.textBox1,
            this.textBox15,
            this.textBox16,
            this.textBox5,
            this.textBox18,
            this.textBox2,
            this.textBox60,
            this.textBox13,
            this.textBox20,
            this.textBox3,
            this.textBox21,
            this.textBox22,
            this.textBox14,
            this.textBox9,
            this.textBox29,
            this.textBox26});
         this.pageHeaderSection1.Name = "pageHeaderSection1";
         // 
         // textBox4
         // 
         this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(15.649981498718262D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
         this.textBox4.Name = "textBox4";
         this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992120265960693D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox4.Style.Font.Bold = true;
         this.textBox4.Value = "Dosage Form";
         // 
         // textBox6
         // 
         this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(18.150102615356445D), Telerik.Reporting.Drawing.Unit.Inch(0.29999867081642151D));
         this.textBox6.Name = "textBox6";
         this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox6.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox6.Style.Font.Bold = true;
         this.textBox6.Value = "Lot #";
         // 
         // textBox7
         // 
         this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(19.050180435180664D), Telerik.Reporting.Drawing.Unit.Inch(0.30000028014183044D));
         this.textBox7.Name = "textBox7";
         this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3996853828430176D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox7.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox7.Style.Font.Bold = true;
         this.textBox7.Value = "Exp. Date";
         // 
         // textBox10
         // 
         this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(21.449943542480469D), Telerik.Reporting.Drawing.Unit.Inch(0.29999861121177673D));
         this.textBox10.Name = "textBox10";
         this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69999933242797852D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox10.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox10.Style.Font.Bold = true;
         this.textBox10.Value = "Partial %";
         // 
         // textBox11
         // 
         this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(22.1500186920166D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox11.Name = "textBox11";
         this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.200000524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox11.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox11.Style.Font.Bold = true;
         this.textBox11.Value = "Waste Reason Code Description";
         // 
         // textBox24
         // 
         this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(28.850044250488281D), Telerik.Reporting.Drawing.Unit.Inch(0.30000177025794983D));
         this.textBox24.Name = "textBox24";
         this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999248027801514D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox24.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox24.Style.Font.Bold = true;
         this.textBox24.Value = "Waste Amount";
         // 
         // textBox27
         // 
         this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(32.250045776367188D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox27.Name = "textBox27";
         this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.79992413520813D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox27.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox27.Style.Font.Bold = true;
         this.textBox27.Value = "Creditable Amount (After MFG Discount)";
         // 
         // textBox28
         // 
         this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(35.050045013427734D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
         this.textBox28.Name = "textBox28";
         this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8999226093292236D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox28.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox28.Style.Font.Bold = true;
         this.textBox28.Value = "Creditable Amount (Before MFG Discount)";
         // 
         // textBox30
         // 
         this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(39.850051879882812D), Telerik.Reporting.Drawing.Unit.Inch(0.299999475479126D));
         this.textBox30.Name = "textBox30";
         this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999251127243042D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox30.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox30.Style.Font.Bold = true;
         this.textBox30.Value = "Unit price(After Discount)";
         // 
         // panel1
         // 
         this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtReportName,
            this.txtStartDate,
            this.txtTimeZoneValue,
            this.txtEndDate,
            this.txtStartDateValue,
            this.txtEndDateValue,
            this.textTimeZone});
         this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(1.9868215517249155E-08D));
         this.panel1.Name = "panel1";
         this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(41.749935150146484D), Telerik.Reporting.Drawing.Unit.Inch(0.30007877945899963D));
         this.panel1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(127)))), ((int)(((byte)(182)))));
         this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         // 
         // txtReportName
         // 
         this.txtReportName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtReportName.Name = "txtReportName";
         this.txtReportName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0998423099517822D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtReportName.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
         this.txtReportName.Style.Color = System.Drawing.Color.White;
         this.txtReportName.Style.Font.Bold = true;
         this.txtReportName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
         this.txtReportName.Value = "Sample Report Retailer Box Detail";
         // 
         // txtStartDate
         // 
         this.txtStartDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1999607086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtStartDate.Name = "txtStartDate";
         this.txtStartDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7999998927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtStartDate.Style.Color = System.Drawing.Color.White;
         this.txtStartDate.Style.Font.Bold = true;
         this.txtStartDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.txtStartDate.Value = "StartDate :";
         // 
         // txtTimeZoneValue
         // 
         this.txtTimeZoneValue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(10.799961090087891D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtTimeZoneValue.Name = "txtTimeZoneValue";
         this.txtTimeZoneValue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.200000524520874D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtTimeZoneValue.Style.Color = System.Drawing.Color.White;
         this.txtTimeZoneValue.Style.Font.Bold = true;
         this.txtTimeZoneValue.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.txtTimeZoneValue.Value = "= Parameters.ReportDataTimeZone.Value";
         // 
         // txtEndDate
         // 
         this.txtEndDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtEndDate.Name = "txtEndDate";
         this.txtEndDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000025033950806D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtEndDate.Style.Color = System.Drawing.Color.White;
         this.txtEndDate.Style.Font.Bold = true;
         this.txtEndDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.txtEndDate.Value = "EndDate :";
         // 
         // txtStartDateValue
         // 
         this.txtStartDateValue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9999606609344482D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtStartDateValue.Name = "txtStartDateValue";
         this.txtStartDateValue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000007390975952D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtStartDateValue.Style.Color = System.Drawing.Color.White;
         this.txtStartDateValue.Style.Font.Bold = true;
         this.txtStartDateValue.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.txtStartDateValue.Value = "= Parameters.DataStartDate.Value";
         // 
         // txtEndDateValue
         // 
         this.txtEndDateValue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7999610900878906D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.txtEndDateValue.Name = "txtEndDateValue";
         this.txtEndDateValue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.txtEndDateValue.Style.Color = System.Drawing.Color.White;
         this.txtEndDateValue.Style.Font.Bold = true;
         this.txtEndDateValue.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.txtEndDateValue.Value = "= Parameters.DataEndDate.Value";
         // 
         // textTimeZone
         // 
         this.textTimeZone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(9.0999612808227539D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
         this.textTimeZone.Name = "textTimeZone";
         this.textTimeZone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7000004053115845D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
         this.textTimeZone.Style.Color = System.Drawing.Color.White;
         this.textTimeZone.Style.Font.Bold = true;
         this.textTimeZone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
         this.textTimeZone.Value = "Time Zone Report Data :";
         // 
         // textBox12
         // 
         this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(27.649969100952148D), Telerik.Reporting.Drawing.Unit.Inch(0.30000177025794983D));
         this.textBox12.Name = "textBox12";
         this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999205350875855D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox12.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox12.Style.Font.Bold = true;
         this.textBox12.Value = "Override Amount";
         // 
         // textBox17
         // 
         this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(26.150091171264648D), Telerik.Reporting.Drawing.Unit.Inch(0.30000177025794983D));
         this.textBox17.Name = "textBox17";
         this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999223947525024D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox17.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox17.Style.Font.Bold = true;
         this.textBox17.Value = "Waste Reason code #";
         // 
         // textBox8
         // 
         this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(25.150093078613281D), Telerik.Reporting.Drawing.Unit.Inch(0.30000177025794983D));
         this.textBox8.Name = "textBox8";
         this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992036819458008D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox8.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox8.Style.Font.Bold = true;
         this.textBox8.Value = "Full Quantity";
         // 
         // textBox19
         // 
         this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox19.Name = "textBox19";
         this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992108345031738D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox19.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox19.Style.Font.Bold = true;
         this.textBox19.Value = "Store Number";
         // 
         // textBox1
         // 
         this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0000790357589722D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992108345031738D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox1.Style.Font.Bold = true;
         this.textBox1.Value = "Store Name";
         // 
         // textBox15
         // 
         this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0000789165496826D), Telerik.Reporting.Drawing.Unit.Inch(0.299999475479126D));
         this.textBox15.Name = "textBox15";
         this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992108345031738D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox15.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox15.Style.Font.Bold = true;
         this.textBox15.Value = "Region Name";
         // 
         // textBox16
         // 
         this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0000789165496826D), Telerik.Reporting.Drawing.Unit.Inch(0.29999938607215881D));
         this.textBox16.Name = "textBox16";
         this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89992004632949829D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox16.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox16.Style.Font.Bold = true;
         this.textBox16.Value = "Box Number";
         // 
         // textBox5
         // 
         this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(17.449983596801758D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
         this.textBox5.Name = "textBox5";
         this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69992077350616455D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox5.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox5.Style.Font.Bold = true;
         this.textBox5.Value = "Strength";
         // 
         // textBox18
         // 
         this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9000790119171143D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox18.Name = "textBox18";
         this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992032051086426D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox18.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox18.Style.Font.Bold = true;
         this.textBox18.Value = "ASN #";
         // 
         // textBox2
         // 
         this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.70007848739624D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
         this.textBox2.Name = "textBox2";
         this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2499401569366455D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
         this.textBox2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox2.Style.Font.Bold = true;
         this.textBox2.Value = "Process Date";
         // 
         // textBox60
         // 
         this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9500975608825684D), Telerik.Reporting.Drawing.Unit.Inch(0.29999923706054688D));
         this.textBox60.Name = "textBox60";
         this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7998834848403931D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
         this.textBox60.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox60.Style.Font.Bold = true;
         this.textBox60.Value = "Disposal Date";
         // 
         // textBox13
         // 
         this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.7500600814819336D), Telerik.Reporting.Drawing.Unit.Inch(0.30000028014183044D));
         this.textBox13.Name = "textBox13";
         this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999215126037598D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox13.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox13.Style.Font.Bold = true;
         this.textBox13.Value = "Vendor Number (Optional) ";
         // 
         // textBox20
         // 
         this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(10.550060272216797D), Telerik.Reporting.Drawing.Unit.Inch(0.30000033974647522D));
         this.textBox20.Name = "textBox20";
         this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999203205108643D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox20.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox20.Style.Font.Bold = true;
         this.textBox20.Value = "Vendor Name";
         // 
         // textBox3
         // 
         this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(11.850059509277344D), Telerik.Reporting.Drawing.Unit.Inch(0.30000042915344238D));
         this.textBox3.Name = "textBox3";
         this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0998427867889404D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox3.Style.Font.Bold = true;
         this.textBox3.Value = "NDC";
         // 
         // textBox21
         // 
         this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(12.949981689453125D), Telerik.Reporting.Drawing.Unit.Inch(0.30000051856040955D));
         this.textBox21.Name = "textBox21";
         this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.699920654296875D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox21.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox21.Style.Font.Bold = true;
         this.textBox21.Value = "Retailer Item # (Optional)";
         // 
         // textBox22
         // 
         this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(14.649981498718262D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
         this.textBox22.Name = "textBox22";
         this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992120265960693D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox22.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox22.Style.Font.Bold = true;
         this.textBox22.Value = "NDA Name";
         // 
         // textBox14
         // 
         this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(24.350095748901367D), Telerik.Reporting.Drawing.Unit.Inch(0.30000171065330505D));
         this.textBox14.Name = "textBox14";
         this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992157220840454D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox14.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox14.Style.Font.Bold = true;
         this.textBox14.Value = "DEA Class";
         // 
         // textBox9
         // 
         this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(16.649984359741211D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
         this.textBox9.Name = "textBox9";
         this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992085695266724D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox9.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox9.Style.Font.Bold = true;
         this.textBox9.Value = "Pkg Size";
         // 
         // textBox29
         // 
         this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(37.950046539306641D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
         this.textBox29.Name = "textBox29";
         this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999251127243042D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox29.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox29.Style.Font.Bold = true;
         this.textBox29.Value = "Unit price(Before Discount)";
         // 
         // textBox26
         // 
         this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(29.950044631958008D), Telerik.Reporting.Drawing.Unit.Inch(0.30000194907188416D));
         this.textBox26.Name = "textBox26";
         this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.29992413520813D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox26.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
         this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox26.Style.Font.Bold = true;
         this.textBox26.Value = "In date Amount (Moved to Stock)";
         // 
         // detail
         // 
         this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000068843364716D);
         this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox48,
            this.textBox62,
            this.textBox55,
            this.txtExpirationDate,
            this.textBox41,
            this.textBox40,
            this.textBox37,
            this.textBox47,
            this.textBox46,
            this.textBox45,
            this.textBox44,
            this.textBox43,
            this.textBox42,
            this.textBox35,
            this.textBox34,
            this.textBox33,
            this.textBox32,
            this.textBox31,
            this.textBox39,
            this.textBox49,
            this.textBox59,
            this.textBox56,
            this.textBox38,
            this.textBox53,
            this.textBox51,
            this.textBox50,
            this.textBox36,
            this.textBox57,
            this.textBox58});
         this.detail.Name = "detail";
         // 
         // textBox48
         // 
         this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8996024967636913E-05D), Telerik.Reporting.Drawing.Unit.Inch(7.9472862068996619E-08D));
         this.textBox48.Name = "textBox48";
         this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox48.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox48.Value = "";
         // 
         // textBox62
         // 
         this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9501762390136719D), Telerik.Reporting.Drawing.Unit.Inch(3.97364289028701E-07D));
         this.textBox62.Name = "textBox62";
         this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7998834848403931D), Telerik.Reporting.Drawing.Unit.Inch(0.20000013709068298D));
         this.textBox62.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox62.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox62.Value = "";
         // 
         // textBox55
         // 
         this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(29.950044631958008D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox55.Name = "textBox55";
         this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999253273010254D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox55.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox55.Value = "";
         // 
         // txtExpirationDate
         // 
         this.txtExpirationDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(19.050260543823242D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.txtExpirationDate.Name = "txtExpirationDate";
         this.txtExpirationDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3996446132659912D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.txtExpirationDate.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.txtExpirationDate.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.txtExpirationDate.Value = "=Fields.ExpirationDate";
         // 
         // textBox41
         // 
         this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(27.650047302246094D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox41.Name = "textBox41";
         this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999205350875855D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox41.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox41.Value = "=Fields.OverrideAmount";
         // 
         // textBox40
         // 
         this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(22.149978637695312D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox40.Name = "textBox40";
         this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000796794891357D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox40.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox40.Value = "=Fields.WasteReasonCodeDescription";
         // 
         // textBox37
         // 
         this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(25.150053024291992D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox37.Name = "textBox37";
         this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99996107816696167D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox37.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox37.Value = "=Fields.FullQty";
         // 
         // textBox47
         // 
         this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9001579284667969D), Telerik.Reporting.Drawing.Unit.Inch(2.384185791015625E-07D));
         this.textBox47.Name = "textBox47";
         this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992032051086426D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox47.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox47.Value = "";
         // 
         // textBox46
         // 
         this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(26.150049209594727D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox46.Name = "textBox46";
         this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999222755432129D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox46.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox46.Value = "=Fields.WasteReasonCode";
         // 
         // textBox45
         // 
         this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0001575946807861D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox45.Name = "textBox45";
         this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89992129802703857D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox45.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox45.Value = "=Fields.BoxNumber";
         // 
         // textBox44
         // 
         this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0001578330993652D), Telerik.Reporting.Drawing.Unit.Inch(7.9472862068996619E-08D));
         this.textBox44.Name = "textBox44";
         this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox44.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox44.Value = "=Fields.RegionName";
         // 
         // textBox43
         // 
         this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(24.350053787231445D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox43.Name = "textBox43";
         this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992157220840454D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox43.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox43.Value = "=Fields.DEAClass";
         // 
         // textBox42
         // 
         this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.7501382827758789D), Telerik.Reporting.Drawing.Unit.Inch(5.5630999895583955E-07D));
         this.textBox42.Name = "textBox42";
         this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999215126037598D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox42.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox42.Value = "=Fields.VendorNumber";
         // 
         // textBox35
         // 
         this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(18.150180816650391D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox35.Name = "textBox35";
         this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000015497207642D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox35.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox35.Value = "=Fields.LotNumbersID";
         // 
         // textBox34
         // 
         this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(17.450063705444336D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox34.Name = "textBox34";
         this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69992053508758545D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox34.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox34.Value = "=Fields.Strength";
         // 
         // textBox33
         // 
         this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(15.650059700012207D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox33.Name = "textBox33";
         this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox33.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox33.Value = "=Fields.DosageForm";
         // 
         // textBox32
         // 
         this.textBox32.Format = "{0:#.}";
         this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(11.850138664245606D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox32.Name = "textBox32";
         this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0997641086578369D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox32.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox32.Value = "= CStr(Fields.NDC)";
         // 
         // textBox31
         // 
         this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7001571655273438D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox31.Name = "textBox31";
         this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2499401569366455D), Telerik.Reporting.Drawing.Unit.Inch(0.20000013709068298D));
         this.textBox31.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox31.Value = "=Fields.ProcessDate";
         // 
         // textBox39
         // 
         this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(21.449981689453125D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox39.Name = "textBox39";
         this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69992178678512573D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox39.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox39.Value = "=Fields.PartialQty";
         // 
         // textBox49
         // 
         this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(10.550139427185059D), Telerik.Reporting.Drawing.Unit.Inch(5.5630999895583955E-07D));
         this.textBox49.Name = "textBox49";
         this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999203205108643D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox49.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox49.Value = "=Fields.VendorName";
         // 
         // textBox59
         // 
         this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(39.850090026855469D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox59.Name = "textBox59";
         this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.899888277053833D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox59.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox59.Value = "=Fields.UnitPriceAfter";
         // 
         // textBox56
         // 
         this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(32.250041961669922D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox56.Name = "textBox56";
         this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999255657196045D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox56.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox56.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox56.Value = "=Fields.CreditableAmountAfter";
         // 
         // textBox38
         // 
         this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(16.650062561035156D), Telerik.Reporting.Drawing.Unit.Inch(7.152557373046875E-07D));
         this.textBox38.Name = "textBox38";
         this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.799920916557312D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox38.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox38.Value = "=Fields.PackageSize";
         // 
         // textBox53
         // 
         this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(28.850044250488281D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox53.Name = "textBox53";
         this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999258756637573D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox53.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox53.Value = "=Fields.WasteAmount";
         // 
         // textBox51
         // 
         this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(14.650059700012207D), Telerik.Reporting.Drawing.Unit.Inch(7.152557373046875E-07D));
         this.textBox51.Name = "textBox51";
         this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992233514785767D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox51.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox51.TextWrap = true;
         this.textBox51.Value = "=Fields.NDAName";
         // 
         // textBox50
         // 
         this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(12.950060844421387D), Telerik.Reporting.Drawing.Unit.Inch(7.152557373046875E-07D));
         this.textBox50.Name = "textBox50";
         this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6999218463897705D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox50.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox50.Value = "";
         // 
         // textBox36
         // 
         this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0001581907272339D), Telerik.Reporting.Drawing.Unit.Inch(7.9472862068996619E-08D));
         this.textBox36.Name = "textBox36";
         this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox36.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox36.Value = "";
         // 
         // textBox57
         // 
         this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(35.050045013427734D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox57.Name = "textBox57";
         this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8999240398406982D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox57.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox57.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox57.Value = "=Fields.CreditableAmountBefore";
         // 
         // textBox58
         // 
         this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(37.950046539306641D), Telerik.Reporting.Drawing.Unit.Inch(0D));
         this.textBox58.Name = "textBox58";
         this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8999264240264893D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox58.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));
         this.textBox58.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
         this.textBox58.Value = "=Fields.UnitPriceBefore";
         // 
         // pageFooterSection1
         // 
         this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.6999213695526123D);
         this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox61,
            this.txtReportGeneratedTime});
         this.pageFooterSection1.Name = "pageFooterSection1";
         // 
         // textBox61
         // 
         this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(40.549980163574219D), Telerik.Reporting.Drawing.Unit.Inch(0.29984235763549805D));
         this.textBox61.Name = "textBox61";
         this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999994516372681D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox61.Value = "=PageNumber";
         // 
         // txtReportGeneratedTime
         // 
         this.txtReportGeneratedTime.Format = "{0}";
         this.txtReportGeneratedTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.29992151260375977D));
         this.txtReportGeneratedTime.Name = "txtReportGeneratedTime";
         this.txtReportGeneratedTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999209880828857D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.txtReportGeneratedTime.Value = resources.GetString("txtReportGeneratedTime.Value");
         // 
         // entityDataSource1
         // 

         this.entityDataSource1.Context = typeof(Qualanex.QoskCloud.Web.Areas.Report.Models.ReportDataContext);
         this.entityDataSource1.ContextMember = "GetRetailer_Box_Details";
         this.entityDataSource1.Name = "entityDataSource1";
         this.entityDataSource1.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("NDCNumber", typeof(long), "= Parameters.NDCNumber.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("DataStartDate", typeof(System.Nullable<System.DateTime>), "= Parameters.DataStartDate.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("DataEndDate", typeof(System.Nullable<System.DateTime>), "= Parameters.DataEndDate.Value"),
            new Telerik.Reporting.ObjectDataSourceParameter("timezone", typeof(string), "= Parameters.ReportDataTimeZone.Value")});
         // 
         // Sample_Report_Retailer_Box_Detail
         // 
         this.DataSource = this.entityDataSource1;
         this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
         this.Name = "Sample_Report_Retailer_Box_Details";
         this.PageSettings.Landscape = false;
         this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
         this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
         this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(48D), Telerik.Reporting.Drawing.Unit.Mm(250D));
         reportParameter1.Name = "NDCNumber";
         reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
         reportParameter2.Name = "ReportDataTimeZone";
         reportParameter3.AllowNull = true;
         reportParameter3.Name = "DataStartDate";
         reportParameter3.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter4.AllowNull = true;
         reportParameter4.Name = "DataEndDate";
         reportParameter4.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter5.Name = "ReportScheduleTimeZone";
         this.ReportParameters.Add(reportParameter1);
         this.ReportParameters.Add(reportParameter2);
         this.ReportParameters.Add(reportParameter3);
         this.ReportParameters.Add(reportParameter4);
         this.ReportParameters.Add(reportParameter5);
         styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
         styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
         styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
         this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
         this.Width = Telerik.Reporting.Drawing.Unit.Inch(41.749977111816406D);
         ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

      }


      #endregion

      private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
      private Telerik.Reporting.DetailSection detail;
      private Telerik.Reporting.PageFooterSection pageFooterSection1;
      private Telerik.Reporting.TextBox textBox2;
      private Telerik.Reporting.TextBox textBox3;
      private Telerik.Reporting.TextBox textBox4;
      private Telerik.Reporting.TextBox textBox5;
      private Telerik.Reporting.TextBox textBox6;
      private Telerik.Reporting.TextBox textBox7;
      private Telerik.Reporting.TextBox textBox8;
      private Telerik.Reporting.TextBox textBox9;
      private Telerik.Reporting.TextBox textBox10;
      private Telerik.Reporting.TextBox textBox11;
      private Telerik.Reporting.TextBox textBox12;
      private Telerik.Reporting.TextBox textBox13;
      private Telerik.Reporting.TextBox textBox14;
      private Telerik.Reporting.TextBox textBox15;
      private Telerik.Reporting.TextBox textBox16;
      private Telerik.Reporting.TextBox textBox17;
      private Telerik.Reporting.TextBox textBox18;
      private Telerik.Reporting.TextBox textBox19;
      private Telerik.Reporting.TextBox textBox20;
      private Telerik.Reporting.TextBox textBox21;
      private Telerik.Reporting.TextBox textBox22;
      private Telerik.Reporting.TextBox textBox24;
      private Telerik.Reporting.TextBox textBox26;
      private Telerik.Reporting.TextBox textBox27;
      private Telerik.Reporting.TextBox textBox28;
      private Telerik.Reporting.TextBox textBox29;
      private Telerik.Reporting.TextBox textBox30;
      private Telerik.Reporting.TextBox textBox31;
      private Telerik.Reporting.TextBox textBox32;
      private Telerik.Reporting.TextBox textBox33;
      private Telerik.Reporting.TextBox textBox34;
      private Telerik.Reporting.TextBox textBox35;
      private Telerik.Reporting.TextBox txtExpirationDate;
      private Telerik.Reporting.TextBox textBox38;
      private Telerik.Reporting.TextBox textBox39;
      private Telerik.Reporting.TextBox textBox40;
      private Telerik.Reporting.TextBox textBox41;
      private Telerik.Reporting.TextBox textBox42;
      private Telerik.Reporting.TextBox textBox43;
      private Telerik.Reporting.TextBox textBox45;
      private Telerik.Reporting.TextBox textBox46;
      private Telerik.Reporting.TextBox textBox47;
      private Telerik.Reporting.TextBox textBox48;
      private Telerik.Reporting.TextBox textBox49;
      private Telerik.Reporting.TextBox textBox50;
      private Telerik.Reporting.TextBox textBox51;
      private Telerik.Reporting.TextBox textBox53;
      private Telerik.Reporting.TextBox textBox55;
      private Telerik.Reporting.TextBox textBox56;
      private Telerik.Reporting.TextBox textBox57;
      private Telerik.Reporting.TextBox textBox58;
      private Telerik.Reporting.TextBox textBox59;
      private Telerik.Reporting.TextBox textBox61;
      private Telerik.Reporting.EntityDataSource entityDataSource1;
      private Telerik.Reporting.TextBox textBox37;
      private Telerik.Reporting.Panel panel1;
      private Telerik.Reporting.TextBox txtReportName;
      private Telerik.Reporting.TextBox txtStartDate;
      private Telerik.Reporting.TextBox txtTimeZoneValue;
      private Telerik.Reporting.TextBox txtEndDate;
      private Telerik.Reporting.TextBox txtStartDateValue;
      private Telerik.Reporting.TextBox txtEndDateValue;
      private Telerik.Reporting.TextBox textTimeZone;
      private Telerik.Reporting.TextBox txtReportGeneratedTime;
      private Telerik.Reporting.TextBox textBox1;
      private Telerik.Reporting.TextBox textBox60;
      private Telerik.Reporting.TextBox textBox36;
      private Telerik.Reporting.TextBox textBox44;
      private Telerik.Reporting.TextBox textBox62;
   }
}