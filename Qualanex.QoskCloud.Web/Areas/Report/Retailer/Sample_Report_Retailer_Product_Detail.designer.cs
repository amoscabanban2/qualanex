namespace Qualanex.QoskCloud.Web.Areas.Report.Retailer
{
   partial class Sample_Report_Retailer_Product_Detail
   {
      #region Component Designer generated code
      /// <summary>
      /// Required method for telerik Reporting designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sample_Report_Retailer_Product_Detail));
         Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter13 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter14 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter15 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter16 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter17 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter18 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter19 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter20 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter21 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter22 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter23 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter24 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter25 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter26 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter27 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter28 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter29 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter30 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter31 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter32 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.ReportParameter reportParameter33 = new Telerik.Reporting.ReportParameter();
         Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
         this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
         this.panel1 = new Telerik.Reporting.Panel();
         this.txtReportName = new Telerik.Reporting.TextBox();
         this.detail = new Telerik.Reporting.DetailSection();
         this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
         this.textBox61 = new Telerik.Reporting.TextBox();
         this.txtReportGeneratedTime = new Telerik.Reporting.TextBox();
         ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();

         // 
         // pageHeaderSection1
         // 
         this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.600001335144043D);
         this.pageHeaderSection1.Name = "pageHeaderSection1";
         this.pageHeaderSection1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));

         // 
         // panel1
         // 
         this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {this.txtReportName});
         this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0), Telerik.Reporting.Drawing.Unit.Inch(0));
         this.panel1.Name = "panel1";
         this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0), Telerik.Reporting.Drawing.Unit.Inch(0));
         //this.panel1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(127)))), ((int)(((byte)(182)))));
         //this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;

         // 
         // txtReportName
         // 
         this.txtReportName.CanGrow = false;
         this.txtReportName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
         this.txtReportName.Name = "txtReportName";
         this.txtReportName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
         this.txtReportName.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
         this.txtReportName.Style.Color = System.Drawing.Color.White;
         this.txtReportName.Style.Font.Bold = true;
         this.txtReportName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
         this.txtReportName.Value = "Product Details";

         // 
         // detail
         // 
         this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20015604794025421D);
         this.detail.Name = "detail";
         this.detail.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(236)))), ((int)(((byte)(244)))));

         // 
         // pageFooterSection1
         // 
         this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.6999213695526123D);
         this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] { this.textBox61, this.txtReportGeneratedTime });
         this.pageFooterSection1.Name = "pageFooterSection1";

         // 
         // textBox61
         // 
         this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(51.900005340576172D), Telerik.Reporting.Drawing.Unit.Inch(0.2999214231967926D));
         this.textBox61.Name = "textBox61";
         this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999994516372681D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.textBox61.Value = "=PageNumber";

         // 
         // txtReportGeneratedTime
         // 
         this.txtReportGeneratedTime.Format = "{0}";
         this.txtReportGeneratedTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.29992151260375977D));
         this.txtReportGeneratedTime.Name = "txtReportGeneratedTime";
         this.txtReportGeneratedTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.5999209880828857D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
         this.txtReportGeneratedTime.Value = resources.GetString("txtReportGeneratedTime.Value");
         
         // 
         // Product Details
         // 
         this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] { this.pageFooterSection1 });
         this.Name = "Product Details";
         this.PageSettings.Landscape = false;
         this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
         this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
         reportParameter1.AllowNull = true;
         reportParameter1.Name = "NDCNumber";
         reportParameter2.AllowNull = true;
         reportParameter2.Name = "ReportDataTimeZone";
         reportParameter3.AllowNull = true;
         reportParameter3.Name = "DataStartDate";
         reportParameter3.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter4.AllowNull = true;
         reportParameter4.Name = "DataEndDate";
         reportParameter4.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter5.AllowNull = true;
         reportParameter5.Name = "ReportScheduleTimeZone";
         reportParameter6.AllowNull = true;
         reportParameter6.Name = "FileType";
         reportParameter7.AllowNull = true;
         reportParameter7.Name = "StoreName";
         reportParameter8.AllowNull = true;
         reportParameter8.Name = "VendorName";
         reportParameter9.AllowNull = true;
         reportParameter9.Name = "ProductDescription";
         reportParameter10.AllowNull = true;
         reportParameter10.Name = "LotNumber";
         reportParameter11.AllowNull = true;
         reportParameter11.Name = "ExpDate";
         reportParameter11.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter12.AllowNull = true;
         reportParameter12.Name = "FullQty";
         reportParameter12.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter13.AllowNull = true;
         reportParameter13.Name = "PartialQty";
         reportParameter13.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter14.AllowNull = true;
         reportParameter14.Name = "UnitPriceBefore";
         reportParameter14.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter15.AllowNull = true;
         reportParameter15.Name = "CreditableAmountBeforeMFGDiscount";
         reportParameter15.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter16.AllowNull = true;
         reportParameter16.Name = "ReturnStatus";
         reportParameter17.AllowNull = true;
         reportParameter17.Name = "OutofpolicyDescription";
         reportParameter18.AllowNull = true;
         reportParameter18.Name = "Strength";
         reportParameter19.AllowNull = true;
         reportParameter19.Name = "ItemGuid";
         reportParameter20.AllowNull = true;
         reportParameter20.Name = "QoskProcessDate";
         reportParameter20.Type = Telerik.Reporting.ReportParameterType.DateTime;
         reportParameter21.AllowNull = true;
         reportParameter21.Name = "ControlNumber";
         reportParameter21.Type = Telerik.Reporting.ReportParameterType.Integer;
         reportParameter22.AllowNull = true;
         reportParameter22.Name = "PackageSize";
         reportParameter22.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter23.AllowNull = true;
         reportParameter23.Name = "UnitPriceAfter";
         reportParameter23.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter24.AllowNull = true;
         reportParameter24.Name = "CreditableAmountAfter";
         reportParameter24.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter25.AllowNull = true;
         reportParameter25.Name = "RecalledProduct";
         reportParameter26.AllowNull = true;
         reportParameter26.Name = "DiscontinuedProduct";
         reportParameter27.AllowNull = true;
         reportParameter27.Name = "RXorOTC";
         reportParameter28.AllowNull = true;
         reportParameter28.Name = "DosageForm";
         reportParameter29.AllowNull = true;
         reportParameter29.Name = "PartialPercentage";
         reportParameter29.Type = Telerik.Reporting.ReportParameterType.Float;
         reportParameter30.AllowBlank = false;
         reportParameter30.Name = "UserID";
         reportParameter30.Type = Telerik.Reporting.ReportParameterType.Integer;
         reportParameter31.AllowNull = true;
         reportParameter31.Name = "StoreNumber";
         reportParameter32.AllowNull = true;
         reportParameter32.Name = "PackageForm";
         reportParameter33.AllowNull = true;
         reportParameter33.Name = "ExtendedDiscountPercent";
         reportParameter33.Type = Telerik.Reporting.ReportParameterType.Float;
         this.ReportParameters.Add(reportParameter1);
         this.ReportParameters.Add(reportParameter2);
         this.ReportParameters.Add(reportParameter3);
         this.ReportParameters.Add(reportParameter4);
         this.ReportParameters.Add(reportParameter5);
         this.ReportParameters.Add(reportParameter6);
         this.ReportParameters.Add(reportParameter7);
         this.ReportParameters.Add(reportParameter8);
         this.ReportParameters.Add(reportParameter9);
         this.ReportParameters.Add(reportParameter10);
         this.ReportParameters.Add(reportParameter11);
         this.ReportParameters.Add(reportParameter12);
         this.ReportParameters.Add(reportParameter13);
         this.ReportParameters.Add(reportParameter14);
         this.ReportParameters.Add(reportParameter15);
         this.ReportParameters.Add(reportParameter16);
         this.ReportParameters.Add(reportParameter17);
         this.ReportParameters.Add(reportParameter18);
         this.ReportParameters.Add(reportParameter19);
         this.ReportParameters.Add(reportParameter20);
         this.ReportParameters.Add(reportParameter21);
         this.ReportParameters.Add(reportParameter22);
         this.ReportParameters.Add(reportParameter23);
         this.ReportParameters.Add(reportParameter24);
         this.ReportParameters.Add(reportParameter25);
         this.ReportParameters.Add(reportParameter26);
         this.ReportParameters.Add(reportParameter27);
         this.ReportParameters.Add(reportParameter28);
         this.ReportParameters.Add(reportParameter29);
         this.ReportParameters.Add(reportParameter30);
         this.ReportParameters.Add(reportParameter31);
         this.ReportParameters.Add(reportParameter32);
         this.ReportParameters.Add(reportParameter33);
         styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
         styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
         styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
         this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] { styleRule1 });
         this.Width = Telerik.Reporting.Drawing.Unit.Inch(6D);
         ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

      }


      #endregion

      private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
      private Telerik.Reporting.DetailSection detail;
      private Telerik.Reporting.PageFooterSection pageFooterSection1;
      private Telerik.Reporting.TextBox textBox61;
      private Telerik.Reporting.Panel panel1;
      private Telerik.Reporting.TextBox txtReportName;
      private Telerik.Reporting.TextBox txtReportGeneratedTime;
   }
}