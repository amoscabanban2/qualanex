﻿using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.Report
{
    public class ReportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Report";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Report_default",
                "Report/{controller}/{action}/{ReportId}",
                new { action = "Index", ReportId = UrlParameter.Optional }
            );
        }
    }
}