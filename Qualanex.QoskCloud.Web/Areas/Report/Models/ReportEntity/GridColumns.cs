﻿namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// GridColumns: This Entity class is used for binding jqx-grid property.
    /// </summary>
    public class GridColumns
    {
       public string text { get; set; }
       public string editable { get; set; }
       public string datafield { get; set; }
       public string width { get; set; }
       public string title { get; set; }
       public string data { get; set; }
       public int? Order { get; set; }
   }

}