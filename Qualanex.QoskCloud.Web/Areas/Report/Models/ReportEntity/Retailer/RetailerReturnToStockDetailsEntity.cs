﻿using System;
namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// RetailerReturnToStockDetailsEntity: This Entity class is used to save and update RetailerReturnToStockDetails criteria.
    /// </summary>
    public class RetailerReturnToStockDetailsEntity
    {
        public long? RegionNumber { get; set; }
        public string ProductName { get; set; }
        public string RegionName { get; set; }
        public string PeriodProcessed { get; set; }
        public int? ASN { get; set; }
        public string Form { get; set; }
        public string VendorName { get; set; }
        public long? NDC { get; set; }
        public int? RetailerItemNumber { get; set; }
        public string IndateReprocessingMonthyear { get; set; }
        public long? StoreNumber { get; set; }
        public string DEANumber { get; set; }
        public Decimal? PackageSize { get; set; }
        public string Strength { get; set; }
        public string LotNumber { get; set; }
        public string ExpirationDate { get; set; }
        public double? FullQty { get; set; }
        public double? PartialQty { get; set; }
        public double? Partial { get; set; }
        public int? Labeler { get; set; }
        public string Shipment { get; set; }
        public decimal? Indatevalue { get; set; }
        public string StoreName { get; set; }
    }
    /// <summary>
    /// ReportRetailerReturnToStockDetailsEntity
    /// </summary>
    public class ReportRetailerReturnToStockDetailsEntity
    {
        public System.Collections.Generic.List<RetailerReturnToStockDetailsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
