﻿using Qualanex.QoskCloud.Web.Areas.Report.ReportClasses;
namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// NonCreditableSummaryEntity: This Entity class is used to save and update NonCreditableSummary criteria.
    /// </summary>
    public class NonCreditableSummaryEntity : ReportCriteriaBaseClass
    {
        public string StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string RegionName { get; set; }
        public string NonCreditableValue { get; set; }
        public string NonCreditableQty { get; set; }
        public string ManufacturerName { get; set; }
        public string NonCreditableReasonCode { get; set; }
        public string NonCreditableReasonDescription { get; set; }
    }
    /// <summary>
    /// ReportNonCreditableSummaryEntity
    /// </summary>
    public class ReportNonCreditableSummaryEntity
    {
        public System.Collections.Generic.List<NonCreditableSummaryEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}