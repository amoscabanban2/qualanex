﻿namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// RetailerMFGSummaryEntity: This Entity class is used to save and update RetailerMFGSummary criteria.
    /// </summary>
    public class RetailerMFGSummaryEntity
    {
        public int? MFGLabeler { get; set; }
        public string MFGNumber { get; set; }
        public long? DebitMemoInvoiceNo { get; set; }
        public string CreditableValue { get; set; }
        public string RecallCreditableValue { get; set; }
        public string MFGName { get; set; }
        public long? RetailerVendorNumber { get; set; }
        public double? CreditableQty { get; set; }
        public double? TotalProductvalue { get; set; }
        public double? TotalpriceQty { get; set; }
        public double? ProcessingFee { get; set; }
        public double? RecallCreditableQty { get; set; }
        public double? InvoiceAmount { get; set; }
    }
    /// <summary>
    /// ReportRetailerMFGSummary
    /// </summary>
    public class ReportRetailerMFGSummary
    {
        public System.Collections.Generic.List<RetailerMFGSummaryEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
