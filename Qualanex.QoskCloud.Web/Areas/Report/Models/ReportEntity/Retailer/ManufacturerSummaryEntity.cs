﻿using Qualanex.QoskCloud.Web.Areas.Report.ReportClasses;
namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// ManufacturerSummaryEntity: This Entity class is used to save and update ManufacturerSummary criteria.
    /// </summary>
    public class ManufacturerSummaryEntity : ReportCriteriaBaseClass
    {
        public string MFGLabeler { get; set; }
        public string ManufacturerName { get; set; }
        public string RetailerVendorNumber { get; set; }
        public string DebitMemoInvoiceNo { get; set; }
        public string CreditableValue { get; set; }
        public string CreditableQty { get; set; }
        public string RecallCreditableValue { get; set; }
        public string RecallCreditableQty { get; set; }
        public string TotalProductValue { get; set; }
        public string TotalQty { get; set; }
        public string ProcessingFee { get; set; }
        public string TotalInvoiceAmount { get; set; }
    }
    /// <summary>
    /// ReportManufacturerSummaryEntity
    /// </summary>
    public class ReportManufacturerSummaryEntity
    {
        public System.Collections.Generic.List<ManufacturerSummaryEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}