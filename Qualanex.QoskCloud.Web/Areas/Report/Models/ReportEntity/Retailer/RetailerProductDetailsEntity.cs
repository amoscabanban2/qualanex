﻿using Qualanex.QoskCloud.Web.Areas.Report.ReportClasses;

namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
   /// <summary>
   /// RetailerProductDetailsEntity: This Entity class is used to save and update RetailerProductDetails criteria.
   /// </summary>
   public class RetailerProductDetailsEntity : ReportCriteriaBaseClass
   {
      public string ExpDate { get; set; }
      public string LotNumber { get; set; }
      public string VendorName { get; set; }
      /// <summary>
      /// ReturnStatus - Is it Creditable [Yes,No]/Return To Stock/Aging?
      /// </summary>
      public string ReturnStatus { get; set; }
      public string StoreName { get; set; }
      public string StoreNumber { get; set; }
      public decimal? PartialQty { get; set; }
      public string Strength { get; set; }
      public string NDCNumber { get; set; }
      public string ItemGuid { get; set; }
      public string QoskProcessDate { get; set; }
      public string ProductDescription { get; set; }
      public int? ControlNumber { get; set; }
      public decimal? PackageSize { get; set; }
      public decimal? FullQty { get; set; }
      //public int? RetailerItem { get; set; }
      /// <summary>
      /// UnitPriceBefore aka Unit Price
      /// </summary>
      public decimal? UnitPriceBefore { get; set; }
      public decimal? UnitPriceAfter { get; set; }
      public decimal? CreditableAmountBeforeMFGDiscount { get; set; }
      /// <summary>
      /// CreditableAmountAfter aka Extended Price
      /// </summary>
      public decimal? CreditableAmountAfter { get; set; }
      public string OutofpolicyDescription { get; set; }
      public string RecalledProduct { get; set; }
      public string DiscontinuedProduct { get; set; }
      public string RXorOTC { get; set; }
      public string DosageForm { get; set; }
      public string PackageForm { get; set; }
      public decimal? PartialPercentage { get; set; }
      /// <summary>
      /// Extended Discount Percent - discount percent between the price of the item and its price after discounts have been applied
      /// </summary>
      public decimal? ExtendedDiscountPercent { get; set; }
   }
   /// <summary>
   /// ReportRetailerProductDetails
   /// </summary>
   public class ReportRetailerProductDetails
   {
      public System.Collections.Generic.List<ProductDetailsEmailEntity> reportRetailerBoxDetails { get; set; }
      public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
   }

}
