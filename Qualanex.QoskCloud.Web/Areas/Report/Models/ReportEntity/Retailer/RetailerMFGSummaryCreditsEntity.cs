﻿namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// RetailerMFGSummaryCreditsEntity: This Entity class is used to save and update RetailerMFGSummaryCredits criteria.
    /// </summary>
    public class RetailerMFGSummaryCreditsEntity
    {
        public int? MFGLabeler { get; set; }
        public string MFGNumber { get; set; }
        public long? DebitMemoInvoiceNo { get; set; }
        public string CreditableValue { get; set; }
        public string RecallCreditableValue { get; set; }
        public string MFGName { get; set; }
        public long? RetailerVendorName { get; set; }
        public double? CreditableQty { get; set; }
        public double? TotalProductvalue { get; set; }
        public double? TotalpriceQty { get; set; }
        public double? RecallCreditableQty { get; set; }
        public double? InvoiceAmount { get; set; }
        public double? MFGPMTCredits { get; set; }
        public string MFGPMTCreditsChecks { get; set; }
        public string Collection { get; set; }
        public int? MFGPhoneNumber { get; set; }
    }
    /// <summary>
    /// ReportRetailerMFGSummaryCredit
    /// </summary>
    public class ReportRetailerMFGSummaryCredit
    {
        public System.Collections.Generic.List<RetailerMFGSummaryCreditsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
