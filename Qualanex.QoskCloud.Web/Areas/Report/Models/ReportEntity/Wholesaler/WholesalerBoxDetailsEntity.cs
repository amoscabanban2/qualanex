﻿using System;
namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
    /// <summary>
    /// WholesalerBoxDetailsEntity: This Entity class is used to save and update WholesalerBoxDetails criteria.
    /// </summary>
    public class WholesalerBoxDetailsEntity
    {
        public long? DivisionNumber { get; set; }
        public string DivisionName { get; set; }
        public string RegionName { get; set; }
        public int? BoxNumber { get; set; }
        public int? ASN { get; set; }
        public string ProcessDate { get; set; }
        public string DisposalDate { get; set; }
        public int? VendorNumber { get; set; }
        public string VendorName { get; set; }
        public long? NDC { get; set; }
        public int? WholesalerItem { get; set; }
        public string NDCName { get; set; }
        public string DosageForm { get; set; }
        public int? DEAClass { get; set; }
        public Decimal? PackageSize { get; set; }
        public string Strength { get; set; }
        public string LotNumbersID { get; set; }
        public string ExpirationDate { get; set; }
        public double? FullQty { get; set; }
        public double? PartialQty { get; set; }
        public double? Partial { get; set; }
        public decimal? UnitPriceBefore { get; set; }
        public decimal? UnitPriceAfter { get; set; }
        public decimal? CreditableAmountBefore { get; set; }
        public decimal? CreditableAmountAfter { get; set; }
        public decimal? IndateAmount { get; set; }
        public decimal? OverrideAmount { get; set; }
        public decimal? WasteAmount { get; set; }
        public int? WasteReasonCode { get; set; }
        public string WasteReasonCodeDescription { get; set; }
    }
    /// <summary>
    /// ReportWholesalerBoxDetails
    /// </summary>
    public class ReportWholesalerBoxDetails
    {
        public System.Collections.Generic.List<WholesalerBoxDetailsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
