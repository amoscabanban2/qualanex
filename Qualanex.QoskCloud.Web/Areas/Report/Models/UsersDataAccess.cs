﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;
namespace Qualanex.QoskCloud.Web.Areas.Report
{
    internal class UsersDataAccess
    {
        /// <summary>
        /// ValidateUserLogin: Validate User Login
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        internal UserRegistrationRequest ValidateUserLogin(string userName)
        {
            UserRegistrationRequest userreturnData = null;
            try
            {
                using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
                {

                    userreturnData = (from user in cloudModelEntities.User
                                      join profile in cloudModelEntities.Profile
                                      on user.ProfileCode equals profile.ProfileCode
                                      where user.UserName.Equals(userName.Trim())
                                      select new UserRegistrationRequest
                                      {
                                          UserID = user.UserID,
                                          UserName = user.UserName,
                                          Email = user.Email,
                                          FirstName = user.FirstName,
                                          LastName = user.LastName,
                                          ProfileCode = user.ProfileCode,
                                          RollupProfileCode = profile.RollupProfileCode,
                                          IsTemporaryPassword = user.IsTemporaryPassword ?? false
                                      }).SingleOrDefault();


                    return userreturnData;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return userreturnData;
            }

        }


        /// <summary>
        /// GetLeftLinksData: Get the menus.
        /// </summary>
        /// <param name="parentMenuID"></param>
        /// <returns></returns>
        internal ICollection<LeftMenuLinks> GetLeftLinksData(int parentMenuID = 1)
        {

            ICollection<LeftMenuLinks> sublinks = null;
            try
            {
                using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
                {

                    sublinks = (from menuLinks in cloudModelEntities.ReportMenu
                                where menuLinks.ParentMenuID == parentMenuID && (menuLinks.IsActive ?? false)
                                select new LeftMenuLinks
                                {
                                    PageName = menuLinks.PageName,
                                    PageURL = menuLinks.PageURL,
                                    PageURLClass = menuLinks.PageURLClass,
                                    Application = menuLinks.Application,
                                    ParentMenuID = menuLinks.ParentMenuID ?? 0
                                }).ToList();


                    return sublinks;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return sublinks;
            }

        }


    }
}