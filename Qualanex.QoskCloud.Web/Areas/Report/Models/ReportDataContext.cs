﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.ReportClasses;

namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ReportDataContext : Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<RetailerBoxDetailsEntity> GetRetailer_Box_Details(long NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Below code is commented because this report is no longer being used in the reporting application.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //    var result = (from lotnumber in cloudModelEntities.Lot
         //                  join products in cloudModelEntities.Product
         //                  on lotnumber.ProductID equals products.ProductID
         //                  join Dosages in cloudModelEntities.Dosage
         //                  on products.DosageID equals Dosages.DosageID
         //                  where products.NDC == NDCNumber && (DataStartDate == null || DataEndDate == null || ((products.CreatedDate >= DataStartDate && products.CreatedDate <= DataEndDate)
         //                                                     || (products.ModifiedDate >= DataStartDate && products.ModifiedDate <= DataEndDate)))

         //                  select new RetailerBoxDetailsEntity
         //                  {
         //                      NDC = products.NDC,
         //                      DosageForm = Dosages.DosageCode,
         //                      Strength = products.Strength,
         //                      LotNumbersID = lotnumber.LotNumber,
         //                      ExpirationDate = lotnumber.ExpirationDate != null ? lotnumber.ExpirationDate.ToString() : string.Empty,
         //                      PackageSize = products.PackageSize

         //                  }).ToList();
         //    if (result != null)
         //    {
         //        foreach (RetailerBoxDetailsEntity item in result)
         //        {
         //            if (!string.IsNullOrEmpty(item.ExpirationDate) && !string.IsNullOrEmpty(timezone))
         //            {
         //                DateTime? Converteddate = SchedulerFunctions.ConvertUTCToTimeZoneDate(timezone, Convert.ToDateTime(item.ExpirationDate));
         //                if (Converteddate != null)
         //                {
         //                    item.ExpirationDate = Converteddate.Value.ToString("MM/dd/yyyy hh:mm:ss tt");
         //                }

         //            }
         //        }
         //    }
         //    return result;

         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<RetailerBoxSummaryEntity> GetRetailer_Box_Summary(long BoxNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Code is commented. Reason: WholesalerNumber is not present in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //        var result = (from wholesalerNumber in cloudModelEntities.WholesalerNumber
         //                      join items in cloudModelEntities.Items
         //                      on wholesalerNumber.ProductID equals items.ProductID
         //                      where items.ItemID == BoxNumber
         //                      select new RetailerBoxSummaryEntity
         //                      {
         //                          WholesalerNumber = wholesalerNumber.WholesalerNumberID,
         //                          BoxNumber = items.ItemID
         //                      }).ToList();
         //        return result;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<RetailerMFGSummaryEntity> GetRetailer_MFG_Summary(long BoxNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Code is commented. Reason: MFGLabelerCode is not present in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from products in cloudModelEntities.Products
         //                      where products.MFGLabelerCode == BoxNumber
         //                      select new RetailerMFGSummaryEntity
         //                      {
         //                          MFGLabeler = products.MFGLabelerCode,
         //                          MFGNumber = products.MFGProductNumber
         //                      }).Take(500).ToList();
         //        return result;


         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<RetailerMFGSummaryCreditsEntity> GetRetailer_MFG_Credits_Summary(long BoxNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Code is commented. Reason: MFGLabelerCode is not present in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from products in cloudModelEntities.Products
         //                      where products.MFGLabelerCode == BoxNumber
         //                      select new RetailerMFGSummaryCreditsEntity
         //                      {
         //                          MFGLabeler = products.MFGLabelerCode,
         //                          MFGNumber = products.MFGProductNumber
         //                      }).Take(500).ToList();
         //        return result;
         //}

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<RetailerReturnToStockDetailsEntity> GetRetailer_Return_Stock_Details(long NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Code is commented. Reason: MFGLabelerCode is not present in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from lotnumber in cloudModelEntities.Lots
         //                      join products in cloudModelEntities.Products
         //                      on lotnumber.ProductID equals products.ProductID
         //                      join profiles in cloudModelEntities.Profiles
         //                      on products.ProfileCode equals profiles.ProfileCode
         //                      join dosages in cloudModelEntities.Dosages
         //                      on products.DosageID equals dosages.DosageID
         //                      where products.NDC == NDCNumber
         //                      select new RetailerReturnToStockDetailsEntity
         //                      {
         //                          ExpirationDate = lotnumber.ExpirationDate != null ? lotnumber.ExpirationDate.ToString() : string.Empty,
         //                          NDC = products.NDC,
         //                          PackageSize = products.PackageSize,
         //                          LotNumber = lotnumber.LotNumber,
         //                          Strength = products.Strength,
         //                          DEANumber = profiles.DEANumber,
         //                          Form = dosages.Description,
         //                          Labeler = products.MFGLabelerCode
         //                      }).ToList();
         //        if (result != null)
         //        {
         //            foreach (RetailerReturnToStockDetailsEntity item in result)
         //            {
         //                if (!string.IsNullOrEmpty(item.ExpirationDate) && !string.IsNullOrEmpty(timezone))
         //                {
         //                    DateTime? Converteddate = SchedulerFunctions.ConvertUTCToTimeZoneDate(timezone, Convert.ToDateTime(item.ExpirationDate));
         //                    if (Converteddate != null)
         //                    {
         //                        item.ExpirationDate = Converteddate.Value.ToString("MM/dd/yyyy hh:mm:ss tt");
         //                    }

         //                }
         //            }

         //        }
         //        return result;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WholesalerBoxDetailsEntity> GetWholesaler_Box_Details(long NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         //Below code is commented because this report is no longer being used in the reporting application.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //    var result = (from lotnumber in cloudModelEntities.Lot
         //                  join products in cloudModelEntities.Product
         //                  on lotnumber.ProductID equals products.ProductID
         //                  join Dosages in cloudModelEntities.Dosage
         //                  on products.DosageID equals Dosages.DosageID
         //                  where products.NDC == NDCNumber && (DataStartDate == null || DataEndDate == null || ((products.CreatedDate >= DataStartDate && products.CreatedDate <= DataEndDate)
         //                                                     || (products.ModifiedDate >= DataStartDate && products.ModifiedDate <= DataEndDate)))

         //                  select new WholesalerBoxDetailsEntity
         //                  {
         //                      NDC = products.NDC,
         //                      PackageSize = products.PackageSize,
         //                      Strength = products.Strength,
         //                      LotNumbersID = lotnumber.LotNumber,
         //                      ExpirationDate = lotnumber.ExpirationDate != null ? lotnumber.ExpirationDate.ToString() : string.Empty,
         //                      DosageForm = Dosages.DosageCode
         //                  }).ToList();
         //    if (result != null)
         //    {
         //        foreach (WholesalerBoxDetailsEntity item in result)
         //        {
         //            if (!string.IsNullOrEmpty(item.ExpirationDate) && !string.IsNullOrEmpty(timezone))
         //            {
         //                DateTime? Converteddate = SchedulerFunctions.ConvertUTCToTimeZoneDate(timezone, Convert.ToDateTime(item.ExpirationDate));
         //                if (Converteddate != null)
         //                {
         //                    item.ExpirationDate = Converteddate.Value.ToString("MM/dd/yyyy hh:mm:ss tt");
         //                }

         //            }
         //        }
         //    }
         //    return result;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WholesalerBoxSummaryEntity> GetWholesaler_Box_Summary(long NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         // Code is commented.Reason: MFGLabelerCode is not present in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from wholesalerNumber in cloudModelEntities.WholesalerNumber
         //                      join items in cloudModelEntities.Items
         //                      on wholesalerNumber.ProductID equals items.ProductID
         //                      where items.ItemID == NDCNumber
         //                      select new WholesalerBoxSummaryEntity
         //                      {
         //                          BoxNumber = items.ItemID
         //                      }).ToList();
         //        return result;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WholesalerMFGSummaryCreditsEntity> GetWholesaler_MFG_Credits_Summary(long BoxNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {

         // Code is commented.Reason:  MFGLabelerCode Column is not present Product Table in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from products in cloudModelEntities.Products
         //                      where products.MFGLabelerCode == BoxNumber
         //                      select new WholesalerMFGSummaryCreditsEntity
         //                      {
         //                          MFGLabeler = products.MFGLabelerCode,
         //                          MFGNumber = products.MFGProductNumber
         //                      }).Take(500).ToList();
         //        return result;


         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WholesalerMFGSummaryEntity> GetWholesaler_MFG_Summary(long BoxNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         // Code is commented.Reason:  MFGLabelerCode Column is not present Product Table in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from products in cloudModelEntities.Products
         //                      where products.MFGLabelerCode == BoxNumber
         //                      select new WholesalerMFGSummaryEntity
         //                      {
         //                          MFGLabeler = products.MFGLabelerCode,
         //                          MFGNumber = products.MFGProductNumber
         //                      }).Take(500).ToList();
         //        return result;
         //}

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="BoxNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WholesalerReturnToStockDetailsEntity> GetWholesaler_Return_Stock_Details(long NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string timezone = "")
      {
         // Code is commented.Reason:  MFGLabelerCode Column is not present Product Table in dvqoskdb2 database.

         //using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         //{

         //        var result = (from lotnumber in cloudModelEntities.Lots
         //                      join products in cloudModelEntities.Products
         //                      on lotnumber.ProductID equals products.ProductID
         //                      join profiles in cloudModelEntities.Profiles
         //                      on products.ProfileCode equals profiles.ProfileCode
         //                      join dosages in cloudModelEntities.Dosages
         //                      on products.DosageID equals dosages.DosageID
         //                      where products.NDC == NDCNumber
         //                      select new WholesalerReturnToStockDetailsEntity
         //                      {
         //                          ExpirationDate = lotnumber.ExpirationDate != null ? lotnumber.ExpirationDate.ToString() : string.Empty,
         //                          NDC = products.NDC,
         //                          PackageSize = products.PackageSize,
         //                          LotNumber = lotnumber.LotNumber,
         //                          Strength = products.Strength,
         //                          DEANumber = profiles.DEANumber,
         //                          Form = dosages.Description,
         //                          Labeler = products.MFGLabelerCode
         //                      }).ToList();
         //        if (result != null)
         //        {
         //            foreach (WholesalerReturnToStockDetailsEntity item in result)
         //            {
         //                if (!string.IsNullOrEmpty(item.ExpirationDate) && !string.IsNullOrEmpty(timezone))
         //                {
         //                    DateTime? Converteddate = SchedulerFunctions.ConvertUTCToTimeZoneDate(timezone, Convert.ToDateTime(item.ExpirationDate));
         //                    if (Converteddate != null)
         //                    {
         //                        item.ExpirationDate = Converteddate.Value.ToString("MM/dd/yyyy hh:mm:ss tt");
         //                    }

         //                }
         //            }

         //        }
         //        return result;

         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Product Details report.
      /// </summary>
      /// <param name="objRetailerProductDetail"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ProductDetailsEmailEntity> GetRetailer_Product_Details(ProductDetailsEmailEntity objRetailerProductDetailsEntity, string FileType, long UserId)
      {
         DateTime? expDate = null;
         DateTime? qoskProcessDate = null;

         if (!string.IsNullOrEmpty(objRetailerProductDetailsEntity.ExpDate))
         {
            expDate = Convert.ToDateTime(objRetailerProductDetailsEntity.ExpDate);
         }

         if (!string.IsNullOrEmpty(objRetailerProductDetailsEntity.QoskProcessDate))
         {
            qoskProcessDate = Convert.ToDateTime(objRetailerProductDetailsEntity.QoskProcessDate);
         }

         return GetRetailer_Product_Details(objRetailerProductDetailsEntity.NDCNumber, objRetailerProductDetailsEntity.DataStartDate,
            objRetailerProductDetailsEntity.DataEndDate, objRetailerProductDetailsEntity.ReportDataTimeZone, FileType, objRetailerProductDetailsEntity.StoreName, objRetailerProductDetailsEntity.VendorName,
            objRetailerProductDetailsEntity.ProductDescription, objRetailerProductDetailsEntity.LotNumber, expDate,
            objRetailerProductDetailsEntity.FullQty, objRetailerProductDetailsEntity.PartialQty, objRetailerProductDetailsEntity.UnitPriceBefore,
            objRetailerProductDetailsEntity.CreditableAmountBeforeMFGDiscount,
            objRetailerProductDetailsEntity.ReturnStatus, objRetailerProductDetailsEntity.OutofpolicyDescription, objRetailerProductDetailsEntity.Strength,
            objRetailerProductDetailsEntity.ItemGuid, qoskProcessDate, objRetailerProductDetailsEntity.ControlNumber,
            objRetailerProductDetailsEntity.PackageSize, objRetailerProductDetailsEntity.UnitPriceAfter, objRetailerProductDetailsEntity.CreditableAmountAfter,
            objRetailerProductDetailsEntity.RecalledProduct, objRetailerProductDetailsEntity.DiscontinuedProduct, objRetailerProductDetailsEntity.RXorOTC,
            objRetailerProductDetailsEntity.DosageForm, objRetailerProductDetailsEntity.PackageForm, objRetailerProductDetailsEntity.PartialPercentage, objRetailerProductDetailsEntity.StoreNumber,
            objRetailerProductDetailsEntity.ExtendedDiscountPercent, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Product Details report.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="StoreName"></param>
      /// <param name="VendorName"></param>
      /// <param name="ProductDescription"></param>
      /// <param name="LotNumber"></param>
      /// <param name="ExpDate"></param>
      /// <param name="FullQty"></param>
      /// <param name="PartialQty"></param>
      /// <param name="UnitPriceBefore"></param>
      /// <param name="CreditableAmountBeforeMFGDiscount"></param>
      /// <param name="ReturnStatus"></param>
      /// <param name="OutofpolicyDescription"></param>
      /// <param name="Strength"></param>
      /// <param name="ItemGuid"></param>
      /// <param name="QoskProcessDate"></param>
      /// <param name="ControlNumber"></param>
      /// <param name="PackageSize"></param>
      /// <param name="UnitPriceAfter"></param>
      /// <param name="CreditableAmountAfter"></param>
      /// <param name="RecalledProduct"></param>
      /// <param name="DiscontinuedProduct"></param>
      /// <param name="RXorOTC"></param>
      /// <param name="DosageForm"></param>
      /// <param name="PackageForm"></param>
      /// <param name="PartialPercentage"></param>
      /// <param name="StoreNumber"></param>
      /// <param name="ExtendedDiscountPercent"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ProductDetailsEmailEntity> GetRetailer_Product_Details(string NDCNumber, DateTime? DataStartDate, DateTime? DataEndDate, string Timezone,
         string FileType, string StoreName, string VendorName, string ProductDescription, string LotNumber, DateTime? ExpDate, decimal? FullQty, decimal? PartialQty,
         decimal? UnitPriceBefore, decimal? CreditableAmountBeforeMFGDiscount, string ReturnStatus, string OutofpolicyDescription, string Strength, string ItemGuid,
         DateTime? QoskProcessDate, int? ControlNumber, decimal? PackageSize, decimal? UnitPriceAfter, decimal? CreditableAmountAfter, string RecalledProduct,
         string DiscontinuedProduct, string RXorOTC, string DosageForm, string PackageForm, decimal? PartialPercentage, string StoreNumber, decimal? ExtendedDiscountPercent, long UserID)
      {
         DateTime processDate = QoskProcessDate == null ? DateTime.MinValue : QoskProcessDate.Value.Date;
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var productId = !string.IsNullOrWhiteSpace(NDCNumber) ? ParseNdcUpc(NDCNumber) : null;
            var userProfileCodeList = GetValidUserProfileCodes(UserID);
            var tempStartDate = DataStartDate;
            var tempEndDate = DataEndDate;

            if (DataStartDate != null && DataEndDate != null && DataStartDate > DataEndDate)
            {
               tempStartDate = DataEndDate;
               tempEndDate = DataStartDate;
            }

            //account for the entire day of the end date
            tempEndDate = tempEndDate?.AddDays(1);

            var query = (from product in cloudModelEntities.Product
                         join item in cloudModelEntities.Item on product.ProductID equals item.ProductID
                         join dosageDict in cloudModelEntities.DosageDict on product.DosageCode equals dosageDict.Code into dosageDicts
                         from dosageDict in dosageDicts.DefaultIfEmpty()
                         join packageDict in cloudModelEntities.PackageDict on product.PackageCode equals packageDict.Code into packageDicts
                         from packageDict in packageDicts.DefaultIfEmpty()
                         join profile in cloudModelEntities.Profile on product.ProfileCode equals profile.ProfileCode into profiles
                         from profile in profiles.DefaultIfEmpty()
                         join qoskprofile in cloudModelEntities.Profile on item.UserProfileCode equals qoskprofile.ProfileCode into qoskprofiles
                         from qoskprofile in qoskprofiles.DefaultIfEmpty()
                         join itemStateDict in cloudModelEntities.ItemStateDict on item.ItemStateCode equals itemStateDict.Code
                         join lot in cloudModelEntities.Lot on item.LotNumber equals lot.LotNumber into lots
                         from lot in lots.DefaultIfEmpty()
                         where
                            userProfileCodeList.Contains(item.UserProfileCode)
                            && (productId == null || product.ProductID == productId)
                            && (string.IsNullOrEmpty(ItemGuid) || item.ItemGUID.ToString().Contains(ItemGuid))
                            && (FullQty == null || (itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S && item.Quantity == FullQty) || (itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C && (item.Quantity * item.CaseSize) == FullQty))
                            && (PartialQty == null || (itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O && item.Quantity == PartialQty))
                            && (UnitPriceBefore == null || item.ItemUnitPrice == UnitPriceBefore)
                            && (UnitPriceAfter == null || item.ItemUnitPrice == UnitPriceAfter)
                            && (CreditableAmountAfter == null || item.ReturnableItemLowestPriceValue == CreditableAmountAfter)
                            && (string.IsNullOrEmpty(Strength) || product.Strength.ToString().Contains(Strength.ToString()))
                            && (processDate == DateTime.MinValue || item.CreatedDate == null || (System.Data.Entity.DbFunctions.TruncateTime(item.CreatedDate) == QoskProcessDate))
                            && (ControlNumber == null || product.ControlNumber.ToString().Contains(ControlNumber.ToString()))
                            && (PackageSize == null || product.PackageSize.ToString().Contains(PackageSize.ToString()))
                            && (string.IsNullOrEmpty(RecalledProduct) || (lot.RecallID != null ? Utility.Constants.ReportDataContext_Yes : Utility.Constants.ReportDataContext_No).Contains(RecalledProduct))
                            && (string.IsNullOrEmpty(DiscontinuedProduct) || ((product.Withdrawn ?? false) ? Utility.Constants.ReportDataContext_Yes : Utility.Constants.ReportDataContext_No).Contains(DiscontinuedProduct))
                            && (string.IsNullOrEmpty(RXorOTC) || (product.RXorOTC == false ? Utility.Constants.ReportDataContext_OTC : Utility.Constants.ReportDataContext_RX).Contains(RXorOTC))
                            && (string.IsNullOrEmpty(DosageForm) || dosageDict.Description.Contains(DosageForm))
                            && (string.IsNullOrEmpty(PackageForm) || packageDict.Description.Contains(PackageForm))
                            && (string.IsNullOrEmpty(StoreNumber) || qoskprofile.StoreNumber == StoreNumber)
                            && (string.IsNullOrEmpty(VendorName) || profile.Name.Contains(VendorName))
                            && (string.IsNullOrEmpty(StoreName) || qoskprofile.Name.Contains(StoreName))
                            && (string.IsNullOrEmpty(ProductDescription) || product.Description.Contains(ProductDescription))
                            && (string.IsNullOrEmpty(LotNumber) || item.LotNumber.Contains(LotNumber))
                            && (CreditableAmountBeforeMFGDiscount == null || item.ReturnableItemLowestPriceValueNoFees == CreditableAmountBeforeMFGDiscount)
                            && (ExpDate == null || item.ExpirationDate == ExpDate)
                            && (string.IsNullOrEmpty(OutofpolicyDescription) || item.NonReturnableReasonDetail.Contains(OutofpolicyDescription))
                            && (tempStartDate != null && tempEndDate == null //Start Date Only
                               ? item.CreatedDate >= tempStartDate
                               : tempStartDate == null && tempEndDate != null //End Date Only
                                  ? item.CreatedDate <= tempEndDate
                                  : tempStartDate != null && tempEndDate != null //Both Start Date and End Date
                                     ? tempStartDate == tempEndDate
                                       ? System.Data.Entity.DbFunctions.TruncateTime(item.CreatedDate) == tempStartDate //Start and End Dates are the same
                                       : item.CreatedDate >= tempStartDate && item.CreatedDate <= tempEndDate //Date Range
                                     : tempStartDate == null && tempEndDate == null) //Ignore Item Process Date

                         select new ProductDetailsEmailEntity
                         {
                            NDCNumber = product.NDCUPCWithDashes,
                            DosageForm = dosageDict.Description,
                            PackageForm = packageDict.Description,
                            Strength = product.Strength,
                            LotNumber = item.LotNumber.ToUpper(),
                            QoskProcessDate = item.CreatedDate != null ? item.CreatedDate.ToString() : string.Empty,
                            ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString() : string.Empty,
                            PackageSize = product.PackageSize,
                            RXorOTC = !product.RXorOTC ? Utility.Constants.ReportDataContext_OTC : Utility.Constants.ReportDataContext_RX,
                            ControlNumber = product.ControlNumber,
                            ItemGuid = item.ItemGUID.ToString(),
                            VendorName = profile.Name,
                            StoreName = qoskprofile.Name,
                            StoreNumber = qoskprofile.StoreNumber,
                            ProductDescription = product.Description,
                            CreditableAmountBeforeMFGDiscount = item.ReturnableItemLowestPriceValueNoFees,
                            CreditableAmountAfter = item.ReturnableItemLowestPriceValue,
                            UnitPriceBefore = item.ItemUnitPriceNoFees,
                            UnitPriceAfter = item.ItemUnitPrice,
                            ExtendedDiscountPercent = item.ReturnableItemLowestPriceValue != null && item.ReturnableItemLowestPriceValueNoFees != null && item.ReturnableItemLowestPriceValueNoFees > 0
                               ? (100 - ((item.ReturnableItemLowestPriceValue / item.ReturnableItemLowestPriceValueNoFees) * 100))
                               : null,
                            FullQty = itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S ? item.Quantity : itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C ? (item.Quantity * item.CaseSize) : (decimal?)null,
                            PartialPercentage = itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O && product.PackageSize != 0 ? ((item.Quantity / product.PackageSize) * 100) : (decimal?)null,
                            PartialQty = itemStateDict.Code == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O ? item.Quantity : (decimal?)null,
                            ReturnStatus =
                               item.Disposition == Utility.Constants.ReportDataContext_Aging || item.Disposition == Utility.Constants.ReportDataContext_Stock
                                 ? item.Disposition
                                 : item.Returnable == true
                                    ? "Creditable"
                                    : "Non-Creditable",
                            OutofpolicyDescription = item.NonReturnableReasonDetail,
                            RecalledProduct = lot.RecallID != null ? Utility.Constants.ReportDataContext_Yes : Utility.Constants.ReportDataContext_No,
                            DiscontinuedProduct = (product.Withdrawn ?? false) ? Utility.Constants.ReportDataContext_Yes : Utility.Constants.ReportDataContext_No,
                            City = profile.City,
                            DEANumber = profile.DEANumber,
                            State = profile.State,
                            ContainerTypeName = item.ContainerTypeName,
                            SealedOpenCase = itemStateDict.Description,
                            ZipCode = profile.ZipCode
                         });

            query = query.Where(rtrnStatus => string.IsNullOrEmpty(ReturnStatus) || rtrnStatus.ReturnStatus.Contains(ReturnStatus));

            var result = query.Distinct().ToList();

            if (result.Any())
            {
               foreach (ProductDetailsEmailEntity item in result)
               {
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }

                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? convertedProcessDate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Convert.ToDateTime(item.QoskProcessDate));
                     if (convertedProcessDate != null)
                     {
                        item.QoskProcessDate = convertedProcessDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }

                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? expdate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = expdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }

                  if (item.FullQty != null)
                  {
                     item.FullQty = Convert.ToDecimal(RemoveTrailingZero(item.FullQty));
                  }

                  if (item.UnitPriceBefore != null)
                  {
                     item.UnitPriceBefore = Math.Round(Convert.ToDecimal(item.UnitPriceBefore), 2);
                  }

                  if (item.UnitPriceAfter != null)
                  {
                     item.UnitPriceAfter = Math.Round(Convert.ToDecimal(item.UnitPriceAfter), 2);
                  }

                  if (item.CreditableAmountBeforeMFGDiscount != null)
                  {
                     item.CreditableAmountBeforeMFGDiscount = Math.Round(Convert.ToDecimal(item.CreditableAmountBeforeMFGDiscount), 2);
                  }

                  if (item.CreditableAmountAfter != null)
                  {
                     item.CreditableAmountAfter = Math.Round(Convert.ToDecimal(item.CreditableAmountAfter), 2);
                  }

                  if (item.ExtendedDiscountPercent != null)
                  {
                     item.ExtendedDiscountPercent = Math.Round(Convert.ToDecimal(item.ExtendedDiscountPercent), 2);
                  }

                  if (item.PartialPercentage != null)
                  {
                     item.PartialPercentage = Math.Round(Convert.ToDecimal(item.PartialPercentage), 2);
                  }

                  if (item.PartialQty != null)
                  {
                     item.PartialQty = Convert.ToDecimal(RemoveTrailingZero(item.PartialQty));
                  }

                  if (item.PackageSize != null)
                  {
                     item.PackageSize = Convert.ToDecimal(RemoveTrailingZero(item.PackageSize));
                  }
               }
            }

            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Waste report.
      /// </summary>
      /// <param name="objWasteReportEntity"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WasteReportEmailEntity> Get_Waste_Report_Details(WasteReportEmailEntity objWasteReportEntity, string FileType, long UserId)
      {
         DateTime? expDate = null;
         if (!string.IsNullOrEmpty(objWasteReportEntity.ExpDate))
         {
            expDate = Convert.ToDateTime(objWasteReportEntity.ExpDate);
         }
         return Get_Waste_Report_Details(objWasteReportEntity.DataStartDate, objWasteReportEntity.DataEndDate,
            objWasteReportEntity.ReportDataTimeZone, FileType, objWasteReportEntity.NDCNumber, objWasteReportEntity.ProductDescription, objWasteReportEntity.Strength,
            objWasteReportEntity.FullQty, objWasteReportEntity.LotNumber, objWasteReportEntity.SealedOpenCase, objWasteReportEntity.ItemGuid,
            objWasteReportEntity.WasteCode, objWasteReportEntity.WasteStreamProfile, objWasteReportEntity.PartialQty, objWasteReportEntity.StoreName,
            objWasteReportEntity.StoreNumber, expDate, objWasteReportEntity.PackageSize, objWasteReportEntity.ControlNumber, objWasteReportEntity.DosageForm, objWasteReportEntity.PackageForm, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Waste report.
      /// </summary>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="NDCNumber"></param>
      /// <param name="ProductDescription"></param>
      /// <param name="Strength"></param>
      /// <param name="FullQty"></param>
      /// <param name="LotNumber"></param>
      /// <param name="SealedOpenCase"></param>
      /// <param name="ItemGuid"></param>
      /// <param name="WasteCode"></param>
      /// <param name="WasteStreamProfile"></param>
      /// <param name="PartialQty"></param>
      /// <param name="StoreName"></param>
      /// <param name="StoreNumber"></param>
      /// <param name="ExpDate"></param>
      /// <param name="PackageSize"></param>
      /// <param name="ControlNumber"></param>
      /// <param name="DosageForm"></param>
      /// <param name="PackageForm"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<WasteReportEmailEntity> Get_Waste_Report_Details(DateTime? DataStartDate, DateTime? DataEndDate, string Timezone, string FileType, string NDCNumber,
         string ProductDescription, string Strength, string FullQty, string LotNumber, string SealedOpenCase, string ItemGuid, string WasteCode, string WasteStreamProfile, string PartialQty,
         string StoreName, string StoreNumber, DateTime? ExpDate, decimal? PackageSize, int? ControlNumber, string DosageForm, string PackageForm, long UserID)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profileUsers = GetValidProfileUsers(UserID);

            var query = (from item in cloudModelEntities.Item
                         join combinedResult in

                            (from item in cloudModelEntities.Item
                             join qosk in cloudModelEntities.Qosk
                                on item.QoskID equals qosk.QoskID
                             join qoskprofile in cloudModelEntities.Profile
                                on qosk.ProfileCode equals qoskprofile.ProfileCode

                             join statedict in cloudModelEntities.StateDict
                                on qoskprofile.State equals statedict.Code

                             join productWasteCode in cloudModelEntities.ProductWasteCode on
                                item.ProductID equals productWasteCode.ProductID into productWasteCodes
                             from productWasteCode in productWasteCodes.DefaultIfEmpty()

                             join wasteCodeDict in cloudModelEntities.WasteCodeDict on
                                productWasteCode.WasteCode equals wasteCodeDict.Code into wasteCodes
                             from wasteCodeDict in wasteCodes.DefaultIfEmpty()

                             where (string.IsNullOrEmpty(qoskprofile.State) || productWasteCode.StateCode == statedict.Code)
                             select new { itemid = item.ItemGUID, WasteCodeDescription = wasteCodeDict.Code + " " + wasteCodeDict.Description }
                               )

                            on item.ItemGUID equals combinedResult.itemid into groupData
                         from groupDataDetail in groupData.DefaultIfEmpty()

                         join product in cloudModelEntities.Product
                            on item.ProductID equals product.ProductID into products
                         from product in products.DefaultIfEmpty()
                         join profile in cloudModelEntities.Profile
                            on product.ProfileCode equals profile.ProfileCode into profiles
                         from profile in profiles.DefaultIfEmpty()

                         join qosk in cloudModelEntities.Qosk
                            on item.QoskID equals qosk.QoskID into qosks
                         from qosk in qosks.DefaultIfEmpty()

                         join qoskprofile in cloudModelEntities.Profile
                            on qosk.ProfileCode equals qoskprofile.ProfileCode into qoskprofiles
                         from qoskprofile in qoskprofiles.DefaultIfEmpty()

                         join dosageDict in cloudModelEntities.DosageDict
                            on product.DosageCode equals dosageDict.Code into dosageDicts
                         from dosageDict in dosageDicts.DefaultIfEmpty()

                         join packageDict in cloudModelEntities.PackageDict
                            on product.PackageCode equals packageDict.Code into packageDicts
                         from packageDict in packageDicts.DefaultIfEmpty()

                         join productWasteStreamProfile in cloudModelEntities.ProductWasteStreamProfile
                            on item.ProductID equals productWasteStreamProfile.ProductID into productWasteStreamProfiles
                         from productWasteStreamProfile in productWasteStreamProfiles.DefaultIfEmpty()

                            //join wasteStreamProfile in cloudModelEntities.WasteStreamProfile
                            //   on productWasteStreamProfile.WasteStreamProfileID equals wasteStreamProfile.WasteStreamProfileID into wasteStreamProfiles
                            //from wasteStreamProfile in wasteStreamProfiles.DefaultIfEmpty()

                         where
                            (profileUsers.Contains(item.CreatedBy))
                            && (string.IsNullOrEmpty(NDCNumber) || (!string.IsNullOrEmpty(product.NDCUPCWithDashes) && product.NDCUPCWithDashes.Replace("-", "").Contains(NDCNumber)))

                            && (string.IsNullOrEmpty(ItemGuid) || item.ItemGUID.ToString().Contains(ItemGuid))
                            && (string.IsNullOrEmpty(FullQty) || (item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S && item.Quantity.ToString().Contains(FullQty)) || (item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C && (item.Quantity * item.CaseSize).ToString().Contains(FullQty)))
                            //&& (string.IsNullOrEmpty(WasteStreamProfile) || (wasteStreamProfile.ApprovalCode + " " + wasteStreamProfile.Description).Contains(WasteStreamProfile))

                            && (ControlNumber == null || product.ControlNumber.ToString().Contains(ControlNumber.ToString()))
                            && (string.IsNullOrEmpty(StoreName) || qoskprofile.Name.Contains(StoreName))
                            && (string.IsNullOrEmpty(PartialQty) || (item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O && item.Quantity.ToString().Contains(PartialQty)))
                            && (string.IsNullOrEmpty(DosageForm) || dosageDict.Description.Contains(DosageForm))
                            && (string.IsNullOrEmpty(PackageForm) || packageDict.Description.Contains(PackageForm))
                            && (string.IsNullOrEmpty(StoreNumber) || qoskprofile.StoreNumber == StoreNumber)
                            && (ExpDate == null || item.ExpirationDate == ExpDate)

                            && (string.IsNullOrEmpty(WasteCode) || (!string.IsNullOrEmpty(groupDataDetail.WasteCodeDescription) && groupDataDetail.WasteCodeDescription.Contains(WasteCode)))

                            && (string.IsNullOrEmpty(ProductDescription) || product.Description.Contains(ProductDescription))
                            && (string.IsNullOrEmpty(Strength) || product.Strength.Contains(Strength))
                            && (string.IsNullOrEmpty(LotNumber) || item.LotNumber.Contains(LotNumber))
                            && (string.IsNullOrEmpty(SealedOpenCase) || string.IsNullOrEmpty(item.ItemStateCode) || item.ItemStateCode.Contains(SealedOpenCase))
                            //&& (DataStartDate == null || DataEndDate == null || item.CreatedDate >= DataStartDate && item.CreatedDate <= DataEndDate)
                            //&& (qosk.ProfileCode < 900000 || qosk.ProfileCode > 999999)
                            && (productWasteStreamProfile.Priority == 1 || productWasteStreamProfile.Priority.ToString() == "")
                         select new
                         {
                            item.ItemGUID,
                            item.ProductID,
                            item.LotNumber,
                            //wasteStreamProfile.ApprovalCode,
                            //wasteStreamProfileDescription = wasteStreamProfile.Description,
                            item.ItemStateCode,
                            productDescription = product.Description,
                            Quantity = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S ? item.Quantity.ToString() : item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C ? (item.Quantity * item.CaseSize).ToString() : null,
                            PartialQuantity = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O ? item.Quantity.ToString() : null,
                            product.Strength,
                            product.NDCUPCWithDashes,
                            groupDataDetail.WasteCodeDescription,
                            ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString() : string.Empty,
                            QoskProcessDate = item.CreatedDate != null ? item.CreatedDate.ToString() : string.Empty,
                            StoreName = qoskprofile.Name,
                            profile.Name,
                            qoskprofile.StoreNumber,
                            product.PackageSize,
                            product.ControlNumber,
                            DosageForm = dosageDict.Description,
                            PackageForm = packageDict.Description,
                            profile.City,
                            profile.DEANumber,
                            profile.State,
                            item.ContainerTypeName,
                            SealedOpenCase = item.ItemStateCode,
                            profile.ZipCode,
                         }
               ).ToList();

            var queryExtended = query.Count() > 0
               ? (from record in query.Distinct()
                  where (record.PartialQuantity != null
                  ? record.PartialQuantity.Contains(PartialQty) || PartialQty == ""
                  : PartialQty == "")
                  select new WasteReportEmailEntity
                  {
                     NDCNumber = record.NDCUPCWithDashes,
                     ItemGuid = record.ItemGUID.ToString(),
                     Strength = record.Strength,
                     LotNumber = record.LotNumber?.ToUpper(),
                     //WasteStreamProfile = record.ApprovalCode + Utility.Constants.ReportDataContext_Space + record.wasteStreamProfileDescription,
                     WasteCode = record.WasteCodeDescription,
                     SealedOpenCase = record.SealedOpenCase,
                     ProductDescription = record.productDescription,
                     FullQty = record.Quantity,
                     PartialQty = record.PartialQuantity,
                     StoreName = record.StoreName,
                     VendorName = record.Name,
                     StoreNumber = record.StoreNumber,
                     ExpDate = record.ExpDate,
                     PackageSize = record.PackageSize,
                     ControlNumber = record.ControlNumber,
                     DosageForm = record.DosageForm,
                     PackageForm = record.PackageForm,
                     City = record.City,
                     State = record.State,
                     DEANumber = record.DEANumber,
                     ZipCode = record.ZipCode,
                     ContainerTypeName = record.ContainerTypeName,
                     QoskProcessDate = record.QoskProcessDate
                  }).ToList()
                : new List<WasteReportEmailEntity>();

            if (queryExtended.Any())
            {
               foreach (WasteReportEmailEntity item in queryExtended)
               {
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }
                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? convertedQoskProcessDate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone,
                        Convert.ToDateTime(item.QoskProcessDate));
                     if (convertedQoskProcessDate != null)
                     {
                        item.QoskProcessDate =
                           convertedQoskProcessDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }
                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? Expdate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = Expdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.FullQty))
                  {
                     item.FullQty = RemoveTrailingZero(item.FullQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.PartialQty))
                  {
                     item.PartialQty = RemoveTrailingZero(item.PartialQty).ToString();
                  }
                  if (item.PackageSize != null)
                  {
                     item.PackageSize = Convert.ToDecimal(RemoveTrailingZero(item.PackageSize));
                  }
               }
            }

            return queryExtended;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Pharmacy Summary report.
      /// </summary>
      /// <param name="objPharmacySummaryEntity"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<PharmacyReportEmailEntity> Get_Pharmacy_Summary_Details(PharmacyReportEmailEntity objPharmacySummaryEntity, string FileType, long UserId)
      {
         return Get_Pharmacy_Summary_Details(objPharmacySummaryEntity.DataStartDate, objPharmacySummaryEntity.DataEndDate,
            objPharmacySummaryEntity.ReportDataTimeZone, FileType, objPharmacySummaryEntity.StoreNumber, objPharmacySummaryEntity.StoreName, objPharmacySummaryEntity.WholesalerCustomerNumber,
            objPharmacySummaryEntity.RegionName, objPharmacySummaryEntity.ReturnCreditableValue, objPharmacySummaryEntity.ReturnCreditableQty, objPharmacySummaryEntity.RecallCreditableValue,
            objPharmacySummaryEntity.RecallCreditableQty, objPharmacySummaryEntity.NonCreditableValue, objPharmacySummaryEntity.NonCreditableQty, objPharmacySummaryEntity.TotalProductValue,
            objPharmacySummaryEntity.TotalQty, objPharmacySummaryEntity.ProcessingFeeBilled, objPharmacySummaryEntity.TotalInvoiceAmount, objPharmacySummaryEntity.TotalDebitMemoQty, objPharmacySummaryEntity.ReturnToStockValue,
            objPharmacySummaryEntity.ReturnToStockQty, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Pharmacy Summary report.
      /// </summary>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="StoreNumber"></param>
      /// <param name="StoreName"></param>
      /// <param name="WholesalerCustomerNumber"></param>
      /// <param name="RegionName"></param>
      /// <param name="ReturnCreditableValue"></param>
      /// <param name="ReturnCreditableQty"></param>
      /// <param name="RecallCreditableValue"></param>
      /// <param name="RecallCreditableQty"></param>
      /// <param name="NonCreditableValue"></param>
      /// <param name="NonCreditableQty"></param>
      /// <param name="TotalProductValue"></param>
      /// <param name="TotalQty"></param>
      /// <param name="ProcessingFeeBilled"></param>
      /// <param name="TotalInvoiceAmount"></param>
      /// <param name="TotalDebitMemoQty"></param>
      /// <param name="ReturnToStockValue"></param>
      /// <param name="ReturnToStockQty"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<PharmacyReportEmailEntity> Get_Pharmacy_Summary_Details(DateTime? DataStartDate, DateTime? DataEndDate, string Timezone, string FileType, string StoreNumber,
         string StoreName, string WholesalerCustomerNumber, string RegionName, string ReturnCreditableValue, string ReturnCreditableQty, string RecallCreditableValue, string RecallCreditableQty, string NonCreditableValue, string NonCreditableQty,
         string TotalProductValue, string TotalQty, string ProcessingFeeBilled, string TotalInvoiceAmount, string TotalDebitMemoQty, string ReturnToStockValue, string ReturnToStockQty, long UserID)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profileUsers = GetValidProfileUsers(UserID);

            var result = (from item in cloudModelEntities.Item
                          join product in cloudModelEntities.Product
                             on item.ProductID equals product.ProductID
                          join itemStateDict in cloudModelEntities.ItemStateDict
                             on item.ItemStateCode equals itemStateDict.Code

                          join profile in cloudModelEntities.Profile
                             on product.ProfileCode equals profile.ProfileCode into profiles
                          from profile in profiles.DefaultIfEmpty()

                          join qosk in cloudModelEntities.Qosk
                             on item.QoskID equals qosk.QoskID

                          join qoskprofile in cloudModelEntities.Profile
                             on qosk.ProfileCode equals qoskprofile.ProfileCode

                          join lot in cloudModelEntities.Lot
                             on item.LotNumber equals lot.LotNumber into lots
                          from lot in lots.DefaultIfEmpty()
                          where

                             (profileUsers.Contains(item.CreatedBy))
                             && (string.IsNullOrEmpty(StoreNumber) || qoskprofile.StoreNumber == StoreNumber)
                             && (string.IsNullOrEmpty(StoreName) || qoskprofile.Name.Contains(StoreName))
                             && (string.IsNullOrEmpty(ReturnCreditableValue) || item.ReturnableItemLowestPriceValue.ToString().Contains(ReturnCreditableValue))
                             && (string.IsNullOrEmpty(ReturnCreditableQty) || item.Returnable == true && item.Quantity.ToString().Contains(ReturnCreditableQty))
                             && (string.IsNullOrEmpty(RecallCreditableValue) || lot.RecallID != null && item.ProductID == lot.ProductID && item.ReturnableItemLowestPriceValue.ToString().Contains(RecallCreditableValue))
                             && (string.IsNullOrEmpty(RecallCreditableQty) || lot.RecallID != null && item.ProductID == lot.ProductID && item.Quantity.ToString().Contains(RecallCreditableQty))
                             && (string.IsNullOrEmpty(NonCreditableValue) || item.Returnable == false && item.ReturnableItemLowestPriceValue.ToString().Contains(NonCreditableValue))
                             && (string.IsNullOrEmpty(NonCreditableQty) || item.Returnable == false && item.Quantity.ToString().Contains(NonCreditableQty))
                             && (string.IsNullOrEmpty(TotalProductValue) || (item.ItemUnitPrice * item.Quantity).ToString().Contains(TotalProductValue))
                             && (string.IsNullOrEmpty(TotalQty) || item.Quantity.ToString().Contains(TotalQty))
                          //&& (DataStartDate == null || DataEndDate == null || item.CreatedDate >= DataStartDate && item.CreatedDate <= DataEndDate)
                          //&& (qosk.ProfileCode < 900000 || qosk.ProfileCode > 999999)
                          select new PharmacyReportEmailEntity
                          {
                             StoreName = qoskprofile.Name,
                             StoreNumber = qoskprofile.StoreNumber,
                             WholesalerCustomerNumber = Utility.Constants.ReportDataContext_NA,
                             RegionName = Utility.Constants.ReportDataContext_NA,
                             ReturnCreditableValue = item.ReturnableItemLowestPriceValue.ToString(),
                             ReturnCreditableQty = item.Returnable == true ? item.Quantity.ToString() : null,
                             RecallCreditableValue = lot.RecallID != null && item.ProductID == lot.ProductID ? item.ReturnableItemLowestPriceValue.ToString() : null,
                             RecallCreditableQty = lot.RecallID != null && item.ProductID == lot.ProductID ? item.Quantity.ToString() : null,
                             NonCreditableValue = item.Returnable == false ? item.ReturnableItemLowestPriceValue.ToString() : null,
                             NonCreditableQty = item.Returnable == false ? item.Quantity.ToString() : null,
                             TotalProductValue = (item.ItemUnitPrice * item.Quantity).ToString(),
                             TotalQty = item.Quantity.ToString(),
                             ProcessingFeeBilled = Utility.Constants.ReportDataContext_NA,
                             TotalInvoiceAmount = Utility.Constants.ReportDataContext_NA,
                             TotalDebitMemoQty = Utility.Constants.ReportDataContext_NA,
                             ReturnToStockValue = Utility.Constants.ReportDataContext_NA,
                             ReturnToStockQty = Utility.Constants.ReportDataContext_NA,
                             //For email use
                             City = profile.City,
                             State = profile.State,
                             ZipCode = profile.ZipCode,
                             DEANumber = profile.DEANumber,
                             ContainerTypeName = item.ContainerTypeName,
                             QoskProcessDate = item.CreatedDate != null ? item.CreatedDate.ToString() : string.Empty,
                             VendorName = profile.Name,
                             ItemGuid = item.ItemGUID.ToString(),
                             FullQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S ? item.Quantity.ToString() : item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C ? (item.Quantity * item.CaseSize).ToString() : null,
                             PartialQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O ? item.Quantity.ToString() : null,
                             ProductDescription = product.Description,
                             NDCNumber = product.NDCUPCWithDashes,
                             LotNumber = item.LotNumber.ToUpper(),
                             ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString() : string.Empty,
                             SealedOpenCase = itemStateDict.Description,
                          }).Distinct().ToList();

            if (result.Any())
            {
               foreach (var item in result)
               {
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }
                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? convertedQoskProcessDate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Convert.ToDateTime(item.QoskProcessDate));
                     if (convertedQoskProcessDate != null)
                     {
                        item.QoskProcessDate = convertedQoskProcessDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }
                  if (!string.IsNullOrEmpty(item.ReturnCreditableValue))
                  {
                     item.ReturnCreditableValue = (Math.Round(Convert.ToDouble(item.ReturnCreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.RecallCreditableValue))
                  {
                     item.RecallCreditableValue = (Math.Round(Convert.ToDouble(item.RecallCreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.NonCreditableValue))
                  {
                     item.NonCreditableValue = (Math.Round(Convert.ToDouble(item.NonCreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.ReturnCreditableQty))
                  {
                     item.ReturnCreditableQty = RemoveTrailingZero(item.ReturnCreditableQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.NonCreditableQty))
                  {
                     item.NonCreditableQty = RemoveTrailingZero(item.NonCreditableQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.TotalProductValue))
                  {
                     item.TotalProductValue = (Math.Round(Convert.ToDouble(item.TotalProductValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);

                  }
                  if (!string.IsNullOrEmpty(item.TotalQty))
                  {
                     item.TotalQty = RemoveTrailingZero(item.TotalQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? Expdate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = Expdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.FullQty))
                  {
                     item.FullQty = RemoveTrailingZero(item.FullQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.PartialQty))
                  {
                     item.PartialQty = RemoveTrailingZero(item.PartialQty).ToString();
                  }
               }
            }

            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Non Creditable Summary report.
      /// </summary>
      /// <param name="objNonCreditableSummaryEntity"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<NonCreditableSummaryEmailEntity> Get_Non_Creditable_Summary_Details(NonCreditableSummaryEmailEntity objNonCreditableSummaryEntity, string FileType, long UserId)
      {
         return Get_Non_Creditable_Summary_Details(objNonCreditableSummaryEntity.DataStartDate, objNonCreditableSummaryEntity.DataEndDate,
            objNonCreditableSummaryEntity.ReportDataTimeZone, FileType, objNonCreditableSummaryEntity.StoreNumber, objNonCreditableSummaryEntity.StoreName,
            objNonCreditableSummaryEntity.RegionName, objNonCreditableSummaryEntity.NonCreditableValue, objNonCreditableSummaryEntity.NonCreditableQty, objNonCreditableSummaryEntity.ManufacturerName,
            objNonCreditableSummaryEntity.NonCreditableReasonCode, objNonCreditableSummaryEntity.NonCreditableReasonDescription, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Non Creditable Summary report.
      /// </summary>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="StoreNumber"></param>
      /// <param name="StoreName"></param>
      /// <param name="RegionName"></param>
      /// <param name="NonCreditableValue"></param>
      /// <param name="NonCreditableQty"></param>
      /// <param name="ManufacturerName"></param>
      /// <param name="NonCreditableReasonCode"></param>
      /// <param name="NonCreditableReasonDescription"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<NonCreditableSummaryEmailEntity> Get_Non_Creditable_Summary_Details(DateTime? DataStartDate, DateTime? DataEndDate, string Timezone, string FileType, string StoreNumber,
         string StoreName, string RegionName, string NonCreditableValue, string NonCreditableQty, string ManufacturerName, string NonCreditableReasonCode,
         string NonCreditableReasonDescription, long UserID)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profileUsers = GetValidProfileUsers(UserID);

            var result = (from item in cloudModelEntities.Item
                          join product in cloudModelEntities.Product
                             on item.ProductID equals product.ProductID
                          join itemStateDict in cloudModelEntities.ItemStateDict
                             on item.ItemStateCode equals itemStateDict.Code

                          join profile in cloudModelEntities.Profile
                             on product.ProfileCode equals profile.ProfileCode into profiles
                          from profile in profiles.DefaultIfEmpty()

                          join qosk in cloudModelEntities.Qosk
                             on item.QoskID equals qosk.QoskID

                          join qoskprofile in cloudModelEntities.Profile
                             on qosk.ProfileCode equals qoskprofile.ProfileCode

                          where

                             (profileUsers.Contains(item.CreatedBy))
                             && (string.IsNullOrEmpty(StoreNumber) || qoskprofile.StoreNumber == StoreNumber)
                             && (string.IsNullOrEmpty(StoreName) || qoskprofile.Name.Contains(StoreName))
                             && (string.IsNullOrEmpty(NonCreditableValue) || item.Returnable == false && item.ReturnableItemLowestPriceValue.ToString().Contains(NonCreditableValue))
                             && (string.IsNullOrEmpty(NonCreditableQty) || item.Returnable == false && item.Quantity.ToString().Contains(NonCreditableQty))
                             && (string.IsNullOrEmpty(ManufacturerName) || profile.Name.Contains(ManufacturerName))
                             && (string.IsNullOrEmpty(NonCreditableReasonDescription) || item.NonReturnableReasonDetail.Contains(NonCreditableReasonDescription))
                          //&& (DataStartDate == null || DataEndDate == null || item.CreatedDate >= DataStartDate && item.CreatedDate <= DataEndDate)
                          //&& (qosk.ProfileCode < 900000 || qosk.ProfileCode > 999999)
                          select new NonCreditableSummaryEmailEntity
                          {
                             StoreName = qoskprofile.Name,
                             StoreNumber = qoskprofile.StoreNumber,
                             RegionName = Utility.Constants.ReportDataContext_NA,
                             NonCreditableValue = item.Returnable == false ? item.ReturnableItemLowestPriceValue.ToString() : null,
                             NonCreditableQty = item.Returnable == false ? item.Quantity.ToString() : null,
                             ManufacturerName = profile.Name,
                             NonCreditableReasonCode = Utility.Constants.ReportDataContext_NA,
                             NonCreditableReasonDescription = item.NonReturnableReasonDetail,
                             //for email use
                             City = profile.City,
                             State = profile.State,
                             ZipCode = profile.ZipCode,
                             DEANumber = profile.DEANumber,
                             ContainerTypeName = item.ContainerTypeName,
                             QoskProcessDate = item.CreatedDate != null ? item.CreatedDate.ToString() : string.Empty,
                             VendorName = profile.Name,
                             ItemGuid = item.ItemGUID.ToString(),
                             FullQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S ? item.Quantity.ToString() : item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C ? (item.Quantity * item.CaseSize).ToString() : null,
                             PartialQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O ? item.Quantity.ToString() : null,
                             ProductDescription = product.Description,
                             NDCNumber = product.NDCUPCWithDashes,
                             LotNumber = item.LotNumber.ToUpper(),
                             ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString() : string.Empty,
                             SealedOpenCase = itemStateDict.Description,
                          }).Distinct().ToList();

            if (result.Any())
            {
               foreach (var item in result)
               {
                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? ConvertedQoskProcessDate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Convert.ToDateTime(item.QoskProcessDate));
                     if (ConvertedQoskProcessDate != null)
                     {
                        item.QoskProcessDate = ConvertedQoskProcessDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }
                  if (!string.IsNullOrEmpty(item.NonCreditableQty))
                  {
                     item.NonCreditableQty = RemoveTrailingZero(item.NonCreditableQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.NonCreditableValue))
                  {
                     item.NonCreditableValue = (Math.Round(Convert.ToDouble(item.NonCreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }
                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? Expdate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = Expdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.FullQty))
                  {
                     item.FullQty = RemoveTrailingZero(item.FullQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.PartialQty))
                  {
                     item.PartialQty = RemoveTrailingZero(item.PartialQty).ToString();
                  }
               }

            }

            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Manufacturer Summary report.
      /// </summary>
      /// <param name="objManufacturerSummaryEmailEntity"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ManufacturerSummaryEmailEntity> Get_Manufacturer_Summary_Details(ManufacturerSummaryEmailEntity objManufacturerSummaryEmailEntity, string FileType, long UserId)
      {
         return Get_Manufacturer_Summary_Details(objManufacturerSummaryEmailEntity.DataStartDate, objManufacturerSummaryEmailEntity.DataEndDate,
            objManufacturerSummaryEmailEntity.ReportDataTimeZone, FileType, objManufacturerSummaryEmailEntity.MFGLabeler, objManufacturerSummaryEmailEntity.ManufacturerName,
            objManufacturerSummaryEmailEntity.RetailerVendorNumber, objManufacturerSummaryEmailEntity.DebitMemoInvoiceNo, objManufacturerSummaryEmailEntity.CreditableValue, objManufacturerSummaryEmailEntity.CreditableQty,
            objManufacturerSummaryEmailEntity.RecallCreditableValue, objManufacturerSummaryEmailEntity.RecallCreditableQty, objManufacturerSummaryEmailEntity.TotalProductValue, objManufacturerSummaryEmailEntity.TotalQty,
            objManufacturerSummaryEmailEntity.ProcessingFee, objManufacturerSummaryEmailEntity.TotalInvoiceAmount, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Manufacturer Summary report.
      /// </summary>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="MFGLabeler"></param>
      /// <param name="ManufacturerName"></param>
      /// <param name="RetailerVendorNumber"></param>
      /// <param name="DebitMemoInvoiceNo"></param>
      /// <param name="CreditableValue"></param>
      /// <param name="CreditableQty"></param>
      /// <param name="RecallCreditableValue"></param>
      /// <param name="RecallCreditableQty"></param>
      /// <param name="TotalProductValue"></param>
      /// <param name="TotalQty"></param>
      /// <param name="ProcessingFee"></param>
      /// <param name="TotalInvoiceAmount"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ManufacturerSummaryEmailEntity> Get_Manufacturer_Summary_Details(DateTime? DataStartDate, DateTime? DataEndDate, string Timezone, string FileType, string MFGLabeler,
         string ManufacturerName, string RetailerVendorNumber, string DebitMemoInvoiceNo, string CreditableValue, string CreditableQty, string RecallCreditableValue,
         string RecallCreditableQty, string TotalProductValue, string TotalQty, string ProcessingFee, string TotalInvoiceAmount, long UserID)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profileUsers = GetValidProfileUsers(UserID);

            var result = (from item in cloudModelEntities.Item
                          join product in cloudModelEntities.Product
                             on item.ProductID equals product.ProductID
                          join itemStateDict in cloudModelEntities.ItemStateDict
                             on item.ItemStateCode equals itemStateDict.Code

                          join profile in cloudModelEntities.Profile on product.ProfileCode equals profile.ProfileCode
                             into profiles
                          from profile in profiles.DefaultIfEmpty()

                          join qosk in cloudModelEntities.Qosk
                             on item.QoskID equals qosk.QoskID

                          join qoskprofile in cloudModelEntities.Profile
                             on qosk.ProfileCode equals qoskprofile.ProfileCode

                          join lot in cloudModelEntities.Lot
                             on item.LotNumber equals lot.LotNumber into lots
                          from lot in lots.DefaultIfEmpty()
                          where
                             (profileUsers.Contains(item.CreatedBy))
                             && (string.IsNullOrEmpty(CreditableValue) || item.Returnable == true && item.ReturnableItemLowestPriceValue.ToString().Contains(CreditableValue))
                             && (string.IsNullOrEmpty(CreditableQty) || item.Returnable == true && item.Quantity.ToString().Contains(CreditableQty))
                             && (string.IsNullOrEmpty(ManufacturerName) || profile.Name.Contains(ManufacturerName))
                             && (string.IsNullOrEmpty(RecallCreditableValue) || lot.RecallID != null && item.ProductID == lot.ProductID && item.ReturnableItemLowestPriceValue.ToString().Contains(RecallCreditableValue))
                             && (string.IsNullOrEmpty(RecallCreditableQty) || lot.RecallID != null && item.ProductID == lot.ProductID && item.Quantity.ToString().Contains(RecallCreditableQty))
                             && (string.IsNullOrEmpty(TotalProductValue) || (item.ItemUnitPrice * item.Quantity).ToString().Contains(TotalProductValue))
                             && (string.IsNullOrEmpty(TotalQty) || item.Quantity.ToString().Contains(TotalQty))
                          //&& (DataStartDate == null || DataEndDate == null || item.CreatedDate >= DataStartDate && item.CreatedDate <= DataEndDate)
                          //&& (qosk.ProfileCode < 900000 || qosk.ProfileCode > 999999)
                          select new ManufacturerSummaryEmailEntity
                          {
                             MFGLabeler = Utility.Constants.ReportDataContext_NA,
                             ManufacturerName = profile.Name,
                             RetailerVendorNumber = Utility.Constants.ReportDataContext_NA,
                             DebitMemoInvoiceNo = Utility.Constants.ReportDataContext_NA,
                             CreditableValue = item.Returnable == true ? item.ReturnableItemLowestPriceValue.ToString() : null,
                             CreditableQty = item.Returnable == true ? item.Quantity.ToString() : null,
                             RecallCreditableValue = lot.RecallID != null && item.ProductID == lot.ProductID ? item.ReturnableItemLowestPriceValue.ToString() : null,
                             RecallCreditableQty = lot.RecallID != null && item.ProductID == lot.ProductID ? item.Quantity.ToString() : null,
                             TotalProductValue = (item.ItemUnitPrice * item.Quantity).ToString(),
                             TotalQty = item.Quantity.ToString(),
                             ProcessingFee = Utility.Constants.ReportDataContext_NA,
                             TotalInvoiceAmount = Utility.Constants.ReportDataContext_NA,
                             //for email use
                             StoreName = qoskprofile.Name,
                             City = profile.City,
                             State = profile.State,
                             ZipCode = profile.ZipCode,
                             DEANumber = profile.DEANumber,
                             ContainerTypeName = item.ContainerTypeName,
                             QoskProcessDate = item.CreatedDate != null ? item.CreatedDate.ToString() : string.Empty,
                             VendorName = profile.Name,
                             ItemGuid = item.ItemGUID.ToString(),
                             FullQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_S ? item.Quantity.ToString() : item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_C ? (item.Quantity * item.CaseSize).ToString() : null,
                             PartialQty = item.ItemStateCode == Utility.Constants.ReportDataContext_ItemStateDict_Code_Initial_O ? item.Quantity.ToString() : null,
                             ProductDescription = product.Description,
                             NDCNumber = product.NDCUPCWithDashes,
                             LotNumber = item.LotNumber.ToUpper(),
                             ExpDate = item.ExpirationDate != null ? item.ExpirationDate.ToString() : string.Empty,
                             SealedOpenCase = itemStateDict.Description,
                          }).Distinct().ToList();

            if (result.Any())
            {
               foreach (var item in result)
               {
                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? convertedQoskProcessDate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Convert.ToDateTime(item.QoskProcessDate));
                     if (convertedQoskProcessDate != null)
                     {
                        item.QoskProcessDate = convertedQoskProcessDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }
                  if (!string.IsNullOrEmpty(item.CreditableQty))
                  {
                     item.CreditableQty = RemoveTrailingZero(item.CreditableQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.TotalQty))
                  {
                     item.TotalQty = RemoveTrailingZero(item.TotalQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.CreditableValue))
                  {
                     item.CreditableValue = (Math.Round(Convert.ToDouble(item.CreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.RecallCreditableValue))
                  {
                     item.RecallCreditableValue = (Math.Round(Convert.ToDouble(item.RecallCreditableValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.TotalProductValue))
                  {
                     item.TotalProductValue = (Math.Round(Convert.ToDouble(item.TotalProductValue), 2)).ToString(Utility.Constants.ReportDataContext_ZeroAfterDecimal);
                  }
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }
                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? expDate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = expDate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.FullQty))
                  {
                     item.FullQty = RemoveTrailingZero(item.FullQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.PartialQty))
                  {
                     item.PartialQty = RemoveTrailingZero(item.PartialQty).ToString();
                  }
               }

            }
            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Return To Stock report.
      /// </summary>
      /// <param name="objReturnToStockEmailEntity"></param>
      /// <param name="FileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ReturnToStockEmailEntity> Get_Return_To_Stock_Details(ReturnToStockEmailEntity objReturnToStockEmailEntity, string FileType, long UserId)
      {
         DateTime? expDate = null;
         DateTime? qoskProcessDate = null;
         DateTime? dateEligibleForCredit = null;
         if (!string.IsNullOrEmpty(objReturnToStockEmailEntity.ExpDate))
         {
            expDate = Convert.ToDateTime(objReturnToStockEmailEntity.ExpDate);
         }
         if (!string.IsNullOrEmpty(objReturnToStockEmailEntity.QoskProcessDate))
         {
            qoskProcessDate = Convert.ToDateTime(objReturnToStockEmailEntity.QoskProcessDate);
         }
         if (!string.IsNullOrEmpty(objReturnToStockEmailEntity.DateEligibleForCredit))
         {
            dateEligibleForCredit = Convert.ToDateTime(objReturnToStockEmailEntity.DateEligibleForCredit);
         }
         return Get_Return_To_Stock_Details(objReturnToStockEmailEntity.DataStartDate,
            objReturnToStockEmailEntity.DataEndDate, objReturnToStockEmailEntity.ReportDataTimeZone, FileType, objReturnToStockEmailEntity.VendorName, objReturnToStockEmailEntity.StoreName,
            objReturnToStockEmailEntity.StoreNumber, objReturnToStockEmailEntity.NDCNumber, objReturnToStockEmailEntity.ProductDescription,
            objReturnToStockEmailEntity.Strength, objReturnToStockEmailEntity.ControlNumber, objReturnToStockEmailEntity.RXorOTC, objReturnToStockEmailEntity.DosageForm, objReturnToStockEmailEntity.PackageForm,
            objReturnToStockEmailEntity.LotNumber, expDate, qoskProcessDate, dateEligibleForCredit, objReturnToStockEmailEntity.OutofpolicyDescription, UserId);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get data for Return To Stock report.
      /// </summary>
      /// <param name="DataStartDate"></param>
      /// <param name="DataEndDate"></param>
      /// <param name="Timezone"></param>
      /// <param name="FileType"></param>
      /// <param name="VendorName"></param>
      /// <param name="StoreName"></param>
      /// <param name="StoreNumber"></param>
      /// <param name="NDCNumber"></param>
      /// <param name="ProductDescription"></param>
      /// <param name="Strength"></param>
      /// <param name="ControlNumber"></param>
      /// <param name="RXorOTC"></param>
      /// <param name="DosageForm"></param>
      /// <param name="PackageForm"></param>
      /// <param name="LotNumber"></param>
      /// <param name="ExpDate"></param>
      /// <param name="QoskProcessDate"></param>
      /// <param name="DateEligibleForCredit"></param>
      /// <param name="OutofpolicyDescription"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ReturnToStockEmailEntity> Get_Return_To_Stock_Details(DateTime? DataStartDate, DateTime? DataEndDate, string Timezone,
         string FileType, string VendorName, string StoreName, string StoreNumber, string NDCNumber, string ProductDescription,
         string Strength, int? ControlNumber, string RXorOTC, string DosageForm, string PackageForm, string LotNumber, DateTime? ExpDate, DateTime? QoskProcessDate,
         DateTime? DateEligibleForCredit, string OutofpolicyDescription, long UserID)
      {
         DateTime processDate = QoskProcessDate == null ? DateTime.MinValue : QoskProcessDate.Value.Date;
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //var profileUsers = GetValidProfileUsers(UserID);
            var result = (from productNotification in cloudModelEntities.ProductNotification

                          join qosk in cloudModelEntities.Qosk
                             on productNotification.QoskMachineID equals qosk.MachineID

                          join product in cloudModelEntities.Product
                             on productNotification.ProductID equals product.ProductID

                          join profile in cloudModelEntities.Profile on product.ProfileCode equals profile.ProfileCode
                             into profiles
                          from profile in profiles.DefaultIfEmpty()

                          join dosageDict in cloudModelEntities.DosageDict
                             on product.DosageCode equals dosageDict.Code

                          join packageDict in cloudModelEntities.PackageDict
                             on product.PackageCode equals packageDict.Code

                          join qoskprofile in cloudModelEntities.Profile
                             on qosk.ProfileCode equals qoskprofile.ProfileCode

                          where
                                         /*(profileUsers.Contains(item.CreatedBy))
                                          &&*/ (string.IsNullOrEmpty(VendorName) || profile.Name.Contains(VendorName))
                                                                                                           && (string.IsNullOrEmpty(StoreName) || qoskprofile.Name.Contains(StoreName))
                                                                                                           && (string.IsNullOrEmpty(StoreNumber) || qoskprofile.StoreNumber == StoreNumber)
                                                                                                           && (string.IsNullOrEmpty(NDCNumber) || (!string.IsNullOrEmpty(product.NDCUPCWithDashes) && product.NDCUPCWithDashes.Replace(Utility.Constants.ReportDataContext_Dash, string.Empty).Contains(NDCNumber)))

                                                                                                           && (string.IsNullOrEmpty(ProductDescription) || product.Description.Contains(ProductDescription))
                                                                                                           && (string.IsNullOrEmpty(Strength) || product.Strength.Contains(Strength))
                                                                                                           && (ControlNumber == null || product.ControlNumber.ToString().Contains(ControlNumber.ToString()))
                                                                                                           && (string.IsNullOrEmpty(RXorOTC) || (product.RXorOTC == false ? Utility.Constants.ReportDataContext_OTC : Utility.Constants.ReportDataContext_RX).Contains(RXorOTC))

                                                                                                           && (string.IsNullOrEmpty(DosageForm) || dosageDict.Description.Contains(DosageForm))
                                                                                                           && (string.IsNullOrEmpty(PackageForm) || packageDict.Description.Contains(PackageForm))
                                                                                                           && (string.IsNullOrEmpty(LotNumber) || productNotification.LotNumber.Contains(LotNumber))
                                                                                                           && (ExpDate == null || productNotification.ExpirationDate == ExpDate)
                                                                                                           && (processDate == DateTime.MinValue || productNotification.CreatedDate == null || (System.Data.Entity.DbFunctions.TruncateTime(productNotification.CreatedDate) == QoskProcessDate))
                                                                                                           && (DateEligibleForCredit == null || productNotification.FutureIndateOpen == DateEligibleForCredit)
                          //&& (qosk.ProfileCode < 900000 || qosk.ProfileCode > 999999)
                          select new ReturnToStockEmailEntity
                          {
                             NDCNumber = product.NDCUPCWithDashes,
                             DosageForm = dosageDict.Description,
                             PackageForm = packageDict.Description,
                             Strength = product.Strength,
                             LotNumber = productNotification.LotNumber,
                             QoskProcessDate = productNotification.CreatedDate != null ? productNotification.CreatedDate.ToString() : string.Empty,
                             ExpDate = productNotification.ExpirationDate != null ? productNotification.ExpirationDate.ToString() : string.Empty,
                             DateEligibleForCredit = productNotification.FutureIndateOpen != null ? productNotification.FutureIndateOpen.ToString() : string.Empty,
                             RXorOTC = product.RXorOTC == false ? Utility.Constants.ReportDataContext_OTC : Utility.Constants.ReportDataContext_RX,
                             ControlNumber = product.ControlNumber,
                             VendorName = profile.Name,
                             StoreName = qoskprofile.Name,
                             StoreNumber = qoskprofile.StoreNumber,
                             ProductDescription = product.Description,
                             ItemGuid = productNotification.ItemGUID.ToString(),
                             City = profile.City,
                             DEANumber = profile.DEANumber,
                             State = profile.State,
                             ZipCode = profile.ZipCode
                          }).Distinct().ToList();
            if (result != null)
            {
               foreach (ReturnToStockEmailEntity item in result)
               {
                  if (!string.IsNullOrEmpty(item.NDCNumber))
                  {
                     item.NDCNumber = RemoveDashFromNDC(item.NDCNumber);
                  }
                  if (!string.IsNullOrEmpty(Timezone) && !string.IsNullOrEmpty(item.QoskProcessDate))
                  {
                     DateTime? ConvertedProcessdate = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Convert.ToDateTime(item.QoskProcessDate));
                     if (ConvertedProcessdate != null)
                     {
                        item.QoskProcessDate = ConvertedProcessdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                     }
                  }
                  if (!string.IsNullOrEmpty(item.ExpDate))
                  {
                     DateTime? Expdate = Convert.ToDateTime(item.ExpDate);
                     item.ExpDate = Expdate.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.DateEligibleForCredit))
                  {
                     DateTime? dateEligibleForCredit = Convert.ToDateTime(item.DateEligibleForCredit);
                     item.DateEligibleForCredit = dateEligibleForCredit.Value.ToString(Utility.Constants.ReportDataContext_DateFormat);
                  }
                  if (!string.IsNullOrEmpty(item.FullQty))
                  {
                     item.FullQty = RemoveTrailingZero(item.FullQty).ToString();
                  }
                  if (!string.IsNullOrEmpty(item.PartialQty))
                  {
                     item.PartialQty = RemoveTrailingZero(item.PartialQty).ToString();
                  }
               }

            }
            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Remove trailing zero.
      /// </summary>
      /// <param name="itemValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      public object RemoveTrailingZero(object itemValue)
      {
         itemValue = Convert.ToDecimal(itemValue) / 1.00000000000000000000m;
         return itemValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Remove dash from NDC.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public string RemoveDashFromNDC(string ndcNumber)
      {
         if (ndcNumber.Contains(Utility.Constants.ReportDataContext_Dash))
         {
            ndcNumber = ndcNumber.Replace(Utility.Constants.ReportDataContext_Dash, string.Empty);
            if (ndcNumber.Contains(Utility.Constants.ReportDataContext_Space))
               ndcNumber = ndcNumber.Replace(Utility.Constants.ReportDataContext_Space, string.Empty);
         }

         int strLength = ndcNumber.Length;

         if (strLength != 11)
         {
            int diff = 11 - strLength;
            for (int i = 0; i < diff; i++)
            {
               ndcNumber = 0 + ndcNumber;
            }
         }

         return ndcNumber;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="UserID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<string> GetValidProfileUsers(long UserID)
      {
         using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profileCode = (from users in cloudModelEntities.User
                               where users.UserID == UserID
                               select users.ProfileCode).FirstOrDefault();
            var userList = (from user in cloudModelEntities.User
                            where user.ProfileCode == profileCode
                            select user.UserName).ToList();
            return userList;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the list of all user profile codes in a group based on the user ID (pulled by username)
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<int?> GetValidUserProfileCodes(long userId)
      {
         using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var userList = (from users in cloudModelEntities.User
                            join userGrp in cloudModelEntities.UserGroups on users.UserID equals userGrp.UserID
                            join grp in cloudModelEntities.Group on userGrp.ProfileGroupID equals grp.GroupID
                            join prflGrpRel in cloudModelEntities.ProfileGrpRel on grp.GroupID equals prflGrpRel.GrpID
                            where users.UserID == userId
                            select (int?)prflGrpRel.ProfileCode).ToList();

            return userList;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Parse the NDC Number field to get the product ID
      /// </summary>
      /// <param name="ndcUpc"></param>
      /// <returns></returns>
      ///****************************************************************************
      public long? ParseNdcUpc(string ndcUpc)
      {
         var dbModel = new Qualanex.Qosk.Library.Model.DBModel.DBModel("name=QoskCloud");
         long upc;
         long ndc = 0;
         long productId = Qualanex.Qosk.Library.Model.ProductModel.ProductModel.IsExistUPC(ref dbModel, ndcUpc, out upc);

         if (productId == 0)
         {
            productId = Qualanex.Qosk.Library.Model.ProductModel.ProductModel.IsExistNDC(ref dbModel, ndcUpc, out ndc);
         }

         return productId != 0 ? productId : (long?)null;
      }
   }
}