﻿using System;
using System.Data.Entity.Validation;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Web.Areas.Report.ViewModel;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Common;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;
using Qualanex.QoskCloud.Web.Common;

namespace Qualanex.QoskCloud.Web.Areas.Report.Models
{
   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ReportDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   Get region list.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public List<StateDict> GetRegionList()
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var regions = (from r in cloudModelEntities.StateDict
                           select r).ToList();

            return regions;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieve all the reports from report table.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static IList<ReportEntity> GetAllReport()
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from report in cloudEntities.Report
                       where report.IsDeleted == false
                       select new ReportEntity
                       {
                          ReportID = report.ReportID,
                          ReportTitle = report.ReportTitle == "Sample_Report_Retailer_Product_Detail" ? "Product Details" : report.ReportTitle,
                          ReportModule = report.ReportModule
                       }).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Insert and update schedule data in report schedule table.
      /// </summary>
      /// <param name="reportScheduleDetailViewModel"></param>
      /// <param name="userId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response InsertUpdateScheduledData(ReportScheduleDetailViewModel reportScheduleDetailViewModel, long userId)
      {
         Response objclsResponse = new Response();
         ReportSchedule objReportSchedule = null;
         long reportScheduleID = 0;

         if (reportScheduleDetailViewModel != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               using (var dbTran = cloudModelEntities.Database.BeginTransaction())
               {
                  try
                  {
                     ReportSchedulerEntity objReportSchedulerEntity = new ReportSchedulerEntity()
                     {
                        ScheduleType = (ScheduleType)reportScheduleDetailViewModel.ScheduleType,
                        FileType = reportScheduleDetailViewModel.FileType,
                        SMSTo = reportScheduleDetailViewModel.SMSTo,
                        Every = reportScheduleDetailViewModel.Every,
                        StartDate =
                           CreateUTCDateTime(reportScheduleDetailViewModel.ReportScheduleTimeZone,
                              reportScheduleDetailViewModel.StartDate, reportScheduleDetailViewModel.Hour,
                              reportScheduleDetailViewModel.Minute,
                              reportScheduleDetailViewModel.Meridieum.ToString()),
                        EndDate =
                           CreateUTCDateTime(reportScheduleDetailViewModel.ReportScheduleTimeZone,
                              reportScheduleDetailViewModel.EndDate, reportScheduleDetailViewModel.Hour,
                              reportScheduleDetailViewModel.Minute,
                              reportScheduleDetailViewModel.Meridieum.ToString()),
                        Hour = reportScheduleDetailViewModel.Hour,
                        Minute = reportScheduleDetailViewModel.Minute,
                        Meridieum = reportScheduleDetailViewModel.Meridieum,
                        ReportScheduleTimeZone = reportScheduleDetailViewModel.ReportScheduleTimeZone

                     };

                     var JsonSchedule = JsonConvert.SerializeObject(objReportSchedulerEntity);

                     //Code for update report schedule table

                     var retailerReportSchedule =
                        cloudModelEntities.ReportSchedule.FirstOrDefault(
                           p => p.ReportScheduleID == reportScheduleDetailViewModel.ReportScheduleID);
                     var response = 0;

                     if (retailerReportSchedule != null)
                     {
                        retailerReportSchedule.ReportScheduleTitle =
                           reportScheduleDetailViewModel.ScheduleReportTitle;
                        retailerReportSchedule.Schedule = JsonSchedule;
                        retailerReportSchedule.ModifiedDate = DateTime.UtcNow;
                        cloudModelEntities.Entry(retailerReportSchedule).State =
                           System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                     }
                     else
                     {
                        objReportSchedule = new ReportSchedule()
                        {
                           ReportID = reportScheduleDetailViewModel.ReportID,
                           ReportScheduleTitle = reportScheduleDetailViewModel.ScheduleReportTitle,
                           UserID = userId,
                           Schedule = JsonSchedule,
                           CreatedDate = DateTime.UtcNow,
                           ModifiedDate = DateTime.UtcNow
                        };
                        cloudModelEntities.ReportSchedule.Add(objReportSchedule);
                        response = cloudModelEntities.SaveChanges();
                     }

                     if (response > 0 && !string.IsNullOrEmpty(reportScheduleDetailViewModel.EmailTo))
                     {
                        if (retailerReportSchedule != null)
                        {
                           var retailerReportScheduleData =
                              cloudModelEntities.ReportRecipient.Where(
                                 c => c.ReportScheduleId == reportScheduleDetailViewModel.ReportScheduleID)
                                 .ToList();
                           cloudModelEntities.ReportRecipient.RemoveRange(retailerReportScheduleData);
                           var responseResult = cloudModelEntities.SaveChanges();
                           reportScheduleID = reportScheduleDetailViewModel.ReportScheduleID;
                           string[] EmailAddresses = reportScheduleDetailViewModel.EmailTo.Split(Constants.Comma);
                           foreach (var email in EmailAddresses)
                           {
                              cloudModelEntities.ReportRecipient.Add(new ReportRecipient()
                              {
                                 ReportScheduleId = reportScheduleID,
                                 RecipientEmail = email
                              });
                           }
                        }
                        else if (objReportSchedule != null)
                        {
                           reportScheduleID = objReportSchedule.ReportScheduleID;
                           string[] EmailAddresses = reportScheduleDetailViewModel.EmailTo.Split(Constants.Comma);
                           foreach (var email in EmailAddresses)
                           {
                              cloudModelEntities.ReportRecipient.Add(new ReportRecipient()
                              {
                                 ReportScheduleId = reportScheduleID,
                                 RecipientEmail = email
                              });
                           }
                        }

                        if (cloudModelEntities.SaveChanges() > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.ReportScheduleId = reportScheduleID;
                           dbTran.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Fail;
                           dbTran.Rollback();
                        }
                     }
                     else
                     {
                        objclsResponse.Status = Status.Fail;
                        dbTran.Rollback();
                     }
                  }
                  catch
                  {
                     objclsResponse.Status = Status.Fail;
                     dbTran.Rollback();
                  }
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Product Details criteria in ReportSchedule table in criteria
      ///   column.
      /// </summary>
      /// <param name="retailerProductDetailsEntity"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdateProductDetailsCriteria(RetailerProductDetailsEntity retailerProductDetailsEntity, long ReportScheduleId)
      {
         Response objclsResponse = new Response();
         if (retailerProductDetailsEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var tempStartDate = retailerProductDetailsEntity.DataStartDate;
               var tempEndDate = retailerProductDetailsEntity.DataEndDate;

               if (retailerProductDetailsEntity.DataStartDate != null && retailerProductDetailsEntity.DataEndDate != null && retailerProductDetailsEntity.DataStartDate > retailerProductDetailsEntity.DataEndDate)
               {
                  tempStartDate = retailerProductDetailsEntity.DataEndDate;
                  tempEndDate = retailerProductDetailsEntity.DataStartDate;
               }

               RetailerProductDetailsEntity objRetailerProductDetailsEntity = new RetailerProductDetailsEntity()
               {
                  ItemGuid = retailerProductDetailsEntity.ItemGuid,
                  Strength = retailerProductDetailsEntity.Strength,
                  QoskProcessDate = retailerProductDetailsEntity.QoskProcessDate,
                  ControlNumber = retailerProductDetailsEntity.ControlNumber,
                  PackageSize = retailerProductDetailsEntity.PackageSize,
                  UnitPriceAfter = retailerProductDetailsEntity.UnitPriceAfter,
                  CreditableAmountAfter = retailerProductDetailsEntity.CreditableAmountAfter,
                  RecalledProduct = retailerProductDetailsEntity.RecalledProduct,
                  DiscontinuedProduct = retailerProductDetailsEntity.DiscontinuedProduct,
                  RXorOTC = retailerProductDetailsEntity.RXorOTC,
                  DosageForm = retailerProductDetailsEntity.DosageForm,
                  PackageForm = retailerProductDetailsEntity.PackageForm,
                  PartialPercentage = retailerProductDetailsEntity.PartialPercentage,
                  NDCNumber = retailerProductDetailsEntity.NDCNumber,
                  ReportDataTimeZone = retailerProductDetailsEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = retailerProductDetailsEntity.ReportScheduleTimeZone,
                  StoreName = retailerProductDetailsEntity.StoreName,
                  StoreNumber = retailerProductDetailsEntity.StoreNumber,
                  VendorName = retailerProductDetailsEntity.VendorName,
                  ProductDescription = retailerProductDetailsEntity.ProductDescription,
                  LotNumber = retailerProductDetailsEntity.LotNumber,
                  ExpDate = retailerProductDetailsEntity.ExpDate,
                  FullQty = retailerProductDetailsEntity.FullQty,
                  PartialQty = retailerProductDetailsEntity.PartialQty,
                  UnitPriceBefore = retailerProductDetailsEntity.UnitPriceBefore,
                  CreditableAmountBeforeMFGDiscount = retailerProductDetailsEntity.CreditableAmountBeforeMFGDiscount,
                  ReturnStatus = retailerProductDetailsEntity.ReturnStatus,
                  OutofpolicyDescription = retailerProductDetailsEntity.OutofpolicyDescription,
                  DataStartDate = tempStartDate,
                  DataEndDate = tempEndDate,
                  ExtendedDiscountPercent = retailerProductDetailsEntity.ExtendedDiscountPercent
               };

               var JsonCriteria = JsonConvert.SerializeObject(objRetailerProductDetailsEntity);

               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
               var response = 0;

               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  retailerReportSchedule.LastRunTime = null;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }

            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Waste Report criteria in ReportSchedule table in criteria
      ///   column.
      /// </summary>
      /// <param name="wasteReportDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdateWasteReportCriteria(WasteReportEntity wasteReportEntity, long ReportScheduleId)
      {
         Response objclsResponse = new Response();
         if (wasteReportEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               WasteReportEntity objWasteReportEntity = new WasteReportEntity()
               {
                  NDCNumber = wasteReportEntity.NDCNumber,
                  StoreName = wasteReportEntity.StoreName,
                  StoreNumber = wasteReportEntity.StoreNumber,
                  ReportDataTimeZone = wasteReportEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = wasteReportEntity.ReportScheduleTimeZone,
                  ItemGuid = wasteReportEntity.ItemGuid,
                  ProductDescription = wasteReportEntity.ProductDescription,
                  LotNumber = wasteReportEntity.LotNumber,
                  Strength = wasteReportEntity.Strength,
                  FullQty = wasteReportEntity.FullQty, //Partial Quantity
                  ExpDate = wasteReportEntity.ExpDate,
                  PackageSize = wasteReportEntity.PackageSize,
                  PartialQty = wasteReportEntity.PartialQty,
                  WasteStreamProfile = wasteReportEntity.WasteStreamProfile,
                  WasteCode = wasteReportEntity.WasteCode,
                  SealedOpenCase = wasteReportEntity.SealedOpenCase,
                  DataStartDate = wasteReportEntity.DataStartDate,
                  DataEndDate = wasteReportEntity.DataEndDate,
                  ControlNumber = wasteReportEntity.ControlNumber,
                  DosageForm = wasteReportEntity.DosageForm,
                  PackageForm = wasteReportEntity.PackageForm

               };
               string JsonCriteria = JsonConvert.SerializeObject(objWasteReportEntity);

               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
               var response = 0;

               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }

            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Pharmacy Summary criteria in ReportSchedule table in criteria
      ///   column.
      /// </summary>
      /// <param name="pharmacySummaryEntity"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdatePharmacySummaryCriteria(PharmacySummaryEntity pharmacySummaryEntity, long ReportScheduleId)
      {
         Response objclsResponse = new Response();

         if (pharmacySummaryEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               PharmacySummaryEntity objPharmacySummaryEntity = new PharmacySummaryEntity()
               {
                  StoreNumber = pharmacySummaryEntity.StoreNumber,
                  StoreName = pharmacySummaryEntity.StoreName,
                  WholesalerCustomerNumber = pharmacySummaryEntity.WholesalerCustomerNumber,
                  ReportDataTimeZone = pharmacySummaryEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = pharmacySummaryEntity.ReportScheduleTimeZone,
                  RegionName = pharmacySummaryEntity.RegionName,
                  ReturnCreditableValue = pharmacySummaryEntity.ReturnCreditableValue,
                  ReturnCreditableQty = pharmacySummaryEntity.ReturnCreditableQty,
                  RecallCreditableValue = pharmacySummaryEntity.RecallCreditableValue,
                  RecallCreditableQty = pharmacySummaryEntity.RecallCreditableQty, //Partial Quantity
                  NonCreditableValue = pharmacySummaryEntity.NonCreditableValue,
                  NonCreditableQty = pharmacySummaryEntity.NonCreditableQty,
                  TotalProductValue = pharmacySummaryEntity.TotalProductValue,
                  TotalQty = pharmacySummaryEntity.TotalQty,
                  ProcessingFeeBilled = pharmacySummaryEntity.ProcessingFeeBilled,
                  TotalInvoiceAmount = pharmacySummaryEntity.TotalInvoiceAmount,
                  TotalDebitMemoQty = pharmacySummaryEntity.TotalDebitMemoQty,
                  ReturnToStockValue = pharmacySummaryEntity.ReturnToStockValue,
                  ReturnToStockQty = pharmacySummaryEntity.ReturnToStockQty
               };
               string JsonCriteria = JsonConvert.SerializeObject(objPharmacySummaryEntity);
               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
               var response = 0;

               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Non Creditable Summary criteria in ReportSchedule table in
      ///   criteria column.
      /// </summary>
      /// <param name="nonCreditableSummaryEntity"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdateNonCreditableSummaryCriteria(NonCreditableSummaryEntity nonCreditableSummaryEntity, long reportScheduleId)
      {
         Response objclsResponse = new Response();
         if (nonCreditableSummaryEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               NonCreditableSummaryEntity objNonCreditableSummaryEntity = new NonCreditableSummaryEntity()
               {
                  StoreNumber = nonCreditableSummaryEntity.StoreNumber,
                  StoreName = nonCreditableSummaryEntity.StoreName,
                  ReportDataTimeZone = nonCreditableSummaryEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = nonCreditableSummaryEntity.ReportScheduleTimeZone,
                  RegionName = nonCreditableSummaryEntity.RegionName,
                  NonCreditableValue = nonCreditableSummaryEntity.NonCreditableValue,
                  NonCreditableQty = nonCreditableSummaryEntity.NonCreditableQty,
                  ManufacturerName = nonCreditableSummaryEntity.ManufacturerName,
                  NonCreditableReasonCode = nonCreditableSummaryEntity.NonCreditableReasonCode,
                  NonCreditableReasonDescription = nonCreditableSummaryEntity.NonCreditableReasonDescription,

               };
               string JsonCriteria = JsonConvert.SerializeObject(objNonCreditableSummaryEntity);

               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
               var response = 0;

               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Manufacturer Summary criteria in ReportSchedule table in
      ///   criteria column.
      /// </summary>
      /// <param name="manufacturerSummaryEntity"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdateManufacturerSummaryCriteria(ManufacturerSummaryEntity manufacturerSummaryEntity, long reportScheduleId)
      {
         Response objclsResponse = new Response();
         if (manufacturerSummaryEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               ManufacturerSummaryEntity objManufacturerSummaryEntity = new ManufacturerSummaryEntity()
               {
                  MFGLabeler = manufacturerSummaryEntity.MFGLabeler,
                  ManufacturerName = manufacturerSummaryEntity.ManufacturerName,
                  ReportDataTimeZone = manufacturerSummaryEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = manufacturerSummaryEntity.ReportScheduleTimeZone,
                  RetailerVendorNumber = manufacturerSummaryEntity.RetailerVendorNumber,
                  DebitMemoInvoiceNo = manufacturerSummaryEntity.DebitMemoInvoiceNo,
                  CreditableValue = manufacturerSummaryEntity.CreditableValue,
                  CreditableQty = manufacturerSummaryEntity.CreditableQty,
                  RecallCreditableValue = manufacturerSummaryEntity.RecallCreditableValue,
                  RecallCreditableQty = manufacturerSummaryEntity.RecallCreditableQty,
                  TotalProductValue = manufacturerSummaryEntity.TotalProductValue,
                  TotalQty = manufacturerSummaryEntity.TotalQty,
                  ProcessingFee = manufacturerSummaryEntity.ProcessingFee,
                  TotalInvoiceAmount = manufacturerSummaryEntity.TotalInvoiceAmount,

               };

               var JsonCriteria = JsonConvert.SerializeObject(objManufacturerSummaryEntity);

               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
               int response = 0;
               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Return To Stock criteria in ReportSchedule table in criteria
      ///   column.
      /// </summary>
      /// <param name="returnToStockReportEntity"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public Response UpdateReturnToStockCriteria(ReturnToStockReportEntity returnToStockReportEntity, long reportScheduleId)
      {
         Response objclsResponse = new Response();
         if (returnToStockReportEntity != null)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               ReturnToStockReportEntity objReturnToStockReportEntity = new ReturnToStockReportEntity()
               {
                  VendorName = returnToStockReportEntity.VendorName,
                  StoreName = returnToStockReportEntity.StoreName,
                  ReportDataTimeZone = returnToStockReportEntity.ReportDataTimeZone,
                  ReportScheduleTimeZone = returnToStockReportEntity.ReportScheduleTimeZone,
                  StoreNumber = returnToStockReportEntity.StoreNumber,
                  NDCNumber = returnToStockReportEntity.NDCNumber,
                  ProductDescription = returnToStockReportEntity.ProductDescription,
                  Strength = returnToStockReportEntity.Strength,
                  ControlNumber = returnToStockReportEntity.ControlNumber,
                  RXorOTC = returnToStockReportEntity.RXorOTC,
                  DosageForm = returnToStockReportEntity.DosageForm,
                  PackageForm = returnToStockReportEntity.PackageForm,
                  LotNumber = returnToStockReportEntity.LotNumber,
                  ExpDate = returnToStockReportEntity.ExpDate,
                  QoskProcessDate = returnToStockReportEntity.QoskProcessDate,
                  DateEligibleForCredit = returnToStockReportEntity.DateEligibleForCredit,
                  OutofpolicyDescription = returnToStockReportEntity.OutofpolicyDescription,

               };
               string JsonCriteria = JsonConvert.SerializeObject(objReturnToStockReportEntity);

               //Code for update report schedule table
               var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
               var response = 0;

               if (retailerReportSchedule != null)
               {
                  retailerReportSchedule.Criteria = JsonCriteria;
                  cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                  response = cloudModelEntities.SaveChanges();
               }

               if (response > 0)
               {
                  objclsResponse.Status = Status.OK;
               }
               else
               {
                  objclsResponse.Status = Status.Fail;
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Convert date into UTC datetime.
      /// </summary>
      /// <param name="datetime"></param>
      /// <param name="hour"></param>
      /// <param name="minute"></param>
      /// <returns></returns>
      ///****************************************************************************
      public DateTime? CreateUTCDateTime(string timezone, DateTime? datetime, int hour, int minute, string Meridieum)
      {
         if (datetime == null)
            return null;

         return SchedulerFunctions.ConvertTimeZoneDateTimeToUTC(timezone, datetime.Value, hour, minute, Meridieum);
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind Report Status Grid
      /// </summary>
      /// <param name="userId"></param>
      /// <param name="ReportScheduleID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ReportSummary> GetReportRunDetails(long userId, long ReportScheduleID)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var reportDetails = (from report in cloudModelEntities.Report
                                 join reportSchedule in cloudModelEntities.ReportSchedule on
                                    report.ReportID equals reportSchedule.ReportID
                                 join reportRun in cloudModelEntities.ReportRun on
                                    reportSchedule.ReportScheduleID equals reportRun.ReportScheduleID
                                 where reportSchedule.UserID == userId && reportSchedule.ReportScheduleID == ReportScheduleID
                                 select new ReportSummary()
                                 {
                                    ReportId = report.ReportID,
                                    ReportTitle = report.ReportTitle == "Sample_Report_Retailer_Product_Detail" ? "Product Details" : report.ReportTitle,
                                    Status = reportRun.Status,
                                    StatusDescription = reportRun.StatusDescription,
                                    RunTime = reportRun.RunTime.ToString(),
                                    BlobFile = reportRun.BlobFile
                                 }).ToList();

            return reportDetails;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set data on Report Scheduler Grid
      /// </summary>
      /// <param name="userId"></param>
      /// <param name="reportId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ReportScheduleInformation> GetReportSchedulerDetails(long userId, int reportId)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var reportDetails = (from reportSchedule in cloudModelEntities.ReportSchedule
                                 where reportSchedule.ReportID == reportId && reportSchedule.UserID == userId && !reportSchedule.IsDeleted
                                 orderby reportSchedule.ModifiedDate descending
                                 select new ReportScheduleInformation()
                                 {
                                    ReportScheduleID = reportSchedule.ReportScheduleID,
                                    ReportID = reportSchedule.ReportID,
                                    UserID = reportSchedule.UserID,
                                    ReportScheduleTitle = reportSchedule.ReportScheduleTitle,
                                    Schedule = reportSchedule.Schedule,
                                    Criteria = reportSchedule.Criteria
                                 }).ToList();


            var specialCols = new List<string>
            {
               Utility.Constants.Report_ProductDetail_Parameter_StoreNumber,
               Utility.Constants.Report_Parameter_DataStartDate,
               Utility.Constants.Report_Parameter_DataEndDate
            };

            if (reportDetails != null)
            {
               foreach (var item in reportDetails)
               {
                  Dictionary<string, object> tempCriteria = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Criteria);
                  tempCriteria = tempCriteria.Where(x => specialCols.Contains(x.Key)).Select(x => new {x.Key, x.Value}).ToDictionary(t => t.Key, t => t.Value);
                  item.DateRange = FormattedDateRange(tempCriteria);
                  item.StoreNumber =
                     tempCriteria[Utility.Constants.Report_ProductDetail_Parameter_StoreNumber] != null
                        ? tempCriteria[Utility.Constants.Report_ProductDetail_Parameter_StoreNumber].ToString()
                        : "";
                  item.Schedule = FormattedSchedule(item.Schedule);
                  item.Criteria = FormattedCriteria(item.Criteria);
               }
            }

            return reportDetails;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Function for showing Formatted Schedule.
      /// </summary>
      /// <param name="Schedule"></param>
      /// <returns></returns>
      public string FormattedSchedule(string Schedule)
      {
         string text = string.Empty;
         string scheduledType = string.Empty;
         ReportScheduleDetailViewModel objReportScheduleDetailViewModel = JsonConvert.DeserializeObject<ReportScheduleDetailViewModel>(Schedule);

         if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Hour)
         {
            scheduledType = Constants.FormattedSchedule_Hour;
         }
         else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Day)
         {
            scheduledType = Constants.FormattedSchedule_Day;
         }
         else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Week)
         {
            scheduledType = Constants.FormattedSchedule_Week;
         }
         else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Month)
         {
            scheduledType = Constants.FormattedSchedule_Month;
         }

         objReportScheduleDetailViewModel.StartDate = objReportScheduleDetailViewModel.StartDate != null ? SchedulerFunctions.ConvertUTCToTimeZoneDate(objReportScheduleDetailViewModel.ReportScheduleTimeZone, objReportScheduleDetailViewModel.StartDate).Value : objReportScheduleDetailViewModel.StartDate;
         SetTimeAndMeridieum(objReportScheduleDetailViewModel);

         text = Constants.FormattedSchedule_Every + objReportScheduleDetailViewModel.Every + Constants.FormattedSchedule_Space + scheduledType + (objReportScheduleDetailViewModel.Every == 1 ? Constants.FormattedSchedule_Space : Constants.FormattedSchedule_s) + Constants.FormattedSchedule_At + objReportScheduleDetailViewModel.Hour + Constants.FormattedSchedule_Colon + objReportScheduleDetailViewModel.Minute.ToString().PadLeft(2, '0') + Constants.FormattedSchedule_Space + objReportScheduleDetailViewModel.Meridieum;

         return text;
      }

      ///****************************************************************************
      /// <summary>
      ///   Function for showing single and multiple criteria.
      /// </summary>
      /// <param name="Criteria"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string FormattedCriteria(string Criteria)
      {
         var criteria = string.Empty;
         int count = 0;

         try
         {
            Dictionary<string, object> parameterList = JsonConvert.DeserializeObject<Dictionary<string, object>>(Criteria);
            foreach (KeyValuePair<string, object> parameter in parameterList)
            {
               if (parameter.Value != null && parameter.Key != Constants.Report_Parameter_ReportScheduleTimeZone && parameter.Key != Constants.Report_Parameter_ReportDataTimeZone && parameter.Key != Constants.FormattedSchedule_QoskID && parameter.Key != Constants.Report_Parameter_FileType)
               {
                  criteria = parameter.Key.ToString().Replace(Constants.Report_Parameter_Initial, string.Empty) + Constants.FormattedSchedule_Colon + parameter.Value;
                  count = count + 1;
               }
            }

            if (count > 1)
            {
               criteria = Constants.FormattedSchedule_MultipleCriteria;
            }

            return criteria;
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Function for showing date range criteria fields
      /// </summary>
      /// <param name="Criteria"></param>
      /// <returns></returns>
      ///****************************************************************************
      private string FormattedDateRange(Dictionary<string, object> Criteria)
      {
         try
         {
            var criteria = string.Empty;
            var startDate = Criteria[Utility.Constants.Report_Parameter_DataStartDate];
            var endDate = Criteria[Utility.Constants.Report_Parameter_DataEndDate];

            if (startDate == null && endDate == null)
            {
               return criteria;
            }
            
            if (startDate != null && endDate == null)
            {
               criteria = "Processed On " + Convert.ToDateTime(startDate).ToShortDateString() + " Or Later";
            }
            else if (startDate == null && endDate != null)
            {
               criteria = "Processed On Or Before " + Convert.ToDateTime(endDate).ToShortDateString();
            }
            else
            {
               if (Convert.ToDateTime(startDate) == Convert.ToDateTime(endDate))
               {
                  criteria = "Processed On " + Convert.ToDateTime(startDate).ToShortDateString();
               }
               else if (Convert.ToDateTime(startDate) > Convert.ToDateTime(endDate))
               {
                  criteria = "Processed Between " + Convert.ToDateTime(endDate).ToShortDateString() + " And " + Convert.ToDateTime(startDate).ToShortDateString();
               }
               else
               {
                  criteria = "Processed Between " + Convert.ToDateTime(startDate).ToShortDateString() + " And " + Convert.ToDateTime(endDate).ToShortDateString();
               }
            }

            return criteria;
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete records from Report Scheduler Grid
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal Response DeleteReportSchedule(int reportScheduleId)
      {
         Response objclsResponse = new Response();
         try
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {

               ReportSchedule reportScheduleItem = (from reportSchedule in cloudModelEntities.ReportSchedule
                                                    where reportSchedule.ReportScheduleID == reportScheduleId
                                                    select reportSchedule).SingleOrDefault();

               if (reportScheduleItem != null)
               {
                  reportScheduleItem.IsDeleted = true;

                  var status = cloudModelEntities.SaveChanges();

                  if (status > 0)
                  {
                     objclsResponse.Status = Status.OK;
                  }
                  else
                  {
                     objclsResponse.Status = Status.Fail;
                  }

               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            objclsResponse.Status = Status.Fail;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Set data on Edit UI
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ReportScheduleDetailViewModel GetReportScheduleInformation(int reportScheduleId, long userId, string timeZone)
      {
         ReportScheduleDetailViewModel retailerBoxDetailReportViewModel = null;

         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            if (reportScheduleId == 0)
            {
               var untitled = (from reportSchedule in cloudModelEntities.ReportSchedule
                               where reportSchedule.UserID == userId && reportSchedule.ReportScheduleTitle.Contains("Untitled")
                               select reportSchedule.ReportScheduleTitle).ToList();
               var title = "";
               var count = 1;

               while (title == "")
               {
                  var possibleTitle = "Untitled " + count;

                  if (untitled.Contains(possibleTitle))
                  {
                     count++;
                  }
                  else
                  {
                     title = possibleTitle;
                  }
               }

               var timeZoneFormatted = !string.IsNullOrEmpty(timeZone)
                  ? SchedulerFunctions.GetTimeZoneFromJs(timeZone)
                  : SchedulerFunctions.GetAllTimeZones().Keys.ToList()[0];

               retailerBoxDetailReportViewModel = (from user in cloudModelEntities.User
                                                   where user.UserID == userId
                                                   select new ReportScheduleDetailViewModel
                                                   {
                                                      ScheduleReportTitle = title,
                                                      FileType = FileType.PDF,
                                                      Every = 1,
                                                      ScheduleType = ScheduleType.Month,
                                                      StartDate = DateTime.Today,
                                                      ReportScheduleTimeZone = timeZoneFormatted,
                                                      ReportDataTimeZone = timeZoneFormatted,
                                                      EmailTo = user.Email,
                                                      Hour = 1
                                                   }).SingleOrDefault();
            }
            else
            {
               var reportScheduleDetail = (from reportSchedule in cloudModelEntities.ReportSchedule
                                           where reportSchedule.ReportScheduleID == reportScheduleId
                                           select reportSchedule).FirstOrDefault();

               if (reportScheduleDetail != null)
               {
                  ReportSchedulerEntity objReportSchedulerEntity =
                     JsonConvert.DeserializeObject<ReportSchedulerEntity>(reportScheduleDetail.Schedule);
                  RetailerProductDetailsEntity objRetailerProductDetailsEntity =
                     JsonConvert.DeserializeObject<RetailerProductDetailsEntity>(reportScheduleDetail.Criteria);

                  if (objReportSchedulerEntity != null && objRetailerProductDetailsEntity != null)
                  {
                     retailerBoxDetailReportViewModel = new ReportScheduleDetailViewModel()
                     {
                        ScheduleType = objReportSchedulerEntity.ScheduleType,
                        FileType = objReportSchedulerEntity.FileType,
                        ReportID = reportScheduleDetail.ReportID,
                        ReportScheduleID = reportScheduleDetail.ReportScheduleID,
                        Every = objReportSchedulerEntity.Every,
                        SMSTo = objReportSchedulerEntity.SMSTo,
                        StartDate =
                           objReportSchedulerEntity.StartDate != null
                              ? SchedulerFunctions.ConvertUTCToTimeZoneDate(
                                 objReportSchedulerEntity.ReportScheduleTimeZone, objReportSchedulerEntity.StartDate)
                                 .Value
                              : objReportSchedulerEntity.StartDate,
                        EndDate =
                           objReportSchedulerEntity.EndDate != null
                              ? SchedulerFunctions.ConvertUTCToTimeZoneDate(
                                 objReportSchedulerEntity.ReportScheduleTimeZone, objReportSchedulerEntity.EndDate)
                                 .Value
                              : objReportSchedulerEntity.EndDate,
                        ReportScheduleTimeZone = objReportSchedulerEntity.ReportScheduleTimeZone,
                        ReportDataTimeZone = objRetailerProductDetailsEntity.ReportDataTimeZone,
                        ScheduleReportTitle = reportScheduleDetail.ReportScheduleTitle,
                        EmailTo = GetEmailAddress((from reportRecipient in cloudModelEntities.ReportRecipient
                                                   where reportRecipient.ReportScheduleId == reportScheduleId
                                                   select reportRecipient.RecipientEmail).Distinct().ToList()),
                        Hour = objReportSchedulerEntity.Hour,
                        Minute = objReportSchedulerEntity.Minute,
                        Meridieum = objReportSchedulerEntity.Meridieum
                     };

                     this.SetTimeAndMeridieum(retailerBoxDetailReportViewModel);
                  }
               }
            }

            return retailerBoxDetailReportViewModel;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set email address in Edit UI
      /// </summary>
      /// <param name="emailList"></param>
      /// <returns></returns>
      ///****************************************************************************
      public string GetEmailAddress(List<string> emailList)
      {
         var emailAddress = string.Empty;

         if (emailList != null && emailList.Count > 0)
         {
            emailAddress = string.Join(Constants.Model_ReportDataAccess_Comma, emailList);
         }

         return emailAddress;
      }

      ///****************************************************************************
      /// <summary>
      ///   Set AM, PM and date on Edit UI
      /// </summary>
      /// <param name="retailerBoxDetailReportViewModel"></param>
      ///****************************************************************************
      public void SetTimeAndMeridieum(ReportScheduleDetailViewModel retailerBoxDetailReportViewModel)
      {
         if (retailerBoxDetailReportViewModel != null && retailerBoxDetailReportViewModel.StartDate != null)
         {
            var hour = retailerBoxDetailReportViewModel.StartDate.Value.Hour;

            if (hour >= 12)
            {
               if (hour > 12)
               {
                  hour = hour - 12;
               }
               retailerBoxDetailReportViewModel.Meridieum = Meridieum.PM;
            }
            else
            {
               if (hour == 0)
               {
                  hour = 12;
               }
               retailerBoxDetailReportViewModel.Meridieum = Meridieum.AM;
            }

            retailerBoxDetailReportViewModel.Hour = hour;
            retailerBoxDetailReportViewModel.Minute = retailerBoxDetailReportViewModel.StartDate.Value.Minute;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Report Name from Report table.
      /// </summary>
      /// <param name="reportID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public string GetReportName(long reportID)
      {
         var name = string.Empty;

         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var reportName = (from report in cloudModelEntities.Report
                              where report.ReportID == reportID && report.IsDeleted == false
                              select report.ReportTitle).FirstOrDefault();
            name = reportName == "Sample_Report_Retailer_Product_Detail" ? "Product Details" : reportName;
         }

         return name;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Report Module from Report table.
      /// </summary>
      /// <param name="reportID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public string GetReportModule(long reportID)
      {
         var ReportModule = string.Empty;

         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var Module = (from report in cloudModelEntities.Report
                          where report.ReportID == reportID
                          select report.ReportModule).FirstOrDefault();

            ReportModule = Module;
         }

         return ReportModule;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get layout id based on report name.
      /// </summary>
      /// <param name="reportName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ReportMasterLayoutList> GetPanelLayoutId(string reportName)
      {

         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var query = (from reportLayoutTableMapping in cloudModelEntities.ReportLayoutTableMapping
                         join reportMasterTableList in cloudModelEntities.ReportMasterTableList on
                            reportLayoutTableMapping.TableId equals reportMasterTableList.ReportMasterTableListID
                         join reportMasterLayoutList in cloudModelEntities.ReportMasterLayoutList on
                            reportLayoutTableMapping.LayoutId equals reportMasterLayoutList.ReportMasterLayoutListID

                         where reportMasterTableList.TableName == (reportName == "Product Details" ? "Sample_Report_Retailer_Product_Detail" : reportName)
                         select reportMasterLayoutList);
            return query.ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Report Criteria based on reportScheduleId.
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public string GetReportCriteria(int reportScheduleId)
      {
         using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var reportCriteria = (from reportSchedule in cloudModelEntities.ReportSchedule
                                  where reportSchedule.ReportScheduleID == reportScheduleId
                                  select reportSchedule.Criteria).FirstOrDefault();

            return reportCriteria;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Details Auto Complete Based On ControlID
      /// </summary>
      /// <param name="terms"></param>
      /// <param name="controlID"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal ICollection<AutoCompleteEntity> GetDetailsUsingAutoCompleteBasedOnControlID(string terms, string controlID)
      {
         ICollection<AutoCompleteEntity> lstAutoCompleteDetails = null;

         try
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               switch (controlID)
               {
                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_VendorName:
                     {
                        lstAutoCompleteDetails = (from profile in cloudModelEntities.Profile.Where(p => p.Name.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = profile.ProfileCode.ToString(),
                                                     Text = profile.Name,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;

                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_NDCNumber:
                     {
                        lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.NDC.ToString().Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = product.ProductID.ToString(),
                                                     Text = product.NDC.ToString(),
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_ProductDescription:
                     {
                        lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.Description.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = product.ProductID.ToString(),
                                                     Text = product.Description,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_LotNumber:
                     {
                        lstAutoCompleteDetails = (from lotNumber in cloudModelEntities.Lot.Where(p => p.LotNumber.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = lotNumber.LotNumberID.ToString(),
                                                     Text = lotNumber.LotNumber,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_StoreName:
                     {
                        lstAutoCompleteDetails = (from item in cloudModelEntities.Item
                                                  join qosk in cloudModelEntities.Qosk
                                                     on item.QoskID equals qosk.QoskID

                                                  join qoskprofile in cloudModelEntities.Profile
                                                     on qosk.ProfileCode equals qoskprofile.ProfileCode
                                                  where qoskprofile.Name.Contains(terms)
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = qoskprofile.ProfileCode.ToString(),
                                                     Text = qoskprofile.Name,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_OutofPolicyCode:
                     {
                        lstAutoCompleteDetails = (from item in cloudModelEntities.Item.Where(p => p.NonReturnableReason.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = item.ItemGUID.ToString(),
                                                     Text = item.NonReturnableReason,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_OutofpolicyCodeDescription:
                     {
                        lstAutoCompleteDetails = (from item in cloudModelEntities.Item.Where(p => p.NonReturnableReasonDetail.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = item.ItemGUID.ToString(),
                                                     Text = item.NonReturnableReasonDetail,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_Description:
                     {
                        lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.Description.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = product.ProductID.ToString(),
                                                     Text = product.Description,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_SealedOpenCase:
                     {
                        lstAutoCompleteDetails = (from itemStateDict in cloudModelEntities.ItemStateDict.Where(p => p.Description.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = itemStateDict.Code,
                                                     Text = itemStateDict.Description,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }

                  // ...
                  //=============================================================================
                  case Constants.GetDetails_AutoComplete_ManufacturerName:
                     {
                        lstAutoCompleteDetails = (from profile in cloudModelEntities.Profile.Where(p => p.Name.Contains(terms))
                                                  select new AutoCompleteEntity
                                                  {
                                                     Value = profile.ProfileCode.ToString(),
                                                     Text = profile.Name,
                                                  }).Distinct().Take(15).ToList();

                        return lstAutoCompleteDetails;
                     }
               }
            }

            return lstAutoCompleteDetails;
         }
         catch
         {
            return lstAutoCompleteDetails;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionMenuModel GetActionMenu()
      {
         var actionMenu = new ActionMenuModel
         {
            Name = "Change Report",
            Links = new List<ActionMenuEntry>()
            {
               new ActionMenuEntry
               {
                  Name = "Product Details",
                  Url = "/Report/Home/Id/13"
               },
               new ActionMenuEntry
               {
                  Name = "Waste Summary",
                  Url = "/Report/Home/Id/14"
               },
               new ActionMenuEntry
               {
                  Name = "Pharmacy Summary",
                  Url = "/Report/Home/Id/16"
               },
               new ActionMenuEntry
               {
                  Name = "Non Creditable Summary",
                  Url = "/Report/Home/Id/17"
               },
               new ActionMenuEntry
               {
                  Name = "Manufacturer Summary",
                  Url = "/Report/Home/Id/18"
               },
               new ActionMenuEntry
               {
                  Name = "Return To Stock",
                  Url = "/Report/Home/Id/19"
               }
            }
         };

         return actionMenu;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <param name="isDownload"></param>
      /// <param name="userId"></param>
      ///****************************************************************************
      public void RecordPreviewOrDownload(int reportScheduleId, bool isDownload, long userId)
      {
         if (reportScheduleId > 0)
         {
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var userName = (from user in cloudModelEntities.User
                               where user.UserID == userId
                               select user.UserName).SingleOrDefault();

               // Update Report Schedule Table.
               var reportScheduleEntity = (from report in cloudModelEntities.Report
                                           join reportSchedule in cloudModelEntities.ReportSchedule on report.ReportID equals reportSchedule.ReportID
                                           where reportSchedule.ReportScheduleID == reportScheduleId
                                           select reportSchedule).FirstOrDefault();

               if (reportScheduleEntity != null)
               {
                  var reportRun = new ReportRun
                  {
                     ReportRunGuid = Guid.NewGuid(),
                     ReportScheduleID = reportScheduleId,
                     RunTime = DateTime.UtcNow,
                     Status = isDownload ? "Downloaded" : "Previewed",
                     StatusDescription = "Report " + (isDownload ? "Downloaded" : "Previewed") + (userName != null ? " by " + userName : "")
                  };

                  cloudModelEntities.ReportRun.Add(reportRun);
                  cloudModelEntities.SaveChanges();
               }
            }

         }
      }

   }
}