﻿using Qualanex.QoskCloud.Utility.Common;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility
{
    /// <summary>
    /// Helping communication to Azure
    /// </summary>
    public class AzureCommonUtility
    {
        /// <summary>
        /// DownloadImagesVideosFromAzure: Get all blobs based on search criteria from Azure directory.
        /// </summary>
        /// <param name="itemGuid">Item id through which search withing the container</param>

        /// <returns></returns>
        public static List<SasUriModel> DownloadImagesVideosFromAzure(string itemGuid)
        {
            try
            {
                List<SasUriModel> sasURIList = new List<SasUriModel>();
                List<SasUriModel> Images_sasURIList = BlobResource.GetFolderItemsListSASUrlBasedOnSearchCriteria(Utility.Constants.AzureAccount_Image, Utility.Constants.AzureContainer_Images, Utility.Constants.ContentType_Images, itemGuid);
                List<SasUriModel> Videos_sasURIList = BlobResource.GetFolderItemsListSASUrlBasedOnSearchCriteria(Utility.Constants.AzureAccount_Video, Utility.Constants.AzureContainer_Videos, Utility.Constants.ContentType_Videos, itemGuid);
                sasURIList.AddRange(Images_sasURIList);
                sasURIList.AddRange(Videos_sasURIList);
                return sasURIList;
            }
            catch
            {
                throw;
            }
        }
    }
}