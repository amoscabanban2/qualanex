﻿using System;
using Qualanex.QoskCloud.Utility;
using System.ComponentModel.DataAnnotations;

namespace Qualanex.QoskCloud.Web.Areas.Report.ViewModel
{
    /// <summary>
    ///  ReportScheduleDetailViewModel: This entity is the view model for Report Schedule UI.
    /// </summary>
    public class ReportScheduleDetailViewModel
    {
        public long ReportScheduleID { get; set; }
        public long? ReportID { get; set; }
        public long UserID { get; set; }
        public FileType FileType { get; set; }
        public ScheduleType ScheduleType { get; set; }
        public string ReportScheduleTimeZone { get; set; }
        public string ReportDataTimeZone { get; set; }
        public string ScheduleReportTitle { get; set; }

        [Range(1, 99, ErrorMessage = "Can only be between 0 .. 12")]
        public double Every { get; set; }
        public Meridieum Meridieum { get; set; }

        [Range(1, 12, ErrorMessage = "Can only be between 0-12")]
        public int Hour { get; set; }
        public int Minute { get; set; }
        public string EmailTo { get; set; }
        public string SMSTo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }

}