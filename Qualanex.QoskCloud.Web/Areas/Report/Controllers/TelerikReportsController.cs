namespace Qualanex.QoskCloud.Web.Areas.Report.Controllers
{
    using Utility;
    using System.IO;
    using System.Web;
    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.Engine;
    using Telerik.Reporting.Services.WebApi;

    //The class name determines the service URL. 
    //ReportsController class name defines /api/report/ service URL.
    //[System.CLSCompliant(false)]
    public class TelerikReportsController : ReportsControllerBase
    {
        static ReportServiceConfiguration preservedConfiguration;

        static IReportServiceConfiguration PreservedConfiguration
        {
            get
            {
                if (null == preservedConfiguration)
                {
                    preservedConfiguration = new ReportServiceConfiguration
                    {
                        HostAppId = Constants.TelerikReportsController_MvcDemoApp,
                        Storage = new Telerik.Reporting.Cache.File.FileStorage(),
                        ReportResolver = CreateResolver(),
                    };
                }
                return preservedConfiguration;
            }
        }

        public TelerikReportsController()
        {
            this.ReportServiceConfiguration = PreservedConfiguration;
        }
        static IReportResolver CreateResolver()
        {
            var appPath = HttpContext.Current.Server.MapPath(Constants.TelerikReportsController_MapPath);
            var reportsPath = Path.Combine(appPath, Constants.TelerikReportsController_TelerikReports);

            return new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());
        }
    }
}