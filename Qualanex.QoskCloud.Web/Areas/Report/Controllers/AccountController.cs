﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Qualanex.QoskCloud.Web.Areas.Report.Models;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;
using System;
using System.Linq;
namespace Qualanex.QoskCloud.Web.Areas.Report.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }
        /// <summary>
        /// AccountController
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        /// <summary>
        /// SignInManager
        /// </summary>
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        /// <summary>
        /// UserManager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            LoginViewModel objLogin = new LoginViewModel();
            if (Request.Cookies[Utility.Constants.UserController_EnteredUserName] != null)
            {
                objLogin.UserName = Request.Cookies[Utility.Constants.UserController_EnteredUserName].Value;
                objLogin.RememberMe = true;
                string username = Request.Cookies[Utility.Constants.UserController_EnteredUserName].Value;
                MD5Encryption objEncrypt = new MD5Encryption();
                objLogin.UserName = objEncrypt.Decrypt(username);
                objLogin.RememberMe = true;
            }
            else
            {
                objLogin.RememberMe = false;
            }

            ViewBag.ReturnUrl = returnUrl;

            //Uncomment below code at the time of deployment (comment when debug locally).
            #region 1.Uncomment below code at the time of deployment (comment when debug locally)
            return new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl));
            #endregion

            //Uncomment below code to debug locally (comment when deploy).
            #region 2.Uncomment below code to debug locally (comment when deploy).
            //return View("Login", objLogin);
            #endregion
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            // var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:                
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }


        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        /// <summary>
        /// Get Logout screen and clear current cache 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Logout()
        {

            //Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            System.Web.HttpContext.Current.Cache.Remove(Utility.Constants.ManageLayoutConfig_UserDataWithLink);
            //signout from report web library website two
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);            
            return new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl));

        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ValidateUser(string returnUrl)
        {
            MD5Encryption objEncrypt = null;
            UsersDataAccess objUsersDataAccess = null;
            UserRegistrationRequest userreturnData = null;
            ReturnLoginUserEntity objReturnLoginUserData = null;

            try
            {
                objReturnLoginUserData = new ReturnLoginUserEntity();
                var result = User.Identity.IsAuthenticated;
                if (result)
                {
                    objUsersDataAccess = new UsersDataAccess();
                    objEncrypt = new MD5Encryption();
                    userreturnData = objUsersDataAccess.ValidateUserLogin(User.Identity.Name);
                    if (userreturnData != null)
                    {

                        List<LeftMenuLinks> subLinksList = (List<LeftMenuLinks>)objUsersDataAccess.GetLeftLinksData(1);
                        objReturnLoginUserData.UserReturnData = userreturnData;
                        objReturnLoginUserData.UserLeftLinks = subLinksList;
                        objReturnLoginUserData.AdminApplicationUrl = Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl);
                        objReturnLoginUserData.ReportsNameLinks = ReportDataAccess.GetAllReport().ToList();

                        if (objReturnLoginUserData.UserReturnData != null)
                        {
                            var userdata = objReturnLoginUserData.UserReturnData;

                            bool IsTemportPassword = objReturnLoginUserData.UserReturnData.IsTemporaryPassword;
                            if (IsTemportPassword)
                            {
                                userdata.IsTemporaryPassword = true;
                                return Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);

                            }
                            else
                            {
                                return Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);

                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(Utility.Constants.UserController_Password, Utility.Constants.UserController_IncorrectCredential);
                        return Json(objReturnLoginUserData, JsonRequestBehavior.AllowGet);
                    }
                    return RedirectToLocal(returnUrl);

                }
                return new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl));
            }
            catch (Exception)
            {
                return new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Utility.Constants.AdminWebsiteUrl));
            }
            finally
            {
                objUsersDataAccess = null;
                objEncrypt = null;
            }

        }
        /// <summary>
        /// RedirectToLocal
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}