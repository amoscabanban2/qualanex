﻿namespace Qualanex.QoskCloud.Web.Areas.Report.Controllers
{
   using System;
   using System.Collections;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web;
   using System.Web.Mvc;

   using Microsoft.AspNet.Identity;
   using Newtonsoft.Json;
   using Telerik.Reporting;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility.Common;
   using Qualanex.QoskCloud.Web.Areas.Report.Models;
   using Qualanex.QoskCloud.Web.Areas.Report.Models.AzureUtility;
   using Qualanex.QoskCloud.Web.Areas.Report.ReportClasses;
   using Qualanex.QoskCloud.Web.Resources;
   using Qualanex.QoskCloud.Web.Areas.Report.Retailer;
   using Qualanex.QoskCloud.Web.Areas.Report.ViewModel;

   using Constants = Qualanex.QoskCloud.Utility.Constants;
   using Logger = Qualanex.QoskCloud.Utility.Logger;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [QoskAuthorize]
   public class HomeController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   First action method which execute when navigated from Admin Website.
      /// </summary>
      /// <param name="reportId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Index(string reportId)
      {
         this.ViewBag.ReportId = reportId + this.Request.Url.Query;

         return this.View();
      }

      // GET: Report
      public static string ReportName = string.Empty;

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ReportId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Id(string ReportId)
      {
         try
         {
            if (string.IsNullOrEmpty(ReportId))
            {
               return this.View(Utility.Constants.Report_ReportPreview);
            }

            this.ViewBag.ReportId = ReportId;
            var reportDataAccess = new ReportDataAccess();
            ReportName = reportDataAccess.GetReportName(Convert.ToInt64(ReportId));

            if (string.IsNullOrEmpty(ReportName))
            {
               return this.View(Utility.Constants.Report_ReportPreview);
            }

            var reportMasterLayoutList = reportDataAccess.GetPanelLayoutId(ReportName);
            var SearchPanellayoutID = reportMasterLayoutList.FirstOrDefault(x => x.SectionType == Constants.GetSearchPanelLayoutId_SearchPanel).ReportMasterLayoutListID;
            var ListPanellayoutID = reportMasterLayoutList.FirstOrDefault(x => x.SectionType == Constants.GetListPanelLayoutId_ListPanel).ReportMasterLayoutListID;
            var ReportModule = reportDataAccess.GetReportModule(Convert.ToInt64(ReportId));

            this.ViewBag.ReportName = ReportName;
            this.ViewBag.ReportModule = ReportModule;
            this.ViewBag.SearchPanellayoutID = SearchPanellayoutID;
            this.ViewBag.ListPanellayoutID = ListPanellayoutID;

            return this.View(reportDataAccess.GetActionMenu());
         }
         catch (Exception ex)
         {
            Logger.Error("Error", ex);
            return this.View(Utility.Constants.Report_ReportPreview);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult Sample_Report_Retailer_Box_Detail(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var NDC = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource
            {
               TypeName = typeof(Sample_Report_Retailer_Box_Detail).AssemblyQualifiedName
            };

            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));

            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }

            this.ViewBag.ReportSource = Source;
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportRetailerBoxDetails();
            var listReportPreview = new List<RetailerBoxDetailsEntity>();
            NDCNumber = string.IsNullOrEmpty(NDCNumber) ? "0" : NDCNumber;
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetRetailer_Box_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerBoxDetail)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="fileType"></param>
      ///****************************************************************************
      [NonAction]
      public void DownloadReportOnBrowser(TypeReportSource source, string fileType = Utility.Constants.Report_Using_FileType_pdf)
      {
         try
         {
            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            var result = reportProcessor.RenderReport(fileType, source, null);
            var FileName = result.DocumentName + Utility.Constants.Report_Extension_Dot + result.Extension;
            this.Response.Clear();
            this.Response.ContentType = result.MimeType;
            this.Response.Cache.SetCacheability(HttpCacheability.Private);
            this.Response.Expires = -1;
            this.Response.Buffer = true;
            // Uncomment to handle the file as attachment
            this.Response.AddHeader(Utility.Constants.ResponseHeader_Content_Disposition,
                           string.Format(Utility.Constants.ResponseHeader_FileName_Format,
                                           Utility.Constants.ResponseHeader_Attachment,
                                           FileName));
            this.Response.BinaryWrite(result.DocumentBytes);
            this.Response.End();
         }
         catch
         {
            ;
            ;
            ;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_RetailerBoxSummary)]
      public ActionResult Sample_Reports_Retailer_Box_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var boxNumber = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Sample_Reports_Retailer_Box_Summary).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportRetailerBoxSummary();
            var listReportPreview = new List<RetailerBoxSummaryEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetRetailer_Box_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerBoxSummary)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_RetailerMFGSummary)]
      public ActionResult Retailer_MGF_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var boxNumber = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Retailer_MFG_Summary).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportRetailerMFGSummary();
            var listReportPreview = new List<RetailerMFGSummaryEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetRetailer_MFG_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerMFGSummary)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_RetailerMFGCreditSummary)]
      public ActionResult Retailer_MFG_Summary_Credits(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var boxNumber = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Retailer_MFG_Summary_Credits).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportRetailerMFGSummaryCredit();
            var listReportPreview = new List<RetailerMFGSummaryCreditsEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetRetailer_MFG_Credits_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerMFGSummaryCredits)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_RetailerReturnToStock)]
      public ActionResult Retailer_Return_To_Stock_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var NDC = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Retailer_Return_To_Stock_Details).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportRetailerReturnToStockDetailsEntity();
            var listReportPreview = new List<RetailerReturnToStockDetailsEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetRetailer_Return_Stock_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerReturnToStockDetails)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_RetailerReturnToStockSummary)]
      public ActionResult Retailer_Return_To_Stock_Summary()
      {
         var Source = new TypeReportSource();
         Source.TypeName = typeof(Retailer_Return_To_Stock_Summary).AssemblyQualifiedName;
         this.ViewBag.ReportSource = Source;
         return this.View(Utility.Constants.Report_ReportRender);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_WholesalerBoxDetail)]
      public ActionResult Sample_Report_Wholesaler_Box_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var NDC = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Sample_Reports_Wholesaler_Box_Detail).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportWholesalerBoxDetails();
            var listReportPreview = new List<WholesalerBoxDetailsEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetWholesaler_Box_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerBoxDetails)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_WholesalerBoxSummary)]
      public ActionResult Sample_Report_Wholesaler_Box_Details_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var NDC = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Sample_Reports_Wholesaler_Box_Summary).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportWholesalerBoxSummary();
            var listReportPreview = new List<WholesalerBoxSummaryEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetWholesaler_Box_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerBoxSummary)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_WholesalerMFGCreditSummary)]
      public ActionResult Wholesaler_MFG_Summary_Credits(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var boxNumber = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Wholesaler_MFG_Summary_Credits).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportWholesalerMFGSummaryCredits();
            var listReportPreview = new List<WholesalerMFGSummaryCreditsEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetWholesaler_MFG_Credits_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerMFGSummaryCredits)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_WholesalerMFGSummary)]
      public ActionResult Wholesaler_MGF_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var boxNumber = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Wholesaler_MFG_Summary).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportWholesalerMFGSummary();
            var listReportPreview = new List<WholesalerMFGSummaryEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetWholesaler_MFG_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerMFGSummary)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="NDCNumber"></param>
      /// <param name="ReportDataTimeZone"></param>
      /// <param name="ReportScheduleTimeZone"></param>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_WholesalerReturnToStock)]
      public ActionResult Wholesaler_Return_To_Stock_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
      {
         if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
         {
            var NDC = Convert.ToInt64(NDCNumber);
            var Source = new TypeReportSource();
            Source.TypeName = typeof(Wholesaler_Return_To_Stock_Details).AssemblyQualifiedName;
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
            Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
            if (Isdownload)
            {
               this.DownloadReportOnBrowser(Source, FileType);
               return null;
            }
            else
            {
               this.ViewBag.ReportSource = Source;
            }
            return this.View(Utility.Constants.Report_ReportRender);
         }
         else
         {
            var reportData = new ReportWholesalerReturnToStockDetails();
            var listReportPreview = new List<WholesalerReturnToStockDetailsEntity>();
            using (var reportDataContext = new ReportDataContext())
            {
               listReportPreview = reportDataContext.GetWholesaler_Return_Stock_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
            }
            List<GridColumns> gcList = null;
            using (var r = new System.IO.StreamReader(this.Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerReturnToStockDetails)))
            {
               var json = r.ReadToEnd();
               gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
            }
            reportData.reportRetailerBoxDetails = listReportPreview;
            reportData.gridColumns = gcList;
            return this.Json(reportData, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Binding the dropdowns
      /// </summary>
      ///****************************************************************************
      [NonAction]
      public void CreateCommonData()
      {
         this.ViewBag.FileTypeList = this.GetFileTypeList();
         this.ViewBag.HourList = this.GetHourList();
         this.ViewBag.EveryList = this.GetEveryList();
         this.ViewBag.ReportTimeZoneList = this.GetReportTimeZoneList();
         this.ViewBag.ScheduleTypeList = this.GetScheduleTypeList();
         this.ViewBag.MinuteList = this.GetMinuteList();
         this.ViewBag.MeridieumList = this.GetMeridieumList();
      }

      ///****************************************************************************
      /// <summary>
      ///   Edit record from Report Scheduler Grid
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <param name="timeZoneOffset"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult EditReportScheduler(int reportScheduleId, string timeZone)
      {
         var userId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         var reportDataAccess = new ReportDataAccess();
         var retailerBoxDetailReportViewModel = new ReportScheduleDetailViewModel();
         retailerBoxDetailReportViewModel = reportDataAccess.GetReportScheduleInformation(reportScheduleId, userId, timeZone);
         this.CreateCommonData();
         this.ViewBag.ReportScheduleId = reportScheduleId;
         return this.PartialView(Utility.Constants.Report_ReportScheduler, retailerBoxDetailReportViewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind Report Scheduler Grid
      /// </summary>
      /// <param name="reportId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult BindReportSchedulerGrid(int reportId)
      {
         var listReportSummary = new List<ReportScheduleInformation>();
         if (reportId != 0)
         {
            var userId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            listReportSummary = reportDataAccess.GetReportSchedulerDetails(userId, reportId);
         }

         return this.Json(listReportSummary, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind report status grid
      /// </summary>
      /// <param name="ReportScheduleID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult BindReportRunGrid(long ReportScheduleID, string timeZone)
      {
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         var reportDataAccess = new ReportDataAccess();
         var listReportSummary = new List<ReportSummary>();
         listReportSummary = reportDataAccess.GetReportRunDetails(UserId, ReportScheduleID);
         foreach (var reportSummary in listReportSummary)
         {
            DateTime runTime;
            if (DateTime.TryParse(reportSummary.RunTime, out runTime))
            {
               DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone, runTime) ?? runTime;
               reportSummary.RunTime = "<span hidden>" + time.Value.ToString("s") + "</span>" + time.Value.ToString("MMM d yyyy hh:mm tt");
            }
         }

         return this.Json(listReportSummary, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind dropdown for file type from ReportFormData.xml
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private List<SelectListItem> GetFileTypeList()
      {
         var xmldoc = new System.Xml.XmlDocument();
         xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
         var fileTypeList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_FileType);
         var lstSelectListItem = new List<SelectListItem>();
         foreach (System.Xml.XmlNode fileType in fileTypeList)
         {
            var obj = new SelectListItem()
            {
               Value = fileType.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
               Text = fileType.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
            };
            lstSelectListItem.Add(obj);
         }
         return lstSelectListItem;
      }

      ///****************************************************************************
      /// <summary>
      /// GetEveryList: Bind "Every" spinnner control on form
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private SelectList GetEveryList()
      {
         var dicList = new Dictionary<string, string>();
         for (var count = 1; count < 100; count++)
         {
            dicList.Add(count.ToString(), count.ToString());
         }
         var everyList = new SelectList((IEnumerable)dicList, Utility.Constants.Report_EveryList_Key, Utility.Constants.Report_EveryList_Value);
         return everyList;
      }

      ///****************************************************************************
      /// <summary>
      /// GetHourList: Bind dropdown for Hour from ReportFormData.xml
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private List<SelectListItem> GetHourList()
      {
         var xmldoc = new System.Xml.XmlDocument();
         xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
         var hourList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Hour);
         var lstSelectListItem = new List<SelectListItem>();
         foreach (System.Xml.XmlNode hour in hourList)
         {
            var obj = new SelectListItem()
            {
               Value = hour.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
               Text = hour.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
            };
            lstSelectListItem.Add(obj);
         }
         return lstSelectListItem;
      }

      ///****************************************************************************
      /// <summary>
      /// GetMinuteList: Bind dropdown for Minute from ReportFormData.xml
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private List<SelectListItem> GetMinuteList()
      {
         var xmldoc = new System.Xml.XmlDocument();
         xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
         var minuteList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Minute);
         var lstSelectListItem = new List<SelectListItem>();
         foreach (System.Xml.XmlNode minute in minuteList)
         {
            var obj = new SelectListItem()
            {
               Value = minute.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
               Text = minute.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
            };
            lstSelectListItem.Add(obj);
         }
         return lstSelectListItem;
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind dropdown for Schedule type from ReportFormData.xml
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private List<SelectListItem> GetScheduleTypeList()
      {
         var xmldoc = new System.Xml.XmlDocument();
         xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
         var scheduleType = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_ScheduleType);
         var lstSelectListItem = new List<SelectListItem>();
         foreach (System.Xml.XmlNode schedule in scheduleType)
         {
            var obj = new SelectListItem()
            {
               Value = schedule.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
               Text = schedule.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
            };
            lstSelectListItem.Add(obj);
         }
         return lstSelectListItem;
      }

      ///****************************************************************************
      /// <summary>
      ///   Bind dropdown for Meridieum from ReportFormData.xml
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private List<SelectListItem> GetMeridieumList()
      {
         var xmldoc = new System.Xml.XmlDocument();
         xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
         var meridieumList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Meridieum);
         var lstSelectListItem = new List<SelectListItem>();
         foreach (System.Xml.XmlNode meridieum in meridieumList)
         {
            var obj = new SelectListItem()
            {
               Value = meridieum.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
               Text = meridieum.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
            };
            lstSelectListItem.Add(obj);
         }
         return lstSelectListItem;
      }

      ///****************************************************************************
      /// <summary>
      ///   Set time zones on the UI.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      private SelectList GetReportTimeZoneList()
      {
         var timeZoneList = new SelectList(SchedulerFunctions.GetAllTimeZones(), Utility.Constants.Report_ReportTimeZoneList_Key, Utility.Constants.Report_ReportTimeZoneList_Key);
         return timeZoneList;
      }

      ///****************************************************************************
      /// <summary>
      ///   Save scheduled and criteria information
      /// </summary>
      /// <param name="ReportScheduleDetailViewModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public ActionResult SaveReportDetails(ReportScheduleDetailViewModel reportScheduleDetailViewModel)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.InsertUpdateScheduledData(reportScheduleDetailViewModel, UserId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Product Detail criteria.
      /// </summary>
      /// <param name="retailerProductDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Constants.Report_ActionName_RetailerProduct_Detail_Criteria)]
      public ActionResult Sample_Report_Retailer_Product_Detail_Criteria(RetailerProductDetailsEntity retailerProductDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdateProductDetailsCriteria(retailerProductDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Waste Report Criteria.
      /// </summary>
      /// <param name="wasteReportDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Utility.Constants.Report_ActionName_Waste_Report_Criteria)]
      public ActionResult Waste_Report_Criteria(WasteReportEntity wasteReportDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdateWasteReportCriteria(wasteReportDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Pharmacy Summary Criteria
      /// </summary>
      /// <param name="pharmacySummaryDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Utility.Constants.Report_ActionName_Pharmacy_Summary_Criteria)]
      public ActionResult Pharmacy_Summary_Criteria(PharmacySummaryEntity pharmacySummaryDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdatePharmacySummaryCriteria(pharmacySummaryDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Non Creditable Summary Criteria
      /// </summary>
      /// <param name="nonCreditableSummaryDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Utility.Constants.Report_ActionName_Non_Creditable_Summary_Criteria)]
      public ActionResult Non_Creditable_Summary_Criteria(NonCreditableSummaryEntity nonCreditableSummaryDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdateNonCreditableSummaryCriteria(nonCreditableSummaryDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Manufacturer Summary Criteria
      /// </summary>
      /// <param name="manufacturerSummaryDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Utility.Constants.Report_ActionName_Manufacturer_Summary_Criteria)]
      public ActionResult Manufacturer_Summary_Criteria(ManufacturerSummaryEntity manufacturerSummaryDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdateManufacturerSummaryCriteria(manufacturerSummaryDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method for update Return To Stock Criteria
      /// </summary>
      /// <param name="returnToStockDetail"></param>
      /// <param name="ReportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ActionName(Utility.Constants.Report_ActionName_Return_To_Stock_Criteria)]
      public ActionResult Return_To_Stock_Criteria(ReturnToStockReportEntity returnToStockDetail, string ReportScheduleId)
      {
         Response objclsResponse = null;
         var reportDataAccess = new ReportDataAccess();
         var reportScheduleId = Convert.ToInt64(ReportScheduleId);
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         objclsResponse = reportDataAccess.UpdateReturnToStockCriteria(returnToStockDetail, reportScheduleId);
         return this.Json(objclsResponse, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Product Details report in PDF,XLS and CSV format.
      /// </summary>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Constants.Report_ActionName_RetailerProductDetail)]
      public ActionResult ProductDetails(string FileType, bool Isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, Isdownload, UserId);
            var objRetailerProductDetailsEntity = JsonConvert.DeserializeObject<ProductDetailsEmailEntity>(reportCriteriaDetail);

            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
               var source = new TypeReportSource();
               source.TypeName = typeof(Sample_Report_Retailer_Product_Detail).AssemblyQualifiedName;
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, objRetailerProductDetailsEntity.NDCNumber));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objRetailerProductDetailsEntity.ReportDataTimeZone));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objRetailerProductDetailsEntity.ReportScheduleTimeZone));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objRetailerProductDetailsEntity.DataStartDate));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objRetailerProductDetailsEntity.DataEndDate));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_StoreName, objRetailerProductDetailsEntity.StoreName));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_VendorName, objRetailerProductDetailsEntity.VendorName));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ProductDescription, objRetailerProductDetailsEntity.ProductDescription));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_LotNumber, objRetailerProductDetailsEntity.LotNumber));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ExpDate, objRetailerProductDetailsEntity.ExpDate));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_FullQty, objRetailerProductDetailsEntity.FullQty));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PartialQty, objRetailerProductDetailsEntity.PartialQty));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceBefore, objRetailerProductDetailsEntity.UnitPriceBefore));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountBeforeMFGDiscount, objRetailerProductDetailsEntity.CreditableAmountBeforeMFGDiscount));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ReturnStatus, objRetailerProductDetailsEntity.ReturnStatus));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_OutofpolicyCodeDescription, objRetailerProductDetailsEntity.OutofpolicyDescription));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_Strength, objRetailerProductDetailsEntity.Strength));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ItemGuid, objRetailerProductDetailsEntity.ItemGuid));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_QoskProcessDate, objRetailerProductDetailsEntity.QoskProcessDate));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ControlNumber, objRetailerProductDetailsEntity.ControlNumber));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PackageSize, objRetailerProductDetailsEntity.PackageSize));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceAfter, objRetailerProductDetailsEntity.UnitPriceAfter));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountAfter, objRetailerProductDetailsEntity.CreditableAmountAfter));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_RecalledProduct, objRetailerProductDetailsEntity.RecalledProduct));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_DiscontinuedProduct, objRetailerProductDetailsEntity.DiscontinuedProduct));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_RXorOTC, objRetailerProductDetailsEntity.RXorOTC));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_DosageForm, objRetailerProductDetailsEntity.DosageForm));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PackageForm, objRetailerProductDetailsEntity.PackageForm));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PartialPercentage, objRetailerProductDetailsEntity.PartialPercentage));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_StoreNumber, objRetailerProductDetailsEntity.StoreNumber));
               source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ExtendedDiscountPercent, objRetailerProductDetailsEntity.ExtendedDiscountPercent));

               if (Isdownload)
               {
                  this.DownloadReportOnBrowser(source, FileType);
                  return null;
               }

               this.ViewBag.ReportSource = source;
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportRetailerProductDetails();
               var listReportPreview = new List<ProductDetailsEmailEntity>();

               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.GetRetailer_Product_Details(objRetailerProductDetailsEntity, FileType, UserId);
               }

               var commonDataAccess = new CommonDataAccess();
               var gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ProductDetailListPanel, UserId);
               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               // Get the local time zone
               var timeZone = TimeZone.CurrentTimeZone;

               foreach (var productDetailsEmailEntity in reportData.reportRetailerBoxDetails.ToList())
               {
                  DateTime expDate;
                  DateTime qoskProcessDate;

                  if (DateTime.TryParse(productDetailsEmailEntity.ExpDate, out expDate))
                  {
                     DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone.ToString(), expDate) ?? expDate;
                     productDetailsEmailEntity.ExpDate = "<span hidden>" + time.Value.ToString("s") + "</span>" + productDetailsEmailEntity.ExpDate;
                  }

                  if (DateTime.TryParse(productDetailsEmailEntity.QoskProcessDate, out qoskProcessDate))
                  {
                     DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone.ToString(), qoskProcessDate) ?? qoskProcessDate;
                     productDetailsEmailEntity.QoskProcessDate = "<span hidden>" + time.Value.ToString("s") + "</span>" + productDetailsEmailEntity.QoskProcessDate;
                  }
               }

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }
         return this.View(Utility.Constants.Report_ReportPreview);

      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Waste Report in PDF,XLS and CSV format.
      /// </summary>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_Waste_Report)]
      public ActionResult WasteReport(string FileType, bool Isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, Isdownload, UserId);
            var objWasteReportEntity =
               JsonConvert.DeserializeObject<WasteReportEmailEntity>(reportCriteriaDetail);
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
               var Source = new TypeReportSource();
               Source.TypeName = typeof(WasteReport).AssemblyQualifiedName;
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, objWasteReportEntity.NDCNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objWasteReportEntity.ReportDataTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objWasteReportEntity.ReportScheduleTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objWasteReportEntity.DataStartDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objWasteReportEntity.DataEndDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_Description, objWasteReportEntity.ProductDescription));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_Strength, objWasteReportEntity.Strength));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_FullQuantity, objWasteReportEntity.FullQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_LotNumber, objWasteReportEntity.LotNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_SealedOpenCase, objWasteReportEntity.SealedOpenCase));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ItemGuid, objWasteReportEntity.ItemGuid));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_WasteCode, objWasteReportEntity.WasteCode));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_WasteStreamProfile, objWasteReportEntity.WasteStreamProfile));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PartialQuantity, objWasteReportEntity.PartialQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_StoreNumber, objWasteReportEntity.StoreNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_StoreName, objWasteReportEntity.StoreName));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ExpDate, objWasteReportEntity.ExpDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PackageSize, objWasteReportEntity.PackageSize));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ControlNumber, objWasteReportEntity.ControlNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_DosageForm, objWasteReportEntity.DosageForm));
               Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PackageForm, objWasteReportEntity.PackageForm));
               if (Isdownload)
               {
                  this.DownloadReportOnBrowser(Source, FileType);
                  return null;
               }
               else
               {
                  this.ViewBag.ReportSource = Source;
               }
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportRetailerWasteDetail();
               var listReportPreview = new List<WasteReportEmailEntity>();
               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.Get_Waste_Report_Details(objWasteReportEntity, FileType,
                     UserId);
               }
               var commonDataAccess = new CommonDataAccess();
               var gcList =
                  commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_WasteReportListPanel,
                     UserId);
               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               // Get the local time zone
               var timeZone = TimeZone.CurrentTimeZone;

               foreach (var wasteReportEmailEntity in reportData.reportRetailerBoxDetails.ToList())
               {
                  DateTime expDate;
                  if (DateTime.TryParse(wasteReportEmailEntity.ExpDate, out expDate))
                  {
                     DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone.ToString(), expDate) ??
                                      expDate;
                     wasteReportEmailEntity.ExpDate = "<span hidden>" + time.Value.ToString("s") + "</span>" +
                                                      wasteReportEmailEntity.ExpDate;
                  }
               }

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }

         return this.View(Utility.Constants.Report_ReportPreview);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Pharmacy Summary Report in PDF,XLS and
      ///   CSV format.
      /// </summary>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_PharmacySummary)]
      public ActionResult PharmacySummary(string FileType, bool Isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, Isdownload, UserId);
            var objPharmacyReportEmailEntity = JsonConvert.DeserializeObject<PharmacyReportEmailEntity>(reportCriteriaDetail);
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
               var Source = new TypeReportSource();
               Source.TypeName = typeof(PharmacySummary).AssemblyQualifiedName;
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_StoreNumber, objPharmacyReportEmailEntity.StoreNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objPharmacyReportEmailEntity.ReportDataTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objPharmacyReportEmailEntity.ReportScheduleTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objPharmacyReportEmailEntity.DataStartDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objPharmacyReportEmailEntity.DataEndDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_StoreName, objPharmacyReportEmailEntity.StoreName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_WholesalerCustomerNumber, objPharmacyReportEmailEntity.WholesalerCustomerNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RegionName, objPharmacyReportEmailEntity.RegionName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableValue, objPharmacyReportEmailEntity.ReturnCreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableQty, objPharmacyReportEmailEntity.ReturnCreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableValue, objPharmacyReportEmailEntity.RecallCreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableQty, objPharmacyReportEmailEntity.RecallCreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableValue, objPharmacyReportEmailEntity.NonCreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableQty, objPharmacyReportEmailEntity.NonCreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalProductValue, objPharmacyReportEmailEntity.TotalProductValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalQty, objPharmacyReportEmailEntity.TotalQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ProcessingFeeBilled, objPharmacyReportEmailEntity.ProcessingFeeBilled));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalInvoiceAmount, objPharmacyReportEmailEntity.TotalInvoiceAmount));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalDebitMemoQty, objPharmacyReportEmailEntity.TotalDebitMemoQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockValue, objPharmacyReportEmailEntity.ReturnToStockValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockQty, objPharmacyReportEmailEntity.ReturnToStockQty));
               if (Isdownload)
               {
                  this.DownloadReportOnBrowser(Source, FileType);
                  return null;
               }
               else
               {
                  this.ViewBag.ReportSource = Source;
               }
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportPharmacySummaryEntity();
               var listReportPreview = new List<PharmacyReportEmailEntity>();
               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.Get_Pharmacy_Summary_Details(objPharmacyReportEmailEntity, FileType, UserId);
               }
               var commonDataAccess = new CommonDataAccess();
               var gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_PharmacySummaryListPanel, UserId);

               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }

         return this.View(Utility.Constants.Report_ReportPreview);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Non Creditable Summary Report in PDF,XLS
      ///   and CSV format.
      /// </summary>
      /// <param name="FileType"></param>
      /// <param name="Isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_NonCreditableSummary)]
      public ActionResult NonCreditableSummary(string FileType, bool Isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, Isdownload, UserId);
            var objNonCreditableSummaryEmailEntity = JsonConvert.DeserializeObject<NonCreditableSummaryEmailEntity>(reportCriteriaDetail);
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
               var Source = new TypeReportSource();
               Source.TypeName = typeof(NonCreditableSummary).AssemblyQualifiedName;
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreNumber, objNonCreditableSummaryEmailEntity.StoreNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objNonCreditableSummaryEmailEntity.ReportDataTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objNonCreditableSummaryEmailEntity.ReportScheduleTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objNonCreditableSummaryEmailEntity.DataStartDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objNonCreditableSummaryEmailEntity.DataEndDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreName, objNonCreditableSummaryEmailEntity.StoreName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_RegionName, objNonCreditableSummaryEmailEntity.RegionName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableValue, objNonCreditableSummaryEmailEntity.NonCreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableQty, objNonCreditableSummaryEmailEntity.NonCreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_ManufacturerName, objNonCreditableSummaryEmailEntity.ManufacturerName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonCode, objNonCreditableSummaryEmailEntity.NonCreditableReasonCode));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonDescription, objNonCreditableSummaryEmailEntity.NonCreditableReasonDescription));
               if (Isdownload)
               {
                  this.DownloadReportOnBrowser(Source, FileType);
                  return null;
               }
               else
               {
                  this.ViewBag.ReportSource = Source;
               }
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportNonCreditableSummaryEntity();
               var listReportPreview = new List<NonCreditableSummaryEmailEntity>();
               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.Get_Non_Creditable_Summary_Details(objNonCreditableSummaryEmailEntity, FileType, UserId);
               }
               var commonDataAccess = new CommonDataAccess();
               var gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_NonCreditableSummaryListPanel, UserId);

               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }

         return this.View(Utility.Constants.Report_ReportPreview);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Manufacturer Summary Report in PDF, XLS
      ///   and CSV format.
      /// </summary>
      /// <param name="fileType"></param>
      /// <param name="isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_ManufacturerSummary)]
      public ActionResult ManufacturerSummary(string fileType, bool isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, isdownload, UserId);
            var objManufacturerSummaryEmailEntity = JsonConvert.DeserializeObject<ManufacturerSummaryEmailEntity>(reportCriteriaDetail);
            if (fileType == Utility.Constants.Report_FileType_PDF || isdownload)
            {
               var Source = new TypeReportSource();
               Source.TypeName = typeof(ManufacturerSummary).AssemblyQualifiedName;
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_MFGLabeler, objManufacturerSummaryEmailEntity.MFGLabeler));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objManufacturerSummaryEmailEntity.ReportDataTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objManufacturerSummaryEmailEntity.ReportScheduleTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objManufacturerSummaryEmailEntity.DataStartDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objManufacturerSummaryEmailEntity.DataEndDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, fileType));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_ManufacturerName, objManufacturerSummaryEmailEntity.ManufacturerName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RetailerVendorNumber, objManufacturerSummaryEmailEntity.RetailerVendorNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_DebitMemoInvoiceNo, objManufacturerSummaryEmailEntity.DebitMemoInvoiceNo));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableValue, objManufacturerSummaryEmailEntity.CreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableQty, objManufacturerSummaryEmailEntity.CreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableValue, objManufacturerSummaryEmailEntity.RecallCreditableValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableQty, objManufacturerSummaryEmailEntity.RecallCreditableQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalProductValue, objManufacturerSummaryEmailEntity.TotalProductValue));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalQty, objManufacturerSummaryEmailEntity.TotalQty));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_ProcessingFee, objManufacturerSummaryEmailEntity.ProcessingFee));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalInvoiceAmount, objManufacturerSummaryEmailEntity.TotalInvoiceAmount));
               if (isdownload)
               {
                  this.DownloadReportOnBrowser(Source, fileType);
                  return null;
               }
               else
               {
                  this.ViewBag.ReportSource = Source;
               }
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportManufacturerSummaryEntity();
               var listReportPreview = new List<ManufacturerSummaryEmailEntity>();
               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.Get_Manufacturer_Summary_Details(objManufacturerSummaryEmailEntity, fileType, UserId);
               }
               var commonDataAccess = new CommonDataAccess();
               var gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ManufacturerSummaryListPanel, UserId);

               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }

         return this.View(Utility.Constants.Report_ReportPreview);
      }

      ///****************************************************************************
      /// <summary>
      ///   Action method to generate Return To Stock Report in PDF, XLS
      ///   and CSV format.
      /// </summary>
      /// <param name="fileType"></param>
      /// <param name="isdownload"></param>
      /// <param name="reportCriteriaDetail"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      [ActionName(Utility.Constants.Report_ActionName_ReturnToStock)]
      public ActionResult ReturnToStock(string fileType, bool isdownload, string reportCriteriaDetail, int reportScheduleId)
      {
         if (!string.IsNullOrEmpty(reportCriteriaDetail) || isdownload)
         {
            var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
            var reportDataAccess = new ReportDataAccess();
            reportDataAccess.RecordPreviewOrDownload(reportScheduleId, isdownload, UserId);
            var objReturnToStockEmailEntity = JsonConvert.DeserializeObject<ReturnToStockEmailEntity>(reportCriteriaDetail);
            if (fileType == Utility.Constants.Report_FileType_PDF || isdownload)
            {
               var Source = new TypeReportSource();
               Source.TypeName = typeof(ReturnToStock).AssemblyQualifiedName;
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_VendorName, objReturnToStockEmailEntity.VendorName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objReturnToStockEmailEntity.ReportDataTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objReturnToStockEmailEntity.ReportScheduleTimeZone));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objReturnToStockEmailEntity.DataStartDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objReturnToStockEmailEntity.DataEndDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, fileType));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_StoreName, objReturnToStockEmailEntity.StoreName));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_StoreNumber, objReturnToStockEmailEntity.StoreNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_NDCNumber, objReturnToStockEmailEntity.NDCNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ProductDescription, objReturnToStockEmailEntity.ProductDescription));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_Strength, objReturnToStockEmailEntity.Strength));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ControlNumber, objReturnToStockEmailEntity.ControlNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_RXorOTC, objReturnToStockEmailEntity.RXorOTC));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_DosageForm, objReturnToStockEmailEntity.DosageForm));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_PackageForm, objReturnToStockEmailEntity.PackageForm));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_LotNumber, objReturnToStockEmailEntity.LotNumber));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ExpDate, objReturnToStockEmailEntity.ExpDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_QoskProcessDate, objReturnToStockEmailEntity.QoskProcessDate));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_DateEligibleForCredit, objReturnToStockEmailEntity.DateEligibleForCredit));
               Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_OutofpolicyDescription, objReturnToStockEmailEntity.OutofpolicyDescription));
               if (isdownload)
               {
                  this.DownloadReportOnBrowser(Source, fileType);
                  return null;
               }
               else
               {
                  this.ViewBag.ReportSource = Source;
               }
               return this.View(Utility.Constants.Report_ReportRender);
            }
            else
            {
               var reportData = new ReportReturnToStockReportEntity();
               var listReportPreview = new List<ReturnToStockEmailEntity>();
               using (var reportDataContext = new ReportDataContext())
               {
                  listReportPreview = reportDataContext.Get_Return_To_Stock_Details(objReturnToStockEmailEntity, fileType, UserId);
               }
               var commonDataAccess = new CommonDataAccess();
               var gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ReturnToStockListPanel, UserId);

               reportData.reportRetailerBoxDetails = listReportPreview;
               reportData.gridColumns = gcList;

               // Get the local time zone
               var timeZone = TimeZone.CurrentTimeZone;

               foreach (var returnToStockEmailEntity in reportData.reportRetailerBoxDetails.ToList())
               {
                  DateTime expDate;
                  if (DateTime.TryParse(returnToStockEmailEntity.ExpDate, out expDate))
                  {
                     DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone.ToString(), expDate) ?? expDate;
                     returnToStockEmailEntity.ExpDate = "<span hidden>" + time.Value.ToString("s") + "</span>" + returnToStockEmailEntity.ExpDate;
                  }
                  DateTime qoskProcessDate;
                  if (DateTime.TryParse(returnToStockEmailEntity.QoskProcessDate, out qoskProcessDate))
                  {
                     DateTime? time = SchedulerFunctions.ConvertUTCtojsTimeZone(timeZone.ToString(), qoskProcessDate) ?? qoskProcessDate;
                     returnToStockEmailEntity.QoskProcessDate = "<span hidden>" + time.Value.ToString("s") + "</span>" + returnToStockEmailEntity.QoskProcessDate;
                  }
               }

               var jsonResult = this.Json(reportData, JsonRequestBehavior.AllowGet);
               jsonResult.MaxJsonLength = int.MaxValue;
               return jsonResult;
            }
         }

         return this.View(Utility.Constants.Report_ReportPreview);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete record from Report Scheduler Grid
      /// </summary>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public JsonResult Delete(int reportScheduleId)
      {
         ReportDataAccess reportDataAccess = null;
         Response objclsResponse = null;
         try
         {
            reportDataAccess = new ReportDataAccess();
            var modifiedBy = this.User.Identity.Name;
            objclsResponse = reportDataAccess.DeleteReportSchedule(reportScheduleId);
            switch (objclsResponse.Status)
            {
               case Status.OK:
                  return this.Json(new { Result = QoskCloudWebResource.DeletedSuccessfully, Status = true }, JsonRequestBehavior.AllowGet);

               case Status.Deleted:
                  return this.Json(new { Result = QoskCloudWebResource.Deleted, Status = false }, JsonRequestBehavior.AllowGet);

               case Status.Fail:
                  return this.Json(new { Result = QoskCloudWebResource.DeletedFails, Status = false }, JsonRequestBehavior.AllowGet);
            }
         }
         finally
         {
            reportDataAccess = null;
            objclsResponse = null;
         }

         return this.Json(QoskCloudWebResource.DeletedFails);
      }

      ///****************************************************************************
      /// <summary>
      ///   Function for Send Email
      /// </summary>
      /// <param name="emailBody"></param>
      /// <param name="emailList"></param>
      /// <param name="details"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public ActionResult SendEmail(string emailBody, string emailList, string details)
      {
         var isEmailSend = false;
         Dictionary<string, object> emailDetailList = null;
         try
         {
            if (details != null)
            {
               emailDetailList = JsonConvert.DeserializeObject<Dictionary<string, object>>(details);
               var _EmailBody = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType<List<SasUriModel>>(emailBody, new List<SasUriModel>());
               if (!string.IsNullOrEmpty(emailList))
               {
                  var emails = emailList.Split(',');
                  var _emailList = new List<string>(emails);
                  var commonDataAccess = new CommonDataAccess();
                  isEmailSend = commonDataAccess.SendEmail(_EmailBody, _emailList, emailDetailList);
               }
               if (isEmailSend)
               {
                  return this.Json(Utility.Constants.SendEmail_SuccessfullySend, JsonRequestBehavior.AllowGet);
               }
               else
               {
                  return this.Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet);
               }
            }
            else
            {
               return this.Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet);
            }
         }
         catch
         {
            return this.Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Function for Display Image And Video in Popup
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult DisplayImageAndVideo(string itemGuid)
      {
         try
         {
            //if QoskID and ItemIDForBarcode is null then return no image
            if (string.IsNullOrEmpty(itemGuid))
               return this.Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet);

            //Get images/Videos from azure blob.
            var imageList = AzureCommonUtility.DownloadImagesVideosFromAzure(itemGuid);
            if (imageList != null && imageList.Count > 0)
               return this.Json(imageList, JsonRequestBehavior.AllowGet);
            else return this.Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet);
         }
         catch
         {
            return this.Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Auto Complete Details for criteria
      /// </summary>
      /// <param name="terms"></param>
      /// <param name="controlID"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetAutoCompleteDetails(string terms, string controlID)
      {
         controlID = controlID.Replace(Utility.Constants.GetAutoCompleteDetails_textbox, string.Empty);
         var objReportDataAccess = new ReportDataAccess();

         var profilelist = objReportDataAccess.GetDetailsUsingAutoCompleteBasedOnControlID(terms, controlID);
         if (profilelist != null)
         {
            var objProfile = profilelist
                              .Select(n => new SelectListItem
                              {
                                 Text = n.Text,
                                 Value = Convert.ToString(n.Value)
                              });
            return this.Json(objProfile, JsonRequestBehavior.AllowGet);
         }

         return this.Json(null, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get column list save by specific user and show into custom grid popup.
      /// </summary>
      /// <param name="reportName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [QoskAuthorizeAttribute]
      [HttpGet]
      public PartialViewResult BindColumnListing(string reportName)
      {
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         ColumnListEntity objListing = null;

         if (reportName == Utility.Constants.Report_ActionName_RetailerProductDetail)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ProductDetailListPanel, UserId, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_RetailerWasteReport)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_WasteReportListPanel, UserId, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_PharmacySummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_PharmacySummaryListPanel, UserId, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_NonCreditableSummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_NonCreditableSummaryListPanel, UserId, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_ManufacturerSummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ManufacturerSummaryListPanel, UserId, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_ReturnToStock)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ReturnToStockListPanel, UserId, true);
         }

         return this.PartialView("_ReportColumnGridListPanel", objListing);
      }

      ///****************************************************************************
      /// <summary>
      ///   Create criteria search panel dynamically based on report name.
      /// </summary>
      /// <param name="reportName"></param>
      /// <param name="reportScheduleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public PartialViewResult CreateSearchPanel(string reportName, int reportScheduleId)
      {
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());
         ColumnListEntity objListing = null;
         var objReportDataAccess = new ReportDataAccess();
         var reportCriteria = objReportDataAccess.GetReportCriteria(reportScheduleId);
         Dictionary<string, object> parameterList = null;

         if (!string.IsNullOrEmpty(reportCriteria))
         {
            parameterList = JsonConvert.DeserializeObject<Dictionary<string, object>>(reportCriteria);
         }

         if (reportName == Utility.Constants.Report_ActionName_RetailerProductDetail)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ProductDetailSearchPanel, UserId, false, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_RetailerWasteReport)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_WasteReportSearchPanel, UserId, false, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_PharmacySummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_PharmacySummarySearchPanel, UserId, false, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_NonCreditableSummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_NonCreditableSummarySearchPanel, UserId, false, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_ManufacturerSummary)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ManufacturerSummarySearchPanel, UserId, false, true);
         }
         else if (reportName == Utility.Constants.Report_ActionName_ReturnToStock)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ReturnToStockSearchPanel, UserId, false, true);
         }

         this.ViewData["ControlListingForPopUp"] = objListing;
         var objControlVMCollection = new List<ControlViewModel>();
         var controlListing = new DynamicControlEntity();
         var commonDataAccess = new CommonDataAccess();

         for (var i = 0; i < objListing.CriteriaColumn.Count; i++)
         {
            var controlName = objListing.CriteriaColumn[i].ColumnName;
            object controlValue = null;
            object autoComplete = null;

            if (parameterList != null && parameterList.ContainsKey(controlName) && controlName != Utility.Constants.CreateSearchPanel_DefaulColumnFlag)
            {
               controlValue = parameterList.Single(x => x.Key == controlName).Value;
            }

            autoComplete = objListing.CriteriaColumn[i].IsAutoComplete;
            objControlVMCollection.Add(commonDataAccess.CreateDynamicControlBasedOnValue(objListing.CriteriaColumn[i], controlValue, autoComplete, UserId));
         }

         controlListing.Controls = objControlVMCollection.OrderByDescending(x => x.FieldLength).ToList();
         controlListing.Controls.RemoveAll(x => x.Name == "DefaulColumnFlag");

         return controlListing.Controls.Count > 0 ? this.PartialView("_ReportSearchCriteriaDisplay", controlListing) : null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get column list save by specific user and show into custom search popup
      /// </summary>
      /// <param name="reportName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult CreateColumnListingPopUp(string reportName)
      {
         ColumnListEntity objListing = null;
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());

         switch (reportName)
         {
            case Utility.Constants.Report_ActionName_RetailerProductDetail:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ProductDetailSearchPanel);

            case Utility.Constants.Report_ActionName_RetailerWasteReport:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_WasteReportSearchPanel);

            case Utility.Constants.Report_ActionName_PharmacySummary:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_PharmacySummarySearchPanel);

            case Utility.Constants.Report_ActionName_NonCreditableSummary:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_NonCreditableSummarySearchPanel);

            case Utility.Constants.Report_ActionName_ManufacturerSummary:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ManufacturerSummarySearchPanel);

            case Utility.Constants.Report_ActionName_ReturnToStock:
               return this.GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ReturnToStockSearchPanel);

         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Common Function For ListingPopUp.
      /// </summary>
      /// <param name="layoutName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [NonAction]
      public ColumnListEntity CommonFunctionForListingPopUp(string layoutName)
      {
         ColumnListEntity objListing = null;
         var UserId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());

         if (this.ViewData["ControlListingForPopUp"] == null)
         {
            objListing = ManageLayoutConfig.GetColumnListingByLayout(layoutName, UserId, false);
         }
         return objListing;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Partial View For Report Entity.
      /// </summary>
      /// <param name="objListing"></param>
      /// <param name="layoutName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpGet]
      public ActionResult GetPartialViewForReportEntity(ColumnListEntity objListing, string layoutName)
      {
         objListing = this.CommonFunctionForListingPopUp(layoutName);

         return objListing != null
                     ? this.PartialView("_ReportColumnSearchListPanel", objListing)
                     : this.PartialView("_ReportColumnSearchListPanel");
      }

      ///****************************************************************************
      /// <summary>
      ///   Save custom search and custom grid columns detail based on layout id.
      /// </summary>
      /// <param name="searchList"></param>
      /// <param name="layoutId"></param>
      /// <param name="tableId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public JsonResult SaveCustomSearchDetailsByUserName(string searchList, int layoutId, int tableId = 0)
      {
         var objList = new ColumnListEntity
         {
            SubscribedColumn = new List<ColumnDetails>()
         };

         var sColumnList = searchList.Split(',');
         var userId = CommonDataAccess.GetCurrentUserId(this.User.Identity.GetUserId());

         foreach (var objColumn in sColumnList)
         {
            if (!string.IsNullOrEmpty(objColumn))
            {
               var objColumnDetails = new ColumnDetails
               {
                  ColumnId = Convert.ToInt32(objColumn),
                  LayoutId = layoutId,
                  UserId = userId
               };

               objList.SubscribedColumn.Add(objColumnDetails);
            }
         }

         return objList.SubscribedColumn != null
            ? this.Json(ManageLayoutConfig.SaveSubscribedColumn(objList, userId, layoutId, tableId)
               ? "Custom_Save_Success"
               : "UnDefined")
            : this.Json("UnDefined");
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

   }
}