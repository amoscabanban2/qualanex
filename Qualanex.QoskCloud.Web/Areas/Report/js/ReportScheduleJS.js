﻿
var Details = "";
var reportCriteriaDetail = "";
var PreviewGridHeight = "";
var zoomValue = 0;
var zoomVal = 0;

//These function are executing while loading the page
//LoadHeight();
ChangeReportName();
ProductDetailsInformation();
CreateReportScheduleGridDesign();

//Function to load height dynamically.
//function LoadHeight() {
//    var winHeight = $(window).height();
//    var gridHeight = winHeight - 505;
//    $("#hdnGridHeight").val(gridHeight);
//    AllGridHeight = gridHeight;

//    var headerHeight = $(".header-top").height();
//    var customBarHeight = $(".top-nav-box").height();
//    var headingHeight = $(".text-center").height();
//    var previewTabsHeight = $(".navbar-div").height();
//    var footerHeight = $(".report-footer").height();

//    PreviewGridHeight = winHeight - (headerHeight + customBarHeight + 4 * headingHeight + previewTabsHeight + footerHeight);
//    $(".data-grid-show").css("height", gridHeight + "px");
//    //$("#ifReport").css("height", PreviewGridHeight + "px");
//    $("#dvJqxgrid_ReportPreviewCSV").css("height", PreviewGridHeight + "px");
//    $("#dvJqxgrid_ReportPreviewXLS").css("height", PreviewGridHeight + "px");
//    $("#dvJqxgrid_ReportImageVideo").css("height", PreviewGridHeight + "px");
//}
//$(window).resize(function () {
//    var height = $(window).height() - 505;
//    $(".data-grid-show").height(height);
//    AllGridHeight = height;
//    //$("#ifReport").height(PreviewGridHeight);
//    $("#dvJqxgrid_ReportPreviewCSV").height(PreviewGridHeight);
//    $("#dvJqxgrid_ReportPreviewXLS").height(PreviewGridHeight);
//    $("#dvJqxgrid_ReportImageVideo").height(PreviewGridHeight);
//});
//$(window).trigger("resize");


function ProductDetailsInformation()
{

   $("#reportPreviewHeading").text($("#hdnReportName").val());
   if ($("#hdnReportName").val() === ReportNames.ProductDetails)
   {
      $("#reportPreviewHeading").text("Product Details");
   }
   if ($("#hdnReportName").val() === ReportNames.ProductDetails)
   {
      $("#reportScheduleHeading").text("Scheduled Reports (Product Details)");
   }
   if ($("#hdnReportName").val() === ReportNames.ProductDetails)
   {
      $("#reportScheduleHistoryHeading").text("Scheduled Reports History (Product Details)");
   }
   if ($("#hdnReportName").val() === ReportNames.ProductDetails)
   {
      $("#reportImageVideoHeading").text("Image/Video (Product Details)");
   }

   $("#reportScheduleHistoryHeading").hide();
   $("#reportPreviewHeading").hide();
   $("#reportImageVideoHeading").hide();
}

$(".lot-icon").unbind("click");

//Function for showing and hiding loader
function ShowLoading()
{
   $("#divloaderBackground").show();
   $("#divLoader").show();
}
function HideLoading()
{
   $("#divLoader").hide();
   $("#divloaderBackground").hide();
}
//Toolbar control functionality
$("#btn_toolbar_new").on("click", function ()
{
   $("#hdnEditNewClicked").val("Edit_New");
   localStorage["visited"] = "yes";
   $("#hdnReportScheduleId").val("");
   CreateReportSearchPanel($("#hdnReportScheduleId").val());
   var datarow = {
      ReportScheduleID: null,
      ReportScheduleTitle: "&nbsp;",
      //Schedule: "",
      DateRange: "",
      StoreNumber: ""
      //Criteria: ""

   };
   $("#datagridtable").DataTable().row.add(datarow).draw(false);

   DisableEnableToolbarAfterAddNewAndEdit();

   EditScheduleGrid(0);
   $("#divReportScheduler td.tdSeparatorB,td.tdSeparatorA").css("background-color", "#ECDD9D");
   event.stopPropagation();
});
$("#btn_toolbar_edit").click(function ()
{
   $("#hdnEditNewClicked").val("Edit_New");
   DisableEnableToolbarAfterAddNewAndEdit();
   if (isExclamationEnable === true)
   {
      $("#hdnEditNewClicked").val("");
      CreateReportSearchPanel($("#hdnReportScheduleId").val());
      $("#hdnEditNewClicked").val("Edit_New");
      EditScheduleGrid($("#hdnReportScheduleId").val());
   }
   EnableScheduleAndCriteriaAfterEdit();
});
$("#btn_toolbar_cancel").click(function ()
{

   $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
   $("#confirmAlert").show();

   $("#btdeletenCancel,#btnok,.alert-close").click(function ()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });
   $("#btdeletenok").unbind("click");
   $("#btdeletenok").click(function ()
   {
      $("#hdnEditNewClicked").val("");
      location.reload();

   });
});

$("#btn_toolbar_refresh").click(function ()
{

   if (localStorage["History"] === "yes")
   {
      CreateReportStatusGridDesign();
      DisableEnableToolbarAfterHistory();
   }
   else
   {
      isExclamationEnable = false;
      //CreateReportScheduleGridDesign();
      $("#datagridtable").DataTable().destroy();
      $.ajax({
         type: "Get",
         contentType: "application/json; charset=utf-8",
         url: resolveUrl() + "/Home/BindReportSchedulerGrid",
         cache: false,
         data: { ReportId: $("#hdnReportid").val() },
         success: function (data)
         {
            ReloadScheduleGridData(data);
         },
         error: function (response)
         {
            if (response.status !== 0 && response.status !== 401)
            {
               alert(response.status + " " + response.statusText);
            }
         }
      }).done(function ()
      {
         LoadTopRowScheduleDetails($("#datagridtable").DataTable().rows(this).data()[0]);
         window.resizeTabs();
      });
   }
});
function ReloadScheduleGridData(json)
{
   $("#datagridtable")
      .DataTable({
         scrollX: true,
         scrollCollapse: true,
         fixedColumns: true,
         height: "auto",
         data: json,
         columns: [
            { title: "Report ID", data: "ReportScheduleID" },
            { title: "Report Title", data: "ReportScheduleTitle" },
            //{ title: "Schedule", data: "Schedule" },
            { title: "Date Range", data: "DateRange" },
            { title: "Store Number", data: "StoreNumber" }
            //{ title: "Criteria", data: "Criteria" }
         ],
         columnDefs: [
            {
               targets: [0],
               visible: false
            }
         ],
         select: true
      });

   $("#datagridtable").DataTable().rows().nodes().to$().attr("data-unique-selection", "schedule");
   $("#datagridtable").DataTable().rows().nodes().to$().addClass("selectable");

   $("#datagridtable tbody tr:first").addClass("selected");

   $(document).on("click", "#datagridtable tbody tr", function ()
   {
      if ($("#hdnEditNewClicked").val() !== "")
      {
         PendingChanges();
      }
      else
      {
         changeSelection(this);
         LoadTopRowScheduleDetails($("#datagridtable").DataTable().rows(this).data()[0]);
      }
   });
}

$("#btn_toolbar_zoomIn").click(function ()
{
   if (navigator.userAgent.indexOf("Chrome") !== -1)
   {
      zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom"));
   }
   else if (!!document.documentMode === true /*For IE > 10*/)
   {
      zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom")) / 100;
   }
   else if (zoomVal === 0)
   {
      zoomVal = 1;
   }

   if (zoomVal === 5)
   {
      return;
   }

   zoomVal = zoomVal + .5;

   if (navigator.userAgent.indexOf("Chrome") !== -1 || (!!document.documentMode === true /*For IE > 10*/))
   {
      $("#dvJqxgrid_schedule").css("zoom", zoomVal);
   }
   else if (navigator.userAgent.indexOf("Firefox") !== -1)
   {
      $("#dvJqxgrid_schedule").css("MozTransform", "scale(" + zoomVal + ")");
      $("#dvJqxgrid_schedule").css("MozTransformOrigin", "0 0");
   }

});

$("#btn_toolbar_zoomOut").click(function ()
{
   if (navigator.userAgent.indexOf("Chrome") !== -1)
   {
      zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom"));
   }
   else if (!!document.documentMode === true /*For IE > 10*/)
   {
      zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom")) / 100;
   }

   if (zoomVal <= 1)
   {
      return;
   }

   zoomVal = zoomVal - 0.5;

   if (navigator.userAgent.indexOf("Chrome") !== -1 || (!!document.documentMode === true /*For IE > 10*/))
   {
      $("#dvJqxgrid_schedule").css("zoom", zoomVal);
   }
   else if (navigator.userAgent.indexOf("Firefox") !== -1)
   {
      $("#dvJqxgrid_schedule").css("MozTransform", "scale(" + zoomVal + ")");
      $("#dvJqxgrid_schedule").css("MozTransformOrigin", "0 0");
   }
});

$("#btn_toolbar_delete").click(function ()
{
   var reportScheduleId = $("#datagridtable").DataTable().rows(".selected").data()[0].ReportScheduleID;

   if (reportScheduleId !== null)
   {
      deleteReport(reportScheduleId);
   }

   function deleteReport()
   {
      $("#alertdeletemessageid").text(Report.DeleteConfirmation);
      $("#confirmAlert").show();
   }

   $("#btdeletenCancel,#btnok,.alert-close").click(function ()
   {
      $("#confirmAlert").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });

   $("#btdeletenok").unbind("click");
   $("#btdeletenok").click(function ()
   {
      $.ajax({
         url: resolveUrl() + "/Home/Delete",
         cache: false,
         data: {
            ReportScheduleId: reportScheduleId
         },
         success: function (data)
         {
            if (data.Status === true)
            {

               $("#confirmAlert").hide();
               $("#alertmessageid").text(data.Result);
               $(".alert-popup-body-message").show();

               $("#btnok").click(function ()
               {
                  $(".alert-popup-body-message").hide();
                  location.reload();
               });

            }
            else
            {
               $("#alertmessageid").text(data.Result);
               $(".alert-popup-body-message").show();
            }

         },
         error: function (response)
         {
            if (response.status !== 0 && response.status !== 401)
            {
               alert(response.status + " " + response.statusText);
            }
         }
      });
   });
});

$("#btn_toolbar_download").click(function ()
{
   var reportScheduleId = $("#hdnReportScheduleId").val();
   var ndc = $("#NDCNumber").val();
   var reportScheduleTimeZone = $("#ReportScheduleTimeZone").val();
   var reportDataTimeZone = $("#ReportDataTimeZone").val();
   var fileType = $("select#FileType option:selected").text();

   if (typeof (fileType) !== "undefined" && fileType !== "Select")
   {
      //Just to call report action method
      //As ajax does not support to binary write response.
      if (report === ReportNames.ProductDetails)
      {
         reportCriteriaDetail = productDetailCriteria();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else if (report === ReportNames.WasteReport)
      {
         reportCriteriaDetail = wasteReportCriteria();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else if (report === ReportNames.PharmacySummary)
      {
         reportCriteriaDetail = PharmacySummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else if (report === ReportNames.NonCreditableSummary)
      {
         reportCriteriaDetail = NonCreditableSummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else if (report === ReportNames.ManufacturerSummary)
      {
         reportCriteriaDetail = ManufacturerSummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else if (report === ReportNames.ReturnToStock)
      {
         reportCriteriaDetail = ReturnToStockDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);

      }
      else
      {
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?NDCNumber=" + ndc + "&ReportDataTimeZone=" + reportDataTimeZone + "&ReportScheduleTimeZone=" + reportScheduleTimeZone + "&FileType=" + fileType + "&Isdownload=" + true);
      }
   }
});
//Anchor tabs (Detail,History and Preview) functionality
$("#anchorDetail").click(function (event)
{
   $("#reportPageNumber").hide();
   $("#gridData").show();
   $("#reportScheduleHistoryHeading").hide();
   $("#reportPreviewHeading").hide();
   $("#reportScheduleHeading").show();
   $("#reportImageVideoHeading").hide();

   if ($("#hdnEditNewClicked").val() !== "")
   {
      return;
   }

   $("#anchorImagesVideos").removeClass("active");
   $("#anchorPreview").removeClass("active");
   $("#anchorDetail").addClass("active");
   $("#anchorHistory").removeClass("active");

   $("#dvReportPreview").hide();
   $("#dvReportPreviewXLS").hide();
   $("#dvJqxgrid_ReportImageVideo").hide();
   DisableToolbarControl("#btn_toolbar_download");

   $(".report-wrapper").show();

   DisableEnableToolbarControlAfterDetail();
   localStorage["History"] = "";
   event.stopPropagation();
});

$("#anchorHistory").click(function (event)
{
   DisableToolbarControl("#btn_toolbar_grid");
   $("#reportPageNumber").hide();

   if ($("#hdnEditNewClicked").val() !== "")
   {
      PendingChanges();
   }
   else
   {
      if ($("#hdnReportScheduleId").val() === "")
      {
         ScheduleNotSelect();
         return;
      }

      $("#gridData").show();
      $("#reportScheduleHeading").hide();
      $("#reportPreviewHeading").hide();
      $("#reportImageVideoHeading").hide();
      $("#reportScheduleHistoryHeading").show();

      $("#anchorImagesVideos").removeClass("active");
      $("#anchorPreview").removeClass("active");
      $("#anchorDetail").removeClass("active");
      $("#anchorHistory").addClass("active");

      $(".report-wrapper").show();

      $("#dvReportPreview").hide();
      $("#dvReportPreviewXLS").hide();
      $("#dvJqxgrid_ReportImageVideo").hide();
      CreateReportStatusGridDesign();
      localStorage["History"] = "yes";
      DisableEnableToolbarAfterHistory();
      //DisableToolbarControl("#btn_toolbar_search");
      //DisableToolbarControl("#btn_toolbar_Exclamation");

   }
   event.stopPropagation();
});

var report = _ReportName;
$("#anchorPreview").click(function (event)
{
   PreviewClicked(event);
});

$("#btn_toolbar_Run").click(function (event)
{
   $("#tab-container").easytabs("select", "#tabs-preview");
   PreviewClicked(event);
});

function PreviewClicked(event)
{
   if ($("#hdnEditNewClicked").val() !== "")
   {
      PendingChanges();
   }
   else
   {
      if ($("#hdnReportScheduleId").val() === "")
      {
         ScheduleNotSelect();
         return;
      }

      DisableToolbarControl("#btn_toolbar_grid");
      //DisableToolbarControl("#btn_toolbar_search");
      $("#gridData").hide();
      $("#reportScheduleHistoryHeading").hide();
      $("#reportScheduleHeading").hide();
      $("#reportPreviewHeading").show();
      $("#reportImageVideoHeading").hide();
      $("#anchorPreview").addClass("active");
      $("#anchorDetail").removeClass("active");
      $("#anchorHistory").removeClass("active");
      $("#anchorImagesVideos").removeClass("active");
      $("#dvJqxgrid_ReportImageVideo").hide();
      $("#dvReportPreview").show();
      $(".report-wrapper").hide();
      //DisableToolbarControl("#btn_toolbar_Exclamation");
      EnableToolbarControl("#btn_toolbar_download");

      var fileType = $("select#FileType option:selected").text();
      GenerateReportAsPerFileType(fileType, false);
   }

   event.stopPropagation();
}
//Image/Video Click
$("#anchorImagesVideos").click(function ()
{
   $("#reportPageNumber").hide();
   if ($("#hdnEditNewClicked").val() !== "")
   {
      PendingChanges();
   }
   else
   {
      DisableEnableToolbarAfterImageVideo();
      $("#gridData").hide();
      $("#reportScheduleHistoryHeading").hide();
      $("#reportScheduleHeading").hide();
      $("#reportPreviewHeading").hide();
      $("#reportImageVideoHeading").show();

      $("#anchorImagesVideos").addClass("active");
      $("#anchorPreview").removeClass("active");
      $("#anchorDetail").removeClass("active");
      $("#anchorHistory").removeClass("active");


      $("#dvReportPreview").hide();
      $("#dvJqxgrid_ReportPreviewXLS").hide();
      $("#dvJqxgrid_ReportPreviewCSV").hide();
      $("#dvReportPreviewXLS").show();
      $("#dvJqxgrid_ReportImageVideo").show();

      $(".report-wrapper").hide();
      //DisableToolbarControl("#btn_toolbar_Exclamation");

      ShowLoading();

      var fileType = "XLS";
      var report = _ReportName;
      var reportCriteriaDetail = "";

      if (report === ReportNames.ProductDetails)
      {
         reportCriteriaDetail = productDetailCriteria();
      }
      else if (report === ReportNames.WasteReport)
      {
         reportCriteriaDetail = wasteReportCriteria();
      }
      else if (report === ReportNames.PharmacySummary)
      {
         reportCriteriaDetail = PharmacySummaryDetails();
      }
      else if (report === ReportNames.NonCreditableSummary)
      {
         reportCriteriaDetail = NonCreditableSummaryDetails();
      }
      else if (report === ReportNames.ManufacturerSummary)
      {
         reportCriteriaDetail = ManufacturerSummaryDetails();
      }
      else if (report === ReportNames.ReturnToStock)
      {
         reportCriteriaDetail = ReturnToStockDetails();
      }

      CreateReportImageVideoDesign(report, fileType, false, JSON.stringify(reportCriteriaDetail));
   }

});
//Function to remove ampersand from json data
function RemoveAmpersandFromJson(reportCriteriaDetail)
{
   reportCriteriaDetail = JSON.stringify(reportCriteriaDetail);
   reportCriteriaDetail = reportCriteriaDetail.replace(/&/g, "%26");
   return reportCriteriaDetail;
}

//Report Name: Product Detail
//Function to get product detail criteria
function productDetailCriteria()
{
   var retailerProductDetail =
      {
         NDCNumber: $("[name=textbox-NDCNumber]").val(),
         StoreName: $("[name=textbox-StoreName]").val(),
         StoreNumber: $("[name=ddl-StoreNumber]").val(),
         VendorName: $("[name=textbox-VendorName]").val(),
         ProductDescription: $("[name=textbox-ProductDescription]").val(),
         LotNumber: $("[name=textbox-LotNumber]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val(),
         ExpDate: $("[name=datepicker-ExpDate]").val(),
         FullQty: $("[name=textbox-FullQty]").val(),
         PartialQty: $("[name=textbox-PartialQty]").val(),
         UnitPriceBefore: $("[name=textbox-UnitPriceBefore]").val(),
         CreditableAmountBeforeMFGDiscount: $("[name=textbox-CreditableAmountBeforeMFGDiscount]").val(),
         ReturnStatus: $("[name=textbox-ReturnStatus]").val(),
         OutofpolicyDescription: $("[name=textbox-OutofpolicyDescription]").val(),
         DataStartDate: $("[name=datepicker-DataStartDate]").val(),
         DataEndDate: $("[name=datepicker-DataEndDate]").val(),
         Strength: $("[name=textbox-Strength]").val(),
         ItemGuid: $("[name=textbox-ItemGuid]").val(),
         QoskProcessDate: $("[name=datepicker-QoskProcessDate]").val(),
         ControlNumber: $("[name=textbox-ControlNumber]").val(),
         PackageSize: $("[name=textbox-PackageSize]").val(),
         UnitPriceAfter: $("[name=textbox-UnitPriceAfter]").val(),
         CreditableAmountAfter: $("[name=textbox-CreditableAmountAfter]").val(),
         RecalledProduct: $("[name=textbox-RecalledProduct]").val(),
         DiscontinuedProduct: $("[name=textbox-DiscontinuedProduct]").val(),
         RXorOTC: $("[name=textbox-RXorOTC]").val(),
         DosageForm: $("[name=textbox-DosageForm]").val(),
         PackageForm: $("[name=textbox-PackageForm]").val(),
         PartialPercentage: "",
         ExtendedDiscountPercent: ""
      };
   return retailerProductDetail;
}

//Report Name: Waste Report
//Function to get Waste Report criteria
function wasteReportCriteria()
{
   var wasteReportDetail =
      {
         NDCNumber: $("[name=textbox-NDCNumber]").val(),
         StoreName: $("[name=textbox-StoreName]").val(),
         StoreNumber: $("[name=textbox-StoreNumber]").val(),
         ItemGuid: $("[name=textbox-ItemGuid]").val(),
         ProductDescription: $("[name=textbox-ProductDescription]").val(),
         LotNumber: $("[name=textbox-LotNumber]").val(),
         Strength: $("[name=textbox-Strength]").val(),
         FullQty: $("[name=textbox-FullQty]").val(),
         ControlNumber: $("[name=textbox-ControlNumber]").val(),
         DosageForm: $("[name=textbox-DosageForm]").val(),
         PackageForm: $("[name=textbox-PackageForm]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val(),
         ExpDate: $("[name=datepicker-ExpDate]").val(),
         PackageSize: $("[name=textbox-PackageSize]").val(),
         WasteStreamProfile: $("[name=textbox-WasteStreamProfile]").val(),
         WasteCode: $("[name=textbox-WasteCode]").val(),
         SealedOpenCase: $("[name=textbox-SealedOpenCase]").val(),
         DataStartDate: $("[name=datepicker-DataStartDate]").val(),
         DataEndDate: $("[name=datepicker-DataEndDate]").val(),
         PartialQty: $("[name=textbox-PartialQty]").val()

      };
   return wasteReportDetail;
}
//Report Name: Pharmacy Summary
//Function to get Pharmacy Summary criteria
function PharmacySummaryDetails()
{
   var pharmacySummaryDetail =
      {
         StoreNumber: $("[name=textbox-StoreNumber]").val(),
         StoreName: $("[name=textbox-StoreName]").val(),
         WholesalerCustomerNumber: $("[name=textbox-WholesalerCustomerNumber]").val(),
         RegionName: $("[name=textbox-RegionName]").val(),
         ReturnCreditableValue: $("[name=textbox-ReturnCreditableValue]").val(),
         ReturnCreditableQty: $("[name=textbox-ReturnCreditableQty]").val(),
         RecallCreditableValue: $("[name=textbox-RecallCreditableValue]").val(),
         RecallCreditableQty: $("[name=textbox-RecallCreditableQty]").val(),
         NonCreditableValue: $("[name=textbox-NonCreditableValue]").val(),
         NonCreditableQty: $("[name=textbox-NonCreditableQty]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val(),
         TotalProductValue: $("[name=textbox-TotalProductValue]").val(),
         TotalQty: $("[name=textbox-TotalQty]").val(),
         ProcessingFeeBilled: $("[name=textbox-ProcessingFeeBilled]").val(),
         TotalInvoiceAmount: $("[name=textbox-TotalInvoiceAmount]").val(),
         TotalDebitMemoQty: $("[name=textbox-TotalDebitMemoQty]").val(),
         ReturnToStockValue: $("[name=textbox-ReturnToStockValue]").val(),
         ReturnToStockQty: $("[name=textbox-ReturnToStockQty]").val()
      };
   return pharmacySummaryDetail;
}
//Report Name: Non Creditable Summary
//Function to get Non Creditable Summary criteria
function NonCreditableSummaryDetails()
{
   var nonCreditableSummaryDetail =
      {
         StoreNumber: $("[name=textbox-StoreNumber]").val(),
         StoreName: $("[name=textbox-StoreName]").val(),
         RegionName: $("[name=textbox-RegionName]").val(),
         NonCreditableValue: $("[name=textbox-NonCreditableValue]").val(),
         NonCreditableQty: $("[name=textbox-NonCreditableQty]").val(),
         ManufacturerName: $("[name=textbox-ManufacturerName]").val(),
         NonCreditableReasonCode: $("[name=textbox-NonCreditableReasonCode]").val(),
         NonCreditableReasonDescription: $("[name=textbox-NonCreditableReasonDescription]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val()
      };
   return nonCreditableSummaryDetail;
}
//Report Name: Manufacturer Summary
//Function to get Manufacturer Summary criteria
function ManufacturerSummaryDetails()
{
   var manufacturerSummaryDetail =
      {
         MFGLabeler: $("[name=textbox-MFGLabeler]").val(),
         ManufacturerName: $("[name=textbox-ManufacturerName]").val(),
         RetailerVendorNumber: $("[name=textbox-RetailerVendorNumber]").val(),
         DebitMemoInvoiceNo: $("[name=textbox-DebitMemoInvoiceNo]").val(),
         CreditableValue: $("[name=textbox-CreditableValue]").val(),
         CreditableQty: $("[name=textbox-CreditableQty]").val(),
         RecallCreditableValue: $("[name=textbox-RecallCreditableValue]").val(),
         RecallCreditableQty: $("[name=textbox-RecallCreditableQty]").val(),
         TotalProductValue: $("[name=textbox-TotalProductValue]").val(),
         TotalQty: $("[name=textbox-TotalQty]").val(),
         ProcessingFee: $("[name=textbox-ProcessingFee]").val(),
         TotalInvoiceAmount: $("[name=textbox-TotalInvoiceAmount]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val()
      };
   return manufacturerSummaryDetail;
}
//Report Name: Return To Stock
//Function to get Return To Stock criteria
function ReturnToStockDetails()
{
   var returnToStockDetail =
      {
         VendorName: $("[name=textbox-VendorName]").val(),
         StoreName: $("[name=textbox-StoreName]").val(),
         StoreNumber: $("[name=textbox-StoreNumber]").val(),
         NDCNumber: $("[name=textbox-NDCNumber]").val(),
         ProductDescription: $("[name=textbox-ProductDescription]").val(),
         Strength: $("[name=textbox-Strength]").val(),
         ControlNumber: $("[name=textbox-ControlNumber]").val(),
         RXorOTC: $("[name=textbox-RXorOTC]").val(),
         DosageForm: $("[name=textbox-DosageForm]").val(),
         PackageForm: $("[name=textbox-PackageForm]").val(),
         LotNumber: $("[name=textbox-LotNumber]").val(),
         ExpDate: $("[name=datepicker-ExpDate]").val(),
         QoskProcessDate: $("[name=datepicker-QoskProcessDate]").val(),
         DateEligibleForCredit: $("[name=datepicker-DateEligibleForCredit]").val(),
         OutofpolicyDescription: $("[name=textbox-OutofpolicyDescription]").val(),
         ReportDataTimeZone: $("#ReportDataTimeZone").val(),
         ReportScheduleTimeZone: $("#ReportScheduleTimeZone").val()
      };
   return returnToStockDetail;
}
//Function for pending changes(Run when user edit or add new record and click on grid then this function show the prompt)
function PendingChanges()
{
   $(".alert-popup-body-message-confirm").hide();
   $("#alertmessageid").text("You have pending changes. Please save or cancel.");
   $(".alert-popup-body-message").show();

   $("#btnok").click(function ()
   {
      $(".alert-popup-body-message").hide();
   });
}
//Function for ScheduleNotSelect
function ScheduleNotSelect()
{
   $(".alert-popup-body-message-confirm").hide();
   $("#alertmessageid").text("Please select a schedule.");
   $(".alert-popup-body-message").show();

   $("#btnok").click(function ()
   {
      $(".alert-popup-body-message").hide();
   });
}
//Function for edit scheduled report information
function EditScheduleGrid(scheduleId)
{
   //ignore message when we add new report.
   if ($("#hdnEditNewClicked").val() !== "" && scheduleId !== 0 && isExclamationEnable === false)
   {
      PendingChanges();
   }
   else
   {
      var timeZone = null;

      if (scheduleId === 0)
      {
         timeZone = new Date().toTimeString();
      }
      $.ajax({
         type: "Get",
         cache: false,
         contentType: "application/json; charset=utf-8",
         url: resolveUrl() + "/Home/EditReportScheduler",
         data: { ReportScheduleId: scheduleId, timeZone: timeZone },
         success: function (data)
         {
            $("#frmReportScheduler").empty().html(data);
         },
         error: function (response)
         {
            if (response.status !== 0 && response.status !== 401)
            {
               alert(response.status + " " + response.statusText);
            }
         },
         complete: function ()
         {
            DisableEnableToolbarControlAfterDetail();
            if (scheduleId === -1)
            {
               $("#divReportScheduler :input").prop("disabled", true);
               $("#SearchPanelList :input").prop("disabled", true);
               $("#divReportScheduler td.tdSeparatorB,td.tdSeparatorA").css("background-color", "#fff");
               //EnableToolbarControl("#btn_toolbar_Exclamation");
            }
            else if (scheduleId !== -1 && scheduleId !== 0 && isExclamationEnable === false)
            {
               $("#divReportScheduler :input").prop("disabled", true);
               $("divReportScheduler td.tdSeparatorB,td.tdSeparatorA").css("background-color", "#fff");
            }
            else if (scheduleId === 0)
            {
               $("#divReportScheduler :input").removeAttr("readonly");
               $("#SearchPanelList :input").removeAttr("readonly");
               $("#divReportScheduler td.tdSeparatorB,td.tdSeparatorA").css("background-color", "#ECDD9D");
               //DisableToolbarControl("#btn_toolbar_Exclamation");
            }
         }
      });
   }
}
//Function for disable toolbar control
window.DisableToolbarControl = function disableToolbarControl(controlId)
{
   if (controlId === "#btn_toolbar_cancel")
   {
      $(controlId).css("color", "#3b8aBd");
   }

   if (controlId === "#btn_toolbar_Run")
   {
      $(controlId).attr("src", "/images/Actionbar/ActionBar_Run(0).svg");
   }
   else
   {
      $(controlId).attr("src", $(controlId).attr("src").replace("1", "0"));
   }

   $(controlId).css("pointer-events", "none");
   $(controlId).removeClass("darktxt");
   $(controlId).addClass("lighttxt");
};
//Function for enable toolbar control
window.EnableToolbarControl = function enableToolbarControl(controlId)
{
   if (controlId === "#btn_toolbar_cancel")
   {
      $(controlId).css("color", "darkred");
   }

   $(controlId).attr("src", $(controlId).attr("src").replace("0", "1"));
   $(controlId).css("pointer-events", "auto");
   $(controlId).removeClass("lighttxt");
   $(controlId).addClass("darktxt");
};

//function for disable enable toolbar control value after click
function DetailToolbar()
{
   EnableToolbarControl("#btn_toolbar_refresh");
   //EnableToolbarControl("#btn_toolbar_zoomIn");
   //EnableToolbarControl("#btn_toolbar_zoomOut");
   EnableToolbarControl("#btn_toolbar_new");
   EnableToolbarControl("#btn_toolbar_edit");
   EnableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   EnableToolbarControl("#btn_toolbar_Run");
}
function EnableOnlyNewInToolbar()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   //DisableToolbarControl("#btn_toolbar_zoomIn");
   //DisableToolbarControl("#btn_toolbar_zoomOut");
   EnableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   DisableToolbarControl("#btn_toolbar_Run");
}
var rowscounts = "";
function DisableEnableToolbarControlAfterDetail()
{
   DisableToolbarControl("#btn_toolbar_download");
   //EnableToolbarControl("#btn_toolbar_search");
   EnableToolbarControl("#btn_toolbar_grid");
   //EnableToolbarControl("#btn_toolbar_Exclamation");
   if ($("#hdnEditNewClicked").val() === "Edit_New")
   {
      DisableEnableToolbarAfterAddNewAndEdit();
   }
   else if ($("#datagridtable").DataTable().rows().data()[0] === undefined)
   {
      EnableOnlyNewInToolbar();
      rowscounts = 0;
   }
   else
   {
      DetailToolbar();
   }

}
function DisableEnableToolbarAfterAddNewAndEdit()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   //DisableToolbarControl("#btn_toolbar_zoomIn");
   //DisableToolbarControl("#btn_toolbar_zoomOut");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_delete");
   EnableToolbarControl("#btn_toolbar_save");
   EnableToolbarControl("#btn_toolbar_cancel");
   DisableToolbarControl("#btn_toolbar_download");
   DisableToolbarControl("#btn_toolbar_Run");
}
function EnableScheduleAndCriteriaAfterEdit()
{
   $("#divReportScheduler :input").prop("disabled", false).removeAttr("readonly");
   $("#SearchPanelList :input").prop("disabled", false).removeAttr("readonly");
   $("#divReportScheduler td.tdSeparatorB,td.tdSeparatorA").css("background-color", "#ECDD9D");
   $("#divReportScheduler :input[data-val-date]").css("cursor", "pointer");
   $("#divReportParameter :input").css("background-color", "#ECDD9D");
   $("#SearchPanelList :input").css("cursor", "text");
   $("#SearchPanelList :input.autocomplete").attr("placeholder", "auto complete");

   //DisableToolbarControl("#btn_toolbar_Exclamation");

}
function DisableEnableToolbarAfterHistory()
{
   EnableToolbarControl("#btn_toolbar_refresh");
   //DisableToolbarControl("#btn_toolbar_zoomIn");
   //DisableToolbarControl("#btn_toolbar_zoomOut");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   DisableToolbarControl("#btn_toolbar_download");
   EnableToolbarControl("#btn_toolbar_Run");
}
function DisableEnableToolbarAfterPreview()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   //DisableToolbarControl("#btn_toolbar_zoomIn");
   //DisableToolbarControl("#btn_toolbar_zoomOut");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   EnableToolbarControl("#btn_toolbar_download");
   EnableToolbarControl("#btn_toolbar_Run");
}
function DisableEnableToolbarAfterImageVideo()
{
   DisableToolbarControl("#btn_toolbar_refresh");
   //DisableToolbarControl("#btn_toolbar_zoomIn");
   //DisableToolbarControl("#btn_toolbar_zoomOut");
   DisableToolbarControl("#btn_toolbar_new");
   DisableToolbarControl("#btn_toolbar_edit");
   DisableToolbarControl("#btn_toolbar_delete");
   DisableToolbarControl("#btn_toolbar_save");
   DisableToolbarControl("#btn_toolbar_cancel");
   DisableToolbarControl("#btn_toolbar_download");
   DisableToolbarControl("#btn_toolbar_grid");
   //DisableToolbarControl("#btn_toolbar_search");
   EnableToolbarControl("#btn_toolbar_Run");
}
//Function for load top row schedule details
function LoadTopRowScheduleDetails(datarow)
{
   if (datarow === undefined)
   {
      EditScheduleGrid(-1);
      CreateReportSearchPanel("");
   }
   else
   {
      EditScheduleGrid(datarow["ReportScheduleID"]);
      $("#hdnReportScheduleId").val(datarow["ReportScheduleID"]);
      CreateReportSearchPanel($("#hdnReportScheduleId").val());
   }
}
//Function for create report schedule grid design
function CreateReportScheduleGridDesign()
{
   $.ajax({
      type: "Get",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/BindReportSchedulerGrid",
      cache: false,
      data: { ReportId: $("#hdnReportid").val() },
      success: function (data)
      {
         GenerateScheduleGridData(data);
      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      }
   }).done(function ()
   {
      LoadTopRowScheduleDetails($("#datagridtable").DataTable().rows().data()[0]);
      //$("table.dataTable").removeAttr("style");
      //$("table.dataTable").css("width", "100%");
      //$("div.dataTables_scrollHeadInner").css({ "width": "100%", "padding-right": "0px" });
      $("#tabs-scheduler").css("overflow-y", "none");
   });
}

function GenerateScheduleGridData(json)
{

   $("#datagridtable")
      .DataTable({
         scrollX: true,
         scrollCollapse: true,
         fixedColumns: true,
         height: "auto",
         data: json,
         columns: [
            { title: "Report ID", data: "ReportScheduleID" },
            { title: "Report Title", data: "ReportScheduleTitle" },
            //{ title: "Schedule", data: "Schedule" },
            { title: "Date Range", data: "DateRange" },
            { title: "Store Number", data: "StoreNumber" }
            //{ title: "Criteria", data: "Criteria" }
         ],
         columnDefs: [
            {
               targets: [0],
               visible: false
            }
         ],
         select: true
      });
   //$("table.dataTable").removeAttr("style");
   $("table.dataTable").css("width", "100%");
   $("div.dataTables_scrollHeadInner").css("width", "100%");
   $("#tabs-scheduler").css("overflow-y", "none");
   //$("#datagridtable").width("");

   $("#datagridtable").DataTable().rows().nodes().to$().attr("data-unique-selection", "schedule");
   $("#datagridtable").DataTable().rows().nodes().to$().addClass("selectable");

   $("#datagridtable tbody tr:first").addClass("selected");

   $(document).on("click", "#datagridtable tbody tr", function ()
   {
      if ($("#hdnEditNewClicked").val() !== "")
      {
         PendingChanges();
      }
      else
      {
         changeSelection(this);
         LoadTopRowScheduleDetails($("#datagridtable").DataTable().rows(this).data()[0]);
      }
   });

   $("#historyTable")
      .DataTable({
         scrollX: true,
         scrollCollapse: true,
         fixedColumns: true,
         height: "auto",
         columns: [
            { title: "Report ID", data: "ReportId" },
            { title: "Report Name", data: "ReportTitle" },
            { title: "Report Status", data: "Status" },
            { title: "Status Description", data: "StatusDescription" },
            { title: "Report Run Time", data: "RunTime", width: "20%" }
         ],
         order: [[4, "desc"]],
         columnDefs: [
            {
               targets: [0],
               visible: false
            }
         ]
      });
}
// Function for create report status grid design
function CreateReportStatusGridDesign()
{
   var timeZone = new Date().toTimeString();
   $("#currentTimeZone").text(timeZone.substr(timeZone.indexOf("GMT")).replace("GMT", "UTC"));
   $.ajax({
      type: "Get",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/BindReportRunGrid",
      cache: false,
      data: { ReportScheduleID: $("#hdnReportScheduleId").val(), timeZone: timeZone }, // Note it is important
      success: function (data)
      {
         GenerateReportStatusGridData(data);
      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      }, beforeSend: function ()
      {
         ShowLoading();
      },
      complete: function ()
      {
         HideLoading();
      }
   });
   $(".grid-details").hide();
   $(".grid-btn-prev").hide();
   $(".grid-btn-next").hide();
}
function GenerateReportStatusGridData(json)
{
   $("#historyTable").DataTable().clear();
   json.forEach(function (row, index)
   {
      $("#historyTable").DataTable().rows.add([row]);
   });
   $("#historyTable").DataTable().order([[4, "desc"]]).draw(false);
   //$("#historyTable").width("");
   //$("table.dataTable").removeAttr("style");
   //$("table.dataTable").css("width", "100%");
   //$("div.dataTables_scrollHeadInner").css("width", "100%");
}

//Preview section for XLS
function CreateReportPreviewGridDesign(report, fileType, isDownload, reportCriteriaDetail)
{
   var reportScheduleId = $("#hdnReportScheduleId").val();
   $.ajax({
      type: "Get",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/" + report,
      cache: false,
      data: { FileType: fileType, Isdownload: isDownload, reportCriteriaDetail: reportCriteriaDetail, reportScheduleId: reportScheduleId }, // Note it is important
      success: function (data)
      {
         GenerateReportPreviewGridData(data.reportRetailerBoxDetails, data.gridColumns);
      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      },
      beforeSend: function ()
      {
         ShowLoading();
      },
      complete: function ()
      {
         HideLoading();
      }
   });
   $(".grid-details").hide();
   $(".grid-btn-prev").hide();
   $(".grid-btn-next").hide();
}

//Bind Grid for XLS 
function GenerateReportPreviewGridData(json, columns)
{
   if (!$("#anchorPreview").hasClass("active"))
   {
      return false;
   }

   $("#dvJqxgrid_ReportPreviewXLS").css("height", $("#hdnGridHeight").val() + "px");
   var rows = json;
   var source =
      {
         datatype: "json",
         id: "id",
         localdata: rows
      };
   var dataAdapter = new $.jqx.dataAdapter(source,
      {
         loadComplete: function ()
         {
            var length = dataAdapter.records.length;
            $("#hdnTotalRecords").val(parseInt(length));

         }
      });

   $("#dvJqxgrid_ReportPreviewXLS")
      .jqxGrid(
      {
         width: "100%",
         height: "100%",
         autowidth: false,
         sortable: true,
         filterable: true,
         pageable: false,
         columnsreorder: true,
         source: dataAdapter,
         columnsresize: true,
         theme: "koalityblue",
         columns: columns
      });

   //$("#dvJqxgrid_ReportPreviewXLS").jqxGrid({ columns: columns });
   //$("#dvJqxgrid_ReportPreviewXLS").jqxGrid("render");

   // initialize the popup window and buttons.

}

//Render CSV data
function GenerateCSVDataGrid(report, fileType, isDownload, reportCriteriaDetail)
{
   var reportScheduleId = $("#hdnReportScheduleId").val();
   $.ajax({
      type: "Get",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/" + report,
      cache: false,
      data: { FileType: fileType, Isdownload: isDownload, reportCriteriaDetail: reportCriteriaDetail, reportScheduleId: reportScheduleId },
      success: function (data)
      {
         if (data !== null && data.reportRetailerBoxDetails !== null && data.gridColumns !== null)
         {
            var table = "";
            var tr = "";

            if (data.reportRetailerBoxDetails.length !== 0)
            {
               for (var i = 0; i < data.reportRetailerBoxDetails.length; i++)
               {
                  tr += "<p>";
                  for (var col = 0; col < data.gridColumns.length; col++)
                  {
                     var val = data.reportRetailerBoxDetails[i][data.gridColumns[col].datafield];
                     if (val === null)
                     {
                        val = "";
                     }

                     tr += val + ",";
                  }

                  if (tr.length > 3)
                  {
                     tr = tr.substr(0, tr.length - 1);
                  }

                  tr += "</p>";
               }

               table += tr;
               $("#dvJqxgrid_ReportPreviewCSV").empty().append(table);
            }
            else
            {
               table += "<p style=\"text-align: center\;color:red\"><b>No data to display</b></p>";
               $("#dvJqxgrid_ReportPreviewCSV").empty().append(table);
            }
         }

      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      },
      beforeSend: function ()
      {
         ShowLoading();
      },
      complete: function ()
      {
         HideLoading();
      }
   });

}
// Functions to use 
function isReportIframeReady()
{
   if ($("#ifReport").length > 0 && $("#ifReport").is(":visible"))
   {
      return true;
   }

   return false;
}

function ReportPageCount()
{
   if (isReportIframeReady() === true)
   {
      return document.getElementById("ifReport").contentWindow.PageCount();
   }

   return 0;
}
function ReportCurrentPageNumber()
{
   if (isReportIframeReady() === true)
   {
      return document.getElementById("ifReport").contentWindow.CurrentPageNumber();
   }

   return 0;
}

function GoToNextPage()
{
   document.getElementById("ifReport").contentWindow.GoToNextPage();
}

function GoToPreviousPage()
{
   document.getElementById("ifReport").contentWindow.GoToPreviousPage();
}

function ExportReport()
{
   var fileType = $("select#FileType option:selected").text();

   if (typeof (fileType) !== "undefined" && fileType !== "Select")
   {
      document.getElementById("ifReport").contentWindow.ExportReport(fileType);
   }
}


function ChangeReportName()
{
   var reportName = _ReportName;
   $("#hdnReportName").val(reportName);
}

window.ShowReportNumber = function ShowReportNumber(numbertext)
{
   $("#reportPageNumber").show();
   $("#reportPageNumber").text(numbertext);
};
//If unauthorized error occurred in any Ajax call, we navigate user to login page.
$(document).ajaxError(function (xhr, response)
{
   console.log(xhr);

   if (response.status === 401)
   {
      window.location.reload();
   }
});




var imageWidth = 0;
var imageHeight = 0;
//Zoom in functionality for pop-up
$("#btn_Image_zoomIn").click(function ()
{
   imageWidth = parseFloat($("#displayImage .image-popup-text img").css("width"));
   imageHeight = parseFloat($("#displayImage .image-popup-text img").css("height"));

   if (imageWidth > 1200)
   {
      return;
   }

   imageWidth = imageWidth + 200;
   imageHeight = imageHeight + 100;
   $("#displayImage .image-popup-text img").css("width", imageWidth);
   $("#displayImage .image-popup-text img").css("height", imageHeight);



});
//Zoom out functionality for pop-up
$("#btn_Image_zoomOut").click(function ()
{

   imageWidth = parseFloat($("#displayImage .image-popup-text img").css("width"));
   imageHeight = parseFloat($("#displayImage .image-popup-text img").css("height"));

   if (imageWidth <= 634)
   {
      return;
   }

   imageWidth = imageWidth - 200;
   imageHeight = imageHeight - 100;
   $("#displayImage .image-popup-text img").css("width", imageWidth);
   $("#displayImage .image-popup-text img").css("height", imageHeight);

});
//Close event functionality for send email pop-up
$("#btnCancel,.alert-close, .email-close ").click(function ()
{
   $("#sendEmail").hide();
});
//Close event functionality for display Image and video pop-up
$(".alert-close").click(function ()
{
   $("#displayImage").hide();
   $(".video_box").removeAttr("controls", "controls");
});
//Function for preview images videos data
function PreviewImagesVideosData(json, columns)
{
   var rows = json;

   var source =
      {
         datatype: "json",
         id: "id",
         localdata: rows
      };
   var dataAdapter = new $.jqx.dataAdapter(source,
      {
         loadComplete: function ()
         {
            var length = dataAdapter.records.length;
         }
      });

   var initrowdetails = function (index, parentElement, gridElement, record)
   {
      Details = record;
      $.ajax({
         url: resolveUrl() + "/Home/DisplayImageAndVideo",
         cache: false,
         type: "Get",
         data: { ItemGuid: record.ItemGuid },
         success: function (result)
         {
            var isAllUri = false;
            var defaultImage = '<img style="margin:2px; margin-left: 10px;" max-width="100" max-height="100" src=/Areas/Report/images/Defaultimages.jpg />';
            var defaultVideo = '<video style="margin:2px; margin-left: 10px;" max-width="100" max-height="70" poster=/Areas/Report/images/no-video.gif />';
            var prescriptionHippaImage = '<td valign="top"><img   style="margin:0px; margin-left: 10px;display:block;margin-top:15px;background:none" max-width="100" max-height="100" src="/Images/ResizedHIPAA.png" /></td>';
            var htmlForImage = "";

            if (result !== null && typeof (result) !== "undefined" && result.length > 0)
            {
               htmlForImage = '<table class="ImageTable" style="background-color:#eff0f1"><tr>';
               for (var i = 0; i < result.length; i++)
               {
                  if (result[i].ContentType === "Images")
                  {
                     if (record.ContainerTypeName.indexOf("Prescription") !== -1 && htmlForImage.indexOf("ResizedHIPAA") === -1)
                     {
                        htmlForImage += prescriptionHippaImage;
                     }

                     htmlForImage += '<td valign="top"><input class="login-checkbox" type="checkbox"/><img onclick="window.ShowImage(this)"  class="side"  style="margin:0px; margin-left: 10px;display:block" max-width="100" max-height="100" title="' + result[i].ContentName + '" src="' + result[i].ContentSasURI + '" /><span>' + result[i].ContentSasTimeStamp + '</span></td>';

                     if (isUri(result[i].ContentSasURI))
                     {
                        isAllUri = true;
                     }

                  }
                  else if (result[i].ContentType === "Videos")
                  {
                     if (result[i].ContentName.indexOf(".mp4") !== -1)
                     {
                        htmlForImage += '<td valign="top"><input class="login-checkbox" type="checkbox"/><video preload="auto" onclick="window.ShowVideo(this)" class="side video_box customvideo"  style="margin:0px; margin-left: 10px;display:block" max-width="200" max-height="100" title="' + result[i].ContentName + '" src="' + result[i].ContentSasURI + '"  type="video/mp4" /><span>' + result[i].ContentSasTimeStamp + '</span></td>';
                     }
                     else
                     {
                        htmlForImage += "<td>" + defaultVideo + "</td>";
                     }

                  }
                  else
                  {
                     htmlForImage += "<td>" + defaultImage + "</td>";
                  }
               }

            }
            var htmlImageUpdated;

            if (isAllUri)
            {
               btnSendEmail = '<input id="btnOpenSendEmailPopup" class="btn-primary" type="button" value="Email"/>';
               htmlImageUpdated = btnSendEmail + htmlForImage;
            }
            else
            {
               htmlImageUpdated = htmlForImage;
            }

            $("#grid" + index).empty().append(htmlImageUpdated);

         }
         ,
         beforeSend: function ()
         {
            var defaultHtml = '<table class="ImageTable" style=""><tr>' +
               '<td><img  style="margin:2px; margin-left: 10px;" max-width="100" max-height="100" /></td>' +
               "</tr></table>";
            $("#grid" + index).empty().append(defaultHtml);
         }
      });
   };

   function isUri(source)
   {
      return source.indexOf("http") > -1;
   }

   // creage jqxgrid
   $("#dvJqxgrid_ReportImageVideo")
      .jqxGrid(
      {
         width: "100%",
         height: "100%",
         source: source,
         sortable: true,
         autowidth: false,
         rowdetails: true,
         columnsresize: true,
         rowsheight: 25,
         pageable: false,
         initrowdetails: initrowdetails,
         rowdetailstemplate: { rowdetails: "<div id='grid' style='width:100%;overflow:auto'></div>", rowdetailsheight: 150, rowdetailshidden: true },
         theme: "koalityblue",
         columns: columns
      });

   Imagedialog = $("#window").dialog({
      autoOpen: false,
      height: 550,
      width: 850,
      maxHeight: 1024,
      maxWidth: 600,
      resizable: false,
      title: "Image",
      left: 0,
      modal: true,
      theme: "koalityblue",
      buttons: {
         Cancel: {
            text: "Close",
            click: function ()
            {
               Imagedialog.dialog("close");
            }
         }
      }
   });

   Videodialog = $("#videoPopup").dialog({
      autoOpen: false,
      height: 550,
      width: 850,
      maxHeight: 1024,
      maxWidth: 600,
      resizable: false,
      left: 0,
      modal: true,
      title: "Video",
      theme: "koalityblue",
      buttons: {
         Cancel: {
            text: "Close",
            click: function ()
            {
               Videodialog.dialog("close");
            }
         }
      }
   });


   //Function for display image and video in popup
   var contentType = "";
   window.ShowImage = function (ele)
   {
      $(".ui-dialog-titlebar-close").hide();
      $(".ui-dialog").css("border", "#125484 solid 2px");
      $(".ui-dialog-titlebar").css("background", "#2a669f");
      Imagedialog.dialog("open");
      console.log("created");
      contentType = ele.tagName;

      if (contentType === "IMG")
      {
         var clone = $(ele).clone();

         zoomVal = 0;
         $(".ui-dialog-title").html("Image | " + clone.attr("title"));
         $("#window").css("height", "490px");
         $(".element_to_magnify").css({ "overflow": "auto", "height": "488px", "text-align": "center" });
         $(".element_to_magnify").html(clone);
         $(".element_to_magnify img").css({ 'max-width': "833px", 'max-height': "445px", 'margin': "0px auto" });
         $(".element_to_magnify img").attr("onclick", "window.ZoomIn(this)");
      }

      $(".video_box").attr("controls", "controls");

   };
}

window.ShowVideo = function (ele)
{
   $(".ui-dialog-titlebar-close").hide();
   Videodialog.dialog("open");
   var clone = $(ele).clone();
   $(".ui-dialog-title").html("Video | " + clone.attr("title"));

   $("#videoPopup").html(clone);
   $("#videoPopup video").css({ 'width': "700px", 'height': "450px", 'margin': "0" });
   $("#videoPopup video").removeAttr("onclick").attr("controls", "controls");

};

window.ZoomIn = function (e)
{
   var h = e.height;
   var w = e.width;

   if (zoomVal === 0)
   {
      var orginalWidth = e.naturalWidth > 2000 ? 2000 : e.naturalWidth;
      var orginalHeight = e.naturalHeight > 2000 ? 2000 : e.naturalHeight;
      var zoomFactorX = Math.round((orginalWidth / w) * 100) / 100;
      var zoomFactorY = Math.round((orginalHeight / h) * 100) / 100;
      var parentOffset = e.offsetParent;
      var clickedX = event.clientX - $(".element_to_magnify img").offset().left;
      var clickedY = event.clientY - $(".element_to_magnify img").offset().top;
      var scaleX = clickedX * zoomFactorX;
      var scaleY = clickedY * zoomFactorY;

      $(".element_to_magnify img").css({ "max-width": "2000px", "max-height": "2000px" });
      $(".element_to_magnify").scrollLeft(scaleX - clickedX);
      $(".element_to_magnify").scrollTop((event.y + event.offsetY) - e.scrollHeight / zoomFactorY);

      zoomVal = 1;
   }
   else
   {
      $(".element_to_magnify img").css({ 'max-width': "833px", 'max-height': "445px" });
      zoomVal = 0;
   }
};

//*****************************************************************************
//*
//* Summary:
//*   Click event on the Email button under the Images tab; prepare email that contains product images and details
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$(document).on("click", "#btnOpenSendEmailPopup", function ()
{
   var imageHtml = "<table>";

   //grab the index of the row that the "Email" button belongs to
   //then get the product details based on the index captured (Details is a global variable)
   var id = $(this).closest("div").attr("id");
   id = id.replace("grid", "");
   var index = $("#dvJqxgrid_ReportImageVideo").jqxGrid("getrowboundindex", id);
   Details = $("#dvJqxgrid_ReportImageVideo").jqxGrid("getrowdata", index);

   //get all images that were checked
   $(this).closest("div").find("table.ImageTable img").each(function ()
   {
      var checked = $(this).prev("input[type=checkbox ]").prop("checked");

      if (checked)
      {
         imageHtml += '<tr><td><a style="color:blue" href="' + this.src + '">' + this.title + "</a></td></tr>";
      }
   });

   imageHtml += "</table>";

   var videoHtml = "<table>";

   //get all vidoes that were checked
   $(this).closest("div").find("table.ImageTable video").each(function ()
   {
      var checked = $(this).prev("input[type=checkbox ]").prop("checked");

      if (checked)
      {
         videoHtml += '<tr><td><a style="color:blue" href="' + this.src + '">' + this.title + "</a></td></tr>";
      }
   });

   videoHtml += "</table>";

   $("#sendEmail #divImage").empty().html(imageHtml);
   $("#sendEmail #divVideo").empty().html(videoHtml);
   $("#txtEmail").val("").focus();
   $("#sendEmail").show();
});

//*****************************************************************************
//*
//* Summary:
//*   Click event on the Send Email button
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************
$("#btnSendEmail").click(function ()
{
   var emailBody = [];
   var objImages;
   var objVideos;

   $("#divImage").find("a").each(function ()
   {
      objImages = { ContentType: "Images", ContentName: this.innerHTML, ContentSasURI: this.href };
      emailBody.push(objImages);
   });
   $("#divVideo").find("a").each(function ()
   {
      objVideos = { ContentType: "Videos", ContentName: this.innerHTML, ContentSasURI: this.href };
      emailBody.push(objVideos);
   });

   if (!emailBody.length)
   {
      AlertMessage("Please select at least one product image.", false);
      return;
   }

   var emails = $("#txtEmail").val().trim();

   if (emails === "")
   {
      $("#txtEmail").val("");
      AlertMessage("Please enter valid email id(s).", false);
      $("#txtEmail").focus();
      return;
   }

   var invalidEmails = ReturnInvalidEmails(emails);

   if (invalidEmails !== "")
   {
      AlertMessage(invalidEmails + " not valid email(s).", false);
      $("#txtEmail").focus();
      return;
   }

   $.ajax({
      type: "Post",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/SendEmail",
      cache: false,
      data: "{ 'EmailBody': '" + JSON.stringify(emailBody) + "', 'EmailList': '" + emails + "','Details':'" + JSON.stringify(Details) + "' }",
      //traditional: true,
      //async: false,
      success: function (data)
      {
         AlertMessage(data, true);
      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      },
      beforeSend: function ()
      {
         ShowLoading();
      },
      complete: function ()
      {
         HideLoading();
      }
   });
});
//Function for create report image video design
function CreateReportImageVideoDesign(report, fileType, isDownload, reportCriteriaDetail)
{
   var reportScheduleId = $("#hdnReportScheduleId").val() === "" ? 0 : $("#hdnReportScheduleId").val();
   $.ajax({
      type: "Get",
      contentType: "application/json; charset=utf-8",
      url: resolveUrl() + "/Home/" + report,
      cache: false,
      data: { FileType: fileType, Isdownload: isDownload, reportCriteriaDetail: reportCriteriaDetail, reportScheduleId: reportScheduleId }, // Note it is important
      success: function (data)
      {

         $("#dvJqxgrid_ReportImageVideo").jqxGrid("clear");
         $("#dvJqxgrid_ReportImageVideo").jqxGrid("render");
         $("#dvJqxgrid_ReportImageVideo").jqxGrid("updatebounddata");
         PreviewImagesVideosData(data.reportRetailerBoxDetails, data.gridColumns);
      },
      complete: function ()
      {
         HideLoading();
      },
      error: function (response)
      {
         if (response.status !== 0 && response.status !== 401)
         {
            alert(response.status + " " + response.statusText);
         }
      }
   });
   $(".grid-details").hide();
   $(".grid-btn-prev").hide();
   $(".grid-btn-next").hide();
}

//Function for showing alert message
function AlertMessage(message, hideEmailBox)
{
   $(".alert-popup-body-message-confirm").hide();
   $("#alertmessageid").text(message);
   $(".alert-popup-body-message").show();

   $("#btnok").click(function ()
   {
      $(".alert-popup-body-message").hide();

      if (hideEmailBox && $(".email-popup-body-confirm").length > 0)
      {
         $(".email-popup-body-confirm").hide();
      }
   });
}
var scaleNum = 1;
$(document).ready(function ()
{
   localStorage["History"] = "";
   //SetMagnify();
});

//Function to set zoom level
function SetMagnify()
{
   $(".magnify").jfMagnify();
   $(".plus").click(function ()
   {
      scaleNum += .3;

      if (scaleNum >= 3)
      {
         scaleNum = 3;
      };

      $(".magnify").data("jfMagnify").scaleMe(scaleNum);
   });

   $(".minus").click(function ()
   {
      scaleNum -= .3;

      if (scaleNum <= 1)
      {
         scaleNum = 1;
      };

      $(".magnify").data("jfMagnify").scaleMe(scaleNum);
   });
   $(".magnify_glass").animate({
      'top': "55%",
      'left': "60%"
   }, {
         duration: 3000,
         progress: function ()
         {
            $(".magnify").data("jfMagnify").update();
         },
         easing: "easeOutElastic"
      });

}

function setScale(scale)
{
   scaleNum = scale; $(".magnify").data("jfMagnify").scaleMe(scaleNum);
}

function GenerateReportAsPerFileType(fileType, isDownload)
{
   var reportScheduleId = $("#hdnReportScheduleId").val();
   if (fileType === "PDF")
   {
      $("#dvReportPreviewXLS").hide();
      $("#dvJqxgrid_ReportPreviewCSV").hide();
      $("#dvJqxgrid_ReportPreviewXLS").hide();

      if (report === ReportNames.ProductDetails)
      {
         reportCriteriaDetail = productDetailCriteria();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
      else if (report === ReportNames.WasteReport)
      {
         reportCriteriaDetail = wasteReportCriteria();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
      else if (report === ReportNames.PharmacySummary)
      {
         reportCriteriaDetail = PharmacySummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
      else if (report === ReportNames.NonCreditableSummary)
      {
         reportCriteriaDetail = NonCreditableSummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
      else if (report === ReportNames.ManufacturerSummary)
      {
         reportCriteriaDetail = ManufacturerSummaryDetails();
         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
      else if (report === ReportNames.ReturnToStock)
      {
         reportCriteriaDetail = ReturnToStockDetails();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
         $("#ifReport").attr("src", resolveUrl() + "/Home/" + report + "?FileType=" + fileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail + "&reportScheduleId=" + reportScheduleId);
      }
   }
   else if (fileType === "XLS")
   {

      $("#dvReportPreview").hide();
      $("#dvReportPreviewXLS").show();
      $("#dvJqxgrid_ReportPreviewCSV").hide();
      $("#dvJqxgrid_ReportPreviewXLS").show();

      if (report === ReportNames.ProductDetails)
      {
         reportCriteriaDetail = productDetailCriteria();
         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.WasteReport)
      {
         reportCriteriaDetail = wasteReportCriteria();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.PharmacySummary)
      {
         reportCriteriaDetail = PharmacySummaryDetails();
         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.NonCreditableSummary)
      {
         reportCriteriaDetail = NonCreditableSummaryDetails();
         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.ManufacturerSummary)
      {
         reportCriteriaDetail = ManufacturerSummaryDetails();
         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.ReturnToStock)
      {
         reportCriteriaDetail = ReturnToStockDetails();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         CreateReportPreviewGridDesign(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
   }
   else if (fileType === "CSV")
   {
      $("#dvReportPreview").hide();
      $("#dvReportPreviewXLS").show();
      $("#dvJqxgrid_ReportPreviewXLS").hide();
      $("#dvJqxgrid_ReportPreviewCSV").show();

      if (report === ReportNames.ProductDetails)
      {
         reportCriteriaDetail = productDetailCriteria();
         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.WasteReport)
      {
         reportCriteriaDetail = wasteReportCriteria();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.PharmacySummary)
      {
         reportCriteriaDetail = PharmacySummaryDetails();
         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.NonCreditableSummary)
      {
         reportCriteriaDetail = NonCreditableSummaryDetails();
         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.ManufacturerSummary)
      {
         reportCriteriaDetail = ManufacturerSummaryDetails();
         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
      else if (report === ReportNames.ReturnToStock)
      {
         reportCriteriaDetail = ReturnToStockDetails();

         if (reportCriteriaDetail.NDCNumber !== "")
         {
            reportCriteriaDetail.NDCNumber = parseInt(reportCriteriaDetail.NDCNumber);
         }

         GenerateCSVDataGrid(report, fileType, isDownload, JSON.stringify(reportCriteriaDetail));
      }
   }

   DisableEnableToolbarAfterPreview();
}

//Function for remove and disable control value
function RemoveAndDisableControlValue()
{
   $("#SearchPanelList :input").prop("disabled", false);
   $("#divReportScheduler :input").val("");
   $("#SearchPanelList :input").val("");

   $("#divReportScheduler option[value='']").attr("selected", true);
   $("#StartDate").datepicker("setDate", null);
   $("#EndDate").datepicker("setDate", null);

   $("#FileType").attr("disabled", false);
   $("#ReportScheduleTimeZone").attr("disabled", false);
   $("#ReportDataTimeZone").attr("disabled", false);
   $("#SearchPanelList :input").css("cursor", "text");

   $("#FileType").css("background-color", "#ECDD9D");
   $("#ReportScheduleTimeZone").css("background-color", "#ECDD9D");
   $("#ReportDataTimeZone").css("background-color", "#ECDD9D");
}
var isExclamationEnable = false;
$("#btn_toolbar_Exclamation").on("click", function ()
{
   isExclamationEnable = true;

   if (isExclamationEnable === true)
   {
      RemoveAndDisableControlValue();
   }

   $("#divReportScheduler option[value='']").attr("selected", false);
});
function disableCriteriaAndSchedule()
{
   $("#SearchPanelList :input").attr("disabled", true);
   $("#divReportScheduler :input").prop("disabled", true);;
}

$("#tab-container")
   .bind("easytabs:before",
   function ()
   {
      if ($("#hdnEditNewClicked").val() !== "")
      {
         return false;
      } else return true;
   });

$(".dotsMenu-content a").on("click", function (e)
{
   if ($("#hdnEditNewClicked").val() !== "")
   {
      PendingChanges();
      e.preventDefault();
   }
});

//*****************************************************************************************
//*
//* Summary:
//*   Click event on mini action bar in History tab of Reports
//*   Filter report history table based on user input
//*
//* Parameters:
//*   None.
//*
//* Returns:
//*   None.
//*
//*****************************************************************************************
$(".historyFilter").on("click", function (e)
{
   var clickedImage = $(e.currentTarget);

   //enable/disable button that was clicked
   if (clickedImage.attr("src").includes("0"))
   {
      clickedImage.attr("src", clickedImage.attr("src").replace("0", "1"));
   }
   else
   {
      clickedImage.attr("src", clickedImage.attr("src").replace("1", "0"));
   }

   var regSearchTerm = "";
   var invSearchTerm = "";
   var inverse = false;

   //goes through each of the buttons in the action bar with this class
   //determines which ones are enabled/disabled to generate our search criteria
   $(".historyFilterIcon").each(function ()
   {
      var element = $(this);

      if (element.attr("filter") === "Scheduled")
      {
         inverse = element.attr("src").includes("0") ? false : true;
      }
      else
      {
         if (element.attr("src").includes("0"))
         {
            invSearchTerm += "|" + element.attr("filter");
         }
         else
         {
            regSearchTerm += "|" + element.attr("filter");
         }
      }
   });

   regSearchTerm = regSearchTerm.substring(1);
   invSearchTerm = invSearchTerm.substring(1);

   //inverse means the Scheduled reports are toggled on (Scheduled button is enabled)
   if (inverse)
   {
      //invSearchTerm holds the columns to ignore
      //for example, if Scheduled is enabled and we turn off the Previewed reports, then this will search for all rows that are not Previewed
      if (invSearchTerm !== "")
      {
         $("#historyTable").DataTable().columns(2).search("^((?!\\" + invSearchTerm + ").)*$", true, false).draw();
      }
      else
      {
         //search for ALL STATUSES i.e. all buttons are enabled
         $("#historyTable").DataTable().columns(2).search("").draw();
      }
   }
   else
   {
      //search by the regSearchTerm, which determines what report status we should search for
      //here we will search for: Previewed, Downloaded, Previewed AND Downloaded or ALL STATUSES if all buttons are disabled
      $("#historyTable").DataTable().columns(2).search(regSearchTerm, true, false).draw();
   }
});