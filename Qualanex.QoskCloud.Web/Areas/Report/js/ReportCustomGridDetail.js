﻿
$(document).ready(function ()
{
   LoadColumnListingInPopup();
   CreateGridColumnPanel(true);

});

function ToolbarClicked()
{
   var heigrid = $(window).height();
   var popctrgrid = (heigrid - 435) / 2;

   $(".custom-grid").show();
   $(".custom-search-box").show();
   $(".custom-search-box").css({ "top": +popctrgrid + "px" });
   $(".gapping-inline").click(function ()
   {
      $(".model-popup").show();
      $(".model-box").animate({ "top": "20%" });
   });
   // drag and drop

   $(".close-button, .cancel, #btnSubmit").click(function ()
   {
      $(".custom-search-box").hide();
      $(".model-popup,.custom-grid").hide();
   });

   $('#move_right1').click(function (evt)
   {

      var selectedOpts = $('.list3 option:selected');
      if (selectedOpts.length > 0)
      {
         $('.list4').append($(selectedOpts).clone());
         $(selectedOpts).remove();
         evt.stopPropagation();
         evt.preventDefault();
      }
      else
      {

         $("#alertmessageid").text(Common.SelectLeft);
         $(".alert-popup-body-message").show();
         return;
      }

   });



   $('#move_left1').click(function (e)
   {

      var selectedOptss = $('.list4 option:selected');
      if (selectedOptss.length > 0)
      {
         if (selectedOptss.attr('data-moveable') == "true")
         {

            e.preventDefault();
            return;
         }
         else
         {
            $('.list3').append($(selectedOptss).clone());
            $(selectedOptss).remove();
            e.preventDefault();
            return;
         }
      }
      else
      {

         $("#alertmessageid").text(Common.SelectRight);
         $(".alert-popup-body-message").show();
         return;
      }
   });

   $("#move_up").children().click(function ()
   {
      $this = $(this);
      var $op1 = $('.list4 option:selected');

      if ($op1.length)
      {
         $op1.first().prev().before($op1);
      }
   });
   $("#move_down").click(function ()
   {
      $this = $(this);
      var $op1 = $('.list4 option:selected');

      if ($op1.length)
      {
         $op1.last().next().after($op1);
      }
   });
   // custom gid
   $(".custom-grid").click(function ()
   {
      $(".custom-search-box").hide();
      $(".custom-grid").hide();
   }).children().click(function (e)
   {
      return false;
   });
}
//Function to generate parameters dynamically
function CreateReportSearchPanel(reportScheduleId)
{
   var report = _ReportName;

   if ($("#hdnEditNewClicked").val() != "" && reportScheduleId != "")
   {
      PendingChanges();
   }
   else
   {
      var reportScheduleId = reportScheduleId;
      if (reportScheduleId == "")
      {
         reportScheduleId = -1;
      }
      ShowLoading();
      var Url1 = resolveUrl() + '/Home/CreateSearchPanel';
      $.ajax({
         url: Url1,
         cache: false,
         data: { ReportName: report, ReportScheduleId: reportScheduleId },
         async: true,
         success: function (data)
         {
            $("#SearchPanelList").html(data);
         },
         complete: function ()
         {
            if ($("#hdnEditNewClicked").val() == "")
            {
               $("#SearchPanelList :input").attr("disabled", true);
               $("#SearchPanelList :input").css("cursor", "not-allowed");
               $("#divReportScheduler :input").prop('disabled', true);
            }
            else
            {
               $("#divReportScheduler :input").css("background-color", "#ECDD9D");
               $("#SearchPanelList :input").css("background-color", "#ECDD9D");
               $("#SearchPanelList :input.autocomplete").attr("placeholder", "auto complete");
               isRunEnable = false;
               $("#btn_toolbar_Run").attr("src", "/Areas/Report/images/RunStandardDisable.png");
               //DisableToolbarControl("#btn_toolbar_Exclamation");
            }
            HideLoading();
            LoadColumnListingInPopup();
         }

      });
   }
}
//Function for pending changes(Run when user edit or add new record and click on grid then this function show the prompt)
function PendingChanges()
{
   $(".alert-popup-body-message-confirm").hide();
   $("#alertmessageid").text("You have pending changes. Please save or cancel.");
   $(".alert-popup-body-message").show();
   $("#btnok").click(function ()
   {
      $(".alert-popup-body-message").hide();
   });
}
//Function to bind parameters details in popup
function LoadColumnListingInPopup()
{
   var report = _ReportName;
   $.ajax({
      url: resolveUrl() + '/Home/CreateColumnListingPopUp',
      cache: false,
      data: { ReportName: report },
      success: function (data)
      {
         $("#ColumnListingPopUp").html(data);
         BindCustomSearch();
      },
      error: function (response)
      {
         if (response.status != 0)
         {

         }
      }, beforeSend: function ()
      {
         ShowLoading();
      },
      complete: function ()
      {
         HideLoading();
      }
   });
};
function SetSearchFieldValue(objSearchFieldValue)
{

   for (var i = 0; i < objSearchFieldValue.length; i++)
   {
      var SearchFieldObject = objSearchFieldValue[i];

      var controlname = SearchFieldObject.Type + '-' + SearchFieldObject.Name;
      document.getElementsByName(controlname)[0].value = SearchFieldObject.Value1;
   }

}
//Function to create grid column panel
function CreateGridColumnPanel(first)
{
   var report = _ReportName;
   var Url1 = resolveUrl() + '/Home/BindColumnListing';
   $.ajax({
      url: Url1,
      cache: false,
      data: { ReportName: report },
      success: function (data)
      {
         $("#CustomGridListing").html(data);
         if (first)
         {
            BindCustomGrid();
         } else
         {
            ToolbarClicked();
         }
      },
      error: function (response)
      {
         if (response.status != 0)
         {

         }
      }
   });
}

$(document).ready(function ()
{
   $("#btdeletenCancel,.alert-close").click(function ()
   {
      $(".alert-popup-body-message-confirm").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });

});
//Function for showing and hiding loader
function ShowLoading()
{
   $('#divloaderBackground').show();
   $('#divLoader').show();

}
function HideLoading()
{
   $('#divLoader').hide();
   $('#divloaderBackground').hide();
}

//selectAllList
function selectAllList()
{

   var aSelect = $('#sel2');

   var options = $('#sel2 option');

   var values = $.map(options, function (option)
   {
      // alert(option.value);
   });


   var aSelectLen = aSelect.length;
   for (i = 0; i < aSelectLen; i++)
   {
      aSelect.options[i].attr('selected', 'selected');
   }
}

//GetLeftPanelList
function GetLeftPanelList()
{
   var layoutID = _SearchPanelLayoutID;
   var optionValues = [];
   var options = $('#sel2 option');
   var values = $.map(options, function (option)
   {
      optionValues.push(option.value);
   });
   var srchrslt = optionValues;
   $.ajax({
      type: "POST",
      url: resolveUrl() + '/Home/SaveCustomSearchDetailsByUserName',
      cache: false,
      data: { SearchList: srchrslt.toString(), LayoutId: layoutID },
      dataType: "json"
      , beforeSend: function ()
      {
         ShowLoading();
      }
   }).done(function (data)
   {
      CreateReportSearchPanel($("#hdnReportScheduleId").val());
      HideLoading();

   }).fail(function (response)
   {

   });

}

//GetGridPanelList
function GetGridPanelList()
{
   var layoutID = _ListPanellayoutID;
   var optionValues = [];
   var options = $('#GridList2 option');
   var tableId = $(".custom-grid-title").attr("tableId");

   var values = $.map(options, function (option)
   {
      optionValues.push(option.value);
   });
   var srchrslt = optionValues;

   $.ajax({
      type: "POST",
      url: resolveUrl() + '/Home/SaveCustomSearchDetailsByUserName',
      cache: false,
      data: { SearchList: srchrslt.toString(), LayoutId: layoutID, TableId: tableId },
      dataType: "json"
      , beforeSend: function ()
      {
         ShowLoading();
      }
   }
   ).done(function (data)
   {
      $(".custom-grid").hide();
      HideLoading();
   }).fail(function (response)
   {
      if (response.status != 0)
      {

      }
   });

}

