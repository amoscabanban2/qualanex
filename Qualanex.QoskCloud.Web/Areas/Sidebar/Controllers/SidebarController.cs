﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarController.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Controller exposing sidebar specific APIs
/// </summary>
/// <remarks>
///   ProtoContainer defined in the SidebarContainer.cs file is used as a type agnostic container
///   for api call inputs. The object is created by serializing the container inputs in the html.
///   It can be converted to a bin/gaylord/tote by calling SidebarCommon.ContainerFromProto()
/// </remarks>
///-----------------------------------------------------------------------------------------------
// ReSharper disable RedundantCaseLabel

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Web.Mvc;

   using Microsoft.Ajax.Utilities;

   using Qualanex.Qosk.Library.Model.UPAModel;

   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "QLUser")]
   public class SidebarController : Controller
   {
      private UPAApplication _upaApplication;

      ///****************************************************************************
      /// <summary>
      ///   Api to open a Container with the containerId and other properties defined
      /// in the passed in container object
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="currentStatus">a comma separated list or javascript array 
      /// containing acceptable values for the pre-open status of the container</param>
      /// <returns>JSON containing success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult Open(ProtoContainer protoContainer, string currentStatus = "")
      {
         //instantiate an appropriate container from the given properties
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         var success = false;
         var error = string.Empty;
         var errorCode = 0;
         var containers = string.Empty;

         try
         {
            var upaApplication = this.GetApplicationFromSession();
            var currentStatusList = currentStatus.Split(',').ToList();

            currentStatusList.Remove(string.Empty);

            if (!currentStatusList.Any())
            {
               currentStatusList.AddRange(GetDefaultStatuses(container.Attribute));
            }

            if (container.Status.IsNullOrWhiteSpace())
            {
               switch (container.Attribute)
               {
                  case SidebarContainer.ATTR_INBOUND:
                     container.Status = SidebarContainer.STATUS_INPROCESS;
                     break;

                  case SidebarContainer.ATTR_OUTBOUND:
                  default:
                     container.Status = SidebarContainer.STATUS_PREPARING;
                     break;
               }
            }
            //Calls the type specific Open function
            success = container.Open(SidebarCommon.GetMachineName(this.Request), upaApplication.Code, currentStatusList);
            //Update html for the sidebar
            //The html is returned as part of the json to allow for additional information on the success or error state of the open call
            containers = this.ConvertViewToString("_Sidebar", new UpaViewModel() { UpaApplication = this.GetApplicationFromSession() });
         }
         catch (Exception e)
         {
            //the error message will sometimes get a code of 1. That means the specific container type was found,
            //so we need to return the error specific to the container (see DE962)
            var errorList = e.Message.Split(',').ToList();

            for (var x = 0; x > errorList.Count; x++)
            {
               var text = errorList[x];
               errorList[x] = text.Trim();
            }

            error = errorList[0];
            errorCode = errorList.Count > 1 ? Int32.Parse(errorList[1]) : 0;
         }

         return this.Json(new { success, containers, error, errorCode });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="attribute"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static List<string> GetDefaultStatuses(string attribute)
      {
         var currentStatusList = new List<string>();

         switch (attribute)
         {
            case SidebarContainer.ATTR_INBOUND:
               currentStatusList.Add(SidebarContainer.STATUS_STAGED);
               break;
            case SidebarContainer.ATTR_OUTBOUND:
            default:
               currentStatusList.AddRange(new List<string>() { SidebarContainer.STATUS_PREPARING, SidebarContainer.STATUS_STAGED, SidebarContainer.STATUS_AVAILABLE });
               break;
         }

         return currentStatusList;
      }

      ///****************************************************************************
      /// <summary>
      ///   Closes a container, removing it from the station and updating the status.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="closeStatus">allows the caller to specify the status of
      /// the container after it is closed</param>
      /// <returns>JSON containing success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      public JsonResult Close(ProtoContainer protoContainer, string closeStatus = null)
      {
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         var success = false;
         var error = string.Empty;
         var containers = string.Empty;
         var upaApplication = this.GetApplicationFromSession();
         var printString = string.Empty;
         var isRecall = false;

         if (!closeStatus.IsNullOrWhiteSpace())
         {
            container.Status = closeStatus;
         }
         else
         {
            switch (container.Attribute)
            {
               case SidebarContainer.ATTR_INBOUND:
                  container.Status = SidebarContainer.STATUS_AVAILABLE;
                  break;
               case SidebarContainer.ATTR_OUTBOUND:
               default:
                  container.Status = SidebarContainer.STATUS_STAGED;
                  break;
            }
         }

         try
         {
            //call the container type specific close method
            success = container.Close(SidebarCommon.GetMachineName(this.Request), upaApplication.Code);
            containers = this.GetSidebarHtml();

            //only gaylords should be printed upon container close
            if (success && container.TypeGroup == Constants.Gaylord)
            {
               printString = !string.IsNullOrEmpty(protoContainer.RecallID)
                  ? container.GetCloseContainerPrintStringRecall(protoContainer)
                  : container.GetCloseContainerPrintString(protoContainer);

               isRecall = !string.IsNullOrEmpty(protoContainer.RecallID);
            }
         }
         catch (Exception e)
         {
            error = e.Message;
         }

         return this.Json(new { success, containers, printString, error, isRecall });
      }

      ///****************************************************************************
      /// <summary>
      ///   Removes a container from the station, leaving the status such that it can be
      /// re-attached at a later time or other station.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="pauseStatus">allows the caller to specify the status of
      /// the container after it is paused</param>
      /// <returns>JSON containing success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      public JsonResult Pause(ProtoContainer protoContainer, string pauseStatus = null)
      {
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         var success = false;
         var error = string.Empty;
         var containers = string.Empty;
         var upaApplication = this.GetApplicationFromSession();

         if (!pauseStatus.IsNullOrWhiteSpace())
         {
            container.Status = pauseStatus;
         }
         else
         {
            switch (container.Attribute)
            {
               case SidebarContainer.ATTR_INBOUND:
                  container.Status = SidebarContainer.STATUS_STAGED;
                  break;
               case SidebarContainer.ATTR_OUTBOUND:
               default:
                  // Do nothing
                  break;
            }
         }

         try
         {
            //call the container type specific pause method
            success = container.Pause(SidebarCommon.GetMachineName(this.Request), upaApplication.Code);
            containers = this.GetSidebarHtml();
         }
         catch (Exception e)
         {
            error = e.Message;
         }

         return this.Json(new { success, containers, error });
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="itemId">Item that is being put into the container</param>
      /// <returns>JSON containing success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult Put(ProtoContainer protoContainer, Guid itemId, bool isForeignContainer = false, string manufacturerProfileCode = "")
      {
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         ContainerInstance previousContainer = null;
         var error = string.Empty;
         var success = true;
         var containers = string.Empty;

         try
         {
            IsSpecialHandling(protoContainer, manufacturerProfileCode, isForeignContainer);
            IsWhiteBinC2(protoContainer, itemId);
            IsRecall(protoContainer, itemId);
            IsWaste(protoContainer, itemId);
            previousContainer = container.Put(itemId);
            containers = this.GetSidebarHtml();
         }
         catch (Exception e)
         {
            success = false;
            error = e.Message;
         }

         return this.Json(new { previousContainer, success, containers, error });
      }

      ///****************************************************************************
      /// <summary>
      ///   Puts an item into a virtual container for when the item is not physically 
      ///   present.
      /// </summary>
      /// <param name="containerType">the type of the virtual container</param>
      /// <param name="itemId">item id being put into the container</param>
      /// <param name="category">category of gaylord</param>
      /// <returns>JSON containing the previous container id and instance id 
      /// success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult PutVirtual(string containerType, Guid itemId, string category)
      {
         var container = SidebarCommon.GetVirtualContainer("VRT_GAYLRD", category);
         ContainerInstance previousContainer = null;
         var error = string.Empty;
         var success = true;
         var containers = string.Empty;

         try
         {
            previousContainer = container.Put(itemId);
            containers = this.GetSidebarHtml();
         }
         catch (Exception e)
         {
            success = false;
            error = e.Message;
         }

         return this.Json(new { previousContainer, success, containers, error });
      }

      ///****************************************************************************
      /// <summary>
      ///   Requests a new instance of a physical container to be created.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <returns>JSON containing success boolean, error message, print string for 
      /// generating a new label, the ID of the new container</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult RequestContainer(ProtoContainer protoContainer)
      {
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         var success = false;
         var printString = string.Empty;
         var containerId = string.Empty;
         var error = string.Empty;
         var isRecall = false;

         try
         {
            printString = container.Request();
            containerId = printString.SubstringUpToFirst('§').Split('=')[1];
            success = true;
            isRecall = !string.IsNullOrEmpty(protoContainer.RecallID);
         }
         catch (Exception e)
         {
            error = e.Message;
         }

         return this.Json(new { success, printString, containerId, error, isRecall });
      }

      ///****************************************************************************
      /// <summary>
      ///   Remove an item from a container.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="itemId"></param>
      /// <returns>JSON containing success boolean, error message, and updated sidebar html</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult Remove(ProtoContainer protoContainer, Guid itemId)
      {
         var container = SidebarCommon.ContainerFromProto(protoContainer);
         var previousContainer = false;
         var error = string.Empty;
         var containers = string.Empty;

         try
         {
            previousContainer = container.Remove(itemId, container);
            //containers = this.GetSidebarHtml();
         }
         catch (Exception e)
         {
            error = e.Message;
         }

         return this.Json(new { Success = previousContainer, containers, error });
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the container type of the container ID scanned
      /// </summary>
      /// <param name="containerId">Scanned container that we're attempting to place the product in</param>
      /// <returns>JSON containing container type string</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult GetContainerType(string containerId = "")
      {
         var containerType = string.Empty;

         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               containerType = (from contr in cloudEntities.WrhsContr where contr.WrhsContrID == containerId select contr.Type).FirstOrDefault();
            }
         }
         catch (Exception e)
         {
            //do nothing
         }

         return this.Json(containerType);
      }

      ///****************************************************************************
      /// <summary>
      ///   Finds existing containers matching the properties of the proto container
      /// </summary>
      /// <param name="proto">The values from the container</param>
      /// <param name="statuses">a comma separated list or javascript array 
      /// containing acceptable values for the current status of the container</param>
      /// <returns>containerId and instanceId of a matching container</returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult FindAvailableContainer(ProtoContainer proto, string statuses)
      {
         var statusList = statuses.Split(',').ToList();

         statusList.Remove(string.Empty);
         statusList.Remove("null");
         statusList.Remove("undefined");

         if (statusList.Count == 0)
         {
            statusList.AddRange(GetDefaultStatuses(proto.Attribute));
         }

         var ignoreCategory = proto.WasteProfileID.IsNullOrWhiteSpace();

         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var container = (
                  from status in cloudEntities.WrhsContrStatusHist
                  join contr in cloudEntities.WrhsContr on status.WrhsContrID equals contr.WrhsContrID
                  where statusList.Contains(status.StatusCode)
                        && contr.Type == proto.Type
                        && (ignoreCategory || proto.WasteProfileID == contr.WasteProfileID)
                        && status.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                  where olderStatus.WrhsContrID == status.WrhsContrID
                                                  select olderStatus.CreatedDate).OrderByDescending(x => x)
                        .FirstOrDefault()
                  select new ContainerInstance
                  {
                     ContainerId = status.WrhsContrID,
                     InstanceId = status.WrhsInstID
                  }).FirstOrDefault();

               return this.Json(container);
            }
         }
         catch (Exception e)
         {
            Logger.Error(e.Message);
            throw new Exception("There was an error finding an available container");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns true if all items have been removed from a container.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult IsComplete(ProtoContainer protoContainer)
      {
         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var container = (from wrhsContRel in cloudEntities.WrhsContrItemRel
                                where wrhsContRel.IsActive
                                      && wrhsContRel.WrhsContrID == protoContainer.ContainerInstance.ContainerId
                                      && wrhsContRel.WrhsInstID == protoContainer.ContainerInstance.InstanceId
                                select new
                                {
                                   WrhsContr = (from wrhsRl in cloudEntities.WrhsContrItemRel
                                                where wrhsRl.ItemGUID == wrhsContRel.ItemGUID
                                                && wrhsRl.IsActive
                                                orderby wrhsRl.CreatedDate descending, wrhsRl.ModifiedDate
                                                select wrhsRl.WrhsContrID).FirstOrDefault(),
                                   ItemGUID = wrhsContRel.ItemGUID
                                }).Distinct().ToList();

               return this.Json(container);
            }
         }
         catch (Exception e)
         {
            Logger.Error(e.Message);
            throw new Exception("There was an error finding an container");
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns true if the item is currently in the container.
      /// </summary>
      /// <param name="protoContainer">The values from the container</param>
      /// <param name="itemGuid">the item to verify</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public JsonResult IsItemIn(ProtoContainer protoContainer, Guid itemGuid)
      {
         var success = false;
         var error = String.Empty;
         var isItemIn = false;

         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var container = (from wrhsContRel in cloudEntities.WrhsContrItemRel
                                where wrhsContRel.ItemGUID == itemGuid
                                  && wrhsContRel.IsActive == true
                                orderby wrhsContRel.CreatedDate descending, wrhsContRel.ModifiedDate descending
                                select new
                                {
                                   WrhsContr = wrhsContRel.WrhsContrID,
                                   WrhsInstId = wrhsContRel.WrhsInstID
                                }).FirstOrDefault();

               isItemIn = container != null && container.WrhsContr == protoContainer.ContainerInstance.ContainerId;
               success = true;
            }
         }
         catch (Exception e)
         {
            Logger.Error(e.Message);
            error = "There was an error associating the item with a container. Please clear your screen and re-process Item GUID " + itemGuid + ".";
            success = false;
            isItemIn = false;
         }

         return this.Json(new { isItemIn, success, error });
      }

      ///****************************************************************************
      /// <summary>
      ///   Generates the html for containers attached to the current station and 
      ///   returns as a string.
      /// </summary>
      /// <returns>The HTML for the sidebar containers as a string</returns>
      ///****************************************************************************
      private string GetSidebarHtml()
      {
         return this.ConvertViewToString("_Sidebar", new UpaViewModel() { UpaApplication = this.GetApplicationFromSession() });
      }

      ///****************************************************************************
      /// <summary>
      ///   Finds the currently attached sidebar containers for a given station and application.
      ///   This is called from _Sidebar.cshtml
      /// </summary>
      /// <param name="upaViewModel">Viewmodel containing the upa application and
      /// will be updated with the container object models</param>
      /// <param name="machineName">the machine name for the current station</param>
      ///****************************************************************************
      public static void InflateSidebar(UpaViewModel upaViewModel, string machineName)
      {
         //find the sidebar tab 
         var sidebar = upaViewModel.UpaApplication.childObj.Values.FirstOrDefault(t => t.ObjectID == Constants.UpaSidebar);
         //if this application does not have a sidebar tab defined, skip the rest
         if (sidebar == null)
         {
            return;
         }

         List<ProtoContainer> currentContainers;

         try
         {
            //Look up all containers currently attached to this station and application
            //as protocontainers
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               currentContainers = new List<ProtoContainer>(
                  from status in cloudEntities.WrhsContrStatusHist
                  join container in cloudEntities.WrhsContr on status.WrhsContrID equals container.WrhsContrID
                  join qosk in cloudEntities.Qosk on status.QoskID equals qosk.QoskIDx
                  join waste in cloudEntities.WasteStreamProfile on container.WasteProfileID equals waste.WasteProfileID into d
                  from wastelist in d.DefaultIfEmpty()
                  join profile in cloudEntities.Profile on container.ProfileCode equals profile.ProfileCode into c
                  from profilelist in c.DefaultIfEmpty()
                  where qosk.MachineID == machineName
                        && status.AppCode == upaViewModel.UpaApplication.Code
                        && ((status.StatusCode == SidebarContainer.STATUS_INPROCESS
                        && status.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                  where olderStatus.WrhsContrID == status.WrhsContrID
                                                  select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault())
                                                  || (status.StatusCode == SidebarContainer.STATUS_PREPARING
                                                  && status.CreatedDate >= (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                                            where olderStatus.WrhsContrID == status.WrhsContrID
                                                                                  && olderStatus.StatusCode != SidebarContainer.STATUS_PREPARING
                                                                            select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault()))
                  select
                  new ProtoContainer()
                  {
                     ContainerInstance = new ContainerInstance
                     {
                        ContainerId = container.WrhsContrID,
                        InstanceId = status.WrhsInstID
                     },
                     Type = container.Type,
                     WasteProfileID = container.WasteProfileID,
                     CtrlNo = container.CtrlNo,
                     ProfileCode = container.ProfileCode,
                     RecallID = container.AssocID,
                     Attribute = status.WrhsContrAttribCode,
                     Status = status.StatusCode,
                     ApprovalCode = wastelist.ApprovalCode,
                     Name = profilelist.Name,
                     Count = Constants.ToteTypes.Contains(container.Type) ?
                     (from item in cloudEntities.WrhsContrItemRel
                      where item.WrhsContrID == status.WrhsContrID
                            && item.WrhsInstID == status.WrhsInstID
                            && item.IsDeleted != true
                      select item.ItemGUID).ToList().Count :
                     (from item in cloudEntities.WrhsContrItemRel
                      where item.WrhsContrID == status.WrhsContrID
                            && item.WrhsInstID == status.WrhsInstID
                            && item.IsDeleted != true
                            && item.IsActive == true
                      select item.ItemGUID).ToList().Count,
                     SeverityOrder = (from wcpr in cloudEntities.WasteCodeProfileRel
                                      join wcd in cloudEntities.WasteCodeDict on wcpr.WasteCode equals wcd.Code
                                      where wcpr.WasteStreamProfileID == container.WasteProfileID
                                      select wcd.SeverityOrder).FirstOrDefault()
                  });
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
         //Add the containers to the appropriate list within the UPAViewModel
         foreach (var sidebarControl in sidebar.childObj.Values)
         {

            switch (sidebarControl.ObjectID)
            {
               case Constants.BlueInduction:
                  upaViewModel.BlueInduction.AddRange(FindContainers(currentContainers, Constants.BlueInduction, false, sidebarControl));
                  break;

               case Constants.GrayInduction:
                  upaViewModel.GrayInduction.AddRange(FindContainers(currentContainers, Constants.GrayInduction, false, sidebarControl));
                  break;

               case Constants.BlackSortBin:
                  upaViewModel.BlackSortBin.AddRange(FindContainers(currentContainers, Constants.BlackSortBin, false, sidebarControl));
                  break;

               case Constants.BlueSortBin:
                  upaViewModel.BlueSortBin.AddRange(FindContainers(currentContainers, Constants.BlueSortBin, false, sidebarControl));
                  break;

               case Constants.ClearSortBin:
                  upaViewModel.ClearSortBin.AddRange(FindContainers(currentContainers, Constants.ClearSortBin, false, sidebarControl));
                  break;

               case Constants.GreenSortBin:
                  upaViewModel.GreenSortBin.AddRange(FindContainers(currentContainers, Constants.GreenSortBin, false, sidebarControl));
                  break;

               case Constants.WhiteSortBin:
                  upaViewModel.WhiteSortBin.AddRange(FindContainers(currentContainers, Constants.WhiteSortBin, false, sidebarControl));
                  break;

               case Constants.YellowSortBin:
                  upaViewModel.YellowSortBin.AddRange(FindContainers(currentContainers, Constants.YellowSortBin, false, sidebarControl));
                  break;

               case Constants.RedSortBin:
                  upaViewModel.RedSortBin.AddRange(FindContainers(currentContainers, Constants.RedSortBin, false, sidebarControl));
                  break;

               case Constants.SmallGaylord:
                  upaViewModel.SmallGaylord.AddRange(FindContainers(currentContainers, Constants.SmallGaylord, false, sidebarControl));
                  break;

               case Constants.LargeGaylord:
                  upaViewModel.LargeGaylord.AddRange(FindContainers(currentContainers, Constants.LargeGaylord, false, sidebarControl));
                  break;

               case Constants.OrangeGaylord:
                  upaViewModel.OrangeGaylord.AddRange(FindContainers(currentContainers, Constants.OrangeGaylord, false, sidebarControl));
                  break;

               case Constants.WasteGaylord:
                  upaViewModel.WasteGaylord.AddRange(FindContainers(currentContainers, Constants.WasteGaylord, true, sidebarControl));
                  break;

               case Constants.RecallGaylord:
                  upaViewModel.RecallGaylord.AddRange(FindContainers(currentContainers, Constants.RecallGaylord, true, sidebarControl));
                  break;

               case Constants.BlackGaylord:
                  upaViewModel.BlackGaylord.AddRange(FindContainers(currentContainers, Constants.BlackGaylord, true, sidebarControl));
                  break;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Finds matching containers within a list for the included upa segment.
      /// </summary>
      /// <param name="currentContainers">A list of protocontainers attached to the station</param>
      /// <param name="type">The type of container to find in the list</param>
      /// <param name="alwaysIncludeEmpty">If true, an empty instance of the container type will be 
      /// included even if there an instance is found in the list</param>
      /// <param name="sidebarObj">The upa segment for the sidebar container</param>
      /// <returns>A filtered list, including empty containers if no match is found 
      /// or the alwaysIncludeEmpty flag is true</returns>
      ///****************************************************************************
      private static List<ISidebarContainer> FindContainers(List<ProtoContainer> currentContainers, string type, bool alwaysIncludeEmpty, UPASegment sidebarObj)
      {
         var containers = new List<ISidebarContainer>();
         var containerGroup = SidebarCommon.GetGroupFromType(type);

         //Iterate through all containers of the selected type
         foreach (var proto in currentContainers.Where(x => x.Type == type))
         {
            //convert the protocontainer to the appropriate type
            switch (containerGroup)
            {
               case Constants.Tote:
                  containers.Add(new SidebarTote(proto));
                  break;

               case Constants.Bin:
                  containers.Add(new SidebarBin(proto));
                  break;

               case Constants.Gaylord:
                  containers.Add(new SidebarGaylord(proto));
                  break;
            }
            //Always include an empty field for outbound containers
            if (proto.Attribute == SidebarContainer.ATTR_OUTBOUND && !alwaysIncludeEmpty)
            {
               alwaysIncludeEmpty = true;
            }

            if (proto.Attribute == SidebarContainer.ATTR_OUTBOUND && sidebarObj.childObj.Where(c => c.Key == "OUTBOUND").Count() > 0)
            {
               var prt = new ProtoContainer
               {
                  Attribute = SidebarContainer.ATTR_OUTBOUND,
                  TypeGroup = proto.TypeGroup,
                  Type = proto.Type
               };
               switch (containerGroup)
               {
                  case Constants.Tote:
                     containers.Add(new SidebarTote(prt));
                     break;

                  case Constants.Bin:
                     containers.Add(new SidebarBin(prt));
                     break;

                  case Constants.Gaylord:
                     containers.Add(new SidebarGaylord(prt));
                     break;
               }
            }
         }

         //Add empty container instances as appropriate
         if (containers.Count == 0 || alwaysIncludeEmpty)
         {
            //If the container is only inbound or only outbound
            if (!(sidebarObj.childObj.Any(c => new[] { "OUTBOUND", "MISSORT" }.Contains(c.Key))
               && sidebarObj.childObj.Any(c => c.Key == "INBOUND")))
            {
               switch (containerGroup)
               {
                  case Constants.Tote:
                     containers.Add(new SidebarTote(new ProtoContainer() { Type = type, Attribute = sidebarObj.childObj.Any(c => c.Key == "OUTBOUND") ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                     break;

                  case Constants.Bin:
                     containers.Add(new SidebarBin(new ProtoContainer() { Type = type, Attribute = sidebarObj.childObj.Any(c => new[] { "OUTBOUND", "MISSORT" }.Contains(c.Key)) ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                     break;

                  case Constants.Gaylord:
                     containers.Add(new SidebarGaylord(new ProtoContainer() { Type = type, Attribute = sidebarObj.childObj.Any(c => c.Key == "OUTBOUND") ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                     break;
               }
            }
            else
            {
               //If both inbound and outbound versions of the container are required, add both
               //This can be dangerous if additional functions are added to sidebar containers
               //in the UPA definition.
               foreach (var attr in sidebarObj.childObj)
               {
                  switch (containerGroup)
                  {
                     case Constants.Tote:
                        containers.Add(new SidebarTote(new ProtoContainer() { Type = type, Attribute = attr.Key == "OUTBOUND" ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                        break;

                     case Constants.Bin:
                        if (!new[] { "OUTBOUND", "MISSORT", "INBOUND" }.Contains(attr.Key))
                        {
                           break;
                        }

                        containers.Add(new SidebarBin(new ProtoContainer() { Type = type, Attribute = new[] { "OUTBOUND", "MISSORT" }.Contains(attr.Key) ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                        break;

                     case Constants.Gaylord:
                        containers.Add(new SidebarGaylord(new ProtoContainer() { Type = type, Attribute = attr.Key == "OUTBOUND" ? SidebarContainer.ATTR_OUTBOUND : SidebarContainer.ATTR_INBOUND }));
                        break;
                  }
               }
            }
         }

         return containers;
      }

      ///****************************************************************************
      /// <summary>
      ///   Returns the current upa application.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      private UPAApplication GetApplicationFromSession()
      {
         return this._upaApplication ?? (this._upaApplication = (UPAApplication)QoskSessionHelper.GetSessionData(
                   Constants.UpaApplicationKey + this.Request.Params["pageViewId"]));
      }

      ///****************************************************************************
      /// <summary>
      ///   Helper method to return a string value for a given view and model.
      /// </summary>
      /// <param name="viewName">the name of the view file</param>
      /// <param name="model">the model to be passed to that view</param>
      /// <returns></returns>
      ///****************************************************************************
      private string ConvertViewToString(string viewName, object model)
      {
         this.ViewData.Model = model;

         using (StringWriter writer = new StringWriter())
         {
            ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
            ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, this.ViewData, new TempDataDictionary(), writer);
            vResult.View.Render(vContext, writer);

            return writer.ToString();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine special handling restrictions for adding items to outbound
      ///   container
      /// </summary>
      /// <param name="protoContainer">the destination container</param>
      /// <param name="profileCode">profile code of the manufacturer of the item</param>
      /// <param name="isForeignContainer">whether this item is a foreign container item</param>
      /// <returns></returns>
      ///****************************************************************************
      private static void IsSpecialHandling(ProtoContainer protoContainer, string profileCode, bool isForeignContainer)
      {
         if (protoContainer.Attribute == SidebarContainer.ATTR_OUTBOUND && (protoContainer.Type == Constants.SmallGaylord ||
                                                 protoContainer.Type == Constants.LargeGaylord ||
                                                 protoContainer.Type == Constants.OrangeGaylord))
         {
            var manuProfile = ProfilesDataAccess.GetProfile(int.Parse(profileCode), 0);
            if (!manuProfile.SpecialHandling && protoContainer.ProfileCode != 0)
            {
               throw new Exception($"Must be sorted to a generic gaylord");
            }
            if (manuProfile.SpecialHandling && !isForeignContainer && manuProfile.ProfileCode != protoContainer.ProfileCode)
            {
               throw new Exception($"Cannot sort to a container assigned to another manufacturer");
            }
            if (manuProfile.SpecialHandling && isForeignContainer && protoContainer.ProfileCode != 0)
            {
               throw new Exception($"Foreign container items must be sorted to a generic gaylord");
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine which type of white bin (c2 vs audit) we need in order to sort the currently-processed item
      /// </summary>
      /// <param name="protoContainer">the destination container</param>
      /// <param name="itemGuid">item that needs to be sorted to an outbound container</param>
      /// <returns></returns>
      ///****************************************************************************
      private static void IsWhiteBinC2(ProtoContainer protoContainer, Guid itemGuid)
      {
         using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var itemCtrlNum =
               (from item in cloudEntities.Item
                join p in cloudEntities.Product on item.ProductID equals p.ProductID
                where item.ItemGUID == itemGuid && !item.IsDeleted && !p.IsDeleted
                select p.ControlNumber).FirstOrDefault();

            //grab list of items currently in the bin that the item is being processed into
            var protoItem =
               (from itemRel in cloudEntities.WrhsContrItemRel
                join item in cloudEntities.Item on itemRel.ItemGUID equals item.ItemGUID
                join p in cloudEntities.Product on item.ProductID equals p.ProductID
                where itemRel.WrhsContrID == protoContainer.ContainerInstance.ContainerId
                      && itemRel.WrhsInstID == protoContainer.ContainerInstance.InstanceId
                      && !item.IsDeleted && !itemRel.IsDeleted && !p.IsDeleted && protoContainer.Type == Constants.WhiteSortBin
                select p).ToList();

            //if there are no items for this container, then assume it's an empty bin
            if (!protoItem.Any())
            {
               return;
            }

            //determine whether that bin is a C2 or Audit white bin
            var protoIsC2 = protoItem.Any(c => c.ControlNumber == 2);

            if (protoContainer.Attribute == SidebarContainer.ATTR_OUTBOUND && protoContainer.Type == Constants.WhiteSortBin && protoIsC2 && itemCtrlNum != 2)
            {
               throw new Exception("This item must be sorted to an Audit White Bin");
            }

            if (protoContainer.Attribute == SidebarContainer.ATTR_OUTBOUND && protoContainer.Type == Constants.WhiteSortBin && !protoIsC2 && itemCtrlNum == 2)
            {
               throw new Exception("This item must be sorted to a C2 White Bin");
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine if the item and bins are recall and if the recall ids match
      /// </summary>
      /// <param name="protoContainer">the destination container</param>
      /// <param name="itemGuid">item that needs to be sorted to an outbound container</param>
      /// <returns></returns>
      ///****************************************************************************
      private static void IsRecall(ProtoContainer protoContainer, Guid itemGuid)
      {
         if (protoContainer.Type == Constants.RecallGaylord && protoContainer.RecallID != null && protoContainer.RecallID != "")
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var itemRecallId =
               (from item in cloudEntities.Item
                where item.ItemGUID == itemGuid
                select item.RecallID).FirstOrDefault();

               if (itemRecallId != null)
               {
                  if (Convert.ToInt64(protoContainer.RecallID) != itemRecallId)
                  {
                     throw new Exception($"This item must be sorted to a Recall { itemRecallId } Gaylord");
                  }
               }
               else
               {
                  throw new Exception("This item cannot be sorted to a Recall Gaylord");
               }
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Determine if the items waste profile id matches the gaylord
      /// </summary>
      /// <param name="protoContainer">the destination container</param>
      /// <param name="itemGuid">item that needs to be sorted to an outbound container</param>
      /// <returns></returns>
      ///****************************************************************************
      private static void IsWaste(ProtoContainer protoContainer, Guid itemGuid)
      {
         if (protoContainer.Type == Constants.WasteGaylord)
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var wasteStreamProfileId =
               (from item in cloudEntities.Item
                where item.ItemGUID == itemGuid
                select item.WasteStreamProfileID).FirstOrDefault();

               if (wasteStreamProfileId != protoContainer.WasteProfileID)
               {
                  throw new Exception($"This item must be sorted to a { wasteStreamProfileId } Waste Gaylord");
               }
            }
         }
      }
   }
}