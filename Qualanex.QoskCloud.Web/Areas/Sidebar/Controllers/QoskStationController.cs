﻿using Qualanex.Qosk.Library.Model.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Controllers
{

   [Authorize(Users = ("Admin2"))]
   public class QoskStationController : Controller
   {
      // GET: Sidebar/QoskStation
      public ActionResult Index()
      {
         var model = new Qosk.Library.Model.DBModel.Qosk();
         try
         {
            var objReturnLoginUserEntity =
               (ReturnLoginUserEntity)
               QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

            model.SerialNumber = "0081376";
            model.ProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode ?? 0;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }
         return this.View(model);
      }

      [ValidateAntiForgeryToken]
      public JsonResult CreateQosk(Qosk.Library.Model.DBModel.Qosk station)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var context = cloudEntities.Database.BeginTransaction();
            try
            {

               var oldStation = (from qosk in cloudEntities.Qosk
                                 where qosk.MachineID == station.MachineID
                                       && !qosk.IsDeleted
                                 select qosk.QoskIDx).FirstOrDefault();


               if (oldStation == null)
               {

                  var objReturnLoginUserEntity =
                     (ReturnLoginUserEntity)
                     QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);


                  var user = objReturnLoginUserEntity.UserReturnData;
                  station.ProfileCode = user.ProfileCode ?? 0;
                  station.QoskIDx = Common.Methods.NextId();
                  station.CreatedDate= DateTime.UtcNow;
                  station.CreatedBy = user.UserName;
                  station.ModifiedDate = station.CreatedDate;
                  station.ModifiedBy = station.CreatedBy;
                  station.Version = "1";
                  cloudEntities.Qosk.Add(station);
                  cloudEntities.SaveChanges();
                  return this.Json("Qosk record has been created");
               }
               else
               {
                  return this.Json("This station already has a qosk record");
               }



            }
            catch (Exception e)
            {
               context.Rollback();
               Logger.Error(e.Message);
               return this.Json(e.Message);
            }
            finally
            {
               context.Commit();
            }
         }
      }


   }
}
