﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ISidebarContainer.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Interface specifying required fields and methods for sidebar containers
/// </summary>
/// <remarks>
///   Since much of this is shared between all container types, an abstract implementation of this 
///   interface is available in SidebarContainer.cs
/// </remarks>
///-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Models
{
   public interface ISidebarContainer
   {
      /// <summary>
      /// Reference ID for the container/tote/bin etc
      /// </summary>
      [DisplayName("Container ID")]
      ContainerInstance ContainerInstance { get; set; }
      /// <summary>
      /// String specifying the type group of the container (tote, bin, gaylord, etc)
      /// </summary>
      string TypeGroup { get; }
      /// <summary>
      /// String specifying the container's type (green bin, yellow bin, large gaylord, small gaylord, etc)
      /// Must match Database WrhsContrTypeDict code
      /// </summary>
      string Type { get; }
      /// <summary>
      /// String specifying the container's waste profile (WSTE10000001, WSTE10000002, RtrnAuthID12... etc)
      /// </summary>
      string WasteProfileID { get; }
      /// <summary>
      /// Waste profile approval code
      /// </summary>
      string ApprovalCode { get; }
      /// <summary>
      /// Count of items currently in container
      /// </summary>
      int Count { get; }
      /// <summary>
      /// Control number for this container
      /// </summary>
      int CtrlNo { get; }
      /// <summary>
      /// Non Zero profile code indicates that items in this container
      /// are for an RA with the specified profile
      /// </summary>
      int ProfileCode { get; }
      /// <summary>
      /// Manufacturer name based on the profile code of the container
      /// </summary>
      string Name { get; }
      /// <summary>
      /// Severity Order - determines whether a waste profile ID is hazardous or not
      /// </summary>
      byte SeverityOrder { get; }
      /// <summary>
      /// When set, indicates that this container is for items included
      /// under the specified recall
      /// </summary>
      string RecallID { get; }
      /// <summary>
      /// Status of container.
      /// Must match WrhsContrStatusDict
      /// </summary>
      string Status { get; set; }
      /// <summary>
      /// Specifies if a container is inbound "I" or outbound "O"
      /// </summary>
      string Attribute { get; }
      /// <summary>
      /// Specifies if empty instances of this container should be shown or hidden
      /// </summary>
      bool ShowEmpty { get; set; }
      /// <summary>
      /// Specifies if this container is required for the application to continue
      /// </summary>
      bool Required { get; set; }
      /// <summary>
      /// Opens a container, attaching it to the station
      /// </summary>
      /// <param name="machineName">the Id of the current station</param>
      /// <param name="appCode">the UPA app code</param>
      /// <param name="currentStatus">acceptable statuses for the container prior to opening</param>
      /// <returns></returns>
      bool Open(string machineName, string appCode, List<string> currentStatus);
      /// <summary>
      /// closes the container setting the status to the next
      /// appropriate for the container type
      /// </summary>
      /// <returns>true if succesful</returns>
      bool Close(string machineName, string appCode);
      /// <summary>
      /// Suspend and disconnect this container, but will be 
      /// able to resume at previous state later on
      /// </summary>
      /// <returns>true if succesful</returns>
      bool Pause(string machineName, string appCode);
      /// <summary>
      /// Request a new instance of this container
      /// </summary>
      /// <returns>true if succesful</returns>
      string Request();
      /// <summary>
      /// Sets an item into the container. Removes from previous container 
      /// if necessary, returning the previous container's guid
      /// </summary>
      /// <param name="itemId">Guid for the item being moved to this container</param>
      /// <returns>Guid of item's previous container if available</returns>
      ContainerInstance Put(Guid itemId);
      /// <summary>
      /// Sets an item into the container. Removes from previous container 
      /// if necessary, returning the previous container's guid
      /// </summary>
      /// <param name="itemId">Guid for the item being moved to this container</param>
      /// <returns>Guid of item's previous container if available</returns>
      bool Remove(Guid itemId, ISidebarContainer container);
      /// <summary>
      /// Check if an item is currently in this container
      /// </summary>
      /// <param name="itemId">Item ID</param>
      /// <returns>True if Item is currently in container</returns>
      bool IsItemIn(Guid itemId);
      /// <summary>
      /// Gets a List of Item guids currently in the container
      /// </summary>
      /// <returns>List of Item guids currently in the container</returns>
      List<Guid> EnumItems();
      /// <summary>
      /// Get the gaylord label print string for closing a gaylord
      /// </summary>
      /// <param name="protoContainer">the container in question</param>
      /// <returns>String to be passed to print function</returns>
      string GetCloseContainerPrintString(ProtoContainer protoContainer);
      /// <summary>
      /// Get the gaylord label print string for closing a recall gaylord
      /// </summary>
      /// <param name="protoContainer">the container in question</param>
      /// <returns>String to be passed to print function</returns>
      string GetCloseContainerPrintStringRecall(ProtoContainer protoContainer);
   }
}
