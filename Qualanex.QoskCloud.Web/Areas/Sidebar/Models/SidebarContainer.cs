﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarContainer.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Abstract implementation of ISidebarContainer
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Web.Areas.Product.Models;
using Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Models;
using Qualanex.QoskCloud.Web.Model;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Models
{
   public abstract class SidebarContainer : ISidebarContainer
   {
      //constants for container status and attribute values
      public const string STATUS_AVAILABLE = "A";
      public const string STATUS_INPROCESS = "I";
      public const string STATUS_STAGED = "S";
      public const string STATUS_PREPARING = "P";
      public const string ATTR_INBOUND = "I";
      public const string ATTR_OUTBOUND = "O";
      //default constructor
      protected SidebarContainer() { }
      //constructor copies values from a protocontainer
      protected SidebarContainer(ProtoContainer container)
      {
         this.Count = container.Count;
         this.ContainerInstance.ContainerId = container.ContainerInstance.ContainerId;
         this.ContainerInstance.InstanceId = container.ContainerInstance.InstanceId;
         this.Status = container.Status;
         this.Type = container.Type;
         this.TypeGroup = container.TypeGroup;
         this.WasteProfileID = container.WasteProfileID;
         this.CtrlNo = container.CtrlNo;
         this.RecallID = container.RecallID;
         this.ProfileCode = container.ProfileCode;
         this.Attribute = container.Attribute;
         this.ShowEmpty = container.ShowEmpty;
         this.Required = container.Required;
         this.ApprovalCode = container.ApprovalCode;
         this.Name = container.Name;
         this.SeverityOrder = container.SeverityOrder;
      }
      public virtual ContainerInstance ContainerInstance { get; set; } = new ContainerInstance();
      public virtual string TypeGroup { get; }
      public virtual string Type { get; }
      public virtual string WasteProfileID { get; }
      public virtual int CtrlNo { get; }
      public virtual int ProfileCode { get; }
      public virtual string RecallID { get; }
      public virtual int Count { get; set; }
      public virtual string Status { get; set; }
      public virtual string Attribute { get; set; } = ATTR_OUTBOUND;
      public bool ShowEmpty { get; set; }
      public virtual bool Required { get; set; }
      public virtual string ApprovalCode { get; set; }
      public virtual string Name { get; set; }
      public virtual byte SeverityOrder { get; set; }

      /// <summary>
      /// Generic implementation of Open
      /// </summary>
      /// <param name="machineName"></param>
      /// <param name="appCode"></param>
      /// <param name="currentStatus"></param>
      /// <returns></returns>
      public virtual bool Open(string machineName, string appCode, List<string> currentStatus)
      {
         /*
          * Definitions:
          *    Containers Attributes
          *       OB - Outbound
          *       IB - Inbound
          */

         var protoContainer = SidebarCommon.GetContainer(this.ContainerInstance.ContainerId);
         currentStatus.Remove("");

         //verify the container and status are correct
         if (protoContainer == null)
         {
            throw new Exception("The requested container could not be found");
         }

         if (!VerifyType(protoContainer))
         {
            throw new Exception($"Container must be a {this.TypeGroup} ({this.Type})");
         }

         if (this.Attribute == ATTR_INBOUND && IsInboundOpen(machineName, appCode))
         {
            throw new Exception("Only one inbound container can be attached at a time, 1");
         }

         if (currentStatus.Count > 0 && !currentStatus.Contains(protoContainer.Status))
         {
            //this occurs when we're taking an OB container previously used in an applet, and
            //we're attaching it to another applet as an IB container
            if (this.Attribute == ATTR_INBOUND && protoContainer.Status == STATUS_PREPARING)
            {
               this.UpdateOpen(machineName);
            }
            else if (this.Attribute == ATTR_INBOUND && protoContainer.Status == STATUS_AVAILABLE)
            {
               throw new Exception("Container is not in the correct status, 1");
            }
            else
            {
               throw new Exception("Container is currently in use on another station, 1");
            }
         }

         //ignore any OB containers that have been used as IB containers (attached to another applet)
         if (this.Attribute == ATTR_OUTBOUND && protoContainer.Attribute == ATTR_INBOUND && !new List<string> { STATUS_AVAILABLE }.Contains(protoContainer.Status))
         {
            throw new Exception("Container is being processed in another application, 1");
         }

         //For Induction Audit ONLY - do not attach a white audit bin if it has any items processed by the auditor; also ignore any C2 White Bins
         if (appCode == "WAREHS_AUDIT" && this.Attribute == ATTR_INBOUND)
         {
            var auditBinventory = BinventoryDataAccess.GetBinventoryList(this.ContainerInstance.ContainerId, "", "", appCode);

            if (auditBinventory.Any(c => c.CtrlNumber == "2"))
            {
               throw new Exception($"C2 Container cannot be Audited, 1");
            }

            if (auditBinventory.Any(c => c.CreatedBy.ToLower() == SidebarCommon.GetUserName().ToLower()))
            {
               throw new Exception($"Container cannot be Audited as it includes items processed by the auditor, 1");
            }
         }

         if (!this.WasteProfileID.IsNullOrWhiteSpace() && this.WasteProfileID != protoContainer.WasteProfileID)
         {
            throw new Exception($"Container must have a category of {this.WasteProfileID}, 1");
         }

         //update the status
         this.ContainerInstance.InstanceId = protoContainer.ContainerInstance.InstanceId;
         this.UpdateStatus(machineName, appCode);
         return true;
      }

      /// <summary>
      /// Determines if an inbound container is currently open on the given
      /// station and application
      /// </summary>
      /// <param name="machineName">the current station's Id</param>
      /// <param name="appCode">the current application</param>
      /// <returns></returns>
      public bool IsInboundOpen(string machineName, string appCode)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               return (from statusHist in cloudEntities.WrhsContrStatusHist
                       join qosk in cloudEntities.Qosk on statusHist.QoskID equals qosk.QoskIDx
                       where qosk.MachineID == machineName
                       && statusHist.WrhsContrAttribCode == ATTR_INBOUND
                       && statusHist.StatusCode == STATUS_INPROCESS
                       && statusHist.AppCode == appCode
                       && statusHist.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                     where olderStatus.WrhsContrID == statusHist.WrhsContrID
                                                     select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault()
                       select statusHist.WrhsContrID).Any();
            }
            catch (Exception e)
            {
               throw;
            }
         }
      }
      /// <summary>
      /// Determines that the type is correct for the generic case
      /// </summary>
      /// <param name="container"></param>
      /// <returns></returns>
      protected virtual bool VerifyType(ISidebarContainer container)
      {
         return container.Type == this.Type;
      }

      /// <summary>
      /// Closes the container
      /// </summary>
      /// <param name="machineName"></param>
      /// <param name="appCode"></param>
      /// <returns></returns>
      public virtual bool Close(string machineName, string appCode)
      {
         if (this.Status == STATUS_INPROCESS || this.Status == STATUS_PREPARING)
         {
            throw new Exception("Status of closed container cannot be In-Process or Preparing");
         }

         this.UpdateStatus(machineName, appCode);
         return true;
      }

      /// <summary>
      /// Pauses the container
      /// </summary>
      /// <param name="machineName"></param>
      /// <param name="appCode"></param>
      /// <returns></returns>
      public virtual bool Pause(string machineName, string appCode)
      {
         if (this.Status == STATUS_INPROCESS || this.Status == STATUS_PREPARING)
         {
            throw new Exception("Status of Paused container cannot be In-Process or Preparing");
         }

         this.UpdateStatus(machineName, appCode);
         return true;
      }

      /// <summary>
      /// Generic implementation of request. Always returns true without creating
      /// a new container unless overridden
      /// </summary>
      /// <returns></returns>
      public virtual string Request()
      {
         return "true";
      }

      /// <summary>
      /// Puts an item into this container
      /// </summary>
      /// <param name="itemId">Item to be moved into the current container</param>
      /// <returns></returns>
      public virtual ContainerInstance Put(Guid itemId)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var context = cloudEntities.Database.BeginTransaction();
            ContainerInstance previousContainer = null;

            try
            {
               //Find the item's previous location
               var previousContainerRel = (from container in cloudEntities.WrhsContrItemRel
                                           where container.ItemGUID == itemId && container.IsActive
                                           select container).OrderByDescending(s => s.CreatedDate).FirstOrDefault();

               if (previousContainerRel != null)
               {
                  previousContainer = new ContainerInstance()
                  {
                     InstanceId = previousContainerRel.WrhsInstID,
                     ContainerId = previousContainerRel.WrhsContrID
                  };
               }
               else if (previousContainerRel == null)
               {
                  previousContainer = new ContainerInstance()
                  {
                     InstanceId = this.ContainerInstance.InstanceId,//.WrhsInstID,
                     ContainerId = this.ContainerInstance.ContainerId//previousContainerRel.WrhsContrID
                  };
               }

               //verify that the item exists
               if (!(from item in cloudEntities.Item where item.ItemGUID == itemId select item.ItemGUID).Any())
               {
                  throw new Exception("Item could not be found");
               }

               //Determine if the item had been in this container before
               var username = SidebarCommon.GetUserName();
               var newItem = (from item in cloudEntities.WrhsContrItemRel
                              where item.ItemGUID == itemId
                              && item.WrhsContrID == this.ContainerInstance.ContainerId
                              && item.WrhsInstID == this.ContainerInstance.InstanceId
                              select item).OrderByDescending(c => c.CreatedDate).FirstOrDefault();

               if (newItem == null) //add a new relation record
               {
                  var newContainerRel = new WrhsContrItemRel()
                  {
                     ItemGUID = itemId,
                     Version = 1,
                     IsActive = true,
                     CreatedBy = username,
                     CreatedDate = DateTime.UtcNow,
                     WrhsContrID = this.ContainerInstance.ContainerId,
                     WrhsInstID = this.ContainerInstance.InstanceId,
                  };

                  cloudEntities.WrhsContrItemRel.Add(newContainerRel);
               }
               else //update the previous relation record
               {
                  newItem.IsActive = true;
                  newItem.IsDeleted = false;
                  newItem.ModifiedBy = username;
                  newItem.ModifiedDate = DateTime.UtcNow;
               }

               if (previousContainerRel != null)
               {
                  previousContainerRel.IsActive = false;
               }

               cloudEntities.SaveChanges();
               return previousContainer;
            }
            catch (Exception e)
            {
               context.Rollback();
               throw e;
            }
            finally
            {
               context.Commit();
            }
         }
      }

      /// <summary>
      /// Removes an item from a container
      /// </summary>
      /// <param name="itemId">The ID of the item to be removed</param>
      /// <param name="containerInfo">The container from which it is removed</param>
      /// <returns></returns>
      public virtual bool Remove(Guid itemId, ISidebarContainer containerInfo)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var context = cloudEntities.Database.BeginTransaction();

            try
            {
               var username = SidebarCommon.GetUserName();
               var previousContainerRel = (from container in cloudEntities.WrhsContrItemRel
                                           where container.ItemGUID == itemId
                                           && container.WrhsContrID == containerInfo.ContainerInstance.ContainerId
                                           && container.WrhsInstID == containerInfo.ContainerInstance.InstanceId
                                           select container).OrderByDescending(s => s.CreatedDate).FirstOrDefault();

               if (previousContainerRel != null)
               {
                  previousContainerRel.IsDeleted = true;
                  previousContainerRel.IsActive = false;
                  previousContainerRel.ModifiedBy = username;
                  previousContainerRel.ModifiedDate = DateTime.UtcNow;
               }

               return true;
            }
            catch (Exception e)
            {
               context.Rollback();
               throw e;
            }
            finally
            {
               context.Commit();
            }
         }
      }

      /// <summary>
      /// Determine if an item is in this container
      /// </summary>
      /// <param name="itemId">The id of the item</param>
      /// <returns></returns>
      public virtual bool IsItemIn(Guid itemId)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               var previousContainerRel = (from container in cloudEntities.WrhsContrItemRel
                                           where container.ItemGUID == itemId
                                           select container).OrderByDescending(s => s.CreatedDate).FirstOrDefault();

               return previousContainerRel != null
                      && previousContainerRel.WrhsContrID == this.ContainerInstance.ContainerId
                      && previousContainerRel.WrhsInstID == this.ContainerInstance.InstanceId;
            }
            catch (Exception e)
            {
               throw;
            }
         }
      }

      /// <summary>
      /// Return a list of item IDs in this container
      /// </summary>
      /// <returns></returns>
      public virtual List<Guid> EnumItems()
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               return (from container in cloudEntities.WrhsContrItemRel
                       where container.WrhsContrID == this.ContainerInstance.ContainerId
                             && container.WrhsInstID == this.ContainerInstance.InstanceId
                             && container.CreatedDate == (from olderContainer in cloudEntities.WrhsContrItemRel
                                                          where olderContainer.WrhsContrID == container.WrhsContrID
                                                          select olderContainer.CreatedDate).OrderByDescending(x => x).FirstOrDefault()
                       select container.ItemGUID).ToList();
            }
            catch (Exception e)
            {
               throw;
            }
         }
      }
      /// <summary>
      /// Updates the status record to indicate that the container is open
      /// on the specified station
      /// </summary>
      /// <param name="machineId">The current station's ID</param>
      public void UpdateOpen(string machineId)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var context = cloudEntities.Database.BeginTransaction();
            try
            {
               var oldStatus = (from status in cloudEntities.WrhsContrStatusHist
                                where status.WrhsContrID == this.ContainerInstance.ContainerId
                                select status).OrderByDescending(s => s.CreatedDate).FirstOrDefault();

               var qoskId = (from qosk in cloudEntities.Qosk
                             where qosk.MachineID == machineId && !qosk.IsDeleted
                             select qosk.QoskIDx).First();

               if (this.Status != null && (oldStatus.StatusCode == STATUS_PREPARING || this.Status == STATUS_PREPARING))
               {
                  var username = SidebarCommon.GetUserName();
                  var newStatus = new WrhsContrStatusHist
                  {
                     StatusCode = STATUS_STAGED,
                     QoskID = qoskId,
                     AppCode = oldStatus.AppCode,
                     CreatedBy = username,
                     CreatedDate = DateTime.UtcNow,
                     WrhsContrID = oldStatus.WrhsContrID,
                     WrhsInstID = oldStatus.WrhsInstID,
                     WrhsContrAttribCode = oldStatus.WrhsContrAttribCode
                  };

                  cloudEntities.WrhsContrStatusHist.Add(newStatus);
                  cloudEntities.SaveChanges();
               }
            }
            catch (Exception e)
            {
               context.Rollback();
               Logger.Error(e.Message);
               throw new Exception("There was an error updating this container's status");
            }
            finally
            {
               context.Commit();
            }
         }
      }

      /// <summary>
      /// Updates the status of this container
      /// </summary>
      /// <param name="machineId">the station performing the update</param>
      /// <param name="appCode">the current application</param>
      public void UpdateStatus(string machineId, string appCode)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var context = cloudEntities.Database.BeginTransaction();
            try
            {
               var oldStatus = (from status in cloudEntities.WrhsContrStatusHist
                                join wrhs in cloudEntities.WrhsContr on status.WrhsContrID equals wrhs.WrhsContrID
                                where status.WrhsContrID == this.ContainerInstance.ContainerId
                                orderby status.CreatedDate descending
                                select new { History = status, wrhs.IsShareable }).FirstOrDefault();

               var qoskId = (from qosk in cloudEntities.Qosk
                             where qosk.MachineID == machineId && !qosk.IsDeleted
                             select qosk.QoskIDx).First();

               if (this.Status == STATUS_AVAILABLE)
               {
                  this.ContainerInstance.InstanceId = Common.Methods.NextId();
               }

               if (this.Status != null
                  && (oldStatus.History.StatusCode == STATUS_PREPARING || this.Status == STATUS_PREPARING) || this.Status != oldStatus.History.StatusCode)
               {
                  if (!oldStatus.IsShareable && oldStatus.History.StatusCode == STATUS_PREPARING && appCode != oldStatus.History.AppCode)
                  {
                     UpdateOpen(machineId);

                     //this is to ensure the pausing of the bin from one application and opening it in another
                     //below doesn't lead to the two entries being entered at the same time
                     System.Threading.Thread.Sleep(500);
                  }

                  var username = SidebarCommon.GetUserName();
                  var newStatus = new WrhsContrStatusHist
                  {
                     StatusCode = this.Status,
                     QoskID = qoskId,
                     AppCode = appCode,
                     CreatedBy = username,
                     CreatedDate = DateTime.UtcNow,
                     WrhsContrID = this.ContainerInstance.ContainerId,
                     WrhsInstID = this.ContainerInstance.InstanceId,
                     WrhsContrAttribCode = this.Attribute
                  };

                  cloudEntities.WrhsContrStatusHist.Add(newStatus);
                  cloudEntities.SaveChanges();
               }
            }
            catch (Exception e)
            {
               context.Rollback();
               Logger.Error(e.Message);
               throw new Exception("There was an error updating this container's status");
            }
            finally
            {
               context.Commit();
            }
         }
      }

      /// <summary>
      /// Build the print string for closing gaylords. This varies from the "open" gaylord 
      /// label in that the RCRA codes are only what is currently in the gaylord
      /// </summary>
      /// <param name="protocontainer">the metadata about the container we are closing</param>
      public string GetCloseContainerPrintString(ProtoContainer protocontainer)
      {
         var result = string.Empty;
         var contrCaption = string.Empty;
         var contrType = string.Empty;
         var contrMfg = string.Empty;
         var contrRCRA = string.Empty;
         DateTime? contrOpenDate = (DateTime?)null;
         var contrCloseDate = DateTime.Today;
         var cntrApproval = string.Empty;
         var contrCtrl = string.Empty;
         var contrCount = 0;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               cntrApproval = (from entity in cloudEntities.WasteStreamProfile
                               where entity.WasteProfileID == protocontainer.WasteProfileID
                               select entity.ApprovalCode).FirstOrDefault();

               contrCtrl = protocontainer.CtrlNo == 2 ? "§CTRLNO=C%20II" : protocontainer.CtrlNo == 345 ? "§CTRLNO=C%0AIII%20IV%20V" : string.Empty;

               var cntrSeverity = (from wcpr in cloudEntities.WasteCodeProfileRel
                                   join wcd in cloudEntities.WasteCodeDict on wcpr.WasteCode equals wcd.Code
                                   where wcpr.WasteStreamProfileID == protocontainer.WasteProfileID
                                   select wcd.SeverityOrder).FirstOrDefault();

               contrMfg = protocontainer.ProfileCode == 0 ? "Multiple%20Manufacturers" :
                           (from entity in cloudEntities.Profile
                            where entity.ProfileCode == protocontainer.ProfileCode
                            select entity.Name).FirstOrDefault();

               if (cntrSeverity > 3)
               {
                  contrCaption = "Non-Hazardous%20Waste";
                  contrType = string.IsNullOrEmpty(protocontainer.Type)
                     ? protocontainer.CtrlNo == 2
                        ? Constants.BlackGaylord
                        : protocontainer.CtrlNo == 345
                           ? Constants.OrangeGaylord
                           : protocontainer.ProfileCode != 0
                              ? Constants.SmallGaylord
                              : Constants.LargeGaylord
                     : protocontainer.Type;
               }
               else
               {
                  contrCaption = "Hazardous%20Waste";
                  contrType = string.IsNullOrEmpty(protocontainer.Type)
                     ? Constants.WasteGaylord
                     : protocontainer.Type;

                  var wasteCodeQuery = "SELECT DISTINCT pw.WasteCode from ProductWasteStream pw " +
                                       "INNER JOIN Item i ON CASE " +
                                       "WHEN i.RollupProductId != '' then i.RollupProductId " +
                                       "ELSE i.ProductId " +
                                       "END " +
                                       "= pw.ProductId " +
                                       "INNER JOIN WrhsContrItemRel w on w.ItemGUID = i.ItemGUID " +
                                       $"WHERE w.WrhsContrId = '{this.ContainerInstance.ContainerId}' " +
                                       "AND pw.StateCode = ''";

                  var rcra = cloudEntities.Database.SqlQuery<WasteCodeResults>(wasteCodeQuery).ToList();

                  contrRCRA = "§RCRA=RCRA%20Codes:%0A";

                  foreach (var x in rcra.Select((value, i) => new { i, value }))
                  {
                     var sep = x.i == rcra.Count - 1
                        ? string.Empty
                        : (x.i + 1) % 7 == 0
                           ? "%0A"
                           : ",";

                     contrRCRA += $"{x.value.WasteCode}{sep}";
                  }
               }

               contrCount = (from entity in cloudEntities.WrhsContrItemRel
                             where entity.WrhsContrID == this.ContainerInstance.ContainerId
                             && entity.IsActive == true
                             && entity.IsDeleted == false
                             select entity.ItemGUID).Count();

               contrOpenDate = (from entity in cloudEntities.WrhsContr
                                where entity.WrhsContrID == this.ContainerInstance.ContainerId
                                select entity.CreatedDate).FirstOrDefault();
            }
            catch (Exception e)
            {
               Logger.Error(e.Message);
               return null;
            }

            result = $"CONTRID={this.ContainerInstance.ContainerId}§CAPTION={contrCaption}{contrCtrl}§APPRVLNO={cntrApproval}§MFG={contrMfg}§DATEOPEN={contrOpenDate:dd-MMM-yyyy}§DATECLSD={contrCloseDate:dd-MMM-yyyy}§CONTRCNT={contrCount.ToString()}{contrRCRA}";
         }

         return result;
      }

      /// <summary>
      /// Build the print string for closing recall gaylords
      /// </summary>
      /// <param name="protocontainer">the metadata about the container we are closing</param>
      public string GetCloseContainerPrintStringRecall(ProtoContainer protocontainer)
      {
         var result = string.Empty;
         var contrCaption = string.Empty;
         var contrNDC = string.Empty;
         var contrLotNo = string.Empty;
         DateTime? contrOpenDate = (DateTime?)null;
         var contrCloseDate = DateTime.Today;
         var cntrApproval = string.Empty;
         var contrCtrl = string.Empty;
         var contrCount = 0;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               var recallNo = Convert.ToInt64(protocontainer.RecallID);

               // get recall info
               ///////////////////////////////////////////////////////////////////////////////
               var recallRec = (from entityR in cloudEntities.Recall
                                join entityP in cloudEntities.Profile on entityR.ManufacturerProfileCode equals entityP.ProfileCode
                                where entityR.RecallID == recallNo && !entityR.IsDeleted
                                select new
                                {
                                   entityR.RecallNumber,
                                   entityP.ProfileCode,
                                   entityP.Name
                                }).FirstOrDefault();

               // Is this a real recall?
               if (recallRec != null)
               {
                  contrCaption = $"Recall%20Number:%20{recallRec.RecallNumber}";

                  var recallProd = (from entityRpl in cloudEntities.RecallProductRel
                                    join entityP in cloudEntities.Product on entityRpl.ProductID equals entityP.ProductID
                                    where entityRpl.RecallID == recallNo && !entityP.IsDeleted
                                    select new
                                    {
                                       entityP.ProductID,
                                       entityP.NDC,
                                       entityP.UPC,
                                       entityP.NDCUPCWithDashes,
                                       entityRpl.AllLots
                                    }).ToList();

                  // Approval Code
                  ///////////////////////////////////////////////////////////////////////////////
                  cntrApproval = (from entity in cloudEntities.WasteStreamProfile
                                  where entity.WasteProfileID == protocontainer.WasteProfileID
                                  select entity.ApprovalCode).FirstOrDefault();

                  // Control Number
                  ///////////////////////////////////////////////////////////////////////////////
                  contrCtrl = protocontainer.CtrlNo == 2
                     ? "§CTRLNO=C%20II"
                     : protocontainer.CtrlNo == 345
                        ? "§CTRLNO=C%0AIII%20IV%20V"
                        : string.Empty;

                  // NDC(s) - only get the NDCs that were processed
                  ///////////////////////////////////////////////////////////////////////////////
                  contrNDC = "§NDC=";

                  var processedItems =
                     (from item in cloudEntities.Item
                      join itemRel in cloudEntities.WrhsContrItemRel on item.ItemGUID equals itemRel.ItemGUID
                      where itemRel.WrhsContrID == this.ContainerInstance.ContainerId
                            && !itemRel.IsDeleted && !item.IsDeleted
                      select item.RollupProductID ?? item.ProductID).Distinct().ToList();

                  var ndcList = recallProd.Where(p => processedItems.Contains(p.ProductID)).Select(x => x.NDCUPCWithDashes).ToList();

                  foreach (var x in ndcList.Select((value, i) => new { i, value }))
                  {
                     var sep = x.i == ndcList.Count - 1
                        ? string.Empty
                        : (x.i + 1) % 3 == 0
                           ? "%0A"
                           : ",";

                     contrNDC += $"{x.value}{sep}";
                  }

                  // Lot Numbers - only get the lots that were processed
                  ///////////////////////////////////////////////////////////////////////////////

                  var lotList =
                     (from item in cloudEntities.Item
                      join itemRel in cloudEntities.WrhsContrItemRel on item.ItemGUID equals itemRel.ItemGUID
                      where itemRel.WrhsContrID == this.ContainerInstance.ContainerId
                            && !itemRel.IsDeleted && !item.IsDeleted
                      select item.LotNumber).Distinct().ToList();

                  if (lotList.Count > 10)
                  {
                     contrLotNo = "§LOTNO=Multiple%20Lots";
                  }
                  else
                  {
                     contrLotNo = "§LOTNO=";

                     foreach (var x in lotList.Select((value, i) => new { i, value }))
                     {
                        var sep = x.i == lotList.Count - 1
                           ? string.Empty
                           : (x.i + 1) % 2 == 0
                              ? "%0A"
                              : ",";

                        contrLotNo += $"{x.value}{sep}";
                     }
                  }

                  contrCount = (from entity in cloudEntities.WrhsContrItemRel
                                where entity.WrhsContrID == this.ContainerInstance.ContainerId
                                && entity.IsActive
                                && entity.IsDeleted == false
                                select entity.ItemGUID).Count();

                  contrOpenDate = (from entity in cloudEntities.WrhsContr
                                   where entity.WrhsContrID == this.ContainerInstance.ContainerId
                                   select entity.CreatedDate).FirstOrDefault();

                  result = $"CONTRID={this.ContainerInstance.ContainerId}§CAPTION={contrCaption}{contrCtrl}§APPRVLNO={cntrApproval}§MFG={recallRec.Name}§DATEOPEN={contrOpenDate:dd-MMM-yyyy}§DATECLSD={contrCloseDate:dd-MMM-yyyy}§CONTRCNT={contrCount.ToString()}{contrNDC}{contrLotNo}";
               }
            }
            catch (Exception e)
            {
               Logger.Error(e.Message);
               return null;
            }
         }

         return result;
      }
   }
   /// <summary>
   /// Model for the two identifying fields of a container
   /// </summary>
   public class ContainerInstance
   {
      /// <summary>
      /// ID of a physical container
      /// </summary>
      public string ContainerId { get; set; }
      /// <summary>
      /// Instance of a container for when
      /// containers are reused
      /// </summary>
      public string InstanceId { get; set; }
   }
   /// <summary>
   /// Protocontainer matches the properties of a sidebarcontainer
   /// for use when the type of a container is unknown upon entering a method
   /// </summary>
   public class ProtoContainer
   {
      public ContainerInstance ContainerInstance { get; set; } = new ContainerInstance();
      public string TypeGroup { get; set; }
      public string Type { get; set; }
      public string WasteProfileID { get; set; }
      public string Attribute { get; set; } = SidebarContainer.ATTR_OUTBOUND;
      public int Count { get; set; }
      public int CtrlNo { get; set; }
      public int ProfileCode { get; set; }
      public string RecallID { get; set; }
      public string Status { get; set; }
      public string CurrentLocation { get; set; }
      public bool ShowEmpty { get; set; }
      public bool Required { get; set; }
      public string ApprovalCode { get; set; }
      public string Name { get; set; }
      public byte SeverityOrder { get; set; }
   }
   public class WasteCodeResults
   {
      public string WasteCode { get; set; }
   }
}