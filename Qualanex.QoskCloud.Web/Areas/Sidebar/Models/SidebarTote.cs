﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarTote.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Tote specific implementation of ISidebarContainer
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Models
{
   public class SidebarTote:SidebarContainer
   {
      /// <summary>
      /// Set the typegroup to tote
      /// </summary>
      public override string TypeGroup { get; } = Constants.Tote;

      /// <summary>
      /// Override of Verify type since gray and blue induction totes should be treated
      /// as the same type
      /// </summary>
      /// <param name="container"></param>
      /// <returns></returns>
      protected override bool VerifyType(ISidebarContainer container)
      {
         return new List<string>(){Constants.GrayInduction,Constants.BlueInduction,Constants.ClearSortBin}.Contains(container.Type);
      }
      /// <summary>
      /// Override of IsItemIn to return true if an item has ever been in a tote
      /// since all item rel records for totes are added when moving to another container
      /// </summary>
      /// <param name="itemId"></param>
      /// <returns></returns>
      public override bool IsItemIn(Guid itemId)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {

               return (from container in cloudEntities.WrhsContrItemRel
                       where container.ItemGUID == itemId
                       && container.WrhsContrID == this.ContainerInstance.ContainerId
                       && container.WrhsInstID == this.ContainerInstance.InstanceId
                       select container.ItemGUID).Any();

            }
            catch (Exception e)
            {
               throw;
            }
         }
      }

      /// <summary>
      /// Override of EnumItems to return all items that have ever been in this tote
      /// since all item rel records for totes are added when moving to another container
      /// </summary>
      /// <returns></returns>
      public override List<Guid> EnumItems()
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {

               return (from container in cloudEntities.WrhsContrItemRel
                       where  container.WrhsContrID == this.ContainerInstance.ContainerId
                             && container.WrhsInstID == this.ContainerInstance.InstanceId
                       select container.ItemGUID).ToList();

            }
            catch (Exception e)
            {
               throw;
            }
         }
      }

      public SidebarTote(ProtoContainer container) : base(container)
      {
      }

      public SidebarTote()
      {
      }
   }
}