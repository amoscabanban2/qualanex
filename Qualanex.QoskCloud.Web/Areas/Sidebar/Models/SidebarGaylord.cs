﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarGaylord.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Gaylord specific implementation of ISidebarContainer
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Microsoft.Ajax.Utilities;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Models
{
   using System;

   using Qualanex.QoskCloud.Web.Areas.WrhsContrMgmt.Models;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   using Constants = Qualanex.QoskCloud.Utility.Constants;
   
   public class SidebarGaylord : SidebarContainer
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Constructor from protocontainer.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public SidebarGaylord(ProtoContainer container) : base(container)
      {
         this.WasteProfileID = container.WasteProfileID;
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~SidebarGaylord()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Specify the type group as gaylord
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string TypeGroup { get; } = Constants.Gaylord;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string WasteProfileID { get; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Override of Request functionality for gaylords.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public override string Request()
      {
         var wasteProfileId = this.WasteProfileID;

         if (wasteProfileId.IsNullOrWhiteSpace())
         {
            switch (this.Type)
            {
               // .. unless specified by the calling code, known gaylords are set to non hazardous
               // .. waste profile
               //=============================================================================
               case Constants.LargeGaylord:
               case Constants.SmallGaylord:
               case Constants.OrangeGaylord:
                  wasteProfileId = WrhsContrMgmtModel.GetWasteStreamProfileId(this.CtrlNo);
                  break;

               // ..
               //=============================================================================
               default:
                  break;
            }
         }

         //we cannot request a gaylord with a missing waste stream
         if (wasteProfileId.IsNullOrWhiteSpace())
         {
            var exception = "Controller call failed on Request due to a missing Waste Stream ID. Please contact your supervisor.";
            throw new Exception(exception);
         }

         //create the new container
         return this.RecallID.IsNullOrWhiteSpace()
            ? WrhsContrMgmtModel.CreateGaylord(wasteProfileId, this.CtrlNo, this.ProfileCode, this.Type)
            : WrhsContrMgmtModel.CreateGaylord(this.RecallID, wasteProfileId, this.CtrlNo);
      }

      #endregion
   }
}
