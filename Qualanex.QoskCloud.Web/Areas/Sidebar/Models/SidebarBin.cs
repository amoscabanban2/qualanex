﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarBin.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Bin specific implementation of ISidebarContainer
/// </summary>
/// <remarks>
///   Bins are generic containers and do not override default functionality except to specify the type
/// </remarks>
///-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar.Models
{
   //Extend the abstract implementation
   public class SidebarBin : SidebarContainer
   {
      public SidebarBin(ProtoContainer container) : base(container)
      {
      }

      public override string TypeGroup { get; } = Constants.Bin;
     
      
   }
}