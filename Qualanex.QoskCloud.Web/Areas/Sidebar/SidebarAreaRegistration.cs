﻿using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar
{
    public class SidebarAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Sidebar";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Sidebar_default",
                "Sidebar/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}