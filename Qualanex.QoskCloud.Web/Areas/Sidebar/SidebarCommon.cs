﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="SidebarCommon.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Helper methods used throughout the sidebar area as well as common methods relating to containers
///   for use in other areas
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.UPAModel;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Web.Areas.Product;
using Qualanex.QoskCloud.Web.Areas.Product.Models;
using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;
using Qualanex.QoskCloud.Web.ViewModels;

namespace Qualanex.QoskCloud.Web.Areas.Sidebar
{
   public class SidebarCommon
   {
      /// <summary>
      /// Retrieves the UPA Viewmodel from session memory or the database
      /// </summary>
      /// <param name="appCode">the upa application</param>
      /// <param name="pageViewId">the pageviewid passed with the request</param>
      /// <returns></returns>
      public static UpaViewModel SidebarUpaViewModel(string appCode, string pageViewId)
      {
         var upaApplication = (UPAApplication)QoskSessionHelper.GetSessionData(Constants.UpaApplicationKey + pageViewId);

         if (upaApplication == null)
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
            int userProfileCode = -99;

            if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
            {
               userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
            }

            upaApplication = UpaDataAccess.GetUpaModel(appCode, objReturnLoginUserEntity.UserReturnData.UserID, userProfileCode);
         }

         QoskSessionHelper.SetSessionData(Constants.UpaApplicationKey + pageViewId, upaApplication);

         return new UpaViewModel()
         {
            UpaApplication = upaApplication
         };

      }

      /// <summary>
      /// retrieves the current user name from the session
      /// </summary>
      /// <returns></returns>
      public static string GetUserName()
      {
         try
         {
            var objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
            return objReturnLoginUserEntity.UserReturnData.UserName;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw new Exception("User session has expired <script>LogOut();</script>");
         }
      }

      /// <summary>
      /// retrieves the machine name from the http request cookie
      /// </summary>
      /// <param name="request"></param>
      /// <returns></returns>
      public static string GetMachineName(HttpRequestBase request)
      {
         if (request.Cookies.Get("MachineName") == null)
         {
            throw new Exception("Qosk Tray Service must be available to run this application");
         }

         return request.Cookies.Get("MachineName").Value;
      }

      /// <summary>
      /// Find the generic type group for a specific type (BIN from RED_BIN)
      /// </summary>
      /// <param name="type"></param>
      /// <returns></returns>
      public static string GetGroupFromType(string type)
      {
         if (Constants.ToteTypes.Contains(type))
         {
            return Constants.Tote;
         }

         if (Constants.BinTypes.Contains(type))
         {
            return Constants.Bin;
         }

         return Constants.GaylordTypes.Contains(type) ? Constants.Gaylord : null;
      }
      /// <summary>
      /// Database lookup to populate a container by containerId
      /// </summary>
      /// <param name="containerID"></param>
      /// <returns></returns>
      public static ISidebarContainer GetContainer(string containerID)
      {
         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var containers =
                  from c in cloudEntities.WrhsContr
                  join status in cloudEntities.WrhsContrStatusHist on c.WrhsContrID equals status.WrhsContrID
                  join waste in cloudEntities.WasteStreamProfile on c.WasteProfileID equals waste.WasteProfileID into d
                  from wastelist in d.DefaultIfEmpty()
                  join profile in cloudEntities.Profile on c.ProfileCode equals profile.ProfileCode into y
                  from profilelist in y.DefaultIfEmpty()
                  where c.WrhsContrID == containerID
                        && status.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                  where olderStatus.WrhsContrID == status.WrhsContrID
                                                  select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault()
                  select
                  new ProtoContainer()
                  {
                     Attribute = status.WrhsContrAttribCode,
                     ContainerInstance = new ContainerInstance
                     {
                        ContainerId = c.WrhsContrID,
                        InstanceId = status.WrhsInstID
                     },
                     Type = c.Type,
                     WasteProfileID = c.WasteProfileID,
                     Status = status.StatusCode,
                     CurrentLocation = status.QoskID,
                     Name = profilelist.Name,
                     ApprovalCode = wastelist.ApprovalCode,
                     ProfileCode = c.ProfileCode
                  };

               return containers.Any() ? ContainerFromProto(containers.First()) : null;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /// <summary>
      /// Retrieves a virtual container for the specified type and category (now wasteprofileId)
      /// </summary>
      /// <param name="containerType"></param>
      /// <param name="category"></param>
      /// <returns></returns>
      public static ISidebarContainer GetVirtualContainer(string containerType, string category = null)
      {
         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var containers =
                  from c in cloudEntities.WrhsContr
                  join status in cloudEntities.WrhsContrStatusHist on c.WrhsContrID equals status.WrhsContrID
                  where c.Type == containerType
                        && status.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                  where olderStatus.WrhsContrID == status.WrhsContrID
                                                        && olderStatus.WrhsInstID == status.WrhsInstID
                                                  select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault()
                     && (string.IsNullOrEmpty(category) || string.IsNullOrEmpty(c.WasteProfileID) || c.WasteProfileID == category)
                  select
                  new ProtoContainer()
                  {
                     ContainerInstance = new ContainerInstance
                     {
                        ContainerId = c.WrhsContrID,
                        InstanceId = status.WrhsInstID
                     },
                     Type = c.Type,
                     WasteProfileID = c.WasteProfileID,
                     Status = status.StatusCode,
                     CurrentLocation = status.QoskID,

                  };

               if (containers.Any())
               {
                  return ContainerFromProto(!string.IsNullOrWhiteSpace(category)
                        && containers.Where(c => c.WasteProfileID == category).Count() > 0
                           ? containers.Where(c => c.WasteProfileID == category).FirstOrDefault()
                           : containers.First());
               }

               return null;
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /// <summary>
      /// instantiate a specific container type from a protocontainer
      /// </summary>
      /// <param name="container">A protocontainer with set properties</param>
      /// <returns></returns>
      public static ISidebarContainer ContainerFromProto(ProtoContainer container)
      {
         if (container.TypeGroup == null)
         {
            if (Constants.BinTypes.Contains(container.Type))
            {
               container.TypeGroup = Constants.Bin;
            }
            else if (Constants.GaylordTypes.Contains(container.Type))
            {
               container.TypeGroup = Constants.Gaylord;
            }
            else if (Constants.ToteTypes.Contains(container.Type))
            {
               container.TypeGroup = Constants.Tote;
            }
         }
         switch (container.TypeGroup)
         {
            case Constants.Bin:
               return new SidebarBin(container);
            case Constants.Tote:
               return new SidebarTote(container);
            case Constants.Gaylord:
               return new SidebarGaylord(container);
            default:
               return null;
         }
      }

      /// <summary>
      /// Determine if a specific product is actively in a green bin
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      public static List<Item> WasItemInGreenBin(int productId)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from item in cloudEntities.Item
                       join contrItemRel in cloudEntities.WrhsContrItemRel on item.ItemGUID equals contrItemRel.ItemGUID
                       join contr in cloudEntities.WrhsContr on Constants.GreenSortBin equals contr.Type
                       where item.ProductID == productId && contrItemRel.IsActive && contr.WrhsContrID == contrItemRel.WrhsContrID
                       select item).ToList();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      public static List <ItemConditionRel> TempGreenBinConditionFix(List <Guid> guidList)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from ConditionCode in cloudEntities.ItemConditionRel
                       where guidList.Contains(ConditionCode.ItemGUID) &&
                       ConditionCode.ConditionCode == "Irrelevant" &&
                       ConditionCode.AppCode == "MASTER" && 
                       ConditionCode.IsDeleted == false
                       select ConditionCode).ToList();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }


      /// <summary>
      /// Determine if the processed item has made it to a white bin for any reason
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      public static bool WasItemInWhiteBin(Guid? itemGuid)
      {
         if (itemGuid == null)
         {
            return false;
         }

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return (from item in cloudEntities.Item
                  join contrItemRel in cloudEntities.WrhsContrItemRel on item.ItemGUID equals contrItemRel.ItemGUID
                  join contr in cloudEntities.WrhsContr on Constants.WhiteSortBin equals contr.Type
                  where item.ItemGUID == itemGuid && contr.WrhsContrID == contrItemRel.WrhsContrID
                  select item).Any();
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return false;
      }

      /// <summary>
      ///   Returns a list of container relations given an item
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      public static List<ContainerItemRelation> GetContainerItemRel(Guid itemGuid)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (from contrItemRel in cloudEntities.WrhsContrItemRel
                    join contr in cloudEntities.WrhsContr on contrItemRel.WrhsContrID equals contr.WrhsContrID
                    where contrItemRel.ItemGUID == itemGuid
                    select new ContainerItemRelation()
                    {
                       ContainerRel = contrItemRel,
                       Container = contr
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Container type.
      /// </summary>
      /// <param name="containerId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsContrTypeDict GetContainerType(string contrType)
      {
         using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (from type in cloudEntities.WrhsContrTypeDict
                    where type.Code == contrType
                    select type).FirstOrDefault() ?? new WrhsContrTypeDict();
         }
      }

      /// <summary>
      /// Returns relevant RA information for a tote
      /// </summary>
      /// <param name="container"></param>
      /// <returns></returns>
      public static ReturnInformation GetRaForTote(ContainerInstance container)
      {
         //Check if there is a cache for this request
         var key = $"GetGetRaForToteProduct|{container?.ContainerId ?? ""}|{container?.InstanceId ?? ""}";
         Logger.Info($"GetRaForTote: Looking up cache({key}) for key:{key}");
         //var result = MemoryCache.Default[key] as ReturnInformation;
         var result = Common.Redis.GetCacheItem<ReturnInformation>(key) as ReturnInformation;

         if (result != null)
         {
            Logger.Info($"GetRaForTote: Found item in cache for key:{key}");
            return result;
         }

         result = null;

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               Logger.Info($"GetRaForTote: Querying database key:{key}");
               var records = (from trackingRel in cloudEntities.WrhsContrTrackingRel
                              join tracking in cloudEntities.Tracking on trackingRel.TrackingID equals tracking.TrackingID
                              join returnAuthorization in cloudEntities.ReturnAuthorization on tracking.AssocID equals returnAuthorization.RtrnAuthID
                              join debitMemo in cloudEntities.DebitMemo on returnAuthorization.DebitMemoID equals debitMemo.DebitMemoID
                              join profile in cloudEntities.Profile on debitMemo.MfrProfileCode equals profile.ProfileCode
                              where tracking.TrackingTypeCode == "RA"
                                    && trackingRel.WrhsContrID == container.ContainerId
                                    && trackingRel.WrhsInstID == container.InstanceId
                              select new ReturnInformation()
                              {
                                 ReturnAuthorization = returnAuthorization,
                                 Tracking = tracking,
                                 DebitMemo = debitMemo,
                                 Profile = profile
                              });

               if (records.Any())
               {
                  Logger.Info($"GetRaForTote: Database record found key:{key}");
                  result = records.FirstOrDefault();
               }
               else
               {
                  Logger.Info($"GetRaForTote: Database record NOT found for key:{key}");
               }

            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
         }

         //set cache (half an hour by default)
         if (container?.ContainerId != null && container?.InstanceId != null)
         {
            Logger.Info($"GetRaForTote: Setting cache for key:{key}");
            //Common.Methods.SetCache(key, result);
            Common.Redis.SetCacheItem(key, result);
            Logger.Info($"GetRaForTote: Setting cache complete key:{key}");
         }

         return result;
      }

      /// <summary>
      /// Find the RA tied to an item's initial induction
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <returns></returns>
      public static ReturnInformation GetRaForItem(Guid? itemGuid)
      {
         Logger.Info($"GetRaForItem is called");

         if (!itemGuid.HasValue)
         {
            Logger.Info($"GetRaForItem: No Item GUID passed in; returning");
            return null;
         }

         try
         {
            //find the tote this item came from
            var container = GetContainerItemRel(itemGuid.Value).FirstOrDefault(c => Constants.ToteTypes.Contains(c.Container.Type));

            if (container == null)
            {
               Logger.Info($"GetRaForItem: Container not found; returning");
               return null;
            }

            Logger.Info($"GetRaForItem: Calling GetRaForTote");

            //get the RA for that tote
            Logger.Info($"GetRaForItem: Call GetRaForTote for Item:{itemGuid.Value}");
            return GetRaForTote(new ContainerInstance()
            {
               ContainerId = container?.Container.WrhsContrID,
               InstanceId = container?.ContainerRel.WrhsInstID
            });
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            throw;
         }
      }

      public static ContainerInstance GetCurrentInboundTote(string machineName, string appCode = "WAREHS_RETURN")
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var data = (from status in cloudEntities.WrhsContrStatusHist
                        join container in cloudEntities.WrhsContr on status.WrhsContrID equals container.WrhsContrID
                        join qosk in cloudEntities.Qosk on status.QoskID equals qosk.QoskIDx
                        where qosk.MachineID == machineName
                              && status.AppCode == appCode
                              && status.StatusCode == SidebarContainer.STATUS_INPROCESS
                              && status.CreatedDate == (from olderStatus in cloudEntities.WrhsContrStatusHist
                                                        where olderStatus.WrhsContrID == status.WrhsContrID
                                                        select olderStatus.CreatedDate).OrderByDescending(x => x).FirstOrDefault()

                        select new ContainerInstance()
                        {
                           ContainerId = container.WrhsContrID,
                           InstanceId = status.WrhsInstID
                        }).ToList();

            return data.Any() ? data.FirstOrDefault() : null;
         }
      }
   }
   /// <summary>
   ///   Convenience class for accessing the history of an item's container relations with the container details
   /// </summary>
   public class ContainerItemRelation
   {
      public WrhsContrItemRel ContainerRel { get; set; }
      public WrhsContr Container { get; set; }
   }
}