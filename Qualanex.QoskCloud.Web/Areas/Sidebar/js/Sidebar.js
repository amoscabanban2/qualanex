﻿//*****************************************************************************
//*
//* Sidebar.js
//*    Copyright (c) 2017 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Provides the functionality and interface for interacting with warehouse
//*    containers
//*
//* Notes:
//*    The sidebar containers available in an application must be defined and loaded 
//*    using the UPA.
//*
//*    HTML for the containers is sent from the server and in the init method, the hidden
//*    values in each container is instantiated as a javascript object. This is done in 
//*    the sidebar.init() function.
//*    
//*    Functions and the collection of containers can be accessed using the window level
//*    javascript object "sidebar" (sidebar.containers will return an array of all containers
//*    whether open or not to the caller)
//*
//*
//*****************************************************************************
$(function ()
{
   //Arrays holding methods triggered during container events
   window.initMethods = [];
   window.requestMethods = [];
   window.toteOpenMethods = [];
   window.toteCloseMethods = [];
   window.totePauseMethods = [];
   window.binOpenMethods = [];
   window.binCloseMethods = [];
   window.binPauseMethods = [];
   window.beforeCloseMethods = [];
   window.beforeOpenMethods = [];
   window.beforePauseMethods = [];
   window.gaylordOpenMethods = [];
   window.gaylordCloseMethods = [];
   window.gaylordPauseMethods = [];
   window.beforeRequestMethods = [];

   var c0Gaylords = ["LRG_GAYLRD", "SML_GAYLRD"];

   //Processing and ajax call for opening a container
   var callOpen = function (event, control, callback)
   {
      //get a reference to the container's html, whether from a click or javascript function
      var $div = $(this).parents(".QoskSegment:first");
      if (control !== undefined)
      {
         $div = $(control);
      }

      //update the hidden field value to the requested containerId value
      var containerId = $div.find(".containerId.text-box").val();

      if (containerId === undefined || containerId === "")
      {
         return false;
      }

      //disable buttons until the event succeeds or fails
      $div.find("[name=containerOpen]").disable();
      $div.find("[name=containerRequest]").disable();

      //make sure that a container with that id isn't already open on this station
      $div.find(".containerId").val(containerId);
      var type = $div.find("[data-container-field=type]").val();

      if (sidebar.containers.filter(c => c.containerId === containerId && c.type === type).length)
      {
         if (callback !== undefined)
         {
            callback({ success: false, error: "a container with this ID is already open" });
         }
         else
         {
            showError("a container with this ID is already open");
         }

         //re-enable the buttons os user can try again with a different id
         $div.find("[name=containerOpen]").enable();
         $div.find("[name=containerRequest]").enable();
         return false;
      }

      if (beforeOpenMethods.length > 0)
      {
         //execute before open methods
         //if an error string is returned, opening does not continue
         var openStatus = executeMethod("beforeOpenMethods", containerId, sidebar.getContainerByType(type).attribute);

         if (openStatus.length > 0)
         {
            $div.find("[name=containerOpen]").enable();
            $div.find("[name=containerRequest]").enable();
            $(".requiredContainerError").html(openStatus);
            $div.find(".error").html(openStatus);
            $(".requiredContainerId").focus().select();
            return false;
         }
      }

      //send the ajax request
      var options = {
         async: false,
         url: "/sidebar/sidebar/open",
         type: "POST",
         data: $div.find("input").serialize() + getAntiForgeryToken(),
         error: function (err)
         {
            //handle errors connecting to the server
            showError(err.statusText);
            $div.find("[name=containerOpen]").enable();
            $div.find("[name=containerRequest]").enable();
         },
         success: function (data)
         {
            console.log(data);

            if (data.success)
            {
               //on successful open, new html is sent from the server to update the counts
               //and remove any containers that had their status changed elsewhere
               replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               sidebar.init();
               executeMethods($div.find("[data-container-field=typeGroup]").val().toLowerCase() + "OpenMethods", data);

            }
            else
            {
               //handle errors from the server, since final sort has no bin designation we have to check if the request sidebar
               //item is visible to display the message, if not, use a pop up.  It's dirty, but I'd like to see you find more screen space on final sort!
               if ($div.is(':visible'))
               {
                  $div.find(".error").html(data.error);
                  $div.find("[name=containerOpen]").enable();
                  $div.find("[name=containerRequest]").enable();
                  window.isInViewPort($div);
               }
               else if (!$("#EMPTY_IN").is(':visible') && window.location.href.includes("WrhsSort"))
               {
                  var messageObject = [{
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>" + data.error + " </h3>" + "<script>$('.close-button-messageBox').hide()</script>"
                  }];
                  var btnObjects = [{
                     "id": "btn_continue", "name": "Close", "function": "onclick='$(this).closeMessageBox();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
                  }];
                  var titleobjects = [{ "title": "Unable to Open Container" }];
                  $(this).addMessageButton(btnObjects, messageObject);
                  $(this).showMessageBox(titleobjects);
               }
               else
               {
                  $div.find(".error").html(data.error);
                  $div.find("[name=containerOpen]").enable();
                  $div.find("[name=containerRequest]").enable();
               }
            }

            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      }
      catch (e)
      {
         throw e;
      }

      ga('send', 'event', 'Sidebar', 'ContainerOpen');
      return false;
   };

   //When a request is processed, show an alert letting users know that a label has been printed
   var showLabelPrintedAlert = function (containerId)
   {
      var container = sidebar.getInboundContainer();
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " +
         "A label for container " + containerId + " has been created at the printing station" +
         "  </h3>" +
         "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue_container_label", "name": "Continue", "function": "onclick='$(this).closeMessageBox();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "New Label Printed" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //logic and ajax for requesting a new instance of a container
   var callRequest = function (event, control, suppressAlert, callback)
   {
      var $div = $(this).parents(".QoskSegment:first");

      if (control !== undefined)
      {
         $div = $(control);
      }

      $div.find("[name=containerRequest]").disable();

      var container = sidebar.containers.find(c => c.segmentId === $div.find(".panel-collapse").attr("id"));

      if (beforeRequestMethods.length > 0)
      {
         executeMethod("beforeRequestMethods", container);
      }

      //update the html values to match the js object before serializing
      container.control.find("[data-container-field]").each(function ()
      {
         var inputField = $(this).attr("data-container-field");
         container.control.find("[data-container-field=" + inputField + "]").val(container[inputField]);
      });

      var options = {
         async: true,
         url: "/sidebar/sidebar/requestcontainer",
         type: "POST",
         data: $div.find("input").serialize() + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
            $div.find("[name=containerRequest]").enable();
         },
         success: function (data)
         {
            console.log(data);

            if (data.success)
            {
               $div.find("[name=containerRequest]").disable();

               //send a print request to the local service with details from the server response
               var funcClass = data.isRecall ? "recall" : "gaylord";
               $.getJSON("https://localhost:8006/Qualanex/http/Print?funcClass=" + funcClass + "&dataStream=" + data.printString, {}, function (data) { console.log(data); });
               data.container = $div;
               executeMethods("requestMethods", data);

               if (!suppressAlert)
               {
                  showLabelPrintedAlert(data.containerId);
               }
            }
            else
            {
               $div.find(".error").html(data.error);
               $div.find("[name=containerRequest]").enable();
            }

            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };

      try
      {
         $.ajax(options);
      }
      catch (e)
      {
         throw e;
      }

      ga('send', 'event', 'Sidebar', 'ContainerRequest');
      return false;
   }

   //Request button is pressed
   $(document).on("click", "#Sidebar [name=containerRequest]", callRequest);

   //javascript method to request a new container instance
   var request = function (category, suppressAlert, callback)
   {
      if (category !== undefined && category !== null)
      {
         this.control.find(".category").val(category);
      }

      callRequest(null, this.control, suppressAlert, callback);
   }

   //javascript method calling request for a container
   //this is where we assign the fields found in _SidebarCommonFields.cshtml
   var requestGaylord = function (profileCode, wasteProfileId, recallId, ctrlNbr, suppressAlert, callback)
   {
      this.profileCode = profileCode;
      this.category = wasteProfileId;
      this.recallId = recallId;
      this.ctrlNo = ctrlNbr;
      callRequest(null, this.control, suppressAlert, callback);
   }

   //trigger for clicking open button on a container
   $(document).on("click", "#Sidebar [name=containerOpen]", callOpen);

   //trigger for when return is pressed in the container id field of a container
   $(document).on("keypress", "#Sidebar .containerId", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;
      if (key === 13)
      {
         $(this).parents(".panel-collapse").find(".containerOpen").click();
      }
   });

   //javascript open method for a container
   var open = function (containerId, callback)
   {
      if (this.containerId !== "")
      {
         if (callback !== undefined && callback !== null)
         {
            callback({ success: false, error: "current container most be closed before opening another one" });
         }
         return;
      }
      this.control.find(".containerId").val(containerId);
      this.control.find("[data-container-field='category']").val(this.category);
      callOpen(null, this.control, callback);
   }

   //logic and ajax request for closing a container
   var callClose = function (event, control, callback)
   {
      var $div = $(this).parents(".QoskSegment:first");
      $div.find(".error").html("");

      if (control !== undefined)
      {
         $div = $(control);
      }

      if (beforeCloseMethods.length > 0)
      {
         //if methods return false, cancel close
         if (!executeMethod("beforeCloseMethods", $div))
         {
            $div.find(".error").html("Please process all items before closing");
            return false;
         }
      }

      $div.find("[name=containerClose]").disable();

      var options = {
         async: true,
         url: "/sidebar/sidebar/close",
         type: "POST",
         data: $div.find("input").serialize() + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
            $div.find("[name=containerClose]").enable();
         },
         success: function (data)
         {
            console.log(data);

            if (data.success)
            {
               //execute close methods for appropriate container type
               executeMethods($div.find("[data-container-field=typeGroup]").val().toLowerCase() + "CloseMethods", data);
               //replace the sidebar segments and re-init
               replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               sidebar.init();

               //only print out the label if we have data string
               //in the SidebarController, only gaylord get a print string
               if (data.printString !== null && data.printString !== "")
               {
                  var funcClass = data.isRecall ? "recall" : "gaylord";
                  $.getJSON("https://localhost:8006/Qualanex/http/Print?funcClass=" + funcClass + "&dataStream=" + data.printString, {}, function (data)
                  {
                     //data should return the pipe string generated by the print function
                     //if it returns false, then either a printer was not found, or an exception was caught
                     console.log(data);
                  });
               }
            }
            else
            {
               $div.find(".error").html(data.error);
               $div.find("[name=containerClose]").enable();
            }

            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      }
      catch (e)
      {
         throw e;
      }

      ga('send', 'event', 'Sidebar', 'ContainerClose');
      return false;
   };

   //event handler for close button click
   $(document).on("click", "#Sidebar [name=containerClose]", callClose);

   //javascript function for closing a container
   //update status and callback are optional
   var close = function (updateStatus, callback)
   {
      if (this.containerId === "" || this.containerId === undefined || this.containerId === null)
      {
         if (callback !== undefined && callback !== null)
         {
            callback({ success: false, error: "there is no container open" });
            return;
         }
      }

      if (updateStatus !== undefined && updateStatus !== null)
      {
         this.control.find("[data-container-field=status]").val(updateStatus);
      }

      callClose(null, this.control, callback);
   }

   //Ajax call for pausing a container, nearly identical to close
   var callPause = function (event, control, callback)
   {
      if (beforePauseMethods.length > 0)
      {
         var pauseStatus = executeMethod("beforePauseMethods");

         if (!pauseStatus)
         {
            return false;
         }
      }

      var $div = $(this).parents(".QoskSegment:first");

      if (control !== undefined)
      {
         $div = $(control);
      }

      $div.find("[name=containerPause]").disable();

      var options = {
         async: true,
         url: "/sidebar/sidebar/pause",
         type: "POST",
         data: $div.find("input").serialize() + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
            $div.find("[name=containerPause]").enable();
         },
         success: function (data)
         {
            console.log(data);

            if (data.success)
            {
               executeMethods($div.find("[data-container-field=typeGroup]").val().toLowerCase() + "PauseMethods", data);
               replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               sidebar.init();

            }
            else
            {
               $div.find(".error").html(data.error);
               $div.find("[name=containerPause]").enable();
            }

            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      }
      catch (e)
      {
         throw e;
      }

      ga('send', 'event', 'Sidebar', 'ContainerPause');
      return false;
   };
   //button click handler for pausing a container
   $(document).on("click", "#Sidebar [name=containerPause]", callPause);

   //javascript function for pausing a container
   //updatesStatus and callback are optional
   var pause = function (updateStatus, callback)
   {
      if (this.containerId === "" || this.containerId === undefined || this.containerId === null)
      {
         if (callback !== undefined && callback !== null)
         {
            callback({ success: false, error: "there is no container open" });
            return;
         }
      }

      if (updateStatus !== undefined && updateStatus !== null)
      {
         this.control.find("[data-container-field=status]").val(updateStatus);
      }

      callPause(null, this.control, callback);
   }


   //*****************************************************************************
   //*
   //* Summary:
   //*   Adds a containeritemrel record 
   //*
   //* Parameters:
   //*   itemId - the item's guid
   //*   callback - a function to be called after completion
   //*
   //* Returns:
   //*   None
   //*
   //*****************************************************************************
   var put = function (itemId, callback)
   {
      //let the caller know if the call cannot proceed
      if (callback !== undefined && callback !== null)
      {
         if (this.containerId === "" || this.containerId === undefined || this.containerId === null)
         {
            callback({ success: false, error: "there is no container open" });
            return false;
         }

         if (itemId === "" || itemId === undefined || itemId === null)
         {
            callback({ success: false, error: "Item must be specified" });
            return false;
         }
      }

      //final sort does not use policy form; get foreign container status from the selected binventory row in Final Sort
      var foreignContr = $(".searchGrid_WrhsSrtDetails-div-click.selected").find("td[exchange='isForeignContainer']").attr("exchange-value");
      var isForeignContainer =
         $("#PolicyForm_IsForeignContainer").val() === undefined || $("#PolicyForm_IsForeignContainer").val() === ""
            ? foreignContr === undefined || foreignContr === ""
               ? ""
               : "&isForeignContainer=" + foreignContr
            : "&isForeignContainer=" + $("#PolicyForm_IsForeignContainer").val();

      //final sort does not use policy form; get Manufacturer Profile Code from the selected binventory row in Final Sort
      var mfgProfileCode = $(".searchGrid_WrhsSrtDetails-div-click.selected").find("td[exchange='MfgProfileCode']").attr("exchange-value");
      var manufacturerProfileCode =
         $("#PolicyForm_Product_ProfileCode").val() === undefined || $("#PolicyForm_Product_ProfileCode").val() === ""
            ? mfgProfileCode === undefined || mfgProfileCode === ""
               ? ""
               : "&manufacturerProfileCode=" + mfgProfileCode
            : "&manufacturerProfileCode=" + $("#PolicyForm_Product_ProfileCode").val();

      var $div = $(this.control);

      var options = {
         async: true,
         url: "/sidebar/sidebar/put",
         type: "POST",
         data: $div.find("input").serialize() + "&itemId=" + itemId + isForeignContainer + manufacturerProfileCode + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            console.log(data);
            if (data.success)
            {
               replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               sidebar.init();
            } else
            {
               $div.find(".error").html(data.error);
            }
            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      } catch (e)
      {
         throw e;
      }
      ga('send', 'event', 'Sidebar', 'ContainerPut');
      return false;
   };

   var remove = function (itemId, callback)
   {
      if (callback !== undefined && callback !== null)
      {
         if (this.containerId === "" || this.containerId === undefined || this.containerId === null)
         {
            callback({ success: false, error: "there is no container open" });
            return false;
         }
         if (itemId === "" || itemId === undefined || itemId === null)
         {
            callback({ success: false, error: "Item must be specified" });
            return false;
         }
      }
      var $div = $(this.control);
      var containerId = this.containerId;
      var category = this.category;
      var type = this.type;
      var options = {
         async: true,
         url: "/sidebar/sidebar/Remove",
         type: "POST",
         data: $div.find("input").serialize() + "&itemId=" + itemId + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            ;
            if (data.Success)
            {
               //replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               var containers = sidebar.getContainerByType(type, category, containerId);
               containers.count = containers.count - 1;
               $("#Sidebar div#" + type + " input[name='Category'][value='" + category + "']").closest("div").find("input[data-container-field='containerId'][value='" + containerId + "']").closest("div").find("tr.counter td:last").html(containers.count);
               sidebar.init();
            } else
            {
               $div.find(".error").html(data.error);
            }
            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      } catch (e)
      {
         throw e;
      }
      ga('send', 'event', 'Sidebar', 'ContainerRemove');
      return false;
   };

   var getAvailableContainer = function (statuses, callback)
   {
      var $div = $(this.control);

      var options = {
         async: true,
         url: "/sidebar/sidebar/FindAvailableContainer",
         type: "POST",
         data: $div.find("input").serialize() + "&statuses=" + statuses + getAntiForgeryToken(),
         error: function (err)
         {
            console.log(err);
            callback({ ContainerId: null, InstanceId: null });
         },
         success: function (data)
         {
            callback(data);
         }
      };
      try
      {
         $.ajax(options);
      } catch (e)
      {
         throw e;
      }
      ga('send', 'event', 'Sidebar', 'ContainerAvailable');
      return false;
   };


   var getContainerByType = function (type, category, containerId)
   {
      var container = sidebar.containers.find(c => c.type === type && (category === undefined || category === null || category === "" || c.category === category)
         && (containerId === undefined || containerId === null || containerId === "" || c.containerId.toLowerCase() === containerId.toLowerCase()));
      if (container === undefined)
      {
         container = sidebar.containers.find(c => c.type === type && !c.isOpen());
         if (container !== undefined && container !== null)
         {
            container.category = category;

         }
      }
      return container;
   }

   //get container type based on the container ID passed in
   var getContrType = function (containerId)
   {
      console.log("var getContrType = function (" + containerId + ")");
      var request = $.ajax({
         cache: false,
         async: false,
         url: "/sidebar/sidebar/getContainerType",
         type: "POST",
         datatype: "JSON",
         data: "containerId=" + containerId + getAntiForgeryToken()
      });

      return request.responseJSON;
   };


   //Returns a gaylord matching the passed in parameters, null or default value parameters will be ignored
   //when comparing to open gaylord.
   //If a matching gaylord is not found, an empty gaylord will be configured with values matching
   //the passed in parameters and returned
   var getGaylord = function (ctrlNo, profileCode, recallId, wasteProfileId, type, containerId)
   {
      //type coercing comparison should be allowed for ctrlNo, recallId, and profileCode since they should be int, but get converted to strings in the js property
      var container = sidebar.containers.find(
         c => c.typeGroup === "Gaylord"
            && (ctrlNo === undefined || ctrlNo === null || ctrlNo === "" || c.ctrlNo == ctrlNo)
            && (profileCode === undefined || profileCode === null || profileCode === "" || c.profileCode == profileCode)
            && (recallId === undefined || recallId === null || recallId === 0 || recallId === "" || c.recallId == recallId)
            && (wasteProfileId === undefined || wasteProfileId === null || wasteProfileId === "" || c.category === wasteProfileId)
            && (type === undefined || type === null || type === "" || c.type === type)
            && (containerId === undefined || containerId === null || containerId === "" || c.containerId.toLowerCase() === containerId.toLowerCase()));

      if (container === undefined)
      {
         container = sidebar.containers.find(
            c => c.typeGroup === "Gaylord"
               && c.type === type
               && !c.isOpen());

         if (container !== undefined && container !== null)
         {
            container.category = wasteProfileId;
            container.ctrlNo = ctrlNo;
            container.profileCode = profileCode;
            container.recallId = recallId;
         }
      }
      return container;
   }


   //Returns the inbound container
   var getInboundContainer = function ()
   {
      var containers = sidebar.containers.filter(e => e.attribute === "I").sort(function (a, b)
      {
         console.log("a.containerId: '" + (a.containerId) + "'");
         console.log("b.containerId: '" + (b.containerId) + "'");
         var x = a.containerId < b.containerId ? 1 : -1;
         console.log(x);
         return x;
      });

      var index = 0;
      containers.forEach(function ()
      {
         var string = "containers[" + index + "]: '" + containers[index].containerId + "'";
         console.log(string);
         index++;
      });

      return containers[0];
   }

   //Returns true if there are no items remaining in the inbound container
   //This function uses synchronous ajax and should be refactored as async
   //or used sparingly
   var isComplete = function ()
   {
      if (sidebar.containers.find(c => c.required === "true" && c.isOpen()) !== undefined)
      {
         var container = sidebar.getInboundContainer();
         var $div = $(".containerId.text-box[value=" + container.containerId + "]").closest("div.QoskSegment");

         var request = $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: $div.find("input").serialize() + getAntiForgeryToken(),
            async: false,
            url: "/sidebar/sidebar/iscomplete "
         });

         return request.responseJSON;
      }

      return true;
   }

   var isItemIn = function (itemGuid, containerId, type, category, callback)
   {
      var container = sidebar.getContainerByType(type, category, containerId);
      var $div = $("#Sidebar div#" + type + " .QoskSegment:first");

      if (container === undefined)
      {
         container = sidebar.getContainerByType(type);
         container.containerId = containerId;
         container.category = category;
         $div.find("data-container-field[category]").val(category);
         $div.find(".containerId").val(containerId);
      }
      else
      {
         $div = $(".containerId.text-box[value=" + container.containerId + "]").closest("div.QoskSegment");
      }

      if (container === undefined)
      {
         return false;
      }

      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: $div.find("input").serialize() + "&itemGuid=" + itemGuid + getAntiForgeryToken(),
         async: true,
         url: "/sidebar/sidebar/isitemIn",
         success: function (data)
         {
            callback(data);
         },
         error: function(data)
         {
            callback(data);
         }
      });

      return request.responseJSON;
   }

   function requireContainers()
   {

      var requiredContainers = sidebar.containers.filter(c => c.required === "true");
      if (requiredContainers.length === 0 || requiredContainers.find(c => c.containerId !== "") !== undefined)
      {
         if (sidebar.containers.find(c => c.required === "true" && c.isOpen()) !== undefined)
         {
            var cntrId = sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId;
            var $html = $("#Sidebar input.containerId[value='" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId + "']").closest("div#" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).type + "");
            var div = $("#Sidebar input.containerId[value='" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId + "']").closest("div#" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).type + "").closest("div");
            div.remove();
            $html.insertBefore($("#Sidebar div:first"));
            if ($("#Sidebar input.containerId[value='" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId + "']").closest("div#" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).type + "").closest("div").find(".QoskSegment").length > 1)
            {
               var $html2 = $("#Sidebar input.containerId[value='" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId + "']").closest("div.QoskSegment");
               var div2 = $("#Sidebar input.containerId[value='" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).containerId + "']").closest("div.QoskSegment");
               div2.remove();
               $html2.insertBefore($("#Sidebar div#" + sidebar.containers.find(c => c.required === "true" && c.isOpen()).type + " div:first"));
            }
         }
         return;
      }
      var container = sidebar.containers.find(c => c.required === "true" && c.containerId === "");

      var types = [];
      var titles = "";

      requiredContainers.forEach(function (c, i)
      {
         if (i > 0)
         {
            titles += " or ";
         }
         titles += c.title;
         types.push(c.type);
      });

      if (window.requiredContainerMessage === undefined)
      {
         window.requiredContainerMessage = "";
      }
      if ($("#Sidebar div#EMPTY_IN").length > 0)
      {
         $("#Sidebar div#EMPTY_IN").show();
      }
      else
      {
         var emptyInBound = "<div id='EMPTY_IN'><div class='QoskSegment'><table><tbody><tr>";
         emptyInBound += "<td rowspan='2' class='QnexBorderL level0'></td><td class='QnexHeader level0'>";
         emptyInBound += "<span class='containerTitle' style='height: 100%; position: relative; top: 25%'> Open </span>";
         emptyInBound += "<span style='background: #3A80BB; height: 100%' class='pull-right'></span> </td><td rowspan='2' class='tdMarginR level0'>";
         emptyInBound += "<img class='imgEx' alt=''><img class='collapse-arrow' data-target='#EMPTY_IN_BOX' style='' src='/Areas/Product/Images/QoskGraphics/Qosk_Arrow_Up.png' alt=''>";
         emptyInBound += "</td></tr><tr id='EMPTY_IN_BOX' name='' data-segment-complete='false' data-segment-validation='' class='panel-collapse '><td class='QnexContent level0'><div class='panel-body'>";
         emptyInBound += "<input class='containerId' data-container-field='containerId' name='ContainerInstance.ContainerId' type='hidden' autocomplete='off'>";
         emptyInBound += "<table class='tableEx QoskForm QoskKeyboard' style='width: 100%;'><tbody><tr><td><table class='tableEx' style=''><tbody><tr>";
         emptyInBound += "<td><table class='' style='width: 300px;'><tbody><tr><td>ID</td><td><input class=\"scannerRequired requiredContainerId  text-box single-line\" data-type=\"" + types + "\" style=\"height: 22px;\" type=\"text\" autocomplete=\"off\">";
         emptyInBound += "</td></tr><tr><td colspan='2'><table><tbody><tr><td style='padding: 10px 0px 4px 8px; text-align: left;'><input class='buttonEx cmdKey containerClose' name='containerClose' style='' type='button' value='Close' disabled='disabled' autocomplete='off'></td>";
         emptyInBound += "<td style='padding: 10px 0px 4px 8px; text-align: left;'><input class='buttonEx cmdKey containerRequest' name='containerRequest' style='' type='button' value='Request' disabled='disabled' autocomplete='off'></td>";
         emptyInBound += "<td style='padding: 10px 8px 4px 0px; text-align: right;'><input class='buttonEx cmdKey requireContainerOpen'  style='' type='button' value='Open' autocomplete='off'></td>";
         emptyInBound += "</tr></tbody></table></td></tr></tbody></table></td><td>&nbsp;</td></tr></tbody></table></td></tr><tr><td class='tdMessage'><table style='width: 100%;'><tbody><tr><td>";
         emptyInBound += "<span class='tdCaption requiredContainerError'><br></span></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div></div>";
         $(emptyInBound).insertBefore($("#Sidebar div:first"));
      }
      $(".requiredContainerId").focus();
   }

   //adding show() method to container
   var show = function ()
   {
      this.control.show();
   }
   //adding hide() method to container
   var hide = function ()
   {
      this.control.hide();
   }
   //return the open status of container
   var isOpen = function ()
   {
      return this.containerId !== "";
   }

   var requiredOpen = function ()
   {
      var types = $(".requiredContainerId").attr("data-type").split(",");
      var breakLoop = false;
      types.forEach(function (type)
      {
         var container = sidebar.containers.find(c => c.type === type && c.attribute === "I");

         if (container === undefined || breakLoop)
         {
            return;
         }

         container.open($(".requiredContainerId").val(), function (data)
         {
            if (data.success)
            {
               if ($("#EMPTY_IN") !== undefined)
               {
                  $("#EMPTY_IN").hide();
               }

               sidebar.requireContainers();
               return;
            }
            else
            {
               $(".requiredContainerError").html(data.error);
               $("input[type=text].requiredContainerId").select();
               $("input[type=text].requiredContainerId").focus();

               //if we find an error specific to the container (mainly by type) then break out of the forEach()
               if (data.errorCode === 1)
               {
                  breakLoop = true;
               }
            }
         });
      });
   }

   $(document).on("click", ".requireContainerOpen", requiredOpen);
   $(document).on("keypress", ".requiredContainerId", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;
      if (key === 13)
      {
         requiredOpen();
      }
   });

   var putVirtual = function (type, itemId, category, callback)
   {
      if (callback !== undefined && callback !== null)
      {
         if (type === "" || type === undefined || type === null)
         {
            callback({ success: false, error: "Type must be specified" });
            return false;
         }
         if (itemId === "" || itemId === undefined || itemId === null)
         {
            callback({ success: false, error: "Item must be specified" });
            return false;
         }
      }
      var $div = $(this.control);

      var options = {
         async: true,
         url: "/sidebar/sidebar/PutVirtual",
         type: "POST",
         data: "&containerType=" + type + "&itemId=" + itemId + "&category=" + category + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            console.log(data);
            if (data.success)
            {
               replaceSegments($("#Sidebar"), $(data.containers).find("#Sidebar"));
               sidebar.init();
            } else
            {
               $div.find(".error").html(data.error);
            }
            if (callback !== undefined && callback !== null)
            {
               callback(data);
            }
         }
      };
      try
      {
         $.ajax(options);
      } catch (e)
      {
         throw e;
      }
      ga('send', 'event', 'Sidebar', 'VirtualPut');
      return false;
   };


   //called from sidebar.init()
   //builds the javascript objects for each container from the html fields
   var populateSidebar = function ()
   {
      window.sidebar.containers = [];
      if (!$("#Sidebar").length)
      {
         return;
      }
      $("#Sidebar .QoskSegment").each(function (i, control)
      {
         //add functions to the container
         var container = {
            title: $(control).find(".containerTitle").html(),
            segmentId: $(control).find(".panel-collapse").attr("id"),
            control: $(control),
            isOpen: isOpen,
            open: open,
            close: close,
            pause: pause,
            put: put,
            request: request,
            requestGaylord: requestGaylord,
            show: show,
            hide: hide,
            remove: remove,
            getAvailableContainer: getAvailableContainer
         };
         //Add server defined properties (in _SidebarCommonFields.cshtml)
         $(control).find("[data-container-field]").each(function (i2, field)
         {
            container[$(field).attr("data-container-field")] = $(field).val();
         });
         //add the container to the containers array
         window.sidebar.containers.push(container);
      });

      executeMethods("initMethods");
      sidebar.requireContainers();
   }


   //Convenience methods to add functions for sidebar events
   var onInit = function (func)
   {
      exclusivePush(initMethods, func);
   }
   var onRequest = function (func)
   {
      exclusivePush(requestMethods, func);
   }
   var onToteOpen = function (func)
   {
      exclusivePush(toteOpenMethods, func);
   }
   var onToteClose = function (func)
   {
      exclusivePush(toteCloseMethods, func);
   }
   var onTotePause = function (func)
   {
      exclusivePush(totePauseMethods, func);
   }
   var onBinOpen = function (func)
   {
      exclusivePush(binOpenMethods, func);
   }
   var onBinClose = function (func)
   {
      exclusivePush(binCloseMethods, func);
   }
   var onBinPause = function (func)
   {
      exclusivePush(binPauseMethods, func);
   }
   var beforeClose = function (func)
   {
      exclusivePush(beforeCloseMethods, func);
   }
   var beforeOpen = function (func)
   {
      exclusivePush(beforeOpenMethods, func);
   }
   var beforePause = function (func)
   {
      exclusivePush(beforePauseMethods, func);
   }
   var onGaylordOpen = function (func)
   {
      exclusivePush(gaylordOpenMethods, func);
   }
   var onGaylordClose = function (func)
   {
      exclusivePush(gaylordCloseMethods, func);
   }
   var onGaylordPause = function (func)
   {
      exclusivePush(gaylordPauseMethods, func);
   }
   var beforeRequest = function (func)
   {
      exclusivePush(beforeRequestMethods, func);
   }
   //the sidebar object is initialized on page load with the sidebar level 
   //functions and arrays defined above
   window.sidebar =
      {
         putVirtual: putVirtual,
         onToteOpen: onToteOpen,
         onToteClose: onToteClose,
         onTotePause: onTotePause,
         onBinOpen: onBinOpen,
         onBinClose: onBinClose,
         onBinPause: onBinPause,
         onGaylordOpen: onGaylordOpen,
         onGaylordClose: onGaylordClose,
         onGaylordPause: onGaylordPause,
         onRequest: onRequest,
         onInit: onInit,
         beforeClose: beforeClose,
         beforeOpen: beforeOpen,
         beforePause: beforePause,
         init: populateSidebar,
         requireContainers: requireContainers,
         containers: [],
         getContainerByType: getContainerByType,
         getInboundContainer: getInboundContainer,
         getContrType: getContrType,
         getGaylord: getGaylord,
         isComplete: isComplete,
         isItemIn: isItemIn,
         beforeRequest: beforeRequest
      };
   $(document).on("ready",
      function ()
      {
         sidebar.init();
      });
});
