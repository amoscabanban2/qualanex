﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="GreenBinController.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GreenBin.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.Qosk.Dictionary;
   using Qualanex.Qosk.Library.Barcode;
   using Qualanex.Qosk.Library.Model;
   using Qualanex.Qosk.Library.LogTraceManager;
   using System.Web;
   using Entity;
   using Product;
   using Sidebar;
   using Product.Models;
   using Utility;
   using Model;
   using Newtonsoft.Json;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_GRNBIN")]
   public class GreenBinController : Controller, IQoskController
   {
      private Qosk.Library.Model.UPAModel.UPAApplication _upaApplication;
      ///****************************************************************************
      /// <summary>
      ///   Green Bin View.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GreenBinView()
      {
         DeleteFiles(this.HttpContext.User.Identity.Name + "_GreenBin");

         this.ModelState.Clear();
         var objReturnLoginUserEntity =
             (ReturnLoginUserEntity)
             QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         int userProfileCode = -99;

         if (objReturnLoginUserEntity?.UserReturnData.ProfileCode != null)
         {
            userProfileCode = objReturnLoginUserEntity.UserReturnData.ProfileCode.Value;
         }

         this._upaApplication = UpaDataAccess.GetUpaModel("WAREHS_GRNBIN",
                                                           objReturnLoginUserEntity.UserReturnData.UserID,
                                                            userProfileCode);

         var pageViewId = Guid.NewGuid();
         QoskSessionHelper.SetSessionData(Constants.UpaApplicationKey + pageViewId, this._upaApplication);

         SearchRequest request = new SearchRequest { Product = new ProductDetail() };

         var viewModel = new GreenBinViewModel
         {
            ActionMenu = GreenBinModel.GetActionMenu(),
            lstImages = new List<Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels.ImageResult>(),
            UPAControl = new Web.ViewModels.UpaViewModel
            {
               WasteResult = new WasteResult(),
               UpaApplication = this._upaApplication,
               SearchResults = new SearchResult()
               {
                  Message = "Perform a search to see results",
                  Products = new List<ProductResult>()

               },
               Binventory = new Web.ViewModels.BinventoryViewModel
               {
                  Options = new Web.ViewModels.BinventoryOptions
                  {
                     AllItemsCount = 0,
                     RowProperty = new BinventoryDataAccess()
                  },
                  Rows = new List<BinventoryDataAccess>()
               }
            }
         };

         this.ViewBag.pageViewId = pageViewId;

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete files for current user.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public void DeleteFiles(string StartWith)
      {
         var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\GreenBin", Server.MapPath(@"\")));
         string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\CNT");
         bool isExists = System.IO.Directory.Exists(pathString);

         if (!isExists)
         {
            System.IO.Directory.CreateDirectory(pathString);
         }

         string[] fileList = System.IO.Directory.GetFiles(pathString);
         pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\CTR");
         isExists = System.IO.Directory.Exists(pathString);

         if (!isExists)
         {
            System.IO.Directory.CreateDirectory(pathString);
         }

         string[] fileList1 = System.IO.Directory.GetFiles(pathString);

         foreach (var fileName in fileList)
         {
            if (fileName.Contains(StartWith))
            {
               System.IO.File.Delete(fileName);
            }
         }
         foreach (var fileName in fileList1)
         {
            if (fileName.Contains(StartWith))
            {
               System.IO.File.Delete(fileName);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Green Bin Processing
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveGreenBinProcessing(string wasteCode, string wasteStream, string itemEntities, string cancellation)
      {
         var wasteCodeModel = System.Web.Helpers.Json.Decode<List<GreenBinModel>>(wasteCode);
         var wasteStreamModel = System.Web.Helpers.Json.Decode<List<GreenBinModel>>(wasteStream);
         var entities = System.Web.Helpers.Json.Decode<GreenBinModel>(itemEntities);
         var blnCheck = GreenBinModel.SaveItems(wasteCodeModel, wasteStreamModel, entities, this.User.Identity.Name, cancellation);
         return Json(new
         {
            GreenBinReason = blnCheck.ToString()
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Details of bin.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetBinventoryDetails(string binId, string stationId)
      {
         this.ModelState.Clear();
         stationId = string.IsNullOrWhiteSpace(stationId) ? SidebarCommon.GetMachineName(this.Request) : stationId;

         var viewModel = string.IsNullOrWhiteSpace(binId)
            ? new GreenBinViewModel()
            : new GreenBinViewModel
            {
               ActionMenu = GreenBinModel.GetActionMenu(),
               //GreenBinDetails = GreenBinModel.GetItemList(binId),
               UPAControl = new Web.ViewModels.UpaViewModel
               {
                  Binventory = new Web.ViewModels.BinventoryViewModel
                  {
                     Options = new Web.ViewModels.BinventoryOptions
                     {
                        AllItemsCount = 0,
                        RowProperty = new BinventoryDataAccess()
                     },
                     Rows = BinventoryDataAccess.GetBinventoryList(binId, stationId, ProductCommon.GetUserState(), "WAREHS_GRNBIN")
                     //GreenBinModel.GetItemList(binId)
                  }
               }
            };

         return this.PartialView("_GreenBinSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Clicked Items images.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetItemImages(int productId, Guid itemGUID)
      {
         this.ModelState.Clear();
         return this.PartialView("_ImageCarousel", GreenBinModel.GetItemImages(productId));
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Bin Processing Information.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetBinProcessingInfo(Guid itemGuid, int productId)
      {
         this.ModelState.Clear();
         var notificationIds = new List<long>();
         DeleteFiles(this.HttpContext.User.Identity.Name + "_GreenBin");
         GreenBinModel selectedItem = null;
         int innerOuterProductId = 0;

         if (!string.IsNullOrWhiteSpace(itemGuid.ToString()))
         {
            selectedItem = GreenBinModel.GetBinProcessingInfo(itemGuid, productId);
            innerOuterProductId = selectedItem.NDCInnerPack ? (int)selectedItem.RollupProductId : productId;
         }

         var viewModel = string.IsNullOrWhiteSpace(itemGuid.ToString())
            ? new GreenBinViewModel()
            : new GreenBinViewModel
            {
               SelectedGreenBinModel = selectedItem,
               lstImages = GreenBinModel.GetItemImages(productId),
               LstWasteInformation = new GreenBinRelationControl
               {
                  WasteStream = new Model.RelationControl
                  {
                     Options = new Model.RelationControlOptions
                     {
                        AddMethod = this.Url.Action(nameof(GreenBinModel)),
                        ShowRowControls = false,
                        HiddenProperties =
                        {
                           nameof(GreenBinModel.ContainerImagesCount),
                           nameof(GreenBinModel.ContentImagesCount),
                           nameof(GreenBinModel.DebitMemo),
                           nameof(GreenBinModel.Destination),
                           nameof(GreenBinModel.DosageCode),
                           nameof(GreenBinModel.DosageImageCode),
                           nameof(GreenBinModel.ExpDate),
                           nameof(GreenBinModel.ImageSourceCode),
                           nameof(GreenBinModel.IndvWeight),
                           nameof(GreenBinModel.ItemContainerWeight),
                           nameof(GreenBinModel.ItemContentsWeight),
                           nameof(GreenBinModel.ItemQty),
                           nameof(GreenBinModel.IsDisposed),
                           nameof(GreenBinModel.ItemCount),
                           nameof(GreenBinModel.ItemGUID),
                           nameof(GreenBinModel.LotNumber),
                           nameof(GreenBinModel.PackageSize),
                           nameof(GreenBinModel.ProductID),
                           nameof(GreenBinModel.RollupProductId),
                           nameof(GreenBinModel.NDCInnerPack),
                           nameof(GreenBinModel.ProductName),
                           nameof(GreenBinModel.ProfileCode),
                           nameof(GreenBinModel.RAorWPT),
                           nameof(GreenBinModel.SeqNo),
                           nameof(GreenBinModel.Status),
                           nameof(GreenBinModel.StatusDate),
                           nameof(GreenBinModel.TrackingID),
                           nameof(GreenBinModel.UnitOfMeasure),
                           nameof(GreenBinModel.WrhsContrID),
                           nameof(GreenBinModel.WrhsContrInst),
                           nameof(GreenBinModel.WrhsContrType),
                           nameof(GreenBinModel.WasteCode),
                           nameof(GreenBinModel.WasteStream),
                           nameof(GreenBinModel.ExpMonth),
                           nameof(GreenBinModel.ExpYear),
                           nameof(GreenBinModel.ImageTypeCode),
                           nameof(GreenBinModel.InductionDate),
                           nameof(GreenBinModel.StationId),
                           nameof(GreenBinModel.TimeProcessing),
                           nameof(GreenBinModel.Inductor),
                           nameof(GreenBinModel.ContainerTypeName),
                           nameof(GreenBinModel.DestinationType),
                           nameof(GreenBinModel.FullContainerWeight),
                           nameof(GreenBinModel.EmptyContainerWeight),
                           nameof(GreenBinModel.Category),
                           nameof(GreenBinModel.DestinationCode),
                           nameof(GreenBinModel.Sorted),
                           nameof(GreenBinModel.OutboundContainerID),
                           nameof(GreenBinModel.CtrlNumber),
                           nameof(GreenBinModel.MFG),
                           nameof(GreenBinModel.Brand),
                           nameof(GreenBinModel.Generic),
                           nameof(GreenBinModel.Dosage),
                           nameof(GreenBinModel.Unit),
                           nameof(GreenBinModel.Rx),
                           nameof(GreenBinModel.NDC),
                           nameof(GreenBinModel.LotFlag),
                           nameof(GreenBinModel.LotChanged),
                           nameof(GreenBinModel.ItemStateCode),
                           nameof(GreenBinModel.isForeignContainer),
                           nameof(GreenBinModel.Quarantine),
                           nameof(GreenBinModel.WasteSegmentChanged)
                        },
                        UpdateMethod = "console.log('not yet implemented');",
                        InitialReadOnly = true,
                        RowProperty = new GreenBinModel(),
                        CustomControls = new List<Model.CustomControl>
                        {
                           new Model.CustomControl
                           {
                              EditView="_WasteStreamCustomColumn",
                              AddView="_WasteStreamCustomColumn",
                              ReadOnlyView=""
                           }
                        },
                        SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        {
                           nameof(GreenBinModel.WasteStreamCode),
                        GreenBinModel.GetWasteStreamList(innerOuterProductId)
                        }
                     }
                     },
                     Rows = GreenBinModel.GetWasteStreams(innerOuterProductId)
                  }
              ,
                  WasteCode = new Model.RelationControl
                  {
                     Options = new Model.RelationControlOptions
                     {
                        AddMethod = this.Url.Action(nameof(GreenBinModel)),
                        ShowRowControls = false,
                        HiddenProperties =
                     {
                        nameof(GreenBinModel.ContainerImagesCount),
                        nameof(GreenBinModel.ContentImagesCount),
                        nameof(GreenBinModel.DebitMemo),
                        nameof(GreenBinModel.Destination),
                        nameof(GreenBinModel.DosageCode),
                        nameof(GreenBinModel.DosageImageCode),
                        nameof(GreenBinModel.ExpDate),
                        nameof(GreenBinModel.ImageSourceCode),
                        nameof(GreenBinModel.IndvWeight),
                        nameof(GreenBinModel.ItemContainerWeight),
                        nameof(GreenBinModel.ItemContentsWeight),
                        nameof(GreenBinModel.ItemQty),
                        nameof(GreenBinModel.IsDisposed),
                        nameof(GreenBinModel.ItemCount),
                        nameof(GreenBinModel.ItemGUID),
                        nameof(GreenBinModel.LotNumber),
                        nameof(GreenBinModel.PackageSize),
                        nameof(GreenBinModel.ProductID),
                        nameof(GreenBinModel.RollupProductId),
                        nameof(GreenBinModel.NDCInnerPack),
                        nameof(GreenBinModel.ProductName),
                        nameof(GreenBinModel.ProfileCode),
                        nameof(GreenBinModel.RAorWPT),
                        nameof(GreenBinModel.SeqNo),
                        nameof(GreenBinModel.Status),
                        nameof(GreenBinModel.StatusDate),
                        nameof(GreenBinModel.TrackingID),
                        nameof(GreenBinModel.UnitOfMeasure),
                        nameof(GreenBinModel.WasteStreamCode),
                        nameof(GreenBinModel.WrhsContrID),
                        nameof(GreenBinModel.WrhsContrInst),
                        nameof(GreenBinModel.WrhsContrType),
                        nameof(GreenBinModel.WasteStream),
                        nameof(GreenBinModel.ExpMonth),
                        nameof(GreenBinModel.ExpYear),
                        nameof(GreenBinModel.ImageTypeCode),
                        nameof(GreenBinModel.InductionDate),
                        nameof(GreenBinModel.StationId),
                        nameof(GreenBinModel.TimeProcessing),
                        nameof(GreenBinModel.Inductor),
                        nameof(GreenBinModel.ContainerTypeName),
                        nameof(GreenBinModel.DestinationType),
                        nameof(GreenBinModel.FullContainerWeight),
                        nameof(GreenBinModel.EmptyContainerWeight),
                        nameof(GreenBinModel.Category),
                        nameof(GreenBinModel.DestinationCode),
                        nameof(GreenBinModel.Sorted),
                        nameof(GreenBinModel.OutboundContainerID),
                        nameof(GreenBinModel.CtrlNumber),
                        nameof(GreenBinModel.MFG),
                        nameof(GreenBinModel.Brand),
                        nameof(GreenBinModel.Generic),
                        nameof(GreenBinModel.Dosage),
                        nameof(GreenBinModel.Unit),
                        nameof(GreenBinModel.Rx),
                        nameof(GreenBinModel.NDC),
                        nameof(GreenBinModel.LotFlag),
                        nameof(GreenBinModel.LotChanged),
                        nameof(GreenBinModel.ItemStateCode),
                        nameof(GreenBinModel.isForeignContainer),
                        nameof(GreenBinModel.Quarantine),
                        nameof(GreenBinModel.WasteSegmentChanged)
                     },
                        UpdateMethod = "console.log('not yet implemented');",
                        InitialReadOnly = true,
                        RowProperty = new GreenBinModel(),
                        CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView="",
                           AddView="",
                           ReadOnlyView=""
                        }
                     },
                        //SelectLists = new Dictionary<string, IEnumerable<SelectListItem>> //commented out because of the changes below (see WasteCode SelectLists inquiry)
                        //{
                        //   {
                        //      nameof(GreenBinModel.WasteCode), GreenBinModel.GetWasteCodeList(innerOuterProductId)
                        //   }
                        //}
                     },
                     Rows = GreenBinModel.GetWasteCodes(innerOuterProductId)
                  }
               },
               // LstWasteStreamCodeRel = GreenBinModel.GetWasteStreamCodeRel()
            };

         //if we're loading a product that already has a waste stream, then use that waste stream to load its corresponding waste codes
         //otherwise, skip the waste code entry into the dictionary
         //WasteCode SelectLists inquiry
         var wasteStreamVal = viewModel.LstWasteInformation.WasteStream.Options.RowProperty.GetType().GetProperties().First(p => p.Name == "WasteStreamCode");
         var wasteStreamCode = viewModel.LstWasteInformation.WasteStream.Rows.Any() ? wasteStreamVal.GetValue(viewModel.LstWasteInformation.WasteStream.Rows.FirstOrDefault()) : null;
         var wasteCodeList = !string.IsNullOrEmpty((string)wasteStreamCode) ? GreenBinModel.GetWasteCodesFromWasteStream((string)wasteStreamCode, innerOuterProductId) : null;

         if (wasteCodeList != null)
         {
            viewModel.LstWasteInformation.WasteCode.Options.SelectLists.Add(nameof(GreenBinModel.WasteCode), wasteCodeList);
         }

         viewModel.UPAControl = new Web.ViewModels.UpaViewModel
         {
            PolicyForm = new Product.Models.PolicyForm
            {
               ProductId = viewModel.SelectedGreenBinModel.ProductID,
               RollupProductId = viewModel.SelectedGreenBinModel.RollupProductId,
               YearOptions = viewModel.YearOptions,
               SelectedExpYear = viewModel.SelectedGreenBinModel.ExpYear,
               SelectedExpMonth = viewModel.SelectedGreenBinModel.ExpMonth,
               ReadyForDisposition = false,
               MonthOptions = viewModel.MonthOptions,
               SelectedLot = new LotResults { LotNumber = viewModel.SelectedGreenBinModel.LotNumber },
               ContainerType = viewModel.SelectedGreenBinModel.ContainerTypeName,
               ProductWeight = viewModel.SelectedGreenBinModel.ItemContainerWeight,
               ContentWeight = viewModel.SelectedGreenBinModel.ItemContentsWeight,
               Quantity = viewModel.SelectedGreenBinModel.ItemQty,
               ItemGuid = viewModel.SelectedGreenBinModel.ItemGUID,
               IsRecallPossible = ProductCommon.IsAnyProductRecall(viewModel.SelectedGreenBinModel.ProductID),
               Product = ProductDetail.GetGreenBinProcessingData(viewModel.SelectedGreenBinModel.ProductID),
               IsForeignContainer = viewModel.SelectedGreenBinModel.isForeignContainer == "true"
            }
         };
         viewModel.UPAControl.PolicyForm.GreenBinReason = GreenBinModel.GetGreenBinReason(itemGuid.ToString());
         viewModel.lstSegments =
              new List<GreenBinSegmentControl>() {
                 new GreenBinSegmentControl()
                 {
                     NextSegment= "Container",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_ReturnPolicy_50Opacity.png",
           Partial= "_QuarantineSegment",
           Required=(viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.Quarantine)
            || viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.Quarantine))?"Required":"",
           SegmentType= "Quarantine",
           Title= "Quarantine"
                 },
                 new GreenBinSegmentControl()
                 {
                     NextSegment= "Weight",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_ReturnPolicy_50Opacity.png",
           Partial= "_ContainerImage",
           Required=(viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.ContainerImage)
            || viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.ContentImage))?"Required":"",
           SegmentType= "Container",
           Title= "Image Capture"
                 },
                 new GreenBinSegmentControl()
                 {
                       NextSegment= "WasteStreamSegment",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_AdditionalInformation_50Opacity.png",
           Partial= "_IndividualCountWeight",
           Required=viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.Weight)?"Required" : "",
           SegmentType= "Weight",
           Title= "Individual Count Weight"
                 },
                  new GreenBinSegmentControl()
                 {
                       NextSegment= "lotDate",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_ReturnPolicy_50Opacity.png",
           Partial= "_WasteStream",
           Required=viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.WasteStream )? "Required":"",
           SegmentType= "WasteStreamSegment",
           Title= "Waste Stream"
                 },
                   new GreenBinSegmentControl()
                 {
                       NextSegment= "",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_ReturnPolicy_50Opacity.png",
           Partial= "_LotDateSegment",
           Required=viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.LotExpiration)? "Required":"",
           SegmentType= "lotDate",
           Title= "Lot and Expiration"
                 },
                    new GreenBinSegmentControl()
                 {
                       NextSegment= "",
           Images= "/Areas/Product/Images/Segments/Qosk_Segment_ReturnPolicy_50Opacity.png",
           Partial= "_BinResult",
           Required="Required",
           SegmentType= "binDesignation",
           Title= "Bin Designation"
                 }
              };

         if (!viewModel.UPAControl.PolicyForm.GreenBinReason.HasFlag(GreenBinReason.Quarantine))
         {
            viewModel.lstSegments.RemoveAll(c => c.Title == "Quarantine");
         }

         PopulatePolicyDropDownLists(viewModel);
         return this.PartialView("_GreenBinProcessing", viewModel);
      }

      public static Product.Models.GreenBinReason GetGreenBinReason(Product.Models.PolicyForm policyForm)
      {
         var greenBinItems = SidebarCommon.WasItemInGreenBin(policyForm.ProductId);
         if (policyForm.ContainerType == "Original|Sealed"
            && !greenBinItems.Any(i => i.ConditionCode == "Org" && i.ItemStateCode == "S"))
         {
            var product = UpaDataAccess.GetSearch(
               new SearchRequest() { Product = new ProductDetail() { ProductId = policyForm.ProductId } }).Products.First();
            if ((product.ProductDetail.IndividualCountWeight == null && product.ProductDetail.UnitsPerPackage == 1)
               || (product.ProductDetail.DosageImageCode != "P" && product.ProductDetail.ContainerWeight == null)
               || product.ProductDetail.FullContainerWeight == null)
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.Weight;
            }

            if (!product.ImprintGroups.Any(r => r.ImageResults.Any(i => i.ImageType == "CTR")))
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.ContainerImage;
            }

            if (!product.ImprintGroups.Any(r => r.ImageResults.Any(i => i.ImageType == "CNT")))
            {
               policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.ContentImage;
            }
         }
         var lot = UpaDataAccess.GetLots(policyForm.ProductId, policyForm.SelectedLot.LotNumber).FirstOrDefault();
         if ((lot == null && !string.IsNullOrWhiteSpace(policyForm.SelectedLot.LotNumber)
             || (lot != null && (!string.IsNullOrWhiteSpace(policyForm.SelectedLot.LotNumber) &&
                                 policyForm.SelectedLot.ExpirationDate.HasValue
                                 && (policyForm.SelectedLot.ExpirationDate.Value.Year !=
                                     (lot.ExpirationDate?.Year ??
                                      policyForm.SelectedLot.ExpirationDate.Value.Year)
                                     || policyForm.SelectedLot.ExpirationDate.Value.Month !=
                                     (lot.ExpirationDate?.Month ??
                                      policyForm.SelectedLot.ExpirationDate.Value.Month)))))
            && !greenBinItems.Any(i => i.LotNumber == policyForm.SelectedLot.LotNumber &&
                                       i.ExpirationDate == policyForm.SelectedLot.ExpirationDate))
         {
            policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.LotExpiration;
         }
         var wasteStreams = UpaDataAccess.GetWasteStreams(policyForm.ProductId, ProductCommon.GetUserState());
         if (!wasteStreams.Any())
         {
            policyForm.GreenBinReason = policyForm.GreenBinReason | GreenBinReason.WasteStream;
         }
         return policyForm.GreenBinReason;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="model"></param>
      ///****************************************************************************
      private static void PopulatePolicyDropDownLists(GreenBinViewModel model)
      {
         model.YearOptions = new List<SelectListItem>() { new SelectListItem { Text = string.Empty, Value = string.Empty } };
         var year = DateTime.UtcNow.Year;

         for (int i = year - 50; i < year + 50; i++)
         {
            model.YearOptions.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
         }

         model.YearOptions.Add(new SelectListItem { Text = "----NA----", Value = " " });

         model.MonthOptions = new List<SelectListItem>()
         {
            new SelectListItem {Text = "-------- Month --------", Value = string.Empty},
            new SelectListItem {Text = "1 - January", Value = "1"},
            new SelectListItem {Text = "2 - February", Value = "2"},
            new SelectListItem {Text = "3 - March", Value = "3"},
            new SelectListItem {Text = "4 - April", Value = "4"},
            new SelectListItem {Text = "5 - May", Value = "5"},
            new SelectListItem {Text = "6 - June", Value = "6"},
            new SelectListItem {Text = "7 - July", Value = "7"},
            new SelectListItem {Text = "8 - August", Value = "8"},
            new SelectListItem {Text = "9 - September", Value = "9"},
            new SelectListItem {Text = "10 - October", Value = "10"},
            new SelectListItem {Text = "11 - November", Value = "11"},
            new SelectListItem {Text = "12 - December", Value = "12"}
         };
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the list of associated waste codes based on the waste stream selected
      /// </summary>
      /// <param name="wasteStreamCode"></param>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetWasteCodesFromWasteStream(string wasteStreamCode, int productId)
      {
         var viewModel = new GreenBinViewModel
         {
            LstWasteInformation = new GreenBinRelationControl
            {
               WasteCode = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     AddMethod = this.Url.Action(nameof(GreenBinModel)),
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(GreenBinModel.ContainerImagesCount),
                        nameof(GreenBinModel.ContentImagesCount),
                        nameof(GreenBinModel.DebitMemo),
                        nameof(GreenBinModel.Destination),
                        nameof(GreenBinModel.DosageCode),
                        nameof(GreenBinModel.DosageImageCode),
                        nameof(GreenBinModel.ExpDate),
                        nameof(GreenBinModel.ImageSourceCode),
                        nameof(GreenBinModel.IndvWeight),
                        nameof(GreenBinModel.ItemContainerWeight),
                        nameof(GreenBinModel.ItemContentsWeight),
                        nameof(GreenBinModel.ItemQty),
                        nameof(GreenBinModel.IsDisposed),
                        nameof(GreenBinModel.ItemCount),
                        nameof(GreenBinModel.ItemGUID),
                        nameof(GreenBinModel.LotNumber),
                        nameof(GreenBinModel.PackageSize),
                        nameof(GreenBinModel.ProductID),
                        nameof(GreenBinModel.RollupProductId),
                        nameof(GreenBinModel.NDCInnerPack),
                        nameof(GreenBinModel.ProductName),
                        nameof(GreenBinModel.ProfileCode),
                        nameof(GreenBinModel.RAorWPT),
                        nameof(GreenBinModel.SeqNo),
                        nameof(GreenBinModel.Status),
                        nameof(GreenBinModel.StatusDate),
                        nameof(GreenBinModel.TrackingID),
                        nameof(GreenBinModel.UnitOfMeasure),
                        nameof(GreenBinModel.WasteStreamCode),
                        nameof(GreenBinModel.WrhsContrID),
                        nameof(GreenBinModel.WrhsContrInst),
                        nameof(GreenBinModel.WrhsContrType),
                        nameof(GreenBinModel.WasteStream),
                        nameof(GreenBinModel.ExpMonth),
                        nameof(GreenBinModel.ExpYear),
                        nameof(GreenBinModel.ImageTypeCode),
                        nameof(GreenBinModel.InductionDate),
                        nameof(GreenBinModel.StationId),
                        nameof(GreenBinModel.TimeProcessing),
                        nameof(GreenBinModel.Inductor),
                        nameof(GreenBinModel.ContainerTypeName),
                        nameof(GreenBinModel.DestinationType),
                        nameof(GreenBinModel.FullContainerWeight),
                        nameof(GreenBinModel.EmptyContainerWeight),
                        nameof(GreenBinModel.Category),
                        nameof(GreenBinModel.DestinationCode),
                        nameof(GreenBinModel.Sorted),
                        nameof(GreenBinModel.OutboundContainerID),
                        nameof(GreenBinModel.CtrlNumber),
                        nameof(GreenBinModel.MFG),
                        nameof(GreenBinModel.Brand),
                        nameof(GreenBinModel.Generic),
                        nameof(GreenBinModel.Dosage),
                        nameof(GreenBinModel.Unit),
                        nameof(GreenBinModel.Rx),
                        nameof(GreenBinModel.NDC),
                        nameof(GreenBinModel.LotFlag),
                        nameof(GreenBinModel.LotChanged),
                        nameof(GreenBinModel.ItemStateCode),
                        nameof(GreenBinModel.isForeignContainer),
                        nameof(GreenBinModel.Quarantine),
                        nameof(GreenBinModel.WasteSegmentChanged)
                     },
                     UpdateMethod = "console.log('not yet implemented');",
                     InitialReadOnly = true,
                     RowProperty = new GreenBinModel(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView="",
                           AddView="",
                           ReadOnlyView=""
                        }
                     },
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        {
                           nameof(GreenBinModel.WasteCode), GreenBinModel.GetWasteCodesFromWasteStream(wasteStreamCode, productId)
                        }
                     }
                  },
                  Rows = GreenBinModel.GetWasteCodes(productId)
               }
            }
         };

         return this.PartialView("_RelationControl", viewModel.LstWasteInformation.WasteCode);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save files into local storage by drag and drop.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult SaveUploadedFile()
      {
         bool isSavedSuccessfully = true;
         string fName = "";
         string flName = "";
         try
         {
            foreach (string fileName in Request.Files)
            {
               HttpPostedFileBase file = Request.Files[fileName];
               //Save file content goes here
               fName = Guid.NewGuid().ToString(); //file.FileName;
               if (file != null && file.ContentLength > 0)
               {
                  flName = file.FileName;
                  var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\GreenBin", Server.MapPath(@"\")));
                  string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\CTR");
                  var fileName1 = this.HttpContext.User.Identity.Name + "_GreenBin_" + System.IO.Path.GetFileName(file.FileName);
                  fName = fileName1;
                  bool isExists = System.IO.Directory.Exists(pathString);
                  if (!isExists)
                     System.IO.Directory.CreateDirectory(pathString);
                  var path = string.Format("{0}\\{1}", pathString, fileName1);
                  file.SaveAs(path);
                  fName = "\\Areas\\GreenBin\\Attached\\CTR\\" + fileName1;
               }
            }
         }
         catch (Exception ex)
         {
            isSavedSuccessfully = false;
         }

         if (isSavedSuccessfully)
            return Json(new { Message = flName, path = fName });
         else
            return Json(new { Message = "Error in saving file", path = "" });
      }

      ///****************************************************************************
      /// <summary>
      ///   Save files into local storage by drag and drop.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult SaveContentUploadedFile()
      {
         bool isSavedSuccessfully = true;
         string fName = "";
         string flName = "";
         try
         {
            foreach (string fileName in Request.Files)
            {
               HttpPostedFileBase file = Request.Files[fileName];
               //Save file content goes here
               fName = Guid.NewGuid().ToString(); //file.FileName;
               if (file != null && file.ContentLength > 0)
               {
                  flName = file.FileName;
                  var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\GreenBin", Server.MapPath(@"\")));
                  string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\CNT");
                  var fileName1 = this.HttpContext.User.Identity.Name + "_GreenBin_" + System.IO.Path.GetFileName(file.FileName);
                  fName = fileName1;
                  bool isExists = System.IO.Directory.Exists(pathString);
                  if (!isExists)
                     System.IO.Directory.CreateDirectory(pathString);
                  var path = string.Format("{0}\\{1}", pathString, fileName1);
                  file.SaveAs(path);
                  fName = "\\Areas\\GreenBin\\Attached\\CNT\\" + fileName1;
               }
            }
         }
         catch (Exception ex)
         {
            isSavedSuccessfully = false;
         }

         if (isSavedSuccessfully)
            return Json(new { Message = flName, path = fName });
         else
            return Json(new { Message = "Error in saving file", path = "" });
      }

      ///****************************************************************************
      /// <summary>
      /// Mostly the same as SaveUploadedFile, but with some extra functionality.  Uploads a new file to server
      /// user name is contained in file which is important in other parts of the code
      /// SaveUploadedFile could possibly be deleted - didn't know if that was maybe used if any other parts of code and being cautious
      /// </summary>
      /// <returns>
      /// Json object - success true/false,
      /// IsImage - false if pdf, true all image types, logic placed here to reduce javascript code and C# has powerful string manipulation, easier to read on and on
      /// pathOnServer - to use as href,
      /// fileName - used as identifier if deleting pending files
      /// </returns>
      ///****************************************************************************
      [HttpPost]
      public string UploadNewFile(string imgType)
      {
         try
         {
            var fName = string.Empty;
            string fileName1 = string.Empty;
            string fullPath = string.Empty;

            //this is called in a loop per file for simplicty sake by the caller, so we should only ever see one here if you're wordering about the code
            for (int i = 0; i < Request.Files.Count; i++)
            {

               HttpPostedFileBase file = Request.Files[i];
               //Save file content goes here
               fName = Guid.NewGuid().ToString(); //file.FileName;
               if (file != null && file.ContentLength > 0)
               {
                  var originalDirectory = new System.IO.DirectoryInfo($"{Server.MapPath(@"\")}Areas\\GreenBin");
                  var pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\" + imgType);
                  fileName1 = this.HttpContext.User.Identity.Name + "_GreenBin_" + System.IO.Path.GetFileName(file.FileName);
                  //%, # and + make bad urls for image
                  //you would think just URLEncode them, but that creates a security problem - search this error message for more info...
                  //"The request filtering module is configured to deny a request that contains a double escape sequence."
                  //simply going to remove these characters, these are for the actual storage location, so it needs to happen on the server-side
                  fileName1 = fileName1.Replace("#", "_");
                  fileName1 = fileName1.Replace("+", "_");
                  fileName1 = fileName1.Replace("%", "_");

                  //related to this, the filename is used as a div ID, when called in jquery some names can cause a problem if they have 
                  //parenthesis and or spaces.  This was a problem when clicking the delete button for pending attachments.
                  //Instead of haviing replaces in 3 different places, let's consolidate them all here
                  fileName1 = fileName1.Replace("(", "_");
                  fileName1 = fileName1.Replace(")", "_");
                  fileName1 = fileName1.Replace(" ", "_");

                  var isExists = System.IO.Directory.Exists(pathString);

                  //Create temp directory if it doesn't exist
                  if (!isExists)
                  {
                     System.IO.Directory.CreateDirectory(pathString);
                  }

                  fullPath = $"{pathString}\\{fileName1}";

                  if (System.IO.File.Exists(fullPath))
                  {
                     return JsonConvert.SerializeObject(new { success = false, error = "File is already uploaded" });
                  }

                  file.SaveAs(fullPath);
               }
            }

            bool isImg = !fileName1.EndsWith(".pdf");
            return JsonConvert.SerializeObject(new { success = true, status = "success", isImage = isImg, pathOnServer = "/Areas/GreenBin/Attached/" + imgType + "/" + fileName1, fileName = fileName1 });
         }
         catch (Exception e)
         {
            return JsonConvert.SerializeObject(new { success = false, error = e.Message });
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Delete local selected images by their fileName.
      /// </summary>
      /// <param name="fileName"></param>
      /// <param name="intrFolder"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult DeleteSelectedImage(string fileName, string intrFolder)
      {
         var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\GreenBin", Server.MapPath(@"\")));
         string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached\\" + intrFolder);
         var fileName1 = this.HttpContext.User.Identity.Name + "_GreenBin_" + System.IO.Path.GetFileName(fileName);
         var path = string.Format("{0}\\{1}", pathString, fileName1);
         if (System.IO.File.Exists(path))
         {
            System.IO.File.Delete(path);
         }
         return Json(new { Success = true, Error = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete local image by their address.
      /// </summary>
      /// <param name="address"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult DeleteSelectedImageAddress(string address)
      {
         var originalDirectory = Server.MapPath(address);
         if (System.IO.File.Exists(originalDirectory.ToString()))
         {
            System.IO.File.Delete(originalDirectory.ToString());
         }
         return Json(new { Success = true, Error = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      /// Similar to DeleteSelected image, but this is for ajax post and made to accept the full file name with username embedded etc
      /// adding try catch
      /// </summary>
      /// <param name="fileName">full filename of file to be deleted from areas\wrhsstaging\attached files - can be image or pdf</param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      public ActionResult DeletePendingFile(string fileName)
      {
         try
         {
            var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\GreenBin", Server.MapPath(@"\")));
            string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "Attached");
            var path = string.Format("{0}\\{1}", pathString, fileName);

            if (System.IO.File.Exists(path))
            {
               System.IO.File.Delete(path);
            }

            return Json(new { success = true });
         }
         catch (Exception e)
         {
            return Json(new { success = false, error = e.Message });
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get container type.
      /// </summary>
      /// <param name="containerId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetContainerType(string containerId)
      {
         var strCheck = GreenBinModel.GetContainerType(containerId);
         return this.Json(new
         {
            Success = strCheck != null,
            Error = strCheck == null,
            WrhsContr = strCheck
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Get machine Location.
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetMachineLocation(string machineName)
      {
         machineName = string.IsNullOrWhiteSpace(machineName) ? SidebarCommon.GetMachineName(this.Request) : machineName;
         var strCheck = GreenBinModel.GetMachineLocation(machineName);
         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(strCheck),
            Error = string.IsNullOrWhiteSpace(strCheck),
            Location = strCheck
         });
      }
   }
}
