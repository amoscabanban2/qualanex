///-----------------------------------------------------------------------------------------------
/// <copyright file="LotEntity.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GreenBin.Models
{
   using System;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;
   using System.Runtime.Caching;

   using Microsoft.Ajax.Utilities;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using Qualanex.Qosk.Library.Common.Logger;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class LotEntity
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public LotEntity()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~LotEntity()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Lot Number ID")]
      public int LotNumberID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Product ID")]
      public int ProductID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Lot Number")]
      public string LotNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Expiration Date")]
      public DateTime? ExpirationDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Recall ID")]
      public long? RecallID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"No Returns Allowed")]
      public bool? NoReturnsAllowed { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Version")]
      public int Version { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Is Deleted")]
      public bool IsDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Effective Start Date")]
      public DateTime? EffectiveStartDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Effective End Date")]
      public DateTime? EffectiveEndDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created By")]
      public string CreatedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created Date")]
      public DateTime? CreatedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified By")]
      public string ModifiedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified Date")]
      public DateTime? ModifiedDate { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Save LotNumber in database.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="lotNumber"></param>
      /// <param name="expDate"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveLotNumber(int productId, string lotNumber, DateTime expDate, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         { 
            try
            {
               // check for existing lot
               var lotEntity = (from lot in cloudEntities.Lot
                                where lot.ProductID == productId
                                && lot.LotNumber == lotNumber
                                && lot.IsDeleted == false
                                select lot
                                ).FirstOrDefault();

               //=============================================================================
               // If we do not get an entity match, then add new lot; otherwise, update
               //-----------------------------------------------------------------------------
               if (lotEntity == null)
               {
                  // Add a new lot into the DB
                  var newLot = new Lot
                  {
                     ProductID = productId,
                     LotNumber = lotNumber,
                     ExpirationDate = expDate,
                     NoReturnsAllowed = false,
                     IsDeleted = false,
                     CreatedBy = curUser,
                     CreatedDate = DateTime.UtcNow
                  };

                  cloudEntities.Lot.Add(newLot);
               }
               else
               {
                  // Update the existing lot in the DB
                  lotEntity.ExpirationDate = expDate;
                  lotEntity.ModifiedBy = curUser;
                  lotEntity.ModifiedDate = DateTime.UtcNow;
               }

               cloudEntities.SaveChanges(); 
               return true;
            }
            catch (Exception ex)
            {
               Logger.Error(ex.Message);
               throw;
            }
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Delete the lot number based on Lot ID
      /// </summary>
      /// <param name="lotId"></param>
      /// <param name="deletedBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool DeleteLotNumber(int lotId, string deletedBy)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               // check for existing lot
               var lotEntity = (from lot in cloudEntities.Lot
                                where lot.LotNumberID == lotId
                                && lot.IsDeleted == false
                                select lot
                                ).FirstOrDefault();

               //=============================================================================
               // Set the Lot to deleted if found
               //-----------------------------------------------------------------------------
               if (lotEntity != null)
               {
                  lotEntity.IsDeleted = true;
                  lotEntity.ModifiedBy = deletedBy;
                  lotEntity.ModifiedDate = DateTime.UtcNow;
                  cloudEntities.SaveChanges();
                  return true;
               }
               
               return false;
            }
            catch (Exception ex)
            {
               Logger.Error(ex.Message);
               throw;
            }
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Retrieve the lot number based on Lot ID
      /// </summary>
      /// <param name="lotId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Lot GetLotNumberById(int lotId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               return (from lot in cloudEntities.Lot
                       where lot.LotNumberID == lotId
                       && lot.IsDeleted == false
                       select lot
                       ).FirstOrDefault();
            }
            catch (Exception ex)
            {
               Logger.Error(ex.Message);
               throw;
            }
         }

      }

      #endregion

   }
}
