///-----------------------------------------------------------------------------------------------
/// <copyright file="GreenBinModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.QoskCloud.Web.Areas.GreenBin.Models;

namespace Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels
{
   using System;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;

   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using System.Data.SqlClient;
   using System.Data.Entity.Validation;
   using Utility.Common;
   using Microsoft.WindowsAzure.Storage.Blob;
   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Shared.Protocol;
   using System.IO;
   using Product.Models;
   using Product;
   using System.Security.Cryptography;
   using System.Web.WebPages;
   using System.Runtime.Caching;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GreenBinModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public GreenBinModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~GreenBinModel()
      {
         this.Dispose(false);
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"NDC/UPC")]
      public string NDC { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"MFG")]
      public string MFG { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"LotFlag")]
      public bool LotFlag { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Brand")]
      public string Brand { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Generic")]
      public string Generic { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Dosage")]
      public string Dosage { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Unit/Pkg")]
      public string Unit { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Rx/OTC")]
      public string Rx { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"PkgSize")]
      public decimal? PackageSize { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Green Bin")]
      public string WrhsContrID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"ItemGUID")]
      public Guid ItemGUID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"ProductName")]
      public string ProductName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Product ID")]
      public int ProductID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Rollup Product ID")]
      public int? RollupProductId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   True/False flag that states whether this is an Inner Product
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"NDCInnerPack")]
      public bool NDCInnerPack { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"WrhsContrType")]
      public string WrhsContrType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RAorWPT")]
      public string RAorWPT { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"TrackingID")]
      public string TrackingID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"DebitMemo")]
      public string DebitMemo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"ItemCount")]
      public int ItemCount { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Destination")]
      public string Destination { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"SeqNo")]
      public long? SeqNo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"WrhsContrInst")]
      public string WrhsContrInst { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Status")]
      public string Status { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Status Date")]
      public DateTime StatusDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Individual Count Weight")]
      public string IndvWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the Item Guid's container weight
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Container Weight")]
      public decimal? ItemContainerWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the Item Guid's content weight
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Content Weight")]
      public decimal? ItemContentsWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the Item Guid's Quantity
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Content Weight")]
      public decimal ItemQty { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Lot Number")]
      public string LotNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Flag to determine if the lot number was changed during the green bin process
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Lot Changed")]
      public bool LotChanged { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Flag to determine if the waste stream was changed during the green bin process
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Stream Segment Changed")]
      public bool WasteSegmentChanged { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Stream")]
      public string WasteStream { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Stream")]
      public string WasteStreamCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Expiration Date")]
      public DateTime? ExpDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Expiration Month")]
      public string ExpMonth { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Expiration Year")]
      public string ExpYear { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Dosage Code")]
      public string DosageCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Dosage Image Code")]
      public string DosageImageCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Image Source Code")]
      public string ImageSourceCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Image Type Code")]
      public string ImageTypeCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Profile Code")]
      public int ProfileCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Container Images Count")]
      public int ContainerImagesCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Content Images Count")]
      public int ContentImagesCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Unit Of Measure")]
      public string UnitOfMeasure { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Code")]
      public string WasteCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Induction Date")]
      public DateTime? InductionDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Station #")]
      public string StationId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Time Processing")]
      public string TimeProcessing { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Inductor")]
      public string Inductor { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Container Type")]
      public string ContainerTypeName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Destination Type")]
      public string DestinationType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Full Container Weight")]
      public string FullContainerWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Empty Container Weight")]
      public string EmptyContainerWeight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Destination Code")]
      public string DestinationCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Category")]
      public string Category { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Sorted")]
      public int Sorted { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Sorted Location")]
      public string OutboundContainerID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Control Number")]
      public int CtrlNumber { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item State Code")]
      public string ItemStateCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Whether this item was inducted from a foreign container
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"isForeignContainer")]
      public string isForeignContainer { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Determines if the user acknowledged the quarantine messaging
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Quarantine")]
      public string Quarantine { get; set; }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Get List of WasteCodes Associated with specific productId.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GreenBinModel> GetWasteCodes(int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from productWasteCode in cloudEntities.ProductWasteCode
                    join wasteDict in cloudEntities.WasteCodeDict on productWasteCode.WasteCode equals wasteDict.Code
                    where productWasteCode.ProductID == productId && productWasteCode.WasteCode != "" && productWasteCode.WasteCode != "PHME"
                          && productWasteCode.StateCode == "" && !productWasteCode.IsDeleted
                    select new GreenBinModel
                    {
                       WasteCode = productWasteCode.WasteCode,
                       WasteStreamCode = productWasteCode.WasteStreamCode,
                       WasteStream = productWasteCode.WasteCode
                    }).Distinct().ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of waste code from WasteDict.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetWasteCodeList(int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {

            var itemsQuery = cloudEntities.ProductWasteCode.Where(c => c.ProductID == productId && c.IsDeleted == false && c.WasteCode != "" && c.WasteCode != "PHME" && c.StateCode == "")
                             .Select(c => c.WasteCode).Distinct().ToList();
            var items = cloudEntities.WasteCodeDict.Where(c => c.IsDeleted == false && c.Code != "" && c.Code != "PHME" && c.Code != null && c.HazardTypeCode == "Federal")
                             .Select(c => new { c.Code, c.Description }).ToList();

            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "-------- New Waste Code --------",
                  Value = "Add",
                  //Disabled = true
               }
            };

            selectedItems.AddRange(items.Select(c => new SelectListItem { Value = c.Code, Text = c.Code + " - " + c.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
            selectedItems.Where(c => itemsQuery.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of WasteCodes Associated with a specific waste stream
      /// </summary>
      /// <param name="wasteStreamCode"></param>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetWasteCodesFromWasteStream(string wasteStreamCode, int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var itemsQuery = cloudEntities.ProductWasteCode.Where(c => c.ProductID == productId && c.IsDeleted == false && c.WasteCode != "" && c.WasteCode != "PHME" && c.StateCode == "")
               .Select(c => c.WasteCode).Distinct().ToList();
            var items = (from codePr in cloudEntities.WasteCodeProfileRel
                         join wasteDict in cloudEntities.WasteCodeDict on codePr.WasteCode equals wasteDict.Code
                         join steamPr in cloudEntities.WasteStreamProfileRel on codePr.WasteStreamProfileID equals steamPr.WasteStreamProfileID
                         where steamPr.WasteStreamCode == wasteStreamCode
                               && wasteDict.HazardTypeCode == "Federal" && !wasteDict.IsDeleted && !codePr.IsDeleted
                               && wasteDict.Code != "PHME" && wasteDict.Code != "" && wasteDict.Code != null
                         select new
                         {
                            Code = codePr.WasteCode,
                            Description = wasteDict.Description
                         }).Distinct().ToList();
            //var items = cloudEntities.WasteCodeDict.Where(c => c.IsDeleted == false && c.Code != "" && c.Code != "PHME" && c.Code != null && c.HazardTypeCode == "Federal").Select(c => new { c.Code, c.Description }).ToList();

            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "-------- New Waste Code --------",
                  Value = "Add",
                  //Disabled = true
               }
            };

            selectedItems.AddRange(items.Select(c => new SelectListItem { Value = c.Code, Text = c.Code + " - " + c.Description }));
            selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
            selectedItems.Where(c => itemsQuery.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      /////****************************************************************************
      ///// <summary>
      /////   Get Waste Stream WasteCode Rel
      ///// </summary>
      ///// <returns></returns>
      /////****************************************************************************
      //public static List<GreenBinModel> GetWasteStreamCodeRel()
      //{
      //   using (var cloudEntities = new QoskCloud())
      //   {
      //      return cloudEntities.WasteStreamCodeRel.Select(c => new GreenBinModel { WasteStreamCode = c.WasteStream, WasteCode = c.WasteCode }).ToList();
      //   }
      //}

      ///****************************************************************************
      /// <summary>
      ///   Get All waste stream from waste stream dict.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetWasteStreamList(int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var itemsQuery = cloudEntities.ProductWasteCode.Where(c => c.ProductID == productId && c.IsDeleted == false && c.WasteCode != "" && c.WasteCode != "PHME" && c.StateCode == "")
                             .Select(c => c.WasteStreamCode).Distinct().ToList();
            var items = cloudEntities.WasteStreamDict.Where(c => c.IsDeleted == false && c.Code != "PHME").Select(c => new { c.Code, c.Description }).ToList();

            var selectedItems = new List<SelectListItem>
            {
               new SelectListItem
               {
                  Text = "-------- New Waste Stream --------",
                  Value = "Add",
                  //Disabled = true
               }
            };

            selectedItems.AddRange(items.Select(c => new SelectListItem { Value = c.Code, Text = c.Description }));
            //selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
            selectedItems.Where(c => itemsQuery.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

            return selectedItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get All Waste Streams associated with productId.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<GreenBinModel> GetWasteStreams(int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from productWasteCode in cloudEntities.ProductWasteCode
                    join wasteDict in cloudEntities.WasteStreamDict on productWasteCode.WasteStreamCode equals wasteDict.Code
                    where productWasteCode.ProductID == productId && productWasteCode.WasteStreamCode != "" && productWasteCode.WasteStreamCode != "PHME"
                          && productWasteCode.StateCode == "" && !productWasteCode.IsDeleted && !wasteDict.IsDeleted
                    select new GreenBinModel
                    {
                       WasteStreamCode = productWasteCode.WasteStreamCode,
                       WasteStream = wasteDict.Description,
                       WasteCode = productWasteCode.WasteStreamCode
                    }).Distinct().ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of product images.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ImageResult> GetItemImages(int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var lst = (from image in cloudEntities.ProductImage
                       join product in cloudEntities.Product on image.ProductID equals product.ProductID into prdct
                       from product in prdct.DefaultIfEmpty()
                       where image.ProductID == productId && !image.IsDeleted
                       select
                          new ImageResult()
                          {
                             EffectiveEndDate = image.EffectiveEndDate,
                             EffectiveStartDate = image.EffectiveStartDate,
                             ImageType = image.ImageTypeCode,
                             FileName = image.FileName,
                             AltText = product.Description
                          }).ToList();

            lst.ForEach(c => c.Url = ProductCommon.GetProductImageUrl((c.FileName)));

            return lst;
         }
      }

      /////****************************************************************************
      ///// <summary>
      /////   Get List of items.
      ///// </summary>
      ///// <param name="itemGuid"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static List<GreenBinModel> GetItemList(string binId)
      //{
      //   using (var cloudEntities = new QoskCloud())
      //   {
      //      var binInfo = (from wrhsStatus in cloudEntities.WrhsContrStatusHist
      //                     where wrhsStatus.WrhsContrID == binId
      //                     && wrhsStatus.WrhsInstID == (from crtDate in cloudEntities.WrhsContrStatusHist
      //                                                  where crtDate.WrhsContrID == binId
      //                                                  orderby crtDate.CreatedDate descending
      //                                                  select crtDate.WrhsInstID).FirstOrDefault()
      //                     select new
      //                     {
      //                        wrhsStatus.WrhsContrID,
      //                        wrhsStatus.WrhsInstID,
      //                        wrhsStatus.CreatedDate
      //                     }).FirstOrDefault();

      //      var manufacturerTypeCode = "MFGR";
      //      var lst = (from items in cloudEntities.Item
      //                 join prdct in cloudEntities.Product on items.ProductID equals prdct.ProductID
      //                 join mfrProfile in cloudEntities.Profile on new { profileCode = prdct.ProfileCode, profileTypeCode = manufacturerTypeCode } equals
      //             new { profileCode = mfrProfile.ProfileCode, profileTypeCode = mfrProfile.ProfileTypeCode }
      //                 join dsg in cloudEntities.DosageDict on prdct.DosageCode equals dsg.Code
      //                 join wrhsRel in cloudEntities.WrhsContrItemRel on items.ItemGUID equals wrhsRel.ItemGUID into wrhsItems
      //                 from wrhsRel in wrhsItems.DefaultIfEmpty()
      //                 join wrhsStatus in cloudEntities.WrhsContrStatusHist on new { wrhsRel.WrhsContrID, wrhsRel.WrhsInstID } equals new { wrhsStatus.WrhsContrID, wrhsStatus.WrhsInstID }
      //                 join wrhsCntr in cloudEntities.WrhsContr on wrhsRel.WrhsContrID equals wrhsCntr.WrhsContrID
      //                 join wrhsType in cloudEntities.WrhsContrTypeDict on wrhsCntr.Type equals wrhsType.Code
      //                 where
      //                 wrhsRel.WrhsContrID == binId
      //                 && wrhsCntr.WrhsContrID == binId
      //                 && wrhsType.Designation == "B"
      //                 && wrhsStatus.StatusCode == "S"
      //                 && wrhsRel.WrhsInstID == binInfo.WrhsInstID
      //                 && wrhsRel.IsActive == true
      //                 select new GreenBinModel
      //                 {
      //                    ItemGUID = items.ItemGUID,
      //                    ProductName = prdct.Description,
      //                    ProductID = items.ProductID,
      //                    WrhsContrType = wrhsType.Description,
      //                    Brand = prdct.Description,
      //                    Dosage = dsg.Description,
      //                    Generic = prdct.GenericName,
      //                    Unit = prdct.UnitsPerPackage.ToString(),
      //                    Rx = prdct.RXorOTC ? "RX" : "OTC",
      //                    TrackingID = items.TrackingID,
      //                    ContainerTypeName = items.ContainerTypeName,
      //                    MFG = mfrProfile.Name,
      //                    PackageSize = prdct.PackageSize,
      //                    ItemCount = 0,
      //                    SeqNo = items.RecallID,
      //                    WrhsContrInst = wrhsStatus.WrhsInstID,
      //                    Status = wrhsStatus.StatusCode,
      //                    ExpDate = (DateTime)items.ExpirationDate,
      //                    IndvWeight = prdct.IndividualCountWeight.ToString(),
      //                    LotNumber = items.LotNumber,
      //                    CtrlNumber = prdct.ControlNumber,
      //                    NDC = prdct.NDCUPCWithDashes,
      //                    WrhsContrID = (from wrhsRl in cloudEntities.WrhsContrItemRel
      //                                   where wrhsRl.ItemGUID == items.ItemGUID && wrhsRl.IsActive == true
      //                                   orderby wrhsRl.CreatedDate descending, wrhsRl.ModifiedDate
      //                                   select wrhsRl.WrhsContrID).FirstOrDefault()

      //                 }).Distinct().ToList();
      //      lst.ForEach(c => c.Sorted = c.WrhsContrID.ToUpper() == binInfo.WrhsContrID.ToUpper() ? 0 : 1);
      //      var itemsList = new List<GreenBinModel>();
      //      foreach (var items in lst)
      //      {
      //         if (!itemsList.Select(c => c.ItemGUID).Contains(items.ItemGUID))
      //         {
      //            if (items.SeqNo == 0 || string.IsNullOrWhiteSpace(items.SeqNo.ToString()))
      //            {
      //               var wasteStreamCodes = GetWasteStreamInfo(items.ProductID, ProductCommon.GetUserState());
      //               if (wasteStreamCodes == null)
      //               {
      //                  items.Category = "";
      //                  items.DestinationType = "LRG_GAYLRD";
      //                  items.DestinationCode = "Large Gaylord";
      //                  items.Destination = "Gaylord";
      //               }
      //               else
      //               {
      //                  items.Category = wasteStreamCodes.Category;

      //                  items.DestinationCode = wasteStreamCodes.DestinationCode;
      //                  items.Destination = wasteStreamCodes.Destination;
      //                  if (wasteStreamCodes.WasteCode == "Q999")
      //                  {
      //                     switch (items.CtrlNumber)
      //                     {
      //                        case 0:
      //                           {
      //                              items.DestinationType = "LRG_GAYLRD";
      //                              items.Destination = "Control 0 Gaylord";
      //                              break;
      //                           }
      //                        case 2:
      //                           {
      //                              items.DestinationType = "BLK_GAYLRD";
      //                              items.Destination = "Control 2 Gaylord";
      //                              break;
      //                           }
      //                        default:
      //                           {
      //                              items.DestinationType = "ORG_GAYLRD";
      //                              items.Destination = "Control 3 4 5 Gaylord";
      //                              break;
      //                           }
      //                     }
      //                  }
      //                  else
      //                  {
      //                     items.DestinationType = "WST_GAYLRD";
      //                  }
      //               }

      //            }
      //            else
      //            {
      //               items.Category = items.SeqNo.ToString();
      //               items.DestinationType = "RCL_GAYLRD";
      //               var recallItems = (from rcl in cloudEntities.Recall
      //                                  where rcl.RecallID == items.SeqNo
      //                                  select new
      //                                  {
      //                                     RecallNumber = rcl.RecallNumber
      //                                  }).FirstOrDefault();
      //               items.DestinationCode = items.SeqNo.ToString();
      //               items.Destination = "Recall -" + recallItems.RecallNumber.ToString();
      //            }
      //            if (items.Sorted == 1)
      //            {
      //               items.OutboundContainerID = items.WrhsContrID;
      //            }
      //            itemsList.Add(items);
      //         }
      //      }
      //      return itemsList.Count == 0 ? new List<GreenBinModel>() : itemsList;
      //   }
      //}

      ///****************************************************************************
      /// <summary>
      ///   Get Waste Stream record.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="stateCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static GreenBinModel GetWasteStreamInfo(int productId, string stateCode)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from prdct in cloudEntities.ProductWasteStream
                    join wstStreamProfileRel in cloudEntities.WasteStreamProfileRel on prdct.WasteStreamCode equals wstStreamProfileRel.WasteStreamCode
                    join wstStreamProfile in cloudEntities.WasteStreamProfile on wstStreamProfileRel.WasteStreamProfileID equals wstStreamProfile.WasteProfileID
                    join wrhsStream in cloudEntities.WasteStreamDict on prdct.WasteStreamCode equals wrhsStream.Code
                    from stateDict in cloudEntities.StateDict
                           .Where(x => x.Code == prdct.StateCode).DefaultIfEmpty()
                    where prdct.ProductID == productId &&
                             (prdct.StateCode == stateCode || prdct.StateCode == "")
                             && !wstStreamProfileRel.IsDeleted
                             && !wstStreamProfile.IsDeleted
                             && !wrhsStream.IsDeleted
                             && !stateDict.IsDeleted
                    select new GreenBinModel
                    {
                       SeqNo = prdct.SeverityOrder,
                       WasteCode = prdct.WasteCode,
                       DestinationCode = prdct.WasteStreamCode,
                       Category = wstStreamProfileRel.WasteStreamProfileID,
                       Destination = wrhsStream.Description
                    }).OrderByDescending(c => c.SeqNo).FirstOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static GreenBinReason GetGreenBinReason(string itemGuid)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var itemsProperty = (from prop in cloudEntities.PropertyValueRel
                                 where prop.RelationCode == itemGuid && prop.SValue != "C" && prop.SValue != "X"
                                 select prop.PropertyCode).ToList();

            var greenBinReason = new GreenBinViewModel();

            foreach (var itm in itemsProperty)
            {
               switch (itm)
               {
                  case "CNTRIMG":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.ContainerImage;
                        break;
                     }
                  case "CONTIMG":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.ContentImage;
                        break;
                     }
                  case "PRODWGT":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.Weight;
                        break;
                     }
                  case "PRODWST":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.WasteStream;
                        break;
                     }
                  case "LOTEXP":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.LotExpiration;
                        break;
                     }
                  case "QUARTNE":
                     {
                        greenBinReason.GreenBinReasons = greenBinReason.GreenBinReasons | GreenBinReason.Quarantine;
                        break;
                     }
               }
            }

            return greenBinReason.GreenBinReasons;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static GreenBinModel GetBinProcessingInfo(Guid itemGUID, int productId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var sqlQuery = "select distinct items.ItemGUID as ItemGUID , prdct.Description as ProductName, " +
                                  "items.ProductID as ProductID, items.RollupProductId as RollupProductId, prdct.Description as Brand,dsg.Description as Dosage , " +
                                  " prdct.GenericName as Generic,convert(nvarchar(max), prdct.UnitsPerPackage) as Unit , " +
                                  " prdct.UnitOfMeasure as UnitOfMeasure,case when prdct.RXorOTC = 1 then 'RX' else 'OTC' end as Rx " +
                                  " , items.TrackingID as TrackingID,items.ContainerTypeName as ContainerTypeName,mfrProfile.Name as MFG " +
                                  " , prdct.PackageSize as PackageSize,items.RecallID as SeqNo, items.ExpirationDate as ExpDate, " +
                                  " convert(nvarchar(max), prdct.IndividualCountWeight) as IndvWeight, items.LotNumber as LotNumber ," +
                                  " items.ContainerWeight as ItemContainerWeight,items.ContentsWeight as ItemContentsWeight, items.Quantity as ItemQty,  " +
                                  " dsgPackRel.DosageImageCode as DosageImageCode, prdct.NDCUPCWithDashes as NDC, ISNULL(prdct.NDCInnerPack,0) as NDCInnerPack," +
                                  " isnull(cast(prdct.ControlNumber as int),0) as CtrlNumber, dsg.Code as DosageCode, items.CreatedDate as InductionDate , " +
                                  " (select count(*) from productimage prdctImg where prdctImg.ProductID = prdct.ProductID and prdctImg.ImageTypeCode = 'CTR') as ContainerImagesCount " +
                                 " ,(select count(*) from productimage prdctImg where prdctImg.ProductID = prdct.ProductID and prdctImg.ImageTypeCode = 'CNT') as ContentImagesCount " +
                                 " ,case when items.ExpirationDate is not null then convert(nvarchar,MONTH(items.ExpirationDate)) else '' end as ExpMonth " +
                                 ",case when items.ExpirationDate is not null then convert(nvarchar, YEAR(items.ExpirationDate)) else '' end as ExpYear, items.ItemStateCode" +
                                 ",case when exists (select * from ItemConditionRel where ItemGUID = @itemGuid and ConditionCode = 'ForeignContainer' and AppCode = 'MASTER' and isDeleted = 0) then 'true' else 'false' end as isForeignContainer " +
                                 " from item items join product prdct on items.ProductID = prdct.[ProductID] " +
                                 " join Profile mfrProfile on mfrProfile.ProfileTypeCode = 'MFGR' and prdct.ProfileCode = mfrProfile.ProfileCode " +
                                 " join DosageDict dsg on prdct.DosageCode = dsg.Code join DosagePackageRel dsgPackRel on dsg.Code = dsgPackRel.DosageCode" +
                                 " where items.ItemGUID = @itemGuid and prdct.ProductID = @prdctId ";

               var productItems = cloudEntities.Database.SqlQuery<GreenBinModel>(sqlQuery
                                                            , new SqlParameter("@itemGuid", itemGUID)
                                                            , new SqlParameter("@prdctId", productId)).FirstOrDefault();

               return productItems;
            }
            catch (Exception ex)
            {
               Utility.Logger.Error(ex.Message);
               throw;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static GreenBinReason SaveItems(List<GreenBinModel> wasteCodes, List<GreenBinModel> wasteStreams, GreenBinModel entities, string curUser, string cancellation)
      //Guid itemGUID, int productId, string curUser)
      {
         var itemX = 0;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var existProductInfo = (from productImages in cloudEntities.ProductImage
                                       join product in cloudEntities.Product on productImages.ProductID equals product.ProductID
                                       join dsgdict in cloudEntities.DosageDict on product.DosageCode equals dsgdict.Code
                                       join dsgDicTel in cloudEntities.DosagePackageRel on dsgdict.Code equals dsgDicTel.DosageCode
                                       where product.ProductID == entities.ProductID
                                       select new GreenBinModel
                                       {
                                          ProductID = productImages.ProductImageID,
                                          DosageCode = dsgDicTel.DosageCode,
                                          DosageImageCode = dsgDicTel.DosageImageCode,
                                          ImageTypeCode = productImages.ImageTypeCode,
                                          ImageSourceCode = productImages.ImageSourceCode,
                                          ProfileCode = product.ProfileCode
                                       }
                                       ).Distinct().ToList();
               var profileCode = 0;
               var ctrImages = 0;
               var cntImages = 0;
               //var itemInfo= cloudEntities.Item.Where(c=>c.ItemGUID==entities.ItemGUID)
               var itemInfo = (from items in cloudEntities.Item
                               join prdct in cloudEntities.Product on items.ProductID equals prdct.ProductID
                               where items.ItemGUID == entities.ItemGUID && items.ProductID == entities.ProductID
                               select new { Items = items, Products = prdct }).SingleOrDefault();

               if (existProductInfo.Count > 0)
               {
                  profileCode = existProductInfo.Select(c => c.ProfileCode).First();
                  ctrImages = existProductInfo.Count(c => c.ImageTypeCode == "CTR");
                  cntImages = existProductInfo.Count(c => c.ImageTypeCode == "CNT");
               }
               else
               {
                  profileCode = itemInfo.Products.ProfileCode;
               }

               if (entities.ContainerImagesCount > 0 || entities.ContentImagesCount > 0)
               {
                  var propValSet = false; //Set both image type properties to "Complete" if at least one image exists
                  var ctrImgCancel = false;
                  var cntImgCancel = false;
                  var originalDirectoryContainer = new System.IO.DirectoryInfo($"{System.Web.HttpContext.Current.Server.MapPath(@"\")}Areas\\GreenBin\\Attached");
                  var pathString = Path.Combine(originalDirectoryContainer.ToString(), "CNT");
                  string[] fileList = null;

                  // check directories for both image types
                  for (var x = 0; x < 2; x++)
                  {
                     if (pathString == "")
                     {
                        pathString = Path.Combine(originalDirectoryContainer.ToString(), "CTR");
                     }

                     var isExists = Directory.Exists(pathString);

                     if (!isExists)
                     {
                        Directory.CreateDirectory(pathString);
                     }

                     fileList = System.IO.Directory.GetFiles(pathString);

                     if (!fileList.Any() && cancellation.Contains("Image"))
                     {
                        if (pathString.Contains("CTR"))
                        {
                           CheckPropertyAndFailed(entities.ItemGUID.ToString(), "CNTRIMG", curUser);
                           ctrImgCancel = true;
                        }
                        else if (pathString.Contains("CNT"))
                        {
                           CheckPropertyAndFailed(entities.ItemGUID.ToString(), "CONTIMG", curUser);
                           cntImgCancel = true;
                        }
                     }

                     if (x < 1)
                     {
                        pathString = "";
                     }
                  }

                  foreach (var files in fileList)
                  {
                     var fileName = Path.GetFileName(files);

                     if (fileName.Contains(curUser + "_GreenBin_"))
                     {
                        if (itemX == 0)
                        {
                           if (!ctrImgCancel)
                           {
                              SetPropertyValueRel(entities.ItemGUID.ToString(), "CNTRIMG", curUser);
                           }

                           if (!cntImgCancel)
                           {
                              SetPropertyValueRel(entities.ItemGUID.ToString(), "CONTIMG", curUser);
                           }

                           propValSet = true;
                        }

                        itemX++;

                        var newFileName = entities.ProductID + "_" + 1 + "_q" + (ctrImages == 0 ? Path.GetExtension(files) : ctrImages + Path.GetExtension(files));
                        ctrImages++;
                        var localPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"/ProductImages/";
                        var Pth = Path.Combine(localPath, profileCode.ToString() + "/");

                        var md5 = UploadFilesfromLocalToBlob(profileCode.ToString(), files, Utility.Constants.AzureAccount_Products, Pth, newFileName);
                        var productImage = new ProductImage
                        {
                           ConditionCode = itemInfo.Items.ConditionCode,
                           ItemStateCode = itemInfo.Items.ItemStateCode,
                           ImageTypeCode = "CTR",
                           FileName = profileCode.ToString() + @"\" + newFileName,
                           ProductID = entities.ProductID,
                           ImageSourceCode = "Q",
                           MD5 = md5
                        };

                        cloudEntities.ProductImage.Add(productImage);
                        cloudEntities.SaveChanges();
                     }
                  }

                  // Content
                  pathString = Path.Combine(originalDirectoryContainer.ToString(), "CNT");
                  fileList = System.IO.Directory.GetFiles(pathString);
                  itemX = 0;

                  foreach (var files in fileList)
                  {
                     var fileName = Path.GetFileName(files);

                     if (fileName.Contains(curUser + "_GreenBin_"))
                     {
                        if (itemX == 0 && !propValSet)
                        {
                           if (!ctrImgCancel)
                           {
                              SetPropertyValueRel(entities.ItemGUID.ToString(), "CNTRIMG", curUser);
                           }

                           if (!cntImgCancel)
                           {
                              SetPropertyValueRel(entities.ItemGUID.ToString(), "CONTIMG", curUser);
                           }
                        }

                        itemX++;

                        var newFileName = entities.ProductID + "_" + 2 + "_q" + (cntImages == 0 ? Path.GetExtension(files) : cntImages + Path.GetExtension(files));
                        cntImages++;
                        var localPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"/ProductImages/";
                        var Pth = Path.Combine(localPath, profileCode.ToString() + "/");
                        var md5 = UploadFilesfromLocalToBlob(profileCode.ToString(), files, Utility.Constants.AzureAccount_Products, Pth, newFileName);
                        var productImage = new ProductImage
                        {
                           ConditionCode = itemInfo.Items.ConditionCode,
                           ItemStateCode = itemInfo.Items.ItemStateCode,
                           ImageTypeCode = "CNT",
                           FileName = Path.Combine(profileCode.ToString(), newFileName),
                           ProductID = entities.ProductID,
                           ImageSourceCode = "Q",
                           MD5 = md5
                        };

                        cloudEntities.ProductImage.Add(productImage);
                        cloudEntities.SaveChanges();
                     }
                  }
               }
               else
               {
                  if ((entities.ContainerImagesCount == 0 || entities.ContentImagesCount == 0) && cancellation.Contains("Image"))
                  {
                     CheckPropertyAndFailed(entities.ItemGUID.ToString(), "CNTRIMG", curUser);
                     CheckPropertyAndFailed(entities.ItemGUID.ToString(), "CONTIMG", curUser);
                  }
               }

               itemX = 0;

               //=============================================================================
               // Product Weights section - Update Product Weights
               //-----------------------------------------------------------------------------
               if (itemInfo.Products != null)
               {
                  if (!string.IsNullOrWhiteSpace(entities.FullContainerWeight) && !string.IsNullOrWhiteSpace(entities.EmptyContainerWeight))
                  {
                     itemInfo.Products.FullContainerWeightWithContents = Convert.ToDecimal(entities.FullContainerWeight);
                     itemInfo.Products.ContainerWeight = Convert.ToDecimal(entities.EmptyContainerWeight);
                     itemX++;
                  }
                  if (!string.IsNullOrWhiteSpace(entities.IndvWeight))
                  {
                     itemInfo.Products.IndividualCountWeight = Convert.ToDecimal(entities.IndvWeight);
                     itemX++;
                  }
                  if ((entities.UnitOfMeasure == "EA" && itemX >= 1) || (entities.UnitOfMeasure != "EA" && itemX == 2))
                  {
                     SetPropertyValueRel(entities.ItemGUID.ToString(), "PRODWGT", curUser);
                     cloudEntities.SaveChanges();
                  }
                  else
                  {
                     if (cancellation.Contains("Weight"))
                     {
                        CheckPropertyAndFailed(entities.ItemGUID.ToString(), "PRODWGT", curUser);
                     }
                  }
               }

               //=============================================================================
               // Waste Stream section - update Waste Stream/codes
               //-----------------------------------------------------------------------------
               // For an inner/outer, use the outer (rollup) product ID to save/update the waste streams
               var prodId = itemInfo.Products.NDCInnerPack == true ? (int)itemInfo.Products.RollupProductID : entities.ProductID;
               wasteStreams.ForEach(c => c.ProductID = prodId);
               wasteCodes.ForEach(c => c.ProductID = prodId);
               SetWasteStreamWasteCode(wasteStreams, wasteCodes, curUser, entities.ItemGUID.ToString(), entities.WasteSegmentChanged);

               //=============================================================================
               // Lot/Expiration Date section - update Lot and Expiration only if all three fields are filled
               //-----------------------------------------------------------------------------
               if (!string.IsNullOrWhiteSpace(entities.LotNumber) && !string.IsNullOrWhiteSpace(entities.ExpMonth) && !string.IsNullOrWhiteSpace(entities.ExpYear) && entities.LotChanged)
               {
                  // Update the Lot Number/Expiration Date On the Item
                  var expDate = new DateTime(entities.ExpYear.AsInt(), entities.ExpMonth.AsInt(), 1);
                  itemInfo.Items.LotNumber = entities.LotNumber;
                  itemInfo.Items.ExpirationDate = expDate;

                  // Save/Update lot in the Lot table
                  // For an inner/outer, use the outer (rollup) product ID to save/update the lot
                  var lots = LotEntity.SaveLotNumber(itemInfo.Products.NDCInnerPack == true ? (int)itemInfo.Products.RollupProductID : entities.ProductID, entities.LotNumber, expDate, curUser);

                  //PropertyValueRel table update, only if the lot was saved properly
                  if (lots)
                  {
                     SetPropertyValueRel(entities.ItemGUID.ToString(), "LOTEXP", curUser);
                  }
               }
               else
               {
                  if (cancellation.Contains("Lot"))
                  {
                     CheckPropertyAndFailed(entities.ItemGUID.ToString(), "LOTEXP", curUser);
                  }
               }

               //=============================================================================
               // Quarantine section - this field only has a value if someone clicked the acknowledgement button
               //-----------------------------------------------------------------------------
               if (!string.IsNullOrWhiteSpace(entities.Quarantine))
               {
                  SetPropertyValueRel(entities.ItemGUID.ToString(), "QUARTNE", curUser);
               }

               //cloudEntities.Entry().CurrentValues.SetValues(itemInfo.Items);
               cloudEntities.SaveChanges();
            }

         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }

         return GetGreenBinReason(entities.ItemGUID.ToString());
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Property value and set failed flag.
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <param name="curUser"></param>
      /// <param name="propertyVal"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void CheckPropertyAndFailed(string itemGuid, string propertyVal, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var propertyInfo = cloudEntities.PropertyValueRel.Where(c => c.PropertyCode == propertyVal
            && c.RelationCode == itemGuid).FirstOrDefault();
            if (propertyInfo != null)
            {
               propertyInfo.SValue = "X";
               propertyInfo.Version += 1;
               propertyInfo.ModifiedBy = curUser;
               propertyInfo.ModifiedDate = DateTime.UtcNow;
               cloudEntities.SaveChanges();
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set Property value.
      /// </summary>
      /// <param name="itemGuid"></param>
      /// <param name="curUser"></param>
      /// <param name="propertyVal"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SetPropertyValueRel(string itemGuid, string propertyVal, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var propertyInfo = cloudEntities.PropertyValueRel.Where(c => c.PropertyCode == propertyVal
             && c.RelationCode == itemGuid).FirstOrDefault();
            if (propertyInfo != null)
            {
               propertyInfo.SValue = "C";
               propertyInfo.Version += 1;
               propertyInfo.ModifiedBy = curUser;
               propertyInfo.ModifiedDate = DateTime.UtcNow;
            }
            else
            {
               var propInfo = new PropertyValueRel
               {
                  Version = 0,
                  IsDeleted = false,
                  PropertyCode = propertyVal,
                  CreatedDate = DateTime.UtcNow,
                  CreatedBy = curUser,
                  RelationCode = itemGuid,
                  SValue = "C"
               };
               cloudEntities.PropertyValueRel.Add(propInfo);
            }
            cloudEntities.SaveChanges();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Container type.
      /// </summary>
      /// <param name="containerId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsContr GetContainerType(string containerId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from type in cloudEntities.WrhsContr
                    where type.WrhsContrID == containerId
                    select type).FirstOrDefault() ?? new WrhsContr();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get machine location i.e. determine if PC has been registered
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetMachineLocation(string machineName)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var loc = (from qsk in cloudEntities.Qosk where qsk.MachineID == machineName select qsk).FirstOrDefault();
            return loc?.LocationRoute.ToString() ?? "";
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set Waste Information in Product Waste Code table
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="wasteCode"></param>
      /// <param name="wasteStreams"></param>
      /// <param name="itemGUID"></param>
      /// <param name="wasteStreamChanged"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SetWasteStreamWasteCode(List<GreenBinModel> wasteStreams, List<GreenBinModel> wasteCode, string curUser, string itemGUID, bool wasteStreamChanged)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               //first, delete any non-state-specific waste stream/code
               var productId = wasteCode[0].ProductID;
               var deleteWasteCode = cloudEntities.ProductWasteCode.Where(c => c.ProductID == productId && c.StateCode == "").ToList();

               if (deleteWasteCode.Any())
               {
                  cloudEntities.ProductWasteCode.RemoveRange(deleteWasteCode);
                  cloudEntities.SaveChanges();
               }

               foreach (var wasteStream in wasteStreams)
               {
                  //strings used to cache the ProductWasteStream
                  //important as we use caches for the waste stream in bin designation
                  string userState = ProductCommon.GetUserState();
                  string key;

                  //this should only be true if we don't set a waste code along with the waste stream
                  if (!wasteCode.Where(c => c.WasteStream == wasteStream.WasteStream).Any())
                  {
                     var wsStream = new ProductWasteCode
                     {
                        ProductID = wasteStream.ProductID,
                        WasteCode = wasteStream.WasteCode,
                        WasteStreamCode = wasteStream.WasteStream,
                        StateCode = "",
                        IsDeleted = false,
                        Version = 1,
                        CreatedBy = curUser,
                        CreatedDate = DateTime.UtcNow
                     };

                     cloudEntities.ProductWasteCode.Add(wsStream);
                     cloudEntities.SaveChanges();

                     key = $"ProductWasteStream|{wsStream.ProductID}|{userState}";
                     MemoryCache.Default.Remove(key);
                  }
                  else
                  {
                     foreach (var wsCode in wasteCode.Where(c => c.WasteStream == wasteStream.WasteStream).ToList())
                     {
                        // check for duplicates ProductWasteCode records
                        var pwcEntity = cloudEntities.ProductWasteCode.FirstOrDefault(c => c.ProductID == wsCode.ProductID && c.WasteCode == wsCode.WasteCode && c.StateCode == "");

                        if (pwcEntity == null)
                        {
                           var wstCode = new ProductWasteCode
                           {
                              ProductID = wasteStream.ProductID,
                              WasteCode = wsCode.WasteCode,
                              WasteStreamCode = wasteStream.WasteStream,
                              StateCode = "",
                              IsDeleted = false,
                              Version = 1,
                              CreatedBy = curUser,
                              CreatedDate = DateTime.UtcNow
                           };

                           cloudEntities.ProductWasteCode.Add(wstCode);
                           cloudEntities.SaveChanges();

                           key = $"ProductWasteStream|{wstCode.ProductID}|{userState}";
                           MemoryCache.Default.Remove(key);
                        }
                     }
                  }
               }

               //once item waste stream/codes get saved, update PropertyValueRel table
               if (wasteStreamChanged)
               {
                  SetPropertyValueRel(itemGUID, "PRODWST", curUser);
               }

            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Upload files from local to blob.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="filePath"></param>
      /// <param name="accountName"></param>
      /// <param name="localPath"></param>
      /// <param name="newFileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string UploadFilesfromLocalToBlob(string container, string filePath, string accountName, string localPath, string newFileName)
      {
         var retrn = "";
         try
         {
            if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true" && !String.IsNullOrWhiteSpace(localPath))
            {
               bool isExists = Directory.Exists(localPath);
               if (!isExists)
                  Directory.CreateDirectory(localPath);
               File.Copy(filePath, localPath + newFileName, true);
               return CreateMD5FromFile(filePath);
            }
            else
            {
               var connectString = GetConnectionString(accountName);

               CloudStorageAccount account = CloudStorageAccount.Parse(connectString);

               CloudBlobClient client = account.CreateCloudBlobClient();

               CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());

               sampleContainer.CreateIfNotExists();

               sampleContainer.SetPermissions(new BlobContainerPermissions()
               {
                  PublicAccess = BlobContainerPublicAccessType.Container
               });
               CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(newFileName);

               using (Stream file = File.OpenRead(filePath))
               {

                  blob.UploadFromStream(file);
                  retrn = CreateMD5FromFile(filePath);
               }

               if (File.Exists(filePath))
               {
                  File.Delete(filePath);
               }

            }
            return retrn;
         }
         catch (Exception ex)
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   convert to md5.
      /// </summary>
      /// <param name="filePath"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CreateMD5FromFile(string filePath)
      {

         byte[] retrievedBuffer = File.ReadAllBytes(filePath);
         var md5Check = System.Security.Cryptography.MD5.Create();
         md5Check.TransformBlock(retrievedBuffer, 0, retrievedBuffer.Length, null, 0);
         md5Check.TransformFinalBlock(new byte[0], 0, 0);

         // Get Hash Value
         byte[] hashBytes = md5Check.Hash;
         return Convert.ToBase64String(hashBytes);

      }

      ///****************************************************************************
      /// <summary>
      ///   Get connection string.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetConnectionString(string account)
      {
         var connectString = string.Empty;

         connectString = ConfigurationManager.GetConnectionString(account);

         if (string.IsNullOrEmpty(connectString))
         {
            throw new ApplicationException(string.Format(Qualanex.QoskCloud.Utility.Constants.Azure_ConnectionString_NotDefined_Format, account));
         }

         return connectString;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }

   ///============================================================================
   /// <summary>
   ///   DataModel for use in API and website views.
   /// </summary>
   ///============================================================================
   public class ImageResult : Areas.Product.IEffectiveDate
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public ImageResult()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~ImageResult()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string FileName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string AltText { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string Url { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ImageType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public DateTime? EffectiveStartDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public DateTime? EffectiveEndDate { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///////////////////////////////////////////////////////////////////////////////
      ///   No methods defined for this class.
      ///////////////////////////////////////////////////////////////////////////////

      #endregion
   }
}
