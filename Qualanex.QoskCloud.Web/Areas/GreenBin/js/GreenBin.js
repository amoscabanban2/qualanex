﻿$(function ()
{
   //*****************************************************************************************
   //*
   //* GreenBin.js
   //*    Copyright (c) 2017 - 2018, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the GreenBin partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************

   var notForProcessing = false;
   var baseUrl = "/GreenBin";
   var table;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   segmentId = "Weight";
   segmentSelector = "#" + segmentId;
   var editMode = false;
   var active = "";
   var requestedGaylord = [];
   var validate = false;
   var binItemId = "";
   var itemGD = "";
   var prdctId = "";
   var lastTextboxChange = "";
   var itm = 0;
   var $firstWasteStreamRow = "";
   var $firstWasteCodeRow = "";
   var $expDate = "";
   var $lotNumber = "";
   var $lastHeaderBeforeCalibrate = "";
   var $lastBodyBeforeCalibrate = "";
   var $lastCountBeforeCalibrate = "";
   var stationId = "";
   var wasteStreamCodes = [];
   var location = "";
   var wasteCode = [];
   var containerImage = false;
   var contentImage = false;
   var weightValid = false;
   var imageCancel = false;
   var lotCancel = false;
   var weightCancel = false;
   var prevLot = "";
   var prevMonthEntered = "";
   var prevYearEntered = "";
   var noLotChange = false;
   var wasteStreamChange = false;
   var emptyCupTare = false;

   // Segment violations - Weight related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_WEIGHT = "The content weight cannot be greater than the product weight, please update the weights.";
   ERR_INVALIDPRODUCTWT = "The product weight is invalid. Please capture or re-enter the weight.";
   ERR_INVALIDCONTENTWT = "The content weight is invalid. Please capture or re-enter the weight.";
   ERR_MISSINGPRODUCTWT = "The product weight is required.";
   ERR_MISSINGCONTENTWT = "The content weight is required.";

   // Segment violations - Quantity related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_INVALIDPACKAGE = "The package count is invalid. Please re-enter the value.";
   ERR_INVALIDINDIVIDUAL = "The individual count is invalid. Please re-enter the value.";
   ERR_INVALIDQTY = "The quantity is invalid. Please re-enter the value.";
   ERR_INVALIDCASE = "The case count is invalid. Please re-enter the value.";
   ERR_INVALIDCASESIZE = "The case size is invalid. Please re-enter the value.";
   ERR_MISSINGPACKAGE = "The package count is required.";
   ERR_MISSINGINDIVIDUAL = "The individual count is required.";
   ERR_MISSINGQTY = "The quantity is required.";
   ERR_MISSINGCASE = "The case count is required.";
   ERR_MISSINGCASESIZE = "The case size is required.";

   // Segment violations - Hardware related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_NOSCALE = "There was an error connecting to the scale.";
   ERR_NOACK = "There was an error communicating with the scale";
   ERR_CALIBRATE = "The scale needs to be calibrated prior to use. Please click on Calibrate Button to start Calibration. When you are finished Please Click on Tare.";

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).ready(function ()
   {
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
      {
         stationId = data;
      });
      stationId = stationId === "" ? getCookie("MachineName") : stationId;
      table = window.dtHeaders("#searchGrid_WrhsSrtDetails", "ordering : false", ["sorting"], []);
      sidebar.init();
      sidebar.beforeOpen(function (d, s)
      {
         location = location === "" ? getMachineLocation().Location : location;

         //do not try to open container in sidebar if the PC is not registered
         if (location === "" || location === undefined)
         {
            return "PC Registration Not Found. Please Contact Administrator.";
         }

         console.log(location);

         return getValidateType(d).WrhsContr.Type === "I"
            ? (location === "3" || location === "4" || location === "5")
               ? ""
               : "Container is not in the correct location"
            : "";
      });
      sidebar.beforeClose(function (d)
      {
         return $(d).find("input[name='Attribute']").val() === "I"
            ? ($(".searchGrid_WrhsSrtDetails-div-click").length === $(".searchGrid_WrhsSrtDetails-div-click.done").length)
            : true;
      });
      sidebar.beforePause(function (d)
      {
         //we can only pause a green bin if the designation has not occured i.e. green bin processing is occuring
         return $(".txtSortBin").length === 0;
      });
      getBinventoryTabDetails(sidebar.getInboundContainer().containerId);
      sidebar.onRequest(function () { updateRequestedField(); });
      sidebar.onBinOpen(function () { getBinventoryTabDetails(sidebar.getInboundContainer().containerId); });
      sidebar.onBinClose(function () { getBinventoryTabDetails(sidebar.getInboundContainer() === undefined ? "" : sidebar.getInboundContainer().containerId); });
      sidebar.onBinPause(function ()
      {
         $("#CMD_CLEAR").click();
         setTimeout(function ()
         {
            getBinventoryTabDetails("");
         }, 150);
      });

      window.addDesignationMethod(function ()
      {
         clearAfterSave();
      });
      window.addInventoryMethod(function ()
      {
         clearAfterSave();
      });

      window.addSelectMethod(function (e)
      {
         //$("#ItemGUID").removeAttr("readonly").removeAttr("disabled");
         $("#searchGrid_WrhsSrtDetails tr.selected").removeClass("selected");
         $("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr").click();
         binventoryRowClick($("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr"));
         productId = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ProductID']").attr("exchange-value");

         if ($(e).hasClass("done"))
         {
            return false;
         }

         binProcessing($(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").attr("exchange-value"), productId);
         //$("#greenbinBody #ItemCount").text($(".searchGrid_WrhsSrtDetails-div-click.done").length + " of " + $(".searchGrid-div-click").length);
         return false;
      });

      window.addUnselectMethod(function (e)
      {
         $("#wait").show();
         $(".formMax .inputEx").val("");
         $("#WrhsContrType").text("");
         $("#GreenBinProcessingDetailsForm").html("<h2> Green Bin Processing</h2>");
         var $html = $("#FormGreenBinProcessingDetailForm").html();
         $($html).insertBefore("#GreenBinProcessingDetailsForm h2");
         $("#FormGreenBinProcessingDetailForm input").val("");
         var newHtml = "<div class='carousel-inner' role='listbox'><div class='item active'><img src='/Areas/Product/Images/Qosk_Logo.jpg' alt='No product image available' style='width:90%' /></div>  </div>";
         $("td#tab-container a[href='#tabs-summary']").click();
         $(".carousel.slide").html(newHtml);
         //$("#greenbinBody #ItemCount").text($(".searchGrid_WrhsSrtDetails-div-click.done").length + " of " + $(".searchGrid-div-click").length);
         $("#wait").hide();
         return true;
      });

      window.addClickGrid_ControlMethod(function (e)
      {
         if (((prdctId !== "" && prdctId !== 0) || itemGD !== "") && $(".txtSortBin").length > 0)
         {
            return false;
         }

         if ($(e).closest("tr").hasClass("MgmtEntity"))
         {
            var $html = $("#FormGreenBinProcessingDetailForm").html();
            $("#GreenBinProcessingDetailsForm").html('');
            $("#GreenBinProcessingDetailsForm").append("<h2> Green Bin Processing</h2>");
            $($html).insertBefore("#GreenBinProcessingDetailsForm h2");
            $("#FormGreenBinProcessingDetailForm input").val("");
            var newHtml = "<div class='carousel-inner' role='listbox'><div class='item active'><img src='/Areas/Product/Images/Qosk_Logo.jpg' alt='No product image available' style='width:90%' /></div>  </div>";
            $(".carousel.slide").html(newHtml);
            $("td#tab-container a[href='#tabs-summary']").click();
            prdctId = "";
            itemGD = "";
            $(e).closest("tr").removeClass("MgmtEntity");
            return false;
         }

         var productId = 0;
         $("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr").removeClass("MgmtEntity");
         return true;
      });

      initialState();
      $("#ItemGUID").removeAttr("disabled").removeAttr("readonly");
   });

   function getCookie(cname)
   {
      var name = cname + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++)
      {
         var c = ca[i];
         while (c.charAt(0) === ' ')
         {
            c = c.substring(1);
         }

         if (c.indexOf(name) === 0)
         {
            return c.substring(name.length, c.length);
         }
      }

      return "";
   }

   function getMachineLocation()
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "machineName=" + stationId + getAntiForgeryToken(),
         async: false,
         url: baseUrl + baseUrl + "/GetMachineLocation",
         success: function (data)
         {
         }
      });
      return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getValidateType(containerId)
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "containerId=" + containerId + getAntiForgeryToken(),
         async: false,
         url: baseUrl + baseUrl + "/GetContainerType",
         success: function (data)
         {
         }
      });
      return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clearAfterSave()
   {
      itemGD = "";
      prdctId = 0;
      getBinventoryTabDetails(sidebar.getInboundContainer().containerId);
      $(".formEx .inputEx").val("");
      var $html = $("#FormGreenBinProcessingDetailForm").html();
      $("#GreenBinProcessingDetailsForm").html('');
      $("#GreenBinProcessingDetailsForm").append("<h2> Green Bin Processing</h2>");
      $($html).insertBefore("#GreenBinProcessingDetailsForm h2");
      $("#FormGreenBinProcessingDetailForm input").val("");
      var newHtml = "<div class='carousel-inner' role='listbox'><div class='item active'><img src='/Areas/Product/Images/Qosk_Logo.jpg' alt='No product image available' style='width:100%' /></div>  </div>";
      $(".carousel.slide").html(newHtml);
      $("td#tab-container a[href='#tabs-summary']").click();
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function updateRequestedField()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
      {
         $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='OutboundContainerID']").html("Requested");
         $(".searchGrid_WrhsSrtDetails-div-click.selected").addClass("Requested");
         requestedGaylord.push({ "itemGuid": $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").attr("exchange-value") });
         $(".searchGrid_WrhsSrtDetails-div-click.selected").click();
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Readonly mode and select first row of grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function initialState()
   {
      setEditMode(false);

      if (!$("#searchGrid tbody tr").hasClass("selected"))
      {
         $("#searchGrid tbody").find("tr:eq(1)").click();
      }

      $("td#tab-container a[href='#tabs-summary']").click();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      if (!getEditMode())
      {
         if ((prdctId !== "" && prdctId !== 0) || itemGD !== "")
         {
            return false;
         }

         $(".formMax .inputEx").val("");
         $("#greenbinBody #Inductor,#greenbinBody #RAorWPT,#greenbinBody #Station,#greenbinBody #DebitMemo,#greenbinBody #WrhsContrID,#greenbinBody #ItemCount,#greenbinBody #Destination").text('');
         $("#WrhsContrType").text("");

         if (sidebar.containers.find(c => c.attribute === 'I' && c.isOpen()) === undefined)
         {
            return false;
         }

         if (requestedGaylord.length > 0)
         {
            requestedGaylord = [];
         }

         getBinventoryTabDetails(binItemId);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getItemsImages(productId, itemGuid)
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "productId=" + productId + "&itemGUID=" + itemGuid + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/GetItemImages",
         async: false,
         success: function (data)
         {
            var $target = $("#GreenBinFormBody .tableEx td.ql-detailimage.ql-full.tdPreview div:first");//$("#GreenBinFormBody");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
      console.log("print");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function ()
   {
      console.log("new");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      console.log("edit");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function ()
   {
      console.log("cancel");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Items associated with bin Id.
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CloseBin", function ()
   {
      binItemId = "";
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Items associated with bin Id.
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getBinventoryTabDetails(binId)
   {
      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "binId=" + binId + "&stationId=" + stationId + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/GetBinventoryDetails",
         async: false,
         success: function (data)
         {
            var $target = $("#GreenBinSummary");
            var $newHtml = $(data);

            if ($newHtml.find(".searchGrid_WrhsSrtDetails-div-click").length > 0)
            {
               binItemId = binId.toUpperCase();
            }
            else
            {
               binItemId = "";
               $(".formMax .inputEx").val("");
            }

            if (requestedGaylord.length > 0)
            {
               $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='ItemGUID']").each(function ()
               {
                  if (requestedGaylord.find(c => c.itemGuid === $(this).attr("exchange-value")) !== undefined)
                  {
                     $(this).closest("tr").addClass("Requested");
                     $(this).closest("tr").find("td[exchange='OutboundContainerID']").html("Requested");
                  }
               });
            }

            $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='Sorted']").closest("td").each(function ()
            {
               if (parseInt($(this).attr("exchange-value")) === 1)
               {
                  $(this).closest("tr").addClass("done");
                  $(this).closest("tr").css("text-decoration", "line-through");
               }
            });

            if ($newHtml.find(".searchGrid_WrhsSrtDetails-div-click.done").length === $newHtml.find(".searchGrid_WrhsSrtDetails-div-click").length)
            {
               $newHtml.find("input#Reconcile_Close").val("Close");
            }

            $target.replaceWith($newHtml);
            $("#wait").hide();
            //window.dtHeaders("#searchGrid", "max-height:360px,ordering: false", ["sorting"], []);
            //$("#searchGrid_filter").css("display", "none");
            //$("#searchGrid_filter .dataTables_scroll table.dataTable thead tr th").removeClass("sorting");
            //$("#ItemGUID").length > 0 ? $("#greenbinBody #ItemGUID").removeAttr('disabled').removeAttr("readonly") : "";
            $("#searchGrid_WrhsSrtDetails_filter").css("display", "none");
            $("#searchGrid_WrhsSrtDetails_filter .dataTables_scroll table.dataTable thead tr th").removeClass("sorting");
            //if ($("#greenbinBody #ItemCount").length > 0)
            //{
            //   $("#greenbinBody #ItemCount").text($(".searchGrid-div-click.done").length + " of " + $(".searchGrid-div-click").length);
            //}

            setTimeout(function ()
            {
               $("#ItemGUID").focus();
            }, 500);
         },
         error: function (data)
         {
            $("#wait").hide();
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        $("#" + field).val(data);
                     }
               }

               break;
            }

         case "SELECT":
            {
               $("#" + field).val(data);
               break;
            }

         case "LABEL":
            {
               //console.log("SELECT::" + field + "::" + data);
               $("#" + field).text(data);
               break;
            }
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Save Role detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".selectEx", function (e)
   {
      if ($(e.target).val() === "CNT")
      {
         $("#divAttachementSegment_Content").css("display", "inline-block");
         $("#divAttachementSegment_Container").css("display", "none");
         $("div#divAttachementSegment_Content form.dropzone").off("click");
         $("#review").removeAttr("src");
      }
      else if ($(e.target).val() === "CTR")
      {
         $("#divAttachementSegment_Content").css("display", "none");
         $("#divAttachementSegment_Container").css("display", "inline-block");
         $("div#divAttachementSegment_Container form.dropzone").off("click");
         $("#review").removeAttr("src");
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Save Role detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   window.OnQnexSave = function ()
   {
      console.log("save");
   }

   ////*****************************************************************************
   ////*
   ////* Summary:
   ////*   click on Binventory Grid.
   ////*
   ////* Parameters:
   ////*   None.
   ////*
   ////* Returns:
   ////*   None.
   ////*
   ////*****************************************************************************
   //$(document).on("click", ".searchGrid_WrhsSrtDetails-div-click", function(e)
   //{
   //   if (((prdctId !== "" && prdctId !== 0) || itemGD !== "")
   //      && $(".txtSortBin").length > 0)
   //   {
   //      return false;
   //   }
   //   if ($(e.target).closest("tr").hasClass("MgmtEntity"))
   //   {
   //      var $html = $("#FormGreenBinProcessingDetailForm").html();
   //      $("#GreenBinProcessingDetailsForm").html('');
   //      $("#GreenBinProcessingDetailsForm").append("<h2> Green Bin Processing</h2>");
   //      $($html).insertBefore("#GreenBinProcessingDetailsForm h2");
   //      $("#FormGreenBinProcessingDetailForm input").val("");
   //      var newHtml = "<div class='carousel-inner' role='listbox'><div class='item active'><img src='/Areas/Product/Images/Qosk_Logo.jpg' alt='No product image available' style='width:90%' /></div>  </div>";
   //      $(".carousel.slide").html(newHtml);
   //      $("td#tab-container a[href='#tabs-summary']").click();
   //      prdctId = "";
   //      itemGD = "";
   //      $(e.target).closest("tr").removeClass("MgmtEntity");
   //      return false;
   //   }

   //   var productId = 0;
   //   $("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr").removeClass("MgmtEntity");
   //   if ($(this).hasClass("selected"))
   //   {
   //      $(this).removeClass("selected");
   //      //$("#greenbinBody #ItemGUID").attr({ "readonly": "readonly", "disabled": "disabled" });
   //      $(".formMax .inputEx").val("");
   //      $("#WrhsContrType").text("");
   //      $("#GreenBinProcessingDetailsForm").html("<h2> Green Bin Processing</h2>");
   //      var $html = $("#FormGreenBinProcessingDetailForm").html();
   //      $($html).insertBefore("#GreenBinProcessingDetailsForm h2");
   //      $("#FormGreenBinProcessingDetailForm input").val("");
   //      var newHtml = "<div class='carousel-inner' role='listbox'><div class='item active'><img src='/Areas/Product/Images/Qosk_Logo.jpg' alt='No product image available' style='width:90%' /></div>  </div>";
   //      $("td#tab-container a[href='#tabs-summary']").click();
   //      $(".carousel.slide").html(newHtml);
   //   }

   //   else
   //   {
   //      $(this).closest("tr").addClass("MgmtEntity");
   //      $(".searchGrid_WrhsSrtDetails-div-click.selected").removeClass("selected");
   //      $(".searchGrid_WrhsSrtDetails-div-click.MgmtEntity").closest("tr").click();
   //      //$("#ItemGUID").removeAttr("readonly").removeAttr("disabled");
   //      productId = $(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ProductID']").attr("exchange-value");
   //      $("#ItemGUID").val($(".searchGrid_WrhsSrtDetails-div-click.selected td[exchange='ItemGUID']").attr("exchange-value"));
   //      if ($(this).hasClass("done"))
   //      {
   //         return false;
   //      }
   //      binProcessing($("#ItemGUID").val(), productId);
   //   }

   //   $("#greenbinBody #ItemCount").text($(".searchGrid_WrhsSrtDetails-div-click.done").length + " of " + $(".searchGrid-div-click").length);
   //});

   //*****************************************************************************
   //*
   //* Summary:
   //*   upload a file and preview.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#uploadFiles", function ()
   {
      //$("input[type='submit']").click();
      $("#preView").html("");
      var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp|.svg|.ico|.tif)$/;

      if (regex.test($(this).val().toLowerCase()))
      {
         if (typeof (FileReader) !== "undefined")
         {
            $("#preView").show();
            $("#preView").append("<img style='max-width:304px;max-height:168px;margin:0px auto' />");
            var reader = new FileReader();
            reader.onload = function (e)
            {
               $("#preView img").attr("src", e.target.result);
            }
            reader.readAsDataURL($(this)[0].files[0]);
            $("#lblFileName").text($(this)[0].files[0].name);
         }
         else
         {
            $(this).setErrorMessage("This browser does not support FileReader.", "Error Message", "ok");
            //alert("This browser does not support FileReader.");
         }
      }
      else
      {
         $("#uploadFiles").val("");
         $(this).setErrorMessage("Please upload a valid image file.", "Error Message", "ok");
         //alert("Please upload a valid image file.");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete Draged images.
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".dz-deletefile img", function (e)
   {
      var imageName = $(e.target).parent().parent().parent().find("div.dz-details div.dz-filename span").html();
      var intrFolder = $(e.target).parent().parent().parent().parent().parent().attr("data-field-name") === "Container" ? "CTR" : "CNT";
      var $targetForm = $(e.target).closest("form");

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "fileName=" + imageName + "&intrFolder=" + intrFolder + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/DeleteSelectedImage",
         success: function (data)
         {
            if (data.Success)
            {
               var div = $(e.target).parent().parent().parent().closest("div");
               div.remove();
               $("#review").removeAttr("src");

               if ($(e.target).closest("form").find("div").length === 0)
               {
                  $targetForm.removeClass("dz-started");
               }
            }
         }
      });

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on thumnail images to see on preview.
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".dz-preview div", function (e)
   {
      $("#review").attr("src", $(e.target).closest(".dz-preview").find(".dz-details div.dz-filename span").attr("data-path-name"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on upload button and open file uploader.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#openUploadFile", function ()
   {
      $("#uploadFiles").trigger("click");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   clear uploaded Image.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#imageClear", function ()
   {
      $("#lblFileName").text("");
      $("#uploadFiles").val("");
      $("#preView img").remove();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Continue.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".buttonEx", function (e)
   {
      var unit = $("#ProcedureCaptureCount").attr("data-field-name");

      if ($(this).attr("data-field-name") === "Weight")
      {
         //if ($("label.Required").length > 0
         //   && $("label.Required").attr("segment-name") === $(this).attr("data-field-name"))
         //{
         //   return false;
         //}
         if ($(this).attr("id") === "weightContinue")
         {
            if ($("tr#Weight").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField"))
            {
               var imageLength = 0;

               $("img.captureImage").not("#Check_Calibration").each(function ()
               {
                  if ($(this).attr("src").indexOf("BoxGreyCheckGreen.svg") > 0)
                  {
                     imageLength++;
                  }
               });

               if ($("tr[data-field-name='" + unit + "']").not(".hidden").length !== imageLength)
               {
                  return false;
               }
            }
         }
         else
         {
            lastTextboxChange = "";
            $("img.captureImage").not("#Check_Calibration").attr("src", "/images/SVG/BoxGrey.svg");
            $("tr[data-field-name='" + unit + "'] label").text("");
            var count = parseFloat($("#WeightItemCount").attr("data-field-name"));

            $("#ScaleCapture").html("");
            $("#ProcedureCaptureCount").text(count + " " + unit);
            $("#ProcedureCaptureCount").attr("data-field-value", count);
            $("#ProcedureCaptureBody").text("Please clear your scale and click Tare.");
            $("#ProcedureCaptureHeader").text($("#" + $("tr[data-field-name='" + unit + "']").not(".hidden").find("label[class='captures']").first().attr("id")).attr("firstline"));
            emptyCupTare = true;
            weightCancel = true;
         }

         OpenNextSegment($(this).attr("data-ql-next"), $(this).attr("data-field-name"), true);
      }
      else if ($(this).attr("data-field-name") === "Container")
      {
         if ($("label.Required").length > 0
            && $("label.Required").attr("segment-name") === $(this).attr("data-field-name")
            && $("#divAttachementSegment_" + $(this).attr("data-field-name") + " div.dz-preview div.dz-details div.dz-filename span").length === 0)
         {
            return false;
         }

         OpenNextSegment($(this).attr("data-ql-next"), $(this).attr("data-field-name"), true);
      }
      else if ($(this).attr("data-field-name") === "Content")
      {
         if ($("div#GreenBinProcessingDetailsForm tr#Content").length > 0
            && $("div[data-field-name='Content'] div.dz-preview").length === 0)
         {
            $(this).setErrorMessage("No file has been uploaded.", "Error Message", "Continue");
         }

         OpenNextSegment($(this).attr("data-ql-next"), $(this).attr("data-field-name"), true);
      }
      else if ($(this).attr("data-field-name") === "Restart")
      {
         $.getJSON("https://localhost:8001/Qualanex/http/Reset", {}, function (data)
         {
            lastTextboxChange = "";
            emptyCupTare = false;
            tareScale();
            $("img.captureImage").attr("src", "/images/SVG/BoxGrey.svg");
            $("#ScaleCapture").html("");
            $("tr[data-field-name='" + unit + "'] label").text("");
            var count = parseFloat($("#WeightItemCount").attr("data-field-name"));
            $("#ProcedureCaptureCount").text(count + " " + unit);
            $("#ProcedureCaptureCount").attr("data-field-value", count);
            $("#ProcedureCaptureBody").text("Please clear your scale and click Tare.");
            $("#ProcedureCaptureHeader").text($("#" + $("tr[data-field-name='" + unit + "']").not(".hidden").find("label[class='captures']").first().attr("id")).attr("firstline"));
         });
      }
      else if ($(this).attr("data-field-name") === "Tare")
      {
         tareScale();
      }
      else if ($(this).attr("data-field-name") === "Capture")
      {
         var redBox = false;
         var weightValid = false;
         var itemLngth = 0;

         $("#IndividualCountWeightResult tr[data-field-name='" + unit + "'] img").each(function ()
         {
            if ($(this).attr("src").indexOf("BoxGreyXRed.svg") > 0)
            {
               if ($(this).closest("tr").find("label").attr("id") !== "ScaleCapture" || $("#ProcedureCaptureCount").attr("data-field-value") === "0")
               {
                  redBox = true;
                  return false;
               }
            }
            else if ($(this).attr("src").indexOf("BoxGreyCheckGreen") > 0)
            {
               itemLngth++;
            }
         });

         if (redBox)
         {
            return false;
         }
         var itemsLength = $("#IndividualCountWeightResult tr[data-field-name='" + unit + "']").not(".hidden").length;

         if (itemLngth === itemsLength)
         {
            return false;
         }

         $.getJSON("https://localhost:8001/Qualanex/http/ScaleCapture", {}, function (result)
         {
            console.log("ScaleCapture:" + result);

            setScaleBusy(false);

            if (result === "")
            {
               itm++;

               if (itm >= 3)
               {
                  CheckCalibration();
                  itm = 0;
               }
               else
               {
                  $("#CMD_SCALECAPTURE1").click();
               }

               return false;
            }

            itm = 0;
            var txtBox = "#ScaleCapture";
            var percentage = $("#ProcedureCaptureCount").attr("data-field-DosageImg") === "L" ? 20 : 10; //relax the threshold on liquids
            individualCountWeight(txtBox, "#ProcedureCaptureBody", "#ProcedureCaptureCount", unit, result, percentage);
         });

         ga("send", "event", "SegmentWtQty", "capture", "captureButton");
      }
      else if ($(this).attr("data-field-name") === "Calibrate")
      {
         CheckCalibration();
         OpenCalibration();
      }
      else if ($(this).attr("data-field-name") === "ImageCancel")
      {
         imageCancel = true;
         $("#imageOption option").each(function ()
         {
            $("#divAttachementSegment_" + $(this).attr("data-field-name") + " #WrhsStagingPendingAttachments div").each(function ()
            {
               var valid = deleteImagesByAddress($(this).find("a img").attr("src"));

               if (valid.Success)
               {
                  $(this).remove();
                  $("#review").removeAttr("src");
               }
            });
         });

         $(".tdMessage #sameCameraErrorText").hide();
         OpenNextSegment($(this).attr("data-ql-next"), "Container", true);
      }
      else if ($(this).attr("data-field-name") === "ImageContinue")
      {
         if ($("tr#Container").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField"))
         {
            var itemCount = 0;
            var cntRequired = $("#divAttachementSegment_Content").attr("data-field-required") === "1";
            var cntrRequired = $("#divAttachementSegment_Container").attr("data-field-required") === "1";
            var cntCount = 0;
            var cntrCount = 0;

            $("#imageOption option").each(function ()
            {
               //check each attachment div for number of attachments (aka child divs)
               var count = $("#divAttachementSegment_" + $(this).attr("data-field-name") + " #WrhsStagingPendingAttachments div").length;

               if (count > 0)
               {
                  itemCount += count;
                  $(this).attr("data-field-name") === "Content" ? cntCount += count : cntrCount += count;
               }
            });

            if ((parseFloat($("#contentFlag").val()) === 1 && $("#divAttachementSegment_Content").length === 0) && $("#divAttachementSegment_Container").length === 0)
            {
               $(".tdMessage #sameCameraErrorText").show();
               return false;
            }

            contentImage = cntRequired ? cntCount > 0 : true;
            containerImage = cntrRequired ? cntrCount > 0 : true;

            if (!contentImage || !containerImage)
            {
               $(".tdMessage #sameCameraErrorText").show();
               return false;
            }
         }

         $(".tdMessage #sameCameraErrorText").hide();
         OpenNextSegment($(this).attr("data-ql-next"), "Container", true);
      }
      else if ($(this).attr("data-field-name") === "WasteStreamSegmentCancel")
      {
         return false;
         //$("#WasteStreamRelation").html($firstWasteStreamRow);
         //$("#WasteCodeRelation").html($firstWasteCodeRow);
         //$(".relationControl").enableRelationEdit();
         //selectFirstRow($("#WasteStreamRelation tr.relationEditRow td select[name='WasteStreamCode']:first").closest("tr").find("td input[type='image']"), "click");
         //OpenNextSegment($(this).attr("data-ql-next"), "WasteStreamSegment", true);
      }
      else if ($(this).attr("data-field-name") === "WasteStreamSegment")
      {
         if ($("#WasteStreamRelation tr.relationEditRow, #WasteStreamRelation tr.updated").length === 0
            || $("#WasteCodeRelation tr.relationEditRow, #WasteCodeRelation tr.updated").length === 0)
         {
            $("#WasteStreamTable #sameWasteErrorText").show();
            return false;
         }

         OpenNextSegment($(this).attr("data-ql-next"), $(this).attr("data-field-name"), true);
      }
      else if ($(this).attr("data-field-name") === "CancelLotNumber")
      {
         lotCancel = true;
         noLotChange = true;

         //original lot entered by user at Induction
         //assign the original lots from induction into the hidden form
         var indLot = $("#LotSelectionInput").val().toUpperCase().trim();
         var indExpMonth = $("#ExpMonthSelectionInput").val();
         var indExpYear = $("#ExpYearSelectionInput").val();

         $(".LotNumberHidden").val(indLot);
         $(".ExpMonthHidden").val(indExpMonth);
         $(".ExpYearHidden").val(indExpYear);
         OpenNextSegment($(this).attr("data-ql-next"), "lotDate", true);
      }
      else if ($(this).attr("data-field-name") === "Quarantine")
      {
         $("#quarantineConfirm").val("true");
         OpenNextSegment($(this).attr("data-ql-next"), $(this).attr("data-field-name"), true);
      }
      var test = true;
   });

   function checkAllChanges(e, seg)
   {
      //********* Image segment
      //if the attachment segment appears, then it's required; otherwise, set to true to bypass an optional image
      containerImage = $("#divAttachementSegment_Container").attr("data-field-required") === "1" ? $("#divAttachementSegment_Container #WrhsStagingPendingAttachments div").length > 0 : true;
      contentImage = $("#divAttachementSegment_Content").attr("data-field-required") === "1" ? $("#divAttachementSegment_Content #WrhsStagingPendingAttachments div").length > 0 : true;

      if ((!containerImage || !contentImage) && !imageCancel && $("tr#Container").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField"))
      {
         return failValidation("Container");
      }

      var unit = $("#ProcedureCaptureCount").attr("data-field-name");

      var imageLength = 0;

      //check for all green check boxes
      $("img.captureImage").not("#Check_Calibration").each(function ()
      {
         if ($(this).attr("src").indexOf("BoxGreyCheckGreen.svg") > 0)
         {
            imageLength++;
         }
      });

      //**********  Individual count weight
      if ($("tr[data-field-name='" + unit + "']").not(".hidden").length === imageLength)
      {
         var dosage = $("#ProcedureCaptureCount").attr("data-field-DosageImg");

         if ((unit === "EA" && dosage === "P") || (unit === "ML" && dosage === "L"))
         {
            // capture individual count weight using FullContent (capture during weight segment) divided by WeightItemCount (package size)
            $("#GreenBinProcessingDetailsForm .IndividualCountWeightHidden").val(parseFloat($("#ScaleCapture").html()) / parseFloat($("#WeightItemCount").val()));

            // since Empty Container isn't captured for pills and liquids, calculate using FullContainerAndContent weight and FullContent weight
            $("#ScaleCaptureEmpty").html(parseFloat($("#ScaleCaptureFull").html()) - parseFloat($("#ScaleCapture").html()));
         }
         else
         {
            //for all other types, calculate the FullContent weight using FullContainerAndContent and EmptyContainer weights, divided by WeightItemCount (package size)
            $("#GreenBinProcessingDetailsForm .IndividualCountWeightHidden").val((parseFloat($("#ScaleCaptureFull").html()) - parseFloat($("#ScaleCaptureEmpty").html())) / parseFloat($("#WeightItemCount").val()));
         }
      }
      else if ($("tr[data-field-name='" + unit + "']").not(".hidden").length !== imageLength && $("tr#Weight").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField") && !weightCancel)
      {
         return failValidation(segmentId); //all check boxes should be marked green if field is required
      }

      //}
      //if ($("tr#WasteStreamSegment").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField"))
      //{
      wasteStreamCodes = [];
      wasteCode = [];
      var wasteStreamVal = false;

      if ($("#WasteStreamRelation .relationAddRow").not(".blank").length > 0 || $("#WasteStreamRelation .relationEditRow").not(".blank").length > 0)
      {
         //check add row (new waste streams)
         $("#WasteStreamRelation .relationAddRow").not(".blank").each(function ()
         {
            wasteStreamCodes.push({ "WasteStream": $(this).find("td input[name='WasteCode']").val() });
            wasteStreamVal = true;
         });

         //check edit row (existing waste streams)
         $("#WasteStreamRelation .relationEditRow").not(".blank").each(function ()
         {
            wasteStreamCodes.push({ "WasteStream": $(this).find("td input[name='WasteCode']").val() });
            wasteStreamVal = true;
         });
      }

      if ($("#WasteCodeRelation .relationAddRow").not(".blank").length > 0 || $("#WasteCodeRelation .relationEditRow").not(".blank").length > 0)
      {
         $("#WasteCodeRelation .relationAddRow").not(".blank").each(function ()
         {
            if (!wasteStreamVal && wasteStreamCodes.find(c => c.WasteStream === $(this).find("td input[name='WasteStreamCode']").val()) === undefined)
            {
               wasteStreamCodes.push({ "WasteStream": $(this).find("td input[name='WasteStreamCode']").val() });
            }

            wasteCode.push({
               "WasteStream": $(this).find("td input[name='WasteStreamCode']").val(),
               "WasteCode": $(this).find("td input[name='WasteStream']").val()
            });
         });
         $("#WasteCodeRelation .relationEditRow").not(".blank").each(function ()
         {
            if (!wasteStreamVal && wasteStreamCodes.find(c => c.WasteStream === $(this).find("td input[name='WasteStreamCode']").val()) === undefined)
            {
               wasteStreamCodes.push({ "WasteStream": $(this).find("td input[name='WasteStreamCode']").val() });
            }

            wasteCode.push({
               "WasteStream": $(this).find("td input[name='WasteStreamCode']").val(),
               "WasteCode": $(this).find("td input[name='WasteStream']").val()
            });
         });
      }

      //}
      //**********************

      //**********  Lot Number
      if (!compareLots() && !lotCancel)
      {
         $(".txtLotNo").focus();
         return failValidation(segmentId);
      }

      saveGreenBinInfo(seg);
   }

   function saveGreenBinInfo(seg)
   {
      if ($("tr#WasteStreamSegment").closest("tbody").find("tr:first").find("span.RequiredField").length > 0
         && !($("#WasteStreamRelation tr").hasClass("relationEditRow") || $("#WasteStreamRelation tr").hasClass("updated")))
      {
         return failValidation("WasteStreamSegment");
      }

      //For a required segment, this variable will be changed by the waste stream/waste code change events
      //In a case where we send the same product into one bin with missing waste stream, the waste stream will
      //be filled for ALL item guids when the first item's waste stream is updated. In that case, the user won't
      //necessarily have to set a waste stream for the same product even if the waste segment is required.
      if ($("tr#WasteStreamSegment").closest("tbody").find("tr:first").find("span.RequiredField").length > 0)
      {
         wasteStreamChange = true;
      }

      var itemEntities = {
         ItemGUID: itemGD,
         ProductID: prdctId,
         LotNumber: $("#GreenBinProcessingDetailsForm .LotNumberHidden").val(),
         ExpMonth: $("#GreenBinProcessingDetailsForm .ExpMonthHidden").val(),
         ExpYear: $("#GreenBinProcessingDetailsForm .ExpYearHidden").val(),
         LotChanged: !noLotChange,
         WasteSegmentChanged: wasteStreamChange,
         IndvWeight: $("#GreenBinProcessingDetailsForm .IndividualCountWeightHidden").val(),
         ContainerImagesCount: containerImage ? 1 : 0,
         ContentImagesCount: contentImage ? 1 : 0,
         FullContainerWeight: parseFloat($("#ScaleCaptureFull").html()),
         EmptyContainerWeight: parseFloat($("#ScaleCaptureEmpty").html()),
         UnitOfMeasure: $("#ProcedureCaptureCount").attr("data-field-name"),
         Quarantine: $("#quarantineConfirm").val()
      };

      var cancellation = lotCancel ? "Lot," : "";
      cancellation += weightCancel ? "Weight," : "";
      cancellation += imageCancel ? "Image," : "";

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "wasteCode=" + JSON.stringify(wasteCode)
         + "&wasteStream=" + JSON.stringify(wasteStreamCodes)
         + "&itemEntities=" + JSON.stringify(itemEntities)
         + "&cancellation=" + cancellation
         + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/SaveGreenBinProcessing",
         success: function (data)
         {
            $("#GreenBinProcessingDetailsForm .GreenBinReasonHidden").val(data.GreenBinReason);

            if (data.GreenBinReason === "0")
            {
               submitPolicy("", seg);
               requestedGaylord = [];
            }
            else
            {
               $(this).setErrorMessage("The item still has green bin reasons that need to be addressed. Please reprocess the item.", "Error Message", "OK");
            }
         },
         error: function (error)
         {
            $(this).setErrorMessage("The controller call failed on item save request.", "Error Message", "OK");
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change WasteStream DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "select[name='WasteStreamCode']", function ()
   {
      $("#WasteStreamTable #sameWasteErrorText").hide();
      var changedValue = this.value; //get the newly-selected Waste Stream value
      wasteStreamChange = true;
      var wasteStreamCode = $(this).closest("tr").find("td input[name='WasteCode']").val(); //get the value of the currently-selected waste strean from the Waste Stream DDL

      //dynamically load the waste code list based on the waste stream chosen
      //for inner products, use the outer NDC to get the waste codes
      getWasteCode(this.value, $(".NdcInnerPackHidden").val() === "True" ? $(".RollupProductId").val() : prdctId);
      
      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow(); //remove the DDL that was deleted

         $("#WasteStreamRelation select[name='WasteStreamCode'] option").each(function ()
         {
            if ($(this).val() === wasteStreamCode)
            {
               $(this).show();
            }
         });
      }
      else if (wasteStreamCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name='WasteCode']").val(changedValue);

         $("#WasteStreamRelation select[name='WasteStreamCode'] option").each(function ()
         {
            if ($(this).val() === wasteStreamCode)
            {
               $(this).show();
            }
            else if ($(this).val() === "Add")
            {
               $(this).hide();
            }

            if (!$(this).is(":selected") && $(this).val() === changedValue)
            {
               $(this).hide();
            }
         });

         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").attr("name", changedValue);
         $(this).closest("tr").find("input[type='image']").addClass("image_waste_relation");
      }

      //when selecting a waste stream from ddl, remove the waste codes from the previously-chosen waste stream
      $("#WasteCodeRelation table tbody tr td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']").closest("tr").each(function ()
      {
         $(this).deleteRelationRow();
      });

      $("#WasteStreamRelation table tbody tr").removeClass("selected");
      $(this).closest("tr").addClass("selected");

      if (wasteStreamCode === "Add")
      {
         $(this).find("option:eq(0)").prop("selected", true);
         return false;
      }

      if (changedValue === "Delete" || changedValue === "")
      {
         if ($("#WasteStreamRelation table tbody tr td input[name='WasteStreamCode']:first").closest("tr").length > 0)
         {
            selectFirstRow($("#WasteStreamRelation table tbody tr td input[name='WasteStreamCode']:first").closest("tr").find("input.image_waste_relation").closest("input"), "click");
         }
      }
      else
      {
         if ($("#WasteStreamRelation table tbody tr").hasClass("selected"))
         {
            $(this).closest("tr").find("input[type=image]").click();
         }
         else
         {
            $("#WasteCodeRelation table tbody tr").removeClass("selected");
            $("#WasteCodeRelation table tbody").hide();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Waste Code DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "select[name='WasteCode']", function ()
   {
      var changedValue = this.value; //the newly-selected value
      wasteStreamChange = true;
      var wasteStream = $("#WasteStreamRelation table tr.selected").find("td input[name='WasteCode']").val(); //get the value of the currently-selected waste stream from the Waste Stream DDL
      var wasteCode = $(this).closest("tr").find("td input[name=WasteStream]").val(); //get the value of the currently-selected waste code from the Waste Code DDL
      wasteStream = wasteStream === undefined || wasteStream === ""
         ? ($("#WasteStreamRelation tr").hasClass("selected")
            ? $("#WasteStreamRelation tr.selected td select[name='WasteCode']").val()
            : $(this).find("option:eq(0)").prop("selected", true))
         : wasteStream;

      if (changedValue === "Delete")
      {
         $(this).deleteRelationRow(); //delete the DDL where the Delete was selected

         $("#WasteCodeRelation select[name='WasteCode'] option").each(function ()
         {
            if ($(this).val() === wasteCode)
            {
               $(this).show();
            }
         });
      }
      else if (wasteCode !== changedValue)
      {
         $("#WasteCodeRelation select[name='WasteCode'] option").each(function ()
         {
            if ($(this).val() === wasteCode)
            {
               $(this).show(); //make the previous value show up in the DDLs
            }
            else if ($(this).val() === "Add")
            {
               $(this).hide();
            }

            //hide the selected value from other DDLs
            if ($(this).is(":selected") === false && $(this).val() === changedValue)
            {
               $(this).hide();
            }
         });

         $(this).closest("tr").find("td input[name=WasteStream]").attr("value", changedValue);
         $(this).closest('tr').find('td input[name=WasteStreamCode]').attr("value", wasteStream);
         $(this).closest("select").find("option[value='Delete']").show();
      }
      if (changedValue === "Delete" || changedValue === "")
      {
         if ($("#WasteStreamRelation table tbody tr td input[name='WasteCode'][Value='" + wasteStream + "']").closest("tr").length > 0)
         {
            selectFirstRow($("#segmentRelationControlItems table tbody tr td input[name='WasteCode'][Value='" + wasteStream + "']").closest("tr").find("input.image_waste_relation").closest("input"), "click");
         }
      }
      else
      {
         $("#WasteCodeRelation table tr").removeClass("selected");
         $(this).closest("tr").addClass("selected");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Green Bin Processing Lot Segment - compare values to item processed and/or Lot table
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   True - Lot/Exp Date entered matches processed item (Required Field), lot/exp date was voluntarily changed, (Optional Field), or no updates to the lot segment (Optional Field)
   //*   False - mismatches on Lot/Exp Date entered and Item Processed
   //*
   //*****************************************************************************
   function compareLots()
   {
      $("#lotNumberVerificationWarning #lotNumberEntryWarning").hide();

      // no need to compare if we're bypassing the segment
      if (lotCancel)
      {
         return true;
      }

      //new lot just entered by user at Green Bin Processing
      var lotEntered = $(".txtLotNo").val().toUpperCase().trim();
      var expMonthEntered = $(".cboExpMonth").val();
      var expYearEntered = $(".txtExpYear").val();

      //original lot entered by user at Induction
      var indLot = $("#LotSelectionInput").val().toUpperCase().trim();
      var indExpMonth = $("#ExpMonthSelectionInput").val();
      var indExpYear = $("#ExpYearSelectionInput").val();

      //Required field - all three fields must be filled
      if ($("tr#lotDate").closest("tbody").find("td.QnexHeader span").hasClass("RequiredField"))
      {
         if (lotEntered === "")
         {
            $("#missingLotNumberError").show();
            return false;
         }

         if (expMonthEntered === "" || expYearEntered === "")
         {
            $("#missingExpirationDateError").show();
            return false;
         }

         if (lotEntered !== prevLot || expMonthEntered !== prevMonthEntered || expYearEntered !== prevYearEntered)
         {
            if (lotEntered !== indLot || expMonthEntered !== indExpMonth || expYearEntered !== indExpYear)
            {
               prevLot = lotEntered;
               prevMonthEntered = expMonthEntered;
               prevYearEntered = expYearEntered;
               $("#LotSelection").html("");
               $("#ExpSelection").html("");
               $(".txtLotNo").val("");
               $(".cboExpMonth").val("");
               $(".txtExpYear").val("");
               $("#lotNumberVerificationWarning").show();
               $(".txtLotNo").focus();
               return false;
            }
         }

         // Update hidden values used for DB save function
         $(".LotNumberHidden").val($(".txtLotNo").val().toUpperCase());
         $(".ExpMonthHidden").val($(".cboExpMonth").val());
         $(".ExpYearHidden").val($(".txtExpYear").val());

         $("#lotNumberVerificationWarning").hide();
         return true;
      }
      //Optional field - either no fields or all fields filled
      else if ($("tr#lotDate").closest("tbody").find("td.QnexHeader span").hasClass("OptionalField"))
      {
         $("#lotNumberEntryWarning").show();

         if (lotEntered !== "" && expMonthEntered !== "" && expYearEntered !== "")
         {
            if (lotEntered !== prevLot || expMonthEntered !== prevMonthEntered || expYearEntered !== prevYearEntered)
            {
               if (lotEntered !== indLot || expMonthEntered !== indExpMonth || expYearEntered !== indExpYear)
               {
                  prevLot = lotEntered;
                  prevMonthEntered = expMonthEntered;
                  prevYearEntered = expYearEntered;
                  $("#LotSelection").html("");
                  $("#ExpSelection").html("");
                  $(".txtLotNo").val("");
                  $(".cboExpMonth").val("");
                  $(".txtExpYear").val("");
                  $("#lotNumberEntryWarning").show();
                  noLotChange = false;
                  $(".txtLotNo").focus();
                  return false;
               }

               //we do not want to make a lot save each time, even if we choose to enter a lot that's already tied to the item GUID
               //this gets looked up during the Lot segment save in GreenBinModel
               noLotChange = true;
            }

            $(".LotNumberHidden").val($(".txtLotNo").val().toUpperCase());
            $(".ExpMonthHidden").val($(".cboExpMonth").val());
            $(".ExpYearHidden").val($(".txtExpYear").val());
         }
         else //if one of these is not empty, it's assumed a new lot was to be entered
         {
            if (lotEntered === "" && (expMonthEntered !== "" || expYearEntered !== ""))
            {
               $("#missingLotNumberError").show();
               return false;
            }

            if (lotEntered !== "" && (expMonthEntered === "" || expYearEntered === ""))
            {
               $("#missingExpirationDateError").show();
               return false;
            }

            //we do not want to make a lot save each time, even if we choose not to enter a lot to save
            //this gets looked up during the Lot segment save in GreenBinModel
            noLotChange = true;
         }

         $("#lotNumberEntryWarning").hide();
         return true;
      }

      return false; //error if it gets here
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function deleteImagesByAddress(address)
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         async: false,
         data: "address=" + address + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/DeleteSelectedImageAddress"
      });

      return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setScaleBusy(isBusy)
   {
      // disable individual count weight segment for Opened or Unit Dose products (UnitsPerPackage > 1)
      if ($("input#weightOpenUnitDose").val() === "1")
      {
         isBusy = true;
      }

      $("#CMD_SCALETARE1").prop("disabled", isBusy);
      $("#CMD_SCALECAPTURE1").prop("disabled", isBusy);
      $("#CMD_SCALERESTART").prop("disabled", isBusy);
      $("#CMD_SCALECALIBRATE").prop("disabled", isBusy);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var showWtQtyViolation = function (serverity, violation)
   {
      var messageObj = $(segmentSelector + " .tdMessage .tdCaption");

      // Clear any old messages and/or violations
      ///////////////////////////////////////////////////////////////////////////////
      messageObj.removeClass("error warning general");
      messageObj.text("");

      // Display the message and/or violation
      ///////////////////////////////////////////////////////////////////////////////
      switch (serverity)
      {
         case SEG_GENERAL:
            messageObj.addClass("general");
            $("#CMD_SCALECALIBRATE").removeAttr("disabled");
            break;

         case SEG_WARNING:
            messageObj.addClass("warning");
            $("#CMD_SCALECALIBRATE").removeAttr("disabled");
            break;

         case SEG_ERROR:
            messageObj.addClass("error");
            $("#CMD_SCALECALIBRATE").removeAttr("disabled");
            break;
      }

      messageObj.text(violation);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.showWtQtyViolation = showWtQtyViolation;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Close or open segments.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function OpenNextSegment(next, Current, closeCurrent)
   {
      if (Current.length > 0)
      {
         var $attrCurrent = $("img.collapse-arrow[data-target='#" + Current + "']").attr("src");

         if ($attrCurrent !== undefined && $attrCurrent.indexOf("Qosk_Arrow_Up.png") > 0 && closeCurrent)
         {
            $("img.collapse-arrow[data-target='#" + Current + "']").closest("img").trigger("click");
         }
      }

      var nextItem = next;

      if ($("#GreenBinProcessingDetailsForm div.QoskSegment .RequiredField").length > 0)
      {
         if ($("#GreenBinProcessingDetailsForm div.QoskSegment tr.panel-collapse#" + Current + "").closest("div.QoskSegment").nextAll("div.QoskSegment").find("td.QnexHeader .RequiredField").closest("tbody").find("tr.panel-collapse").length > 0)
         {
            nextItem = "#" + $("#GreenBinProcessingDetailsForm div.QoskSegment tr.panel-collapse#" + Current + "").closest("div.QoskSegment").nextAll("div.QoskSegment").find("td.QnexHeader .RequiredField").closest("tbody").find("tr.panel-collapse").first().attr("id");
         }

         if (nextItem !== next)
         {
            $("#GreenBinProcessingDetailsForm div.QoskSegment tr.panel-collapse#" + Current + "").closest("div.QoskSegment")
               .nextAll("div.QoskSegment").find("tr.panel-collapse").closest("tr").each(function ()
               {
                  if (nextItem === "#" + $(this).attr("id"))
                  {
                     next = nextItem;
                     return false;
                  }
                  else
                  {
                     $attrCurrent = $("img.collapse-arrow[data-target='#" + $(this).attr("id") + "']").attr("src");

                     if ($attrCurrent.indexOf("Qosk_Arrow_Up.png") > 0)
                     {
                        $("img.collapse-arrow[data-target='#" + $(this).attr("id") + "']").closest("img").trigger("click");
                     }
                  }
               });
         }
      }

      if (next === "#binDesignation")
      {
         checkAllChanges("", $("tr#" + Current + " input[value='Continue']").closest("input"));
      }

      var $attrNext = $("img.collapse-arrow[data-target='" + next + "']").attr("src");

      if ($attrNext !== undefined && $attrNext.indexOf("Qosk_Arrow_Down.png") > 0)
      {
         $("img.collapse-arrow[data-target='" + next + "']").closest("img").trigger("click");
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function binProcessing(itemGuid, productId)
   {
      itemGD = itemGuid;
      prdctId = productId;
      $("#processing-loading").show();
      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "itemGUID=" + itemGuid + "&productId=" + productId + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/GetBinProcessingInfo",
         async: true,
         success: function (data)
         {
            var $target = $("#GreenBinProcessingDetailsForm");
            var $newHtml = $(data);
            $newHtml.find("tr#lotDate div.panel-body:first").css("padding", "4px");
            var optionalSegmentCount = 0;
            var lotSegmentHtml = $newHtml.find("#LotSelection").closest("tr");

            //collapse all optional fields
            $newHtml.find("span.OptionalField").closest("tr").each(function ()
            {
               $(this).find("td.tdMarginR img.collapse-arrow").click();
               optionalSegmentCount++;
            });

            //remove additional waste stream ddl's (dropdown lists) if the product already has a waste stream
            if ($newHtml.find("#WasteStreamRelation table tbody .relationEditRow").length > 0)
            {
               $newHtml.find("#WasteStreamRelation table tbody .relationAddRow").each(function ()
               {
                  $(this).deleteRelationRow();
               });
            }

            $firstWasteCodeRow = $newHtml.find("#WasteCodeRelation").html();
            $firstWasteStreamRow = $newHtml.find("#WasteStreamRelation").html();
            $lastBodyBeforeCalibrate = $newHtml.find("#ProcedureCaptureBody").html();
            $lastCountBeforeCalibrate = $newHtml.find("#ProcedureCaptureCount").html();
            $lastHeaderBeforeCalibrate = $newHtml.find("#ProcedureCaptureHeader").html();
            $expDate = $newHtml.find("div.QnexExpDate").html();
            $lotNumber = $newHtml.find(".QnexLot div#divLotNo").html();
            $target.replaceWith($newHtml);
            $("#tab-container a[href='#tabs-processing']").closest("a").click();
            $("#greenBinImageCapture").removeAttr("disabled");
            getItemsImages(prdctId, itemGD);
            lastTextboxChange = "";
            $("#ScaleCapture").html(""); // clear previously-captured weight field
            emptyCupTare = false; // clear tare step check
            checkScaleConditionGreenBin();
            selectFirstRow($("#WasteStreamRelation tr.relationEditRow td select[name='WasteStreamCode']:first").length === 0 ?
               $("#WasteStreamRelation tr td input[type='image']:first").click()
               : $("#WasteStreamRelation tr.relationEditRow td select[name='WasteStreamCode']:first").closest("tr").find("td input[type='image']"), "click");

            $("#GreenBinProcessingDetailsForm div.QoskSegment span.OptionalField").closest("tr").each(function ()
            {
               $(this).find("td.tdMarginR img.collapse-arrow").closest("img").click();
            });

            //disable the lot segment for manufacturers that require to give approval before adding lots
            if ($newHtml.find("input#lotManufacturer").val() === "1")
            {
               $(".QoskSegment #lotDateTable").find("td:first").find("input,select").attr({ "disabled": "disabled" }, { "readonly": "readonly" });
            }

            // disable individual count weight segment for Opened or Unit Dose products (UnitsPerPackage > 1)
            if ($newHtml.find("input#weightOpenUnitDose").val() === "1")
            {
               $(".QoskSegment #individualCountWeightTable").find("td:first").find("input,select").attr({ "disabled": "disabled" }, { "readonly": "readonly" });
            }

            //disable the waste stream segment if it's not required
            if ($("tr#WasteStreamSegment").closest("tbody").find("tr:first").find("span.OptionalField").length > 0)
            {
               $(".QoskSegment #WasteStreamTable").find("td:first").find("input,select").attr({ "disabled": "disabled" }, { "readonly": "readonly" });
            }

            // clear lot segment fields
            $("#btnNoExp").hide();
            $("#btnReset").hide();
            $("#LotSelection").html("");
            $("#ExpSelection").html("");
            prevLot = "";
            prevMonthEntered = "";
            prevYearEntered = "";
            noLotChange = false;
            imageCancel = false;
            lotCancel = false;
            weightCancel = false;
            wasteStreamChange = false;
            $(".txtLotNo").val("");
            $(".cboExpMonth").val("");
            $(".txtExpYear").val("");
            $("#processing-loading").hide();
            $("#wait").hide();

            // check that all segments above Lot are optional (therefore collapsed) and that lot is required and quarantine segment is not
            // there (because quarantine does not show up in a collapsed state). If this is true lot is the first required segment and should have focus
            if (optionalSegmentCount >= 3 && $(lotSegmentHtml).find("span.RequiredField").length > 0 && $newHtml.find("tr#Quarantine").length === 0)
            {
               setTimeout(function()
               {
                  $(".txtLotNo").focus();
               }, 300);
            }
         },
         error: function (data)
         {
            $("#wait").hide();
            $("#CMD_CLEAR").click();
            $(this).setErrorMessage("The controller call failed on Green Bin Processing lookup.", "Error Message", "OK");
         }
      });
   }

   function tareScale()
   {
      setScaleBusy(true);
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleTare", {}, function (result)
      {
         setScaleBusy(false);

         if ($.parseJSON(result))
         {
            var unit = $("#ProcedureCaptureCount").attr("data-field-name");

            // do special tare check only if prompted for an empty cup
            if (emptyCupTare)
            {
               checkTare("#ScaleCapture", "#ProcedureCaptureBody", unit, result);
            }

            $(".tdMessage td.tdCaption.warning").html("");
            $("#CMD_SCALECALIBRATE").attr("disabled", "disabled");

            if ($("#Check_Calibration").attr("src").indexOf("BoxGreyCheckGreen.svg") < 0)
            {
               $("img.captureImage").not("#Check_Calibration").attr("src", "/images/SVG/BoxGrey.svg");
               $("#Check_Calibration").attr("src", "/images/SVG/BoxGreyCheckGreen.svg");
               $("tr[data-field-name='" + unit + "'] label").text("");

               var count = parseFloat($("#WeightItemCount").attr("data-field-name"));
               $("#ProcedureCaptureCount").text(count + " " + unit);
               $("#ProcedureCaptureCount").attr("data-field-value", count);
               $("#ProcedureCaptureBody").text("Please clear your scale and click Tare.");
               emptyCupTare = true; // Tare step, check if done correctly
               $("#ProcedureCaptureHeader").text($("#" + $("tr[data-field-name='" + unit + "']").not(".hidden").find("label[class='captures']").first().attr("id")).attr("firstline"));
               $lastBodyBeforeCalibrate = "";
               $lastCountBeforeCalibrate = "";
               $lastHeaderBeforeCalibrate = "";
               $("#ProcedureCaptureCount").attr("style", "visibility:hidden");
            }
         }
         else
         {
            window.showWtQtyViolation(SEG_WARNING, ERR_CALIBRATE);
         }

         CheckCalibration();
      });

      ga("send", "event", "SegmentWtQty", "tare", "tareButton");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Load waste code list based on the waste stream selected
   //*
   //* Parameters:
   //*   None
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getWasteCode(wasteStreamCode, productId)
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "wasteStreamCode=" + wasteStreamCode + "&productId=" + productId + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/GetWasteCodesFromWasteStream",
         async: false,
         success: function (data)
         {
            $("#WasteCodeRelation").find("script").remove();
            var $target = $("#WasteCodeRelation").find("table").parent(); //Relation Control wraps our table in its own div, hence the parent() call
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            $(".relationControl").enableRelationEdit();
         },
         error: function (data)
         {
            
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".image_waste_relation", function (e)
   {
      var $sectionId = $(e.target).closest("table.relationControl").parent().parent().attr("id");
      $("#" + $sectionId + " tr").removeClass("selected");
      $(e.target).closest("tr").addClass("selected");
      var wasteStreamCode = $(e.target).attr("name");

      if ($sectionId === "WasteStreamRelation")
      {
         if (wasteStreamCode === undefined || wasteStreamCode === "")
         {
            $("#WasteCodeRelation table tbody").hide();
            $(e.target).closest("tr").addClass("selected");
            return false;
         }

         $("#WasteCodeRelation table tbody").show();
         $(this).closest("tr").addClass("selected");
         setOptionWasteCode();
         //wasteStreamCode = $(e.target).closest("tr").find("td select[name='WasteStreamCode'] option:selected").val();
         //$("#WasteCodeRelation table.relationControl tbody").hide();
         //$("#WasteCodeRelation table.relationControl tr td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']").closest("tr").show();
         //$("#WasteCodeRelation table.relationControl tbody").show();
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Load option for each wasteCode.
   //*
   //* Parameters:
   //*   None
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setOptionWasteCode()
   {
      var wasteCodeObjects = [];
      var $items = $("#WasteRelHidden tr");

      if ($("#WasteStreamRelation table tbody tr").hasClass("selected"))
      {
         $("#WasteCodeRelation table tbody").show();
         var wasteStreamCode = $("#WasteStreamRelation table tbody tr.selected").find("td select[name=WasteStreamCode]").val();
         var $wasteCodeItems = $items.find("td[data-field-name='" + wasteStreamCode + "']").closest("td");
         $("#WasteCodeRelation table tbody tr.relationEditRow").hide();

         $("#WasteCodeRelation table tbody tr td input[name='WasteStreamCode']").each(function ()
         {
            if ($(this).closest("input").val() === wasteStreamCode)
            {
               var element = {};
               element.WasteCode = $(this).closest("tr").find("td select[name=WasteCode]").val();
               wasteCodeObjects.push(element);
               $(this).closest("tr").show();
            }
            else
            {
               $(this).closest("tr").hide();
            }
         });
         $("#WasteCodeRelation table tbody tr.updated td input[name='WasteStreamCode']").each(function ()
         {
            if ($(this).val() !== wasteStreamCode)
            {
               $(this).closest("tr").hide();
            }
         });
         $("#WasteCodeRelation table tbody tr.updated td input[name='WasteStreamCode']").each(function ()
         {
            if ($(this).closest("input").val() === wasteStreamCode)
            {
               var element = {};
               element.WasteCode = $(this).closest("tr").find("td select[name=WasteCode]").val();
               wasteCodeObjects.push(element);
               $(this).closest("tr").show();
            }
            else
            {
               $(this).closest("tr").hide();
            }
         });
         if ($wasteCodeItems.length > 0)
         {
            $("#WasteCodeRelation table tbody tr.relationEditRow  td input[name='WasteStreamCode']")
               .closest("td").find("input[value = '" + wasteStreamCode + "']")
               .closest("tr").find("select[name=WasteCode] option").each(function ()
               {
                  if ($(this).val() !== "Add")
                  {
                     var itemVal = $(this).val();
                     $wasteCodeItems.each(function ()
                     {
                        if ($(this).attr("data-field-value") === itemVal)
                        {
                           $(this).show();
                        }
                     });

                     for (var i = 0; i < wasteCodeObjects.length; i++)
                     {
                        if (itemVal === wasteCodeObjects[i].WasteCode && $(this).closest("select").val() !== itemVal)
                        {
                           $(this).hide();
                        }
                     }
                  }
               });

            $("#WasteCodeRelation table tbody tr.relationAddRow.blank td select[name=WasteCode]").closest("tr").show();
            $("#WasteCodeRelation table tbody tr.blank td select[name=WasteCode] option").hide();
            $("#WasteCodeRelation table tbody tr.blank td select[name=WasteCode] option").each(function ()
            {
               if ($(this).val() !== "Add")
               {
                  var indexItem = $(this).index();
                  var itemVal = $(this).val();
                  var $option = $(this);
                  $wasteCodeItems.each(function ()
                  {
                     if (itemVal === $(this).attr("data-field-value"))
                     {
                        $option.show();
                     }
                  });
                  for (var i = 0; i < wasteCodeObjects.length; i++)
                  {
                     if ($(this).val() === wasteCodeObjects[i].WasteCode)
                     {
                        $(this).hide();
                     }
                  }
               }
            });
         }
         else
         {
            $("#WasteCodeRelation table tbody tr.relationEditRow  td input[name='WasteStreamCode']")
               .closest("td").find("input[value = '" + wasteStreamCode + "']")
               .closest("tr").find("select[name=WasteCode] option").each(function ()
               {
                  if ($(this).val() !== "Add")
                  {
                     $(this).show();

                     for (var i = 0; i < wasteCodeObjects.length; i++)
                     {
                        if ($(this).val() === wasteCodeObjects[i].WasteCode && $(this).closest("select").val() !== $(this).val())
                        {
                           $(this).hide();
                        }
                     }
                  }
               });
            $("#WasteCodeRelation table tbody tr.relationAddRow.blank td select[name=WasteCode]").closest("tr").show();
            $("#WasteCodeRelation table tbody tr.blank td select[name=WasteCode] option").show();
            $("#WasteCodeRelation table tbody tr.blank td select[name=WasteCode] option").each(function ()
            {
               if ($(this).val() !== "Add")
               {
                  for (var i = 0; i < wasteCodeObjects.length; i++)
                  {
                     if ($(this).val() === wasteCodeObjects[i].WasteCode)
                     {
                        $(this).hide();
                     }
                  }
               }
            });
         }

         $("#WasteCodeRelation table tr.blank select[name='WasteCode'] option").each(function ()
         {
            if ($(this).val() === "Delete")
            {
               $(this).hide();
            }
         });

         $("#WasteCodeRelation table tbody tr").removeClass("selected");

         if ($("#WasteCodeRelation tr.relationAddRow.updated td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']:first").closest("tr").length > 0)
         {
            selectFirstRow($("#WasteCodeRelation tr.relationAddRow.updated td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']:first").closest("tr"), "");
         }
         else if ($("#WasteCodeRelation tr.relationEditRow td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']:first").closest("tr").length > 0)
         {
            selectFirstRow($("#WasteCodeRelation tr.relationEditRow td input[name='WasteStreamCode'][value='" + wasteStreamCode + "']:first").closest("tr"), "");
         }
      }
      else
      {
         $("#WasteCodeRelation table tbody").hide();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_CLEAR", function ()
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click").length > 0 && $(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected") && $(".txtSortBin").length === 0)
      {
         //$(".searchGrid_WrhsSrtDetails-div-click.selected").closest("tr").click();
         binventoryRowClick($(".searchGrid_WrhsSrtDetails-div-click.selected").closest("tr"));
         itemGD = "";
         prdctId = "";
         setTimeout(function ()
         {
            $("#ItemGUID").focus();
         }, 400);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SUBMIT", function ()
   {
      if ($("#GreenBinProcessingDetailsForm div:not(#FormGreenBinProcessingDetailForm).QoskSegment").length > 1)
      {
         checkAllChanges();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function individualCountWeight(txtCurrent, alertBody, alertCount, unit, weight, percentage)
   {
      // cannot continue if weight = 0 or if user is on tare step
      if (weight === "" || parseFloat(weight) === 0 || emptyCupTare === true)
      {
         return false;
      }

      var itemId = $(txtCurrent).text() !== "" && $(txtCurrent).closest("tr").find("img").attr("src").indexOf("BoxGreyXRed.svg") > 0
         ? txtCurrent
         : getItemId(unit, txtCurrent);

      //WeightItemCount is the product package size
      //determine the valid weight range via (full content weight / package size)
      var itemRange = $(txtCurrent).text() !== ""
         ? (parseFloat($(txtCurrent).text()) / parseFloat($("#WeightItemCount").val()))
         : (parseFloat(weight) / parseFloat($("#WeightItemCount").val()));
      var range = (itemRange * percentage) / 100;
      var maxRange = itemRange + range;
      var minRange = itemRange - range;

      if (itemId === "" || itemId === undefined)
      {
         return false;
      }

      // used for RNG below
      // for any liquids with a package size greater than 100 MLs, limit the random # to 100; otherwise, use package size
      var pkg = parseFloat($("#WeightItemCount").val()) > 100 && $("#ProcedureCaptureCount").attr("data-field-DosageImg") === "L"
         ? 99
         : parseFloat($("#WeightItemCount").val());
      var random = Math.floor((Math.random() * pkg) + 1);

      var $newSrc = "";
      var $attrImg = "";
      var color = "";
      var bodyMsg = "";
      var countMsg = "";

      if (itemId === "#ScaleCaptureFull")
      {
         $(itemId).text(weight);
         color = "BoxGreyCheckGreen.svg";

         random = count = $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").first().length > 0
            ? $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").first().attr("id") ===
               $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").last().attr("id")
               ? 0
               : parseFloat($("#WeightItemCount").val())
            : 0;
         countMsg = count === 0 ? "" : count + " " + unit;
      }
      else if (itemId === "#ScaleCaptureEmpty")
      {
         $(itemId).text(weight);
         countMsg = "";
         random = 0;
         color = parseFloat(weight) >= parseFloat($("#ScaleCaptureFull").text())
            ? "BoxGreyXRed.svg"
            : "BoxGreyCheckGreen.svg";
      }
      else
      {
         $(itemId).text(weight);

         if (itemId === txtCurrent)
         {
            var count = parseFloat($(alertCount).attr("data-field-value"));

            if (parseFloat(weight) < 0.5)
            {
               if (count === parseFloat($("#WeightItemCount").attr("data-field-name")))
               {
                  bodyMsg = "Weight of full container not high enough for an accurate measure, please indicate that this cannot be captured or click restart to try again.";
                  countMsg = "";
                  color = "BoxGreyXRed.svg";
                  random = 0;
               }
               else
               {
                  count *= 2.5;
                  color = "BoxGreyXRed.svg";
                  bodyMsg = "Weight not high enough for an accurate measure, please put the below amount of product on the scale and click capture.";
                  countMsg = Math.round((count >= parseFloat($("#WeightItemCount").attr("data-field-name")) ? parseFloat($("#WeightItemCount").attr("data-field-name")) : count)) + " " + unit;
                  random = Math.round(count >= parseFloat($("#WeightItemCount").attr("data-field-name")) ? parseFloat($("#WeightItemCount").attr("data-field-name")) : count);
               }
            }
            else
            {
               $("#WeightItemCount").val(Math.round(count >= parseFloat($("#WeightItemCount").attr("data-field-name")) ? parseFloat($("#WeightItemCount").attr("data-field-name")) : count));
               color = "BoxGreyCheckGreen.svg";
               countMsg = $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("nextfocus") === "" ? "" : random + " " + unit;
               random = $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("nextfocus") === "" ? 0 : random;
            }
         }
         else
         {
            var weightRange = (weight / parseFloat($(alertCount).attr("data-field-value"))); //divide the captured weight by the expected quantity value

            if (minRange <= weightRange && weightRange <= maxRange)
            {
               color = "BoxGreyCheckGreen.svg";
               countMsg = $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("nextfocus") === "" ? "" : random + " " + unit;
               random = $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("nextfocus") === "" ? "" : random;
            }
            else
            {
               color = "BoxGreyXRed.svg";
               bodyMsg = "Click Restart to retry individual count weight capture.";
               countMsg = "";
               random = "";
            }
            if ($("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + itemId.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").length === 0)
            {
               random = 0;
               countMsg = "";
            }
         }
      }

      lastTextboxChange = itemId;
      $attrImg = $(itemId).closest("tr").find("img").attr("src");
      $newSrc = "";

      if ($attrImg.indexOf("BoxGrey.svg") > 0)
      {
         $newSrc = $attrImg.replace("BoxGrey.svg", color);
         $(itemId).closest("tr").find("img").attr("src", $newSrc);
      }
      else if ($attrImg.indexOf("BoxGreyXRed.svg") > 0 && itemId === txtCurrent)
      {
         $newSrc = $attrImg.replace("BoxGreyXRed.svg", color);
         $(itemId).closest("tr").find("img").attr("src", $newSrc);
      }

      $("#ProcedureCaptureHeader").html(getNextContainerName(color, unit));
      $(alertCount).html(countMsg);
      $(alertCount).attr("data-field-value", random);

      //if next segment is "Tare Scale" force tare
      if ($("#ProcedureCaptureHeader").html() === "Tare Scale")
      {
         emptyCupTare = true;
         $(alertCount).attr("style", "visibility:hidden");
      }

      $(alertBody).html(bodyMsg === "" ? getNextBodyLine(color, unit) : bodyMsg);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check for scale tare w/ cup
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkTare(txtCurrent, alertBody, unit, tareRes)
   {
      // if tare function fails, return
      if (tareRes === "" || !tareRes)
      {
         return false;
      }

      // current segment
      var itemId = $(txtCurrent).text() !== "" && $(txtCurrent).closest("tr").find("img").attr("src").indexOf("BoxGreyXRed.svg") > 0
         ? txtCurrent
         : getItemId(unit, txtCurrent);

      if (itemId === "" || itemId === undefined || (itemId !== "#ScaleTare0" && itemId !== "#ScaleTare1" && itemId !== "#ScaleTare2"))
      {
         return false;
      }

      // initial tare, no special instructions needed
      emptyCupTare = itemId !== "#ScaleTare0" ? true : false;

      var color = "BoxGreyCheckGreen.svg";
      var $attrImg = $(itemId).closest("tr").find("img").attr("src");
      var $newSrc = "";

      lastTextboxChange = itemId;

      if ($attrImg.indexOf("BoxGrey.svg") > 0)
      {
         $newSrc = $attrImg.replace("BoxGrey.svg", color);
         $(itemId).closest("tr").find("img").attr("src", $newSrc);
      }
      else if ($attrImg.indexOf("BoxGreyXRed.svg") > 0 && itemId === txtCurrent)
      {
         $newSrc = $attrImg.replace("BoxGreyXRed.svg", color);
         $(itemId).closest("tr").find("img").attr("src", $newSrc);
      }

      $("#ProcedureCaptureHeader").html(getNextContainerName(color, unit));

      //emptyCupTare === false implies it's the initial tare function, otherwise grab instructions
      $(alertBody).html(emptyCupTare === false ? "Please place the full container on the scale and click Capture." : getNextBodyLine(color, unit));
      emptyCupTare = false;
      $("#ProcedureCaptureCount").attr("style", "visibility:visible");
   }

   function getItemId(unit, txt)
   {
      return lastTextboxChange === ""
         ? "#" + $("tr[data-field-name='" + unit + "']").not(".hidden").find("label[class='captures']").first().attr("id")
         : "#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id");
   }

   function getNextContainerName(color, unit)
   {
      return color === "BoxGreyCheckGreen.svg"
         ? $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).length > 0
            ? $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("firstline")
            : "Process Complete"
         : "Process Failed";
   }

   function getNextBodyLine(color, unit)
   {
      // check product's dosage image code; show special vessel instructions if liquid
      var dosage = $("#ProcedureCaptureCount").attr("data-field-DosageImg");

      return color === "BoxGreyCheckGreen.svg"
         ? $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).length > 0
            ? dosage !== "L" || (unit === "EA" && dosage === "P")
               ? $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("bodyline")
               : $("#" + $("tr[data-field-name='" + unit + "']").find("label[class='captures'][id='" + lastTextboxChange.replace("#", "") + "']").closest("tr").nextAll("tr:not('.hidden')").find("label[class='captures']").attr("id")).attr("bodylineVes")
            : "All tests fell within the tolerance threshold. Press Continue to go to the next segment."
         : "Click Restart to retry individual count weight capture.";
   }

   var incompleteMessageId = "IncompleteMessage";
   $(document).on("click", ".LotNumberGreenBin", function (e)
   {
      if (compareLots())
      {
         OpenNextSegment($(this).attr("data-ql-next"), "lotDate", true);
      }
   });

   var removeSegmentIncompleteMessage = function (segmentId)
   {
      var $message = $("#" + segmentId + incompleteMessageId);
      if ($message.length)
      {
         $message.remove();
      }
   }
   window.removeSegmentIncompleteMessage = removeSegmentIncompleteMessage;

   var failValidation = function (segmentId)
   {
      addSegmentIncompleteMessage(segmentId);
      $("#" + segmentId).attr("data-segment-complete", "false");
      return false;
   }
   window.failValidation = failValidation;

   var addSegmentIncompleteMessage = function (segmentId)
   {
      if ($("#" + segmentId + incompleteMessageId).length)
      {
         return;
      }

      var $segment = $("#" + segmentId);
      $("#incompleteMessages").append("<span id='" + segmentId + incompleteMessageId + "' data-target-segment='" + segmentId +
         "' style='cursor:pointer;'>The " + $segment.attr("name") + " segment is not complete<br/></span>");
   }
   window.addSegmentIncompleteMessage = addSegmentIncompleteMessage;

   function OpenCalibration()
   {
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleCalibrate?scaleClass=Induction", {}, function (data) { });
   }

   function CheckCalibration()
   {
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleCalibrated?scaleClass=Induc", {}, function (data)
      {
         var $attr = $("img#Check_Calibration").attr("src");
         $newSrc = "";

         if (data)
         {
            window.showWtQtyViolation(null, "");
            $("#CMD_SCALECALIBRATE").attr("disabled", "disabled");
            $("#ProcedureCaptureBody").html($lastBodyBeforeCalibrate === ""
               ? $("#ProcedureCaptureBody").html()
               : $lastBodyBeforeCalibrate);
            $("#ProcedureCaptureHeader").html($lastHeaderBeforeCalibrate === ""
               ? $("#ProcedureCaptureHeader").html()
               : $lastHeaderBeforeCalibrate);
            $("#ProcedureCaptureCount").html($lastCountBeforeCalibrate === ""
               ? $("#ProcedureCaptureCount").html()
               : $lastCountBeforeCalibrate);
            $lastBodyBeforeCalibrate = "";
            $lastHeaderBeforeCalibrate = "";
            $lastCountBeforeCalibrate = "";

            if ($attr.indexOf("BoxGrey.svg") > 0)
            {
               $newSrc = $attr.replace("BoxGrey.svg", "BoxGreyCheckGreen.svg");
               $("img#Check_Calibration").closest("img").attr("src", $newSrc);
            }
            else if ($attr.indexOf("BoxGreyXRed.svg") > 0)
            {
               $newSrc = $attr.replace("BoxGreyXRed.svg", "BoxGreyCheckGreen.svg");
               $("img#Check_Calibration").closest("img").attr("src", $newSrc);
            }
         }
         else
         {
            if ($attr.indexOf("BoxGrey.svg") > 0)
            {
               $newSrc = $attr.replace("BoxGrey.svg", "BoxGreyXRed.svg");
               $("img#Check_Calibration").closest("img").attr("src", $newSrc);
            }
            else if ($attr.indexOf("BoxGreyCheckGreen.svg") > 0)
            {
               $newSrc = $attr.replace("BoxGreyCheckGreen.svg", "BoxGreyXRed.svg");
               $("img#Check_Calibration").closest("img").attr("src", $newSrc);
            }

            $lastBodyBeforeCalibrate = $("#ProcedureCaptureBody").html();
            $lastHeaderBeforeCalibrate = $("#ProcedureCaptureHeader").html();
            $lastCountBeforeCalibrate = $("#ProcedureCaptureCount").html();
            $("#ProcedureCaptureBody").html("The scale needs to be calibrated prior to use. Please click on Calibrate Button to start Calibration. When you are finished Please Click on Tare.");
            $("#CMD_SCALECALIBRATE").removeAttr("disabled");
            //Please calibrate your scale. Once finished, please tare your scale while empty, place your small 100g calibration weight on the scale and click Capture.");
            $("#ProcedureCaptureHeader").html("Calibration");
            $("#ProcedureCaptureCount").html("");

         }

      });
      //$.getJSON("https://localhost:8001/Qualanex/http/ScaleCapture", {
      //}, function(data)
      //{
      //});
      //ga("send", "event", "SegmentWtQty", "Calibration", "CaptureButton");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var checkScaleConditionGreenBin = function ()
   {
      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleConnected", {}, function (result)
      {
         console.log("ScaleConnected:" + result);

         if ($.parseJSON(result))
         {
            setScaleBusy(false);
            $("#CMD_SCALECALIBRATE").removeAttr("disabled");
            tareScale();
         }
         else
         {
            window.showWtQtyViolation(SEG_WARNING, ERR_NOSCALE);
            $("#CMD_SCALECALIBRATE").attr("disabled", "disabled");
         }
         return result;
      });
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.checkScaleConditionGreenBin = checkScaleConditionGreenBin;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   var updateIncompleteMessages = function ()
   {
      $(".ReadyForDispositionHidden").val("True");
      $("#Policy [data-segment-complete]").each(function ()
      {
         if ($(this).attr("data-segment-complete") === "true")
         {
            removeSegmentIncompleteMessage(this.id);
         }
         else
         {
            $(".ReadyForDispositionHidden").val("False");
            addSegmentIncompleteMessage(this.id);
         }
      });
   }
   window.updateIncompleteMessages = updateIncompleteMessages;

   var submitPolicy = function (e, control)
   {
      updateIncompleteMessages();
      $(".policy-status").hide();
      $(".policy-status-loading").show();
      var $btn = $(this);

      if ($(control).length)
      {
         $btn = $(control);
      }

      var $next = $($btn.attr("data-ql-next"));
      var $form = $("#GreenBinProcessingDetailsForm #FormGreenBinProcessingDetailForm form");
      $form.validate();

      if ($form.valid())
      {
         if ($next.length)
         {
            $btn.parents(".panel-collapse:first").slideCollapse();
         }

         $form.submit();
      }
   }
   window.submitPolicy = submitPolicy;
   //*****************************************************************************
   //*
   //* Summary:
   //*   Select a Form by ID, serialize the content, clean it, and send it back
   //*   as an object
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   (function ($)
   {
      $.fn.serializeObject = function ()
      {
         var self = this,
            json = {
            },
            pushCounters = {
            },
            patterns = {
               "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
               "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
               "push": /^$/,
               "fixed": /^\d+$/,
               "named": /^[a-zA-Z0-9_]+$/
            };

         this.build = function (base, key, value)
         {
            base[key] = value;
            return base;
         };

         this.push_counter = function (key)
         {
            if (pushCounters[key] === undefined)
            {
               pushCounters[key] = 0;
            }
            return pushCounters[key]++;
         };

         $.each($(this).serializeArray(), function ()
         {
            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
               return;
            }

            var k,
               keys = this.name.match(patterns.key),
               merge = this.value,
               reverseKey = this.name;

            while ((k = keys.pop()) !== undefined)
            {
               // adjust reverse_key
               reverseKey = reverseKey.replace(new RegExp("\\[" + k + "\\]$"), "");

               // push
               if (k.match(patterns.push))
               {
                  merge = self.build([], self.push_counter(reverseKey), merge);
               }
               // fixed
               else if (k.match(patterns.fixed))
               {
                  merge = self.build([], k, merge);
               }
               // named
               else if (k.match(patterns.named))
               {
                  merge = self.build({
                  }, k, merge);
               }
            }

            json = $.extend(true, json, merge);
         });

         return json;
      };
   })(jQuery);
});


//function that is called when clicking the garbage can on image in staging
//does ajax to make server delete the file on server
//then removes the div containing the image preview from the page
//special note - jquery has a hard time if the id has a period/dot in it
//that's why we cut it short before the file extension on the ID for the divs
function WrhsStagingDeletePendingBtnClick(event)
{
   var fileName = event.target.id;
   var imgType = $("#imageOption").val();
   var file = imgType === "CTR" ? $("#divAttachementSegment_Container").find("#FileUpload")[0].files : $("#divAttachementSegment_Content").find("#FileUpload")[0].files;

   //DeleteSelectedImage with file name
   $.ajax({
      type: "POST",
      url: "/GreenBin/GreenBin/DeletePendingFile",
      data: JSON.stringify({ fileName: $("#imageOption").val() + "\\" + fileName }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function (response)
      {
         if (response.success)
         {
            //although some characters are compatible for html markup, jquery does not like them
            //so far the list is . (period) which is in every file extension
            //and parenthesis () which are commonly found if you copy/paste a file several times making a file_copy (2) etc
            //space
            var divId = fileName;
            divId = "div_" + divId.slice(0, divId.indexOf("."));
            var div = $("#" + divId);
            div.remove();
         }
         else
         {
            $(this).setErrorMessage(response.error, "Error Message", "OK");
         }
      },
      error: function (error)
      {
         $(this).setErrorMessage("The controller call failed on attachment delete request.", "Error Message", "OK");
      }
   });
}

//Called when you click the upload button for an image in Image Capture segment of Green Bin Processing
//Usually file upload control does a full post back of the page whereas this page
//was designed around dropzone and not for postback.
//Here we do ajax call where the file will be saved to the server.  Info is returned
//in json object including full location on server for href, if it's an image (or pdf)
//fileName in addition to success/error message.
//upon callback we construct a div that looks identical to the existing attachments icons etc.
//PS jquery doesn't like period/dot used for selectors so that's removed for the ID for deleting purposes
function WrhsStagingUploadPendingBtnClick()
{
   var imgType = $("#imageOption").val();
   var totalFiles = imgType === "CTR" ? $("#divAttachementSegment_Container").find("#FileUpload")[0].files.length : $("#divAttachementSegment_Content").find("#FileUpload")[0].files.length;

   if (totalFiles === 0)
   {
      $(this).setErrorMessage("No Files Selected", "Error Message", "OK");
      return false;
   }

   for (var i = 0; i < totalFiles; i++)
   {
      var file = imgType === "CTR" ? $("#divAttachementSegment_Container").find("#FileUpload")[0].files[i] : $("#divAttachementSegment_Content").find("#FileUpload")[0].files[i];
      var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp|.svg|.ico|.tif)$/;

      if (!regex.test(file.name.toLowerCase()))
      {
         $(this).setErrorMessage("Please upload a valid image file.", "Error Message", "ok");
         return false;
      }

      if (file.size > 3145728) //3 mill / mb
      {
         $(this).setErrorMessage("File size must be less than 3 megabytes. Please reduce size and try again.", "Error Message", "OK");
         return false;
      }

      var formData = new FormData();
      formData.append("FileUpload", file);
      formData.append("ImgType", imgType);

      $.ajax({
         type: "POST",
         url: "/GreenBin/GreenBin/UploadNewFile",
         data: formData,
         dataType: "json",
         contentType: false,
         processData: false,
         success: function (response)
         {
            if (response.success)
            {
               var strImg = "";

               if (response.isImage)
               {
                  strImg = "<img src='" + response.pathOnServer + "' style='margin-top:-29px;width:70px;height:70px;' alt='click for download'/>";
               }
               else
               {
                  strImg = "<img src='/Images/SVG/paper-clip.svg' style='margin-top:-29px;width:70px;height:70px;' alt='click for download' />";
               }

               //although some characters are compatible for html markup, jquery does not like them
               //so far the list is . (period) which is in every file extension
               //and parentthesis () which are commonly found if you copy/paste a file several times making a file_copy (2) etc
               //space
               var divId = response.fileName;
               divId = "div_" + divId.slice(0, divId.indexOf("."));

               var strImgDiv = "<div id='" + divId + "' data-row-name='" + response.fileName +
                  "' class='pendingFileUpload' style='width: 80px; height: 80px; border: 2px solid #ccc; border-radius: 5px; padding:3px; margin: 15px 30px; float: left; display: inline-block; '>" +
                  "<img src='/Images/SVG/delete (1).svg' onclick='WrhsStagingDeletePendingBtnClick(event);' id='" +
                  response.fileName + "' style='width:40px; margin-left:80px; margin-top: -10px; cursor:pointer' />";

               strImgDiv = strImgDiv + "<a href='" + response.pathOnServer + "'  target='_blank' style='width:100%;height:80px;'>" + strImg + "</a></div>";

               $("#divAttachementSegment_" + (imgType === "CTR" ? "Container" : "Content") + " #WrhsStagingPendingAttachments").prepend(strImgDiv);
            }
            else
            {
               $(this).setErrorMessage(response.error, "Error Message", "OK");
            }
         },
         error: function (error)
         {
            $(this).setErrorMessage("The controller call failed on attachment upload request.", "Error Message", "OK");
         }
      });
   }

   return false;
}