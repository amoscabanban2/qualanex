///-----------------------------------------------------------------------------------------------
/// <copyright file="GreenBinViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.GreenBin.ViewModels
{
   using System.Collections.Generic;
   using System.Web.Mvc;

   using Model;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GreenBinViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_GRNBIN";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only for Segment options.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GreenBinSegmentControl> lstSegments { get; set; } = new List<GreenBinSegmentControl>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Dock Receiving record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public GreenBinModel SelectedGreenBinModel { get; set; } = new GreenBinModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Green Bin list for the Dock Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GreenBinModel> GreenBinDetails { get; set; } = new List<GreenBinModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<string> ModelList { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<Dictionary<string, IEnumerable<object>>> sidebarObjects { get; set; } = new List<Dictionary<string, IEnumerable<object>>>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<ImageResult> lstImages { get; set; } = new List<ImageResult>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ContainerCondition Segments { get; set; } = new ContainerCondition();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public GreenBinRelationControl LstWasteInformation { get; set; } = new GreenBinRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the list of filtered waste stream waste code rel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<GreenBinModel> LstWasteStreamCodeRel { get; set; } = new List<GreenBinModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<SelectListItem> YearOptions { get; set; } = new List<SelectListItem>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<SelectListItem> MonthOptions { get; set; } = new List<SelectListItem>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate Application Information.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UpaViewModel UPAControl { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate Application Information.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public BinventoryViewModel Inventory { get; set; } = new BinventoryViewModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public UpaViewModel Application { get; set; } = new UpaViewModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public Product.Models.GreenBinReason GreenBinReasons { get; set; } = new Product.Models.GreenBinReason();

   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GreenBinSegmentControl
   {
      public string Images { get; set; }
      public string SegmentName { get; set; }
      public string Required { get; set; }
      public string Title { get; set; }
      public string Partial { get; set; }
      public string SegmentType { get; set; }
      public string NextSegment { get; set; }
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class GreenBinRelationControl
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl WasteStream { get; set; } = new RelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl WasteCode { get; set; } = new RelationControl();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class SideBarItems
   {
      public List<object> ModelList { get; set; } = new List<object>();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ContainerCondition
   {
      public string Name { get; set; }
      public string Code { get; set; }
      public int ContainerTypeIndex { get; set; }
      public string dataHeader { get; set; }
      public string form { get; set; }
      public string uniqSelection { get; set; }
      public string ItemCode { get; set; }
      public bool IsSelected { get; set; }
      public string ImageUrl { get; set; }
   }

}