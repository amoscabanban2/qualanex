///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStagingModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsStaging.ViewModels
{
   using System;
   using System.IO;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Numerics;
   using System.Threading;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Blob;
   using Microsoft.WindowsAzure.Storage.Shared.Protocol;

   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using Model;
   using Utility.Common;
   using Utility;
   using System.Data.SqlClient;
   using System.Runtime.Caching;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStagingModel : IInfiniteScroll, IQoskApplet
   {
      private readonly object _disposedLock = new object();

      private static string[] imagesFormat = { ".JPG", ".PNG", ".JPEG", ".SVG", ".BMP", ".GIF" };

      private bool _isDisposed;
      private static string[] form222Status = { "Printed", "Closed" };

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public WrhsStagingModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~WrhsStagingModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Add Totes")]
      public string Code { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Name")]
      public string Name { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Staged Date
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Staged Date")]
      public DateTime? StgeDate { get; set; }
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified Date")]
      public string ModifiedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Carrier")]
      public string Carrier { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA or WPT")]
      public string RAorWPT { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"IsDeleted")]
      public bool IsDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tracking ID")]
      public string TrackingID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tracking Number")]
      public string TrackingNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Carrier ID")]
      public string CarrierID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Date")]
      public string Date { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Container")]
      public string Container { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get count of containers tied to a staged record
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Container Count")]
      public int ContainerCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Code")]
      public string PackageCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Name")]
      public string PackageName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Condition Code")]
      public List<string> PackageCndCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Condition Code")]
      public string PackageConditionCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Comments for Receiving
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Notes")]
      public string Notes { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Condition Name")]
      public string PackageConditionName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Weight")]
      public string Weight { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"weight Condition")]
      public string weightCondition { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Debit Memo Number
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Debit Memo")]
      public string DebitMemo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Shared")]
      public int IsShared { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Comment")]
      public string Comment { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Staged Debit Memo Number")]
      public string StgeDebitMemoNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 Print Date")]
      public string Form222PrintedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RA/WPT Control Code
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA or WPT Control Code")]
      public string RAorWPTCtrlCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Special Handling Message
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Handling Message")]
      public string HandlingMessage { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Assoc ID which associates the RtrnAuthID from the ReturnAuthorization table to the Tracking table
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tracking Assoc ID")]
      public string AssocID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Control number (0,2,345)
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Control Type")]
      public string ControlType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Handling Segment")]
      public bool HandlingSegment { get; set; } = false;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Special Handling")]
      public bool SpecialHandling { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Compare Received Date to Form222 Print Date in History Tab
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Received Date")]
      public string RcvdDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Debit Memo Manufacturer Profile Code
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Manufacturer Profile Code")]
      public string MfrProfileCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Debit Memo Manufacturer Profile Code
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Manufacturer Profile Name")]
      public string MfrProfileName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - Form 222 Customer Name
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 Customer Name")]
      public string Form222Name { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - Form 222 Dock Door Date
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 Dock Door Date")]
      public string DockDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - Form 222 3rd Party Name
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 3rd Party")]
      public string ReceivedFrom { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - Date Difference
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222/Dock Door Day Difference")]
      public string TimeElapsed { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - Debit Memo Number
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 Debit Memo Number")]
      public string Form222DebitMemoNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Form 222 Details - DEA Number
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Form 222 DEA Number")]
      public string Form222DEANumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Route
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Route Number")]
      public int RouteNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Route Name
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Route")]
      public string Route { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RA Status Classification
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA Status Classification")]
      public string RaStatusClass { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Staged By")]
      public string StgeBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"External RA or WPT")]
      public string ExternRAorWPT { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tote Item Count")]
      public int ToteItemCount { get; set; }

      public int InitialLoadCount { get; set; } = 50;

      public int ScrollLoadCount { get; set; } = 10;

      public int PreviousCount { get; set; } = 0;

      public int LoadAtHeight { get; set; } = 300;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Get Location of current machine.
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetMachineLocation(string machineName)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var machineInfo = (from qsk in cloudEntities.Qosk
                               where qsk.MachineID == machineName && qsk.IsDeleted == false
                               select qsk).FirstOrDefault();
            return machineInfo == null
                        ? ""
                        : machineInfo.LocationRoute.ToString();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get list of RAs based on the debit memo lookup in Totes/Attachments segment
      /// </summary>
      /// <param name="debitMemo"></param>
      /// <param name="searchArg"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetDebitMemoList(string debitMemo, string searchArg)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var reslt = System.Text.RegularExpressions.Regex.Replace(debitMemo, @"[^a-zA-Z0-9]", "");
            var condition = searchArg == "Contain"
                              ? "(dbtMemo.DebitMemoNumber like N'%" + debitMemo + "%' or dbtMemo.DebitMemoNumber like N'%" + reslt + "%')"
                              : "(dbtMemo.DebitMemoNumber like N'" + debitMemo + "%' or dbtMemo.DebitMemoNumber like N'" + reslt + "%')";

            var sqlQuery = " select dbtMemo.DebitMemoID as Code, dbtMemo.DebitMemoNumber as DebitMemo, " +
                           " convert(nvarchar, raTbl.RtrnAuthNumber) as RAorWPT, raTbl.RtrnAuthID as AssocID, " +
                           " dbtMemo.Name as Container, prfl.Name as MfrProfileCode, ISNULL(convert(bit, prfl.SpecialHandling),0) as SpecialHandling, " +
                           " raTbl.RtrnAuthCtrlCode as ControlType " +
                           " from ReturnAuthorization raTbl " +
                           " inner join DebitMemo dbtMemo on raTbl.DebitMemoID = dbtMemo.DebitMemoID " +
                           " inner join Profile prfl on dbtMemo.MfrProfileCode = prfl.ProfileCode " +
                           " where raTbl.IsDeleted = 0 and dbtMemo.IsDeleted = 0 and " +
                           " raTbl.RtrnAuthStatusCode not in ('C','CD', 'CE') and" +
                           " dbtMemo.ApprovedDate between dateadd(mm,-4,getdate()) and getdate() and " +
                           condition;

            return cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   List of data list depends on which textbox is.
      /// </summary>
      /// <param name="browser"></param>
      /// <param name="isDeleted"></param>
      /// <param name="item"></param>
      /// <param name="strData"></param>
      /// <param name="strFilterValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetListOptions(bool isDeleted, string strData, string strFilterValue, string browser, string item)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return GetCarrierList(isDeleted, strData, strFilterValue, browser, item, cloudEntities);
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }


      ///****************************************************************************
      /// <summary>
      ///   Get Exact Match.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<string, List<WrhsStagingModel>> GetPartialMatch(string trackingNumber, string ra)
      {
         var dictMatch = new Dictionary<string, List<WrhsStagingModel>>();

         using (var cloudEntities = new QoskCloud())
         {
            var getPartials = GetPartialBy(trackingNumber, "", cloudEntities);

            var strSearchBy = "Found partial match under entered";

            if (getPartials.Count > 0)
            {
               strSearchBy += " Tracking Number. /1";    // found by Tracking Number
            }
            else
            {
               getPartials = GetPartialBy("", ra, cloudEntities);

               if (getPartials.Count > 0)
               {
                  strSearchBy += " RA. /2";       // found by RA
               }
               else
               {
                  getPartials = GetPartialBy(ra, "", cloudEntities);

                  if (getPartials.Count > 0)
                  {
                     strSearchBy += " Tracking Number as RA. /3";  // found by Tracking number entered as RA
                  }
                  else
                  {
                     getPartials = GetPartialBy("", trackingNumber, cloudEntities);

                     if (getPartials.Count > 0)
                     {
                        strSearchBy += " RA as Tracking Number. /4";   // found by ra entered as tracking number
                     }
                     else
                     {
                        strSearchBy = "No records found";
                     }
                  }
               }
            }

            dictMatch = new Dictionary<string, List<WrhsStagingModel>>
               {
                  { strSearchBy, getPartials }
               };
         }

         return dictMatch;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Exact Match.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<string, List<WrhsStagingModel>> GetPartialMatch(string trackingNumber)
      {
         var dictMatch = new Dictionary<string, List<WrhsStagingModel>>();

         using (var cloudEntities = new QoskCloud())
         {
            var getPartials = GetPartialBy(trackingNumber, "", cloudEntities);

            var strSearchBy = "";

            if (getPartials.Count > 0)
            {
               strSearchBy = "Found Tracking Number Match(es)";
            }

            dictMatch = new Dictionary<string, List<WrhsStagingModel>>
            {
               { strSearchBy,getPartials }
            };
         }

         return dictMatch;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Exact Match.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<string, List<WrhsStagingModel>> GetPartialMatchRA(string ra)
      {
         var dictMatch = new Dictionary<string, List<WrhsStagingModel>>();

         using (var cloudEntities = new QoskCloud())
         {
            var getPartials = GetPartialBy("", ra, cloudEntities);
            var strSearchBy = "";

            if (getPartials.Count > 0)
            {
               strSearchBy = "Found RA Match(es)";
            }

            dictMatch = new Dictionary<string, List<WrhsStagingModel>>
            {
               { strSearchBy,getPartials }
            };
         }

         return dictMatch;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of RAs not received
      /// </summary>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<string, List<WrhsStagingModel>> GetRANoTracking(string ra)
      {
         Dictionary<string, List<WrhsStagingModel>> dictMatch;

         using (var cloudEntities = new QoskCloud())
         {
            var partialQuery = " SELECT t.TrackingID, t.TrackingNumber AS TrackingNumber, CONVERT(nvarchar, t.RcvdDate, 101) AS Date, " +
                               " ra.RtrnAuthCtrlCode AS RAorWPTCTRLCode, CONVERT(nvarchar, ra.RtrnAuthNumber) AS RAorWPT, t.CarrierCode AS CarrierID, " +
                               " cd.Description as Carrier, CONVERT(nvarchar, dbt.MfrProfileCode) AS MfrProfileCode, dbt.DebitMemoNumber AS DebitMemo, " +
                               " CASE WHEN ra.RtrnAuthCtrlCode = 'CTRL2' THEN (SELECT TOP 1 CONVERT(nvarchar, PrintedDate, 101) FROM Form222 f WHERE f.Form222SourceID = ra.RtrnAuthNumber AND f.Form222Source = '0' AND f.Status IN ('Printed', 'Closed'))" +
                               " ELSE '' END AS Form222PrintedDate " +
                               " FROM Tracking t LEFT JOIN ReturnAuthorization ra ON t.AssocID = ra.RtrnAuthID LEFT JOIN CarrierDict cd ON t.CarrierCode = cd.Code " +
                               " LEFT JOIN DebitMemo dbt ON ra.DebitMemoID = dbt.DebitMemoID LEFT JOIN Profile p on p.profileCode = dbt.MfrProfileCode " +
                               " WHERE CONVERT(nvarchar,ra.RtrnAuthNumber) = @ra AND ra.IsDeleted = 0 AND t.IsDeleted = 0 AND t.StgeDate IS NULL AND t.TrackingTypeCode NOT IN ('NR', 'NP')";
            var list = cloudEntities.Database.SqlQuery<WrhsStagingModel>(partialQuery, new SqlParameter("@ra", ra)).ToList();
            var strSearchBy = "";

            if (list.Count > 0)
            {
               strSearchBy = "Found RA Match";
            }
            else
            {
               partialQuery = " SELECT TOP 1 '0' AS TrackingID, 'No-Tracking-Number' AS TrackingNumber, '' AS Date, " +
                              " ra.RtrnAuthCtrlCode AS RAorWPTCTRLCode, CONVERT(nvarchar, ra.RtrnAuthNumber) AS RAorWPT, 'QNEX' AS CarrierID, " +
                              " 'Qualanex' as Carrier, CONVERT(nvarchar, dbt.MfrProfileCode) AS MfrProfileCode, dbt.DebitMemoNumber AS DebitMemo, " +
                              " CASE WHEN ra.RtrnAuthCtrlCode='CTRL2' THEN (SELECT TOP 1 CONVERT(nvarchar, PrintedDate, 101) FROM Form222 f WHERE f.Form222SourceID = ra.RtrnAuthNumber AND f.Form222Source='0' AND f.Status IN ('Printed', 'Closed')) " +
                              " ELSE '' END AS Form222PrintedDate " +
                              " FROM ReturnAuthorization ra LEFT JOIN Tracking t ON t.AssocID = ra.RtrnAuthID " +
                              " LEFT JOIN DebitMemo dbt ON ra.DebitMemoID = dbt.DebitMemoID LEFT JOIN Profile p on p.profileCode = dbt.MfrProfileCode " +
                              " WHERE CONVERT(nvarchar,ra.RtrnAuthNumber) = @ra AND ra.IsDeleted = 0 ORDER BY t.IsDeleted";
               list = cloudEntities.Database.SqlQuery<WrhsStagingModel>(partialQuery, new SqlParameter("@ra", ra)).ToList();

               if (list.Count > 0)
               {
                  strSearchBy = "Found RA Match Without Tracking Number";
               }
            }

            if (string.IsNullOrEmpty(strSearchBy))
            {
               partialQuery = " SELECT t.TrackingID, t.TrackingNumber AS TrackingNumber, CONVERT(nvarchar, t.RcvdDate, 101) AS Date, " +
                                   " ra.RtrnAuthCtrlCode AS RAorWPTCTRLCode, CONVERT(nvarchar, ra.RtrnAuthNumber) AS RAorWPT, t.CarrierCode AS CarrierID, " +
                                   " cd.Description as Carrier, CONVERT(nvarchar, dbt.MfrProfileCode) AS MfrProfileCode, dbt.DebitMemoNumber AS DebitMemo, " +
                                   " CASE WHEN ra.RtrnAuthCtrlCode = 'CTRL2' THEN (SELECT TOP 1 CONVERT(nvarchar, PrintedDate, 101) FROM Form222 f WHERE f.Form222SourceID = ra.RtrnAuthNumber AND f.Form222Source = '0' AND f.Status IN ('Printed', 'Closed'))" +
                                   " ELSE '' END AS Form222PrintedDate " +
                                   " FROM Tracking t LEFT JOIN ReturnAuthorization ra ON t.AssocID = ra.RtrnAuthID LEFT JOIN CarrierDict cd ON t.CarrierCode = cd.Code " +
                                   " LEFT JOIN DebitMemo dbt ON ra.DebitMemoID = dbt.DebitMemoID LEFT JOIN Profile p on p.profileCode = dbt.MfrProfileCode " +
                                   " WHERE ra.ExternRtrnAuthNumber = @ra AND ra.IsDeleted = 0 AND t.IsDeleted = 0 AND t.StgeDate IS NULL AND t.TrackingTypeCode NOT IN ('NR', 'NP')";
               list = cloudEntities.Database.SqlQuery<WrhsStagingModel>(partialQuery, new SqlParameter("@ra", ra)).ToList();

               if (list.Count > 0)
               {
                  strSearchBy = "Found RA Match";
               }
               else
               {
                  partialQuery = " SELECT TOP 1 '0' AS TrackingID, 'No-Tracking-Number' AS TrackingNumber, '' AS Date, " +
                                  " ra.RtrnAuthCtrlCode AS RAorWPTCTRLCode, CONVERT(nvarchar, ra.RtrnAuthNumber) AS RAorWPT, 'QNEX' AS CarrierID, " +
                                  " 'Qualanex' as Carrier, CONVERT(nvarchar, dbt.MfrProfileCode) AS MfrProfileCode, dbt.DebitMemoNumber AS DebitMemo, " +
                                  " CASE WHEN ra.RtrnAuthCtrlCode='CTRL2' THEN (SELECT TOP 1 CONVERT(nvarchar, PrintedDate, 101) FROM Form222 f WHERE f.Form222SourceID = ra.RtrnAuthNumber AND f.Form222Source='0' AND f.Status IN ('Printed', 'Closed')) " +
                                  " ELSE '' END AS Form222PrintedDate " +
                                  " FROM ReturnAuthorization ra LEFT JOIN Tracking t ON t.AssocID = ra.RtrnAuthID " +
                                  " LEFT JOIN DebitMemo dbt ON ra.DebitMemoID = dbt.DebitMemoID LEFT JOIN Profile p on p.profileCode = dbt.MfrProfileCode " +
                                  " WHERE ra.ExternRtrnAuthNumber = @ra AND ra.IsDeleted = 0 ORDER BY t.IsDeleted";
                  list = cloudEntities.Database.SqlQuery<WrhsStagingModel>(partialQuery, new SqlParameter("@ra", ra)).ToList();

                  if (list.Count > 0)
                  {
                     strSearchBy = "Found RA Match Without Tracking Number";
                  }
               }
            }

            dictMatch = new Dictionary<string, List<WrhsStagingModel>>
            {
               { strSearchBy,list }
            };
         }

         return dictMatch;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Exact Match.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetPartialBy(string trackingNumber, string ra, QoskCloud cloudEntities)
      {
         var partialQueryBase =
             "SELECT t.TrackingID as TrackingID , t.TrackingNumber as TrackingNumber,convert(nvarchar, t.RcvdDate,101) as Date," +
             " ra.RtrnAuthCtrlCode as RAorWPTCTRLCode,convert(nvarchar,ra.RtrnAuthNumber) as RAorWPT , " +
             " t.CarrierCode as CarrierID, cd.Description as Carrier, " +
             " convert(nvarchar,dbt.MfrProfileCode) as MfrProfileCode, " +
             " dbt.DebitMemoNumber as DebitMemo," +
             " case when ra.RtrnAuthCtrlCode='CTRL2' then(select top(1) convert(nvarchar, PrintedDate, 101) from Form222 f where f.Form222SourceID = ra.RtrnAuthNumber and f.Form222Source='0' and f.Status  in ('Printed', 'Closed'))" +
             " else '' end as Form222PrintedDate" +
             " FROM Tracking  t " +
             " join ReturnAuthorization ra ON t.AssocID=ra.RtrnAuthID " +
             " join CarrierDict cd ON t.CarrierCode = cd.Code " +
             " join DebitMemo dbt on ra.DebitMemoID = dbt.DebitMemoID " +
             " left outer join Profile p on dbt.MfrProfileCode = p.ProfileCode " +
             " WHERE t.IsDeleted=0 and t.TrackingTypeCode not in ('NR','NP') AND t.StgeDate is null ";

         var internalRaParam = $"{partialQueryBase} " +
             (string.IsNullOrWhiteSpace(trackingNumber)
                   ? " and convert(nvarchar,ra.RtrnAuthNumber) = @ra "
                   : " and t.TrackingNumber = @trk");
         var externalRaParam = $"{partialQueryBase} and ra.ExternRtrnAuthNumber = @ra ";

         try
         {
            var result = cloudEntities.Database.SqlQuery<WrhsStagingModel>(internalRaParam,
                (string.IsNullOrWhiteSpace(trackingNumber)
                    ? new SqlParameter("@ra", ra)
                    : new SqlParameter("@trk", trackingNumber))).ToList();

            if (result.Count == 0 && string.IsNullOrWhiteSpace(trackingNumber))
            {
               result = cloudEntities.Database.SqlQuery<WrhsStagingModel>(externalRaParam, new SqlParameter("@ra", ra)).ToList();
            }

            return result;
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Exact Match.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <param name="location"></param>
      /// <param name="reserved"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<string, WrhsStagingModel> GetExactMatch(string trackingNumber, string ra, int location, WrhsStagingModel reserved)
      {
         return GetExactMatchRecord(trackingNumber, ra, location, reserved);
      }

      ///****************************************************************************
      /// <summary>
      /// 
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <param name="location"></param>
      /// <param name="reserved"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Dictionary<string, WrhsStagingModel> GetExactMatchRecord(string trackingNumber, string ra, int location, WrhsStagingModel reserved)
      {
         var dictMatch = new Dictionary<string, WrhsStagingModel>();
         var wrhsStge = new WrhsStagingModel();
         var fields = "";

         using (var cloudEntities = new QoskCloud())
         {
            var exactQuery =
               "SELECT t.TrackingID as TrackingID , t.TrackingNumber as TrackingNumber,convert(nvarchar, t.RcvdDate,101) as Date," +
               " t.StgeDate as StgeDate,convert(nvarchar,t.StgeDate,101) as ModifiedDate,convert(nvarchar, t.StgeWeight)  as Weight," +
               " t.StgePkgTypeCode as PackageCode, rasd.Classification as RaStatusClass, " +
               " ra.RtrnAuthCtrlCode as RAorWPTCTRLCode,convert(nvarchar,ra.RtrnAuthNumber) as RAorWPT, ra.ExternRtrnAuthNumber as ExternRAorWPT, " +
               " t.CarrierCode as CarrierID, cd.Description as Carrier , pk.Code as PackageCode,pk.Description as PackageName," +
               " dbt.DebitMemoNumber as DebitMemo, t.StgeComment as Comment ," +
               " convert(nvarchar,dbt.MfrProfileCode) as MfrProfileCode, p.Name as MfrProfileName, t.StgeDebitMemoNumber as StgeDebitMemoNumber, " +
               " isnull(convert(bit,p.SpecialHandling),0) as SpecialHandling,rtd.Description as HandlingMessage , " +
               " convert(nvarchar,rtd.Code) as Name " +
               " from Tracking  t " +
               " join ReturnAuthorization ra ON t.AssocID=ra.RtrnAuthID " +
               " join RtrnAuthStatusDict rasd on rasd.Code = ra.RtrnAuthStatusCode " +
               " join CarrierDict cd ON t.CarrierCode = cd.Code " +
               " left outer join WrhsPackageTypeDict pk on t.StgePkgTypeCode = pk.Code " +
               " join DebitMemo dbt on ra.DebitMemoID = dbt.DebitMemoID " +
               " inner join Profile p on dbt.MfrProfileCode = p.ProfileCode " +
               " join WrhsRouteDict rtd on  rtd.Code =t.WrhsRouteCode " +
               " where t.IsDeleted=0 and t.TrackingTypeCode not in('NR','NP') and " +
               " ((t.TrackingNumber = @trk and convert(nvarchar,ra.RtrnAuthNumber) =@ra) " +
               " or (t.TrackingNumber=@ra and convert(nvarchar,ra.RtrnAuthnumber)=@trk) " +
               " or (t.TrackingNumber = @trk and ra.ExternRtrnAuthNumber = @ra) " +
               " or (t.TrackingNumber=@ra and ra.ExternRtrnAuthnumber = @trk)) " +
               " order by t.StgeDate";
            try
            {
               var matches = cloudEntities.Database.SqlQuery<WrhsStagingModel>(exactQuery,
                                                                                 new SqlParameter("@trk", trackingNumber),
                                                                                 new SqlParameter("@ra", ra)).ToList();

               if (matches.Count > 0)
               {
                  matches.ForEach(c => c.RouteNumber = location);

                  if (matches.Any(c => !c.StgeDate.HasValue && c.TrackingNumber.ToLower() == trackingNumber.ToLower() && (c.RAorWPT == ra || c.ExternRAorWPT.ToLower() == ra.ToLower()))) // exact Match
                  {
                     wrhsStge = matches.FirstOrDefault(c => !c.StgeDate.HasValue && c.TrackingNumber.ToLower() == trackingNumber.ToLower() && (c.RAorWPT == ra || c.ExternRAorWPT.ToLower() == ra.ToLower()));
                     fields = wrhsStge.TrackingNumber + "," + ra;
                  }
                  else if (matches.Any(c => c.StgeDate.HasValue && c.TrackingNumber.ToLower() == trackingNumber.ToLower() && (c.RAorWPT == ra || c.ExternRAorWPT.ToLower() == ra.ToLower()))) // staged exact match
                  {
                     wrhsStge = matches.FirstOrDefault(c => c.StgeDate.HasValue && c.TrackingNumber.ToLower() == trackingNumber.ToLower() && (c.RAorWPT == ra || c.ExternRAorWPT.ToLower() == ra.ToLower()));
                     fields = wrhsStge.TrackingNumber + "," + ra;
                  }
                  else if (matches.Any(c => !c.StgeDate.HasValue && c.TrackingNumber.ToLower() == ra.ToLower() && (c.RAorWPT == trackingNumber || c.ExternRAorWPT.ToLower() == trackingNumber.ToLower()))) // switched tracking number and ra 
                  {
                     wrhsStge = matches.FirstOrDefault(c => !c.StgeDate.HasValue && c.TrackingNumber.ToLower() == ra.ToLower() && (c.RAorWPT == trackingNumber || c.ExternRAorWPT.ToLower() == trackingNumber.ToLower()));
                     fields = wrhsStge.TrackingNumber + "," + trackingNumber;
                  }
                  else if (matches.Any(c => c.StgeDate.HasValue && c.TrackingNumber.ToLower() == ra.ToLower() && (c.RAorWPT == trackingNumber || c.ExternRAorWPT.ToLower() == trackingNumber.ToLower()))) // switched tracking number and ra staged
                  {
                     wrhsStge = matches.FirstOrDefault(c => c.StgeDate.HasValue && c.TrackingNumber.ToLower() == ra.ToLower() && (c.RAorWPT == trackingNumber || c.ExternRAorWPT.ToLower() == trackingNumber.ToLower()));
                     fields = wrhsStge.TrackingNumber + "," + trackingNumber;
                  }
               }

               if (!string.IsNullOrWhiteSpace(wrhsStge.TrackingID))
               {
                  if (wrhsStge.Name != "99")
                  {
                     if (wrhsStge.CarrierID == reserved.CarrierID
                         && wrhsStge.Date == reserved.Date
                         && wrhsStge.TrackingNumber.ToLower() == reserved.TrackingNumber.ToLower())
                     {
                        wrhsStge.Weight = reserved.Weight;
                        wrhsStge.PackageCndCode = reserved.PackageCndCode;
                        wrhsStge.PackageCode = reserved.PackageCode;
                     }
                     else
                     {
                        var lstStagedRecord = GetLastStagedRecord(wrhsStge.TrackingNumber, wrhsStge.CarrierID, wrhsStge.Date);
                        if (lstStagedRecord != null)
                        {
                           wrhsStge.PackageCode = lstStagedRecord.PackageCode;
                           wrhsStge.PackageCndCode = lstStagedRecord.PackageCndCode;
                           wrhsStge.Weight = lstStagedRecord.Weight;
                        }
                     }
                  }
                  dictMatch = new Dictionary<string, WrhsStagingModel>
                  {
                     {fields, wrhsStge}
                  };
               }
               else
               {
                  dictMatch = new Dictionary<string, WrhsStagingModel>
                  {
                     {"", wrhsStge}
                  };
               }

               return dictMatch;
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }

            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   check validation of tracking number and ra .
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="trackingNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel CheckValidation(string trackingNumber, string ra)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from tracking in cloudEntities.Tracking
                    join raTbl in cloudEntities.ReturnAuthorization on tracking.AssocID equals raTbl.RtrnAuthID into raTbls
                    from raTbl in raTbls.DefaultIfEmpty()
                    join carrier in cloudEntities.CarrierDict on tracking.CarrierCode equals carrier.Code
                    where raTbl.RtrnAuthNumber.ToString() == ra
                    && tracking.TrackingNumber == trackingNumber
                    && tracking.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       TrackingID = tracking.TrackingID,
                       Name = carrier.Description,
                       Date = tracking.CreatedDate.ToString()
                    }).FirstOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save tracking items information. We had to apply logic from WrhsRecvModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsRecvModel.cs under the function SaveTracking().
      /// </summary>
      /// <param name="entities"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel SaveTracking(WrhsStagingModel entities, string curUser)
      {
         var raInfo = new WrhsStagingModel();

         try
         {
            raInfo = CheckRANumber(entities.RAorWPT);

            if (raInfo.RouteNumber == 100)
            {
               return raInfo;
            }

            var trackingId = Methods.NextId();

            using (var cloudEntities = new QoskCloud())
            {
               var tracks = new Tracking
               {
                  TrackingID = trackingId,
                  TrackingNumber = entities.TrackingNumber,
                  AssocID = raInfo.RAorWPT,
                  WrhsRouteCode = (byte)raInfo.RouteNumber,
                  TrackingTypeCode = "RA",
                  RcvdBy = curUser,
                  RcvdPkgTypeCode = entities.PackageCode,
                  RcvdComment = entities.Notes,
                  CarrierCode = entities.CarrierID,
                  RcvdDate = Convert.ToDateTime(entities.Date),
                  CreatedDate = DateTime.UtcNow,
                  IsDeleted = false,
                  Version = 1,
                  CreatedBy = curUser
               };

               cloudEntities.Tracking.Add(tracks);
               cloudEntities.SaveChanges();

               if (entities.IsDeleted)
               {
                  cloudEntities.Database.ExecuteSqlCommand("UPDATE Tracking SET RcvdComment=@note WHERE CarrierCode=@crId AND RcvdDate=@rcvdDate",
                     new SqlParameter("@crId", entities.CarrierID),
                     new SqlParameter("@rcvdDate", Convert.ToDateTime(entities.Date)),
                     new SqlParameter("@note", entities.Notes));
               }

               SetRAReceivedStatus("Add", raInfo.RAorWPT, trackingId, curUser);
            }

            raInfo.TrackingID = trackingId;
            raInfo.RAorWPT = entities.RAorWPT;
            raInfo.TrackingNumber = entities.TrackingNumber;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
            return new WrhsStagingModel();
         }

         return raInfo;
      }

      ///****************************************************************************
      /// <summary>
      ///   Edit receiving details form.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="entities"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel EditReceiving(WrhsStagingModel entities, string curUser)
      {
         var raInfo = new WrhsStagingModel();

         try
         {
            raInfo = CheckRANumber(entities.RAorWPT);

            if (raInfo.RouteNumber == 100)
            {
               return raInfo;
            }

            using (var cloudEntities = new QoskCloud())
            {
               var existEntity = cloudEntities.Tracking.SingleOrDefault(c => c.TrackingID == entities.TrackingID && c.IsDeleted == false);

               if (existEntity != null)
               {
                  var oldRAID = existEntity.AssocID;

                  existEntity.AssocID = raInfo.RAorWPT;
                  existEntity.TrackingTypeCode = "RA";
                  existEntity.WrhsRouteCode = (byte)raInfo.RouteNumber;
                  existEntity.ModifiedBy = curUser;
                  existEntity.TrackingNumber = entities.TrackingNumber;

                  cloudEntities.SaveChanges();

                  if (oldRAID != raInfo.RAorWPT)
                  {
                     if (string.IsNullOrEmpty(oldRAID))
                     {
                        SetRAReceivedStatus("Add", raInfo.RAorWPT, existEntity.TrackingID, curUser);
                     }
                     else
                     {
                        SetRAReceivedStatus("Delete", oldRAID, existEntity.TrackingID, curUser);
                     }
                  }
               }
            }

            raInfo.TrackingID = entities.TrackingID;
            raInfo.TrackingNumber = entities.TrackingNumber;
            raInfo.RAorWPT = entities.RAorWPT;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
            return new WrhsStagingModel();
         }

         return raInfo;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check validate RA number. We had to apply logic from WrhsRecvModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsRecvModel.cs under the function CheckRANumber().
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel CheckRANumber(string raNumber)
      {
         var routeValue = new WrhsStagingModel();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var dictWhrsRoute = cloudEntities.Database.SqlQuery<WrhsRouteDict>("SELECT * FROM WrhsRouteDict").ToList();
               var routeNumber = 100;

               var sqlBase = "SELECT * FROM ReturnAuthorization WHERE";
               var sqlA = $"{sqlBase} RtrnAuthNumber = {raNumber}";
               var sqlB = $"{sqlBase} ExternRtrnAuthNumber = '{raNumber}'";

               //isNaN will return 0 if it's a non-numeric RA
               var isNaN = new BigInteger();
               BigInteger.TryParse(raNumber, out isNaN);

               var rtrnAuthItem = cloudEntities.Database.SqlQuery<ReturnAuthorization>(isNaN != 0 ? sqlA : sqlB).FirstOrDefault();

               if (rtrnAuthItem == null && isNaN != 0)
               {
                  rtrnAuthItem = cloudEntities.Database.SqlQuery<ReturnAuthorization>(sqlB).FirstOrDefault();
               }

               if (rtrnAuthItem != null)
               {
                  //"closed" RAs should go to their respective Island location
                  var raStatusDict = cloudEntities.RtrnAuthStatusDict.Where(c => c.Code == rtrnAuthItem.RtrnAuthStatusCode).Select(c => c.Classification).FirstOrDefault();

                  if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL2"))
                  {
                     routeNumber = raStatusDict == "C" ? 96 : 2;
                  }
                  else if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL345"))
                  {
                     routeNumber = raStatusDict == "C" ? 97 : 3;
                  }
                  else if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL0"))
                  {
                     routeNumber = raStatusDict == "C" ? 99 : 0;

                     //determine if the non-control, open RA comes from a manufacturer that requires Special Handling; send to SH route if it does (should work just like Floor)
                     if (routeNumber == 0)
                     {
                        var specialHandling = (from dm in cloudEntities.DebitMemo
                           join p in cloudEntities.Profile on dm.MfrProfileCode equals p.ProfileCode
                           where dm.DebitMemoID == rtrnAuthItem.DebitMemoID
                           select p.SpecialHandling).FirstOrDefault();

                        routeNumber = specialHandling ? 6 : 0;
                     }
                  }
               }

               routeValue = dictWhrsRoute.Where(c => c.Code == routeNumber).Select(c => new WrhsStagingModel
               {
                  RouteNumber = c.Code,
                  Route = c.Description,
                  RAorWPT = rtrnAuthItem == null
                     ? string.Empty
                     : rtrnAuthItem.RtrnAuthID
               }).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return routeValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Received RA Status. We had to apply logic from WrhsRecvModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsRecvModel.cs under the function SetRAReceivedStatus().
      /// </summary>
      /// <param name="func"></param>
      /// <param name="raNumber"></param>
      /// <param name="trackingId"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SetRAReceivedStatus(string func, string raNumber, string trackingId, string curUser)
      {
         var status = false;

         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var raInfo = cloudEntities.ReturnAuthorization.FirstOrDefault(c => c.RtrnAuthID == raNumber);

               if (raInfo != null)
               {
                  var rthAuthIdHist = new RtrnAuthStatusHist();

                  if (raInfo.RtrnAuthStatusCode == "A" && func == "Add")
                  {
                     raInfo.RtrnAuthStatusCode = "R";
                     rthAuthIdHist.StatusCode = "R";
                  }
                  else if (raInfo.RtrnAuthStatusCode == "R" && func == "Delete")
                  {
                     if (cloudEntities.Database.SqlQuery<Tracking>("SELECT * FROM TRACKING WHERE AssocID = @ra AND TrackingID <> @trkId AND IsDeleted = 0",
                            new SqlParameter("@ra", raNumber), new SqlParameter("@trkId", trackingId)).FirstOrDefault() == null)
                     {
                        raInfo.RtrnAuthStatusCode = "A";
                        rthAuthIdHist.StatusCode = "A";
                     }
                  }

                  if (!string.IsNullOrWhiteSpace(rthAuthIdHist.StatusCode))
                  {
                     rthAuthIdHist.RtrnAuthID = raNumber;
                     rthAuthIdHist.CreatedDate = DateTime.UtcNow;
                     rthAuthIdHist.CreatedBy = curUser;
                     cloudEntities.RtrnAuthStatusHist.Add(rthAuthIdHist);
                  }

                  cloudEntities.SaveChanges();
                  status = true;
               }
            }
            catch (Exception ex)
            {
               Log.WriteError(ex);
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   form 222 info.
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="ctrlCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetForm222(string ra, string ctrlCode)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = "SELECT isnull(convert(nvarchar,f.PrintedDate,101),'') " +
                        " from Form222 f " +
                        " where f.IsDeleted=0 and f.Status in " +
                        "('Printed','Closed') and f.Form222SourceID=@ra ";
            return ctrlCode == "CTRL2"
                ? cloudEntities.Database.SqlQuery<string>(query, new SqlParameter("@ra", ra)).FirstOrDefault()
                    : "";
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   check tote availability .
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckToteAvailability()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var lstAllTotes = (from wrh in cloudEntities.WrhsContr
                               join wrhcnt in cloudEntities.WrhsContrTypeDict on wrh.Type equals wrhcnt.Code
                               where wrhcnt.Designation == "I"
                               select new { wrh.WrhsContrID }).Distinct().ToList().Select(c => c.WrhsContrID);
            var toteAvailable = new List<string>();
            foreach (var items in lstAllTotes)
            {
               var status = cloudEntities.WrhsContrStatusHist.Where(c => c.WrhsContrID == items)
                 .GroupBy(c => new { c.WrhsContrID, c.WrhsInstID, c.StatusCode, c.CreatedDate })
                 .OrderBy(c => c.Key.CreatedDate).ToList().LastOrDefault();
               if (status != null)
               {
                  if (status.Key.StatusCode == "A")
                  {
                     toteAvailable.Add(items);
                  }
               }
               else
               {
                  toteAvailable.Add(items);
               }
            }

            return toteAvailable.Count > 0 ? true : false;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   check tote availability .
      /// </summary>
      /// <param name="toteCode"></param>
      /// <param name="raCtrlCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckToteValidation(string toteCode, string raCtrlCode)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var lstUsedTotes = (from wrhsContr in cloudEntities.WrhsContr
                                join wrhsType in cloudEntities.WrhsContrTypeDict on wrhsContr.Type equals wrhsType.Code
                                join wrhsStatus in cloudEntities.WrhsContrStatusHist on wrhsContr.WrhsContrID equals wrhsStatus.WrhsContrID into hist
                                from wrhsStatus in hist.DefaultIfEmpty()
                                where wrhsContr.WrhsContrID.ToUpper() == toteCode.ToUpper()
                                      && wrhsType.Designation == "I"
                                      && wrhsContr.WrhsContrID.ToUpper().StartsWith(raCtrlCode == "CTRL2" ? "VT" : "RT")
                                orderby wrhsStatus.CreatedDate descending
                                select new { wrhsContr.WrhsContrID, wrhsStatus.StatusCode }).FirstOrDefault();

            if (lstUsedTotes?.StatusCode == "A")
            {
               var sqlQuery = " select case when exists (select 1 from Item F where exists (select 1 from WrhsContrItemRel G where G.IsDeleted = 0 " +
                              " and G.ItemGUID = F.ItemGUID and UPPER(G.WrhsContrID) = @MyToteID) and not exists (select 1 from ItemLink FF where " +
                              " FF.ItemGUID = F.ItemGUID) and F.TrackingID = (select top 1 HH.TrackingID from WrhsContrTrackingRel HH " +
                              " where UPPER(HH.WrhsContrID) = @MyToteID and HH.IsDeleted = 0 order by HH.CreatedDate desc)) " +
                              " then '0' else '1' end ";

               var qoskQipsSync = cloudEntities.Database.SqlQuery<string>(sqlQuery, new SqlParameter("@MyToteID", toteCode.ToUpper())).FirstOrDefault();

               if (qoskQipsSync != null && qoskQipsSync == "0")
               {
                  return "-2";
               }
            }

            return lstUsedTotes != null
               ? lstUsedTotes.StatusCode == "A"
                  ? lstUsedTotes.WrhsContrID
                  : "-1"
               : "0";
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get tracking id by tracking number or ra.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel CheckTrackingId(string trackingId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from trck in cloudEntities.Tracking
                    join carrier in cloudEntities.CarrierDict on trck.CarrierCode equals carrier.Code
                    join raTbl in cloudEntities.ReturnAuthorization on trck.AssocID equals raTbl.RtrnAuthID into raTbls
                    from raTbl in raTbls.DefaultIfEmpty()
                    where trck.TrackingTypeCode != "NP"
                    && trck.TrackingTypeCode != "NR"
                    && !trck.StgeDate.HasValue
                    && trck.IsDeleted == false
                    && trck.TrackingID == trackingId
                    select new WrhsStagingModel
                    {
                       TrackingID = trck.TrackingID,
                       TrackingNumber = trck.TrackingNumber,
                       RAorWPT = raTbl.RtrnAuthNumber.ToString(),
                       CarrierID = trck.CarrierCode,
                       Carrier = carrier.Description,
                       Date = trck.RcvdDate.ToString()
                    }).FirstOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of information that we know exactly matches.
      /// </summary>
      /// <param name="cloudEntities"></param>
      /// <param name="trackingNumber"></param>
      /// <param name="ra"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetTrackingInformation(string trackingNumber, string ra, QoskCloud cloudEntities)
      {
         return (from trck in cloudEntities.Tracking
                 join carrier in cloudEntities.CarrierDict on trck.CarrierCode equals carrier.Code
                 join raTbl in cloudEntities.ReturnAuthorization on trck.AssocID equals raTbl.RtrnAuthID into raTbls
                 from raTbl in raTbls.DefaultIfEmpty()
                 where trck.TrackingNumber == trackingNumber && raTbl.RtrnAuthNumber.ToString() == ra && trck.TrackingTypeCode != "NP"
                 && trck.TrackingTypeCode != "NR" && !trck.StgeDate.HasValue
                 && trck.IsDeleted == false
                 select new WrhsStagingModel
                 {
                    TrackingID = trck.TrackingID,
                    TrackingNumber = trck.TrackingNumber,
                    RAorWPT = raTbl.RtrnAuthNumber.ToString(),
                    CarrierID = trck.CarrierCode,
                    Carrier = carrier.Description,
                    Date = trck.RcvdDate.ToString()
                 }).ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///   check tracking Id validation.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel CheckValidation(string trackingId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from tracking in cloudEntities.Tracking
                    join carrier in cloudEntities.CarrierDict on tracking.CarrierCode equals carrier.Code
                    where tracking.TrackingID == trackingId && tracking.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       TrackingID = tracking.TrackingID,
                       Name = carrier.Description,
                       Date = tracking.CreatedDate.ToString(),
                       Code = carrier.Code.ToString()
                    }).FirstOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get staged User names.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<string> GetUserNames()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var key = $"GetUserNames";
            var result = MemoryCache.Default[key] as List<string>;

            if (result != null)
            {
               return result;
            }

            result = cloudEntities.Tracking.Where(c => c.StgeDate.HasValue).Select(c => c.StgeBy).Distinct().ToList();
            Common.Methods.SetCache(key, result);
            return result;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get list of carrier for dataList.
      /// </summary>
      /// <param name="cloudEntities"></param>
      /// <param name="strFilterValue"></param>
      /// <param name="strData"></param>
      /// <param name="browser"></param>
      /// <param name="item"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetCarrierList(bool isDeleted, string strData, string strFilterValue, string browser, string item, QoskCloud cloudEntities)
      {
         return browser.Contains("Edge")
            ? (from carrier in cloudEntities.CarrierDict
               where carrier.Description.Contains(strData)
               orderby carrier.DisplayOrder
               select new WrhsStagingModel
               {
                  Code = carrier.Code.ToString(),
                  Name = carrier.Description
               }).Distinct().ToList()
               : (from carrier in cloudEntities.CarrierDict
                  where carrier.Description.Contains(strData)
                  orderby carrier.DisplayOrder
                  select new WrhsStagingModel
                  {
                     Code = carrier.Code.ToString(),
                     Name = carrier.Description
                  }).Take(40).Distinct().ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///   Get All history for current user for get staging information.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <param name="wrhsFltr"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetHistoryList(bool isDeleted, WrhsStagingModel wrhsFltr, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var parameters = new List<SqlParameter>();
            parameters.AddRange(new List<SqlParameter>() {
               new SqlParameter("@isDlt", isDeleted),
               new SqlParameter("@previousCount", wrhsFltr.PreviousCount),
               new SqlParameter("@returnCount", wrhsFltr.PreviousCount == 0 ? wrhsFltr.InitialLoadCount : wrhsFltr.ScrollLoadCount)});

            var condition = "";
            if (!string.IsNullOrWhiteSpace(wrhsFltr.TrackingNumber))
            {
               condition = " t.TrackingNumber like  N'%'+@trackingNumber+'%' ";
               parameters.Add(new SqlParameter("@trackingNumber", wrhsFltr.TrackingNumber));
            }
            if (!string.IsNullOrWhiteSpace(wrhsFltr.RAorWPT))
            {
               parameters.Add(new SqlParameter("@rId", wrhsFltr.RAorWPT));
               condition += (string.IsNullOrWhiteSpace(condition) ? "" : " and ") +
                  (" convert(nvarchar(max), ra.RtrnAuthNumber) like N'%'+@rId+'%'  ");
            }
            DateTime dateValue;
            if (!string.IsNullOrWhiteSpace(wrhsFltr.Date) && !string.IsNullOrWhiteSpace(wrhsFltr.ModifiedDate)
               && DateTime.TryParse(wrhsFltr.Date, out dateValue) && DateTime.TryParse(wrhsFltr.ModifiedDate, out dateValue))
            {
               if (DateTime.Parse(wrhsFltr.Date) > DateTime.Parse(wrhsFltr.ModifiedDate))
               {
                  parameters.AddRange(new List<SqlParameter>{
                  new SqlParameter("@date", wrhsFltr.ModifiedDate),
                  new SqlParameter("@toDate",wrhsFltr.Date)
                  });
               }
               else
               {
                  parameters.AddRange(new List<SqlParameter>{
                  new SqlParameter("@date", wrhsFltr.Date),
                  new SqlParameter("@toDate",wrhsFltr.ModifiedDate)
                  });
               }
               condition += (string.IsNullOrWhiteSpace(condition) ? "" : " and ") + (" (convert(date, t.StgeDate) between convert(date, @date) and convert(date, @toDate) ) ");

            }
            else if (!string.IsNullOrWhiteSpace(wrhsFltr.Date) || !string.IsNullOrWhiteSpace(wrhsFltr.ModifiedDate))
            {
               var fromDate = wrhsFltr.Date;
               var toDate = wrhsFltr.ModifiedDate;

               if (!DateTime.TryParse(wrhsFltr.ModifiedDate, out dateValue))
               {
                  parameters.Add(new SqlParameter("@date", fromDate));
                  condition += (string.IsNullOrWhiteSpace(condition) ? string.Empty : " and ") + (" t.StgeDate >= @date ");
               }
               else if (!DateTime.TryParse(wrhsFltr.Date, out dateValue))
               {
                  parameters.Add(new SqlParameter("@date", toDate));
                  condition += (string.IsNullOrWhiteSpace(condition) ? string.Empty : " and ") + (" t.StgeDate < DATEADD(day, 1, @date) ");
               }
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.Name))
            {
               parameters.Add(new SqlParameter("@username", wrhsFltr.Name));
               condition += (string.IsNullOrWhiteSpace(condition) ? " " : " and ") + " t.StgeBy = @username ";
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.StgeDebitMemoNumber))
            {
               condition += (string.IsNullOrWhiteSpace(condition) ? " " : " and ") + " t.StgeDebitMemoNumber = @dbtMemo ";
               parameters.Add(new SqlParameter("@dbtMemo", wrhsFltr.StgeDebitMemoNumber));
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.Container))
            {
               condition += (string.IsNullOrWhiteSpace(condition) ? " " : " and ") + " trt.WrhsContrID = @tote ";
               parameters.Add(new SqlParameter("@tote", wrhsFltr.Container));
            }

            if (!string.IsNullOrWhiteSpace(condition))
            {
               condition = " and " + condition;
            }

            var sqlQuery = "SELECT DISTINCT t.TrackingID as TrackingID, t.TrackingNumber as TrackingNumber, convert(nvarchar, t.StgeDate,101) as Date, " +
                           " convert(nvarchar, t.ModifiedDate, 101) as ModifiedDate, ra.RtrnAuthCtrlCode as RAorWPTCTRLCode, " +
                           " convert(nvarchar, ra.RtrnAuthNumber) as RAorWPT, t.CarrierCode as CarrierID, cd.Description as Carrier, " +
                           " convert(nvarchar, t.RcvdDate, 101) as RcvdDate, " +
                           " cast(t.WrhsRouteCode as int) as RouteNumber, rtd.Description as Route, 0 as ContainerCount, " +
                           " t.StgeBy as StgeBy, t.StgeDebitMemoNumber as StgeDebitMemoNumber " +
                           " from Tracking t " +
                           " join ReturnAuthorization ra ON t.AssocID = ra.RtrnAuthID " +
                           " join CarrierDict cd ON t.CarrierCode = cd.Code " +
                           " join WrhsContrTrackingRel trt on t.TrackingID = trt.TrackingID and trt.IsDeleted=0 " +
                           " join WrhsRouteDict rtd on t.WrhsRouteCode = rtd.Code " +
                           " where t.IsDeleted = @isDlt and t.TrackingTypeCode not in('NR','NP') and t.StgeDate is not null " +
                           condition +
                           " order by Date desc  OFFSET @previousCount ROWS FETCH NEXT @returnCount ROWS ONLY";

            try
            {
               var historyList = cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery, parameters.ToArray()).ToList();

               //grab tote count; had to do this separate from the list query as it was slowing us down if we filtered by a user
               foreach (var hist in historyList)
               {
                  sqlQuery = " SELECT COUNT(*) FROM TRACKING t INNER JOIN WrhsContrTrackingRel wctr ON wctr.TrackingID = t.TrackingID AND wctr.IsDeleted = 0 " +
                             " WHERE t.IsDeleted = 0 AND t.TrackingTypeCode NOT IN ('NR','NP') AND t.StgeDate IS NOT NULL " +
                             " AND t.TrackingID = '" + hist.TrackingID + "' GROUP BY t.TrackingID";
                  hist.ContainerCount = cloudEntities.Database.SqlQuery<int>(sqlQuery).FirstOrDefault();
               }

               return historyList;
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Download attached files from azure.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SasUriModel> DownloadAttachedFilesFromAzure(string trackingId)
      {
         if (string.IsNullOrWhiteSpace(trackingId))
         {
            return new List<SasUriModel>();
         }
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var lstAttachementFiles = (from attachement in cloudEntities.TrackingAttachment
                                          where (attachement.TrackingID == trackingId
                                          || (attachement.TrackingID != trackingId
                                          && (from trck in cloudEntities.Tracking
                                              where trck.TrackingID == trackingId
                                              select new
                                              {
                                                 lst_trcks = (from trcks2 in cloudEntities.Tracking
                                                              where trcks2.AssocID == trck.AssocID
                                                              && trcks2.TrackingID != trackingId
                                                              select trcks2.TrackingID).ToList()
                                              }).FirstOrDefault().lst_trcks.Contains(attachement.TrackingID)
                                          && attachement.IsShared == true
                                          ))
                                          && !attachement.IsDeleted
                                          select new WrhsStagingModel
                                          {
                                             Code = attachement.SeqNo.ToString(),
                                             Name = attachement.FileName,
                                             IsShared = attachement.IsShared == true ? 1 : 0,
                                             TrackingID = attachement.TrackingID
                                          }).ToList();

               List<SasUriModel> sasURIList = new List<SasUriModel>();
               foreach (var i in lstAttachementFiles)
               {
                  sasURIList.Add(GetFolderItemsListSASUrlBasedOnSearchStaging(Utility.Constants.AzureAccount_KioskSync, Utility.Constants.AzureContainer_AttachedFiles, i.Name, i.Code, i.TrackingID, i.IsShared.ToString()));
               }

               return sasURIList.OrderBy(c => c.ContentSasTimeStamp).ToList();
            }
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Generates SasURI for all the blobs searched by.
      /// </summary>
      /// <param name="account">The Account (without DV)</param>
      /// <param name="containerName">The Container</param>       
      /// <param name="ItemGuid">filename</param>
      /// <param name="SeqNo">seqNo for ordering</param>        
      /// <param name="access">Read or Write</param>
      /// <param name="timeout">Timeout in days</param>
      /// <returns></returns>
      ///****************************************************************************
      public static SasUriModel GetFolderItemsListSASUrlBasedOnSearchStaging(string account, string containerName, string ItemGuid, string seqNo, string trackingId, string isShared, string access = "Read", int timeoutInDays = 10)
      {
         string sasBlobToken = string.Empty;

         var connectString = ConfigurationManager.GetConnectionString(account);
         var storageAccount = CloudStorageAccount.Parse(connectString);

         var blobClient = storageAccount.CreateCloudBlobClient();
         var serviceProperties = blobClient.GetServiceProperties();

         serviceProperties.Cors.CorsRules.Clear();

         serviceProperties.Cors.CorsRules.Add(new CorsRule()
         {
            AllowedHeaders = { Utility.Constants.Triple_Dot },
            AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
            AllowedOrigins = { Utility.Constants.Triple_Dot },
            ExposedHeaders = { Utility.Constants.Triple_Dot },
            MaxAgeInSeconds = 600,

         });

         blobClient.SetServiceProperties(serviceProperties);
         //CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());
         //var container = blobClient.GetContainerReference(containerName.ToLower());
         CloudBlobContainer sampleContainer = blobClient.GetContainerReference(containerName.ToLower());

         var container = sampleContainer.GetBlockBlobReference(ItemGuid);

         var sasConstraints = new SharedAccessBlobPolicy
         {
            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
            SharedAccessExpiryTime = DateTime.UtcNow.AddDays(timeoutInDays),
            Permissions = access.ToLower() == Utility.Constants.FileAccess_Methods_Write ? SharedAccessBlobPermissions.Read |
                 SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List :
                 SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
         };

         //Get list of files containing in this directory
         //CloudBlobDirectory blobDirectory = container.GetDirectoryReference(ItemGuid);

         var listOfBlobs = container;// blobDirectory.ListBlobs();
         SasUriModel SearchedBlobUri = new SasUriModel();
         SasUriModel sasUriModel = null;
         if (listOfBlobs != null && listOfBlobs.Uri.AbsoluteUri.ToLower().Contains(ItemGuid.ToLower()))
         {
            sasUriModel = new SasUriModel();
            CloudBlockBlob blob = (CloudBlockBlob)listOfBlobs;
            sasBlobToken = blob.Uri + blob.GetSharedAccessSignature(sasConstraints);
            sasUriModel.ContentSasURI = sasBlobToken;
            sasUriModel.ContentName = blob.Name + "=" + trackingId + "=" + isShared.ToString();
            var ex = Path.GetExtension(blob.Name).ToUpper();
            sasUriModel.ContentType = imagesFormat.Contains(ex)
                                       ? "Image"
                                       : "File";
            SearchedBlobUri = sasUriModel;

            sasUriModel.ContentSasTimeStamp = seqNo;
         }
         return SearchedBlobUri;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get staging information by tracking ID.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="location"></param>
      /// <param name="reserved"></param>
      /// <param name="searchBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel GetStagingDetails(string trackingId, int location, WrhsStagingModel reserved, string searchBy = null)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var sqlQuery = " SELECT t.TrackingID as TrackingID , t.TrackingNumber as TrackingNumber,convert(nvarchar, t.RcvdDate,101) as Date," +
                              " t.StgeDate as StgeDate,convert(nvarchar,t.StgeDate,101) as ModifiedDate,convert(nvarchar, t.StgeWeight)  as Weight," +
                              " t.StgePkgTypeCode as PackageCode,t.TrackingTypeCode as Code, " +
                              " ra.RtrnAuthCtrlCode as RAorWPTCTRLCode,convert(nvarchar,ra.RtrnAuthNumber) as RAorWPT, rasd.Classification as RaStatusClass, " +
                              " t.CarrierCode as CarrierID, cd.Description as Carrier , pk.Code as PackageCode,pk.Description as PackageName," +
                              " dbt.DebitMemoNumber as DebitMemo, t.StgeComment as Comment , t.AssocID, " +
                              " convert(nvarchar,dbt.MfrProfileCode) as MfrProfileCode, p.Name as MfrProfileName, t.StgeDebitMemoNumber as StgeDebitMemoNumber, " +
                              " ISNULL(convert(bit, p.SpecialHandling),0) as SpecialHandling ,rtd.Description as HandlingMessage,convert(nvarchar,rtd.Code) as Name " +
                              " from Tracking t " +
                              " left outer join ReturnAuthorization ra ON t.AssocID=ra.RtrnAuthID " +
                              " left outer join RtrnAuthStatusDict rasd on rasd.Code = ra.RtrnAuthStatusCode " +
                              " join CarrierDict cd ON t.CarrierCode = cd.Code " +
                              " left outer join WrhsPackageTypeDict pk on t.StgePkgTypeCode = pk.Code " +
                              " left outer join DebitMemo dbt on ra.DebitMemoID = dbt.DebitMemoID " +
                              " left outer join Profile p on dbt.MfrProfileCode = p.ProfileCode " +
                              " join WrhsRouteDict rtd on  rtd.Code =t.WrhsRouteCode " +
                              " where t.IsDeleted=0 and " + (string.IsNullOrWhiteSpace(searchBy) ? "t.TrackingTypeCode not in('NR','NP') and " : "") +
                              " t.TrackingID =@trId  ";

               var obj = cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery, new SqlParameter("@trId", trackingId)).SingleOrDefault();

               if (obj != null)
               {
                  if (obj.Code == "NP" || obj.Code == "NR")
                  {
                     return obj;
                  }

                  obj.RouteNumber = location;
                  if (string.IsNullOrWhiteSpace(obj.ModifiedDate))
                  {
                     if (obj.CarrierID == reserved.CarrierID
                        && obj.Date == reserved.Date
                        && obj.TrackingNumber == reserved.TrackingNumber)
                     {
                        obj.Weight = string.IsNullOrWhiteSpace(obj.Weight) || obj.Weight == "0" ? reserved.Weight : obj.Weight;
                        obj.PackageCndCode = reserved.PackageCndCode.Count == 0 ? GetPackageConditions(obj.TrackingID) : reserved.PackageCndCode;

                        obj.PackageCode = string.IsNullOrWhiteSpace(obj.PackageCode) ? reserved.PackageCode : obj.PackageCode;
                     }
                     else
                     {
                        var lastStagedRecord = GetLastStagedRecord(obj.TrackingNumber, obj.CarrierID, obj.Date);
                        if (lastStagedRecord != null)
                        {
                           obj.Weight = string.IsNullOrWhiteSpace(obj.Weight) || obj.Weight == "0" ? lastStagedRecord.Weight : obj.Weight;
                           obj.PackageCndCode = lastStagedRecord.PackageCndCode.Count == 0 ? GetPackageConditions(obj.TrackingID) : lastStagedRecord.PackageCndCode;
                           obj.PackageCode = string.IsNullOrWhiteSpace(obj.PackageCode) ? lastStagedRecord.PackageCode : obj.PackageCode;
                           obj.PackageName = string.IsNullOrWhiteSpace(obj.PackageName) ? lastStagedRecord.PackageName : obj.PackageName;
                        }
                     }
                  }
                  else
                  {
                     obj.PackageCndCode = GetPackageConditions(obj.TrackingID);
                  }
               }
               return obj;
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// 
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="CarrierCode"></param>
      /// <param name="rcvDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel GetLastStagedRecord(string trackingNumber, string CarrierCode, string rcvDate)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var query = "SELECT convert(nvarchar, t.StgeWeight)  as Weight," +
                " t.StgePkgTypeCode as PackageCode, t.TrackingID as TrackingID," +
                " pk.Code as PackageCode " +
                " from Tracking  t " +
                " left outer join WrhsPackageTypeDict pk on t.StgePkgTypeCode = pk.Code " +
                " where t.IsDeleted=0 and t.TrackingTypeCode not in('NR','NP') and " +
                " t.TrackingNumber =@trk  and t.CarrierCode =@crId  " +
                " and convert(nvarchar,t.RcvdDate,101)=@rcvDt  and t.StgeDate is not null" +
                " order by t.StgeDate desc";
            var objLastRecords = cloudEntities.Database.SqlQuery<WrhsStagingModel>(query
                                          , new SqlParameter("@trk", trackingNumber)
                                          , new SqlParameter("@crId", CarrierCode)
                                          , new SqlParameter("@rcvDt", rcvDate)).FirstOrDefault();
            if (objLastRecords != null)
            {
               objLastRecords.PackageCndCode = GetPackageConditions(objLastRecords.TrackingID);
            }

            return objLastRecords;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   List of package condition from dictionary.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> PackageConditionList()
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from pckCond in cloudEntities.WrhsPackageConditionDict
                    where (pckCond.Code != "PROD"
                    && pckCond.Code != "UNKN")
                    && pckCond.IsDeleted == false
                    orderby pckCond.Code descending
                    select new WrhsStagingModel
                    {
                       PackageConditionCode = pckCond.Code,
                       PackageConditionName = pckCond.Description
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get tote tracking list by tracking id.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetToteTrackingList(string trackingId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var toteList = (from tote in cloudEntities.WrhsContrTrackingRel
                    join status in cloudEntities.WrhsContrStatusHist on new { tote.WrhsContrID, tote.WrhsInstID } equals new { status.WrhsContrID, status.WrhsInstID } into ststHist
                    where tote.TrackingID == trackingId && tote.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       Code = tote.WrhsContrID,
                       Name = tote.WrhsContrID,
                       ControlType = ststHist.OrderByDescending(c => c.CreatedDate).FirstOrDefault().StatusCode,
                       ToteItemCount = (from itemRel in cloudEntities.WrhsContrItemRel
                                        where itemRel.WrhsContrID == tote.WrhsContrID
                                              && itemRel.WrhsInstID == tote.WrhsInstID
                                              && !itemRel.IsDeleted
                                        select itemRel).Count()
                    }).ToList();

            return toteList;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   get list of package type from dictionary.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> PackageTypeList()
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from pckType in cloudEntities.WrhsPackageTypeDict
                    where pckType.Code != "UNKN"
                    && pckType.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       PackageCode = pckType.Code,
                       PackageName = pckType.Description
                    }
                    ).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   get list of package type from dictionary.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckValidateRA(string RA)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from raTbl in cloudEntities.ReturnAuthorization
                    where raTbl.RtrnAuthNumber.ToString() == RA
                    && raTbl.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       RAorWPT = raTbl.RtrnAuthNumber.ToString()
                    }).Count() > 0;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   get list of package type from dictionary.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckValidateDebitMemo(string RA, string DebitMemo)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from raTbl in cloudEntities.ReturnAuthorization
                    join debitMemoTbl in cloudEntities.DebitMemo on raTbl.DebitMemoID equals debitMemoTbl.DebitMemoID
                    where raTbl.RtrnAuthNumber.ToString() == RA
                    && debitMemoTbl.DebitMemoNumber == DebitMemo
                    && debitMemoTbl.DebitMemoID == raTbl.DebitMemoID
                    && debitMemoTbl.IsDeleted == false
                    select new WrhsStagingModel
                    {
                       RAorWPT = raTbl.RtrnAuthID
                    }).Count() > 0;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   get list of RA .
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="wrhsFltr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsStagingModel> GetRAByTrackingNumber(string trackingId, WrhsStagingModel wrhsFltr = null)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var parameters = new List<SqlParameter>();
            parameters.AddRange(new List<SqlParameter>() {
               new SqlParameter("@previousCount", wrhsFltr.PreviousCount),
               new SqlParameter("@returnCount", wrhsFltr.PreviousCount == 0 ? wrhsFltr.InitialLoadCount : wrhsFltr.ScrollLoadCount)});

            var getItems = cloudEntities.Tracking.Where(c => c.TrackingID == trackingId)
                        .Join(cloudEntities.ReturnAuthorization, (c1 => c1.AssocID), (c2 => c2.RtrnAuthID), (c1, c2) => new
                        {
                           RA = c2.RtrnAuthNumber,
                           c1.TrackingNumber,
                           c1.CarrierCode,
                           c1.RcvdDate,
                           c1.AssocID,
                           c2.RtrnAuthCtrlCode
                        })
                        .FirstOrDefault();
            var condition = "";
            var order = "";
            if (getItems.RtrnAuthCtrlCode == "CTRL2")
            {
               condition = " ra.RtrnAuthNumber=" + getItems.RA + " ";
               order = " order by RAorWPT,StgeDate OFFSET @previousCount ROWS FETCH NEXT @returnCount ROWS ONLY ";
            }
            else
            {
               condition = " t.TrackingNumber='" + getItems.TrackingNumber + "' ";
               order = " order by TrackingNumber,StgeDate OFFSET @previousCount ROWS FETCH NEXT @returnCount ROWS ONLY ";
            }

            var sqlQuery = " select t.TrackingID, t.TrackingNumber, convert(nvarchar, t.RcvdDate, 101) as Date, " +
                           " t.StgeDate, convert(nvarchar, t.StgeDate, 101) as ModifiedDate,  ra.RtrnAuthCtrlCode as RAorWPTCTRLCode, convert(nvarchar, ra.RtrnAuthNumber) as RAorWPT, " +
                           " t.CarrierCode as CarrierID, cd.Description as Carrier, isnull(t.StgeDebitMemoNumber, dm.DebitMemoNumber) as DebitMemo, " +
                           " t.StgeBy, rtd.Description as Route, cast(t.WrhsRouteCode as int) as RouteNumber " +
                           " from Tracking t " +
                           " join ReturnAuthorization ra ON t.AssocID = ra.RtrnAuthID " +
                           " join CarrierDict cd ON t.CarrierCode = cd.Code " +
                           " join DebitMemo dm on dm.DebitMemoID = ra.DebitMemoID " +
                           " join WrhsRouteDict rtd on t.WrhsRouteCode = rtd.Code " +
                           " where t.IsDeleted = 0 and t.TrackingTypeCode not in ('NR', 'NP') " +
                           " and " + condition + order;

            try
            {
               return cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery, parameters.ToArray()).ToList();
            }
            catch (Exception ex)
            {
               Log.Write(LogMask.Error, ex.ToString());
            }
            return null;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get form 222 information for a C2 RA
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsStagingModel GetForm222Info(string ra, string rcvDate)
      {
         using (var cloudEntities = new QoskCloud())
         {

            var sqlQuery =
               " SELECT TOP 1 DM.Name AS Form222Name,DM.DebitMemoNumber AS Form222DebitMemoNumber,@rcvDate as DockDate ,isnull(convert(nvarchar,F.PrintedDate,101),'') as Form222PrintedDate, " +
               " CASE WHEN F.PrintedDate IS NULL THEN '' ELSE CAST(DATEDIFF(dd,F.PrintedDate, @rcvDate) AS VARCHAR(10)) END AS TimeElapsed, " +
               " CASE WHEN DM.[3rdPrtyProfileCode] IS NOT NULL THEN ThirdPartyProfile.DEANumber ELSE DM.DEANumber END AS Form222DEANumber, " +
               " CASE WHEN DM.[3rdPrtyProfileCode] IS NOT NULL THEN ThirdPartyProfile.Name ELSE DM.Name END AS ReceivedFrom " +
               " FROM Form222 F INNER JOIN ReturnAuthorization RA on RA.RtrnAuthNumber = F.Form222SourceID and F.Form222Source = '0' " +
               " INNER JOIN DebitMemo DM on DM.DebitMemoID = RA.DebitMemoID " +
               " LEFT JOIN Profile ThirdPartyProfile on ThirdPartyProfile.ProfileCode = DM.[3rdPrtyProfileCode] " +
               " WHERE convert(nvarchar,F.Form222SourceID) = @ra AND F.Status IN ('Printed', 'Closed') ORDER BY F.PrintedDate ";

            var x = cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery, new SqlParameter("@ra", ra), new SqlParameter("@rcvDate", rcvDate)).FirstOrDefault();

            return x ?? new WrhsStagingModel();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Staging Details .
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="lstAttach"></param>
      /// <param name="lstTotes"></param>
      /// <param name="machineId"></param>
      /// <param name="intStep"></param>
      /// <param name="stagingDetail"></param>
      /// <param name="packages"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<WrhsStagingModel, List<string>> SaveStagingDetails(WrhsStagingModel stagingDetail, List<WrhsStagingModel> lstTotes, List<WrhsStagingModel> lstAttach, string machineId, string curUser, int intStep, List<string> packages)
      {
         var fileLists = new List<string>();
         var wrhsStaging = new WrhsStagingModel();
         var dict = new Dictionary<WrhsStagingModel, List<string>> {
            {wrhsStaging,fileLists }
         };
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var existTrackingID = (from trck in cloudEntities.Tracking
                                      where trck.TrackingID == stagingDetail.TrackingID
                                      select trck).SingleOrDefault();

               if (existTrackingID != null)
               {
                  existTrackingID.IsDeleted = false;

                  if (existTrackingID.TrackingNumber != stagingDetail.TrackingNumber)
                  {
                     if (intStep == 2)
                     {
                        var lstTrackingNumbers = cloudEntities.Tracking.Where(c => c.TrackingNumber == existTrackingID.TrackingNumber
                         && c.CarrierCode == existTrackingID.CarrierCode
                         && c.RcvdDate == existTrackingID.RcvdDate
                         && c.TrackingTypeCode != "NP"
                         && c.TrackingTypeCode != "NR"
                         && c.IsDeleted == false).ToList();
                        lstTrackingNumbers.ForEach(c => c.TrackingNumber = stagingDetail.TrackingNumber);
                     }
                     else if (intStep == 1)
                     {
                        existTrackingID.TrackingNumber = stagingDetail.TrackingNumber;
                     }
                  }

                  if (!string.IsNullOrWhiteSpace(stagingDetail.AssocID))
                  {
                     existTrackingID.AssocID = stagingDetail.AssocID;
                  }

                  existTrackingID.StgeDebitMemoNumber = stagingDetail.DebitMemo;
                  existTrackingID.StgePkgTypeCode = stagingDetail.PackageCode;
                  existTrackingID.StgeComment = stagingDetail.Comment;

                  var existsPcks = cloudEntities.TrackingStgePkgConditionRel.Where(c => c.TrackingID == existTrackingID.TrackingID).ToList();

                  foreach (var dlt in existsPcks)
                  {
                     cloudEntities.TrackingStgePkgConditionRel.Remove(dlt);
                     cloudEntities.SaveChanges();
                  }
                  foreach (var pkgs in packages.Distinct().ToList())
                  {
                     var newPkg = new TrackingStgePkgConditionRel
                     {
                        StgePkgConditionCode = pkgs,
                        TrackingID = existTrackingID.TrackingID,
                        Version = 1,
                        IsDeleted = false,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = curUser
                     };

                     cloudEntities.TrackingStgePkgConditionRel.Add(newPkg);
                     cloudEntities.SaveChanges();
                  }

                  existTrackingID.ModifiedBy = curUser;
                  existTrackingID.StgeBy = curUser;
                  existTrackingID.StgeWeight = Convert.ToDecimal(stagingDetail.Weight);

                  if (!existTrackingID.StgeDate.HasValue)
                  {
                     existTrackingID.StgeDate = DateTime.UtcNow;
                  }

                  stagingDetail.Date = existTrackingID.StgeDate?.ToString("MM/dd/yyyy");
                  stagingDetail.ModifiedDate = existTrackingID.ModifiedDate?.ToString("MM/dd/yyyy");

                  var deleteToteObjects = cloudEntities.WrhsContrTrackingRel.Where(c => c.TrackingID == stagingDetail.TrackingID && !c.IsDeleted).ToList();

                  try
                  {
                     cloudEntities.SaveChanges();
                     var dbModel = new QoskCloud() as DBModel;
                     var lstWrhsContrID = new List<string>();
                     var qoskDetails = GetMachineId(machineId);
                     foreach (var dltItems in deleteToteObjects)
                     {
                        if (!lstTotes.Select(c => c.Name).Contains(dltItems.WrhsContrID))
                        {
                           dltItems.IsDeleted = true;

                           var wrhsInstId = cloudEntities.WrhsContrStatusHist
                               .Where(c => c.WrhsContrID == dltItems.WrhsContrID)
                               .OrderByDescending(c => c.CreatedDate).Select(c => c.WrhsInstID).FirstOrDefault();

                           if (wrhsInstId == null)
                           {
                              wrhsInstId = dbModel.Database.SqlQuery<string>("usp_Next_QoskID @Prefix", new SqlParameter("@Prefix", "")).FirstOrDefault();
                           }

                           var histItem = new WrhsContrStatusHist()
                           {
                              WrhsContrID = dltItems.WrhsContrID,
                              WrhsInstID = wrhsInstId,
                              StatusCode = "A",
                              QoskID = qoskDetails.QoskIDx,
                              WrhsContrAttribCode = "I",
                              CreatedBy = curUser,
                              CreatedDate = DateTime.UtcNow,
                              AppCode = "WAREHS_STAGE"
                           };

                           cloudEntities.WrhsContrStatusHist.Add(histItem);
                           cloudEntities.SaveChanges();
                        }
                        else
                        {
                           lstWrhsContrID.Add(dltItems.WrhsContrID);
                        }
                     }

                     foreach (var items in lstTotes)
                     {
                        if (!lstWrhsContrID.Contains(items.Name))
                        {
                           var wrhsInstId = dbModel.Database.SqlQuery<string>("usp_Next_QoskID @Prefix", new SqlParameter("@Prefix", "")).FirstOrDefault();
                           var wrhsTrckRel = cloudEntities.WrhsContrTrackingRel
                                             .FirstOrDefault(c => c.WrhsContrID == items.Name
                                                                  && c.WrhsInstID == wrhsInstId
                                                                  && c.TrackingID == items.TrackingID);
                           if (wrhsTrckRel == null)
                           {
                              var trackingToteObject = new WrhsContrTrackingRel()
                              {
                                 TrackingID = items.TrackingID,
                                 WrhsContrID = items.Name,
                                 IsDeleted = false,
                                 Version = 1,
                                 CreatedBy = curUser,
                                 //QipsID = qoskDetails.QoskID,
                                 CreatedDate = DateTime.UtcNow,
                                 WrhsInstID = wrhsInstId
                              };

                              cloudEntities.WrhsContrTrackingRel.Add(trackingToteObject);
                           }
                           else
                           {
                              wrhsTrckRel.IsDeleted = false;
                              wrhsTrckRel.ModifiedBy = curUser;
                              wrhsTrckRel.ModifiedDate = DateTime.UtcNow;
                           }

                           cloudEntities.SaveChanges();

                           var histItem = new WrhsContrStatusHist()
                           {
                              WrhsContrID = items.Name,
                              WrhsInstID = wrhsInstId,
                              StatusCode = "S",
                              CreatedBy = curUser,
                              QoskID = qoskDetails.QoskIDx,
                              CreatedDate = DateTime.UtcNow,
                              AppCode = "WAREHS_STAGE",
                              WrhsContrAttribCode = "I"
                           };

                           cloudEntities.WrhsContrStatusHist.Add(histItem);
                           cloudEntities.SaveChanges();
                        }
                     }

                     var trckItems = cloudEntities.Tracking
                                    .Where(c => c.AssocID == existTrackingID.AssocID && c.TrackingID != existTrackingID.TrackingID)
                                    .Select(c => c.TrackingID).ToList();

                     var attachmentFiles = (from attachement in cloudEntities.TrackingAttachment
                                            where attachement.TrackingID == existTrackingID.TrackingID
                                            && !attachement.IsDeleted
                                            || (attachement.TrackingID != existTrackingID.TrackingID
                                            && trckItems.Contains(attachement.TrackingID)
                                            && attachement.IsShared == true
                                            && !attachement.IsDeleted
                                            )
                                            select new WrhsStagingModel
                                            {
                                               Code = attachement.SeqNo.ToString(),
                                               Name = attachement.FileName,
                                               IsShared = attachement.IsShared == true ? 1 : 0,
                                               TrackingID = attachement.TrackingID
                                            }).ToList();

                     if (attachmentFiles.Count > 0)
                     {
                        foreach (var attchItems in attachmentFiles)
                        {
                           if (!lstAttach.Select(c => c.Name).Contains(attchItems.Name))
                           {
                              var seqNo = Convert.ToInt16(attchItems.Code);
                              var attchEntity = cloudEntities.TrackingAttachment
                                                .SingleOrDefault(c => c.TrackingID == attchItems.TrackingID && c.FileName == attchItems.Name && c.SeqNo == (byte)seqNo);
                              attchEntity.IsDeleted = true;
                              //DeleteFilesFromBlob(Utility.Constants.AzureContainer_AttachedFiles, attchItems.Name, Utility.Constants.AzureAccount_KioskSync);
                              cloudEntities.SaveChanges();
                           }
                        }
                     }

                     var originalDirectory = new System.IO.DirectoryInfo($"{System.Web.HttpContext.Current.Server.MapPath(@"\")}Areas\\WrhsStaging");
                     var pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");

                     if (!System.IO.Directory.Exists(pathString))
                     {
                        Directory.CreateDirectory(pathString);
                     }

                     string[] fileList = System.IO.Directory.GetFiles(pathString);

                     foreach (var files in fileList)
                     {
                        var fileName = Path.GetFileName(files);

                        if (fileName.Contains(curUser + "_Staging_"))
                        {
                           fileName = fileName.Replace(curUser + "_Staging_", "");
                           var newFileName = curUser + "_Staging_" + Guid.NewGuid() + Path.GetExtension(files);
                           var filesPath = pathString + "\\" + newFileName;
                           File.Move(files, filesPath);

                           var attachment = new TrackingAttachment()
                           {
                              TrackingID = stagingDetail.TrackingID,
                              SeqNo = (byte)((cloudEntities.TrackingAttachment
                                       .Where(c => c.TrackingID == stagingDetail.TrackingID)
                                       .Max(c => (int?)c.SeqNo) ?? 0) + 1),
                              FileName = newFileName,
                              IsDeleted = false,
                              Version = 1,
                              CreatedBy = curUser,
                              CreatedDate = DateTime.UtcNow,
                              IsShared = (Path.GetExtension(files).ToString().ToLower() == ".pdf")
                           };

                           UploadFilesfromLocalToBlob(Utility.Constants.AzureContainer_AttachedFiles, filesPath, Utility.Constants.AzureAccount_KioskSync);
                           cloudEntities.TrackingAttachment.Add(attachment);
                           cloudEntities.SaveChanges();
                           fileLists.Add(fileName);
                        }
                     }

                     SetRAStagedStatus(existTrackingID.AssocID, existTrackingID.TrackingID, curUser);
                     dict = new Dictionary<WrhsStagingModel, List<string>>
                     {
                        { stagingDetail,fileLists }
                     };
                  }
                  catch (Exception ex)
                  {
                     Utility.Logger.Error(ex);
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return dict;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Machine Id
      /// </summary>
      /// <param name="machineId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Qosk GetMachineId(string machineId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from qosk in cloudEntities.Qosk
                    where qosk.MachineID == machineId
                          && !qosk.IsDeleted
                    select qosk).FirstOrDefault();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Machine Id
      /// </summary>
      /// <param name="machineId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<string> GetPackageConditions(string trackingId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from pkg in cloudEntities.TrackingStgePkgConditionRel
                    where pkg.TrackingID == trackingId
                    select pkg.StgePkgConditionCode).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   replace ra
      /// </summary>
      /// <param name="stagingDetail"></param>
      /// <param name="lstTotes"></param>
      /// <param name="lstAttach"></param>
      /// <param name="curUser"></param>
      /// <param name="machineId"></param>
      /// <param name="pkgCodes"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Dictionary<bool, string> ReplaceRa(WrhsStagingModel stagingDetail, List<WrhsStagingModel> lstTotes, List<WrhsStagingModel> lstAttach, string machineId, string curUser, List<string> pkgCodes)
      {
         var fileLists = new List<string>();
         var dict = new Dictionary<bool, string> {
            {false,"" }
         };
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //get Tracking ID that needs to be replaced
               var existTrackingID = (from trck in cloudEntities.Tracking
                                      where trck.TrackingID == stagingDetail.TrackingID
                                      select trck).SingleOrDefault();

               if (existTrackingID != null)
               {
                  existTrackingID.IsDeleted = false;
                  existTrackingID.AssocID = stagingDetail.RAorWPT;
                  existTrackingID.StgeDebitMemoNumber = stagingDetail.DebitMemo;
                  existTrackingID.StgeComment = string.IsNullOrEmpty(stagingDetail.Comment) ? null : stagingDetail.Comment;
                  existTrackingID.StgePkgTypeCode = string.IsNullOrEmpty(stagingDetail.PackageCode) ? null : stagingDetail.PackageCode;
                  existTrackingID.ModifiedBy = curUser;
                  existTrackingID.StgeWeight = stagingDetail.Weight != "0" ? Convert.ToDecimal(stagingDetail.Weight) : (decimal?)null;
                  existTrackingID.WrhsRouteCode = (byte)(stagingDetail.ControlType.Contains("0")
                                                      ? stagingDetail.SpecialHandling
                                                         ? 6
                                                         : 0
                                                      : stagingDetail.ControlType.Contains("2")
                                                         ? 2
                                                         : stagingDetail.ControlType.Contains("345")
                                                               ? 3
                                                               : 1);

                  // get the current package condition, remove them from the listing, and replace with the new conditions
                  var existsPcks = cloudEntities.TrackingStgePkgConditionRel.Where(c => c.TrackingID == existTrackingID.TrackingID).ToList();

                  foreach (var dlt in existsPcks)
                  {
                     cloudEntities.TrackingStgePkgConditionRel.Remove(dlt);
                  }

                  foreach (var pkgs in pkgCodes)
                  {
                     var newPkg = new TrackingStgePkgConditionRel
                     {
                        StgePkgConditionCode = pkgs,
                        TrackingID = existTrackingID.TrackingID,
                        Version = 1,
                        IsDeleted = false,
                        CreatedDate = DateTime.UtcNow,
                        CreatedBy = curUser
                     };
                     cloudEntities.TrackingStgePkgConditionRel.Add(newPkg);
                  }

                  var deleteToteObjects = cloudEntities.WrhsContrTrackingRel.Where(c => c.TrackingID == stagingDetail.TrackingID && c.IsDeleted == false).ToList();

                  try
                  {
                     cloudEntities.SaveChanges();
                     var dbModel = new QoskCloud() as DBModel;
                     var lstWrhsContrID = new List<string>();
                     var qoskDetails = GetMachineId(machineId);
                     foreach (var dltItems in deleteToteObjects)
                     {
                        if (!lstTotes.Select(c => c.Name).Contains(dltItems.WrhsContrID))
                        {
                           dltItems.IsDeleted = true;

                           var wrhsInstId = cloudEntities.WrhsContrStatusHist
                               .Where(c => c.WrhsContrID == dltItems.WrhsContrID)
                               .OrderByDescending(c => c.CreatedDate).Select(c => c.WrhsInstID).FirstOrDefault();

                           if (wrhsInstId == null)
                           {
                              wrhsInstId = dbModel.Database.SqlQuery<string>("usp_Next_QoskID @Prefix", new SqlParameter("@Prefix", "")).FirstOrDefault();
                           }

                           var histItem = new WrhsContrStatusHist()
                           {
                              WrhsContrID = dltItems.WrhsContrID,
                              WrhsInstID = wrhsInstId,
                              StatusCode = "A",
                              QoskID = qoskDetails.QoskIDx,
                              WrhsContrAttribCode = "I",
                              CreatedBy = curUser,
                              CreatedDate = DateTime.UtcNow,
                              AppCode = "WAREHS_STAGE"
                           };

                           cloudEntities.WrhsContrStatusHist.Add(histItem);
                           cloudEntities.SaveChanges();
                        }
                        else
                        {
                           lstWrhsContrID.Add(dltItems.WrhsContrID);
                        }
                     }

                     foreach (var items in lstTotes)
                     {
                        if (!lstWrhsContrID.Contains(items.Name))
                        {
                           var wrhsInstId = dbModel.Database.SqlQuery<string>("usp_Next_QoskID @Prefix", new SqlParameter("@Prefix", "")).FirstOrDefault();
                           var wrhsTrckRel = cloudEntities.WrhsContrTrackingRel
                                             .FirstOrDefault(c => c.WrhsContrID == items.Name
                                                                  && c.WrhsInstID == wrhsInstId
                                                                  && c.TrackingID == items.TrackingID);
                           
                           if (wrhsTrckRel == null)
                           {
                              var trackingToteObject = new WrhsContrTrackingRel()
                              {
                                 TrackingID = items.TrackingID,
                                 WrhsContrID = items.Name,
                                 IsDeleted = false,
                                 Version = 1,
                                 CreatedBy = curUser,
                                 CreatedDate = DateTime.UtcNow,
                                 WrhsInstID = wrhsInstId
                              };
                              cloudEntities.WrhsContrTrackingRel.Add(trackingToteObject);
                           }
                           else
                           {
                              wrhsTrckRel.IsDeleted = false;
                              wrhsTrckRel.ModifiedBy = curUser;
                              wrhsTrckRel.ModifiedDate = DateTime.UtcNow;
                           }

                           cloudEntities.SaveChanges();

                           var histItem = new WrhsContrStatusHist()
                           {
                              WrhsContrID = items.Name,
                              WrhsInstID = wrhsInstId,
                              StatusCode = "S",
                              CreatedBy = curUser,
                              QoskID = qoskDetails.QoskIDx,
                              CreatedDate = DateTime.UtcNow,
                              AppCode = "WAREHS_STAGE",
                              WrhsContrAttribCode = "I"
                           };

                           cloudEntities.WrhsContrStatusHist.Add(histItem);
                           cloudEntities.SaveChanges();
                        }
                     }

                     var trckItems = cloudEntities.Tracking
                                    .Where(c => c.AssocID == existTrackingID.AssocID && c.TrackingID != existTrackingID.TrackingID)
                                    .Select(c => c.TrackingID).ToList();

                     var attachmentFiles = (from attachement in cloudEntities.TrackingAttachment
                                            where attachement.TrackingID == existTrackingID.TrackingID
                                            || (attachement.TrackingID != existTrackingID.TrackingID
                                            && trckItems.Contains(attachement.TrackingID)
                                            && attachement.IsShared == true)
                                            && !attachement.IsDeleted
                                            select new WrhsStagingModel
                                            {
                                               Code = attachement.SeqNo.ToString(),
                                               Name = attachement.FileName,
                                               IsShared = attachement.IsShared == true ? 1 : 0,
                                               TrackingID = attachement.TrackingID
                                            }).ToList();

                     if (attachmentFiles.Count > 0)
                     {
                        foreach (var attchItems in attachmentFiles)
                        {
                           if (!lstAttach.Select(c => c.Name).Contains(attchItems.Name))
                           {
                              var seqNo = Convert.ToInt16(attchItems.Code);
                              var attchEntity = cloudEntities.TrackingAttachment.SingleOrDefault(c => c.TrackingID == attchItems.TrackingID && c.FileName == attchItems.Name && c.SeqNo == (byte)seqNo);
                              attchEntity.IsDeleted = true;
                              //DeleteFilesFromBlob(Utility.Constants.AzureContainer_AttachedFiles, attchItems.Name, Utility.Constants.AzureAccount_KioskSync);
                              cloudEntities.SaveChanges();
                           }
                        }
                     }

                     var originalDirectory = new DirectoryInfo(string.Format("{0}Areas\\WrhsStaging", System.Web.HttpContext.Current.Server.MapPath(@"\")));
                     string pathString = Path.Combine(originalDirectory.ToString(), "AttachedFiles");

                     if (!Directory.Exists(pathString))
                     {
                        Directory.CreateDirectory(pathString);
                     }

                     string[] fileList = Directory.GetFiles(pathString);

                     foreach (var files in fileList)
                     {
                        var fileName = Path.GetFileName(files);

                        if (fileName.Contains(curUser + "_Staging_"))
                        {
                           var newFileName = curUser + "_Staging_" + Guid.NewGuid() + Path.GetExtension(files);
                           var filesPath = pathString + "\\" + newFileName;
                           File.Move(files, filesPath);

                           var attachment = new TrackingAttachment()
                           {
                              TrackingID = stagingDetail.TrackingID,
                              SeqNo = (byte)((cloudEntities.TrackingAttachment
                                       .Where(c => c.TrackingID == stagingDetail.TrackingID)
                                       .Max(c => (int?)c.SeqNo) ?? 0) + 1),
                              FileName = newFileName,
                              IsDeleted = false,
                              Version = 1,
                              CreatedBy = curUser,
                              CreatedDate = DateTime.UtcNow,
                              IsShared = Path.GetExtension(files).ToString().ToLower() == ".pdf"
                           };

                           UploadFilesfromLocalToBlob(Utility.Constants.AzureContainer_AttachedFiles, filesPath, Utility.Constants.AzureAccount_KioskSync);
                           cloudEntities.TrackingAttachment.Add(attachment);
                           cloudEntities.SaveChanges();
                           fileLists.Add(filesPath);
                        }
                     }

                     SetRAStagedStatus(existTrackingID.AssocID, existTrackingID.TrackingID, curUser);
                     dict = new Dictionary<bool, string>
                     {
                        { true,""}
                     };
                  }
                  catch (Exception ex)
                  {
                     Logger.Error(ex);
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return dict;
      }

      ///****************************************************************************
      /// <summary>
      ///   Staged RA Status
      /// </summary>
      /// <param name="raNumber"></param>
      /// <param name="trackingId"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SetRAStagedStatus(string raNumber, string trackingId, string curUser)
      {
         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var raInfo = cloudEntities.ReturnAuthorization.Where(c => c.RtrnAuthID == raNumber && c.RtrnAuthStatusCode == "R").FirstOrDefault();
               var rthAuthIdHist = new RtrnAuthStatusHist();
               if (raInfo != null)
               {
                  raInfo.RtrnAuthStatusCode = "S";
                  rthAuthIdHist.RtrnAuthID = raNumber;
                  rthAuthIdHist.CreatedBy = curUser;
                  rthAuthIdHist.CreatedDate = DateTime.UtcNow;
                  rthAuthIdHist.StatusCode = "S";
                  cloudEntities.RtrnAuthStatusHist.Add(rthAuthIdHist);
                  cloudEntities.SaveChanges();
               }
            }
            catch (Exception ex)
            {
               Utility.Logger.Error(ex);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Count of How Many Unstaged Tracking Number we have.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="machineId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int CountLstUnStagedItems(string trackingId, string machineId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var items = cloudEntities.Tracking.Where(c => c.TrackingID == trackingId).Join(cloudEntities.ReturnAuthorization, (c1 => c1.AssocID), (c2 => c2.RtrnAuthID), (c1, c2) => new
            {
               RA = c2.RtrnAuthNumber,
               c1.TrackingNumber,
               c1.CarrierCode,
               c1.RcvdDate,
               c1.AssocID,
               c2.RtrnAuthCtrlCode
            }).FirstOrDefault();

            //Exception thrown here if there is no tracking number
            if (items == null)
            {
               return 0;
            }

            return items.RtrnAuthCtrlCode == "CTRL2"
               ? cloudEntities.Tracking.Count(c => c.AssocID == items.AssocID
                                                   && !c.StgeDate.HasValue
                                                   && c.IsDeleted == false
                                                   && c.TrackingTypeCode != "NP"
                                                   && c.TrackingTypeCode != "NR")
               : cloudEntities.Tracking.Count(c => c.TrackingNumber.ToUpper() == items.TrackingNumber.ToUpper()
                                                   && !c.StgeDate.HasValue
                                                   && c.IsDeleted == false
                                                   && c.TrackingTypeCode != "NP"
                                                   && c.TrackingTypeCode != "NR"
           );
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Count of How Many Tracking Number we have.
      /// </summary>
      /// <param name="TrackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetCountTrackingNumber(string trackingNumber, string carrier, string date)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return cloudEntities.Database.SqlQuery<object>("select * from tracking where trackingnumber=@tr and carriercode=@cr and IsDeleted=0 and convert(nvarchar,rcvddate,101) = @dt"
               , new SqlParameter("@tr", trackingNumber), new SqlParameter("@cr", carrier), new SqlParameter("@dt", date)).Count();

         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete files from Blob.
      /// </summary>
      /// <param name="accountName"></param>
      /// <param name="container"></param>
      /// <param name="fileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void DeleteFilesFromBlob(string container, string fileName, string accountName)
      {
         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {
            var newPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
                          @"/ProductImages/AttachedFiles/" + fileName;
            if (System.IO.File.Exists(newPath))
            {
               System.IO.File.Delete(newPath);
            }
         }
         else
         {
            var connectString = GetConnectionString(accountName);

            Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());

            sampleContainer.CreateIfNotExists();

            sampleContainer.SetPermissions(new BlobContainerPermissions()
            {
               PublicAccess = BlobContainerPublicAccessType.Container
            });
            CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(fileName);
            blob.DeleteIfExistsAsync();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Upload files from local storage to blob.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="accountName"></param>
      /// <param name="filePath"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void UploadFilesfromLocalToBlob(string container, string filePath, string accountName)
      {
         try
         {
            if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
            {
               var origFilePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"Areas/WrhsStaging/AttachedFiles/" + Path.GetFileName(filePath);
               var newPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
                             @"/ProductImages/AttachedFiles/" + Path.GetFileName(filePath);
               System.IO.File.Copy(origFilePath, newPath, true);
            }
            else
            {
               var connectString = GetConnectionString(accountName);

               Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);

               CloudBlobClient client = account.CreateCloudBlobClient();

               CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());

               sampleContainer.CreateIfNotExists();

               sampleContainer.SetPermissions(new BlobContainerPermissions()
               {
                  PublicAccess = BlobContainerPublicAccessType.Container
               });
               CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(Path.GetFileName(filePath));

               using (Stream file = System.IO.File.OpenRead(filePath))
               {
                  blob.UploadFromStream(file);
               }

            }
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get connection string.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetConnectionString(string account)
      {
         var connectString = string.Empty;

         connectString = ConfigurationManager.GetConnectionString(account);
         if (string.IsNullOrEmpty(connectString))
            throw new ApplicationException(string.Format(Utility.Constants.Azure_ConnectionString_NotDefined_Format, account));
         return connectString;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get tracking NextId.
      /// </summary>
      /// <param name="dbModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetInstanceNewID(ref DBModel dbModel)
      {
         try
         {
            var instanceObj = new NextID(ref dbModel, "");
            return instanceObj.ID;
         }

         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }
         return "";
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }
}
