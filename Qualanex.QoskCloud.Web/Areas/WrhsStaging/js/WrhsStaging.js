﻿//*****************************************************************************************
//*
//* WrhsStaging.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Provides functionality for the Staging partial class.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   // ...
   ///////////////////////////////////////////////////////////////////////////////
   ERR_NORECORDS = "No records found";

   var baseURL = "/WrhsStaging";
   var trackingId = 0;
   var receivingId = 0;
   var stagingDetailObjects = [];
   var validateWeight = false;
   var isHistory = false;
   var trNumber = "";
   var editModeStage = false;
   var segmentId = "WeightCondition";
   var segmentSelector = "#" + segmentId;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   var debitMemoValidate = false;
   var toteList = [];
   var validateUsername = "";
   var carrierCodes = "";
   var dates = "";
   var itemLists = [];
   var attachedFiles = [];
   var finishClick = false;
   var overrideDebitMemo = false;
   var debitMemoEntry = "";
   var checkItem = 0;
   var stationId = "";
   var machineLocation = "";
   var form222PrintDate = "";
   var rcvdDate = "";
   var raId = "";
   var preLoadedWt = "";
   var localStagingDir = "";
   var partialTrackingNumber = "";
   var partialRA = "";
   var $staginEmpty = "";
   var $summaryEmpty = "";
   var $attachmentEmpty = "";
   var reservedTrackingId = "";
   var existSummaryItemsCount = 0;
   var reservedData = [];
   var historySelected = "";
   var loads = false;
   var $lastHistoryFilter = "";
   var historyTabInit = false; //once user clicks into history tab for the first time, do not load list each time the user returns to the same tab
   var table;
   var carrierCode = "";
   var rcvDates = "";
   var searchRslt = false, searchChange = false, tray = false;
   var lastStagingDetailForm = ""; // to get details form html and replace it when clicking on staging ( if we have active record and going to history and click to another record but don't click on continue)
   var lastHistoryForm = ""; // when clicking on the history tab, load the selected html into the form
   var lastSummaryForm = ""; // when clicking on the multi tab, load the selected html into the form
   var newRaMode = false; //status of the "New RA" button

   //these three variables hold the Data Table for each tab (not just the HTML table)
   var partialDt = null;
   var multiDt = null;
   var historyDt = null;

   KEY_TAB = 9;
   KEY_CtrlM = 10; //Note that keyCode = 10 stands for ctrl+m, which is what the barcode scanners use
   KEY_ENTER = 13;

   // Segment violations - Weight related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_WEIGHT = "The content weight cannot be greater than the product weight, please update the weights.";
   ERR_INVALIDPRODUCTWT = "The product weight is invalid. Please capture or re-enter the weight.";
   ERR_INVALIDCONTENTWT = "The content weight is invalid. Please capture or re-enter the weight.";
   ERR_MISSINGPRODUCTWT = "The product weight is required.";
   ERR_MISSINGCONTENTWT = "The content weight is required.";

   // Segment violations - Quantity related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_INVALIDPACKAGE = "The package count is invalid. Please re-enter the value.";
   ERR_INVALIDINDIVIDUAL = "The individual count is invalid. Please re-enter the value.";
   ERR_INVALIDQTY = "The quantity is invalid. Please re-enter the value.";
   ERR_INVALIDCASE = "The case count is invalid. Please re-enter the value.";
   ERR_INVALIDCASESIZE = "The case size is invalid. Please re-enter the value.";
   ERR_MISSINGPACKAGE = "The package count is required.";
   ERR_MISSINGINDIVIDUAL = "The individual count is required.";
   ERR_MISSINGQTY = "The quantity is required.";
   ERR_MISSINGCASE = "The case count is required.";
   ERR_MISSINGCASESIZE = "The case size is required.";

   // Segment violations - Hardware related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_NOSCALE = "There was an error connecting to the scale, please enter weight values manually.";
   ERR_NOACK = "There was an error communicating with the scale, please enter weight values manually.";
   ERR_CALIBRATE = "The scale needs to be calibrated prior to use.";

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).ready(function ()
   {
      try
      {
         $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
         {
            stationId = data;
            machineLocation = getMachineLocation(data).Location;
         });

         $staginEmpty = $(".ql-body.active div.StagingSegment");
         // Get Staging Directory folder

         $.getJSON("https://localhost:8009/Qualanex/http/GetRegistry?regkey=RootStageDoc&default=C:\\Qualanex\\Staging", {}, function (data)
         {
            console.log("localStagingDir: " + data);
            localStagingDir = data;
         });

         window.addGridRefreshMethod(function ()
         {
            $(".dataTables_scrollBody").addClass("QnexScrollbar");
            historyDt = $("#searchGrid").DataTable();
            $("#searchGrid tbody").find("tr:eq(0)").click();
         });

         window.addGridStyleMethod(function ()
         {
            if ($(".ql-body.active").attr("id") === "WrhsHistory")
            {
               window.reshuffleDateRows($(".WrhsStagingDetail-div-click"));
               historyDt = $("#searchGrid").DataTable();
            }
            else if ($(".ql-body.active").attr("id") === "WrhsMulti")
            {
               reorderMultiStatus();
               multiDt = $("#searchGrid_Summary").DataTable();
            }
         });

         // prepare an empty div for new attachments to be added directly to slick slider in Totes and Attachments segment
         $attachmentEmpty =
            "<div class='attachmentFile' data-field-sort='@attchItems.ContentSasTimeStamp' data-row-name='@attchItems.ContentName.Split('=')[0]' style='width: 100px; height: 100px; padding: 10px; margin: 10px; float: left; display: inline - block'>" +
            "<img src='~/Images/SVG/delete (1).svg' style='width: 20px; margin-top:-31px; margin-left:59px; cursor: pointer' class='deleteAttachedFiles' data-row-name='@attchItems.ContentName.Split('=')[1]' data-row-shared='@attchItems.ContentName.Split('=')[2]' />";

         table = window.dtHeaders("#searchGrid", "", ["sorting"], []);
         $summaryEmpty = "<tr class='odd'><td valign='top' colspan='7' class='dataTables_empty'>No data available in table</td></tr></tbody>";
         initialState();
         debitMemoEntry = "";
         $(".relationAddRow").off("change");
         $("#searchGrid th.sorting").removeClass("sorting");
         historyTabInit = false;

         window.dtHeaders("#searchGrid_Summary", "", ["sorting"], []);
         window.enableActionbarControl("#CMD_FILTER", $(".ql-body.active .formExDivFilter").length > 0);
         window.setActionTabCount("WrhsMulti", 0);
         window.selectActionTab("WrhsStaging");

         setTimeout(function ()
         {
            if (stationId === undefined || stationId === "")
            {
               $("#ControlMsg").html("Tray Service Not Found - Please Contact Administrator.");
               tray = true;
            }
            else if (machineLocation === "" || machineLocation === undefined)
            {
               $("#ControlMsg").html("PC Registration Not Found - Please Contact Administrator.");
               tray = true;
            }
         }, 500);

         window.lockCtrl("TrackingNumber", false);
      }
      catch (err)
      {
         console.log(err);
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function reorderMultiStatus()
   {
      try
      {
         var trckNumber = "";
         var itemTrkNo = "";
         var raNumber = "";

         $(".searchGrid_Summary-div-click").each(function ()
         {
            itemTrkNo = $(this).find("td[exchange='TrckNo']").attr("exchange-value");

            if (trckNumber !== itemTrkNo)
            {
               trckNumber = itemTrkNo;
               $(this).find("td[exchange='TrckNo']").removeAttr("style");
            }
            else
            {
               itemTrkNo = "";
            }

            if (raNumber === "" || (raNumber !== $(this).find("td[exchange='RAorWPT']").attr("exchange-value") || trackingId !== $(this).attr("data-row-id")))
            {
               trackingId = $(this).attr("data-row-id");
               raNumber = $(this).find("td[exchange='RAorWPT']").attr("exchange-value");
            }
            else
            {
               $(this).find("td[exchange='RAorWPT']").css({ "background-color": "#808080", "color": "#808080" });
            }
         });
      }
      catch (err)
      {
         console.log(err);
      }

   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Get Machine Location.
   //*
   //* Parameters:
   //*   stId - station id .
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function getMachineLocation(stId)
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "machineName=" + stId + getAntiForgeryToken(),
         async: false,
         url: baseURL + baseURL + "/GetMachineLocation",
         success: function (data)
         {
         }
      });

      return request.responseJSON;
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set up Basic options in the form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function initialState()
   {
      setEditMode(false);
      $(".formMax #TrackingNumber,.formMax #RAorWPT").removeAttr("readonly").removeAttr("disabled");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   saveAndValidate      - Description not available.
   //*   field                - Description not available.
   //*   data                 - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        $("#" + field).val(data);
                     }
               }

               break;
            }

         case "SELECT":
            {
               $("#" + field).val(data);
            }
      }

      if (field === "RAorWPTCtrlCode")
      {
         controlMsgBuilder(data);
      }
      else if (field === "Form222PrintedDate")
      {
         form222PrintDate = data;
      }
      else if (field === "Date")
      {
         dates = data;
      }
      else if (field === "rcvdDate")
      {
         rcvdDate = data;
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Event to handle enabling/disabling of the continue button and arrow upon product weight entry
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", ".formMax .inputRex", function (e)
   {
      if ($(this).attr("id") === "TrackingNumber")
      {
         if (e.keyCode !== KEY_TAB && e.keyCode !== KEY_ENTER && $("#ID_PRODUCTWT").length === 0 && $("#Handling").length === 0 && !$("#RAorWPT").is(":disabled"))
         {
            if ($(this).val() === "")
            {
               if (searchRslt || $("#RAorWPT").val() === "")
               {
                  enableActionbarControl("#CMD_SUBMIT", false);
               }
               else
               {
                  enableActionbarControl("#CMD_SUBMIT", !tray);
                  searchRslt = false;
               }
            }
            else
            {
               enableActionbarControl("#CMD_SUBMIT", $(this).val() !== "" && !tray);
               searchRslt = false;

               if (!tray)
               {
                  $("#ControlMsg").html("");
               }

               searchChange = true;
               newRaMode = false;
            }
         }
      }
      else if ($(this).attr("id") === "RAorWPT")
      {
         if (e.keyCode !== KEY_TAB && e.keyCode !== KEY_ENTER)
         {
            if ($(this).val() === "")
            {
               if (searchRslt || window.lockCtrl("TrackingNumber") || $("#TrackingNumber").val() === "")
               {
                  enableActionbarControl("#CMD_SUBMIT", false);
               }
               else
               {
                  enableActionbarControl("#CMD_SUBMIT", !tray);
                  searchRslt = false;
               }
            }
            else
            {
               enableActionbarControl("#CMD_SUBMIT", $(this).val() !== "" && !tray);
               searchRslt = false;

               if (!tray)
               {
                  $("#ControlMsg").html("");
               }
            }

            searchChange = true;
         }
         else if (e.keyCode === KEY_ENTER)
         {
            $("#CMD_SUBMIT").click();
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Change textboxes value.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".formMax .inputRex", function ()
   {
      var nextValue = "";
      var strValue = "";

      if ($(this).attr("id") === "TrackingNumber")
      {
         $("#TrackingNumber").val($("#TrackingNumber").val().replace(/[^a-zA-Z0-9-]/, ""));
         window.lockCtrl("TrackingNumber", false);

         if ($(this).val() === "" && $("#RAorWPT").val() === "")
         {
            enableActionbarControl("#CMD_SUBMIT", false);
         }
      }
      else if ($(this).attr("id") === "RAorWPT")
      {
         if ($(this).val() === "" && $("#TrackingNumber").val() === "")
         {
            enableActionbarControl("#CMD_SUBMIT", false);
         }
      }

      $("#TrackingNumber").val(window.specialChar($("#TrackingNumber").val(), "SpecialChar")); //remove reserved character added by barcode scanner
      $("#RAorWPT").val(window.specialChar($("#RAorWPT").val(), "SpecialChar")); //remove reserved character added by barcode scanner

      if ($(this).attr("id") === "TrackingNumber" && !editModeStage && existSummaryItemsCount === 0)
      {
         trNumber = "";
      }

      if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "" && $(this).val() !== "")
      {
         $("#" + $(this).attr("nextfocus")).focus();
      }
      else
      {
         $(this).focus();
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   press enter textboxes value.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("keypress", ".formMax .inputRex", function (e)
   {
      if ($(this).attr("id") === "TrackingNumber" && window.lockCtrl("TrackingNumber"))
      {
         window.lockCtrl("TrackingNumber", false);
      }

      if (e.keyCode === KEY_ENTER || e.keyCode === KEY_CtrlM)
      {
         if ($(this).attr("id") === "TrackingNumber")
         {
            $("#TrackingNumber").val($("#TrackingNumber").val().replace(/[^a-zA-Z0-9-]/, ""));
         }

         $("#TrackingNumber").val(window.specialChar($("#TrackingNumber").val(), "SpecialChar")); //remove reserved character added by barcode scanner
         $("#RAorWPT").val(window.specialChar($("#RAorWPT").val(), "SpecialChar")); //remove reserved character added by barcode scanner

         if ($(this).attr("id") === "TrackingNumber" && !editModeStage && existSummaryItemsCount === 0)
         {
            trNumber = "";
         }

         if ($("#TrackingNumber").val().length > 0 && $("#RAorWPT").val().length > 0)
         {
            if ($(this).attr("id") !== "Carrier")
            {
               if (!editModeStage)
               {
                  if (!tray)
                  {
                     $("#ControlMsg").html("");
                  }

                  $("#TrackingNumber").val($("#TrackingNumber").val().replace(/[^a-zA-Z0-9-]/, ""));//gi
                  $("#RAorWPT").removeAttr("disabled readonly");

                  trackingId = 0;

                  if (getEditMode())
                  {
                     initialState();
                  }

                  isHistory = false;

                  //ENTER key forces the load to occur twice (one via this keypress event, and another via a keyup event)
                  //in this case, we're isolating Ctrl + M here, and the submit via ENTER key will be done in the keyup event
                  if (e.keyCode === KEY_CtrlM)
                  {
                     $("#CMD_SUBMIT").click();
                  }
               }
            }
         }
         else
         {
            if ($(this).attr("id") !== "Carrier")
            {
               if (!editModeStage)
               {
                  trackingId = 0;

                  if (getEditMode())
                  {
                     initialState();
                  }

                  //ENTER key forces the load to occur twice (one via this keypress event, and another via a keyup event)
                  //in this case, we're isolating Ctrl + M here, and the submit via ENTER key will be done in the keyup event
                  if (e.keyCode === KEY_CtrlM && (($("#TrackingNumber").val().length > 0 && $("#RAorWPT").val().length === 0) || ($("#TrackingNumber").val().length === 0 && $("#RAorWPT").val().length > 0)))
                  {
                     $("#CMD_SUBMIT").click();
                  }
                  else if ($(".WrhsStagingPartialMatch-div-click").length === 0) 
                  {
                     changeTrackingRaClearStaging();
                  }
               }
            }
         }

         if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "" && $(this).val() !== "")
         {
            $("#" + $(this).attr("nextfocus")).focus();
         }
         else
         {
            $(this).focus();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cut/Paste events on the staging form, which disables/enables the various buttons on the page
   //*   Note that this is wrapped in a setTimeout since the paste event fires before the textbox receives the pasted value
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("cut paste", ".formMax .inputRex", function (e)
   {
      setTimeout(function ()
      {
         $(e.target).focus().keyup();
      }, 150);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Segment collapse/expand function, prevent collapse if special handling is not acknowledged
   //*
   //* Parameters:
   //*   $target - ID of the segment in which the clicked collapse/expand arrow belongs to.
   //*
   //* Returns:
   //*   True, when default processing should proceed. Otherwise; false.
   //*
   //*****************************************************************************
   function OnSegmentCollapse($target)
   {
      var $handlingBtn = $("#Handling").find("#handlingContinue");

      if ($handlingBtn.length > 0 && $handlingBtn.attr("acknowledge") === undefined)
      {
         return false;
      }

      return true;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.OnSegmentCollapse = OnSegmentCollapse;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Currently used to re-slick the attachments portion of Staging when an item is uploaded or deleted
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function slickAttachments()
   {
      var $slickAttach = $(".slick-track div.attachmentFile");

      if ($slickAttach.length)
      {
         var index = $slickAttach.attr("data-slick-index");

         $("#WrhsStagingAttachment .scroll-attachment").slick("unslick");
         $("#WrhsStagingAttachment .scroll-attachment").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            speed: 500
         });

         $("#WrhsStagingAttachment .scroll-attachment").slick("slickGoTo", index);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   check validation of RA number.
   //*
   //* Parameters:
   //*   RAno      - RA number.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_yes_verify", function ()
   {
      trNumber = $("#TrackingNumber").val();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   check validation of RA number.
   //*
   //* Parameters:
   //*   RAno      - RA number.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_no_verify", function ()
   {
      $(this).closeMessageBox();
      $("#TrackingNumber").val(trNumber);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the previous page of container selections
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#containerPrev", function ()
   {
      $(".scroll-container").slick("slickPrev");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the next page of container selections.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#containerNext", function ()
   {
      $(".scroll-container").slick("slickNext");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the previous page of attachments
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#attachmentPrev", function ()
   {
      $(".scroll-attachment").slick("slickPrev");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the next page of attachments
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#attachmentNext", function ()
   {
      $(".scroll-attachment").slick("slickNext");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   These two listeners prevent a user from accidentally opening up an attachment file by misplacing it on the drop area.
   //*   This prevents data loss because QOSK doesn't open up the attachment file in browser window.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.addEventListener("dragover", function (e)
   {
      e = e || event;
      e.preventDefault();
   }, false);
   window.addEventListener("drop", function (e)
   {
      e = e || event;
      e.preventDefault();
   }, false);

   //*****************************************************************************
   //*
   //* Summary:
   //*   search Tracking Number,RPWPT, by input.
   //*
   //* Parameters:
   //*   TrackingNumber      - Tracking Number for get TrackingId.
   //*   RaNumber            - Ra or WPT Number.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkNumbersValidation(TrackingNumber, RaNumber)
   {
      if (TrackingNumber === null && RaNumber === null)
      {
         $("#wait").hide();
         return false;
      }

      $("#wait").show();
      $("#Carrier").val("");
      $("#Carrier").attr("data-field-value", "");
      $("#Date").val("");

      $.ajax({
         cache: false,
         type: "POST",
         data: "trackingNumber=" + TrackingNumber + "&ra=" + RaNumber + "&machineName=" + stationId
         + "&reserved=" + (reservedData.length > 0 ? JSON.stringify(reservedData[0]) : "")
         + "&location=" + machineLocation + getAntiForgeryToken(),
         url: baseURL + baseURL + "/GetTrackingId",
         async: true,
         success: function (data)
         {
            var $target = $("#WrhsStagingDetailsForm");
            searchChange = false;

            if (data.Message.indexOf(",") > 0)
            {
               $("#TrackingNumber").val(data.TrackingNumber);
               $("#RAorWPT").val(data.RA);

               if (data.Received.TrackingID !== null)
               {
                  $("#Carrier").val(data.Received.Carrier);
                  $("#Date").val(data.Received.Date);
                  trackingId = data.Received.TrackingID;
                  $("#Carrier").attr("data-field-value", data.Received.CarrierID);

                  if (window.lockCtrl("TrackingNumber") && trNumber !== "" && trNumber.toLowerCase() !== $("#TrackingNumber").val().toLowerCase())
                  {
                     window.lockCtrl("TrackingNumber", false);
                     trNumber = "";
                  }

                  trNumber = $("#TrackingNumber").val();
                  rcvdDate = data.Received.Date;

                  form222PrintDate = data.Form222Info.Form222PrintedDate;
                  controlMsgBuilder(data.Received.RAorWPTCtrlCode);
                  var $newHtml = populateForm222Segment($(data.Staging));
                  window.setActionTabCount("WrhsMulti", data.Count);

                  $("#RAorWPT").attr("readonly", "readonly");
                  $("#RAorWPT").attr("disabled", "disabled");

                  $newHtml = setContainerAndContentImages($newHtml);
                  $newHtml = disableStagingForm($newHtml);
                  replaceSegments($target, $newHtml);

                  if (data.Received.StgeDate !== null)
                  {
                     if ($("input#handlingChk").length > 0)
                     {
                        $("#handlingContinue").attr("acknowledge", "acknowledge").hide();
                        $("<div id='handlingAck'><img src='/Images/SVG/BoxGreyCheckGreen.svg' style='width:25px' /> <label> Acknowledged <label></div>").insertAfter($("#handlingContinue"));
                        $("input#handlingChk").attr("disabled", "disabled");
                        $("input#handlingChk").prop("checked", true);
                        $("tr[data-segment-group='Handling']").closest("tbody").find("span.QnexTitleFld").text(" - " + $("#handlingMfrName").val());
                     }

                     historySelected = data.Received.ModifiedDate + "," + data.Received.CarrierID;
                     isHistory = true;
                     enableActionbarControl("#CMD_SUBMIT", false);
                     OnQnexEdit();

                     setTimeout(function ()
                     {
                        $("#ID_PRODUCTWT").keyup();
                        setGrapicState("#DM_ENTERED", true);
                        setGrapicState("#btnAddNewTote", true);
                        $("#AttachmentId").attr("disabled", "disabled");
                     }, 1500);
                     //$("#ControlMsg").html("This record has been staged");
                     //enableActionbarControl("#CMD_SUBMIT", false);
                     //$("#wait").hide();
                     //return false;
                  }

                  editModeStage = true;
                  loads = true;

                  if (data.Received.DebitMemo.length > 0)
                  {
                     debitMemoValidate = true;
                     debitMemoEntry = $("input#DebitMemoNumber").val();
                  }

                  if ($("#WrhsStagingDetailsForm div.selected").length === 0)
                  {
                     if (parseFloat($("input#ID_PRODUCTWT").attr("value")) > 0)
                     {
                        openNextSegment("Container", $("#handlingContinue").length > 0 ? "#Handling" : "#WeightCondition");
                        openNextSegment("Totes", "#WeightCondition");
                     }
                     else
                     {
                        if ($("#handlingContinue").length > 0)
                        {
                           openNextSegment("Container", "#Handling");
                        }

                        openNextSegment("WeightCondition", $("#handlingContinue").length > 0 ? "#Handling" : "#Container");
                        openNextSegment("Totes", $("#handlingContinue").length > 0 ? "#Handling" : "#Container");
                     }

                     $("#ToteTable input[type=text],#ToteTable input[type=button]").removeAttr("disabled");

                     //disable the weight segment continue button if no input
                     if ($("#ID_PRODUCTWT").val() !== "")
                     {
                        $("#weightConditionTable input[type=text],#weightConditionTable input[type=button]").removeAttr("disabled");
                     }

                     $("#DebitMemoNumber").val("");
                  }

                  reSelectContainerConditionSegment();
                  $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
                  window.selectActionTab("WrhsStaging");
                  $("#wait").hide();
                  loads = false;
                  setCollapseExpand();
                  enableActionbarControl("#CMD_SUBMIT", false);

                  setTimeout(function ()
                  {
                     var $slickSelected = $(".slick-track div.radio-inline.selected");

                     if ($slickSelected.length)
                     {
                        var index = $(".slick-track div.radio-inline.selected[data-slick-index!=-1]").attr("data-slick-index");
                        $("#ContainerTypeDict .scroll-container").slick("slickGoTo", index);
                     }

                     $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
                     lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
                     lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
                  }, 400);
               }

            }
            else
            {
               if ($("#stgedDate").length > 0)
               {
                  $("#wait").hide();
                  return false;
               }

               var $newHtml = $(data.Staging);

               if (data.Message.indexOf("/") > 0)
               {
                  var splt = data.Message.split("/");

                  if ($.trim(splt[1]) === "4" || $.trim(splt[1]) === "3")
                  {
                     $("#TrackingNumber").val(data.RA);
                     $("#RAorWPT").val(data.TrackingNumber);
                  }

                  $newHtml = setHeaderSegment($newHtml, splt[0]);
                  //$("#ControlMsg").html(splt[0]);
                  enableActionbarControl("#CMD_SUBMIT", !tray);
               }
               else
               {
                  if ($newHtml.find("tr#Container").length > 0)
                  {
                     $newHtml.find("tr#Container td.QnexContent div.panel-body h4").html(data.Message);
                  }
                  else
                  {
                     $newHtml = setHeaderSegment($newHtml, data.Message);
                  }

                  enableActionbarControl("#CMD_SUBMIT", !tray);
               }

               lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
               window.selectActionTab("WrhsStaging");
               replaceSegments($("#WrhsStagingDetailsForm"), $newHtml);
               $(".dataTables_scrollBody").addClass("QnexScrollbar");
               partialDt = $("#WrhsStagingPartialMatch").DataTable();
               $("#wait").hide();
            }
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   set container and content images.
   //*
   //* Parameters:
   //*   $html - html elements from staging.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setContainerAndContentImages($html)
   {
      $("#contents").find("div[data-unique-selection='WeightCondition']").removeClass("selected");
      $("#containers").find("div[data-unique-selection='WeightCondition']").removeClass("selected");

      $html.find("#ContainerCondition div[data-unique-selection='WeightCondition']").removeClass("selected");
      $html.find("#ContainerTypeDict .scroll-container").html($("#containers").html());
      $html.find("div#ContainerCondition").append($("#contents").html());

      var lst = ($html.find("#slctContentCode").val() === ""
         || $html.find("#slctContentCode").val() === undefined
         || $html.find("#slctContentCode").val() === "null")
         ? []
         : JSON.parse($html.find("#slctContentCode").val());

      for (var i = 0; i < lst.length; i++)
      {
         $html.find("#ContainerCondition .ContainerTypeRadio1[value='" + lst[i] + "']").closest("div").addClass("selected");
      }

      $html.find("div#ContainerTypeDict .ContainerTypeRadio1[value='" + $html.find("#slctContainerCode").val() + "']").closest("div").addClass("selected");
      return $html;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check validation by trackingId.
   //*
   //* Parameters:
   //*   trckId - Tracking Id for check validation.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function CheckValidation(trckId)
   {
      if (trckId === "" || trckId === "0" || trckId === null || trckId.indexOf("null") > 0)
      {
         return false;
      }

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingId=" + trckId + getAntiForgeryToken(),
         url: baseURL + baseURL + "/CheckValidation",
         async: true,
         success: function (data)
         {
            trackingId = data.TrackingID;

            if (trackingId === "" || trackingId === undefined)
            {
               trackingId = 0;
               return false;
            }

            $("#Carrier").val(data.Name);
            $("#Carrier").attr("data-field-value", data.Code);
            $("#Date").val(data.Date);
            getStagingDetail(trackingId, false, false);
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   trckRaNumber - Entered RA or Tracking Number value.
   //*   entryField   - Which field we entered Tracking Number or RA Number.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function searchStagingByTrackingId(trckRaNumber, entryField)
   {
      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         data: "trackingField=" + trckRaNumber
         + "&entryField=" + entryField
         + "&reserved=" + (reservedData.length > 0 ? JSON.stringify(reservedData[0]) : "")
         + "&location=" + machineLocation
         + getAntiForgeryToken(),
         url: baseURL + baseURL + "/CheckSingleEntry",
         async: true,
         success: function (data)
         {
            if (data.Staging !== "")
            {
               window.setActionTabCount("WrhsMulti", data.Count);

               carrierCodes = carrierCodes === ""
                  ? reservedData.length > 0
                     ? reservedData[0].CarrierID
                     : $(".searchGrid_Summary-div-click").length > 0
                        ? $("#searchGrid_Summary tr.searchGrid_Summary-div-click:first td[exchange='CarrierID']").closest("td").attr("exchange-value")
                        : ""
                  : carrierCodes;
               dates = dates === ""
                  ? reservedData.length > 0
                     ? reservedData[0].CarrierID
                     : $(".searchGrid_Summary-div-click").length > 0
                        ? $("#searchGrid_Summary tr.searchGrid_Summary-div-click:first td[exchange='Date']").closest("td").attr("exchange-value")
                        : ""
                  : dates;
               var $newHtml = $(data.Staging);

               if (data.Success && data.trackingId !== "0")
               {
                  dates = data.Date;
                  form222PrintDate = data.Form222Info.Form222PrintedDate;
                  $newHtml = populateForm222Segment($(data.Staging));

                  if (data.Ctrl !== null && (data.Type !== "NP" && data.Type !== "NR"))
                  {
                     controlMsgBuilder(data.Ctrl);
                  }

                  $newHtml = setContainerAndContentImages($newHtml);
                  $newHtml = disableStagingForm($newHtml);

                  replaceSegments($("#WrhsStagingDetailsForm"), $newHtml);

                  if (window.lockCtrl("TrackingNumber") && trNumber !== "" && trNumber.toLowerCase() !== data.trackingNumber.toLowerCase())
                  {
                     window.lockCtrl("TrackingNumber", false);
                     trNumber = "";
                  }

                  trNumber = trNumber === "" ? data.trackingNumber : trNumber;
                  trackingId = data.trackingId;
                  $("#Carrier").val(data.Name);
                  $("#Carrier").attr("data-field-value", data.Code);
                  carrierCodes = data.Code;
                  $("#Date").val(data.Date);
                  $("#RAorWPT").val(data.RAorWPT);
                  $("#TrackingNumber").val(data.trackingNumber);

                  if (data.blnStged === 0)
                  {
                     loads = true;
                  }
                  else
                  {
                     $("#handlingContinue").attr("acknowledge", "acknowledge").hide();
                     $("<div id='handlingAck'><img src='/Images/SVG/BoxGreyCheckGreen.svg' style='width:25px' /> <label> Acknowledged <label></div>").insertAfter($("#handlingContinue"));
                     $("input#handlingChk").attr("disabled", "disabled");
                     $("input#handlingChk").prop("checked", true);
                     debitMemoValidate = true;
                     isHistory = true;
                     OnQnexEdit();

                     setTimeout(function ()
                     {
                        $("#ID_PRODUCTWT").keyup();
                        setGrapicState("#DM_ENTERED", true);
                        setGrapicState("#btnAddNewTote", true);
                        $("#AttachmentId").attr("disabled", "disabled");

                        //force scroll to top to appear on first segment
                        $("div.ql-body.active").scrollTop(0);
                     }, 1500);
                  }

                  $("#RAorWPT").attr("readonly", "readonly").attr("disabled", "disabled");
                  editModeStage = true;

                  reSelectContainerConditionSegment();
                  setCollapseExpand();
               }
               else
               {
                  $newHtml = setHeaderSegment($newHtml, data.SearchBy === ""
                     ? ERR_NORECORDS
                     : data.SearchBy);

                  replaceSegments($("#WrhsStagingDetailsForm"), $newHtml);
                  $(".ql-body.active .dataTables_scrollBody").height("auto"); //TODO - hardcoded partial grid height to auto, which allows the grid to flunctuate in size using the search input field
                  partialDt = $("#WrhsStagingPartialMatch").DataTable();
               }
            }
            else
            {
               if (data.Success && data.Type !== undefined)
               {
                  searchRslt = true;

                  if (data.Type === "NR" || data.Type === "NP")
                  {
                     $("#wait").hide();
                     loads = false;
                     $("#ControlMsg").html(data.Type === "NP"
                        ? "This package is not for processing. Please sort it to Island."
                        : "There is no RA for this package. Please sort it to Island.");
                     return false;
                  }
               }

               //clear the partial grid on a no match
               $("#WrhsStagingDetailsForm").replaceWith($staginEmpty);

               if ($("#WrhsStagingDetailsForm tr#Container").length > 0)
               {
                  $("#WrhsStagingDetailsForm tr#Container").find("h4").html(ERR_NORECORDS);
               }

               if (reservedTrackingId === "" && (trackingId === 0 || trackingId === ""))
               {
                  window.setActionTabCount("WrhsMulti", 0);
               }
            }

            lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
            window.selectActionTab("WrhsStaging");
            $(".dataTables_scrollBody").addClass("QnexScrollbar");
            $("#wait").hide();
            loads = false;

            $(".formMax table.tableEx input:not([disabled])").each(function ()
            {
               if ($(this).val() === "")
               {
                  $(this).focus();
                  return false;
               }
            });

            enableActionbarControl("#CMD_SUBMIT", !tray);

            if (data.Success && data.trackingId !== "0")
            {
               setTimeout(function ()
               {
                  console.log("slick");
                  var $slickSelected = $(".slick-track div.radio-inline.selected");

                  if ($slickSelected.length)
                  {
                     var index = $slickSelected.attr("data-slick-index");
                     $("#ContainerTypeDict .scroll-container").slick("slickGoTo", index);
                  }

                  lastStagingDetailForm = $(".slick-track div.radio-inline").length > 0 ? $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone() : "";
                  $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
               }, 450);
            }
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Header for segment 
   //*
   //* Parameters:
   //*   htmlObj - Html which needs to set the title
   //*   message - text of title
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setHeaderSegment(htmlObj, message)
   {
      htmlObj.find(".QoskSegment td.QnexTitle span:first").html(message);
      return htmlObj;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Reselect container and condition 
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function reSelectContainerConditionSegment()
   {
      if (!$("#WrhsStagingDetailsForm div.radio-inline.scroll-item[data-unique-selection='WeightCondition']").hasClass("selected"))
      {
         $("#WrhsStagingDetailsForm div.radio-inline.scroll-item[data-unique-selection='WeightCondition'] input.ContainerTypeRadio1[value='NONE']")
            .closest(".radio-inline").addClass("selected");
      }

      $("#WrhsStagingDetailsForm div.selected").each(function ()
      {
         if ($.trim($("tr[data-segment-group='" + $(this).find(".ContainerTypeRadio1").attr("form") + "']").closest("tbody").find("span.QnexTitleFld").text()).length === 0)
         {
            $("tr[data-segment-group='" + $(this).find(".ContainerTypeRadio1").attr("form") + "']").closest("tbody").find("span.QnexTitleFld").text(" - " + $(this).find("input.ContainerTypeRadio1").attr("data-ql-label"));
         }
         else if ($(this).attr("data-unique-selection") !== "Container")
         {
            $("tr[data-segment-group='" + $(this).find(".ContainerTypeRadio1").attr("form") + "']").closest("tbody").find("span.QnexTitleFld")
               .text($("tr[data-segment-group='" + $(this).find(".ContainerTypeRadio1").attr("form") + "']")
                  .closest("tbody").find("span.QnexTitleFld").text() + "  - " + $(this).find("input.ContainerTypeRadio1").attr("data-ql-label"));
         }
      });

      $("#ID_PRODUCTWT").focus().keyup();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clearContainerConditionHeader()
   {
      $(".QnexTitleFld").not($("tr[data-segment-group='Handling']").closest("tbody").find("span.QnexTitleFld")).text("");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check for special character from barcode scanner; if it's the special
   //*   character, don't add it to barcode (strip it)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#TrackingNumber, #RAorWPT, #DebitMemoNumber, #ToteId, #Comment", function (e)
   {
      if (e.keyCode === KEY_SPECIALCHAR)
      {
         return false;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Prevent enter negavite numbers in past days.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", ".FilterPastDays", function ()
   {
      $(this).val($(this).val().replace(/[^0-9\.]/g, ""));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Press Enter on ProductWeight.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keydown", "#ID_PRODUCTWT", function (e)
   {
      if (e.keyCode === KEY_ENTER)
      {
         $("#weightContinue").click();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Event to handle enabling/disabling of the continue button and arrow upon
   //*   product weight entry.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "#ID_PRODUCTWT", function (e)
   {
      // only enable Confirm button if we have a positive value entered in the weight field, and at least one condition is selected
      if ($(this).val() !== "" && !isNaN($(this).val()) && parseFloat($(this).val()) > 0 && $("div#ContainerCondition div[data-unique-selection='WeightCondition']").hasClass("selected"))
      {
         setGrapicState("#WEIGHT_ENTERED", true);
         $("#weightContinue").removeAttr("disabled");
      }
      else
      {
         setGrapicState("#WEIGHT_ENTERED", false);
         $("#weightContinue").attr("disabled", "disabled");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Staging Details by tracking Id.
   //*
   //* Parameters:
   //*   trkId   - Tracking Id.
   //*   history - is that history or no.
   //*   summary - is that summary or no.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getStagingDetail(trkId, history, summary)
   {
      isHistory = history;

      if (tray)
      {
         $("#wait").hide();
         return false;
      }

      //this implies we're staging an RA previously not received ("No-Tracking-Number")
      if (trkId === "0")
      {
         $("#wait").hide();
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>You Are About To Receive This RA Without A Valid Tracking Record. Would You Like To Continue?</h3>"
         }];

         btnObjects = [{
            "id": "btn_RcvdNoTrk",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='window.receiveNoTrkRa();'"
         }, {
            "id": "btn_no",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='$(this).closeMessageBox();'"
         }
         ];

         titleobjects = [{
            "title": "Confirm Receiving"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         return false;
      }

      if (getEditMode() && (history || summary))
      {
         window.setEditMode(false);

         if (history)
         {
            $("#TrackingNumber,#RAorWPT").removeAttr("readonly disabled");
         }
      }

      $("#WrhsStagingDetailsForm").replaceWith($staginEmpty);
      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingId=" + trkId + "&isHistory=" + (summary ? summary : history) +
         "&reserved=" + (reservedData.length > 0 ? JSON.stringify(reservedData[0]) : "")
         + "&location=" + machineLocation
         + getAntiForgeryToken(),
         url: baseURL + baseURL + "/GetStagingDetail",
         async: true,
         success: function (data)
         {
            window.setActionTabCount("WrhsMulti", data.Count);
            searchChange = false;

            if (window.lockCtrl("TrackingNumber") && trNumber !== "" && $("#TrackingNumber").val().toLowerCase() !== trNumber.toLowerCase())
            {
               window.lockCtrl("TrackingNumber", false);
               trNumber = "";
            }

            trNumber = $("#TrackingNumber").val() === ""
               ? trNumber
               : $("#TrackingNumber").val();

            $("#TrackingNumber").val(trNumber);
            trackingId = trkId;
            debitMemoEntry = "";
            debitMemoValidate = false;
            overrideDebitMemo = false;
            toteList = [];
            editModeStage = false;
            form222PrintDate = data.Form222Info.Form222PrintedDate;

            var $target = $("#WrhsStagingDetailsForm");
            var $newHtml = $(data.Staging);

            $newHtml = setContainerAndContentImages($newHtml);
            $newHtml = disableStagingForm($newHtml);
            //slctContainerCode

            if (parseFloat($newHtml.find("input#ID_PRODUCTWT").attr("value")) > 0)
            {
               preLoadedWt = parseFloat($newHtml.find("input#ID_PRODUCTWT").attr("value"));
            }

            stagingDetailObjects = [];
            $newHtml = populateForm222Segment($newHtml);
            $target.replaceWith($newHtml);
            window.selectActionTab("WrhsStaging");
            controlMsgBuilder(data.RAorWPTCtrlCode);

            //isHistory implies the package has been staged already
            if (isHistory)
            {
               debitMemoValidate = true;
               validateWeight = true;
               debitMemoEntry = $newHtml.find("input#DebitMemoNumber").val();
               OnQnexEdit();

               $("#RAorWPT").attr("readonly", "readonly").attr("disabled", "disabled");

               if ($("input#handlingChk").length > 0)
               {
                  $("#handlingContinue").attr("acknowledge", "acknowledge").hide();
                  $("<div id='handlingAck'><img src='/Images/SVG/BoxGreyCheckGreen.svg' style='width:25px' /> <label> Acknowledged <label></div>").insertAfter($("#handlingContinue"));
                  $("input#handlingChk").attr("disabled", "disabled");
                  $("input#handlingChk").prop("checked", true);
                  $("tr[data-segment-group='Handling']").closest("tbody").find("span.QnexTitleFld").text(" - " + $("#handlingMfrName").val());
               }

               setTimeout(function ()
               {
                  $("#ID_PRODUCTWT").keyup();
                  setGrapicState("#DM_ENTERED", true);
                  setGrapicState("#btnAddNewTote", true);
                  $("#DebitMemoNumber, #AttachmentId").attr("disabled", "disabled");
               }, 1100);
            }
            else
            {
               setEditMode(true);
               editModeStage = true;
               $("#ToteTable input[type=text],#ToteTable input[type=button]").removeAttr("disabled");

               //disable the weight segment continue button if no input
               if ($("#ID_PRODUCTWT").val() !== "")
               {
                  $("#weightConditionTable input[type=text],#weightConditionTable input[type=button]").removeAttr("disabled");
               }

               $("#RAorWPT").attr("readonly", "readonly").attr("disabled", "disabled");

            }

            reSelectContainerConditionSegment();

            if ($("#validRA").val() !== undefined && $("#validRA").val().length > 0)
            {
               $("#RAorWPT").val($("#validRA").val());
            }
            else
            {
               console.log("valid RA null");
               $("#wait").hide();
               return false;
            }

            $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
            // window.checkScaleConditionStaging();
            preLoadedWt = "";

            if (summary)
            {
               $("#Carrier").val($("#searchGrid_Summary tr.selected td[exchange='Carrier']").closest("td").attr("exchange-value"));
               $("#Carrier").attr("data-field-value", $("#searchGrid_Summary tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value"));
               $("#Date").val($("#searchGrid_Summary tr.selected td[exchange='Date']").closest("td").attr("exchange-value"));
               controlMsgBuilder($("#searchGrid_Summary tr.selected td[exchange='ControlMsg']").closest("td").attr("exchange-value"));
            }

            enableActionbarControl("#CMD_SUBMIT", false);
            $("#wait").hide();
            loads = false;

            setCollapseExpand();

            setTimeout(function ()
            {
               var index = null;
               var $slickSelected = $(".slick-track div.radio-inline.selected");

               if ($slickSelected.length)
               {
                  index = $slickSelected.attr("data-slick-index");
                  $("#ContainerTypeDict .scroll-container").slick("slickGoTo", index);
               }

               //SLICK CURRENTLY NOT BEING IMPLEMENTED, WILL BE UTILIZED LATER
               //var $slickAttach = $(".slick-track div.attachmentFile");
               //if ($slickAttach.length)
               //{
               //   index = $slickAttach.attr("data-slick-index");
               //   $("#WrhsStagingAttachment .scroll-attachment").slick("slickGoTo", index);
               //}

               $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");

               //clone the forms to use on forms while switching tabs
               lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
               lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();

               //force scroll to top to appear on first segment
               $("div.ql-body.active").scrollTop(0);
            }, 500);
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Collapse expand 
   //*
   //* Parameters:
   //*   None 
   //*
   //* Returns:
   //*   None
   //*
   //****************************************************************************
   function setCollapseExpand()
   {
      if ($(".searchGrid_Tote-div-click").length === 0)
      {
         if ($("#handlingAck").length === 0)
         {
            if ($("#WrhsStagingDetailsForm .radio-inline.selected").length >= 2
               && $("#handlingContinue").length === 0
               && $("#Form222Continue").length === 0)
            {
               //collapse the container type segment if it's already filled out
               if ($("div#ContainerTypeDict div[data-unique-selection='Container']").hasClass("selected"))
               {
                  $("tr#Container[data-segment-group='Container']").slideCollapse();
               }

               //attempt to collapse the weight segment via the Continue button if the weight is preloaded
               //otherwise collapse the totes segment and focus on the weight segment
               setTimeout(function ()
               {
                  if ($("#ID_PRODUCTWT").val() !== undefined && parseFloat($("#ID_PRODUCTWT").val()) > 0)
                  {
                     $("#weightContinue").click();
                  }
                  else
                  {
                     $("tr#Totes[data-segment-group='Totes']").slideCollapse();
                  }
               }, 300);

            }
            else
            {
               $("#WrhsStagingDetailsForm").find(".toCollapse").slideCollapse();
            }
         }
         else
         {
            $("#WrhsStagingDetailsForm").find(".toCollapse").slideCollapse();
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Disable staging form if one of our totes are not staged 
   //*
   //* Parameters:
   //*   forms - Get Form information
   //*
   //* Returns:
   //*   Forms
   //*
   //****************************************************************************
   function disableStagingForm(forms)
   {
      if (parseInt(forms.find("input#toteDisabled").val()) > 0)
      {
         forms.find("input[type='text'],input[type='button'],input,button,textarea,.buttonEx,.btnErrorMessage,.radio-inline,.deleteAttachedFiles").attr("disabled", "disabled");
         forms.find("input[type='text'],input[type='button'],input,button,textarea,.buttonEx,.btnErrorMessage,.radio-inline,.deleteAttachedFiles").attr("readonly", "readonly");
         $(".deleteAttachedFiles").off("click");
      }

      return forms;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Generate a message based on the RA/WPT control number. For C2s,
   //*   determine the number of days that an RA/ WPT has its form 222
   //*   printed.
   //*
   //* Parameters:
   //*   ctrlNbr - String type - contrl number of RA/WPT
   //*
   //* Returns:w
   //*   null
   //*
   //****************************************************************************
   function controlMsgBuilder(ctrlNbr)
   {
      var controlMsgText = "";

      var dateCalc = rcvdDate ? rcvdDate : dates;
      var form222PrintDateDiff = Math.round((new Date(dateCalc).getTime() - new Date(form222PrintDate).getTime()) / (24 * 3600 * 1000)).toString();

      if (ctrlNbr === "" || ctrlNbr === undefined)
      {
         controlMsgText = "Error Retrieving Control Number Of RA/WPT";
         $("#imgCtrlNbr").attr("src", "");
      }
      else if (ctrlNbr === "CTRL2")
      {
         $("#imgCtrlNbr").attr("src", "/images/Form/Qosk_Ctrl2.svg");

         if (form222PrintDate === undefined || form222PrintDate === null || form222PrintDate === "")
         {
            //controlMsgText = "NO FORM 222 PRINTED";
         }
         else
         {
            //controlMsgText = form222PrintDateDiff + " Day(s) Since 222 Form Print Date";
            if (parseInt(form222PrintDateDiff) > 60)
            {
               controlMsgText = controlMsgText.toUpperCase();
            }
            else if (parseInt(form222PrintDateDiff) < 0)
            {
               //controlMsgText = "Form 222 Printed After The Received Date";
               controlMsgText = controlMsgText.toUpperCase();
            }
         }
      }
      else if (ctrlNbr === "CTRL345")
      {
         //controlMsgText = ctrlNbr;
         $("#imgCtrlNbr").attr("src", "/images/Form/Qosk_Ctrl345.svg");
      }
      else if (ctrlNbr === "CTRL0")
      {
         //controlMsgText = ctrlNbr;
         $("#imgCtrlNbr").attr("src", "/images/Form/Qosk_Ctrl0.svg");
      }
      else
      {
         controlMsgText = "Error Retrieving Control Number Of RA/WPT";
         $("#imgCtrlNbr").attr("src", "");
      }

      if (!tray)
      {
         $("#ControlMsg").html(controlMsgText);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   control - ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function populateForm222Segment(control)
   {
      //if it's a non-C2, the segment should not appear on screen
      if (!$(control).find("#Form222Table").length)
      {
         return $(control);
      }

      //form 222 segment
      var $x = $(control);
      var days = parseInt($(control).find("#TimeElapsed").text());
      var form222Str = "";

      $x.find("#TimeElapsed").html((days >= 0 ? days.toString() + " Day(s)" : ""));

      if (form222PrintDate === undefined || form222PrintDate === null || form222PrintDate === "")
      {
         form222Str = "NO FORM 222 PRINTED";
         $x.find("#Form222Msg").html(form222Str);
         $(this).setErrorMessage("No Form 222 Has Been Printed For This C2 RA. Staging Can Continue. Please See Your Supervisor.", "Error Message", "Continue");
      }
      else if (days > 60)
      {
         form222Str = "NEW FORM 222 REQUIRED";
         $x.find("#Form222Msg").html(form222Str);
         $(this).setErrorMessage("This C2 RA Has A Printed Form 222 That's Beyond The 60 Day Threshold. Staging Can Continue. Please See Your Supervisor.", "Error Message", "Continue");
      }
      else if (days < 0)
      {
         form222Str = "FORM 222 PRINTED AFTER THE RECEIVED DATE";
         $x.find("#Form222Msg").html(form222Str);
         $x.find("#TimeElapsed").html("&nbsp;");
      }
      //end form 222 segment

      return $x;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
      console.log("print");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.getEditMode = function (e)
   {
      if ($(".ql-body.active[id='WrhsHistory']").length > 0
         || $(".ql-body.active[id='WrhsMulti']").length > 0
         || $(".ql-body.active[id='WrhsStaging']").length > 0)
      {
         return false;
      }
      return true;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexFilter = function ()
   {
      $(".filterFrame .FilterStgedFromDate").datepicker();
      $(".filterFrame .FilterStgedFromDate").val($(".ql-body .FilterStgedFromDate").val());
      $(".filterFrame .FilterStgedToDate").datepicker();
      $(".filterFrame .FilterStgedToDate").val($(".ql-body .FilterStgedToDate").val());

      //each time we open up the filter, the background color resets, which means the user doesn't know if there's an error on the input fields
      if ($(".filterFrame .FilterStgedFromDate").hasClass("inputError"))
      {
         $(".filterFrame .FilterStgedFromDate").removeAttr("style").removeClass("inputError");
      }

      if ($(".filterFrame .FilterStgedToDate").hasClass("inputError"))
      {
         $(".filterFrame .FilterStgedToDate").removeAttr("style").removeClass("inputError");
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   click function on container type and condition .
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ContainerTypeRadio1", function (e)
   {
      if (isHistory || $(this).closest("div").attr("disabled") !== undefined)
      {
         return false;
      }

      if ($(this).closest("div").attr("data-unique-selection") !== "WeightCondition")
      {
         openNextSegment($(this).closest("div").attr("data-unique-selection"), $(this).attr("data-ql-next"));
      }

      if ($(this).closest("div").attr("data-unique-selection") === "WeightCondition")
      {
         if ($(e.target).val() === "NONE")
         {
            $("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "]").removeClass("selected");
         }
         else
         {
            if ($("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "] input.ContainerTypeRadio1[value='NONE']").closest("div.radio-inline").hasClass("selected"))
            {
               $("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "] input.ContainerTypeRadio1[value='NONE']").closest(".radio-inline").removeClass("selected");
            }
         }
      }
      else
      {
         $("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "]").removeClass("selected");
      }

      if ($(this).closest("div").hasClass("selected")
         && $(this).closest("div").attr("data-unique-selection") === "WeightCondition"
         && !loads)
      {
         $(this).closest("div").removeClass("selected");

         //if we de-select the container/content damaged buttons, and we have no other conditions selected, defaut to No Damage
         if (!$("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "]").hasClass("selected"))
         {
            $("div.radio-inline.scroll-item[data-unique-selection=" + $(this).closest("div").attr("data-unique-selection") + "] input.ContainerTypeRadio1[value='NONE']").click();
         }
      }
      else
      {
         $(this).closest("div").addClass("selected");
      }

      clearContainerConditionHeader();
      reSelectContainerConditionSegment();

      $("#weightConditionTable input[type=text],#weightConditionTable .ContainerTypeRadio1").removeAttr("disabled");
      //window.checkScaleConditionStaging();

      if ($(this).attr("form") === "Container")
      {
         if ($("#ID_PRODUCTWT").val() === "0")
         {
            $("#ID_PRODUCTWT").val("");
         }

         $("#ID_PRODUCTWT").focus();
      }

      $("#ID_PRODUCTWT").keyup();

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get container's header.
   //*
   //* Parameters:
   //*   cntr       - container segment name.
   //*
   //* Returns:
   //*   Tr from title.
   //*
   //*****************************************************************************
   function getHeaderContainer(cntr)
   {
      return $("tr#" + cntr).closest("tbody").find("td.QnexTitle span:first");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   check tote validation.
   //*
   //* Parameters:
   //*   toteId       - WrhsContrlID.
   //*   row          - row information.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkTotesValidation(toteId, isToteReEntered, callback)
   {
      if (isToteReEntered)
      {
         var data =
            {
               Success: true,
               Error: false,
               ValidTote: toteId
            }

         return callback(data);
      }

      var ctrlNum = $("#validRaCtrlCode").val();

      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         async: true,
         url: baseURL + baseURL + "/CheckToteValidation",
         data: "toteCode=" + toteId + "&raCtrlCode=" + ctrlNum + getAntiForgeryToken(),
         success: function (data)
         {
            callback(data);
         }
      });

      try
      {
         $.ajax(request);
      }
      catch (e)
      {
         throw e;
      }

      ga("send", "event", "Staging", "checkTotesValidation");
      return false;
      //return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*  Arrow Button for Add Totes
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".btn_Check_Tote", function ()
   {
      checkTotes();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   keypress event in Totes textbox; check tote if enter is pressed
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#ToteId", function (e)
   {
      if (e.keyCode === KEY_CtrlM || e.keyCode === KEY_ENTER)
      {
         checkTotes();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   check tote available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkTotes()
   {
      var attSrc = $("#btnAddNewTote").attr("src");

      if (attSrc.indexOf("(Inverted)") > 0)
      {
         return false;
      }

      if ($("#ToteId").val() === "")
      {
         $("#ErrorTote").css("display", "none");
         $("#toteLabel").css("display", "inline-block");
         $("#ToteId").val("");
         $("#ToteId").focus();
         return false;
      }

      $("#ToteId").val(window.specialChar($("#ToteId").val(), "SpecialChar")); //remove reserved character added by barcode scanner

      var validToteName = $("#ToteId").val().toUpperCase();
      var validTote = false;
      var toteReEntry = false; //used only in the case of a tote being initially staged, then deleted, then staged again in the same "staging session"
      var $row = "<tr class='searchGrid_Tote-div-click {0}' name='SearchListItem' wrhsstgetote-method='post' data-row-id='" + $(".searchGrid_Tote-div-click").length + 1;
      $row += "' data-ql-ajax='true' wrhsstgetote-data='WrhsStgeTote={&quot;Code&quot;:&quot;" + validToteName + "&quot;,&quot;Name&quot;:&quot;" + validToteName;
      $row += "&quot;,&quot;ModifiedDate&quot;:null,&quot;Carrier&quot;:null,&quot;RAorWPT&quot;:null,&quot;TrackingID&quot;:null,&quot;TrackingNumber&quot;:null,&quot;CarrierID&quot;";
      $row += ":null,&quot;Date&quot;:null,&quot;Container&quot;:null,&quot;PackageCode&quot;:null,&quot;PackageName&quot;:null,&quot;PackageConditionCode&quot;:null,&quot;PackageConditionName&quot;";
      $row += ":null,&quot;Weight&quot;:null,&quot;weightCondition&quot;:null,&quot;DebitMemo&quot;:null,&quot;IsShared&quot;:0,&quot;Comment&quot;:null,&quot;IsDisposed&quot;:false}' wrhsstgetote-action='' wrhsstgetote-target='#formMax' role='row'>";
      $row += "<td exchange='Code' exchange-value='validCode' style='border-right:1px solid white'>validCode</td><td exchange='checkBox' exchange-value='1'><input class='CheckboxRex' type='checkbox' /></td><td exchange='Name' exchange-value='validCode' hidden>validCode</td></tr>";

      for (var item = 0; item < toteList.length; item++)
      {
         if (toteList[item].Tote.toUpperCase() === validToteName && toteList[item].Tote !== "")
         {
            $row = $row.replace(/validCode/gi, validToteName);
            $row = $row.replace("{0}", $(".searchGrid_Tote-div-click:last").hasClass("odd") ? "even" : "odd");
            $("#ToteId").val("");
            toteList.splice(item, 1);
            $("#ToteId").focus();
            toteReEntry = true;
         }
      }

      if (!validTote)
      {
         if ($(".searchGrid_Tote-div-click td[exchange='Name'][exchange-value='" + validToteName + "']").length > 0)
         {
            $("#ErrorTote").html("Tote Not Available");
            $("#ToteId").val("");
            $("#ToteId").focus();
            $("#ErrorTote").css("display", "inline-block");
            $("#toteLabel").css("display", "none");
            return false;
         }

         checkTotesValidation(validToteName, toteReEntry, function (data)
         {
            if (data.Success)
            {
               validToteName = data.ValidTote;
               $row = $row.replace(/validCode/gi, validToteName);
               $row = $row.replace("{0}", $(".searchGrid_Tote-div-click:last").hasClass("odd") ? "even" : "odd");
               $("#ToteId").val("");
               $("#ToteId").focus();
               validTote = true;
               $("#DebitMemoNumber").attr("disabled", "disabled").attr("validTote", true);
            }
            else
            {
               validTote = false;

               if (!validTote)
               {
                  if (data.ValidTote === "-1")
                  {
                     $("#ErrorTote").html("Tote Not Available");
                     $("#ErrorTote").css("display", "inline-block");
                     $("#toteLabel").css("display", "none");
                     //inprocess
                  }
                  else if (data.ValidTote === "-2")
                  {
                     $("#ErrorTote").html("Items Not Sent To QIPS");
                     $("#ErrorTote").css("display", "inline-block");
                     $("#toteLabel").css("display", "none");
                     //QOSK items have not been transferred over to QIPS
                  }
                  else
                  {
                     $("#ErrorTote").html("Tote Not Found");
                     $("#ErrorTote").css("display", "inline-block");
                     $("#toteLabel").css("display", "none");
                     //not found
                  }

                  $("#ToteId").val("");
                  $("#ToteId").focus();
               }
            }

            if (validTote || $(".searchGrid_Tote-div-click td[exchange='Name'][exchange-value='" + validToteName + "']").length > 0)
            {
               if (!validTote)
               {
                  $row = $row.replace(/validCode/gi, validToteName);
                  $row = $row.replace("{0}", $(".searchGrid_Tote-div-click:last").hasClass("odd") ? "even" : "odd");
               }

               if ($(".searchGrid_Tote-div-click td[exchange='Name'][exchange-value='" + validToteName + "']").length === 0)
               {
                  if ($(".searchGrid_Tote-div-click:last").length > 0)
                  {
                     $($row).insertAfter($(".searchGrid_Tote-div-click:last").closest("tr"));
                  }
                  else
                  {
                     $($row).insertAfter($("#searchGrid_Tote tr.odd").closest("tr"));
                     $("#searchGrid_Tote tr").not(".searchGrid_Tote-div-click").closest("tr").remove();
                  }

                  $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
               }

               $("#ErrorTote").html("");
               $("#ErrorTote").css("display", "none");
               $("#toteLabel").css("display", "inline-block");
            }
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   "Receive" a No Tracking RA prior to staging
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function receiveNoTrkRa()
   {
      $(this).closeMessageBox();
      var $partial = $(".WrhsStagingPartialMatch-div-click.selected");

      //get Today's date as Received Date
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!
      var yyyy = today.getFullYear();

      if (dd < 10)
      {
         dd = '0' + dd;
      }

      if (mm < 10)
      {
         mm = '0' + mm;
      }
      today = mm + '/' + dd + '/' + yyyy;

      var items = [{
         "TrackingNumber": $("#TrackingNumber").val(),
         "RaOrWpt": $("#RAorWPT").val(),
         "TrackingID": "0",
         "CarrierID": $partial.closest("tr").find("td[exchange='CarrierID']").attr("exchange-value"),
         "Date": today,
         "Notes": "Item Received Directly At Staging",
         "PackageCode": "UNKN", // from receiving applet, we receive all packages as UNKN
         "IsDeleted": false
      }];

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingEntity=" + JSON.stringify(items) + getAntiForgeryToken(),
         url: baseURL + baseURL + "/NewReceivingSave",
         async: true,
         success: function (data)
         {
            if (data.Success)
            {
               $partial.closest("tr").find("td[exchange='TrackingID']").attr("exchange-value", data.Tracking.TrackingID);
               $partial.closest("tr").find("td[exchange='Date']").attr("exchange-value", today);
               $("#Date").val(today);
               rcvdDate = today;

               setTimeout(function ()
               {
                  getStagingDetail(data.Tracking.TrackingID, false, false);
               }, 200);
            }
         },
         error: function (data)
         {
            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }
   window.receiveNoTrkRa = receiveNoTrkRa;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Override a received RA with the new RA entered by the user (part of New RA button functionality)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function newRaOverride()
   {
      $(this).closeMessageBox();
      var $partial = $(".WrhsStagingPartialMatch-div-click.selected");

      var items = [{
         "TrackingNumber": $("#TrackingNumber").val(),
         "TypeCode": "RA",
         "RaOrWpt": $("#RAorWPT").val(),
         "TrackingID": $partial.closest("tr").find("td[exchange='TrackingID']").attr("exchange-value"),
         "CarrierID": $partial.closest("tr").find("td[exchange='CarrierID']").attr("exchange-value"),
         "Notes": "",
         "IsDeleted": false
      }];

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingEntity=" + JSON.stringify(items) + getAntiForgeryToken(),
         url: baseURL + baseURL + "/NewReceivingSave",
         async: true,
         success: function (data)
         {
            if (data.Success)
            {
               //controlMsgBuilder($multi.find("td[exchange='ControlMsg']").closest("td").attr("exchange-value"));
               setTimeout(function ()
               {
                  getStagingDetail(data.Tracking.TrackingID, false, false);
               }, 200);
            }
            else
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> RA " + $("#RAorWPT").val() + " Not Found. Please Try Again.</h3>"
               }];

               btnObjects = [{
                  "id": "btn_ok",
                  "name": "OK",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
               ];

               titleobjects = [{
                  "title": "Return Authorization Retrieval Message"
               }];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
               return false;
            }
         },
         error: function (data)
         {
            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }
   window.newRaOverride = newRaOverride;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete button of checked totes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function DeleteToteItems()
   {
      $(this).closeMessageBox();
      var $empty = "<tr class='odd'><td valign='top' colspan='2' class='dataTables_empty'>No data available in table</td></tr>";
      $("#searchGrid_Tote tr.searchGrid_Tote-div-click").find("input[type='checkbox']:checked").each(function ()
      {
         toteList.push({
            "Tote": $(this).closest("tr").find("td[exchange='Name']").attr("exchange-value")
         });

         $(this).closest("tr").remove();
      });

      if ($(".searchGrid_Tote-div-click").length === 0)
      {
         $("#searchGrid_Tote tbody").append($empty);
      }

      $("#ToteId").focus();
   }
   window.DeleteToteItems = DeleteToteItems;

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on the Acknowledge button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#handlingContinue", function ()
   {
      if ($("#handlingChk").attr("name") === "Special")
      {
         $(this).attr("acknowledge", "acknowledge");
         openNextSegment("Handling", "#" + $.trim($(this).attr("form")));
         $("#handlingChk").removeAttr("disabled");
         $("tr[data-segment-group='Handling']").closest("tbody").find("span.QnexTitleFld").text(" - " + $("#handlingMfrName").val());
      }
      else
      {
         reservedTrackingId = trackingId;
         changeTrackingRaClearStaging();
         $("#TrackingNumber,#RAorWPT").val("").removeAttr("readonly disabled");

         //keep the current locked tracking number form if we're on the floor and are working on a multi-box tracking
         if (window.lockCtrl("TrackingNumber"))
         {
            $("#TrackingNumber").val(trNumber);
            $("#RAorWPT").focus();
            lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
            lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
            enableActionbarControl("#CMD_SUBMIT", !tray);
         }
         else
         {
            $("#TrackingNumber").focus();
            lastStagingDetailForm = "";
            lastSummaryForm = "";
            enableActionbarControl("#CMD_SUBMIT", false);
            setEmptySummary();
         }

         editModeStage = false;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   control - ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#handlingAck", function ()
   {
      openNextSegment("Handling", "#Container");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   control - ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   //$(document).on("change", "#handlingChk", function()
   //{
   //   if (!$(this).is(":checked"))
   //   {
   //      $("#handlingContinue").show();
   //      if ($("#handlingAck").length > 0)
   //      {
   //         $("#handlingAck").hide();
   //      }
   //   }
   //   else
   //   {
   //      $("#handlingContinue").hide();
   //      if ($("#handlingAck").length > 0)
   //      {
   //         $("#handlingAck").show();
   //      }
   //      else
   //      {
   //         $("<div id='handlingAck'><img src='/Images/SVG/BoxGreyCheckGreen.svg' style='width:25px' /> <label> Acknowledged <label></div>").insertAfter($("#handlingContinue"));
   //      }
   //   }
   //});

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check/Uncheck - handling checkbox when clicking on span.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#handlingSpan", function ()
   {
      if ($("#handlingChk").attr("disabled") !== undefined && $("#handlingChk").attr("disabled") !== "")
      {
         return false;
      }

      $("#handlingChk").prop("checked", !$("#handlingChk").is(":checked"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete button of each totes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".deleteToteRelation", function ()
   {
      if ($("#searchGrid_Tote tr.searchGrid_Tote-div-click").find("input[type='checkbox']:checked").length > 0)
      {
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>Are you sure you want to delete these records?</h3>"
         }];

         btnObjects = [{
            "id": "btn_deleteTotes",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='window.DeleteToteItems();'"
         }, {
            "id": "btn_no",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='$(this).closeMessageBox();'"
         }
         ];

         titleobjects = [{
            "title": "Confirm Delete"
         }];

         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
      }

      //if (!$(this).closest("tr").hasClass("blank"))
      //{
      //   if ($(this).closest("tr").find("td input[name='Name']").closest("input").val() !== undefined
      //      && $(this).closest("tr").find("td input[name='Name']").closest("input").val() !== "")
      //   {
      //      toteList.push({
      //         "Tote": $(this).closest("tr").find("td input[name='Name']").closest("input").val()
      //      });
      //   }

      //   $(this).deleteRelationRow();
      //}
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   open segment function.
   //*
   //* Parameters:
   //*   current     - get current segment id.
   //*   next        - get next segment id.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function openNextSegment(current, next)
   {
      if (current.length > 0)
      {
         var $attrCurrent = $("img.collapse-arrow[data-target='#" + $.trim(current) + "']").attr("src");

         if ($attrCurrent !== undefined && $attrCurrent.indexOf("Qosk_Arrow_Up.png") > 0)
         {
            $("img.collapse-arrow[data-target='#" + $.trim(current) + "']").closest("img").trigger("click");
         }
      }

      var $attrNext = $("img.collapse-arrow[data-target='" + $.trim(next) + "']").attr("src");

      if ($attrNext !== undefined && $attrNext.indexOf("Qosk_Arrow_Down.png") > 0)
      {
         $("img.collapse-arrow[data-target='" + $.trim(next) + "']").closest("img").trigger("click");
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on weight continue button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#AttachContinue", function ()
   {
      if ($("#DebitMemoNumber").val().length === 0)
      {
         $(this).setErrorMessage("No Debit Memo number has been entered. ", "Error Message", "Continue");
         $("#DebitMemoNumber").focus();
         finishClick = false;
         return false;
      }

      if ($("#DebitMemoNumber").attr("disabled") !== undefined)
      {
         finishClick = false;
         return false;
      }

      if (CompareTextboxes($("#hiddenDebitMemo").val(), $("#DebitMemoNumber").val())
         && CompareTextboxes($("#DebitMemoNumber").val(), $("#hiddenDebitMemo").val()))
      {
         debitMemoValidate = true;
      }

      if (!debitMemoValidate || !CompareTextboxes($("#hiddenDebitMemo").val(), $("#DebitMemoNumber").val()))
      {
         debitMemoValidate = false;
         $(this).setErrorMessage("The Debit Memo number entered does not match the active RA.", "Error Message", "Continue");
         finishClick = false;
         return false;
      }

      openNextSegment("Attachment", "#Totes");
      $("#ToteId").focus();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on form 222 continue button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#Form222Continue", function ()
   {
      openNextSegment("Form222", "#" + $(this).closest(".QoskSegment").next().find("tr.panel-collapse").attr("id"));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on weight continue button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#weightContinue", function ()
   {
      $("#ID_PRODUCTWT").val(window.specialChar($("#ID_PRODUCTWT").val(), "SpecialChar")); //remove reserved character added by barcode scanner

      if (isHistory)
      {
         finishClick = false;
         return false;
      }

      if (!$("div[data-unique-selection='WeightCondition']").hasClass("selected")
         || $("#ID_PRODUCTWT").val().length === 0 || $("#ID_PRODUCTWT").val() === "0")
      {
         finishClick = false;
         $(segmentSelector + " .QnexMessage .QnexCaption").html((!$("div[data-unique-selection='WeightCondition']").hasClass("selected")
            && ($("#ID_PRODUCTWT").val().length === 0 || $("#ID_PRODUCTWT").val() === "0") ? " Please select condition and enter the weight"
            : !$("div[data-unique-selection='WeightCondition']").hasClass("selected")
               ? parseFloat($("#ID_PRODUCTWT").val()) < 0
                  ? "Condition is required and Weight must be positive."
                  : "Condition is required."
               : "Weight is required."));
         $("#ID_PRODUCTWT").focus();
         return false;
      }

      if (parseFloat($("#ID_PRODUCTWT").val()) < 0)
      {
         $(segmentSelector + " .QnexMessage .QnexCaption").html("Weight must be positive");
         openNextSegment($.trim($(this).attr("form")), "#WeightCondition");
         $("#ID_PRODUCTWT").focus();
         $("#ID_PRODUCTWT").select();
         finishClick = false;
         return false;
      }

      if (parseFloat($("#ID_PRODUCTWT").val()) > 10000)
      {
         $(segmentSelector + " .QnexMessage .QnexCaption").html("Weight must not exceed 10,000 lbs");
         openNextSegment($.trim($(this).attr("form")), "#WeightCondition");
         $("#ID_PRODUCTWT").focus();
         $("#ID_PRODUCTWT").select();
         finishClick = false;
         return false;
      }

      validateWeight = true;
      $(segmentSelector + " .QnexMessage .QnexCaption").html("");
      $("#ToteTable input[type=text],ToteTable input[type=button]").not("#AttachmentId,#ToteId").removeAttr("disabled");

      //do not allow the debit memo number field to be enabled again after pressing the continue button
      //the code just above this momentarily enables that field after a DM number has been entered
      if ($("#DebitMemoNumber").attr("validTote") !== undefined && $("#DebitMemoNumber").attr("validTote") === "true")
      {
         $("#DebitMemoNumber").attr("disabled", "disabled");
         $("#ToteId").removeAttr("disabled");
      }

      openNextSegment("WeightCondition", "#" + $.trim($(this).attr("form")));

      setTimeout(function ()
      {
         if (!debitMemoValidate)
         {
            $("#DebitMemoNumber").focus();
         }
      }, 500);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Finish button in Totes and Attachment segment.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#Totefinish", function ()
   {
      if (finishClick)
      {
         return false;
      }

      finishClick = true;
      document.getElementById("Totefinish").style.pointerEvents = "auto";

      if (!$(".ContainerTypeRadio1[form='Container']").closest("div").hasClass("selected"))
      {
         $(this).setErrorMessage("Container is required.", "Error Message", "Continue");
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         return false;
      }

      if ($("#ID_PRODUCTWT").val().length === 0)
      {
         $(this).setErrorMessage("Weight is required.", "Error Message", "Continue");
         $("#CMD_SCALETARE1").click();
         $("#ID_PRODUCTWT").focus();
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         return false;
      }
      else
      {
         if (parseFloat($("#ID_PRODUCTWT").val()) < 0)
         {
            $(this).setErrorMessage("Weight must be positive", "Error Message", "Continue");
            $("#CMD_SCALETARE1").click();
            openNextSegment("", "#WeightCondition");
            $("#ID_PRODUCTWT").focus();
            document.getElementById("Totefinish").style.pointerEvents = "";
            finishClick = false;
            return false;
         }
         else if (parseFloat($("#ID_PRODUCTWT").val()) > 10000)
         {
            $(this).setErrorMessage("Weight must not exceed 10,000 lbs", "Error Message", "Continue");
            $("#CMD_SCALETARE1").click();
            openNextSegment("", "#WeightCondition");
            $("#ID_PRODUCTWT").focus();
            document.getElementById("Totefinish").style.pointerEvents = "";
            finishClick = false;
            return false;
         }
         else
         {
            if ($(".ContainerTypeRadio1[form='WeightCondition']").closest("div").hasClass("selected"))
            {
               validateWeight = true;
               $("#ToteTable input[type=text],ToteTable input[type=button]").not("#AttachmentId,#DebitMemoNumber").removeAttr("disabled");

               if ($("img.collapse-arrow[data-target='#Totes']").closest("img").attr("src").indexOf("Down.png") > 0)
               {
                  openNextSegment("WeightCondition", "#Totes");
               }
            }
            else
            {
               $(this).setErrorMessage("Condition is required.", "Error Message", "Continue");
               document.getElementById("Totefinish").style.pointerEvents = "";
               finishClick = false;
               return false;
            }
         }
      }

      if (!validateWeight)
      {
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         return false;
      }

      if (CompareTextboxes($("#hiddenDebitMemo").val(), $("#DebitMemoNumber").val())
         && CompareTextboxes($("#DebitMemoNumber").val(), $("#hiddenDebitMemo").val()))
      {
         debitMemoValidate = true;
      }

      if (!overrideDebitMemo && !debitMemoValidate)
      {
         if ($("#DebitMemoNumber").val() === "")
         {
            $(this).setErrorMessage("No Debit Memo number has been entered. ", "Error Message", "Continue");
         }
         else
         {
            $(this).setErrorMessage("The Debit Memo number entered does not match the active RA.", "Error Message", "Continue");
         }

         $("#DebitMemoNumber").focus();
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         return false;
      }

      //if (debitMemoValidate
      //   || $("#hiddenDebitMemo").val().toUpperCase() === $("#DebitMemoNumber").val().toUpperCase())
      //{
      debitMemoValidate = true;

      if ($("#handlingChk").length > 0)
      {
         //both the special handling and the totes/attachment segments should be completed
         if ($("#handlingContinue").attr("acknowledge") === undefined)
         {
            $(this).setErrorMessage("Acknowledgment of Special Handling segment is required.", "Error Message", "Continue");
            finishClick = false;
            return false;
         }

         if (!$("#handlingChk").is(":checked"))
         {
            $(this).setErrorMessage($("#handlingChk").attr("name") === "Special"
               ? "Acknowledgment of special handling is required."
               : "You are in a wrong station.", "Error Message", "Continue");
            finishClick = false;
            return false;
         }
      }

      finishStaging();
      //}
      //else
      //{
      //   debitMemoValidate = false;
      //   $(this).setErrorMessage("The Debit Memo number entered does not match the active RA.", "Error Message", "Continue");
      //   finishClick = false;
      //}
   });

   ////*****************************************************************************
   ////*
   ////* Summary:
   ////*   change event for debit memo number textbox.
   ////*
   ////* Parameters:
   ////*   None.
   ////*
   ////* Returns:
   ////*   None.
   ////*
   ////*****************************************************************************
   //$(document).on("change", "#DebitMemoNumber", function()
   //{
   //   debitMemoValidate = false;
   //});

   ////*****************************************************************************
   ////*
   ////* Summary:
   ////*   focus out event for debit memo number textbox.
   ////*
   ////* Parameters:
   ////*   None.
   ////*
   ////* Returns:
   ////*   None.
   ////*
   ////*****************************************************************************
   //$(document).on("focusout", "#DebitMemoNumber", function()
   //{
   //   $("#debitMemoAccept").closest("div").css("display", "none");
   //   overrideDebitMemo = false;
   //   var previousEntry = false;
   //   if ($(this).val() !== "")
   //   {
   //      if (debitMemoEntry === $(this).val().toUpperCase())
   //      {
   //         previousEntry = true;
   //      }
   //      debitMemoEntry = $(this).val().toUpperCase();

   //      if ($(this).val().toUpperCase() !== $("#hiddenDebitMemo").val().toUpperCase()
   //                  && !debitMemoValidate)
   //      {
   //         $("#debitMemoError").css("display", "inline-block");
   //      }
   //      else
   //      {
   //         if ($(this).val().toUpperCase() === $("#hiddenDebitMemo").val().toUpperCase())
   //         {
   //            debitMemoValidate = true;
   //            debitMemoEntry = "";
   //            $("#debitMemoError").css("display", "none");
   //         }
   //         else
   //         {
   //            $("#debitMemoError").css("display", "inline-block");
   //         }
   //      }
   //   }

   //   if (!debitMemoValidate)
   //   {
   //      if ($(this).val() !== "")
   //      {
   //         if (previousEntry)
   //         {
   //            $("#debitMemoAccept").closest("div").css("display", "inline-block");
   //            $("#matchDebitMemoMessage").html("Press accept to continue or try again.");
   //            $(this).focus();
   //         }
   //         else
   //         {
   //            $("#matchDebitMemoMessage").html("Try again.");
   //            $(this).focus();
   //            $(this).val("");
   //         }
   //      }
   //      else
   //      {
   //         $("#debitMemoError").css("display", "inline-block");
   //         $("#debitMemoAccept").closest("div").css("display", "inline-block");
   //         $("#matchDebitMemoMessage").html("Press accept to continue or try again.");
   //         $(this).focus();
   //      }
   //   }
   //   else
   //   {
   //      $("#debitMemoError").css("display", "none");
   //   }
   //});

   //*****************************************************************************
   //*
   //* Summary:
   //*   change event for debit memo number textbox.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#debitMemoAccept", function (e)
   {
      //cannot override debit memo with a blank field
      if ($("#debitMemoSearchField").val() === undefined || $("#debitMemoSearchField").val() === "")
      {
         $("#debitMemoSearchField").focus();
         return false;
      }

      overrideDebitMemo = true;
      debitMemoValidate = true;
      $("#DebitMemoNumber").val($("#debitMemoSearchField").val());
      checkItem = 0;
      $("#debitMemoError").css("display", "none");
      $(".errorButtonsRow").css("display", "none");
      $(".okButtonRow").removeAttr("style");
      $(".debitMemoTable").removeClass("errorCondition");
      debitMemoValidated(true);
      $(this).closeMessageBox();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   keydown event in debit memo
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keydown", "#DebitMemoNumber", function (e)
   {
      var theEvent = e || window.event;
      var keyCode = theEvent.keyCode || theEvent.which;

      //console.log(".editable::keydown:" + keyCode);
      debitMemoValidate = false;
      overrideDebitMemo = false;

      //$(".errorButtonsRow").css("display", "none");
      $("#debitMemoError").css("display", "none");
      $(".debitMemoTable").removeClass("errorCondition");

      switch (keyCode)
      {
         case KEY_TAB:
            e.preventDefault();
            $("#ToteId").focus();
            break;

         case KEY_ENTER:
            $("#EnterDebitMemo").click();
            //$("#divToteSegment tr.relationAddRow.blank input[name='Code']").focus();
            break;

         default:
            break;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   keydown event in debit memo
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#DebitMemoNumber", function (e)
   {
      if (e.keyCode === KEY_CtrlM)
      {
         $("#EnterDebitMemo").click();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   focus in function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusin", "#DebitMemoNumber", function ()
   {
      if (!debitMemoValidate)
      {
         // hide the error message and buttons if they appear
         if ($(".errorButtonsRow").attr("style").indexOf("none") < 0)
         {
            $(".errorButtonsRow").css("display", "none");
            $("#debitMemoError").css("display", "none");
            $(".debitMemoTable").removeClass("errorCondition");
         }

         // show the add debit memo button if it doesn't appear
         if ($(".okButtonRow").attr("style") !== undefined && $(".okButtonRow").attr("style").indexOf("none") > 0)
         {
            $(".okButtonRow").removeAttr("style");
         }

         if ($(this).val() !== "")
         {
            debitMemoEntry = $(this).val().toUpperCase();
         }

         $(this).val("").keyup();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   keyup event for Debit Memo field
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "#DebitMemoNumber", function ()
   {
      if ($(this).val() !== "")
      {
         $("#EnterDebitMemo").removeAttr("disabled");
      }
      else
      {
         $("#EnterDebitMemo, #Totefinish").attr("disabled", "disabled");
         $(".totesPanel input, .attachmentsPanel input, #Comment").attr("disabled", "disabled");
         setGrapicState("#DM_ENTERED", false);
         setGrapicState("#btnAddNewTote", false);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check Debit Memo Validation.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#EnterDebitMemo", function ()
   {
      if (debitMemoValidate)
      {
         return false;
      }

      $("#DebitMemoNumber").val(window.specialChar($("#DebitMemoNumber").val(), "SpecialChar")); //remove reserved character added by barcode scanner

      $(".errorButtonsRow").css("display", "none");
      $(".debitMemoTable").removeClass("errorCondition");
      overrideDebitMemo = false;
      var previousEntry = false;

      if ($("#DebitMemoNumber").val() !== "")
      {
         if (CompareTextboxes(debitMemoEntry, $("#DebitMemoNumber").val()) && CompareTextboxes($("#DebitMemoNumber").val(), debitMemoEntry))
         {
            checkItem = 0;
            previousEntry = true;
         }

         debitMemoEntry = $("#DebitMemoNumber").val().toUpperCase();

         if (!CompareTextboxes($("#hiddenDebitMemo").val(), $("#DebitMemoNumber").val()) && !debitMemoValidate)
         {
            $("#debitMemoError").css("display", "inline-block");
         }
         else
         {
            if (CompareTextboxes($("#hiddenDebitMemo").val(), $("#DebitMemoNumber").val()) && CompareTextboxes($("#DebitMemoNumber").val(), $("#hiddenDebitMemo").val()))
            {
               checkItem = 0;
               debitMemoValidate = true;
               debitMemoEntry = "";
               $("#debitMemoError").css("display", "none");
               $(".debitMemoTable").removeClass("errorCondition");
            }
            else
            {
               debitMemoValidate = false;
               $("#debitMemoError").css("display", "inline-block");
               $(".debitMemoTable").addClass("errorCondition");
            }
         }
      }
      else
      {
         checkItem++;

         if (debitMemoEntry === $("#DebitMemoNumber").val() && checkItem >= 2)
         {
            previousEntry = true;
         }

         debitMemoEntry = "";
      }

      $("#debitMemoError").css("display", "inline-block");
      $(".debitMemoTable").addClass("errorCondition");

      if (!debitMemoValidate)
      {
         if (previousEntry)
         {
            $(".errorButtonsRow").css("display", "table-row");
            $(".okButtonRow").css("display", "none");
            $("#matchDebitMemoMessage").html("This is the Value We Have - " + $("#hiddenDebitMemo").val() + " <br/>Try Again");
            $("#debitMemoSearch").click();
         }
         else
         {
            $("#matchDebitMemoMessage").html("Try Again.");
            $(".okButtonRow").css("display", "table-row");
            $("#DebitMemoNumber").val("").focus().keyup();
         }
      }
      else
      {
         $("#debitMemoError").css("display", "none");
         $(".debitMemoTable").removeClass("errorCondition");
         debitMemoValidated(true);
      }
   });

   //====================================================Debit Memo Lookup =======

   //*****************************************************************************
   //*
   //* Summary:
   //*   enter on accept button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#debitMemoSearch", function (e)
   {
      $("#wait").show();
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "debitMemo=" + encodeURIComponent($("#DebitMemoNumber").val()) + getAntiForgeryToken(),
         url: baseURL + baseURL + "/RALookup",
         async: true,
         success: function (data)
         {
            //$("#CustomMessageBoxListing").html(data);
            $(this).addToContent(data);
            $(this).showMessageBox([{ "title": "RA Lookup" }]);
            $("#CustomMessageBoxEntity").css("overflow", "none");
            window.dtHeaders("#searchGridLookup", "max-height:400px,ordering : false", ["sorting"], []);
            $(".dataTables_scrollBody").css("max-height", "200px").addClass("QnexScrollbar");
            $("#searchGridLookup_filter").css("display", "none");
            $("#wait").hide();
            $("#search_Lookup").click();
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   keydown event in debit memo
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".Searching", function (e)
   {
      if ($(this).hasClass("IsBegin"))
      {
         $("input[type='radio'][id='IsBegin']").prop("checked", "checked");
      }
      else
      {
         $("input[type='radio'][id='IsContain']").prop("checked", "checked");
      }
      //$("input[name='Search'][id='" + $(this).hasClass("IsBegin") ? "IsBegin" : "IsContain" + "']").click();
      //.click();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   search debit memo.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function searchDebitMemo()
   {
      $("#timeoutMsg").html("");
      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "searchArg=" + $("input[type='radio'][name='Search']:checked").val() + "&debitMemo=" + encodeURIComponent($("#debitMemoSearchField").val()) + getAntiForgeryToken(),
         url: baseURL + baseURL + "/RALookupSearch",
         async: true,
         success: function (data)
         {
            var $target = $("#RALookupDetail");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            $("#hiddenDm").html("This is the Value We Have - " + $("#hiddenDebitMemo").val());
            $("#CustomMessageBoxEntity").css("overflow", "none");
            window.dtHeaders("#searchGridLookup", "max-height:400px,ordering : false", ["sorting"], []);
            $(".dataTables_scrollBody").css("max-height", "200px").addClass("QnexScrollbar");
            $("#searchGridLookup_filter").css("display", "none");
            $("#wait").hide();
         },
         error: function (data)
         {
            $("#wait").hide();
            $("#timeoutMsg").html("Execution Timeout Expired");
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   once the debit memo has been validated, activate the remaining portions of the Totes segment
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function debitMemoValidated(userInput)
   {
      setGrapicState("#DM_ENTERED", userInput);
      setGrapicState("#btnAddNewTote", userInput);

      if (!userInput)
      {
         return false;
      }

      $(".totesPanel input, .attachmentsPanel input, #Totefinish, #Comment").not("#AttachmentId").removeAttr("disabled");
      $("#ToteId").focus();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on debit memo search lookup.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".searchGridLookup-div-click", function (e)
   {
      $(".searchGridLookup-div-click").removeClass("selected");
      $(this).addClass("selected");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   lookup replace button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#LookupReplace", function ()
   {
      if (!$(".searchGridLookup-div-click").hasClass("selected"))
      {
         return false;
      }

      $("#wait").show();

      raId = $(".searchGridLookup-div-click.selected td[exchange='RAID']").attr("exchange-value");
      var ctrlCode = $(".searchGridLookup-div-click.selected td[exchange='CTRL']").attr("exchange-value");
      var specialHandling = $(".searchGridLookup-div-click.selected td[exchange='SpecialHandling']").attr("exchange-value") === "true";
      $("#RAorWPT").val($(".searchGridLookup-div-click.selected td[exchange='RA']").attr("exchange-value"));
      $("#DebitMemoNumber").val($(".searchGridLookup-div-click.selected td[exchange='DebitMemo']").attr("exchange-value"));
      $("#hiddenDebitMemo").val($("#DebitMemoNumber").val());
      $("#debitMemoSearchField").val($("#DebitMemoNumber").val());
      debitMemoValidate = true;
      debitMemoEntry = $("#DebitMemoNumber").val();
      $("#debitMemoAccept").click();

      $("#WrhsStagingAttachment div").each(function ()
      {
         if ($(this).attr("data-row-name") !== "")
         {
            attachedFiles.push(
               {
                  "Name": $(this).attr("data-row-name"),
                  "TrackingID": $(this).find("img").attr("data-row-name"),
                  "IsShared": $(this).find("img").attr("data-row-shared")
               });
         }
      });

      itemLists = [];

      var relationObjects = {};
      $("#searchGrid_Tote tr.searchGrid_Tote-div-click").each(function ()
      {
         var toteName = $(this).find("td[exchange='Name']").attr("exchange-value");

         if (toteName !== undefined && toteName !== null && toteName.length > 0)
         {
            relationObjects = {
               "Code": toteName,
               "Name": toteName,
               "TrackingID": trackingId
            };

            itemLists.push(relationObjects);
         }
      });

      itemLists = itemLists.length > 0 ? itemLists : [];
      var packages = [];

      $(".ContainerTypeRadio1[form='WeightCondition']").closest("div.selected").each(function ()
      {
         packages.push($(this).find("input.ContainerTypeRadio1").val());
      });

      stagingDetailObjects.push({
         "PackageCode": $(".ContainerTypeRadio1[form='Container']").closest("div").hasClass("selected") ? $(".ContainerTypeRadio1[form='Container']").closest("div.selected").find("input").val() : "",
         "DebitMemo": $("#DebitMemoNumber").val(),
         "Weight": parseFloat($("#ID_PRODUCTWT").val()) > 0 ? $("#ID_PRODUCTWT").val() : 0,
         "TrackingID": trackingId,
         "TrackingNumber": trNumber,
         "RAorWPT": raId,
         "Comment": $("#Comment").val(),
         "ControlType": ctrlCode,
         "SpecialHandling": specialHandling
      });

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + baseURL + "/ReplaceRA",
         async: true,
         data: "detailEntity=" + JSON.stringify(stagingDetailObjects) +
         "&toteEntity=" + JSON.stringify(itemLists) +
         "&attachedEntity=" + JSON.stringify(attachedFiles)
         + "&machineId=" + stationId
         + "&reserved=" + (reservedData.length > 0 ? JSON.stringify(reservedData[0]) : "")
         + "&pkgCodes=" + JSON.stringify(packages)
         + "&location=" + machineLocation
         + getAntiForgeryToken(),
         success: function (data)
         {
            if (data.Success)
            {
               itemLists = [];
               attachedFiles = [];
               stagingDetailObjects = [];
               editModeStage = true;
               form222PrintDate = data.Form222Info.Form222PrintedDate;
               controlMsgBuilder(data.RAorWPTCtrlCode);
               var $newHtml = $(data.staging);
               $newHtml = populateForm222Segment($newHtml);
               $newHtml = setContainerAndContentImages($newHtml);
               $newHtml = disableStagingForm($newHtml);
               replaceSegments($("#WrhsStagingDetailsForm"), $newHtml);
               reSelectContainerConditionSegment();
               setCollapseExpand();

               setTimeout(function ()
               {
                  var $slickSelected = $(".slick-track div.radio-inline.selected");
                  if ($slickSelected.length)
                  {
                     var index = $slickSelected.attr("data-slick-index");
                     $("#ContainerTypeDict .scroll-container").slick("slickGoTo", index);
                  }
                  $("#divToteAttachedSegment .dataTables_scrollBody").css("max-height", "162px").addClass("QnexScrollbar");
                  lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
                  lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
               }, 200);
            }

            enableActionbarControl("#CMD_SUBMIT", false);
            $(this).closeMessageBox();
            $("#wait").hide();

            //because we're filling in the debit memo automatically, we enable the add debit memo button
            setTimeout(function ()
            {
               $("#DebitMemoNumber").focus().keyup();
               debitMemoValidated(true);
            }, 400);
         },
         error: function (data)
         {
            enableActionbarControl("#CMD_SUBMIT", false);
            $(this).closeMessageBox();
            $("#wait").hide();
            btnObjects = [
               {
                  "id": "btn_ok",
                  "name": "Continue",
                  "function": "onclick='$(this).closeMessageBox()'",
                  "class": "btnErrorMessage"
               }
            ];
            messageObject = [{
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> The controller call failed on Replace RA request. Please restart the Staging application.</h3>"
            }];
            titleobjects = [{
               "title": "Invalid Entry"
            }];
            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   lookup cancel.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#LookupCancel, .close-button-messageBox", function ()
   {
      $(this).closeMessageBox();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Debit Memo Search keypress event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#debitMemoSearchField", function (e)
   {
      if (e.keyCode === KEY_ENTER || e.keyCode === KEY_CtrlM)
      {
         searchDebitMemo();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   focus event for the debit memo search field in debit memo look-up
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusin", "#debitMemoSearchField", function (e)
   {
      $("#hiddenDm").html();
      $(this).val("");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   search debit memo in RA lookup.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#search_Lookup", function (e)
   {
      searchDebitMemo();
   });

   //======================================================End Debit Memo Lookup=

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on finish button and save staging details changes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function finishStaging()
   {
      $("#wait").show();
      if ($(".inputEx").hasClass("inputError"))
      {
         $(".inputEx.inputError:first").focus();
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      var barcodeChar = String.fromCharCode(KEY_SPECIALCHAR); //double check for barcode character; error out if found

      if ($("#TrackingNumber").val().length === 0 || $("#TrackingNumber").val().indexOf(barcodeChar) > -1)
      {
         $(this).setErrorMessage("Tracking Number is required.", "Error Message", "Continue");
         $("#TrackingNumber").select();
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      if (trackingId === 0)
      {
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      if ($("#handlingChk").length > 0)
      {
         //both the special handling and the totes/attachment segments should be completed
         if ($("#handlingContinue").attr("acknowledge") === undefined)
         {
            $(this).setErrorMessage("Acknowledgment of Special Handling segment is required.", "Error Message", "Continue");
            finishClick = false;
            return false;
         }

         if (!$("#handlingChk").is(":checked"))
         {
            $(this).setErrorMessage(
               "This tracking record requires acknowledgment of special handling in order to finish staging.",
               "Error Message",
               "Continue");
            finishClick = false;
            $("#wait").hide();
            return false;
         }
      }

      if (!$(".ContainerTypeRadio1[form='Container']").closest("div").hasClass("selected") || !$(".ContainerTypeRadio1[form='WeightCondition']").closest("div").hasClass("selected"))
      {
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      if ($("#ID_PRODUCTWT").val().length === 0 || parseFloat($("#ID_PRODUCTWT").val()) === 0 || $("#ID_PRODUCTWT").val().indexOf(barcodeChar) > -1)
      {
         $(this).setErrorMessage("The Weight and Condition segment has not been completed.", "Error Message", "Continue");
         $("#CMD_SCALETARE1").click();
         $("#ID_PRODUCTWT").select();
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      //remove reserved character added by barcode scanner
      if ($("#Comment").val().length > 0)
      {
         $("#Comment").val(window.specialChar($("#Comment").val(), "SpecialChar"));
      }

      stagingDetailObjects.push({
         "PackageCode": $("#ContainerTypeDict .ContainerTypeRadio1[form='Container']").closest("div.selected").find("input").val(),
         "DebitMemo": $("#DebitMemoNumber").val() === "" ? $("#hiddenDebitMemo").val() : $("#DebitMemoNumber").val(),
         "Weight": $("#ID_PRODUCTWT").val(),
         "TrackingID": trackingId,
         "TrackingNumber": $("#TrackingNumber").val(),
         "RAorWPT": $("#RAorWPT").val(),
         "Comment": $("#Comment").val(),
         "AssocID": $("#validAssocId").val()
      });

      itemLists = [];
      var relationObjects = {};
      $("#searchGrid_Tote tr.searchGrid_Tote-div-click").each(function ()
      {
         var toteName = $(this).find("td[exchange='Name']").attr("exchange-value");

         if (toteName !== undefined && toteName !== null && toteName.length > 0)
         {
            relationObjects = {
               "Code": toteName,
               "Name": toteName,
               "TrackingID": trackingId
            };

            itemLists.push(relationObjects);
         }
      });

      if (itemLists.length === 0)
      {
         $(this).setErrorMessage("No totes have been associated with this record.", "Error Message", "Continue");
         document.getElementById("Totefinish").style.pointerEvents = "";
         finishClick = false;
         $("#wait").hide();
         stagingDetailObjects = [];
         return false;
      }

      var pdfAttch = 0;
      var imgAttch = 0;
      //not sure why, but #WrhsStagingPendingAttachments div each would not work, so added a class to each pending upload
      $(".pendingFileUpload").each(function ()
      {
         if ($(this).html().indexOf(".pdf") > 0)
         {
            pdfAttch++;
         }
         else if ($(this).html().toLowerCase().indexOf(".jpg") > 0
            || $(this).html().toLowerCase().indexOf(".jpeg") > 0
            || $(this).html().toLowerCase().indexOf(".png") > 0
            || $(this).html().toLowerCase().indexOf(".svg") > 0
            || $(this).html().toLowerCase().indexOf(".gif") > 0
            || $(this).html().toLowerCase().indexOf(".bmp") > 0)
         {
            imgAttch++;
         }
      });

      $("#WrhsStagingAttachment div").each(function ()
      {
         if ($(this).attr("data-row-name") !== "")
         {
            attachedFiles.push(
               {
                  "Name": $(this).attr("data-row-name"),
                  "TrackingID": $(this).find("img").attr("data-row-name"),
                  "IsShared": $(this).find("img").attr("data-row-shared")
               });
            if ($(this).attr("data-row-name").indexOf(".pdf") > 0)
            {
               pdfAttch++;
            }
            else if ($(this).attr("data-row-name").toLowerCase().indexOf(".jpg") > 0
               || $(this).attr("data-row-name").toLowerCase().indexOf(".jpeg") > 0
               || $(this).attr("data-row-name").toLowerCase().indexOf(".png") > 0
               || $(this).attr("data-row-name").toLowerCase().indexOf(".svg") > 0
               || $(this).attr("data-row-name").toLowerCase().indexOf(".gif") > 0
               || $(this).attr("data-row-name").toLowerCase().indexOf(".bmp") > 0)
            {
               imgAttch++;
            }
         }
      });


      if ($(".selected[data-unique-selection='WeightCondition'] input.ContainerTypeRadio1[form='WeightCondition']").val() !== "NONE" && imgAttch === 0)
      {
         $("#wait").hide();

         btnObjects = [
            {
               "id": "btn_ok",
               "name": "Continue",
               "function": "onclick='$(this).closeMessageBox()'",
               "class": "btnErrorMessage"
            }
            //, {
            //   "id": "btn_attach_no",
            //   "name": "No",
            //   "function": "",
            //   "class": "btnErrorMessage"
            //}
         ];
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Damaged packages and product require photo documentation " +
            (pdfAttch === 0 ? " </br> and this staging is missing debit memo paperwork. </br> Please return to the Totes and Attachments segment and attach evidence of the damaged package and/or product."
               : ".</br> Please return to the Totes and Attachments segment and attach evidence of the damaged package and/or product.") + "</h3>"

            //+ (pdfAttch === 0
            //   ?" No documents have been attached to this staging record.</br> No damaged condition requires attach image files"
            //   :"No damaged condition requires attach image files")+"</h3>"
         }];
         titleobjects = [{
            "title": "Warning"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         finishClick = false;
         return false;
      }
      else if ((($("#WrhsStagingPendingAttachments div").length === 0
         && $("#WrhsStagingAttachment div").length === 0)
         || pdfAttch === 0))
      {
         $("#wait").hide();

         btnObjects = [
            {
               "id": "btn_attach_yes",
               "name": "Yes",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "",
               "class": "btnErrorMessage"
            }, {
               "id": "btn_attach_no",
               "name": "No",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "",
               "class": "btnErrorMessage"
            }
         ];
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> This staging is missing debit memo paperwork. Would you like to continue? </h3>" +
            "<script> $('.close-button-messageBox').hide()</script>"
         }];
         titleobjects = [{
            "title": "Confirmation"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
      }
      else
      {
         // we have attached files and saving it.
         saveAfterConfirmation();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   return back to attached files.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_attach_no", function ()
   {
      $("#wait").hide();

      stagingDetailObjects = [];
      finishClick = false;
      $(this).closeMessageBox();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save Without attached files.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_attach_yes", function ()
   {
      $(this).closeMessageBox();
      saveAfterConfirmation();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Dialogue box for managerial login; utilized when tracking number is changed
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function secondaryVerification()
   {
      //RAs received at Staging should only have the option of a tracking number
      //change for a single RA
      if (trNumber === "No-Tracking-Number")
      {
         btnObjects = [
            {
               "id": "btnValidationSubmitSingleStage",
               "name": "Change",
               "function": "",
               "class": "btnErrorMessage"
            }
         ];
      }
      else
      {
         btnObjects = [
            {
               "id": "btnValidationSubmitSingleStage",
               "name": "Change One",
               "function": "",
               "class": "btnErrorMessage"
            },
            {
               "id": "btnValidationSubmitStage",
               "name": "Change All",
               "function": "",
               "class": "btnErrorMessage"
            }
         ];
      }

      var verificationMsg = "Secondary Verification is required to change the Tracking Number";

      $(this).setVerificationMessageBox(verificationMsg, "Secondary Verification", btnObjects, false);
   }
   window.secondaryVerification = secondaryVerification;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Dialogue box for managerial login; utilized when tracking number is changed
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function cancelTrkNumChange()
   {
      finishClick = false;
      $("#TrackingNumber").val(trNumber);
      $(this).closeMessageBox();
   }
   window.cancelTrkNumChange = cancelTrkNumChange;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check if the tracking number was changed. If so, prompt user to confirm; otherwise, save staging record
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function saveAfterConfirmation()
   {
      if (trNumber !== $("#TrackingNumber").val())
      {
         $("#wait").hide();

         var myMsg = "Tracking Number has been modified. The new tracking number will be updated for either this single RA, or all RA's associated with it for this Carrier on this Date. " +
            "Would you like to continue?<br><br>Previous Tracking Number: <span style='font-weight:bold'>" + trNumber +
            "</span><br>Updated Tracking Number: <span style='font-weight:bold'>" + $("#TrackingNumber").val() + "</span>";

         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " + myMsg + " </h3>"
         }];

         btnObjects = [
            {
               "id": "btnYesTrkChnge",
               "name": "Yes",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "onclick='window.secondaryVerification();'",
               "class": "btnErrorMessage"
            }, {
               "id": "btnNoTrkChnge",
               "name": "No",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "onclick='window.cancelTrkNumChange();'",
               "class": "btnErrorMessage"
            }
         ];

         titleobjects = [{
            "title": "Confirm Tracking Number Change"
         }];

         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         return false;
      }
      else
      {
         saveValidateItem(0);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Validate Verification.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnValidationSubmitStage", function ()
   {
      getCountUnderTracking(function (data)
      {
         var notes = data.Count;
         console.log(notes);

         if ($(this).checkForValidation("Change Tracking Number from '" + trNumber + "' to '" + $("#TrackingNumber").val() + "' in " + notes + " rows?").Success)
         {
            saveValidateItem(2);
            $(this).closeMessageBox();
         }

         finishClick = false;
      });
      //$("#searchGrid_Summary  tr.searchGrid_Summary-div-click").each(function()
      //{
      //   if (notes.indexOf($(this).attr("data-row-id")) < 0)
      //   {
      //      if ($(this).attr("data-row-id") ===
      //         $("#searchGrid_Summary  tr.searchGrid_Summary-div-click:last")
      //         .closest("tr").attr("data-row-id"))
      //      {
      //         notes += $(this).attr("data-row-id");
      //      }
      //      else
      //      {
      //         notes += $(this).attr("data-row-id") + ",";
      //      }
      //   }
      //});

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Count .
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getCountUnderTracking(callBack)
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + baseURL + "/GetCountUnderTracking",
         data: "trackingNumber=" + trNumber
         + "&carrier=" + $("#Carrier").attr("data-field-value")
         + "&date=" + $("#Date").val()
         + getAntiForgeryToken(),
         async: true,
         success: function (data)
         {
            callBack(data);
         }
      });

      try
      {
         $.ajax(request);
      }
      catch (e)
      {
         throw e;
      }

      ga("send", "event", "Staging", "GetCountUnderTracking");
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Validate Verification.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnValidationSubmitSingleStage", function ()
   {
      if ($(this).checkForValidation("Change Tracking Number from '" + trNumber + "' to '" + $("#TrackingNumber").val() + "' in " + trackingId + " row").Success)
      {
         saveValidateItem(1);
         $(this).closeMessageBox();
         finishClick = false;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save Validate Items.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function saveValidateItem(intStep)
   {
      $("#wait").show();

      if (trackingId === "0" || trackingId === 0)
      {
         finishClick = false;
         $("#wait").hide();
         return false;
      }

      var raFolderName = stagingDetailObjects[0].RAorWPT;
      var dbtMemo = stagingDetailObjects[0].DebitMemo;
      stagingDetailObjects[0].DebitMemo = "";
      var packages = [];

      $("#ContainerCondition div[data-unique-selection='WeightCondition'].selected").each(function ()
      {
         packages.push($(this).find("input.ContainerTypeRadio1").val());
      });

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + baseURL + "/SaveStagingDetail",
         data: "detailEntity=" + JSON.stringify(stagingDetailObjects) +
         "&toteEntity=" + JSON.stringify(itemLists) +
         "&attachedEntity=" + JSON.stringify(attachedFiles)
         + "&intStep=" + intStep
         + "&machineId=" + stationId
         + "&debitMemo=" + encodeURIComponent(dbtMemo)
         + "&pkgCodes=" + JSON.stringify(packages) + getAntiForgeryToken(),
         success: function (data)
         {
            if (data.Success)
            {
               lastStagingDetailForm = "";
               window.setActionTabCount("WrhsMulti", data.Count);
               existSummaryItemsCount = data.Count;

               if ($("#stgedDate").val() === "" || ((reservedData.length > 0 && reservedData[0].Weight === "") || reservedData.length === 0))
               {
                  setLocalSavedInformation(stagingDetailObjects[0].Weight,
                     stagingDetailObjects[0].PackageCode,
                     packages,
                     $("#Carrier").attr("data-field-value"),
                     $("#Date").val());
               }

               stagingDetailObjects = [];
               itemLists = [];
               attachedFiles = [];
               rcvdDate = "";
               checkItem = 0;
               raId = "";
               $("#wait").hide();

               if (data.folder.length > 0)
               {
                  for (var indx = 0; indx < data.folder.length; indx++)
                  {
                     console.log("Moving attachment index " + indx + " to archive");
                     $.getJSON("https://localhost:8009/Qualanex/http/Archive?clean=true&folder=" + raFolderName + "&file=" + localStagingDir + "\\" + data.folder[indx] + "", {}, function (archiveData)
                     {
                        console.log("Archived: " + archiveData);
                     });
                  }
               }

               debitMemoEntry = "";
               editModeStage = false;

               if (data.Count > 0)
               {
                  window.lockCtrl("TrackingNumber", parseInt(machineLocation) === 0 ? true : false);
                  reservedTrackingId = trackingId;
                  trNumber = $("#TrackingNumber").val();
                  debitMemoValidate = false;
                  refreshStagingPage(1);

                  if (window.lockCtrl("TrackingNumber"))
                  {
                     carrierCode = reservedData[0].CarrierID;
                     rcvDates = reservedData[0].Date;
                  }
                  else
                  {
                     carrierCode = "";
                     rcvDates = "";
                  }
                  //GetAllRaMatchesWithTrackingNumber(trackingId);
               }
               else
               {
                  window.lockCtrl("TrackingNumber", false);
                  carrierCode = "";
                  rcvDates = "";
                  trackingId = 0;
                  reservedTrackingId = "";
                  //setLocalSavedInformation("", "", "", "", "");
                  debitMemoValidate = false;
                  document.getElementById("Totefinish").style.pointerEvents = "";
                  refreshStagingPage(0);
               }

               if (parseInt(machineLocation) === 0 && window.lockCtrl("TrackingNumber"))
               {
                  $("#RAorWPT").val("");
               }
               else
               {
                  $("#TrackingNumber,#RAorWPT").val("");
                  $("#TrackingNumber").focus();
               }
            }
            else
            {
               $("#ControlMsg").html("Error in save function");
               $("#wait").hide();
            }

            finishClick = false;
         },
         error: function (data)
         {
            finishClick = false;
            stagingDetailObjects = [];
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
            return false;
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Local Weight Container and Condition Data.
   //*
   //* Parameters:
   //*   wgt        - Weight
   //*   cntr       - Container
   //*   cnd        - Condition
   //*   carrierId  - Carrier Code
   //*   rcvDate    - Receiving Date
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setLocalSavedInformation(wgt, cntr, cnd, carrierId, rcvDate)
   {
      reservedData = [];
      reservedData.push({
         "Weight": wgt,
         "PackageCode": cntr,
         "PackageCndCode": cnd,
         "TrackingNumber": trNumber,
         "CarrierID": carrierId,
         "Date": rcvDate
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clear textbox and form information when tracking Id is not valid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function changeTrackingRaClearStaging()
   {
      $("#WrhsStagingDetailsForm").replaceWith($staginEmpty);
      toteList = [];
      $("#Carrier,#Date").val("");

      if (!tray)
      {
         $("#ControlMsg").html("");
      }

      $("#imgCtrlNbr").attr("src", "");
      trackingId = 0;
      receivingId = 0;
      stagingDetailObjects = [];
      validateWeight = false;
      finishClick = false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete Draged images.
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".dz-deletefile img", function (e)
   {
      var imageName = $(e.target).parent().parent().parent().find("div.dz-details div.dz-filename span").html();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "fileName=" + imageName + getAntiForgeryToken(),
         url: baseURL + baseURL + "/DeleteSelectedImage",
         success: function (data)
         {
            if (data.Success)
            {
               var div = $(e.target).parent().parent().parent().closest("div");
               div.remove();

               //if user removes all newly-uploaded attachments, then remove the class that hides the Dropzone instructions
               if ($("#dropzoneForm div.dz-preview").length === 0)
               {
                  $("#dropzoneForm").removeClass("dz-started");
               }
            }
         }
      });

   });
   var $enamad;
   //*****************************************************************************
   //*
   //* Summary:
   //*   Deleted attached files.
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function DeleteStagingItems()
   {
      $enamad.closest("div").remove();
      $(this).closeMessageBox();
   }
   window.DeleteStagingItems = DeleteStagingItems;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Deleted attached files.
   //*
   //* Parameters:
   //*   e.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".deleteAttachedFiles", function (e)
   {
      if (isHistory || $(this).attr("disabled") !== undefined)
      {
         $(this).setErrorMessage("Attachment(s) Cannot Be Deleted After A Tote Has Been Opened For Processing.", "Tote In-Progress", "Continue");
         return false;
      }

      if ($(e.target).attr("data-row-shared") === "1")
      {
         $enamad = $(e.target);
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>This file is shared between many staging records. Are you sure you want to delete it?</h3>"
         }];

         btnObjects = [{
            "id": "btn_deleteStagingImage",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='window.DeleteStagingItems();'"
         }, {
            "id": "btn_no",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='$(this).closeMessageBox();'"
         }
         ];

         titleobjects = [{
            "title": "Confirm Delete"
         }];

         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         return false;
      }

      $enamad = null;
      $(e.target).closest("div").remove();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#tab-container a", function ()
   {
      var stagingForm = $("#WrhsStagingDetailsForm #validRA").val(); //determine if the staging details form has been loaded properly

      if ($("a.active").attr("href") === "#WrhsHistory")
      {
         enableActionbarControl("#CMD_REFRESH", true);
         enableActionbarControl("#CMD_FILTER", true);
         enableActionbarControl("#CMD_SUBMIT", $("#searchGrid tbody").find(".selected").length > 0);

         $lastHistoryFilter = $(".ql-body.active #WrhsStagingHistory .formExDivFilter").html();
         $(".filterFrame .formExDiv").html($(".ql-body.active div.formExDivFilter").html());

         if (!$(".ql-body.active div.formExDivFilter input").hasClass("inputError") && !historyTabInit)
         {
            $(".SubmitCriteria").click();
            historyTabInit = true;
            $("#searchGrid tbody").find("tr:eq(0)").click();
         }

         //save the form as is, in case we want to go back to the history tab
         //should reload form upon click to history
         if (lastHistoryForm !== "")
         {
            $(".QoskContent div.QoskForm .formMax").html("");
            $(lastHistoryForm).appendTo(".QoskContent div.QoskForm .formMax");
            lastHistoryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
         }

      }
      else if ($("a.active").attr("href") === "#WrhsMulti")
      {
         enableActionbarControl("#CMD_REFRESH", !tray);
         enableActionbarControl("#CMD_FILTER", false);
         enableActionbarControl("#CMD_SUBMIT", !$("#WrhsStagingGridSummary tbody tr td").hasClass("dataTables_empty") && stagingForm === undefined);

         if (!tray)
         {
            refreshGrid("Summary");
         }

         //clear the form if we're switching tabs and there is no content loaded in the target tab
         if (stagingForm === undefined)
         {
            $(".formMax .inputRex").val("");
            $("#imgCtrlNbr").attr("src", "");
         }

         //save the form as is, in case we want to go back to the history tab
         //should reload form upon click to history
         if (lastSummaryForm !== "")
         {
            $(".QoskContent div.QoskForm .formMax").html("");
            $(lastSummaryForm).appendTo(".QoskContent div.QoskForm .formMax");
            lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
         }
      }
      else
      {
         enableActionbarControl("#CMD_REFRESH", false);
         enableActionbarControl("#CMD_FILTER", false);
         enableActionbarControl("#CMD_SUBMIT", $("#WrhsStagingPartialMatch").length > 0 && stagingForm === undefined);

         $("#dataTables_scrollHeadInner").css("width", "100%");
         $("#dataTables_scrollHeadInner table").css("width", "100%");

         //clear the form if we're switching tabs and there is no content loaded in the target tab
         if (stagingForm === undefined)
         {
            $(".formMax .inputRex").val("");
            $("#imgCtrlNbr").attr("src", "");
         }

         if (lastStagingDetailForm !== "")
         {
            $(".QoskContent div.QoskForm .formMax").html("");
            $(lastStagingDetailForm).appendTo(".QoskContent div.QoskForm .formMax");
            lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on reload button for reload temporary deleted attached files.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#ToteReload", function ()
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingId=" + trackingId + getAntiForgeryToken(),
         url: baseURL + baseURL + "/GetAttacheFiles",
         success: function (data)
         {
            var $target = $("#WrhsStagingAttachment");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
         }
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on history grid record
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".WrhsStagingDetail-div-click", function ()
   {
      searchChange = false;
      window.lockCtrl("TrackingNumber", false);
      enableActionbarControl("#CMD_SUBMIT", !tray);
      lastHistoryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
      historyDt.$("tr.selected").removeClass("selected");
      $(this).addClass("selected");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   double click on history grid record
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("dblclick", ".WrhsStagingDetail-div-click", function ()
   {
      $("#HistoryContinue").click();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   history continue button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#HistoryContinue", function ()
   {
      //in order to load a history record in Staging, we must ensure there is a row selected, and that Infinite Scroll is not currently active
      if (!$(".WrhsStagingDetail-div-click").hasClass("selected") || window.isInfiniteScrollActive())
      {
         return false;
      }

      isHistory = true;

      if (window.lockCtrl("TrackingNumber") && (trNumber !== ""
         && $.trim(trNumber).toLowerCase() !== $.trim($(".WrhsStagingDetail-div-click.selected").find("td[exchange='TrackingNumber']").closest("td").attr("exchange-value")).toLowerCase()))
      {
         window.lockCtrl("TrackingNumber", false);
      }

      searchChange = false;

      $("#RAorWPT").val($(".WrhsStagingDetail-div-click.selected").find("td[exchange='RAOrWPT']").closest("td").attr("exchange-value"));
      $("#TrackingNumber").val($(".WrhsStagingDetail-div-click.selected").find("td[exchange='TrackingNumber']").closest("td").attr("exchange-value"));
      $("#Date").val($(".WrhsStagingDetail-div-click.selected").find("td[exchange='RcvdDate']").closest("td").attr("exchange-value"));
      $("#Carrier").val($(".WrhsStagingDetail-div-click.selected").find("td[exchange='Carrier']").closest("td").attr("exchange-value"));
      rcvdDate = $(".WrhsStagingDetail-div-click.selected").find("td[exchange='RcvdDate']").attr("exchange-value");
      controlMsgBuilder($(".WrhsStagingDetail-div-click.selected").find("td[exchange='ControlMsg']").closest("td").attr("exchange-value"));
      lastStagingDetailForm = "";
      $("#Carrier").attr("data-field-value", $(".WrhsStagingDetail-div-click.selected").find("td[exchange='CarrierID']").closest("td").attr("exchange-value"));
      trackingId = $(".WrhsStagingDetail-div-click.selected").find("td[exchange='HistoryTrackingID']").closest("td").attr("exchange-value");

      setTimeout(function ()
      {
         getStagingDetail(trackingId, true, false);
      }, 100);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on partial grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".WrhsStagingPartialMatch-div-click", function ()
   {
      enableActionbarControl("#CMD_SUBMIT", !tray);
      var entity = $(this).closest("tr");
      partialDt.$("tr.selected").removeClass("selected");
      entity.addClass("selected");
      var entityList = JSON.parse(entity.attr("MgmtEntity-data").split("=")[1]);
      $.each(entityList, function (index, value)
      {
         var entityData = value;

         window.UpdateData(false, index, value);
      });

      lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   double click on partial grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("dblclick", ".WrhsStagingPartialMatch-div-click", function ()
   {
      $("#PartialMatchContinue").click();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on multistatus grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".searchGrid_Summary-div-click", function ()
   {
      enableActionbarControl("#CMD_SUBMIT", !tray);
      var entity = $(this).closest("tr");
      multiDt.$("tr.selected").removeClass("selected");
      entity.addClass("selected");
      var entityList = JSON.parse(entity.attr("searchGrid_Summary-data").split("=")[1]);
      $.each(entityList, function (index, value)
      {
         var entityData = value;

         window.UpdateData(false, index, value);
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   double click on multistatus grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("dblclick", ".searchGrid_Summary-div-click", function ()
   {
      $("#MultiStatusContinue").click();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Press continue button on multistatus grid
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#MultiStatusContinue", function ()
   {
      //in order to load a multi status record in Staging, we must ensure there is a row selected, and that Infinite Scroll is not currently active
      if (!$(".searchGrid_Summary-div-click.selected").hasClass("selected") || window.isInfiniteScrollActive())
      {
         return false;
      }

      var $multi = $(".searchGrid_Summary-div-click.selected");

      if ($("#searchGrid_Summary tr.selected").closest("tr").attr("data-row-id") !== $multi.attr("data-row-id"))
      {
         $("#searchGrid_Summary tr.selected").closest("tr").removeClass("selected");
         $(this).addClass("selected");
      }

      searchChange = false;
      $("#RAorWPT").val($multi.find("td[exchange='RAorWPT']").attr("exchange-value"));
      $("#TrackingNumber").val($multi.find("td[exchange='TrckNo']").attr("exchange-value"));
      trNumber = $("#TrackingNumber").val();
      $("#Carrier").val($multi.find("td[exchange='Carrier']").attr("exchange-value"));
      $("#Carrier").attr("data-field-value", $multi.find("td[exchange='CarrierID']").attr("exchange-value"));
      $("#Date").val($multi.find("td[exchange='Date']").attr("exchange-value"));
      controlMsgBuilder($multi.find("td[exchange='ControlMsg']").closest("td").attr("exchange-value"));
      lastStagingDetailForm = "";

      setTimeout(function ()
      {
         getStagingDetail($multi.attr("data-row-id"),
            $multi.find("td[exchange='StagedBy']").attr("exchange-value") === undefined || $multi.find("td[exchange='StagedBy']").attr("exchange-value") === ""
               ? false
               : true,
            true
         );
      }, 100);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Edit function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      if (!getEditMode() && trackingId !== 0 && isHistory && $(".ContainerTypeRadio1").length > 0)
      {
         if ($("#WrhsStagingPendingAttachments div").length === 0)
         {
            $(".dropzone").dropzone({
               url: baseURL + baseURL + "/SaveUploadedFile"
            });
         }

         $("#RAorWPT").attr("disabled", "disabled");
         $("#TrackingNumber").removeAttr("disabled readonly");

         editModeStage = true;
         setEditMode(true);
         isHistory = false;
         stagingDetailObjects = [];

         if ($("#WrhsStagingDetailsForm div").hasClass("selected"))
         {
            $("#weightConditionTable input[type=text],.ContainerTypeRadio1").removeAttr("disabled");
            $("#ToteTable input[type=text],#ToteTable input[type=button],#weightConditionTable input[type=button]").removeAttr("disabled");
            validateWeight = true;
            debitMemoValidate = true;
         }
         //window.checkScaleConditionStaging();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cancel function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_CLEAR", function ()
   {
      checkItem = 0;
      $("input[type=search]").val("").keyup();
      $("#searchGrid").DataTable().search(
         "",
         false,
         true
      ).draw();
      $("#searchGrid_Summary").DataTable().search(
         "",
         false,
         true
      ).draw();

      dates = "";
      rcvdDate = "";
      preLoadedWt = "";
      $(".formMax .inputRex").val("");
      partialRA = "";
      partialTrackingNumber = "";
      debitMemoEntry = "";
      editModeStage = false;
      newRaMode = false;
      $("#TrackingNumber,#RAorWPT").removeAttr("readonly disabled");
      $("#WrhsStagingDetailsForm").replaceWith($staginEmpty);
      $("#WrhsStagingHistory").find("tr.selected").removeClass("selected");
      changeTrackingRaClearStaging();
      setEmptySummary();
      existSummaryItemsCount = 0;
      $("#TrackingNumber").focus();
      window.lockCtrl("TrackingNumber", false);

      finishClick = false;

      if (!tray)
      {
         $("#ControlMsg").html("");
      }

      lastStagingDetailForm = "";
      lastHistoryForm = "";
      lastSummaryForm = "";
      enableActionbarControl("#CMD_SUBMIT", false);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clear Data from Summary grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setEmptySummary()
   {
      trackingId = 0;
      reservedTrackingId = "";
      $("#searchGrid_Summary tbody").html($summaryEmpty);
      $("#searchGrid_Summary_info").html("Showing 0 to 0 of 0 entries");

      window.resizeTabs();
      window.setActionTabCount("WrhsMulti", 0);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   refresh function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      if ($("a.active").attr("href") === "#WrhsMulti")
      {
         $("#searchGrid_Summary_filter input[type='search']").val("");

         if (!tray)
         {
            refreshGrid("Summary");
         }
      }
      else if ($("a.active").attr("href") === "#WrhsHistory")
      {
         $("#searchGrid_filter input[type='search']").val("");
         //$(".ql-body.active div.formExDivFilter form input[name='criteria.PreviousCount']").attr("value", "0");
         $lastHistoryFilter = $("#WrhsStagingHistory .formExDivFilter").html();
         $(".filterFrame .formExDiv").html($(".ql-body.active div.formExDivFilter").html());
         $(".SubmitCriteria").click();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   enable/disable the progress arrows on weight/quantity segment
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setGrapicState(selector, state)
   {
      // assumes all graphics to be swapped contained the string '(ON)' or '(OFF)' in their filename
      // this is different in Totes and Attachments where the disabled button is considered "inverted"
      var obj = $(selector);
      var src = obj.prop("src");
      var stringOff = selector === "#btnAddNewTote" ? "(Inverted)" : "(OFF)";
      var stringOn = "(ON)";

      if (src === undefined)
      {
         return false;
      }

      if (state === true)
      {
         obj.attr("src", src.replace(stringOff, stringOn));
      }
      else
      {
         obj.attr("src", src.replace(stringOn, stringOff));
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   refresh function.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshGrid(gridName)
   {
      $("#wait").show();
      var mdlElements = [];
      var elements = {};

      $(".ql-body.active .formExDivFilter input").each(function ()
      {
         elements[$(this).attr("data-field-name")] = $(this).val();
      });

      mdlElements.push(elements);

      if (gridName === "Summary")
      {
         var trkId = ((trackingId === "" || parseInt(trackingId) === 0) ? reservedTrackingId : trackingId);
         $(".ql-body.active:first").find(".formExDivFilter form").find("input[name='criteria.TrackingID']").val(trkId);
         $(".ql-body.active:first").find(".formExDivFilter form").find("input[name='criteria.TrackingID']").attr("data-field-value", trkId);
      }

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: gridName === "Summary"
            ? $(".ql-body.active:first").find(".formExDivFilter form").serialize() + getAntiForgeryToken()
            : $(".ql-body.active .formExDivFilter").serialize() + getAntiForgeryToken(),
         url: baseURL + baseURL + "/WrhsStaging" + gridName,
         async: true,
         success: function (data)
         {
            var $target = gridName === "History" ? $("#WrhsStagingHistory") : $("#WrhsStagingGridSummary");
            var $newHtml = $(data);

            if (gridName === "Summary")
            {
               $newHtml.find("input[name='criteria.TrackingID']").val(trackingId);
               $newHtml.find("input[name='criteria.TrackingID']").attr("data-default-value", trackingId);
            }

            $target.replaceWith($newHtml);
            window.dtHeaders(gridName === "History" ? "#searchGrid" : "#searchGrid_Summary", "", ["sorting"], []);

            var $grid = gridName === "History" ? $("#WrhsStagingHistory") : $("#WrhsStagingGridSummary");
            $grid.find(".dataTables_scrollBody").addClass("QnexScrollbar");

            if (gridName === "Summary")
            {
               window.setActionTabCount("WrhsMulti", $("#searchGrid_Summary").attr("multiCount"));
               $("#searchGrid_Summary").closest(".dataTables_scrollBody").height("auto"); //TODO - hardcoded multi status grid height to auto, which allows the grid to flunctuate in size using the search input field

               //while we're staging something, we should "select" the row that we're working on
               if (trackingId !== "0" && trackingId !== "" && editModeStage)
               {
                  $("#searchGrid_Summary tr[data-row-id='" + trackingId + "']").closest("tr").addClass("selected");
               }

               multiDt = $("#searchGrid_Summary").DataTable();
            }
            else if (gridName === "History")
            {
               $("#WrhsStagingHistory .formExDivFilter").html($lastHistoryFilter);
            }

            if (parseFloat(machineLocation) !== 0 && window.lockCtrl("TrackingNumber"))
            {
               $("#TrackingNumber").val("");
               $("#TrackingNumber").focus();
               finishClick = false;
            }

            $("#wait").hide();
            $("#dataTables_scrollHeadInner").css("width", "100%");
            //$("#dataTables_scrollHeadInner th.sorting_disabled").css("width", "0px");
            $("#dataTables_scrollHeadInner table").css("width", "100%");
            //$("#dataTables_scrollBody th.sorting_disabled").css("width", "0px");
            //$("#WrhsStagingGrid" + gridName + " .sorting_disabled").css("width", "0px");
            if (gridName === "History")
            {
               if (historySelected !== "")
               {
                  var rcvDate = historySelected.split(",")[0];
                  var crId = historySelected.split(",")[1];
                  $(".WrhsStagingDetail-div-click").each(function ()
                  {
                     if ($(this).find("td[exchange='TrackingNumber']").attr("exchange-value").toUpperCase() === $("#TrackingNumber").val().toUpperCase()
                        && $(this).find("td[exchange='RAOrWPT']").attr("exchange-value").toUpperCase() === $("#RAorWPT").val().toUpperCase()
                        && $(this).find("td[exchange='Date']").attr("exchange-value") === rcvDate
                        && $(this).find("td[exchange='CarrierID']").attr("exchange-value") === crId)
                     {
                        $(".WrhsStagingDetail-div-click.selected").removeClass("selected");
                        $(this).addClass("selected");
                        historySelected = "";
                        return false;
                     }

                  });

                  historySelected = "";
               }
            }
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name":
                  "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on staging request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save changes after edit .
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SUBMIT", function ()
   {
      if (getEditMode() && editModeStage || $("#CMD_SUBMIT").attr("src").indexOf("0") > 0 || tray)
      {
         if (finishClick)
         {
            return false;
         }
      }
      else
      {
         //partial grid simulate continue button
         if ($(".WrhsStagingPartialMatch-div-click").length > 0 && $(".WrhsStagingPartialMatch-div-click").hasClass("selected") && (!searchChange || newRaMode))
         {
            $("#PartialMatchContinue").click();
            return false;
         }

         newRaMode = false;

         //history grid simulate continue button
         if ($(".ql-body.active").attr("id") === "WrhsHistory" && $(".WrhsStagingDetail-div-click").hasClass("selected") && !searchChange)
         {
            $("#HistoryContinue").click();
            return false;
         }

         //multistatus grid simulate continue button
         if ($(".ql-body.active").attr("id") === "WrhsMulti" && $(".searchGrid_Summary-div-click").hasClass("selected") && !searchChange)
         {
            $("#MultiStatusContinue").click();
            return false;
         }

         setEmptySummary(); //clear Multistatus tab

         if (($("#TrackingNumber").val().length === 0 && $("#RAorWPT").val().length > 0) || ($("#TrackingNumber").val().length > 0 && $("#RAorWPT").val().length === 0) && !editModeStage)
         {
            $("#TrackingNumber").val($("#TrackingNumber").val().replace(/[^a-zA-Z0-9-]/gi, ""));
            lastSummaryForm = "";

            if ($("#TrackingNumber").val().length === 0)
            {
               searchStagingByTrackingId($("#RAorWPT").val(), "RA");
            }
            else
            {
               searchStagingByTrackingId($("#TrackingNumber").val(), "Tracking");
            }

         }
         else if ($("#TrackingNumber").val().length > 0 && $("#RAorWPT").val().length > 0 && !editModeStage)
         {
            lastStagingDetailForm = "";
            $("#TrackingNumber").val($("#TrackingNumber").val().replace(/[^a-zA-Z0-9-]/gi, ""));
            checkNumbersValidation($("#TrackingNumber").val(), $("#RAorWPT").val());
         }

      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Selecing a partial match to stage
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#PartialMatchContinue", function ()
   {
      if (!$("#WrhsStagingPartialMatch tr").hasClass("selected"))
      {
         return false;
      }

      var $partialRow = $("#WrhsStagingPartialMatch tr.selected");

      //New RA button was pressed, give warning to user that they're about to override a previously-received RA
      if (newRaMode && $("#RAorWPT").val() !== "" && $("#RAorWPT").val() !== $partialRow.find("td[exchange='RAorWPT']").attr("exchange-value"))
      {
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You Are About To Override The Received RA " + $partialRow.find("td[exchange='RAorWPT']").attr("exchange-value") + " With RA " + $("#RAorWPT").val() + ". Would You Like To Continue?</h3>"
         }];

         btnObjects = [{
            "id": "btn_newRaOverride",
            "name": "Yes",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='window.newRaOverride();'"
         }, {
            "id": "btn_no",
            "name": "No",
            "class": "btnErrorMessage",
            "style": "margin:0 10px;text-align:center;width:70px",
            "function": "onclick='$(this).closeMessageBox();'"
         }
         ];

         titleobjects = [{
            "title": "Confirm Return Authorization Override"
         }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         return false;
      }

      $("#wait").show();
      lastStagingDetailForm = "";

      if (window.lockCtrl("TrackingNumber") && trNumber !== $partialRow.find("td[exchange='TrackingNumber']").attr("exchange-value"))
      {
         window.lockCtrl("TrackingNumber", false);
      }

      setTimeout(function ()
      {
         getStagingDetail($partialRow.find("td[exchange='TrackingID']").closest("td").attr("exchange-value"), false, false);
      }, 100);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Allow users to replace a previously-received RA with a new RA, immediately stage it
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#NewRA", function ()
   {
      //row was not selected
      if (!$("#WrhsStagingPartialMatch tr").hasClass("selected"))
      {
         return false;
      }
      //selected row is not a received RA
      else if ($("#WrhsStagingPartialMatch tr.selected td[exchange='TrackingID']").attr("exchange-value") === "0")
      {
         return false;
      }

      newRaMode = true;
      $("#RAorWPT").val("").focus();
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SCALETARE1", function ()
   {
      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleTare",
         {
         }, function (result)
         {
            console.log("ScaleTare:" + result);

            setScaleBusy(false);

            if ($.parseJSON(result))
            {
               $("#ID_PRODUCTWT").val("");
               $("#ID_CONTENTWT").val("");

               $("#ID_PRODUCTWT").focus();
            }
            else
            {
               window.showWtQtyViolation(SEG_WARNING, ERR_NOACK);
            }
         });

      ga("send", "event", "SegmentWtQty", "tare", "tareButton");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var showWtQtyViolation = function (serverity, violation)
   {
      var messageObj = $(segmentSelector + " .QnexMessage .QnexCaption");

      // Clear any old messages and/or violations
      ///////////////////////////////////////////////////////////////////////////////
      messageObj.removeClass("error warning general");
      messageObj.text("");

      // Display the message and/or violation
      ///////////////////////////////////////////////////////////////////////////////
      switch (serverity)
      {
         case SEG_GENERAL:
            messageObj.addClass("general");
            break;

         case SEG_WARNING:
            messageObj.addClass("warning");
            break;

         case SEG_ERROR:
            messageObj.addClass("error");
            break;
      }

      messageObj.text(violation);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.showWtQtyViolation = showWtQtyViolation;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setScaleBusy(isBusy)
   {
      $("#CMD_SCALETARE1").prop("disabled", isBusy);
      $("#CMD_SCALECAPTURE1").prop("disabled", isBusy);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var checkScaleConditionStaging = function ()
   {
      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleConnected",
         {
         }, function (result)
         {
            console.log("ScaleConnected:" + result);

            if ($.parseJSON(result))
            {
               setScaleBusy(false);
               window.showWtQtyViolation(null, "");
            }
            else
            {
               window.showWtQtyViolation(SEG_WARNING, ERR_NOSCALE);
            }
         });
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.checkScaleConditionStaging = checkScaleConditionStaging;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SCALECAPTURE1", function ()
   {
      var oldFocus = "#" + previousFocus.prop("id");

      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleCapture",
         {
         }, function (result)
         {
            console.log("ScaleCapture:" + result);

            setScaleBusy(false);

            var arrayObj = "#ID_PRODUCTWT #ID_CONTENTWT";

            $.each(arrayObj.split(/\s+/), function (index, value)
            {
               if ($(value).val() === "" || value === oldFocus)
               {
                  $(value).val(result);
                  window.nextFocus(value);

                  return false;
               }
            });
         });

      ga("send", "event", "SegmentWtQty", "capture", "captureButton");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshStagingPage(sts)
   {

      $("#WrhsStagingDetailsForm").replaceWith($staginEmpty);
      $("#Carrier,#Date").val("");

      if (!tray)
      {
         $("#ControlMsg").html("");
      }

      $("#imgCtrlNbr").attr("src", "");
      finishClick = false;
      receivingId = 0;
      stagingDetailObjects = [];
      validateWeight = false;
      isHistory = false;
      editModeStage = false;
      segmentId = "WeightCondition";
      segmentSelector = "#" + segmentId;
      messageObject = [];
      btnObjects = [];
      titleobjects = [];
      debitMemoEntry = "";
      overrideDebitMemo = false;
      debitMemoValidate = false;
      newRaMode = false;
      toteList = [];
      validateUsername = "";
      $("#TrackingNumber").val(trNumber);
      rcvdDate = "";
      preLoadedWt = "";
      $("#Carrier").attr("data-field-value", carrierCodes);

      if (sts === 0)
      {
         setEmptySummary();
         window.setEditMode(false);
         $("#TrackingNumber,#RAorWPT").val("");
         $("#TrackingNumber,#RAorWPT").removeAttr("disabled readonly");
         $("#TrackingNumber").focus();
         lastSummaryForm = "";
         lastStagingDetailForm = "";
      }
      else
      {
         if ($("#Totefinish").length > 0)
         {
            $("#Totefinish").css("pointer-events", "");
         }

         $("#RAorWPT").removeAttr("disabled readonly");
         $("#RAorWPT").val("");
         $("#RAorWPT").focus();
         lastStagingDetailForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
         lastSummaryForm = $(".QoskContent div.QoskForm .formMax table.tableEx:first").clone();
      }
   }
});

//function that is called when clicking the garbage can on image in staging
//does ajax to make server delete the file on server
//then removes the div containing the image preview from the page
//special note - jquery has a hard time if the id has a period/dot in it
//that's why we cut it short before the file extension on the ID for the divs
function WrhsStagingDeletePendingBtnClick(event)
{
   var fileName = event.target.id;

   //DeleteSelectedImage with file name
   $.ajax({
      type: "POST",
      url: '/WrhsStaging/WrhsStaging/DeletePendingFile',
      data: JSON.stringify({ fileName: fileName }),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function (response)
      {
         if (response.success)
         {
            //although some characters are compatible for html markup, jquery does not like them
            //so far the list is . (period) which is in every file extension
            //and parentthesis () which are commonly found if you copy/paste a file several times making a file_copy (2) etc
            //space
            var divId = fileName;
            divId = 'div_' + divId.slice(0, divId.indexOf("."));
            var div = $('#' + divId);
            div.remove();
         }
         else
         {
            $(this).setErrorMessage(response.error, "Error Message", "Continue");
         }
      },
      error: function (error)
      {
         $(this).setErrorMessage("error, networking, communication, not from server code / catch", "Error Message", "Continue");
      }
   });
}

//Called when you click the upload button for a brand new image or pdf in staging
//Usually file upload control does a full post back of the page whereas this page
//was designed around dropzone and not for postback.
//Here we do ajax call where the file will be saved to the server.  Info is returned
//in json object including full location on server for href, if it's an image (or pdf)
//fileName in addition to success/error message.
//upon callback we construct a div that looks identical to the existing attachments icons etc.
//PS jquery doesn't like period/dot used for selectors so that's removed for the ID for deleting purposes
function WrhsStagingUploadPendingBtnClick()
{
   var totalFiles = document.getElementById("FileUpload").files.length;

   if (totalFiles == 0)
   {
      $(this).setErrorMessage("No Files Selected", "Error Message", "Continue");
      return;
   }

   for (var i = 0; i < totalFiles; i++)
   {
      var file = document.getElementById("FileUpload").files[i];

      if (file.type == "image/jpeg")
      {
         if (file.size > 3145728) //3 mill / mb
         {
            $(this).setErrorMessage("File size must be less than 3 mega bytes. Please reduce size and try again.", "Error Message", "Continue");
            return;
         }
      }
      var formData = new FormData();
      formData.append("FileUpload", file);
      $.ajax({
         type: "POST",
         url: '/WrhsStaging/WrhsStaging/UploadNewFile',
         data: formData,
         dataType: 'json',
         contentType: false,
         processData: false,
         success: function (response)
         {
            if (response.success)
            {
               var strImg = '';

               if (response.isImage)
               {
                  strImg = '<img src="' + response.pathOnServer + '" style="margin-top:-29px;width:70px;height:70px;" alt="click for download"/>';
               }
               else
               {
                  strImg = '<img src="/Images/SVG/paper-clip.svg" style="margin-top:-29px;width:70px;height:70px;" alt="click for download" />';
               }

               //although some characters are compatible for html markup, jquery does not like them
               //so far the list is . (period) which is in every file extension
               //and parentthesis () which are commonly found if you copy/paste a file several times making a file_copy (2) etc
               //space
               var divId = response.fileName;
               divId = 'div_' + divId.slice(0, divId.indexOf("."));

               var strImgDiv = '<div id="' +
                  divId +
                  '" data-row-name="' +
                  response.fileName +
                  '" class="pendingFileUpload" style="width:80px;height:80px;border:2px solid #ccc;border-radius:5px;padding:3px; margin-left: 30px; margin-right: 30px; margin-top: 15px; margin-bottom: 15px;float:left; display: inline-block; ">' +
                  '<img src="/Images/SVG/delete (1).svg" onclick="WrhsStagingDeletePendingBtnClick(event);" id="' +
                  response.fileName +
                  '" style="width:40px;margin-top:-31px;margin-left:80px; margin-top: -10px; cursor:pointer" />';
               strImgDiv = strImgDiv +
                  '<a href="' +
                  response.pathOnServer +
                  '"  target="_blank" style="width:100%;height:80px;">' +
                  strImg +
                  '</a></div>';

               $('#WrhsStagingPendingAttachments').prepend(strImgDiv);
            }
            else
            {
               $(this).setErrorMessage(response.error, "Error Message", "Continue");
            }
         },
         error: function (error)
         {
            $(this).setErrorMessage("error, networking or method visibility", "Error Message", "Continue");
         }
      });
   }
}