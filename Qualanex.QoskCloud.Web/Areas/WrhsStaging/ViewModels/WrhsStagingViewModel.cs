///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStagingViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsStaging.ViewModels
{
   using System.Collections.Generic;

   using Model;
   using Utility.Common;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStagingViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_STAGE";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Staging record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsStagingModel SelectedStagingModel { get; set; } = new WrhsStagingModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Staging list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> StagingDetails { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Tote list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> ToteList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   PackageCondition list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> PackageConditionList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   History list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> StagingHistoryList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   AttachmentFiles list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<SasUriModel> AttachmentFiles { get; set; } = new List<SasUriModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   AvailableTotes list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<string> AvailableTote { get; set; } = new List<string>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   PackageType list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> PackageTypeList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   ValidateTrkNoAndRA list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> ValidateTrkNoAndRA { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   BoxItems Container Condition for Container Item segment.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ContainerCondition BoxItems { get; set; } = new ContainerCondition();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Attachment list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingAttachement> Attachment { get; set; } = new List<WrhsStagingAttachement>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary list for the Staging detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> SummaryList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Debitmemo list for the debit memo detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsStagingModel> DebitMemoList { get; set; } = new List<WrhsStagingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   For C2 RAs, pull its form 222 information
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsStagingModel Form222Info { get; set; } = new WrhsStagingModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ContainerCondition
   {
      public string Name { get; set; }
      public string Code { get; set; }
      public int ContainerTypeIndex { get; set; }
      public string dataHeader { get; set; }
      public string form { get; set; }
      public string uniqSelection { get; set; }
      public string ItemCode { get; set; }
      public bool IsSelected { get; set; }
      public string ImageUrl{ get; set; }
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStagingRelationControl
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl StagingRelationControl { get; set; } = new RelationControl();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsStagingAttachement
   {
      public string Address { get; set; }
      public string FileName { get; set; }
      public string FileType { get; set; }
   }

}
