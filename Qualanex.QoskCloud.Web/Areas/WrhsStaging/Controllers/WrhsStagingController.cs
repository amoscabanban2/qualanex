﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsStagingController.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using System.Web.Razor.Parser;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;

namespace Qualanex.QoskCloud.Web.Areas.WrhsStaging.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System.IO;
   using System.Web.Helpers;
   using System;

   using Qualanex.QoskCloud.Web.Areas.WrhsStaging.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;
   using System.Web;
   using Product;
   using Sidebar;
   using Microsoft.WindowsAzure.Storage;
   using System.Configuration;
   using System.Net;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_STAGE")]
   public class WrhsStagingController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Staging View.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsStagingView()
      {

         DeleteFiles();

         this.ModelState.Clear();

         var viewModel = new WrhsStagingViewModel
         {
            ActionMenu = WrhsStagingModel.GetActionMenu(),
            PackageConditionList = WrhsStagingModel.PackageConditionList(),
            PackageTypeList = WrhsStagingModel.PackageTypeList()
         };
         ViewBag.UserList = WrhsStagingModel.GetUserNames();
         ViewBag.user = this.User.Identity.Name;

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete files for current user.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public void DeleteFiles()
      {
         var StartWith = this.HttpContext.User.Identity.Name + "_Staging";
         var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\WrhsStaging", Server.MapPath(@"\")));
         string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");
         bool isExists = System.IO.Directory.Exists(pathString);
         if (!isExists)
            System.IO.Directory.CreateDirectory(pathString);
         string[] fileList = System.IO.Directory.GetFiles(pathString);
         foreach (var fileName in fileList)
         {
            if (fileName.Contains(StartWith))
            {
               System.IO.File.Delete(fileName);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of items for dataList.
      /// </summary>
      /// <param name="browser"></param>
      /// <param name="item"></param>
      /// <param name="keyPressValue"></param>
      /// <param name="strData"></param>
      /// <param name="strFilterDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetDetailsName(string strData, string strFilterDate, int keyPressValue, string browser, string item)
      {
         var listEntity = WrhsStagingModel.GetListOptions(false, strData, strFilterDate, browser, item);

         var jsonResult = this.Json(new { listEntity = listEntity, keyPressValue = keyPressValue }, JsonRequestBehavior.AllowGet);
         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get staging information.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult StagingEntity()
      {
         this.ModelState.Clear();
         var viewModel = new WrhsStagingViewModel()
         {
            SelectedStagingModel = new WrhsStagingModel(),
            ToteList = new List<WrhsStagingModel>()
         };

         return this.PartialView("_WrhsStaging", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check tracking id validation.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckValidation(string trackingId)
      {
         var listEntity = WrhsStagingModel.CheckValidation(trackingId);

         var jsonResult = this.Json(new
         {
            TrackingID = listEntity.TrackingID,
            Name = listEntity.Name,
            Date = Convert.ToDateTime(listEntity.Date).ToShortDateString(),
            Code = listEntity.Code
         }, JsonRequestBehavior.AllowGet);
         jsonResult.MaxJsonLength = int.MaxValue;
         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get staging Information by tracking id and history status.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="isHistory"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetStagingDetail(string trackingId, bool isHistory, string reserved, int location)
      {
         DeleteFiles();

         var selectedModel = WrhsStagingModel.GetStagingDetails(trackingId, location,
                                     (!string.IsNullOrWhiteSpace(reserved)
                                        ? System.Web.Helpers.Json.Decode<WrhsStagingModel>(reserved)
                                        : new WrhsStagingModel())) ?? new WrhsStagingModel();

         var viewModel = new WrhsStagingViewModel()
         {
            SelectedStagingModel = selectedModel,
            ToteList = WrhsStagingModel.GetToteTrackingList(trackingId),
            AttachmentFiles = WrhsStagingModel.DownloadAttachedFilesFromAzure(trackingId),
            Form222Info = selectedModel.RAorWPTCtrlCode == "CTRL2" ? WrhsStagingModel.GetForm222Info(selectedModel.RAorWPT, selectedModel.Date) : new WrhsStagingModel()
         };

         var jsonResult = this.Json(new
         {
            Staging = this.ConvertViewToString("_WrhsStaging", viewModel),
            success = viewModel.SelectedStagingModel != null,
            error = viewModel.SelectedStagingModel == null,
            Count = WrhsStagingModel.CountLstUnStagedItems(trackingId, SidebarCommon.GetMachineName(this.Request)),
            Form222Info = viewModel.Form222Info,
            RAorWPTCtrlCode = !string.IsNullOrWhiteSpace(viewModel.SelectedStagingModel.TrackingID) ? viewModel.SelectedStagingModel.RAorWPTCtrlCode : ""
         }, JsonRequestBehavior.AllowGet);

         jsonResult.MaxJsonLength = int.MaxValue;
         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Save files into local storage by drag and drop.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult SaveUploadedFile()
      {
         var isSavedSuccessfully = true;
         var fName = "";
         var origFileNames = "";

         try
         {
            foreach (string fileName in Request.Files)
            {
               HttpPostedFileBase file = Request.Files[fileName];
               //Save file content goes here
               fName = Guid.NewGuid().ToString(); //file.FileName;
               if (file != null && file.ContentLength > 0)
               {
                  var originalDirectory = new System.IO.DirectoryInfo($"{Server.MapPath(@"\")}Areas\\WrhsStaging");
                  var pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");
                  var fileName1 = this.HttpContext.User.Identity.Name + "_Staging_" + System.IO.Path.GetFileName(file.FileName);
                  var isExists = System.IO.Directory.Exists(pathString);
                  origFileNames = file.FileName;

                  //Create temp directory if it doesn't exist
                  if (!isExists)
                  {
                     System.IO.Directory.CreateDirectory(pathString);
                  }

                  var path = $"{pathString}\\{fileName1}";
                  file.SaveAs(path);
               }
            }
         }
         catch (Exception ex)
         {
            isSavedSuccessfully = false;
         }

         if (isSavedSuccessfully)
         {
            return Json(new { Message = fName, originalFile = origFileNames });
         }
         else
         {
            return Json(new { Message = "Error in saving file", originalFile = origFileNames });
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save detail of staging when we click on save or finish button.
      /// </summary>
      /// <param name="attachedEntity"></param>
      /// <param name="detailEntity"></param>
      /// <param name="toteEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveStagingDetail(string detailEntity, string toteEntity, string attachedEntity, int intStep, string machineId, string debitMemo, string pkgCodes)
      {
         var details = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(detailEntity);
         details.FirstOrDefault().DebitMemo = debitMemo;
         var pckgCodes = System.Web.Helpers.Json.Decode<List<string>>(pkgCodes);
         var tote = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(toteEntity);
         var attach = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(attachedEntity);
         var curUser = this.HttpContext.User.Identity.Name;
         var blnCheck = WrhsStagingModel.SaveStagingDetails(details[0], tote, attach, machineId, curUser, intStep, pckgCodes);
         return Json(new
         {
            Success = !string.IsNullOrWhiteSpace(blnCheck.Keys.FirstOrDefault().TrackingID),
            Error = string.IsNullOrWhiteSpace(blnCheck.Keys.FirstOrDefault().TrackingID),
            Count = WrhsStagingModel.CountLstUnStagedItems(details[0].TrackingID, machineId),
            folder = blnCheck.Values.FirstOrDefault(),
            row = blnCheck.Keys.FirstOrDefault()
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete local selected images by their fileName.
      /// </summary>
      /// <param name="fileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult DeleteSelectedImage(string fileName)
      {
         var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\WrhsStaging", Server.MapPath(@"\")));
         string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");
         var fileName1 = this.HttpContext.User.Identity.Name + "_Staging_" + System.IO.Path.GetFileName(fileName);
         var path = string.Format("{0}\\{1}", pathString, fileName1);
         if (System.IO.File.Exists(path))
         {
            System.IO.File.Delete(path);
         }
         return Json(new { Success = true, Error = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   History tab.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsStagingHistory(WrhsStagingModel criteria)
      {
         this.ModelState.Clear();

         var viewModel = new WrhsStagingViewModel()
         {
            StagingHistoryList = WrhsStagingModel.GetHistoryList(false, criteria, this.HttpContext.User.Identity.Name)
         };

         ViewBag.Date = criteria.Date;
         ViewBag.ToDate = criteria.ModifiedDate;
         ViewBag.user = this.User.Identity.Name;

         return this.PartialView("_WrhsStagingHistory", viewModel.StagingHistoryList);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get tracking ID by tracking number and Ra.
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="trackingNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetTrackingId(string trackingNumber, string ra, int location, string reserved)
      {
         var slk = WrhsStagingModel.GetExactMatch(trackingNumber, ra, location
            , (!string.IsNullOrWhiteSpace(reserved)
                  ? System.Web.Helpers.Json.Decode<WrhsStagingModel>(reserved) : new WrhsStagingModel()));
         var partialMatch = new Dictionary<string, List<WrhsStagingModel>>();
         if (!slk.FirstOrDefault().Key.Contains(",") && string.IsNullOrWhiteSpace(slk.FirstOrDefault().Value.TrackingID))
         {
            partialMatch = WrhsStagingModel.GetPartialMatch(trackingNumber, ra);
         }
         else
         {
            if (slk.FirstOrDefault().Key.Contains(","))
            {
               trackingNumber = slk.FirstOrDefault().Key.Split(',')[0];
               ra = slk.FirstOrDefault().Key.Split(new char[] { ',' }, 2, StringSplitOptions.None)[1];
               if (!string.IsNullOrWhiteSpace(slk.FirstOrDefault().Value.RAorWPT) && ra != slk.FirstOrDefault().Value.RAorWPT)
               {
                  ra = slk.FirstOrDefault().Value.RAorWPT;
               }
            }
         }

         var viewModel = new WrhsStagingViewModel()
         {
            SelectedStagingModel = slk.FirstOrDefault().Key.Contains(",") ? slk.FirstOrDefault().Value : new WrhsStagingModel(),
            ToteList = slk.FirstOrDefault().Key.Contains(",")
                                 ? WrhsStagingModel.GetToteTrackingList(slk.FirstOrDefault().Value.TrackingID)
                                 : new List<WrhsStagingModel>(),
            AttachmentFiles = WrhsStagingModel.DownloadAttachedFilesFromAzure(slk.FirstOrDefault().Value.TrackingID),
            Form222Info = slk.FirstOrDefault().Key.Contains(",") && slk.FirstOrDefault().Value.RAorWPTCtrlCode == "CTRL2"
                                          ? WrhsStagingModel.GetForm222Info(ra, slk.FirstOrDefault().Value.Date)
                                          : new WrhsStagingModel(),
            ValidateTrkNoAndRA = partialMatch.FirstOrDefault().Value
         };

         var jsonResult = this.Json(new
         {
            success = true,
            matching = "",
            error = false,
            Message = slk.FirstOrDefault().Key == "" && partialMatch.Count > 0 ? partialMatch.FirstOrDefault().Key : slk.FirstOrDefault().Key,
            Received = slk.FirstOrDefault().Value.StgeDate.HasValue
                             ? slk.FirstOrDefault().Value
                             : viewModel.SelectedStagingModel,
            Staging = this.ConvertViewToString("_WrhsStaging", viewModel),
            Count = slk.FirstOrDefault().Key.Contains(",")
            ? WrhsStagingModel.CountLstUnStagedItems(slk.FirstOrDefault().Value.TrackingID, SidebarCommon.GetMachineName(this.Request))
            : 0,
            Form222Info = viewModel.Form222Info,
            TrackingNumber = trackingNumber,
            RA = ra
            //Summary = this.ConvertViewToString("_WrhsStagingSummary", viewModel.SummaryList)
         });
         jsonResult.MaxJsonLength = int.MaxValue;
         return jsonResult;
      }

      private string GetMatchingHtml(WrhsStagingModel viewModel)
      {
         return this.ConvertViewToString("_WrhsStaging", viewModel);
      }

      private string ConvertViewToString(string viewName, object model)
      {
         this.ViewData.Model = model;
         using (StringWriter writer = new StringWriter())
         {
            ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
            ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, this.ViewData, new TempDataDictionary(), writer);
            vResult.View.Render(vContext, writer);
            return writer.ToString();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get AttachedFiles by tracking ID.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetAttacheFiles(string trackingId)
      {
         var viewModel = new WrhsStagingViewModel()
         {
            AttachmentFiles = WrhsStagingModel.DownloadAttachedFilesFromAzure(trackingId)
         };

         return this.PartialView("_AttachedFiles", viewModel.AttachmentFiles);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get AttachedFiles by tracking ID.
      /// </summary>
      /// <param name="RA"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckRAValidation(string RA)
      {
         var validRA = WrhsStagingModel.CheckValidateRA(RA);

         return this.Json(new
         {
            Success = validRA,
            Error = !validRA
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Attempt to place RA into a tote in Staging
      /// </summary>
      /// <param name="toteCode"></param>
      /// <param name="raCtrlCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckToteValidation(string toteCode, string raCtrlCode)
      {
         var validTote = WrhsStagingModel.CheckToteValidation(toteCode, raCtrlCode);

         return this.Json(new
         {
            Success = validTote != "0" && validTote != "-1" && validTote != "-2",
            Error = validTote == "0" || validTote == "-1" || validTote == "-2",
            ValidTote = validTote
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get AttachedFiles by tracking ID.
      /// </summary>
      /// <param name="debitMemo"></param>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckDebitMemoValidation(string debitMemo, string raNumber)
      {
         var validDebitMemo = WrhsStagingModel.CheckValidateDebitMemo(raNumber, debitMemo);

         return this.Json(new
         {
            Success = validDebitMemo,
            Error = !validDebitMemo,
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get AttachedFiles by tracking ID.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsStagingSummary(WrhsStagingModel criteria)
      {
         this.ModelState.Clear();

         criteria.ScrollLoadCount = 50;
         criteria.LoadAtHeight = 10;
         var machineId = SidebarCommon.GetMachineName(this.Request);

         var viewModel = new WrhsStagingViewModel()
         {
            SummaryList = criteria.TrackingID == "0" || string.IsNullOrWhiteSpace(criteria.TrackingID)
                                 ? new List<WrhsStagingModel>()
                                 : WrhsStagingModel.GetRAByTrackingNumber(criteria.TrackingID, criteria)
         };

         ViewBag.MachineName = machineId;
         ViewBag.TrackingID = criteria.TrackingID;
         ViewBag.Count = !string.IsNullOrWhiteSpace(criteria.TrackingID) && criteria.TrackingID != "0"
            ? WrhsStagingModel.CountLstUnStagedItems(criteria.TrackingID, SidebarCommon.GetMachineName(this.Request))
            : 0;
         return this.PartialView("_WrhsStagingSummary", viewModel.SummaryList);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get AttachedFiles by tracking ID.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="reserved"></param>
      /// <param name="location"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult CheckSingleEntry(string trackingField, string entryField, string reserved, int location)
      {
         DeleteFiles();
         var blnCheck = new WrhsStagingModel();
         var trackingPartialMatch = new Dictionary<string, List<WrhsStagingModel>>();
         if (entryField == "Tracking")
         {
            blnCheck = WrhsStagingModel.GetStagingDetails(trackingField, location,
                               (!string.IsNullOrWhiteSpace(reserved)
                                     ? System.Web.Helpers.Json.Decode<WrhsStagingModel>(reserved) : new WrhsStagingModel()), "Search")
                                                 ?? new WrhsStagingModel();

            if (string.IsNullOrWhiteSpace(blnCheck.TrackingID))
            {
               trackingPartialMatch = WrhsStagingModel.GetPartialMatch(trackingField);
            }
            else
            {
               if (blnCheck.Code == "NP" || blnCheck.Code == "NR")
               {
                  blnCheck.SpecialHandling = true;
               }
            }
         }
         else
         {
            trackingPartialMatch = WrhsStagingModel.GetPartialMatchRA(trackingField);

            if (trackingPartialMatch.FirstOrDefault().Value.Count == 0)
            {
               trackingPartialMatch = WrhsStagingModel.GetRANoTracking(trackingField);
            }
         }

         var viewModel = !string.IsNullOrWhiteSpace(blnCheck.TrackingID) && blnCheck.Code != "NP" && blnCheck.Code != "NR"
            ? new WrhsStagingViewModel()
            {
               SelectedStagingModel = blnCheck,
               ToteList = WrhsStagingModel.GetToteTrackingList(trackingField),
               AttachmentFiles = WrhsStagingModel.DownloadAttachedFilesFromAzure(trackingField),
               Form222Info = blnCheck.RAorWPTCtrlCode == "CTRL2" ? WrhsStagingModel.GetForm222Info(blnCheck.RAorWPT, blnCheck.Date) : new WrhsStagingModel(),
               ValidateTrkNoAndRA = new List<WrhsStagingModel>()
            }
            : new WrhsStagingViewModel()
            {
               ValidateTrkNoAndRA = trackingPartialMatch.FirstOrDefault().Value ?? new List<WrhsStagingModel>(),
               SelectedStagingModel = blnCheck
            };

         var jsonResult = this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(blnCheck.TrackingNumber) ? true : false,
            Error = string.IsNullOrWhiteSpace(blnCheck.TrackingNumber) ? true : false,
            trackingId = string.IsNullOrWhiteSpace(blnCheck.TrackingID)
            ? "0"
            : blnCheck.TrackingID,
            trackingNumber = string.IsNullOrWhiteSpace(blnCheck.TrackingNumber)
            ? "" : blnCheck.TrackingNumber,
            RAorWPT = string.IsNullOrWhiteSpace(blnCheck.RAorWPT) ? "" : blnCheck.RAorWPT,
            Date = string.IsNullOrWhiteSpace(blnCheck.Date) ? "" : Convert.ToDateTime(blnCheck.Date).ToShortDateString(),
            Name = string.IsNullOrWhiteSpace(blnCheck.Carrier) ? "" : blnCheck.Carrier,
            Code = string.IsNullOrWhiteSpace(blnCheck.CarrierID) ? "" : blnCheck.CarrierID,
            Staging = (!string.IsNullOrWhiteSpace(blnCheck.TrackingID) || viewModel.ValidateTrkNoAndRA.Count > 0)
                           ? this.ConvertViewToString("_WrhsStaging", viewModel)
                           : "",
            //Summary = !string.IsNullOrWhiteSpace(blnCheck.TrackingID) ? this.ConvertViewToString("_WrhsStagingSummary", viewModel.SummaryList):"",
            blnStged = !string.IsNullOrWhiteSpace(blnCheck.ModifiedDate) ? 1 : 0,
            form222PrintDate = string.IsNullOrWhiteSpace(blnCheck.Form222PrintedDate) ? "" : blnCheck.Form222PrintedDate,
            Ctrl = !string.IsNullOrWhiteSpace(blnCheck.TrackingID) ? blnCheck.RAorWPTCtrlCode : "",
            Count = string.IsNullOrWhiteSpace(blnCheck.TrackingID) || ((blnCheck.Code == "NP" || blnCheck.Code == "NR") && location != 0)
                  ? 0
                  : WrhsStagingModel.CountLstUnStagedItems(blnCheck.TrackingID, SidebarCommon.GetMachineName(this.Request)),
            Type = blnCheck.Code,
            Form222Info = viewModel.Form222Info,
            SearchBy = viewModel.ValidateTrkNoAndRA.Count > 0
                              ? trackingPartialMatch.FirstOrDefault().Key
                              : ""
         }, JsonRequestBehavior.AllowGet);

         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get machine Location.
      /// </summary>
      /// <param name="machineName"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetMachineLocation(string machineName)
      {
         var strCheck = WrhsStagingModel.GetMachineLocation(machineName);
         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(strCheck),
            Error = string.IsNullOrWhiteSpace(strCheck),
            Location = strCheck
         });
      }

      ///****************************************************************************
      /// <summary>
      ///   Get lookup partial.
      /// </summary>
      /// <param name="debitMemo"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult RALookup(string debitMemo = null)
      {
         ViewBag.debitMemo = debitMemo;
         var viewModel = new WrhsStagingViewModel
         {
            DebitMemoList = new List<WrhsStagingModel>()
            //string.IsNullOrWhiteSpace(debitMemo)
            //                  ? new List<WrhsStagingModel>()
            //                  : WrhsStagingModel.GetDebitMemoList(debitMemo, "Begin")
         };
         return this.PartialView("_RALookup", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get search result.
      /// </summary>
      /// <param name="searchArg"></param>
      /// <param name="debitMemo"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult RALookupSearch(string searchArg, string debitMemo = null)
      {
         ViewBag.debitMemo = debitMemo;
         var viewModel = new WrhsStagingViewModel
         {
            DebitMemoList = string.IsNullOrWhiteSpace(debitMemo)
                              ? new List<WrhsStagingModel>()
                              : WrhsStagingModel.GetDebitMemoList(debitMemo, searchArg)
         };
         return this.PartialView("_RALookupDetails", viewModel.DebitMemoList);
      }

      ///****************************************************************************
      /// <summary>
      ///   Replace RA from the Debit Memo Search Lookup in Totes/Attachments
      /// </summary>
      /// <param name="detailEntity"></param>
      /// <param name="attachedEntity"></param>
      /// <param name="machineId"></param>
      /// <param name="toteEntity"></param>
      /// <param name="reserved"></param>
      /// <param name="pkgCodes"></param>
      /// <param name="location"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ReplaceRA(string detailEntity, string toteEntity, string attachedEntity, string machineId, string reserved, string pkgCodes, int location)
      {
         var details = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(detailEntity);
         var tote = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(toteEntity);
         var pckgCodes = System.Web.Helpers.Json.Decode<List<string>>(pkgCodes);
         var attach = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(attachedEntity);
         var curUser = this.HttpContext.User.Identity.Name;
         var trckRecord = WrhsStagingModel.ReplaceRa(details[0], tote, attach, machineId, curUser, pckgCodes);
         var getStagingDetail = WrhsStagingModel.GetStagingDetails(details[0].TrackingID, location,
             (!string.IsNullOrWhiteSpace(reserved)
                  ? System.Web.Helpers.Json.Decode<WrhsStagingModel>(reserved) : new WrhsStagingModel())) ?? new WrhsStagingModel();

         var viewModel = new WrhsStagingViewModel()
         {
            SelectedStagingModel = getStagingDetail,
            ToteList = WrhsStagingModel.GetToteTrackingList(getStagingDetail.TrackingID),
            AttachmentFiles = WrhsStagingModel.DownloadAttachedFilesFromAzure(getStagingDetail.TrackingID),
            Form222Info = getStagingDetail.RAorWPTCtrlCode == "CTRL2" ? WrhsStagingModel.GetForm222Info(getStagingDetail.RAorWPT, getStagingDetail.Date) : new WrhsStagingModel()
         };

         return this.Json(new
         {
            Success = trckRecord.Keys.FirstOrDefault() ? true : false,
            Error = !trckRecord.Keys.FirstOrDefault() ? false : true,
            trackingId = details[0].TrackingID,
            Form222PrintedDate = !string.IsNullOrWhiteSpace(viewModel.SelectedStagingModel.Form222PrintedDate)
                           ? Convert.ToDateTime(viewModel.SelectedStagingModel.Form222PrintedDate).ToShortDateString()
                           : "",
            RAorWPTCtrlCode = viewModel.SelectedStagingModel.RAorWPTCtrlCode,
            staging = this.ConvertViewToString("_WrhsStaging", viewModel),
            Form222Info = viewModel.Form222Info
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Tracking Entity. We had to apply logic from WrhsRecvController.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsRecvController.cs under the function WrhsRecvSave().
      /// </summary>
      /// <param name="trackingEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult NewReceivingSave(string trackingEntity)
      {
         var blnCheck = System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(trackingEntity)[0].TrackingID == "0"
            ? WrhsStagingModel.SaveTracking(System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(trackingEntity)[0], this.HttpContext.User.Identity.Name)
            : WrhsStagingModel.EditReceiving(System.Web.Helpers.Json.Decode<List<WrhsStagingModel>>(trackingEntity)[0], this.HttpContext.User.Identity.Name);

         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(blnCheck.TrackingID),
            Error = string.IsNullOrWhiteSpace(blnCheck.TrackingID),
            Tracking = blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Count.
      /// </summary>
      /// <param name="trackingNumber"></param>
      /// <param name="carrier"></param>
      /// <param name="date"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetCountUnderTracking(string trackingNumber, string carrier, string date)
      {
         var cnt = WrhsStagingModel.GetCountTrackingNumber(trackingNumber, carrier, date);

         return this.Json(new
         {
            Count = cnt
         }, JsonRequestBehavior.AllowGet);
      }

      /// <summary>
      /// anchor with href to blob causes blob to download instead of open in browser even when using target.  GetBlob will
      /// add headers for the type of file which allows the browser to open it instead of download.  This method is only for 
      /// blobs already saved / "finished" and in azure "qoskkiosksync" container.    
      /// </summary>
      /// <param name="url">Full url to an image file or pdf</param>
      /// <param name="isImage">Bool, if true then mime-type/header will be fore image/jpg else application/pdf</param>
      /// <returns></returns>
      public FileContentResult GetBlob(string url, bool isImage = true)
      {
         CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
             ConfigurationManager.ConnectionStrings["Storage.qoskkiosksync"].ConnectionString);

         //  create a blob client.
         CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

         //  create a container example
         //CloudBlobContainer container = blobClient.GetContainerReference("containerone");

         CloudBlockBlob blob = new CloudBlockBlob(new Uri(url), storageAccount.Credentials);
         var memStream = new MemoryStream();
         blob.DownloadToStream(memStream);
         File(memStream.ToArray(), blob.Properties.ContentType);
         Response.AppendHeader("Content-Disposition", "inline; filename=" + url);

         if (isImage)
         {
            return File(memStream.ToArray(), "image/jpeg");
         }
         else
         {
            return File(memStream.ToArray(), "application/pdf");
         }

      }

      /// <summary>
      /// Similar to DeleteSelected image, but this is for ajax post and made to accept the full file name with username embedded etc
      /// adding try catch
      /// </summary>
      /// <param name="fileName">full filename of file to be deleted from areas\wrhsstaging\attached files - can be image or pdf</param>
      /// <returns></returns>
      [HttpPost]
      public ActionResult DeletePendingFile(string fileName)
      {
         try
         {
            var originalDirectory = new System.IO.DirectoryInfo(string.Format("{0}Areas\\WrhsStaging", Server.MapPath(@"\")));
            string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");
            var path = string.Format("{0}\\{1}", pathString, fileName);
            if (System.IO.File.Exists(path))
            {
               System.IO.File.Delete(path);
            }
            return Json(new { success = true });
         }
         catch (Exception e)
         {
            return Json(new { success = false, error = e.Message });
         }

      }

      /// <summary>
      /// Mostly the same as SaveUploadedFile, but with some extra functionality.  Updloads a new file to server
      /// puts it in areas\wrhsstaging\attachedfiles\
      /// user name is contained in file which is important in other parts of the code
      /// SaveUploadedFile could possibly be deleted - didn't know if that was maybe used if any other parts of code and being cautious
      /// </summary>
      /// <returns>
      /// Json object - success true/false,
      /// IsImage - false if pdf, true all image types, logic placed here to reduce javascript code and C# has powerful string manipulation, easier to read on and on
      /// pathOnServer - to use as href,
      /// fileName - used as identifier if deleting pending files
      /// </returns>
      [HttpPost]
      public string UploadNewFile()
      {
         try
         {
            var fName = string.Empty;
            string fileName1 = string.Empty;
            string fullPath = string.Empty;

            //this is called in a loop per file for simplicty sake by the caller, so we should only ever see one here if you're wordering about the code
            for (int i = 0; i < Request.Files.Count; i++)
            {

               HttpPostedFileBase file = Request.Files[i];
               //Save file content goes here
               fName = Guid.NewGuid().ToString(); //file.FileName;
               if (file != null && file.ContentLength > 0)
               {
                  var originalDirectory = new System.IO.DirectoryInfo($"{Server.MapPath(@"\")}Areas\\WrhsStaging");
                  var pathString = System.IO.Path.Combine(originalDirectory.ToString(), "AttachedFiles");
                  fileName1 = this.HttpContext.User.Identity.Name + "_Staging_" + System.IO.Path.GetFileName(file.FileName);
                  //%, # and + make bad urls for image
                  //you would think just URLEncode them, but that creates a security problem - search this error message for more info...
                  //"The request filtering module is configured to deny a request that contains a double escape sequence."
                  //simply going to remove these characters, these are for the actual storage location, so it needs to happen on the server-side
                  fileName1 = fileName1.Replace("#", "_");
                  fileName1 = fileName1.Replace("+", "_");
                  fileName1 = fileName1.Replace("%", "_");

                  //related to this, the filename is used as a div ID, when called in jquery some names can cause a problem if they have 
                  //parenthesis and or spaces.  This was a problem when clicking the delete button for pending attachments.
                  //Instead of haviing replaces in 3 different places, let's consolidate them all here
                  fileName1 = fileName1.Replace("(", "_");
                  fileName1 = fileName1.Replace(")", "_");
                  fileName1 = fileName1.Replace(" ", "_");

                  var isExists = System.IO.Directory.Exists(pathString);

                  //Create temp directory if it doesn't exist
                  if (!isExists)
                  {
                     System.IO.Directory.CreateDirectory(pathString);
                  }

                  fullPath = $"{pathString}\\{fileName1}";

                  if (System.IO.File.Exists(fullPath))
                  {
                     return JsonConvert.SerializeObject(new { success = false, error = "File is already uploaded" });
                  }
                  file.SaveAs(fullPath);
               }
            }

            bool isImg = !fileName1.EndsWith(".pdf");
            return JsonConvert.SerializeObject(new { success = true, status = "success", isImage = isImg, pathOnServer = "/Areas/WrhsStaging/AttachedFiles/" + fileName1, fileName = fileName1 });
         }
         catch (Exception e)
         {
            return JsonConvert.SerializeObject(new { success = false, error = e.Message });
         }

      }

   }
}
