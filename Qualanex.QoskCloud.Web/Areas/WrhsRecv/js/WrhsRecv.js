﻿//*****************************************************************************************
//*
//* WrhsRecv.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Provides functionality for the DockReceiving partial class.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   // ...
   ///////////////////////////////////////////////////////////////////////////////
   var TAB_SUMMARY = "tabs-summary";
   var TAB_TRACKING = "tabs-tracking";
   var KEY_CtrlM = 10; //Note that keyCode = 10 stands for ctrl+m, which is what the barcode scanners use

   //=============================================================================
   // Local variables
   //-----------------------------------------------------------------------------
   var notForProcessing = false;
   var baseUrl = "/WrhsRecv";
   var routeCode = -1;
   var trckId = 0;
   var table;
   var disabled = false;
   var trackingID = 0;
   var keyPressVal = 0;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   var editMode = false;
   var active = "";
   var carrierId = "";
   var lockedtrckNo = false;
   var validate = false;
   var currentTrckNo = "";
   var oldDate = "";
   var entered = false;
   var hiddenTr = "";
   var hiddenTrNothing = "";
   var $collapsedRow = "";
   var $summaryEmptyRow = "";
   var $trackingEmptyRow = "";
   var printTrackingId = "";
   var printTrackingNumber = "";
   var printRaNumber = "";
   var ntChnge = false;
   var txtChange = false;
   var $lastSummaryFilter = "";

   //these two variables hold the Data Table for each tab (not just the HTML table)
   var summaryDt = null;
   var trackingDt = null;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      hiddenTr = "<tr dateRow='0' class='WrhsRecvDetail-div-click infiniteScrollRow MgmtEntity' name='SearchListItem'";
      hiddenTr += " MgmtEntity-id='0' data-ql-ajax='true' MgmtEntity-target='#formMax'>";
      hiddenTr += "<td exchange='DateHidden' exchange-value='' hidden></td> <td class='whiteTextCol' exchange='Date' exchange-value=''><span></span></td>";
      hiddenTr += "<td exchange='Carrier' exchange-value=''></td><td exchange='Count' exchange-value=''> </td>";
      hiddenTr += "<td exchange='PackageName' exchange-value='' hidden></td><td exchange='PackageCode' exchange-value='' hidden></td>";
      hiddenTr += "<td exchange='CarrierID' exchange-value='' hidden></td>";
      hiddenTr += "<td exchange='StageDate' exchange-value='0' hidden></td></tr>";
      hiddenTrNothing = "<tr class='odd'><td valign='top' colspan='3' class='dataTables_empty'>No data available in table</td></tr>";
      table = window.dtHeaders("#searchGrid", "ordering : false", ["sorting"], []);
      $("#searchGrid td.dataTables_empty ").html("Please wait while data is loading...");
      window.enableActionbarControl("#CMD_FILTER", $(".ql-body.active .formExDivFilter").length > 0);

      window.addGridRefreshMethod(function ()
      {
         $("#searchGrid").closest(".dataTables_scrollBody").height("auto").addClass("QnexScrollbar"); //TODO - hardcoded multi status grid height to auto, which allows the grid to flunctuate in size using the search input field
         $("#searchGrid").DataTable().draw();
         summaryDt = $("#searchGrid").DataTable();
         $("#searchGrid tbody").find("tr:eq(0)").click();
      });

      window.addGridStyleMethod(function ()
      {
         reorderSummary(true);
         summaryDt = $("#searchGrid").DataTable();
      });

      initialState();

      $summaryEmptyRow = "<tr class='odd'><td valign='top' colspan='3 class='dataTables_empty'>No data available in table</td></tr>";
      $collapsedRow = "<tr class='odd'><td valign='top' colspan='6' class='dataTables_empty'>Click here to expand list</td></tr>";
      $trackingEmptyRow = "<tr class='odd'><td valign='top' colspan='4' class='dataTables_empty'>No data available in table</td></tr>";
      $("#tab-container a[href='#tabs-summary']").closest("a").click();

      $("#wait").hide();

      window.OnQnexRefresh();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Readonly mode and select first row of grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function initialState()
   {
      setEditMode(false);

      if (!$("#searchGrid tbody tr").hasClass("selected"))
      {
         $("#searchGrid tbody").find("tr:eq(0)").click();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function ()
   {
      switch (window.getSelectedActionTab())
      {
         // ...
         //=============================================================================
         case TAB_SUMMARY:
            if (!getEditMode())
            {
               $lastSummaryFilter = $(".ql-body.active #DockReceivingGridSummary .formExDivFilter").html();
               $(".filterFrame .formExDiv").html($(".ql-body.active div.formExDivFilter").html());
               $(".SubmitCriteria").click();
               return true;
            }

            break;

         // ...
         //=============================================================================
         case TAB_TRACKING:
            unselectedTrackingGrid();
            setCollapseRow();
            enableActionbarControl("#CMD_NEW", false);
            enableActionbarControl("#CMD_EDIT", false);
            getTrackingDetails();
            break;
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexNew = function ()
   {
      if (!getEditMode() && window.getSelectedActionTab() === TAB_SUMMARY)
      {
         $(".ClearCriteria").click();
         $(".SubmitCriteria").click();
         $("#searchGrid tbody").find("tr:eq(0)").click();

         //clear the Tracking tab content
         unselectedTrackingGrid();
         setCollapseRow();

         //make sure the grid loses its filters before entering a new carrier record
         window.onGridReload = function ()
         {
            $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
            $("#ID_ROUTEVALUE span").html("--");
            $("#Date").datepicker();

            lockedtrckNo = false;
            active = TAB_SUMMARY;

            if ($(".WrhsRecvDetail-div-click").length > 0)
            {
               $(".WrhsRecvDetail-div-click:first").before(hiddenTr);
            }
            else
            {
               hiddenTrNothing = $("#searchGrid tbody tr");
               $("#searchGrid tbody tr").closest("tr").remove();
               $("#searchGrid tbody").append(hiddenTr);
            }

            trckId = 0;
            setEditMode(true);
            $("#Notes").val("");
            addDataListOptions("carrierList", "", keyPressVal);
            editMode = false;
            carrierId = "";

            enableActionbarControl("#CMD_REFRESH", false);
            enableActionbarControl("#CMD_SAVE", false);
            enableActionbarControl("#CMD_FILTER", false);

            $("#Carrier,#Notes").removeAttr("disabled");
            $("#Carrier").attr({ "data-field-value": "" }, { "name": "" });
            $("#Count").prop("readonly", "readonly");
            $(".formBody").find("input[type=Text]").val("");

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10)
            {
               dd = '0' + dd;
            }

            if (mm < 10)
            {
               mm = '0' + mm;
            }

            today = mm + '/' + dd + '/' + yyyy;
            document.getElementById("Date").value = today;

            $("#Date").removeAttr("disabled");
            $("#Count").html("0");

            updateDataTable($("#Count").html(), "Count", trckId);
            updateDataTable($("#Date").val(), "Date", trckId);

            if ($("#searchGrid tr").hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }

            //$("#searchGrid tbody tr.hidden").removeClass("hidden");
            $("#searchGrid tbody").find("tr:first").addClass("selected");
            $("#searchGrid tbody").find("tr:first td[exchange='DateHidden']").attr("exchange-value", today);
            $(".inputRex.editable:not([readonly][disabled]):first").focus();
            window.onGridReload = undefined;
         }
      }
      else if (window.getSelectedActionTab() === TAB_TRACKING && $(".searchGrid_WrhsRcvDetails-div-click").hasClass("selected"))
      {
         $(".searchGrid_WrhsRcvDetails-div-click.selected").closest("tr").click();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click the Edit button
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexEdit = function ()
   {
      currentTrckNo = "";

      if ($(".WrhsRecvDetail-div-click").length === 0)
      {
         return false;
      }

      if (!getEditMode())
      {
         if (window.getSelectedActionTab() === TAB_TRACKING)
         {
            if (!$("#searchGrid_WrhsRcvDetails tr").hasClass("selected"))
            {
               return false;
            }

            if ($("#searchGrid_WrhsRcvDetails tr.selected td[exchange='StagingDate']")
               .closest("td").attr("exchange-value") === "1")
            {
               return false;
            }

            enableActionbarControl("#CMD_REFRESH", false);
            enableActionbarControl("#CMD_FILTER", false);

            editMode = true;
            window.setEditMode(true);

            active = TAB_TRACKING;

            currentTrckNo = $("#ID_TRKNO").val();

            $("#WrhsRecvTracking #ID_TRKNO,#ID_RA").removeAttr("readonly disabled").removeClass("inputError").css("background-color", "#ECDD9D");
            $("#WrhsRecvTracking .btnErrorMessage ,#ID_SUBMIT_VALUE ").on("click");

            disabled = false;

            var attSrc = $("#ID_Lock_TrackingNumber").attr("src");
            var $newSrc = "";

            if (attSrc.indexOf("(OFF)") <= 0)
            {
               $newSrc = attSrc.replace("(ON)", "(OFF)");
               $("#ID_TRKNO").removeAttr("readonly");
               $("#ID_Lock_TrackingNumber").attr("src", $newSrc);
            }

            $("#ID_TRKNO").focus().keyup();
            $("#ID_RA").focus().keyup();
            $("#DockReceivingBody .inputRex,#DockReceivingBody .selectEx").attr("readonly", "readonly");
            $("#DockReceivingBody .inputRex,#DockReceivingBody .selectEx").attr("disabled", "disabled");
            trackingID = $("#searchGrid_WrhsRcvDetails tr.selected").attr("data-row-id");
            $("#btn_not_processing,#ID_NO_RA").removeAttr("disabled");
         }
         else
         {
            txtChange = false;
            $("#Date").datepicker();
            lockedtrckNo = false;
            oldDate = $("#Date").val();
            active = TAB_SUMMARY;
            window.setEditMode(true);
            editMode = true;
            trckId = $("#searchGrid tr.selected").closest("tr").attr("MgmtEntity-id");

            // Check for staged records under date and carrier 
            var requesting = $.ajax({
               cache: false,
               type: "POST",
               datatype: "JSON",
               data: "date=" + $(".WrhsRecvDetail-div-click.selected td[exchange='DateHidden']").attr("exchange-value")
               + "&carrierId=" + $(".WrhsRecvDetail-div-click.selected").attr("mgmtentity-id")
               + getAntiForgeryToken(),
               url: baseUrl + baseUrl + "/hasStaged",
               async: false
            });

            if (trckId.length > 0)
            {
               enableActionbarControl("#CMD_REFRESH", false);
               enableActionbarControl("#CMD_FILTER", false);

               $("#Count").prop("readonly", "readonly");

               if (!requesting.responseJSON.Success)
               {
                  $("#PackageName,#Carrier,#Date").removeAttr("disabled readonly");
               }
               else
               {
                  $("#PackageName,#Carrier,#Date").attr("disabled", "disabled");
               }

               $("#Notes").removeAttr("disabled readonly");
               addDataListOptions("carrierList", window.GetIgnoreCaseSpaceTxt($("#Carrier").val()), keyPressVal);
            }

            carrierId = $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value");
            $("#Carrier").attr("data-field-value", carrierId);
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Save Role detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexSave = function ()
   {
      if (window.getEditMode())
      {
         switch (active)
         {
            // ...
            //=============================================================================
            case TAB_SUMMARY:
               {
                  if (!$(".formMax input").hasClass("inputError") && !$(".formMax select").hasClass("inputError"))
                  {
                     if (!getFormChanged())
                     {
                        $(this).closeMessageBox();
                        OnQnexCancel();
                        return false;
                     }
                     else
                     {
                        if ($(this).messageBoxStatus())
                        {
                           $(this).closeMessageBox();
                        }

                        $("#wait").show();

                        if (getFormChanged() && !$(".formBody").filter(":input").hasClass("inputError"))
                        {
                           var Date = $("#Date").val();

                           if (Date.length === 0 || !validatedate(Date))
                           {
                              setColorAndClass("#FFFFC0CB", "#Date", "inputError");
                              enableActionbarControl("#CMD_SAVE", false);

                              if (window.getSelectedActionTab() === TAB_TRACKING)
                              {
                                 $("#tab-container a[href='#tabs-summary']").closest("a").click();
                              }

                              $("#Date").select();
                              $("#wait").hide();

                              return false;
                           }

                           var carrierName = $(".carrierCodeField")[0].value;
                           var carrierCode = $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name") === undefined
                              ? $("#Carrier").attr("data-field-value")
                              : $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name");

                           if ((carrierCode === undefined || carrierCode.length === 0))
                           {
                              setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
                              enableActionbarControl("#CMD_SAVE", false);

                              if (window.getSelectedActionTab() === TAB_TRACKING)
                              {
                                 $("#tab-container a[href='#tabs-summary']").closest("a").click();
                              }

                              $("#Carrier").select();
                              $("#wait").hide();

                              return false;
                           }

                           if ($("#Carrier").val().length === 0)
                           {
                              setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
                              enableActionbarControl("#CMD_SAVE", false);

                              if (window.getSelectedActionTab() === TAB_TRACKING)
                              {
                                 $("#tab-container a[href='#tabs-summary']").closest("a").click();
                              }

                              $("#Carrier").select();
                              $("#wait").hide();

                              return false;
                           }

                           if (trckId !== undefined)
                           {
                              var rowInf = JSON.parse($(".WrhsRecvDetail-div-click.selected").attr("mgmtentity-data").split("=")[1]);
                              rowInf.Date = $("#Date").val();
                              rowInf.CarrierID = carrierCode;
                              rowInf.Carrier = carrierName;
                              rowInf.PackageCode = "UNKN";
                              $(".WrhsRecvDetail-div-click.selected td[exchange='PackageCode']").attr("exchange-value", rowInf.PackageCode);
                              rowInf.Count = $("#Count").html();
                              rowInf.Notes = $("#Notes").val();

                              $(".WrhsRecvDetail-div-click.selected").attr("mgmtentity-data", "entity=" + JSON.stringify(rowInf));
                              var isDelete = false;
                              rowInf.Dates = oldDate;
                              rowInf.ReceivingDate = "";

                              $.ajax({
                                 type: "POST",
                                 url: baseUrl + baseUrl + "/SaveReceivingDetail",
                                 data: "carrierCode=" + carrierId + "&bodyData=" + JSON.stringify(rowInf) + "&isDeleted=" + isDelete + getAntiForgeryToken(),
                                 datatype: "JSON",
                                 async: true,
                                 success: function (data)
                                 {
                                    if (data.Success)
                                    {
                                       ntChnge = false;
                                       editMode = false;
                                       setEditMode(false);

                                       // Update the exchange values and the mgmt entity ID upon save
                                       $(".WrhsRecvDetail-div-click.selected").closest("tr").attr("mgmtentity-id", carrierCode);
                                       $(".WrhsRecvDetail-div-click.selected td[exchange='CarrierID']").closest("td").attr("exchange-value", carrierCode);
                                       $(".WrhsRecvDetail-div-click.selected td[exchange='Carrier']").closest("td").attr("exchange-value", carrierName);

                                       //reorder is required if the date or the carrier on the receipt was changed
                                       if (oldDate !== data.Date || carrierId !== carrierCode)
                                       {
                                          reorderSummary(false);
                                          $("#searchGrid").DataTable().draw();
                                          summaryDt = $("#searchGrid").DataTable();
                                       }

                                       $("#wait").hide();
                                       oldDate = "";
                                       carrierId = "";
                                       removeColorAndClass("#F2F2F2", "#Date", "inputError");
                                       $("#Carrier").removeAttr("style");
                                       $(".formBody .selectEx").removeAttr("required");
                                       $(".formMax .inputRex,.selectEx").attr("disabled", "disabled");

                                       enableActionbarControl("#CMD_REFRESH", true);

                                       if (window.getSelectedActionTab() === TAB_TRACKING)
                                       {
                                          trackingTab();
                                          enableActionbarControl("#CMD_FILTER", false);
                                       }
                                       else
                                       {
                                          enableActionbarControl("#CMD_FILTER", true);
                                       }
                                    }
                                    else
                                    {
                                       $("#wait").hide();

                                       messageObject = [
                                          {
                                             "id": "lbl_messages",
                                             "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on receiving save request.</h3>"
                                          }
                                       ];

                                       btnObjects = [
                                          {
                                             "id": "btn_ok_error",
                                             "name": "Ok",
                                             "class": "btnErrorMessage",
                                             "style": "margin:0 auto;text-align:center",
                                             "function": "onclick='$(this).closeMessageBox();'"
                                          }
                                       ];

                                       titleobjects = [
                                          {
                                             "title": "Invalid Entry"
                                          }
                                       ];

                                       $(this).addMessageButton(btnObjects, messageObject);
                                       $(this).showMessageBox(titleobjects);
                                    }
                                 },
                                 error: function (data)
                                 {
                                    $("#wait").hide();

                                    messageObject = [
                                       {
                                          "id": "lbl_messages",
                                          "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on receiving save request.</h3>"
                                       }
                                    ];

                                    btnObjects = [
                                       {
                                          "id": "btn_ok_error",
                                          "name": "Ok",
                                          "class": "btnErrorMessage",
                                          "style": "margin:0 auto;text-align:center",
                                          "function": "onclick='$(this).closeMessageBox();'"
                                       }
                                    ];

                                    titleobjects = [
                                       {
                                          "title": "Invalid Entry"
                                       }
                                    ];

                                    $(this).addMessageButton(btnObjects, messageObject);
                                    $(this).showMessageBox(titleobjects);
                                 }
                              });
                           }
                        }

                        return false;
                     }
                  }
                  else if (window.getSelectedActionTab() === TAB_TRACKING && editMode)
                  {
                     $("#tab-container a[href='#tabs-summary']").closest("a").click();
                  }

                  break;
               }

            // ...
            //=============================================================================
            case TAB_TRACKING:
               {
                  if (!checkTrackingNumberChange())
                  {
                     clickSubmit();
                  }

                  break;
               }
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexCancel = function ()
   {
      if (getEditMode())
      {
         if (getFormChanged() || txtChange)
         {
            messageObject = [{
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
            }];

            btnObjects = [{
               "id": "btn_cancel_saving",
               "name": "Yes",
               "class": "btnErrorMessage",
               "style": "margin:0 10px;text-align:center;width:70px"
            }, {
               "id": "btn_no",
               "name": "No",
               "class": "btnErrorMessage",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "onclick='$(this).closeMessageBox();'"
            }
            ];

            titleobjects = [{
               "title": "Confirm Cancellation"
            }];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
         else
         {
            txtChange = false;
            carrierId = "";
            trackingID = 0;
            oldDate = "";

            if (!editMode && active === TAB_SUMMARY && window.getSelectedActionTab() === TAB_TRACKING)
            {
               $("#tab-container a[href='#tabs-summary']").closest("a").click();
            }

            editMode = false;

            var carrierCd = $("#searchGrid tr.selected").attr("MgmtEntity-id");
            var date = $("#Date").val();

            document.getElementById("carrierList").innerHTML = "";

            $(".formBody").find("input[type=Text]").val("");

            clearTextBoxes();
            $(".formMax .inputRex,.selectEx").removeClass("inputError");
            setEditMode(false);

            $(".formMax .inputRex,.selectEx").attr("disabled", "disabled");

            //if we're cancelling a new entry, remove the newly-added row
            //if we're cancelling an edit, clear the form and re-populate it as if it was selected again
            if (carrierCd.length === 0 || carrierCd === undefined || carrierCd === "0")
            {
               //delete the row from Data Table i.e. cannot filter on the row via search input
               var $deleteRow = $("#searchGrid tr[MgmtEntity-id=0]").closest("tr");
               $("#searchGrid").DataTable().row($deleteRow).remove();

               if ($(".WrhsRecvDetail-div-click").length === 0 && hiddenTrNothing !== "")
               {
                  $("#searchGrid tbody").append(hiddenTrNothing);
               }

               $("#searchGrid").DataTable().draw(); //re-draw the table onto the screen
               $("#searchGrid tr:eq(1)").trigger("click");
            }
            else
            {
               $("#searchGrid tr.selected").removeClass("selected");
               $("#searchGrid tr[MgmtEntity-id=" + carrierCd + "]").closest("tr").find("td[exchange='Date'][exchange-value='" + date + "']").closest("tr").trigger("click");
            }

            enableActionbarControl("#CMD_REFRESH", true);

            active = active === TAB_TRACKING
               ? ("", $("#ID_TRKNO,#ID_RA").val(""), $("#ID_TRKNO").focus().keyup(), $("#ID_RA").removeClass("inputError"), $("#ID_RA").removeAttr("style").css("width", "95%"), $("#searchGrid_WrhsRcvDetails tr").removeClass("selected"))
               : "";

            if (window.getSelectedActionTab() === TAB_TRACKING)
            {
               enableActionbarControl("#CMD_NEW", false);
               enableActionbarControl("#CMD_EDIT", false);
               enableActionbarControl("#CMD_FILTER", false);

               $("#ID_SUBMIT_VALUE").attr("disabled", "disabled");
            }
            else
            {
               enableActionbarControl("#CMD_FILTER", true);
            }

            trackingTab();
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexPrint = function ()
   {
      console.log("print");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Enable/Disable the filter function
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexFilter = function ()
   {
      $(".filterFrame .FilterRcvdFromDate").datepicker();
      $(".filterFrame .FilterRcvdFromDate").val($(".ql-body .FilterRcvdFromDate").val());
      $(".filterFrame .FilterRcvdToDate").datepicker();
      $(".filterFrame .FilterRcvdToDate").val($(".ql-body .FilterRcvdToDate").val());

      //each time we open up the filter, the background color resets, which means the user doesn't know if there's an error on the input fields
      if ($(".filterFrame .FilterRcvdFromDate").hasClass("inputError"))
      {
         $(".filterFrame .FilterRcvdFromDate").removeAttr("style").removeClass("inputError");
      }

      if ($(".filterFrame .FilterRcvdToDate").hasClass("inputError"))
      {
         $(".filterFrame .FilterRcvdToDate").removeAttr("style").removeClass("inputError");
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_cancel_saving", function ()
   {
      oldDate = "";
      document.getElementById("carrierList").innerHTML = "";
      setEditMode(false);
      lockedtrckNo = false;
      $(this).closeMessageBox();
      trackingID = 0;

      if (getEditMode())
      {
         return false;
      }

      txtChange = false;
      var carrierCode = $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value") === undefined
         ? $("#Carrier").attr("data-field-value")
         : $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value");

      console.log(carrierCode);

      var dt = $("#searchGrid tr.selected td[exchange='DateHidden']").closest("td").attr("exchange-value") === undefined
         ? $("#Date").val()
         : $("#searchGrid tr.selected td[exchange='DateHidden']").closest("td").attr("exchange-value");

      clearTextBoxes();

      $(".formMax .inputRex,.selectEx").removeClass("inputError").prop("disabled", "disabled");
      $("#Carrier").prop("readonly", "readonly");
      carrierId = "";

      enableActionbarControl("#CMD_REFRESH", true);

      //delete the row from Data Table i.e. cannot filter on the row via search input
      var $deleteRow = $("#searchGrid tr[MgmtEntity-id=0]").closest("tr");
      $("#searchGrid").DataTable().row($deleteRow).remove();

      if ($(".WrhsRecvDetail-div-click").length === 0 && hiddenTrNothing !== "")
      {
         $("#searchGrid tbody").append(hiddenTrNothing);
      }

      $("#searchGrid").DataTable().draw(); //re-draw the table onto the screen

      if (!$(".WrhsRecvDetail-div-click").hasClass("selected"))
      {
         $(".WrhsRecvDetail-div-click:first").trigger("click");
      }
      else
      {
         $(".WrhsRecvDetail-div-click.selected").removeClass("selected");
         $(".WrhsRecvDetail-div-click td[exchange='Date'][exchange-value='" + dt + "']").closest("tr").find("td[exchange='CarrierID'][exchange-value='" + carrierCode + "']").closest("tr").trigger("click");
      }

      if (active === TAB_SUMMARY && !editMode)
      {
         $("#tab-container a[href='#tabs-summary']").closest("a").click();
      }
      else if (window.getSelectedActionTab() === TAB_SUMMARY && editMode)
      {
         enableActionbarControl("#CMD_FILTER", true);
      }
      else if (window.getSelectedActionTab() === TAB_TRACKING && editMode)
      {
         unselectedTrackingGrid();
         editMode = false;
         active = "";

         enableActionbarControl("#CMD_NEW", false);
         enableActionbarControl("#CMD_EDIT", false);

         setGrapicState("#IDENT_FIRST_ARROW", false);
         $("#ID_SUBMIT_VALUE").attr("disabled", "disabled");
      }

      editMode = false;
      active = "";
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshReceiptsGrid(carrierCode, date, pckgCode, tracking)
   {
      var url = "ViewDataRequest";
      $.ajax({
         type: "POST",
         url: url,
         data: "criteria=" + $(".ql-body.active .formExDivFilter").serialize() + getAntiForgeryToken(),
         async: false,
         success: function (data)
         {
            var $target = $("#DockReceivingGridSummary");
            var $newHtml = $(data);

            $target.replaceWith($newHtml);
            table = window.dtHeaders("#searchGrid", "ordering: false", ["sorting"], []);

            if (!table.$("tr.selected").hasClass("selected"))
            {
               if (carrierCode.length > 0 && date.length > 0 && pckgCode.length > 0)
               {
                  if ($newHtml.find("#searchGrid tr td[exchange='CarrierID'][exchange-value='" + carrierCode + "']")
                     .closest("tr").find("td[exchange='Date'][exchange-value='" + date + "']")
                     .closest("tr").find("td[exchange='PackageCode'][exchange-value='" + pckgCode + "']")
                     .closest("tr").length > 0)
                  {
                     $newHtml.find("#searchGrid tr td[exchange='CarrierID'][exchange-value='" + carrierCode + "']")
                        .closest("tr").find("td[exchange='Date'][exchange-value='" + date + "']")
                        .closest("tr").find("td[exchange='PackageCode'][exchange-value='" + pckgCode + "']")
                        .closest("tr").click();
                  }
                  else
                  {
                     $newHtml.find("#searchGrid tbody").find("tr:eq(0)").click();

                     if (window.getSelectedActionTab() !== TAB_SUMMARY && !tracking)
                     {
                        lockedtrckNo = false;
                        $("#tab-container a[href='#tabs-summary']").closest("a").click();
                     }
                  }
               }
               else
               {
                  $newHtml.find("#searchGrid tbody").find("tr:eq(0)").click();

                  if (window.getSelectedActionTab() !== TAB_SUMMARY && !tracking)
                  {
                     lockedtrckNo = false;
                     $("#tab-container a[href='#tabs-summary']").closest("a").click();
                  }
               }
            }
            $("#searchGrid th.sorting").removeClass("sorting");
         }
      });

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clear Textboxes in the Dock Receipt form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clearTextBoxes()
   {
      $(".formMax .inputRex,.formMax .textareaRex").val("");
      $(".formMax .selectEx").val("0");
      $(".formMax .inputRex,.selectEx").not("#Count,#Notes").removeAttr("style");
      $("#Date").attr("style", "text-align:center");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   When this gets called, we exchange data from the selected row, and we place the appropriate content into each field
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.UpdateData = function (saveAndvalidate, field, data)
   {
      //console.log(saveAndvalidate + ":" + field + ":" + data);
      //console.log($("#" + field).prop("tagName") + "::" + field);

      switch ($("#" + field).prop("tagName"))
      {
         case "INPUT":
            {
               switch ($("#" + field).attr("type"))
               {
                  case "text":
                     if (data !== "null")
                     {
                        if (field === "CarrierID")
                        {
                           $("#" + field).attr("data-field-value", data);
                        }
                        lockedtrckNo = false;
                        $("#" + field).val(data);
                        updateDataTable(data, field, $(".WrhsRecvDetail-div-click.selected"));
                     }

                  //console.log("TEXT::" + field);
               }

               break;
            }

         case "SELECT":
            {
               //console.log("SELECT::" + field + "::" + data);
               if (field === "CarrierID")
               {
                  $("#" + field).attr("data-field-value", data);
               }

               if (field === "PackageName")
               {
                  $("#" + field).attr("data-field-value", $("#" + field + " option[value='" + data + "']").closest("option").attr("id"));
               }

               $("#" + field).val(data);
               updateDataTable(data, field, $(".WrhsRecvDetail-div-click.selected"));

               break;
            }

         case "TEXTAREA":
            {
               $("#" + field).val(data);
               break;
            }

         case "TD":
            {
               $("#" + field).html(data);
               updateDataTable(data, field, $(".WrhsRecvDetail-div-click.selected"));
               break;
            }
      }

      //reset route section in Identification segment
      $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
      $("#ID_ROUTEVALUE span").html("--");

      lockedtrckNo = false;

      var attSrc = $("#ID_Lock_TrackingNumber").attr("src");
      var $newSrc = "";

      if (attSrc.indexOf("(OFF)") <= 0)
      {
         $newSrc = attSrc.replace("(ON)", "(OFF)");
         $("#ID_Lock_TrackingNumber").attr("src", $newSrc);
      }

      $("#ID_TRKNO,#ID_RA").val("");

      if ($(".searchGrid_WrhsRcvDetails-div-click").length > 0)
      {
         setCollapseRow();
      }

      printTrackingId = "";
      printTrackingNumber = "";
      printRaNumber = "";
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", ".inputRex", function ()
   {
      var id = $(this).attr("id");
      var value = $(this).val();

      if (id === "Carrier")
      {
         var carrierRd = "";
         var carrierName = "";
         var i = 0;
         var strData = window.GetIgnoreCaseSpaceTxt(value);
         if ($("#carrierList option").length === 1)
         {
            $("#carrierList option").each(function ()
            {
               if (CompareTextboxes($(this).val(), strData))
               {
                  carrierRd = $(this).attr("data-field-name");
                  carrierName = $(this).val();
               }
            });

            if (carrierRd !== "")
            {
               $('#carrierList option[data-field-name="' + carrierRd + '"]').closest("option").click();
               $(this).attr("name", carrierRd);
               removeColorAndClass("#ECDD9D", "#Carrier", "inputError");

               if (!$(".formMax .inputRex,selectEx").hasClass("inputError") && editMode)
               {
                  enableActionbarControl("#CMD_SAVE", true);
               }
            }
            else
            {
               setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
               updateDataTable(carrierRd, "CarrierID", trckId);
               updateDataTable(carrierName, "Carrier", trckId);
               addDataListOptions("carrierList", window.GetIgnoreCaseSpaceTxt($(this).val()), keyPressVal);
            }

            if (carrierName !== "")
            {
               $("#Carrier").val(carrierName);
            }

            $("#Carrier").attr("data-field-value", carrierRd);
         }
         else
         {
            addDataListOptions("carrierList", window.GetIgnoreCaseSpaceTxt($(this).val()), keyPressVal);
         }
      }
      else if (id === "ID_TRKNO" || id === "ID_RA" || id === "ID_WPT")
      {
         if (!window.getFormChanged())
         {
            window.setFormChanged(true);
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function validatedate(inputText)
   {
      if (inputText === undefined)
      {
         return false;
      }

      var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

      // Match the date format through regular expression  
      if (inputText.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/))
      {
         var today = new Date();

         // Test which separator is used '/' or '-'
         var opera1 = inputText.split('/');
         var opera2 = inputText.split('-');
         lopera1 = opera1.length;
         lopera2 = opera2.length;
         var pdate = "";

         // Extract the string into month, date and year  
         if (lopera1 > 1)
         {
            pdate = inputText.split('/');
         }
         else if (lopera2 > 1)
         {
            pdate = inputText.split('-');
         }

         var dd = parseInt(pdate[1]);
         var mm = parseInt(pdate[0]);
         var yy = parseInt(pdate[2]);
         // Create list of days of a month [assume there is no leap year by default]  
         var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

         if (mm === 1 || mm > 2)
         {
            if (dd > ListofDays[mm - 1] || ListofDays[mm - 1] === undefined)
            {
               return false;
            }
         }

         if (mm === 2)
         {
            var lyear = false;

            if ((!(yy % 4) && yy % 100) || !(yy % 400))
            {
               lyear = true;
            }

            if (!lyear && (dd >= 29))
            {
               return false;
            }

            if (lyear && (dd > 29))
            {
               return false;
            }
         }

         var inputDate = new Date(inputText);

         if (inputDate > today)
         {
            return false;
         }

         return true;
      }
      else
      {
         return false;
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_dontSaveReceivingDetails", function ()
   {
      $(this).closeMessageBox();
      carrierId = "";
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get All FormBody data to the string list for controller input.
   //*
   //* Parameters:
   //*   groupCode   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function getBodyData(crrCode)
   {
      var bodyData = null;

      $(".formMax *").filter(":input").each(function ()
      {
         if ($(this).attr("id") === "Carrier")
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + crrCode + '",';
         }
         else
         {
            if ($(this).attr("id") === "PackageName")
            {
               bodyData += '"' + $(this).attr("id") + '" : "' + $("#PackageName option[value='" + $("#PackageName").val() + "']").closest("option").attr("id") + '",';
            }
            else
            {
               bodyData += '"' + $(this).attr("id") + '" : "' + $(this).val() + '",';
            }
         }
      });

      return bodyData;
   };

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_saveReceivingDetails", function ()
   {
      $(this).closeMessageBox();

      if ($("#Carrier").val().length === 0)
      {
         setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
         enableActionbarControl("#CMD_SAVE", false);

         return false;
      }

      var carrierName = $(".carrierCodeField")[0].value;
      var carrierCode = $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name") === undefined
         ? $("#Carrier").attr("data-field-value")
         : $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name");

      if ((carrierCode === undefined || carrierCode.length === 0))
      {
         setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
         enableActionbarControl("#CMD_SAVE", false);

         return false;
      }

      currentTrckNo = "";

      if (trckId !== undefined)
      {
         var bodyData = getBodyData(carrierCode) + 'oldDate: "' + oldDate + '"';
         var isDelete = false;

         $.ajax({
            type: "POST",
            url: baseUrl + baseUrl + "/SaveReceivingDetail",
            data: "carrierCode=" + carrierId + "&bodyData=" + bodyData + "&isDeleted=" +
            isDelete + getAntiForgeryToken(),
            datatype: "JSON",
            success: function (data)
            {
               if (data.Success)
               {
                  editMode = false;
                  oldDate = "";
                  var dates = data.Date;
                  var pckCode = "Corrugated Box"; //$("#PackageName").attr("data-field-value");
                  carrierId = "";
                  setEditMode(false);
                  $(".formBody .selectEx").removeAttr("required");
                  clearTextBoxes();
                  refreshReceiptsGrid(data.CarrierId, dates, pckCode, false);

                  enableActionbarControl("#CMD_REFRESH", true);
                  enableActionbarControl("#CMD_FILTER", true);

                  $(".formBody .inputRex,.selectEx").removeClass("editable").addClass("editable");

                  if (window.getSelectedActionTab() === TAB_TRACKING)
                  {
                     trackingTab();
                  }
               }
            },
            error: function (data)
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call fails on save receiving request.</h3>"
               }];

               btnObjects = [{
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }];

               titleobjects = [{
                  "title": "Invalid Entry"
               }];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_Discard_Save", function ()
   {
      if (!getEditMode())
      {
         return false;
      }

      active = "";
      cancel = true;

      $(this).closeMessageBox();
      $(".formBody").find("input[type=Text]").val("");

      //if ($("#searchGrid tbody").find("tr:eq(0)").hasClass("selected")
      //      && !getEditMode())
      //{
      //   $("#searchGrid tbody").find("tr:eq(1)").remove();
      //}

      var carrierCode = $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value");
      var dt = $("#searchGrid tr.selected td[exchange='DateHidden']").closest("td").attr("exchange-value");
      var pckCode = "Corrugated Box"; //$("#searchGrid tr.selected td[exchange='PackageCode']").closest("td").attr("exchange-value");

      clearTextBoxes();

      $(".formMax .inputRex,.selectEx").removeClass("inputError");

      setEditMode(false);

      refreshReceiptsGrid(carrierCode, dt, pckCode, false);

      enableActionbarControl("#CMD_REFRESH", true);
      enableActionbarControl("#CMD_FILTER", true);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   search profile name by input.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function addDataListOptions(dataListId, strData, keyPress)
   {
      document.getElementById(dataListId).innerHTML = "";
      if (strData !== "")
      {
         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "strData=" + strData + "&keyPressValue=" + keyPress + "&browser=" + navigator.userAgent + getAntiForgeryToken(),
            url: baseUrl + baseUrl + "/GetCarrierName",
            async: false,
            success: function (jsonData)
            {
               if (jsonData.keyPressValue !== keyPressVal)
               {
                  return false;
               }

               var itemLists = [];
               var options = '';

               for (var jsIndx = 0; jsIndx < jsonData.profileEntity.length; jsIndx++)
               {
                  if (itemLists.length === 0)
                  {
                     options += '<option data-field-name="' + jsonData.profileEntity[jsIndx].CarrierID +
                        '" value="' + jsonData.profileEntity[jsIndx].Carrier + '"> ' + jsonData.profileEntity[jsIndx].Carrier + '</option>';
                  }
                  else
                  {
                     var bln = false;

                     for (var ind = 0; ind < itemLists.length; ind++)
                     {
                        if (String(jsonData.profileEntity[jsIndx].CarrierID) === itemLists[ind].CarrierID)
                        {
                           bln = true;
                        }
                     }

                     if (bln === false)
                     {
                        options += '<option data-field-name="' + jsonData.profileEntity[jsIndx].CarrierID +
                           '" value="' + jsonData.profileEntity[jsIndx].Carrier + '"> ' + jsonData.profileEntity[jsIndx].Carrier + '</option>';
                     }
                  }
               }

               document.getElementById(dataListId).innerHTML = options;

            }
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set data from input  to Grid.
   //*
   //* Parameters:
   //*   inValue - Description not available.
   //*   inField - Description not available.
   //*   gpId    - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function updateDataTable(inValue, inField, recvId)
   {
      var row = $("#searchGrid tbody").find("tr[MgmtEntity-id=0]").closest("tr");

      if (recvId !== 0)
      {
         row = $("#searchGrid tbody tr.selected").closest("tr");
      }

      if (inField === "Date")
      {
         row.find("td:eq(1) span").html(inValue);
      }

      else if (inField === "Carrier")
      {
         row.find("td:eq(2)").html(inValue);
      }

      else if (inField === "Count")
      {
         row.find("td:eq(3)").html(inValue);
      }

      else if (inField === "PackageName")
      {
         row.find("td:eq(4)").html(inValue);
      }

      else if (inField === "CarrierID")
      {
         row.find("td:eq(6)").html(inValue);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   click event on a row in the Summary tab grid
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".WrhsRecvDetail-div-click", function ()
   {
      //if we're in edit mode, prevent the click event from firing
      if (!getEditMode())
      {
         summaryDt.$("tr.selected").removeClass("selected");
         $(this).addClass("selected");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Double-click event on a row in the Summary tab grid
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("dblclick", ".WrhsRecvDetail-div-click", function ()
   {
      //if we're in edit mode, prevent the double click event from firing
      if (!getEditMode())
      {
         $("#tab-container a[href='#tabs-tracking']").closest("a").click();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Tracking Tab Grid click event
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".searchGrid_WrhsRcvDetails-div-click", function (e)
   {
      if (getEditMode() || $(e.target).prop("tagName") === "IMG")
      {
         return false;
      }

      if ($(this).hasClass("selected"))
      {
         unselectedTrackingGrid();
      }
      else
      {
         $("#WrhsRecvTracking input[type=Text]").val("").removeClass("inputError").css("background-color", "#F3F3F3");
         trackingDt.$("tr.selected").removeClass("selected");
         $(this).addClass("selected");
         removeColorAndClass("#F3F3F3", "#ID_RA", "inputError");

         $("#WrhsRecvTracking input[type=Text]").prop("readonly", "readonly").prop("disabled", "disabled");
         $("#WrhsRecvTracking #ID_SUBMIT_VALUE, #ID_NO_RA, #btn_not_processing").off("click");
         $("#ID_ROUTEVALUE span").html($("#searchGrid_WrhsRcvDetails tr.selected td[exchange='Route'] div").html());
         $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay").addClass($("#searchGrid_WrhsRcvDetails tr.selected td[exchange='Route'] div").attr("class"));
         $("#IDENT_CMD_PRINT").on("click");

         disabled = true;

         // Reset the tracking work flow arrows
         $("#ID_SUBMIT_VALUE").attr("disabled", "disabled");
         setGrapicState("#IDENT_FIRST_ARROW", false);

         var attSrc = $("#ID_Lock_TrackingNumber").attr("src");
         var $newSrc = "";

         if (attSrc.indexOf("(OFF)") <= 0)
         {
            $newSrc = attSrc.replace("(ON)", "(OFF)");
            $("#ID_TRKNO").removeAttr("readonly");
            $("#ID_TRKNO").focus().keyup();
            $("#ID_Lock_TrackingNumber").attr("src", $newSrc);
            lockedtrckNo = false;
         }

         if ($(this).find("td[exchange='StagingDate']").attr("exchange-value") === "1")
         {
            enableActionbarControl("#CMD_EDIT", false);
            enableActionbarControl("#CMD_NEW", true);
         }
         else
         {
            enableActionbarControl("#CMD_EDIT", true);
            enableActionbarControl("#CMD_NEW", true);
         }

         $(this).closest("tr").find("td").each(function ()
         {
            if ($(this).attr("exchange") === "TrackingNumber")
            {
               setData("#ID_TRKNO", $(this).attr("exchange-value"));
            }
            else if ($(this).attr("exchange") === "RA")
            {
               if ($("#searchGrid_WrhsRcvDetails tr.selected td[exchange='TypeCode']").attr("exchange-value") === "RA")
               {
                  setData("#ID_RA", $(this).attr("exchange-value"));
               }
               else if ($("#searchGrid_WrhsRcvDetails tr.selected td[exchange='TypeCode']").attr("exchange-value") === "WPT")
               {
                  setData("#ID_WPT", $(this).attr("exchange-value"));
               }
            }
            else if ($(this).attr("exchange") === "TrackingID")
            {
               trackingID = $(this).attr("exchange-value");
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   In the Tracking tab, click on the Tracking table row "Click here to expand list" to load the RAs tied to the tracking record
   //*
   //* Parameters:
   //*   e                - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#searchGrid_WrhsRcvDetails tr.odd:not(.searchGrid_WrhsRcvDetails-div-click)", function (e)
   {
      getTrackingDetails();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Loading Tracking Grid.
   //*
   //* Parameters:
   //*   None - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function getTrackingDetails()
   {
      $("#wait").show();

      if ($("#searchGrid_WrhsRcvDetails tr:first").closest("tr").html().indexOf("available") > 0)
      {
         $("#wait").hide();
         return false;
      }

      if (getEditMode() && !editMode)
      {
         //New
         $(".formRex .inputRex,.selectEx").attr("readonly", "readonly");
         $(".formRex .inputRex,.selectEx").attr("disabled", "disabled");

         if ($(".formRex .inputRex").hasClass("inputError"))
         {
            $(".formRex input.inputError").removeAttr("readonly").removeAttr("disabled");
         }
         else
         {
            $(".formRex .inputRex,.selectEx").not("#Count,#Notes,#Date").removeAttr("style");
         }
      }

      if (editMode && (trackingID !== 0 && trackingID !== undefined))
      {
         $("#wait").hide();
         return false;
      }

      var carrierName = $(".carrierCodeField")[0].value;
      var carrierCode = carrierId === "" ? $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name") : carrierId;
      var date = oldDate === ""
         ? $("#Date").val() === ""
            ? $("#searchGrid tr.selected td[exchange='Date']").closest("td").attr("exchange-value")
            : $("#Date").val()
         : oldDate;

      if (carrierCode === undefined || carrierCode === "" || carrierCode === "0")
      {
         carrierCode = $("#searchGrid tr.selected td[exchange='CarrierID']")
            .closest("td").attr("exchange-value") === undefined
            ? ""
            : $("#searchGrid tr.selected td[exchange='CarrierID']")
               .closest("td").attr("exchange-value");
      }

      if (!validatedate(date) && getEditMode())
      {
         setColorAndClass("rgb(243, 104, 104)", "#Date", "inputError");
         $("#Date").removeAttr("disabled").removeAttr("readonly");
         $("#Date").focus();
      }

      if ((carrierCode === "" || carrierCode === "0") && getEditMode())
      {
         $("#Carrier").removeAttr("disabled").removeAttr("readonly")
         setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
         $("#Carrier").focus();
      }

      if ((carrierCode === "" || carrierCode === undefined || carrierCode === "0")
         || (date === "" || date === undefined || !validatedate(date)))
      {
         setCollapseRow();
         $("#ID_TRKNO,#ID_RA").attr("readonly", "readonly");
         disabled = true;
         $("#wait").hide();
         return false;
      }

      $("#searchGrid_WrhsRcvDetails tr.odd").find("td").html("Please wait while data is loading ...");

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "carrierCode=" + carrierCode + "&date=" + (date === undefined ? "" : date) + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/WrhsRecvDetails",
         async: true,
         success: function (data)
         {
            $("#WrhsRcvDetailsGrid").replaceWith($(data));
            $("#searchGrid_WrhsRcvDetails_filter .dataTables_scroll table.dataTable thead tr th").removeClass("sorting");
            $("#WrhsRcvDetailsGrid .dataTables_scrollBody").addClass("QnexScrollbar");
            trackingDt = $("#searchGrid_WrhsRcvDetails").DataTable();
            $("#wait").hide();
         },
         error: function (data)
         {
            $("#wait").hide();
         }

      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set data from grid to inputs.
   //*
   //* Parameters:
   //*   inputId - Description not available.
   //*   value   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function setData(inputId, value)
   {
      $(inputId).val(value);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Tab click event - switching between tabs
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#tab-container a", function (e)
   {

      if (window.getSelectedActionTab() === TAB_TRACKING)
      {
         enableActionbarControl("#CMD_FILTER", false);

         if (!getEditMode())
         {
            enableActionbarControl("#CMD_NEW", false);
            enableActionbarControl("#CMD_EDIT", false);
         }
         else
         {
            if (editMode)
            {
               if (active === TAB_SUMMARY)
               {
                  OnQnexSave();
               }

               return false;
            }
         }

         // on adding a new receiving form, keep user in Summary tab if there are errors in the form (carrier, date)
         if (!$(".formMax input").hasClass("inputError") && !$(".formMax select").hasClass("inputError") && $("#Carrier").val() !== "")
         {
            trackingTab();
         }
         else
         {
            $("#tab-container a[href='#tabs-summary']").closest("a").click();
         }
      }
      else if (window.getSelectedActionTab() === TAB_SUMMARY)
      {
         if (getEditMode())
         {
            if (active === TAB_TRACKING)
            {
               $("#tab-container a[href='#tabs-tracking']").closest("a").click();
            }
            else if ($(".searchGrid_WrhsRcvDetails-div-click").length === 0 || $("#searchGrid tr.selected").attr("MgmtEntity-id") === "0")
            {
               $("#PackageName,#Notes,#Carrier,#Date").removeAttr("disabled readonly");
            }
         }
         else
         {
            enableActionbarControl("#CMD_NEW", true);
            enableActionbarControl("#CMD_EDIT", true);
            enableActionbarControl("#CMD_FILTER", true);

            $lastSummaryFilter = $(".ql-body.active #DockReceivingGridSummary .formExDivFilter").html();
            $(".filterFrame .formExDiv").html($(".ql-body.active div.formExDivFilter").html());
         }
      }
      else
      {
         $("#dataTables_scrollHeadInner").css("width", "100%");
         $("#dataTables_scrollHeadInner table").css("width", "100%");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on the Lock Tracking Number icon
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#ID_Lock_TrackingNumber", function ()
   {
      if (($("#ID_TRKNO").attr("disabled") !== undefined || $("#ID_TRKNO").attr("readonly") !== undefined || $("#ID_TRKNO").val() === "" || $("#ID_TRKNO").val() === undefined) &&
         ($("#ID_RA").attr("disabled") !== undefined || $("#ID_RA").attr("readonly") !== undefined))
      {
         return false;
      }

      var trkNoBarcode = parseBarcode($.trim($("#ID_TRKNO").val()), "ID_TRKNO"); //check tracking field for reserved chars

      if (trkNoBarcode !== undefined && !trkNoBarcode)
      {
         return false;
      }

      if (disabled)
      {
         return false;
      }

      removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");

      var attSrc = $(this).attr("src");
      var $newSrc = "";

      if (attSrc.indexOf("(OFF)") > 0)
      {
         if ($("#ID_TRKNO").val().length === 0)
         {
            return false;
         }

         $newSrc = attSrc.replace("(OFF)", "(ON)");
         $("#ID_TRKNO").prop("readonly", "readonly");
         removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
         removeColorAndClass("#F3F3F3", "#ID_TRKNO", "inputError");
         $("#ID_RA").focus().keyup();
         lockedtrckNo = true;
      }
      else
      {
         $newSrc = attSrc.replace("(ON)", "(OFF)");
         removeColorAndClass("#ECDD9D", "#ID_TRKNO", "inputError");
         removeColorAndClass("#F3F3F3", "#ID_RA", "inputError");
         $("#ID_TRKNO").removeAttr("readonly");
         $("#ID_TRKNO").select();
         $("#ID_TRKNO").focus().keyup();
         lockedtrckNo = false;
      }
      $(this).attr("src", $newSrc);

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on the No RA/WPT button in Tracking
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#ID_NO_RA", function ()
   {
      clickNoRa();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Keypress events on various fields, mainly looking for the special character and/or the enter key to set next focus
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".carrierCodeField, #Date, #Notes, #ID_TRKNO, #ID_RA", function (e)
   {
      if (e.keyCode === KEY_SPECIALCHAR)
      {
         return false;
      }
      else if (e.keyCode === 13 || e.keyCode === KEY_CtrlM) //force nextfocus on Enter; work-around for nextfocus from fields w/o change event triggered
      {
         if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
         {
            $("#" + $(this).attr("nextfocus")).focus();
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Paste events in the Tracking tab, which enables the various buttons on the page
   //*   Note that this is wrapped in a setTimeout since the paste event fires before the textbox receives the pasted value
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("paste", "#ID_TRKNO, #ID_RA", function (e)
   {
      setTimeout(function ()
      {
         $(e.target).focus().keyup();
      }, 150);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Keypress event for RA field
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#ID_RA", function (e)
   {
      removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
      $(this).val(parseBarcode($(this).val(), "SpecialChar")); //scannable via barcode scanner

      if (e.keyCode === 13 || e.keyCode === KEY_CtrlM)
      {
         if (($(this).val() !== "" && !entered) || editMode)
         {
            $("#ID_SUBMIT_VALUE").click();
         }
      }
      else if (e.keyCode === 32) //don't allow spaces
      {
         return false;
      }
      //   else if ($(e.target).attr("id") === "ID_WPT")
      //   {
      //      CheckValidation($(this).val(), "WPT", 0);
      //   }
      //}
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Enable/Disable buttons/fields when RA field is interacted with
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "#ID_RA", function (e)
   {
      removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
      disabled = false;

      if ($(this).val() !== "")
      {
         $("#ID_SUBMIT_VALUE").removeAttr("disabled");
      }
      else
      {
         if ($("#ID_TRKNO").val() === "")
         {
            $("#ID_TRKNO").focus().keyup();
         }
         $("#ID_SUBMIT_VALUE").attr("disabled", "disabled");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Tab/Enter out of Tracking Number field and into RA field; check for
   //*   reserved barcode chars.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", "#ID_TRKNO", function (e)
   {
      removeColorAndClass("#ECDD9D", "#ID_TRKNO", "inputError");
      $(this).val(parseBarcode($(this).val(), "SpecialChar")); //scannable via barcode scanner

      if (e.keyCode === 13 || e.keyCode === KEY_CtrlM)
      {
         parseBarcode($(this).val(), "ID_TRKNO");
      }
      else if (e.keyCode === 32) //don't allow spaces
      {
         return false;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Enable/Disable buttons in Identification segment when tracking number
   //*   field is interacted with.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "#ID_TRKNO", function (e)
   {
      if ($(this).val() !== "")
      {
         $("#btn_not_processing,#ID_NO_RA").removeAttr("disabled");
         $("#ID_RA").removeAttr("disabled").removeAttr("readonly");
         disabled = false;
         removeColorAndClass("#ECDD9D", "#ID_RA", "");
         setGrapicState("#IDENT_FIRST_ARROW", true);
      }
      else
      {
         $("#btn_not_processing,#ID_NO_RA").attr("disabled", "disabled");

         if ($("#ID_RA").val() === "")
         {
            $("#ID_RA").attr("disabled", "disabled").attr("readonly", "readonly");
            removeColorAndClass("#F3F3F3", "#ID_RA", "");
            setGrapicState("#ID_SUBMIT_VALUE", false);
            $("#ID_SUBMIT_VALUE").attr("disabled", "disabled");
         }

         setGrapicState("#IDENT_FIRST_ARROW", false);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Enable/Disable buttons in Identification segment when tracking number
   //*   field is interacted with.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", "#searchGrid_WrhsRcvDetails_filter input[type='search']", function (e)
   {
      //grab the refactored grid we're filtering on via the search input box
      var $filterGrid = $(".ql-body.active .QnexGrid");

      //clear all checkboxes to prevent any issues with deleting records
      $(".gridChkBox input").removeAttr("checked");

      // upon first key entry, apply class; remove class if empty
      if ($(this).val() !== undefined && $(this).val() !== "")
      {
         $filterGrid.find(".gridChkBox").each(function ()
         {
            //the "parents" of each tracking grouping already display either the img or the input checkbox, depending on if something for that tracking number has been staged
            //we want to show the staged img tag, but never the input checkbox tag
            if ($(this).closest("tr").attr("parent-id") !== "0" && $(this).closest("tr").attr("hasStaged") === "True")
            {
               $(this).find("img.TrckgStaged").removeAttr("hidden");
            }
         });
      }
      else
      {
         $filterGrid.find(".gridChkBox").each(function ()
         {
            if ($(this).closest("tr").attr("parent-id") !== "0" && $(this).closest("tr").attr("hasStaged") === "True")
            {
               $(this).find("img.TrckgStaged").attr("hidden", "hidden");
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange ProfileCode.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusout", ".carrierCodeField", function (e)
   {
      if (getEditMode() && getFormChanged())
      {
         if ($(e.target).attr("disabled") !== undefined || $(e.target).attr("readonly") !== undefined)
         {
            return false;
         }

         var carrierRd = "";
         var carrierName = "";
         var i = 0;
         var ij = 0;
         var carrierRdIj = "";
         var carrierNmIj = "";
         var strData = window.GetIgnoreCaseSpaceTxt($(e.target).val());
         strData = parseBarcode(strData, "SpecialChar");

         if ($("#carrierList option").length === 0)
         {
            addDataListOptions("carrierList", strData, keyPressVal++);
         }

         $("#carrierList option").each(function ()
         {
            if (CompareTextboxes(strData, $(this).val()))
            {
               carrierRd = $(this).attr("data-field-name");
               carrierName = $(this).val();
               i++;
            }

            if (CompareTextboxes($(this).val(), strData))
            {
               carrierRdIj = $(this).attr("data-field-name");
               carrierNmIj = $(this).val();
               ij++;
            }
         });

         if (carrierRd !== "" && (i === 1 || ij === 1))
         {
            $('#carrierList option[data-field-name="' + (ij === 1 ? carrierRdIj : carrierRd) + '"]').closest("option").click();
            $(this).attr("name", ij === 1 ? carrierRdIj : carrierRd);
            removeColorAndClass("#ECDD9D", "#Carrier", "inputError");
            if (!$(".formMax .inputRex,selectEx").hasClass("inputError") && editMode)
            {
               enableActionbarControl("#CMD_SAVE", true);
            }
         }
         else
         {
            carrierName = "";
            carrierRd = "0";
            setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
            enableActionbarControl("#CMD_SAVE", false);
            $(this).closest("tr").find("td input[name=CarrierID]").val('');
            $(this).val(strData);
         }

         updateDataTable(ij === 1 ? carrierRdIj : carrierRd
            , "CarrierID"
            , trckId);
         updateDataTable(ij === 1 ? carrierNmIj : carrierName
            , "Carrier"
            , trckId);

         if ((ij === 1 ? carrierNmIj : carrierName) !== "")
         {
            $("#Carrier").val(ij === 1 ? carrierNmIj : carrierName);
         }

         $("#Carrier").attr("data-field-value", ij === 1 ? carrierRdIj : carrierRd);
         i = 0;

         if (window.getSelectedActionTab() === TAB_TRACKING)
         {
            var carrierCd = "0";

            $("#carrierList option").each(function ()
            {
               if (window.CompareTextboxes($(this).val(), $(e.target).val()))
               {
                  carrierCd = $(this).attr("data-field-name");
                  i++;
                  return false;
               }
            });

            if (carrierCd !== undefined && carrierCd !== "0" && i === 1 && !editMode)
            {
               $('#carrierList option[data-field-name="' + carrierCd + '"]').closest("option").click();
               $("#Carrier").removeAttr("style").attr("disabled", "disabled").attr("readonly", "readonly");

               if ($("#Date").attr("disabled") !== undefined && $("#Date").attr("readonly") !== undefined)
               {
                  disabled = false;
                  trackingTab();
               }
               else
               {
                  $("#Date").focus();
               }
            }
         }
         if ($(".WrhsRecvDetail-div-click:not(.selected)[MgmtEntity-id='" + $(this).attr("data-field-value") + "']").length > 0
            && $("#Date").val() !== ""
            && $(".WrhsRecvDetail-div-click:not(.selected)[MgmtEntity-id='" + $(this).attr("data-field-value") + "'] td[exchange='Date'][exchange-value='" + $("#Date").val() + "']").closest("tr").length > 0
            && !editMode)
         {
            if ($("#Notes").val() === "" || !ntChnge)
            {
               var rowData = JSON.parse($(".WrhsRecvDetail-div-click:not(.selected)[MgmtEntity-id='" + $(this).attr("data-field-value") +
                  "'] td[exchange='Date'][exchange-value='" + $("#Date").val() + "']").closest("tr").attr("mgmtentity-data").split("=")[1]);
               $("#Notes").val(rowData.Notes);
            }
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange Date
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusout", "#Date", function (e)
   {
      var date = parseBarcode($(e.target).val(), "SpecialChar");

      if (getEditMode() && getFormChanged())
      {
         if (date === undefined || date === "" || !validatedate(date))
         {
            disabled = true;
            return false;
         }

         $(e.target).val(date);

         if (window.getSelectedActionTab() === TAB_TRACKING)
         {
            $("#Date").attr("disabled", "disabled").attr("readonly", "readonly");

            if ($("#Carrier").attr("readonly") !== undefined && $("#Carrier").attr("disabled") !== undefined)
            {
               disabled = false;

               $("#ID_TRKNO,#ID_RA").removeAttr("disabled").removeAttr("readonly");
               $("#ID_TRKNO").focus().keyup();
            }
            else
            {
               $("#Carrier").focus();
            }

         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onChange Notes
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("focusout", "#Notes", function (e)
   {
      var note = parseBarcode($(e.target).val(), "SpecialChar"); // Scannable via barcode scanner

      if (getEditMode() && getFormChanged())
      {
         if (note === undefined)
         {
            disabled = true;
            return false;
         }

         $(e.target).val(note);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Select a carrier from the Carrier dropdown list
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#carrierList option", function (e)
   {
      if ($(e.target).val() !== undefined && $(e.target).val() !== "0" && (trckId === "" || trckId === "0" || trckId === 0))
      {
         $("#Carrier").removeAttr("style");

         updateDataTable($(e.target).attr("data-field-name"), "CarrierID", trckId);
         updateDataTable($(e.target).val(), "Carrier", trckId);

         $("#Carrier").val($(e.target).val());
         $("#Carrier").attr("data-field-value", $(e.target).attr("data-field-name"));
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function submitTracking(validation)
   {
      $(this).closeMessageBox();

      if (active === TAB_SUMMARY && editMode)
      {
         return false;
      }

      var carrierName = $(".carrierCodeField")[0].value;
      var carrierCode = $('#carrierList option[value="' + carrierName + '"]').closest("option").attr("data-field-name") === undefined
         ? $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value")
         : $('#carrierList option[value="' + carrierName + '"]').closest("option").attr("data-field-name");

      if ((($("#ID_RA").val().length > 0 || $("#ID_WPT").val().length > 0) && notForProcessing)
         || (validation === 0 && ($("#ID_RA").val().length === 0 && $("#ID_WPT").val().length === 0))
         || (carrierCode === undefined || carrierCode === "")
         || $("#PackageName").attr("data-field-value") === ""
         || $("#Date").val() === ""
         || GetIgnoreCaseSpaceTxt($("#ID_TRKNO").val()) === "")
      {
         entered = false;

         if (GetIgnoreCaseSpaceTxt($("#ID_TRKNO").val()) === "")
         {
            setColorAndClass("rgb(243, 104, 104)", "#ID_TRKNO", "inputError");
            $("#ID_TRKNO").focus().keyup();
            $("#ID_TRKNO").select();
         }
         else if (validation === 0)
         {
            if ($("#ID_RA").val() === "")
            {
               setColorAndClass("rgb(243, 104, 104)", "#ID_RA", "inputError");
               $("#ID_RA").focus().keyup();
            }
         }

         return false;
      }

      if ($("#ID_RA").attr("readonly") === "readonly" && $("#ID_WPT").attr("readonly") === "readonly")
      {
         entered = false;
         return false;
      }

      var raOrWpt = (validation === 1 && !notForProcessing) || notForProcessing
         ? ""
         : $("#ID_RA").val().length > 0
            ? $("#ID_RA").val()
            : $("#ID_WPT").val();

      var type = (validation === 1 && !notForProcessing)
         ? "NR"
         : notForProcessing
            ? "NP"
            : $("#ID_RA").val().length > 0
               ? "RA"
               : "WPT";

      var trckId = (editMode && getEditMode())
         ? $(".searchGrid_WrhsRcvDetails-div-click.selected").attr("data-row-id")
         : 0;

      var items = [{
         "TrackingNumber": $("#ID_TRKNO").val(),
         "TypeCode": type,
         "RaOrWpt": raOrWpt,
         "TrackingID": trckId,
         "Route": routeCode,
         "CarrierID": carrierCode,
         "Date": $("#Date").val(),
         "Notes": $("#Notes").val(),
         "PackageCode": "UNKN",//UNKN because we don't have unit in the form
         "IsDeleted": (ntChnge && ($(".WrhsRecvDetail-div-click:not(.selected)[MgmtEntity-id='" +
            carrierCode + "'] td[exchange='Date'][exchange-value='" + $("#Date").val() + "']")
            .closest("tr").length > 0)) // this flag just for know note has been changed or not
      }];

      $("#wait").show();

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingEntity=" + JSON.stringify(items) + getAntiForgeryToken(),
         url: baseUrl + "/WrhsRecv/WrhsRecvSave",
         async: true,
         success: function (data)
         {
            var oAudio;
            $("#ID_SUBMIT_VALUE").on("click");
            routeCode = data.Tracking.RouteNumber;

            if (data.Success)
            {
               txtChange = false;
               ntChnge = false;
               printTrackingId = data.Tracking.TrackingID;
               printTrackingNumber = data.Tracking.TrackingNumber;
               printRaNumber = type === "NP" || type === "NR"
                  ? "N/A"
                  : data.Tracking.RaOrWpt;
               oAudio = document.getElementById("ID_" + routeCode);

               var trkRoute = routeCode === "96"
                  ? "Vault " + data.Tracking.Route
                  : routeCode === "97"
                     ? "Cage " + data.Tracking.Route
                     : data.Tracking.Route;

               if (oAudio !== null)
               {
                  if ($("#ID_" + routeCode).attr("src") !== "" && $("#ID_" + routeCode).attr("src") !== undefined)
                  {
                     oAudio.play();
                  }
               }
               else
               {
                  receivingSpeech(trkRoute);
               }

               $("#wait").hide();
               $("#ID_ROUTEVALUE span").html(trkRoute);
               $("#ID_ROUTEVALUE").removeAttr("class").addClass("Qosk_" + (routeCode === "6" ? "SpecialHandling" : data.Tracking.Route + routeCode)).addClass("routeDisplay");

               if ($("#ID_RA").hasClass("inputError"))
               {
                  removeColorAndClass("#F3F3F3", "#ID_RA", "inputError");
               }

               var cnt = parseInt($("#Count").html());

               if (!editMode)
               {
                  // Add count for new record only
                  $("#Count").html(parseInt(++cnt));
               }

               var $selectedRow = $(".WrhsRecvDetail-div-click.selected");
               if ($selectedRow.attr("MgmtEntity-id") === "0")
               {
                  $selectedRow.attr("MgmtEntity-id", carrierCode);
                  data.Tracking.CarrierID = carrierCode;
                  data.Tracking.Carrier = carrierName;
                  data.Tracking.Notes = $("#Notes").val();
                  data.Tracking.Date = $("#Date").val();
                  data.Tracking.Count = "1";
                  $selectedRow.attr("MgmtEntity-data", "entity=" + JSON.stringify(data.Tracking));
                  $selectedRow.find("td[exchange='Count']").attr("exchange-value", data.Tracking.Count).html(data.Tracking.Count);
                  $selectedRow.find("td[exchange='CarrierID']").closest("td").attr("exchange-value", data.Tracking.CarrierID);
                  $selectedRow.find("td[exchange='Carrier']").closest("td").attr("exchange-value", data.Tracking.Carrier);
                  $selectedRow.removeAttr("name");

                  //we must re-draw the grid in order to recognize the new row that was added
                  //TODO: still have to figure out how to set the data row table into a specific index/row. By default, DataTable adds rows to the end of the table.
                  $("#searchGrid").DataTable().row($selectedRow).remove().draw();
                  $("#searchGrid").DataTable().row.add($selectedRow).draw();
                  summaryDt = $("#searchGrid").DataTable();
                  incDecCount(cnt);
                  reorderSummary(false);
               }
               else
               {
                  incDecCount(cnt);
               }

               routeCode = -1;
               validate = false;

               if ($("#ID_Lock_TrackingNumber").attr("src").indexOf("(OFF)") <= 0)
               {
                  $("#ID_RA,#ID_WPT").val("");
                  $("#ID_RA").focus().keyup();
                  lockedtrckNo = true;
               }
               else
               {
                  $("#ID_RA,#ID_TRKNO").val("").removeClass("inputError").removeAttr("readonly");
                  $("#ID_RA").removeAttr("style").css("width", "95%");
                  $("#ID_RA").focus().keyup();
                  $("#ID_TRKNO").focus().keyup();
                  lockedtrckNo = false;
               }

               setCollapseRow();

               $("#WrhsRecvTracking input.inputRex").removeClass("inputError");
               $("#ID_RA").removeAttr("readonly");

               notForProcessing = false;
               updateDataTable($("#Count").html(), "Count", trckId);

               editMode = false;
               active = "";
               currentTrckNo = "";

               if (window.getSelectedActionTab() === TAB_TRACKING)
               {
                  enableActionbarControl("#CMD_NEW", false);
                  enableActionbarControl("#CMD_EDIT", false);
                  enableActionbarControl("#CMD_FILTER", false);
               }
               else
               {
                  enableActionbarControl("#CMD_NEW", true);
                  enableActionbarControl("#CMD_EDIT", true);
                  enableActionbarControl("#CMD_FILTER", true);
               }

               enableActionbarControl("#CMD_REFRESH", true);
               entered = false;

               if (getEditMode())
               {
                  window.setEditMode(false);
                  window.selectActionTab(TAB_TRACKING);
               }
            }
            else
            {
               $("#wait").hide();

               $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
               $("#ID_ROUTEVALUE span").html("RA Not Found");
               $("#ID_SUBMIT_VALUE").on("click");
               setColorAndClass("rgb(243, 104, 104)", "#ID_RA", "inputError");
               $("#ID_RA").select();

               oAudio = document.getElementById("ID_" + routeCode);

               if (oAudio !== null)
               {
                  if ($("#ID_" + routeCode).attr("src") !== "" && $("#ID_" + routeCode).attr("src") !== undefined)
                  {
                     oAudio.play();
                  }
               }
               else
               {
                  receivingSpeech(data.Tracking.Route);
               }

               entered = false;
            }
         },
         error: function (data)
         {
            $("#wait").hide();
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function refreshTrackingGrid(carrierCode, date, trckNo)
   {
      if (carrierCode === "" || date === "")
      {
         return false;
      }

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "carrierCode=" + carrierCode + "&date=" + date + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/WrhsRecvDetails",
         success: function (data)
         {
            var $target = $("#WrhsRcvDetailsGrid");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            entered = false;

            if (trckNo !== "" && trckNo !== "-1" && trckNo !== "0")
            {
               $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr td[exchange=TrackingID][exchange-value='"
                  + trckNo + "']").closest("tr").find("td[exchange=LastChange]")
                  .closest("td").attr("exchange-value", "1");
            }

            if (window.getSelectedActionTab() === TAB_TRACKING)
            {
               enableActionbarControl("#CMD_NEW", false);
               enableActionbarControl("#CMD_EDIT", false);
            }
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   enable/disable the progress arrows on identification segment.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setGrapicState(selector, state)
   {
      // assumes all graphics to be swapped contained the string '(ON)' or '(OFF)' in their filename
      var obj = $(selector);
      var src = obj.prop("src");

      if (state === true)
      {
         obj.attr("src", src.replace("(OFF)", "(ON)"));
      }
      else
      {
         obj.attr("src", src.replace("(ON)", "(OFF)"));
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Trigger the Not For Processing button; also used by the special barcode.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clickNotProcessing()
   {
      if (disabled || (editMode && active === TAB_SUMMARY))
      {
         return false;
      }

      notForProcessing = true;
      $("#ID_RA,#ID_WPT").val("");
      removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
      //removeColorAndClass("#ECDD9D", "#ID_WPT", "inputError");

      if ($("#ID_TRKNO").val().length === 0)
      {
         setColorAndClass("rgb(243, 104, 104)", "#ID_TRKNO", "inputError");
         return false;
      }

      if (!checkTrackingNumberChange("NFP"))
      {
         submitTracking(1);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Trigger the No RA/WPT button; also used by the special barcode.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clickNoRa()
   {
      if (disabled || (editMode && active === TAB_SUMMARY))
      {
         return false;
      }

      $("#ID_RA,#ID_WPT").val("");
      removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
      //removeColorAndClass("#ECDD9D", "#ID_WPT", "inputError");

      if ($("#ID_TRKNO").val().length === 0)
      {
         setColorAndClass("rgb(243, 104, 104)", "#ID_TRKNO", "inputError");
         return false;
      }

      if (!checkTrackingNumberChange("NRA"))
      {
         submitTracking(1);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function clickSubmit()
   {
      var trkNoBarcode = parseBarcode($.trim($("#ID_TRKNO").val()), "SpecialChar"); // Scanned via barcode scanner
      trkNoBarcode = parseBarcode(trkNoBarcode, "ID_TRKNO"); // Double check tracking field for reserved chars

      if (trkNoBarcode !== undefined && !trkNoBarcode)
      {
         if ($("#ID_Lock_TrackingNumber").attr("src").indexOf("(ON)") > 0) // Unlock Tracking number field if locked
         {
            $("#ID_Lock_TrackingNumber").click();
         }

         return false;
      }

      var rtrnAuthBarcode = parseBarcode($.trim($("#ID_RA").val()), "SpecialChar"); // Scanned via barcode scanner
      rtrnAuthBarcode = parseBarcode(rtrnAuthBarcode, "ID_RA"); // Used if a button barcode was scanned/typed in

      // If it's not numeric, then it's assumed to be a button press
      if (rtrnAuthBarcode !== "")
      {
         // Clear out RA textbox
         $("#ID_RA").val("");

         if (rtrnAuthBarcode === "LCK")
         {
            $("#ID_Lock_TrackingNumber").click();
         }
         else if (rtrnAuthBarcode === "NRA")
         {
            clickNoRa();
         }
         else if (rtrnAuthBarcode === "NFP")
         {
            clickNotProcessing();
         }
         else
         {
            setColorAndClass("rgb(243, 104, 104)", "#ID_RA", "inputError");
            $("#ID_RA").focus().keyup();
         }

         return false;
      }

      if (!entered)
      {
         entered = true;
         var submitId = 0;

         if (editMode && window.getEditMode())
         {
            if (($(".searchGrid_WrhsRcvDetails-div-click.selected td[exchange='TypeCode']").attr("exchange-value") === "NR"
               || $(".searchGrid_WrhsRcvDetails-div-click.selected td[exchange='TypeCode']").attr("exchange-value") === "NP")
               && $("#ID_RA").val() === "")
            {
               submitId = 1;
               $(".searchGrid_WrhsRcvDetails-div-click.selected td[exchange='TypeCode']").attr("exchange-value") === "NP"
                  ? notForProcessing = true
                  : notForProcessing = false;
            }
         }

         submitTracking(submitId);
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.clickSubmit = clickSubmit;


   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#ID_SUBMIT_VALUE", function ()
   {
      if (!checkTrackingNumberChange())
      {
         clickSubmit();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Not For Processing button in Tracking tab
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_not_processing", function ()
   {
      clickNotProcessing();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".inputRex,.textareaRex,.ctrlInput", function ()
   {
      if ($(this).attr("id") === "Date")
      {
         updateDataTable($(this).val()
            , "Date"
            , trckId);
         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth() + 1; //January is 0!
         var yyyy = today.getFullYear();

         if (dd < 10)
         {
            dd = '0' + dd;
         }

         if (mm < 10)
         {
            mm = '0' + mm;
         }

         today = mm + '/' + dd + '/' + yyyy;

         var dates = $("#Date").val();

         if (dates.length === 0 || !validatedate(dates))
         {
            setColorAndClass("rgb(243, 104, 104)", "#Date", "inputError");
            return false;
         }

         removeColorAndClass("#F2F2F2", "#Date", "inputError");

         if (window.getSelectedActionTab() === TAB_TRACKING)
         {
            $("a.active").click();
         }

         $(this).focus();

         if (($(".WrhsRecvDetail-div-click:not(.selected) td[exchange='Date'][exchange-value='" + $(this).val() + "']").length > 0
            && ($("#Carrier").attr("data-field-value") !== "" && $("#Carrier").attr("data-field-value") !== undefined)
            && $(".WrhsRecvDetail-div-click:not(.selected) td[exchange='Date'][exchange-value='" + $(this).val() + "']").closest("tr[MgmtEntity-id='" + $("#Carrier").attr("data-field-value") + "']").length > 0)
            && !editMode)
         {
            if ($("#Notes").val() === "" || !ntChnge)
            {
               var rowData = JSON.parse($(".WrhsRecvDetail-div-click:not(.selected) td[exchange='Date'][exchange-value='" + $(this).val() + "']").closest("tr[MgmtEntity-id='" + $("#Carrier").attr("data-field-value") + "']").attr("mgmtentity-data").split("=")[1]);
               $("#Notes").val(rowData.Notes);
            }
         }
      }

      if ($(this).attr("id") === "ID_TRKNO")
      {
         txtChange = true;

         if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
         {
            if (!editMode)
            {
               $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
               $("#ID_ROUTEVALUE span").html("--");
            }

            $("#" + $(this).attr("nextfocus")).focus();
         }

         removeColorAndClass("#ECDD9D", "#ID_TRKNO", "inputError");
         return false;
      }

      if ($(this).attr("id") === "ID_RA")
      {
         txtChange = true;
         if ($(this).val().length === 0)
         {
            validate = false;
            routeCode = -1;
            removeColorAndClass("#ECDD9D", "#ID_RA", "inputError");
         }
         else
         {
            if (!validate)
            {
               $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
               $("#ID_ROUTEVALUE span").html("--");
            }
            else
            {
               validate = false;
               routeCode = -1;
            }
         }
      }
      else if ($(this).attr("id") === "ID_WPT")
      {
         $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
         $("#ID_ROUTEVALUE span").html("--");

         if ($(this).val().length > 0)
         {
            submitTracking(1);
            //CheckValidation($(this).val(), "WPT", 1);
            $("#ID_RA").prop("readonly", "readonly");
         }
         else
         {
            $("#ID_RA").removeAttr("readonly");
            //removeColorAndClass("#ECDD9D", "#ID_WPT", "inputError");
         }
      }
      else if ($(this).attr("id") === "Notes")
      {
         ntChnge = true;
      }

      if (notForProcessing)
      {
         notForProcessing = false;
      }

      if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
      {
         $("#" + $(this).attr("nextfocus")).focus().keyup();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Datepicker event in the Date field up in the form
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#Date").datepicker(
      {
         onSelect: function (input, obj)
         {
            $("#ui-datepicker-div").hide();
            $("#Date").trigger("change");

            if ($(this).attr("nextfocus") !== undefined && $(this).attr("nextfocus") !== "")
            {
               $("#" + $(this).attr("nextfocus")).focus().keyup();
            }
         }
      });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Checkbox click event
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "input[type='checkbox']", function ()
   {
      var $row = $(this).closest("tr");

      if ($row.hasClass("selected"))
      {
         unselectedTrackingGrid();
      }
      else
      {
         $row.click();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Parses the specified field entered for special characters (such as
   //*   lock, No RA, etc.)
   //*
   //* Parameters:
   //*   barcode - String type - actual item scanned in field
   //*   source - String type - textbox in which the barcode resides
   //*
   //* Returns:
   //*   Value scanned, if the barcode scanned it's indeed a button press
   //*   and not an RA/ WPT
   //*
   //*****************************************************************************
   function parseBarcode(barcode, source)
   {
      if (source === "ID_TRKNO")
      {
         if (barcode.indexOf("!") > -1 || barcode.indexOf("@") > -1)
         {
            setColorAndClass("rgb(243, 104, 104)", "#ID_TRKNO", "inputError");
            $("#ID_TRKNO").val("");
            $("#ID_RA").val("");
            $("#ID_RA").focus().keyup();
            removeColorAndClass("#F3F3F3", "#ID_RA", "inputError");
            $("#ID_TRKNO").focus().keyup();

            routeCode = 100;
            var oAudioError = document.getElementById("ID_" + routeCode);

            if (oAudioError !== null)
            {
               if ($("#ID_" + routeCode).attr("src") !== "" && $("#ID_" + routeCode).attr("src") !== undefined)
               {
                  oAudioError.play();
               }
            }
            else
            {
               receivingSpeech("Unknown");
            }

            return false;
         }

         return true;
      }
      else if (source === "ID_RA")
      {
         if (barcode.substr(0, 2) === "!@" && barcode.substr(barcode.length - 2, 2) === "@!")
         {
            return barcode.replace("!@", "").replace("@!", "");
         }

         return "";
      }
      else if (source === "SpecialChar")
      {
         var barcodeChar = String.fromCharCode(KEY_SPECIALCHAR); //double check for barcode character; error out if found

         if (barcode.indexOf(barcodeChar) > -1)
         {
            var barcodeRegEx = new RegExp(barcodeChar, "g");
            barcode = barcode.replace(barcodeRegEx, "");
         }

         return barcode;
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function trackingTab()
   {
      if (getEditMode() && !editMode)
      {
         //New
         $(".formRex .inputRex,.selectEx").attr("readonly", "readonly");
         $(".formRex .inputRex,.selectEx").attr("disabled", "disabled");

         if ($(".formRex .inputRex").hasClass("inputError"))
         {
            $(".formRex input.inputError").removeAttr("readonly").removeAttr("disabled");
         }
         else
         {
            $(".formRex .inputRex,.selectEx").not("#Count,#Notes,#Date").removeAttr("style");
         }
      }

      if (editMode && (trackingID !== 0 && trackingID !== undefined) || $(".searchGrid_WrhsRcvDetails-div-click").hasClass("selected"))
      {
         if ($(".searchGrid_WrhsRcvDetails-div-click").hasClass("selected"))
         {
            enableActionbarControl("#CMD_NEW", true);
            enableActionbarControl("#CMD_EDIT", true);
         }

         return false;
      }

      var carrierName = $(".carrierCodeField")[0].value;
      var carrierCode = carrierId === "" ? $('#carrierList option[value="' + carrierName + '"]').attr("data-field-name") : carrierId;
      var date = oldDate === ""
         ? $("#Date").val() === ""
            ? $("#searchGrid tr.selected td[exchange='Date']").closest("td").attr("exchange-value")
            : $("#Date").val()
         : oldDate;

      if (carrierCode === undefined || carrierCode === "")
      {
         carrierCode = $("#searchGrid tr").hasClass("selected")
            ? $("#searchGrid tr.selected").attr("MgmtEntity-id")
            : "";
      }

      if (!validatedate(date) && getEditMode())
      {
         setColorAndClass("rgb(243, 104, 104)", "#Date", "inputError");
         $("#Date").removeAttr("disabled").removeAttr("readonly");
         $("#Date").focus();
      }

      if (carrierCode === "" && getEditMode())
      {
         $("#Carrier").removeAttr("disabled").removeAttr("readonly");
         setColorAndClass("rgb(243, 104, 104)", "#Carrier", "inputError");
         $("#Carrier").focus();
      }

      if ((carrierCode === "" || carrierCode === undefined || carrierCode === "0") || (date === "" || date === undefined || !validatedate(date)))
      {
         $("#ID_TRKNO,#ID_RA").attr({ "readonly": "readonly" }, { "disabled": "disabled" });
         disabled = true;
      }
      else
      {
         //reset arrows in Identification segment
         $("#ID_RA").removeAttr("disabled").removeAttr("readonly");
         setGrapicState("#IDENT_FIRST_ARROW", lockedtrckNo);

         if (!lockedtrckNo)
         {
            $("#ID_TRKNO").removeAttr("readonly disabled");
            $("#ID_TRKNO").css("background-color", "#ECDD9D");
         }
      }

      if (!$(".searchGrid_WrhsRcvDetails-div-click").hasClass("selected"))
      {
         setCollapseRow();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkTrackingNumberChange(s)
   {
      if (editMode && currentTrckNo !== $("#ID_TRKNO").val())
      {
         var input = s === undefined ? "" : s;

         messageObject = [
            {
               "id": "lbl_messages",
               "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>Tracking Number has been modified.<br><br>The new Tracking Number will be updated for all RA's associated with it for this Carrier on this Date.<br><br>Would you like to continue?</h3>"
            }
         ];

         btnObjects = [
            {
               "id": "btn_confirmation",
               "name": "Yes",
               "class": "btnErrorMessage",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "onclick='window.trkNumOverride(" + JSON.stringify(input) + ");'"
            }, {
               "id": "btn_rejection",
               "name": "No",
               "class": "btnErrorMessage",
               "style": "margin:0 10px;text-align:center;width:70px",
               "function": "onclick='$(this).closeMessageBox();'"
            }
         ];

         titleobjects = [
            {
               "title": "Confirm Changing Tracking Number"
            }
         ];

         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);

         return true;
      }

      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setCollapseRow()
   {
      $("#searchGrid_WrhsRcvDetails").DataTable().clear().draw(); //clear the previous data table entry; prevents from previous data showing up in the Search box (see DE813)
      $("#searchGrid_WrhsRcvDetails tbody").html($collapsedRow);
      $("#searchGrid_WrhsRcvDetails_info").html("Showing 0 to 0 of 0 entries");
      window.resizeTabs();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   always set a focus to the appropriate field, depending on the Edit Mode status
   //*   and the value of each field in question
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   setInterval(function ()
   {
      if (window.getSelectedActionTab() === TAB_TRACKING)
      {
         //as long as we're not in edit mode, keep focus in the grid search field once the user focuses on the field
         if ($("#searchGrid_WrhsRcvDetails_filter input[type=search]").is(":focus") && !getEditMode())
         {
            return false;
         }

         //keep focus on the tracking field so long as it's editable and there is no value in the field
         if ($("#ID_TRKNO").is(":focus") && ($("#ID_TRKNO").attr("disabled") === undefined
            && $("#ID_TRKNO").attr("readonly") === undefined))
         {
            if ($("#ID_TRKNO").val() === "")
            {
               $("#ID_TRKNO").focus().keyup();
            }

            return false;
         }

         //keep focus on the RA field so long as it's editable and there is no value in the field
         else if ($("#ID_RA").is(":focus") && ($("#ID_RA").attr("disabled") === undefined
            && $("#ID_RA").attr("readonly") === undefined))
         {
            return false;
         }

         //keep focus on the tracking field so long as it's editable and there is no value in the field
         if (($("#ID_TRKNO").val() === ""
            && !lockedtrckNo)
            && ($("#ID_TRKNO").attr("disabled") === undefined && $("#ID_TRKNO").attr("readonly") === undefined))
         {
            $("#ID_TRKNO").focus().keyup();
         }

         //focus on the RA field if the tracking number is filled OR we're in locked tracking mode
         else if ((!$("#ID_RA").is(":focus")
            && !$("#ID_TRKNO").is(":focus") && $("#ID_TRKNO").val() !== "")
            && ($("#ID_RA").attr("disabled") === undefined && $("#ID_RA").attr("readonly") === undefined) || lockedtrckNo)
         {
            $("#ID_RA").focus().keyup();

            if ($("#ID_RA").hasClass("inputError"))
            {
               $("#ID_RA").select();
            }
         }

         //keep focus on the carrier field if there is an error there
         else if ($("#Carrier").val() === ""
            || $("#Carrier").attr("data-field-value") === "0"
            && ($("#Carrier").attr("disabled") === undefined && $("#Carrier").attr("readonly") === undefined)
            && (active === TAB_SUMMARY && !editMode))
         {
            $("#Carrier").focus();
         }

         //keep focus on the date field if there is an error there
         else if ($("#Date").val() === ""
            && ($("#Date").attr("disabled") === undefined && $("#Date").attr("readonly") === undefined)
            && (active === TAB_SUMMARY && !editMode))
         {
            $("#Date").focus();
         }
      }
   }, 500);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#IDENT_CMD_PRINT", function ()
   {
      if ($("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr").hasClass("selected"))
      {
         printTrackingId = $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr.selected td[exchange=TrackingID]").closest("td").attr("exchange-value");
         printTrackingNumber = $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr.selected td[exchange=TrackingNumber]").closest("td").attr("exchange-value");
         printRaNumber = $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr.selected td[exchange=TypeCode]").closest("td").attr("exchange-value") === "NR" || $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr.selected td[exchange=TypeCode]").closest("td").attr("exchange-value") === "NP"
            ? "N/A"
            : $("#WrhsRcvDetailsGrid #searchGrid_WrhsRcvDetails tr.selected td[exchange=RA]").closest("td").attr("exchange-value");
      }

      if (printTrackingId === undefined || printTrackingId.length === 0)
      {
         return false;
      }

      $.getJSON("https://localhost:8006/Qualanex/http/Print?funcClass=tracking&dataStream=TRKGID=" + printTrackingId +
         "§TRKGNO=" + printTrackingNumber + "§RA=" + printRaNumber + "",
         {},
         function (data)
         {
            console.log(data);
         });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get Anti Forgery token from the Form.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete Tracking Numbers based on the list passed (checked)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function DeleteTrackingItemList(e)
   {
      $(this).closeMessageBox();
      deleteCheckedTrkOrRaNbrs(e, "TrackingNumber");
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.DeleteTrackingItemList = DeleteTrackingItemList;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete RA Numbers based on the list passed (checked)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function DeleteRAItemList(e)
   {
      $(this).closeMessageBox();
      deleteCheckedTrkOrRaNbrs(e, "RA");
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.DeleteRAItemList = DeleteRAItemList;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete all RAs that have the RA/WPT delete checkbox toggled
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".RaWptListDel", function (e)
   {
      if (getEditMode())
      {
         return false;
      }

      var trkIdList = [];

      $(".searchGrid_WrhsRcvDetails-div-click").each(function ()
      {
         var $chkBox = $(this).find("input.RaWptChkBox[type='checkbox']");

         if ($chkBox.is(":checked"))
         {
            trkIdList.push($chkBox.attr("data-field-value"));
         };
      });

      if (!trkIdList.length)
      {
         return false;
      }

      messageObject = [{
         "id": "lbl_messages",
         "name": trkIdList.length === 1
            ? "<h3 style='margin:0 auto;text-align:center;width:100%'> Are you sure you want to delete this record?</h3>"
            : "<h3 style='margin:0 auto;text-align:center;width:100%'> Are you sure you want to delete these " + trkIdList.length.toString() + " records?</h3>"
      }];

      btnObjects = [{
         "id": "btn_deleteRA",
         "name": "Yes",
         "class": "btnErrorMessage",
         "style": "margin:0 10px;text-align:center;width:70px",
         "function": "onclick='window.DeleteRAItemList(" + JSON.stringify(trkIdList) + ");'"
      }, {
         "id": "btn_no",
         "name": "No",
         "class": "btnErrorMessage",
         "style": "margin:0 10px;text-align:center;width:70px",
         "function": "onclick='$(this).closeMessageBox();'"
      }
      ];

      titleobjects = [{
         "title": "Confirm Delete"
      }];

      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete all Tracking Numbers and its associated RAs/WPTs that have the
   //*   Tracking # delete checkbox toggled
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".TrkNbrListDel", function (e)
   {
      if (getEditMode())
      {
         return false;
      }

      var trkIdList = [];

      $(".searchGrid_WrhsRcvDetails-div-click").each(function ()
      {
         var $chkBox = $(this).find("input.trkChkBox[type='checkbox']");

         if ($chkBox.is(":checked"))
         {
            trkIdList.push($chkBox.attr("data-field-value"));
         };
      });

      if (!trkIdList.length)
      {
         return false;
      }

      var i = 0;
      var countDt = 0;

      for (i = 0; i < trkIdList.length; i++)
      {
         // for Tracking Numbers, we have to grab the count of Tracking IDs (RAs) under each tracking #
         countDt += $("#searchGrid_WrhsRcvDetails tr td[exchange='TrackingNumber'][exchange-value='" + trkIdList[i] + "']").closest("td").length;
      }

      var raCountMsg = "This count affects ";
      raCountMsg += countDt === 1 ? "a single RA record." : countDt.toString() + " RA records.";

      messageObject = [{
         "id": "lbl_messages",
         "name": trkIdList.length === 1
            ? "<h3 style='margin:0 auto;text-align:center;width:100%'> Are you sure you want to delete this tracking record? " + raCountMsg + "</h3>"
            : "<h3 style='margin:0 auto;text-align:center;width:100%'> Are you sure you want to delete these " + trkIdList.length.toString() + " tracking records? " + raCountMsg + "</h3>"
      }];

      btnObjects = [{
         "id": "btn_deleteTrckingNumber",
         "name": "Yes",
         "class": "btnErrorMessage",
         "style": "margin:0 10px;text-align:center;width:70px",
         "function": "onclick='window.DeleteTrackingItemList(" + JSON.stringify(trkIdList) + ");'"
      }, {
         "id": "btn_no",
         "name": "No",
         "class": "btnErrorMessage",
         "style": "margin:0 10px;text-align:center;width:70px",
         "function": "onclick='$(this).closeMessageBox();'"
      }
      ];

      titleobjects = [{
         "title": "Confirm Delete"
      }];

      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Tracking number override confirmation
   //*
   //* Parameters:
   //*   e - the code passed on the confirmation popup, allows us to distinguish between an RA submit and a Not For Processing and/or No RA/WPT button simulation
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function trkNumOverride(e)
   {
      $(this).closeMessageBox();
      switch (e)
      {
         case "NRA":
            submitTracking(1);
            break;
         case "NFP":
            submitTracking(1);
            break;
         default:
            clickSubmit();
            break;
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.trkNumOverride = trkNumOverride;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Unselect Behavior for tracking grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function unselectedTrackingGrid()
   {
      if ($("#searchGrid_WrhsRcvDetails tr").hasClass("selected"))
      {
         $("#ID_ROUTEVALUE").removeAttr("class").addClass("routeDisplay routeNone");
         $("#ID_ROUTEVALUE span").html("--");
      }

      $("#searchGrid_WrhsRcvDetails tr.selected").closest("tr").removeClass("selected");
      $("#WrhsRecvTracking .btnErrorMessage ,#ID_SUBMIT_VALUE ").on("click");

      enableActionbarControl("#CMD_NEW", false);
      enableActionbarControl("#CMD_EDIT", false);

      if (!lockedtrckNo)
      {
         $("#WrhsRecvTracking #ID_TRKNO").removeAttr("readonly disabled").removeClass("inputError").css("background-color", "#ECDD9D");
         disabled = false;
         trackingID = 0;
         $("#WrhsRecvTracking input[type=Text]").val("");

         var attSrc = $("#ID_Lock_TrackingNumber").attr("src");
         var $newSrc = "";

         if (attSrc.indexOf("(OFF)") <= 0)
         {
            $newSrc = attSrc.replace("(ON)", "(OFF)");
            $("#ID_TRKNO").removeAttr("readonly disabled");
            $("#ID_TRKNO").focus().keyup();
            $("#ID_Lock_TrackingNumber").attr("src", $newSrc);
         }

         $("#ID_TRKNO").focus().keyup();
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Delete Items in tracking Grid based on the amount of tracking number
   //*   checkboxes toggled.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function deleteCheckedTrkOrRaNbrs(list, item)
   {
      var countDt = 0;
      $("#wait").show();

      if (item === "RA")
      {
         // for RAs, we grab individual Tracking IDs tied to the RA
         // therefore, 1 tracking ID = 1 RA
         countDt = list.length;
      }
      else if (item === "TrackingNumber")
      {
         for (var i = 0; i < list.length; i++)
         {
            // for Tracking Numbers, we have to grab the count of Tracking IDs (RAs) under each tracking #
            countDt += $("#searchGrid_WrhsRcvDetails tr td[exchange='TrackingNumber'][exchange-value='" + list[i] + "']").closest("td").length;
         }
      }

      if ($("#searchGrid_WrhsRcvDetails tr").hasClass("selected"))
      {
         unselectedTrackingGrid();
      }

      var dt = $("#Date").val();
      var carrierName = $(".carrierCodeField")[0].value;
      var carrierCode = $('#carrierList option[value="' + carrierName + '"]').closest("option").attr("data-field-name") === undefined
         ? $("#searchGrid tr.selected td[exchange='CarrierID']").closest("td").attr("exchange-value")
         : $('#carrierList option[value="' + carrierName + '"]').closest("option").attr("data-field-name");

      currentTrckNo = "";

      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "trackingList=" + list + "&item=" + item + "&CarrierId=" + carrierCode + "&Date=" + dt + getAntiForgeryToken(),
         url: baseUrl + baseUrl + "/DeleteItemsList",
         success: function (data)
         {
            if (data.Success)
            {
               var remainingTrkRec = parseInt($("#Count").html()) - countDt;
               $("#searchGrid_WrhsRcvDetails tr").removeClass("selected");
               $("#TrackingNumber").removeAttr("disabled readonly").val("");

               $("#Count").html(remainingTrkRec <= 0
                  ? 0
                  : remainingTrkRec);

               incDecCount(remainingTrkRec);
               window.OnQnexRefresh();
               $("#wait").hide();
            }
            else
            {
               $("#wait").hide();

               messageObject = [
                  {
                     "id": "lbl_messages",
                     "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on delete request.</h3>"
                  }
               ];

               btnObjects = [
                  {
                     "id": "btn_ok_error",
                     "name": "Ok",
                     "class": "btnErrorMessage",
                     "style": "margin:0 auto;text-align:center",
                     "function": "onclick='$(this).closeMessageBox();'"
                  }
               ];

               titleobjects = [
                  {
                     "title": "Invalid Entry"
                  }
               ];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         },
         error: function (data)
         {
            $("#wait").hide();

            messageObject = [
               {
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call failed on receiving delete request.</h3>"
               }
            ];

            btnObjects = [
               {
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
            ];

            titleobjects = [
               {
                  "title": "Invalid Entry"
               }
            ];

            $(this).addMessageButton(btnObjects, messageObject);
            $(this).showMessageBox(titleobjects);
         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Text to Speech.
   //*
   //* Parameters:
   //*   txt - Text for Text to speech.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function receivingSpeech(txt)
   {
      $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + txt,
         {},
         function (data)
         {
         });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Increment/Decrement Count
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function incDecCount(cnt)
   {
      if (cnt === 0)
      {
         var selectedDate = $(".WrhsRecvDetail-div-click.selected td[exchange='Date']").attr("exchange-value");

         if ($(".WrhsRecvDetail-div-click.selected td[exchange='Date']").attr("style") === undefined)
         {

            var selectedClass = $(".WrhsRecvDetail-div-click.selected").hasClass("odd") ? "odd" : "even";

            $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" +
               selectedDate + "']").closest("tr").first().find("td[exchange='Date']")
               .html($(".WrhsRecvDetail-div-click.selected td[exchange='Date']").attr("exchange-value")).closest("td").removeAttr("style");
         }

         if ($(".WrhsRecvDetail-div-click.selected").next().length > 0)
         {
            $(".WrhsRecvDetail-div-click.selected").remove();
            $(".WrhsRecvDetail-div-click").each(function ()
            {
               $(this).removeClass("odd").removeClass("even").addClass(($(this).index() + 1) % 2 === 0 ? "even" : "odd");
               $(this).closest("tr").first().removeClass("firstRow");
            });
         }

         $(".formRex .inputRex").not("#Count").html("");

         if ($(".WrhsRecvDetail-div-click").length > 0)
         {
            $("#searchGrid_info").html("Showing 1 to " + $(".WrhsRecvDetail-div-click").length + " of " + $(".WrhsRecvDetail-div-click").length + " entries");
         }
         else
         {
            $("#searchGrid_info").html("Showing 0 to 0 of 0 entries");
            $("#WrhsRecvDetail").append($summaryEmptyRow);
            window.resizeTabs();
         }

         $("#tab-container a[href='#tabs-summary']").closest("a").click();
      }
      else
      {
         $(".WrhsRecvDetail-div-click.selected td[exchange='Count']").attr("exchange-value", cnt).html(cnt);
         var rowInf = JSON.parse($(".WrhsRecvDetail-div-click.selected").attr("MgmtEntity-data").split("=")[1]);
         rowInf.Count = cnt;
         $(".WrhsRecvDetail-div-click.selected").attr("MgmtEntity-data", "entity=" + JSON.stringify(rowInf));
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Re-Order summary grid rows by date
   //*
   //* Parameters:
   //*   infScroll - determine if the reorder function was called via infinite scroll or not; affects some re-ordering
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function reorderSummary(infScroll)
   {
      var rowDate = $("#Date").val();
      var oldRowDate = $(".WrhsRecvDetail-div-click.selected td[exchange='DateHidden']").attr("exchange-value");

      $(".WrhsRecvDetail-div-click.selected td[exchange='DateHidden']").attr("exchange-value", rowDate);
      $(".WrhsRecvDetail-div-click.selected td[exchange='Date']").attr("exchange-value", rowDate);

      var rowCarrierId = $(".WrhsRecvDetail-div-click.selected td[exchange='CarrierID']").attr("exchange-value");
      var $rowSelected = $(".WrhsRecvDetail-div-click.selected");

      //first check to see if any other rows exist with the same receiving date as the row that was just entered/edited
      if ($(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']").length > 0)
      {
         //check if the selected row's carrier already exists for that date; merge if so
         if ($(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
            .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").length > 0)
         {
            var rId = JSON.parse($(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr").attr("mgmtentity-data").split("=")[1]);
            var stgDate = $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr").find("td[exchange='StageDate']").attr("exchange-value");

            rId.PackageCode = stgDate === "1" ? rId.PackageCode : "UNKN";
            rId.PackageName = stgDate === "1" ? rId.PackageName : "Unknown";
            rId.Count = parseInt($("#Count").html()) + parseInt(rId.Count);
            rId.Notes = $("#Notes").val();

            $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr")
               .attr("mgmtentity-data", "entity=" + JSON.stringify(rId));

            $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr").
               find("td[exchange='Count']").html(rId.Count);

            $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr").
               find("td[exchange='Count']").attr("exchange-value", rId.Count);

            $(".WrhsRecvDetail-div-click.selected").remove();
            $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']")
               .closest("tr").find("td[exchange='CarrierID'][exchange-value='" + rowCarrierId + "']").closest("tr").first().addClass("selected");

            $("#Count").html(rId.Count);
         }
         //otherwise place to the last row by default
         //note that the infScroll flag should be false if we're reordering via new entry/edit mode
         else if (!infScroll)
         {
            $(".WrhsRecvDetail-div-click.selected").remove();
            $rowSelected.insertAfter($(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date'][exchange-value='" + rowDate + "']").closest("tr").last());
         }

         if ($(".WrhsRecvDetail-div-click td[exchange='Date'][exchange-value='" + oldRowDate + "']").length > 0)
         {
            if ($(".WrhsRecvDetail-div-click td[exchange='Date'][exchange-value='" + oldRowDate + "']").closest("tr").first().find("td[exchange='Date']").attr("style") !== undefined)
            {
               $(".WrhsRecvDetail-div-click td[exchange='Date'][exchange-value='" + oldRowDate + "']").closest("tr").first().find("td[exchange='Date']").removeAttr("style").html(oldRowDate);
            }
         }
      }
      //this assumes the selected row would be the "first entry" for the set received date
      else
      {
         //try to find the row in which the selected receipt belongs (based on received date)
         var dateFound = false;

         //iterate through the list and place the row based on its receiving date
         $(".WrhsRecvDetail-div-click").not(".selected").find("td[exchange='Date']").each(function ()
         {
            var currentDate = new Date($(this).attr("exchange-value"));

            if (currentDate < new Date(rowDate))
            {
               $rowSelected.find("td[exchange='Date']").removeAttr("style").html(rowDate);
               $rowSelected.insertBefore($(this).closest("tr"));
               dateFound = true;
               return false;
            }
         });

         //if we still cannot find the row with the date (i.e. date earlier than last receiving record available), then we assume the date belongs in the last row
         if (!dateFound)
         {
            $rowSelected.find("td[exchange='Date']").removeAttr("style").html(rowDate);
            $(".WrhsRecvDetail-div-click").closest("tbody").append($rowSelected);
         }
      }

      $(".WrhsRecvDetail-div-click").each(function ()
      {
         $(this).removeClass("odd").removeClass("even").addClass(($(this).index() + 1) % 2 === 0 ? "even" : "odd");
      });

      window.reshuffleDateRows($(".WrhsRecvDetail-div-click"));
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Select a Form by ID, serialize the content, clean it, and send it back
   //*   as an object
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   (function ($)
   {
      $.fn.serializeObject = function ()
      {
         var self = this,
            json = {
            },
            pushCounters = {},
            patterns = {
               "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
               "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
               "push": /^$/,
               "fixed": /^\d+$/,
               "named": /^[a-zA-Z0-9_]+$/
            };

         this.build = function (base, key, value)
         {
            base[key] = value;
            return base;
         };

         this.push_counter = function (key)
         {
            if (pushCounters[key] === undefined)
            {
               pushCounters[key] = 0;
            }
            return pushCounters[key]++;
         };

         $.each($(this).serializeArray(), function ()
         {
            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
               return;
            }

            var k,
               keys = this.name.match(patterns.key),
               merge = this.value,
               reverseKey = this.name;

            while ((k = keys.pop()) !== undefined)
            {
               // adjust reverse_key
               reverseKey = reverseKey.replace(new RegExp("\\[" + k + "\\]$"), "");

               // push
               if (k.match(patterns.push))
               {
                  merge = self.build([], self.push_counter(reverseKey), merge);
               }
               // fixed
               else if (k.match(patterns.fixed))
               {
                  merge = self.build([], k, merge);
               }
               // named
               else if (k.match(patterns.named))
               {
                  merge = self.build({
                  }, k, merge);
               }
            }

            json = $.extend(true, json, merge);
         });

         return json;
      };
   })(jQuery);
});
