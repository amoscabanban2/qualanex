///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsRecvViewModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsRecv.ViewModels
{
   using System;
   using System.Collections.Generic;
   using System.Data.SqlClient;
   using System.Linq;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.ViewModels;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsRecvViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "WAREHS_RECV";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first Dock Receiving record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public WrhsRecvModel SelectedDockModel { get; set; } = new WrhsRecvModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Dock Receiving list for the Dock Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsRecvModel> WrhsRecvDetails { get; set; } = new List<WrhsRecvModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Dock Receiving list for the Dock Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsRecvModel> CarrierList { get; set; } = new List<WrhsRecvModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Sound files.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsRecvModel> WrhsSoundFiles { get; set; } = new List<WrhsRecvModel>();
      
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Dock Receiving list for the Dock Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsRecvModel> WrhsRecvTrackingList { get; set; } = new List<WrhsRecvModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<WrhsRecvModel> WrhsPackageTypeList { get; set; } = new List<WrhsRecvModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public string GetTitle()
      {
         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return cloudEntities.Database.SqlQuery<string>("SELECT DisplayFormat FROM AppDiagram WHERE AppCode=@appCode",
                  new SqlParameter("@appCode", this.AppletKey)).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return this.AppletKey;
      }
   }

}
