﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsRecvController.cs">
///   Copyright (c) 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsRecv.Controllers
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.QoskCloud.Web.Areas.WrhsRecv.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "WAREHS_RECV")]
   public class WrhsRecvController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Receiving View.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult WrhsRecvView()
      {
         this.ModelState.Clear();

         var viewModel = new WrhsRecvViewModel
         {
            ActionMenu = WrhsRecvModel.GetActionMenu(),
            CarrierList = WrhsRecvModel.GetCarrierList(),
            WrhsSoundFiles = WrhsRecvModel.GetAllUrl()
         };

         if (viewModel.WrhsRecvDetails.Any())
         {
            viewModel.SelectedDockModel = viewModel.WrhsRecvDetails[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Receiving Summary.
      /// </summary>
      /// <param name="criteria"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(WrhsRecvModel criteria)
      {
         this.ModelState.Clear();

         if (criteria == null)
         {
            criteria = new WrhsRecvModel();
         }

         var viewModel = new WrhsRecvViewModel
         {
            WrhsRecvDetails = WrhsRecvModel.GetDockDoorDetails(criteria)
         };

         ViewBag.Date = criteria.Date;
         ViewBag.ToDate = criteria.ModifiedDate;

         return this.PartialView("_WrhsRecvSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Receiving Details form.
      /// </summary>
      /// <param name="carrierCode"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveReceivingDetail(string carrierCode, string bodyData, bool isDeleted)
      {
         var curUser = this.HttpContext.User.Identity.Name;
         var bodyDateMdl = System.Web.Helpers.Json.Decode<WrhsRecvModel>(bodyData);
         var checkValue = bodyDateMdl.CarrierID;

         if (!string.IsNullOrWhiteSpace(checkValue))
         {
            var pCode = WrhsRecvModel.CheckCarrierCode(checkValue);

            if (pCode == 0)
            {
               var errorMessage = "The Carrier code '" + checkValue +
                  "' does not exist. Please try another carrier code.";

               if (!string.IsNullOrWhiteSpace(errorMessage))
               {
                  return this.Json(new
                  {
                     Success = false,
                     Error = errorMessage,
                     max = 0
                  }, JsonRequestBehavior.AllowGet);
               }
            }
         }

         var blnCheck = WrhsRecvModel.EditReceiving(curUser, carrierCode, bodyDateMdl, isDeleted);

         return this.Json(new
         {
            Success = blnCheck.IsAllDeleted,
            Error = !blnCheck.IsAllDeleted,
            Date = blnCheck.Date,
            CarrierId = blnCheck.CarrierID
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Tracking Tab.
      /// </summary>
      /// <param name="carrierCode"></param>
      /// <param name="date"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsRecvTracking(string carrierCode, string date)
      {
         var viewModel = string.IsNullOrWhiteSpace(carrierCode) || carrierCode == "0"
            ? new WrhsRecvViewModel()
            : new WrhsRecvViewModel
            {
               WrhsRecvTrackingList = WrhsRecvModel.GetTrackingPackages(carrierCode, date)
            };

         return this.PartialView("_WrhsRecvTracking", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Tracking Entity. We had to apply this logic in WrhsStagingController.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsStagingController.cs under the function NewReceivingSave().
      /// </summary>
      /// <param name="trackingEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsRecvSave(string trackingEntity)
      {
         var blnCheck = System.Web.Helpers.Json.Decode<List<WrhsRecvModel>>(trackingEntity)[0].TrackingID == "0"
               ? WrhsRecvModel.SaveTracking(System.Web.Helpers.Json.Decode<List<WrhsRecvModel>>(trackingEntity)[0], this.HttpContext.User.Identity.Name)
               : WrhsRecvModel.EditReceiving(System.Web.Helpers.Json.Decode<List<WrhsRecvModel>>(trackingEntity)[0], this.HttpContext.User.Identity.Name);

         return this.Json(new
         {
            Success = !string.IsNullOrWhiteSpace(blnCheck.TrackingID),
            Error = string.IsNullOrWhiteSpace(blnCheck.TrackingID),
            Tracking = blnCheck,
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Tracking Grid.
      /// </summary>
      /// <param name="date"></param>
      /// <param name="carrierCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult WrhsRecvDetails(string carrierCode, string date)
      {
         var viewModel = new WrhsRecvViewModel
         {
            WrhsRecvTrackingList = string.IsNullOrWhiteSpace(carrierCode) || carrierCode == "0"
                                    ? new List<WrhsRecvModel>()
                                    : WrhsRecvModel.GetTrackingPackages(carrierCode, date)
         };

         return this.PartialView("_WrhsRecvDetails", viewModel.WrhsRecvTrackingList);
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete Checked Items in tracking tab.
      /// </summary>
      /// <param name="trackingList"></param>
      /// <param name="item"></param>
      /// <param name="carrierId"></param>
      /// <param name="date"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult DeleteItemsList(string trackingList, string item, string carrierId = null, string date = null)
      {
         var query = item == "RA" ? 0 : item == "TrackingNumber" ? 1 : -1;

         if (query == -1)
         {
            return this.Json(new { Success = false, Error = true }, JsonRequestBehavior.AllowGet);
         }

         var trk = trackingList.Split(',').ToList();

         switch (query)
         {
            case 0:
               WrhsRecvModel.DeleteItem(trk, this.HttpContext.User.Identity.Name);
               break;
            case 1:
               WrhsRecvModel.DeleteItems(trk, carrierId, date, this.HttpContext.User.Identity.Name);
               break;
         }

         return this.Json(new { Success = true, Error = false }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Carrier Name.
      ///   <param name="browser"></param>
      ///   <param name="strData"></param>
      ///   <param name="keyPressValue"></param>
      ///   <returns></returns>
      /// </summary>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult GetCarrierName(string strData, int keyPressValue, string browser)
      {
         var profileEntity = string.IsNullOrWhiteSpace(strData)
                              ? new List<WrhsRecvModel>()
                              : WrhsRecvModel.GetCarrierList(false, strData, browser);

         var jsonResult = this.Json(new { profileEntity = profileEntity, keyPressValue = keyPressValue }, JsonRequestBehavior.AllowGet);
         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityClick(string entity)
      {
         this.ModelState.Clear();

         return this.PartialView("_WrhsRecvForm", System.Web.Helpers.Json.Decode<WrhsRecvModel>(entity));
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check has staged record under date and carrier.
      /// </summary>
      /// <param name="date"></param>
      /// <param name="carrierId"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult hasStaged(string date, string carrierId)
      {
         return this.Json(new
         {
            Success = WrhsRecvModel.hasStaged(date, carrierId)
         }
         , JsonRequestBehavior.AllowGet);
      }
   }
}
