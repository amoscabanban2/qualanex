///-----------------------------------------------------------------------------------------------
/// <copyright file="WrhsRecvModel.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WrhsRecv.ViewModels
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Data.SqlClient;
   using System.IO;
   using System.Linq;
   using System.Numerics;
   using System.Text.RegularExpressions;
   using System.Threading;

   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Blob;
   using Microsoft.WindowsAzure.Storage.Shared.Protocol;

   using Utility.Common;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model.TrackingModel;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class RAEntity
   {
      public string RtrnAuthID { get; set; }
      public string RtrnAuthCtrlCode { get; set; }
      public string RtrnAuthStatusCode { get; set; }
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class WrhsRecvModel : IInfiniteScroll, IQoskApplet
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      private const string NonRATypeCodes = "NP:NR";

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public WrhsRecvModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~WrhsRecvModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Date")]
      public string Date { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Route Number")]
      public string RouteNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Alert")]
      public string Alert { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"IsDeleted")]
      public bool IsDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Dates")]
      public DateTime Dates { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"IsAllDeleted")]
      public bool IsAllDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created Date")]
      public DateTime? CreatedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Staging Date")]
      public bool StagingDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Determine if the tracking event has at least one staged record for the given tracking number, carrier, and date
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Has Staged Indicator")]
      public bool HasStge { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Receiving Date")]
      public DateTime? ReceivingDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tracking Number")]
      public string TrackingNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Route")]
      public string Route { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tracking ID")]
      public string TrackingID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Code")]
      public string PackageCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Package Name")]
      public string PackageName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"UnitCount")]
      public string UnitCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RaOrWpt")]
      public string RaOrWpt { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Type Code")]
      public string TypeCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA")]
      public string RA { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Barcode")]
      public string Barcode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Receiving ID")]
      public string ReceivingID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Carrier")]
      public string Carrier { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"CarrierID")]
      public string CarrierID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Count")]
      public int Count { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Notes")]
      public string Notes { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Modified Date")]
      public string ModifiedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Infinite Scroll
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int InitialLoadCount { get; set; } = 40;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Infinite Scroll
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int ScrollLoadCount { get; set; } = 40;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Infinite Scroll
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int PreviousCount { get; set; } = 0;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Infinite Scroll
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public int LoadAtHeight { get; set; } = 250;

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      public bool HasStaged()
      {
         using (var cloudEntities = new QoskCloud())
         {
            const string sqlQuery = "SELECT Count(*) " +
                                    "FROM Tracking " +
                                    "WHERE TrackingNumber=@trckNo " +
                                    "AND CarrierCode=@carrierCode " +
                                    "AND CAST(RcvdDate as date)=@rcvdDate " +
                                    "AND StgeDate IS NOT NULL " +
                                    "AND IsDeleted=0";


            var qqq = cloudEntities.Database.SqlQuery<int>(sqlQuery,
               new SqlParameter("@trckNo", this.TrackingNumber),
               new SqlParameter("@rcvdDate", this.ReceivingDate),
               new SqlParameter("@carrierCode", this.CarrierID)).FirstOrDefault();

            return qqq > 0;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = string.Empty;
         char[] spComma = { ',' };
         char[] spColon = { ':' };

         if (inStr != null)
         {
            foreach (var onePair in from oneField in inStr.Replace("\"", string.Empty).Split(spComma)
                                    select oneField.Split(spColon)
                                    into onePair
                                    let id = onePair[0].Trim()
                                    where id.Length > 0
                                    where id == chkStr
                                    select onePair)
            {
               return onePair[1].Trim();
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Carrier Name.
      /// </summary>
      /// <param name="carrierName"></param>
      /// <param name="carrierId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckCarrierReceiving(string carrierName, string carrierId)
      {
         var cloudEntities = new QoskCloud();

         var count = string.IsNullOrWhiteSpace(carrierId)
                              ? cloudEntities.CarrierDict.Count(t => t.Description == carrierName)
                              : cloudEntities.CarrierDict.Count(t => t.Description == carrierName && t.Code != carrierId);

         return count > 0 ? carrierName : null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Carrier code.
      /// </summary>
      /// <param name="carrierId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int CheckCarrierCode(string carrierId)
      {
         var chkCode = 0;

         try
         {
            var cloudEntities = new QoskCloud();
            var gCount = cloudEntities.CarrierDict.Count(t => t.Code == carrierId);

            if (gCount > 0)
            {
               chkCode = gCount;
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return chkCode;
      }

      ///****************************************************************************
      /// <summary>
      ///   add New receiving.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool NewReceiving(string curUser, string bodyData, bool isDeleted)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var receiving = new Tracking
               {
                  CreatedDate = DateTime.UtcNow,
                  RcvdDate = DateTime.UtcNow,
                  RcvdComment = GetOneValue(bodyData, "Notes").Trim(),
                  CarrierCode = GetOneValue(bodyData, "Carrier"),
                  RcvdPkgTypeCode = GetOneValue(bodyData, "PackageName"),
                  Version = 1,
                  IsDeleted = isDeleted,
                  CreatedBy = curUser,
                  RcvdBy = curUser
               };

               cloudEntities.Tracking.Add(receiving);
               cloudEntities.SaveChanges();
            }

            return true;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Edit receiving items.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="oldCarrierCode"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsRecvModel EditReceiving(string curUser, string oldCarrierCode, WrhsRecvModel bodyData, bool isDeleted)
      {
         var status = new WrhsRecvModel();
         var carrCode = bodyData.CarrierID;
         status.IsAllDeleted = false;

         using (var cloudEntities = new QoskCloud())
         {
            var dt = Convert.ToDateTime(bodyData.Dates);
            var existReceiving = (from rcv in cloudEntities.Tracking
                                  where rcv.CarrierCode == oldCarrierCode
                                  && rcv.RcvdDate == dt
                                  && rcv.IsDeleted == false
                                  select rcv).ToList();
            var notes = bodyData.Notes;
            var packageCode = bodyData.PackageCode;

            if (existReceiving != null && existReceiving.Any())
            {
               var date = bodyData.Dates;
               var newDate = Convert.ToDateTime(bodyData.Date);

               if (oldCarrierCode != carrCode || newDate != dt)
               {
                  var newCarrierExistItems = (from rcv in cloudEntities.Tracking
                                              where rcv.CarrierCode == carrCode
                                              && rcv.RcvdDate == newDate
                                              && rcv.IsDeleted == false
                                              select rcv).ToList();

                  if (newCarrierExistItems != null)
                  {
                     if (newCarrierExistItems.Any(c => c.StgeDate.HasValue))
                     {
                        notes = newCarrierExistItems.Select(c => c.RcvdComment).FirstOrDefault();
                        packageCode = newCarrierExistItems.Select(c => c.RcvdPkgTypeCode).FirstOrDefault();
                     }
                     else
                     {
                        newCarrierExistItems.ForEach(c =>
                        {
                           c.RcvdPkgTypeCode = packageCode;
                           c.RcvdComment = notes;
                        });
                     }
                  }
               }
               carrCode = existReceiving.Select(c => c.CarrierCode).FirstOrDefault();
               existReceiving.ForEach(c =>
               {
                  if (!existReceiving.Any(s => s.StgeDate.HasValue))
                  {
                     c.RcvdDate = newDate;
                     date = Convert.ToDateTime(bodyData.Date);
                     c.CarrierCode = bodyData.CarrierID;
                     carrCode = c.CarrierCode;
                  }
                  c.ModifiedBy = curUser;
                  c.RcvdComment = notes;
                  c.RcvdPkgTypeCode = packageCode;
               });

               try
               {
                  cloudEntities.SaveChanges();
                  status.IsAllDeleted = true;
                  status.Date = Convert.ToDateTime(date).ToString("MM/dd/yyyy");
                  status.CarrierID = carrCode;
               }
               catch (Exception ex)
               {
                  Log.WriteError(ex);
               }
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Received RA Status. We had to apply this logic in WrhsStagingModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsStagingModel.cs under the function SetRAReceivedStatus().
      /// </summary>
      /// <param name="func"></param>
      /// <param name="raNumber"></param>
      /// <param name="trackingId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SetRAReceivedStatus(string func, string raNumber, string trackingId, string curUser)
      {
         var status = false;

         using (var cloudEntities = new QoskCloud())
         {
            try
            {
               var raInfo = cloudEntities.ReturnAuthorization.FirstOrDefault(c => c.RtrnAuthID == raNumber);

               if (raInfo != null)
               {
                  var rthAuthIdHist = new RtrnAuthStatusHist();

                  if (raInfo.RtrnAuthStatusCode == "A" && func == "Add")
                  {
                     raInfo.RtrnAuthStatusCode = "R";
                     rthAuthIdHist.StatusCode = "R";
                  }
                  else if (raInfo.RtrnAuthStatusCode == "R" && func == "Delete")
                  {
                     if (cloudEntities.Database.SqlQuery<Tracking>("SELECT * FROM TRACKING WHERE AssocID = @ra AND TrackingID <> @trkId AND IsDeleted = 0",
                            new SqlParameter("@ra", raNumber), new SqlParameter("@trkId", trackingId)).FirstOrDefault() == null)
                     {
                        raInfo.RtrnAuthStatusCode = "A";
                        rthAuthIdHist.StatusCode = "A";
                     }
                  }

                  if (!string.IsNullOrWhiteSpace(rthAuthIdHist.StatusCode))
                  {
                     rthAuthIdHist.RtrnAuthID = raNumber;
                     rthAuthIdHist.CreatedDate = DateTime.UtcNow;
                     rthAuthIdHist.CreatedBy = curUser;
                     cloudEntities.RtrnAuthStatusHist.Add(rthAuthIdHist);
                  }

                  cloudEntities.SaveChanges();
                  status = true;
               }
            }
            catch (Exception ex)
            {
               Log.WriteError(ex);
            }
         }
         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Max TrackingId.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetMax(bool isDeleted)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (cloudEntities.Tracking.Where(c => c.IsDeleted == isDeleted).Select(c => c.TrackingID).LastOrDefault());
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Min Tracking Id.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetMin(bool isDeleted)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (cloudEntities.Tracking.Any(c => c.IsDeleted == isDeleted)
                           ? cloudEntities.Tracking.Where(c => c.IsDeleted == isDeleted).Select(c => c.TrackingID).FirstOrDefault()
                           : string.Empty);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Receiving Details.
      /// </summary>
      /// <param name="wrhsFltr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetDockDoorDetails(WrhsRecvModel wrhsFltr)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var condition = string.Empty; // where clauses depending on the filters applied
            var parameters = new List<SqlParameter>();

            //default parameters for infinite scroll
            parameters.AddRange(new List<SqlParameter>() {
               new SqlParameter("@previousCount", wrhsFltr.PreviousCount),
               new SqlParameter("@returnCount", wrhsFltr.PreviousCount == 0 ? wrhsFltr.InitialLoadCount : wrhsFltr.ScrollLoadCount)});

            DateTime dateValue;

            if (!string.IsNullOrWhiteSpace(wrhsFltr.Date)
                && !string.IsNullOrWhiteSpace(wrhsFltr.ModifiedDate)
                && DateTime.TryParse(wrhsFltr.Date, out dateValue)
                && DateTime.TryParse(wrhsFltr.ModifiedDate, out dateValue))
            {
               if (DateTime.Parse(wrhsFltr.Date) > DateTime.Parse(wrhsFltr.ModifiedDate))
               {
                  parameters.AddRange(new List<SqlParameter>{
                     new SqlParameter("@date", wrhsFltr.ModifiedDate),
                     new SqlParameter("@toDate",wrhsFltr.Date)
                  });
               }
               else
               {
                  parameters.AddRange(new List<SqlParameter>{
                     new SqlParameter("@date", wrhsFltr.Date),
                     new SqlParameter("@toDate",wrhsFltr.ModifiedDate)
                  });
               }

               condition += (string.IsNullOrWhiteSpace(condition) ? string.Empty : " and ") +
                            (" (convert(date, rcv.RcvdDate) between convert(date, @date) and convert(date, @toDate) ) ");
            }
            else if (!string.IsNullOrWhiteSpace(wrhsFltr.Date) || !string.IsNullOrWhiteSpace(wrhsFltr.ModifiedDate))
            {
               var fromDate = wrhsFltr.Date;
               var toDate = wrhsFltr.ModifiedDate;

               if (!DateTime.TryParse(wrhsFltr.ModifiedDate, out dateValue))
               {
                  parameters.Add(new SqlParameter("@date", fromDate));
                  condition += (string.IsNullOrWhiteSpace(condition) ? string.Empty : " and ") + (" rcv.RcvdDate >= @date ");
               }
               else if (!DateTime.TryParse(wrhsFltr.Date, out dateValue))
               {
                  parameters.Add(new SqlParameter("@date", toDate));
                  condition += (string.IsNullOrWhiteSpace(condition) ? string.Empty : " and ") + (" rcv.RcvdDate < DATEADD(day, 1, @date) ");
               }
            }
            else
            {
               condition = "rcv.RcvdDate >= DATEADD(day, -30, getdate())";
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.Carrier))
            {
               condition += " and carrier.Description like N'%'+@carrier+'%' ";
               parameters.Add(new SqlParameter("@carrier", wrhsFltr.Carrier));
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.TrackingNumber))
            {
               condition += " and exists (select * from Tracking where rcv.CarrierCode = CarrierCode and RcvdDate = rcv.RcvdDate and TrackingNumber like N'%'+@trackingNumber+'%' and IsDeleted = 0)";
               parameters.Add(new SqlParameter("@trackingNumber", wrhsFltr.TrackingNumber));
            }

            if (!string.IsNullOrWhiteSpace(wrhsFltr.RaOrWpt))
            {
               condition += " and exists (select * from Tracking where rcv.CarrierCode = CarrierCode and RcvdDate = rcv.RcvdDate and AssocID in (select RtrnAuthID from ReturnAuthorization where RtrnAuthNumber = @rId and IsDeleted = 0) and IsDeleted = 0)";
               parameters.Add(new SqlParameter("@rId", wrhsFltr.RaOrWpt));
            }

            var sqlQuery = $"SELECT carrier.Description as Carrier, rcv.CarrierCode as CarrierID, rcv.RcvdComment as Notes, convert(nvarchar, rcv.RcvdDate, 101) as Date, count(*) as Count, rcv.RcvdDate as ReceivingDate " +
                           $"FROM tracking rcv " +
                           $"JOIN CarrierDict carrier ON rcv.CarrierCode = carrier.Code " +
                           $"WHERE rcv.IsDeleted = 0 AND {condition} " +
                           $"GROUP BY carrier.Description, rcv.CarrierCode, rcv.RcvdComment, convert(nvarchar, rcv.RcvdDate, 101), rcv.RcvdDate " +
                           $"ORDER BY rcv.RcvdDate DESC " +
                           $"OFFSET @previousCount ROWS FETCH NEXT @returnCount ROWS ONLY";

            return cloudEntities.Database.SqlQuery<WrhsRecvModel>(sqlQuery, parameters.ToArray()).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of package type dict.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetPackageTypeDicts()
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from pkg in cloudEntities.WrhsPackageTypeDict
                    where pkg.Code != "UNKN"
                    select new WrhsRecvModel
                    {
                       PackageCode = pkg.Code,
                       PackageName = pkg.Description,
                    }).OrderByDescending(c => c.PackageName == "Corrugated Box").ThenBy(c => c.PackageName).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check staged record.
      /// </summary>
      /// <param name="carrierId"></param>
      /// <param name="date"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool hasStaged(string date, string carrierId)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var sqlQuery = "select top(1) convert(nvarchar, StgeDate,101) as Date from Tracking where StgeDate is not null " +
                           " and convert(nvarchar,RcvdDate,101)=@date and CarrierCode = @carrierId order by StgeDate desc";

            return cloudEntities.Database.SqlQuery<WrhsRecvModel>(sqlQuery,
                                                                     new SqlParameter("@date", date),
                                                                     new SqlParameter("@carrierId", carrierId)).Any();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get carrierList by input.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <param name="strData"></param>
      /// <param name="browser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetCarrierList(bool isDeleted, string strData, string browser)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var lstAllData = (from carrier in cloudEntities.CarrierDict
                                 orderby carrier.DisplayOrder
                                 select new WrhsRecvModel
                                 {
                                    CarrierID = carrier.Code,
                                    Carrier = carrier.Description
                                 }).ToList();
               var lstItems = new List<WrhsRecvModel>();

               if (!string.IsNullOrWhiteSpace(strData))
               {
                  foreach (var items in lstAllData)
                  {
                     var reslt = Regex.Replace(items.Carrier, @"[^a-zA-Z0-9]", string.Empty);

                     if (reslt.ToLower().Contains(strData.ToLower()))
                     {
                        lstItems.Add(items);
                     }
                  }
               }
               else
               {
                  lstItems.AddRange(lstAllData);
               }

               return browser.Contains("Edge") ? lstItems : lstItems.Take(40).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of tracking items in tracking tab.
      /// </summary>
      /// <param name="carrierCode"></param>
      /// <param name="trackingDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetTrackingPackages(string carrierCode, string trackingDate)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var sqlquery = "select " +
                              "trck.TrackingNumber as TrackingNumber, " +
                              "routes.Description as Route, " +
                              "convert(nvarchar, trck.WrhsRouteCode) as RouteNumber, " +
                              "isnull(convert(nvarchar, raTbl.RtrnAuthNumber),'') as RA, " +
                              "trck.TrackingTypeCode as TypeCode, " +
                              "trck.TrackingID as TrackingID, " +
                              "trck.CreatedDate as CreatedDate, " +
                              "raTbl.ExternRtrnAuthNumber as RaOrWpt, " +
                              "trck.CarrierCode as CarrierID, " +
                              "trck.RcvdDate as ReceivingDate, " +
                              "(case when trck.StgeDate is null then convert(bit, 0) else convert(bit, 1) end) as StagingDate, " +
                              //"case when exists (SELECT top 1 trackingID FROM Tracking WHERE TrackingNumber=trck.TrackingNumber " +
                              //"AND CarrierCode=trck.carrierCode AND CAST(RcvdDate as date)=CAST(trck.RcvdDate as date) " +
                              //"AND StgeDate IS NOT NULL AND IsDeleted=0) then convert(bit, 1) else convert(bit,0) end as HasStge " +
                              "convert(bit,0) as HasStge " +
                           "from " +
                              "tracking trck " +
                           "join " +
                              "wrhsroutedict routes on trck.WrhsRouteCode = routes.Code " +
                           "join " +
                              "TrackingTypeDict trackingType on trck.TrackingTypeCode = trackingType.Code " +
                           "left outer join " +
                              "ReturnAuthorization raTbl on trck.AssocID = raTbl.RtrnAuthID " +
                           "where " +
                              "trck.CarrierCode = @crId " +
                              "and convert(nvarchar, trck.RcvdDate, 101) = @dt " +
                              "and trck.IsDeleted = 0 " +
                           "order by " +
                              "trck.CreatedDate desc";

            var lst = cloudEntities.Database.SqlQuery<WrhsRecvModel>(sqlquery,
                                                       new SqlParameter("@crId", carrierCode),
                                                       new SqlParameter("@dt", trackingDate)).ToList();
            var lstItems = new List<WrhsRecvModel>();
            var trackingNumbers = lst.Select(c => c.TrackingNumber.ToUpper()).Distinct();
            ReOrderTrackingRecords(lst, lstItems, trackingNumbers);
            return lstItems;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="lst"></param>
      /// <param name="lstItems"></param>
      /// <param name="trackingNumbers"></param>
      ///****************************************************************************
      private static void ReOrderTrackingRecords(IEnumerable<WrhsRecvModel> lst, List<WrhsRecvModel> lstItems, IEnumerable<string> trackingNumbers)
      {
         foreach (var item in trackingNumbers)
         {
            var staged = lst.FirstOrDefault(i => i.TrackingNumber.ToUpper() == item.ToUpper() && i.StagingDate);
            lstItems.AddRange(lst.Where(i => i.TrackingNumber.ToUpper() == item.ToUpper()));

            //check if there exists at least one record with the tracking number
            //if so then set the entire tracking number grouping to be 'staged' i.e. unable to delete tracking number for the group
            if (staged != null)
            {
               lstItems.Where(i => i.TrackingNumber.ToUpper() == item.ToUpper()).ToList().ForEach(c => c.HasStge = true);
            }

         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Type of AssocID.
      /// </summary>
      /// <param name="db"></param>
      /// <param name="RAType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetRATypeName(string RAType, QoskCloud db)
      {
         return db.TrackingTypeDict.Where(c => c.Code == RAType).Select(c => c.Description).FirstOrDefault();
      }

      ///****************************************************************************
      /// <summary>
      ///   Check validate RA number. We had to apply this logic in WrhsStagingModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsStagingModel.cs under the function CheckRANumber().
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsRecvModel CheckRANumber(string raNumber)
      {
         var routeValue = new WrhsRecvModel();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var dictWhrsRoute = cloudEntities.Database.SqlQuery<WrhsRouteDict>("SELECT * FROM WrhsRouteDict").ToList();
               var routeNumber = 100;

               var sqlBase = "SELECT * FROM ReturnAuthorization WHERE";
               var sqlA = $"{sqlBase} RtrnAuthNumber = {raNumber}";
               var sqlB = $"{sqlBase} ExternRtrnAuthNumber = '{raNumber}'";

               //isNaN will return 0 if it's a non-numeric RA
               var isNaN = new BigInteger();
               BigInteger.TryParse(raNumber, out isNaN);

               var rtrnAuthItem = cloudEntities.Database.SqlQuery<ReturnAuthorization>(isNaN != 0 ? sqlA : sqlB).FirstOrDefault();

               if (rtrnAuthItem == null && isNaN != 0)
               {
                  rtrnAuthItem = cloudEntities.Database.SqlQuery<ReturnAuthorization>(sqlB).FirstOrDefault();
               }

               if (rtrnAuthItem != null)
               {
                  //"closed" RAs should go to their respective Island location
                  var raStatusDict = cloudEntities.RtrnAuthStatusDict.Where(c => c.Code == rtrnAuthItem.RtrnAuthStatusCode).Select(c => c.Classification).FirstOrDefault();
                  
                  if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL2"))
                  {
                     routeNumber = raStatusDict == "C" ? 96 : 2;
                  }
                  else if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL345"))
                  {
                     routeNumber = raStatusDict == "C" ? 97 : 3;
                  }
                  else if (rtrnAuthItem.RtrnAuthCtrlCode.Contains("CTRL0"))
                  {
                     routeNumber = raStatusDict == "C" ? 99 : 0;

                     //determine if the non-control, open RA comes from a manufacturer that requires Special Handling; send to SH route if it does (should work just like Floor)
                     if (routeNumber == 0)
                     {
                        var specialHandling = (from dm in cloudEntities.DebitMemo
                           join p in cloudEntities.Profile on dm.MfrProfileCode equals p.ProfileCode
                           where dm.DebitMemoID == rtrnAuthItem.DebitMemoID
                           select p.SpecialHandling).FirstOrDefault();

                        routeNumber = specialHandling ? 6 : 0;
                     }
                  }
               }

               routeValue = dictWhrsRoute.Where(c => c.Code == routeNumber).Select(c => new WrhsRecvModel
               {
                  RouteNumber = c.Code.ToString(),
                  Route = c.Description,
                  RA = rtrnAuthItem == null
                     ? string.Empty
                     : rtrnAuthItem.RtrnAuthID
               }).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return routeValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get route Mp3 files from blob.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetAllUrl()
      {
         using (var cloudEntities = new QoskCloud())
         {
            var lst = new List<WrhsRecvModel>();
            var lstRoutes = cloudEntities.WrhsRouteDict.Where(c => c.Alert != string.Empty).Select(c => new WrhsRecvModel { Alert = c.Alert, Route = c.Code.ToString() }).ToList();

            foreach (var items in lstRoutes)
            {
               if (!string.IsNullOrWhiteSpace(items.Alert))
               {
                  lst.Add(new WrhsRecvModel()
                  {
                     Alert = string.IsNullOrWhiteSpace(items.Alert) ? string.Empty : GetSoundFileAddress(items.Alert),
                     Route = items.Route
                  });
               }
            }

            return lst;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check validate RA number.
      /// </summary>
      /// <param name="fileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetSoundFileAddress(string fileName)
      {
         var items = GetFolderItemsListSASUrlBasedOnSearchStaging(Utility.Constants.AzureAccount_KioskSync, "mp3", fileName);
         return items.ContentSasURI == null ? string.Empty : items.ContentSasURI.ToString();
      }

      ///****************************************************************************
      /// <summary>
      /// Generates SasURI for all the blobs searched by.
      /// </summary>
      /// <param name="account">The Account (without DV)</param>
      /// <param name="containerName">The Container</param>
      /// <param name="fileName">filename</param>
      /// <param name="access">Read or Write</param>
      /// <param name="timeout">Timeout in days</param>
      /// <returns></returns>
      ///****************************************************************************
      public static SasUriModel GetFolderItemsListSASUrlBasedOnSearchStaging(string account, string containerName, string fileName, string access = "Read", int timeoutInDays = 10)
      {
         var sasBlobToken = string.Empty;

         var connectString = ConfigurationManager.GetConnectionString(account);
         var storageAccount = CloudStorageAccount.Parse(connectString);

         var blobClient = storageAccount.CreateCloudBlobClient();
         var serviceProperties = blobClient.GetServiceProperties();

         serviceProperties.Cors.CorsRules.Clear();

         serviceProperties.Cors.CorsRules.Add(new CorsRule()
         {
            AllowedHeaders = { Utility.Constants.Triple_Dot },
            AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
            AllowedOrigins = { Utility.Constants.Triple_Dot },
            ExposedHeaders = { Utility.Constants.Triple_Dot },
            MaxAgeInSeconds = 600,

         });

         blobClient.SetServiceProperties(serviceProperties);

         var sampleContainer = blobClient.GetContainerReference(containerName.ToLower());
         var container = sampleContainer.GetBlockBlobReference(fileName);

         var sasConstraints = new SharedAccessBlobPolicy
         {
            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
            SharedAccessExpiryTime = DateTime.UtcNow.AddDays(timeoutInDays),
            Permissions = access.ToLower() == Utility.Constants.FileAccess_Methods_Write ? SharedAccessBlobPermissions.Read |
                 SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List :
                 SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
         };

         //Get list of files containing in this directory
         //CloudBlobDirectory blobDirectory = container.GetDirectoryReference(ItemGuid);

         var listOfBlobs = container;// blobDirectory.ListBlobs();
         SasUriModel SearchedBlobUri = new SasUriModel();
         SasUriModel sasUriModel = null;

         if (listOfBlobs != null && listOfBlobs.Uri.AbsoluteUri.ToLower().Contains(fileName.ToLower()))
         {
            sasUriModel = new SasUriModel();
            CloudBlockBlob blob = (CloudBlockBlob)listOfBlobs;
            sasBlobToken = blob.Uri + blob.GetSharedAccessSignature(sasConstraints);
            sasUriModel.ContentSasURI = sasBlobToken;
            sasUriModel.ContentName = blob.Name;
            var ex = Path.GetExtension(blob.Name).ToUpper();
            sasUriModel.ContentType = "File";
            SearchedBlobUri = sasUriModel;
         }

         return SearchedBlobUri;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check validate WPT number.
      /// </summary>
      /// <param name="raNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static object CheckWPTNumber(string raNumber)
      {
         var routeValue = new object();
         using (var cloudEntities = new QoskCloud())
         {
            long ra = string.IsNullOrWhiteSpace(raNumber) ? 0 : Convert.ToInt64(raNumber);

            if (cloudEntities.ReturnAuthorization.Any(c => c.RtrnAuthNumber == ra))
            {
               var lst = cloudEntities.ReturnAuthorization.Where(c => c.RtrnAuthID == raNumber).Select(c => c.RtrnAuthCtrlCode).Distinct().ToList();

               if (lst.Count > 0)
               {
                  if (lst.Contains("2"))
                  {
                     routeValue = cloudEntities.WrhsRouteDict.Where(c => c.Code == 2).Select(c => new { Name = c.Code + "," + c.Description }).ToList();
                  }
                  else if (lst.Contains("3") || lst.Contains("4") || lst.Contains("5"))
                  {
                     routeValue = cloudEntities.WrhsRouteDict.Where(c => c.Code == 3).Select(c => new { Name = c.Code + "," + c.Description }).ToList();
                  }
                  else if (lst.Contains("0"))
                  {
                     routeValue = cloudEntities.WrhsRouteDict.Where(c => c.Code == 0).Select(c => new { Name = c.Code + "," + c.Description }).ToList();
                  }
               }
            }

            return routeValue;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Edit receiving details form.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="entities"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsRecvModel EditReceiving(WrhsRecvModel entities, string curUser)
      {
         var raInfo = new WrhsRecvModel();

         try
         {
            if (NonRATypeCodes.Contains(entities.TypeCode))
            {
               raInfo.RouteNumber = entities.TypeCode == "NR"
                  ? "99"
                  : "98";
               raInfo.RA = string.Empty;
               raInfo.Route = "Island";
            }
            else
            {
               raInfo = CheckRANumber(entities.RaOrWpt);

               if (raInfo.RouteNumber == "100")
               {
                  return raInfo;
               }

            }

            using (var cloudEntities = new QoskCloud())
            {
               var existEntity = cloudEntities.Tracking.SingleOrDefault(c => c.TrackingID == entities.TrackingID && c.IsDeleted == false);

               if (existEntity != null)
               {
                  var oldRAID = existEntity.AssocID;
                  var oldTrck = existEntity.TrackingNumber;

                  existEntity.AssocID = raInfo.RA;
                  existEntity.TrackingTypeCode = entities.TypeCode;
                  existEntity.WrhsRouteCode = (byte)(raInfo.RouteNumber == "0" || string.IsNullOrWhiteSpace(raInfo.RouteNumber) ? 0 : Convert.ToInt32(raInfo.RouteNumber));
                  existEntity.ModifiedBy = curUser;

                  cloudEntities.SaveChanges();

                  if (oldRAID != raInfo.RA && !NonRATypeCodes.Contains(entities.TypeCode))
                  {
                     var result = string.IsNullOrEmpty(oldRAID)
                        ? SetRAReceivedStatus("Add", raInfo.RA, existEntity.TrackingID, curUser)
                        : SetRAReceivedStatus("Delete", oldRAID, existEntity.TrackingID, curUser);
                  }

                  TrackingModel.UpdateTrackingNumber(oldTrck, entities.TrackingNumber, entities.CarrierID, Convert.ToDateTime(entities.Date));
               }
            }

            raInfo.TrackingID = entities.TrackingID;
            raInfo.TrackingNumber = entities.TrackingNumber;
            raInfo.RaOrWpt = entities.RaOrWpt;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
            return new WrhsRecvModel();
         }

         return raInfo;
      }

      ///****************************************************************************
      /// <summary>
      ///   save tracking items information. We had to apply this logic in WrhsStagingModel.cs because there are cases in which users only have Staging but not Receiving access.
      ///   Therefore, any changes made here must also be made in WrhsStagingModel.cs under the function SaveTracking().
      /// </summary>
      /// <param name="entities"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static WrhsRecvModel SaveTracking(WrhsRecvModel entities, string curUser)
      {
         var raInfo = new WrhsRecvModel();

         try
         {
            if (NonRATypeCodes.Contains(entities.TypeCode))
            {
               raInfo.RouteNumber = entities.TypeCode == "NR"
                  ? "99"
                  : "98";
               raInfo.Route = "Island";
               raInfo.RA = string.Empty;
            }
            else
            {
               raInfo = CheckRANumber(entities.RaOrWpt);

               if (raInfo.RouteNumber == "100")
               {
                  return raInfo;
               }
            }

            var trackingID = Methods.NextId();

            new Thread(() =>
            {
               try
               {
                  using (var cloudEntities = new QoskCloud())
                  {
                     var tracks = new Tracking
                     {
                        TrackingID = trackingID,
                        TrackingNumber = entities.TrackingNumber,
                        AssocID = raInfo.RA,
                        WrhsRouteCode = (byte)Convert.ToInt32(raInfo.RouteNumber),
                        TrackingTypeCode = entities.TypeCode,
                        RcvdBy = curUser,
                        RcvdPkgTypeCode = entities.PackageCode,
                        RcvdComment = entities.Notes,
                        CarrierCode = entities.CarrierID,
                        RcvdDate = Convert.ToDateTime(entities.Date),
                        CreatedDate = DateTime.UtcNow,
                        IsDeleted = false,
                        Version = 1,
                        CreatedBy = curUser
                     };

                     cloudEntities.Tracking.Add(tracks);
                     cloudEntities.SaveChanges();

                     if (entities.IsDeleted)
                     {
                        cloudEntities.Database.ExecuteSqlCommand("UPDATE Tracking SET RcvdComment=@note WHERE CarrierCode=@crId AND RcvdDate=@rcvdDate",
                                                                  new SqlParameter("@crId", entities.CarrierID),
                                                                   new SqlParameter("@rcvdDate", Convert.ToDateTime(entities.Date)),
                                                                    new SqlParameter("@note", entities.Notes));
                     }

                     if (!NonRATypeCodes.Contains(entities.TypeCode))
                     {
                        SetRAReceivedStatus("Add", raInfo.RA, trackingID, curUser);
                     }
                  }
               }
               catch (Exception ex)
               {
                  Log.WriteError(ex);
               }
            }).Start();

            raInfo.TrackingID = trackingID;
            raInfo.RaOrWpt = entities.RaOrWpt;
            raInfo.TrackingNumber = entities.TrackingNumber;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
            return new WrhsRecvModel();
         }

         return raInfo;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get tracking NextId.
      /// </summary>
      /// <param name="dbModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetTrackingNewID(ref DBModel dbModel)
      {
         try
         {
            var trackingObj = new NextID(ref dbModel);
            return trackingObj.ID;
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return string.Empty;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Carrier List .
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<WrhsRecvModel> GetCarrierList()
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from carrier in cloudEntities.CarrierDict
                    orderby carrier.DisplayOrder
                    select new WrhsRecvModel
                    {
                       Carrier = carrier.Description,
                       CarrierID = carrier.Code
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete Item by trackingId.
      /// </summary>
      /// <param name="trckId"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool DeleteItem(List<string> trckList, string curUser)
      {
         var status = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               foreach (var trckId in trckList)
               {
                  var sqlQuery = "SELECT * FROM TRACKING t WHERE t.TrackingID = @trkId AND t.StgeDate IS NULL AND t.IsDeleted = 0";
                  var item = cloudEntities.Database.SqlQuery<Tracking>(sqlQuery, new SqlParameter("@trkId", trckId)).SingleOrDefault();

                  if (item != null)
                  {
                     cloudEntities.Database.ExecuteSqlCommand("UPDATE Tracking SET IsDeleted = 1 WHERE TrackingID = @trkId", new SqlParameter("@trkId", item.TrackingID));
                     cloudEntities.SaveChanges();

                     if (!NonRATypeCodes.Contains(item.TrackingTypeCode))
                     {
                        CheckAssociation(item.TrackingID, item.AssocID);
                        SetRAReceivedStatus("Delete", item.AssocID, item.TrackingID, curUser);
                     }
                  }
               }

               status = true;
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Association.
      /// </summary>
      /// <param name="trackingId"></param>
      /// <param name="assocId"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void CheckAssociation(string trackingId, string assocId)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var wrhsCntr = cloudEntities.WrhsContrTrackingRel.Where(c => c.TrackingID == trackingId).ToList();
               if (wrhsCntr.Count > 0)
               {
                  foreach (var cntr in wrhsCntr)
                  {
                     var hist = cloudEntities.WrhsContrStatusHist
                                 .Where(c => c.WrhsInstID == cntr.WrhsInstID
                                       && c.WrhsContrID == cntr.WrhsContrID).OrderByDescending(c => c.CreatedDate).FirstOrDefault();
                     if (hist.StatusCode == "S")
                     {
                        hist.StatusCode = "A";
                        cloudEntities.SaveChanges();
                     }
                  }
               }

               var trckItems = cloudEntities.Tracking.Where(c => c.AssocID == assocId && c.TrackingID != trackingId).Select(c => c.TrackingID).ToList();

               var attachmentFiles = (from attachement in cloudEntities.TrackingAttachment
                                      where attachement.TrackingID == trackingId
                                      || (attachement.TrackingID != trackingId
                                      && trckItems.Contains(attachement.TrackingID)
                                      && attachement.IsShared == true
                                      )
                                      select new
                                      {
                                         Code = attachement.SeqNo.ToString(),
                                         Name = attachement.FileName,
                                         IsShared = attachement.IsShared == true ? 1 : 0,
                                         TrackingID = attachement.TrackingID,
                                         SeqNo = attachement.SeqNo
                                      }).ToList();

               if (attachmentFiles.Count > 0)
               {
                  foreach (var attchItems in attachmentFiles)
                  {
                     var attchEntity = cloudEntities.TrackingAttachment.SingleOrDefault(c => c.TrackingID == attchItems.TrackingID
                                                                                                   && c.FileName == attchItems.Name
                                                                                                   && c.SeqNo == attchItems.SeqNo);
                     cloudEntities.TrackingAttachment.Remove(attchEntity);
                     DeleteFilesFromBlob(Utility.Constants.AzureContainer_AttachedFiles, attchItems.Name, Utility.Constants.AzureAccount_KioskSync);
                     cloudEntities.SaveChanges();
                  }
               }
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete files from Blob.
      /// </summary>
      /// <param name="accountName"></param>
      /// <param name="container"></param>
      /// <param name="fileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static void DeleteFilesFromBlob(string container, string fileName, string accountName)
      {
         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {
            var newPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
                          @"/ProductImages/AttachedFiles/" + fileName;

            if (File.Exists(newPath))
            {
               File.Delete(newPath);
            }
         }
         else
         {
            var connectString = GetConnectionString(accountName);

            var account = CloudStorageAccount.Parse(connectString);
            var client = account.CreateCloudBlobClient();
            var sampleContainer = client.GetContainerReference(container.ToLower());

            sampleContainer.CreateIfNotExists();

            sampleContainer.SetPermissions(new BlobContainerPermissions()
            {
               PublicAccess = BlobContainerPublicAccessType.Container
            });

            var blob = sampleContainer.GetBlockBlobReference(fileName);
            blob.DeleteIfExistsAsync();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get connection string.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetConnectionString(string account)
      {
         var connectString = ConfigurationManager.GetConnectionString(account);

         if (string.IsNullOrEmpty(connectString))
         {
            throw new ApplicationException(string.Format(Utility.Constants.Azure_ConnectionString_NotDefined_Format, account));
         }

         return connectString;
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete list of items that associated with tracking number.
      /// </summary>
      /// <param name="trackingNumbers"></param>
      /// <param name="carrierId"></param>
      /// <param name="curUser"></param>
      /// <param name="date"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool DeleteItems(List<string> trackingNumbers, string carrierId, string date, string curUser)
      {
         var status = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               foreach (var trckNumber in trackingNumbers)
               {
                  var rcvdDate = Convert.ToDateTime(date);
                  var parameters = new List<SqlParameter>();
                  parameters.AddRange(new List<SqlParameter>()
                  {
                     new SqlParameter("@trckNum", trckNumber),
                     new SqlParameter("@date", rcvdDate),
                     new SqlParameter("@carrierCode", carrierId)
                  });

                  var sqlQuery = "SELECT * FROM TRACKING t WHERE t.TrackingNumber = @trckNum AND CONVERT(DATE, t.RcvdDate) = CONVERT(DATE, @date) " +
                                 "AND t.CarrierCode = @carrierCode AND t.StgeDate IS NULL AND t.IsDeleted = 0";
                  var items = cloudEntities.Database.SqlQuery<Tracking>(sqlQuery, parameters.ToArray()).ToList();

                  foreach (var item in items)
                  {
                     cloudEntities.Database.ExecuteSqlCommand("UPDATE Tracking SET IsDeleted = 1 WHERE TrackingID = @trkId", new SqlParameter("@trkId", item.TrackingID));
                  }

                  cloudEntities.SaveChanges();

                  foreach (var dltItems in items)
                  {
                     if (!NonRATypeCodes.Contains(dltItems.TrackingTypeCode))
                     {
                        CheckAssociation(dltItems.TrackingID, dltItems.AssocID);
                        SetRAReceivedStatus("Delete", dltItems.AssocID, dltItems.TrackingID, curUser);
                     }
                  }
               }
               status = true;
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Defines the 3Dot menu for the Actionbar based upon application
      ///   requirements.
      /// </summary>
      /// <returns>
      ///   Menu definition (empty definition if not supported).
      /// </returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         // This application does not support the 3Dot menu on the Actionbar
         return new ActionMenuModel();
      }

      #endregion
   }
}
