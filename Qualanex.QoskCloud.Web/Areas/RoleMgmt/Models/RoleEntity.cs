///-----------------------------------------------------------------------------------------------
/// <copyright file="RoleEntity.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------
///
namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models
{
   using System;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System.Data.Entity.Validation;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;

   using Telerik.Reporting.Barcodes;
   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class RoleEntity
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      public RoleEntity()
      {
      }

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Role ID")]
      public string RoleId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Role Name")]
      public string RoleName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Group Name")]
      public string GroupName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Group ID")]
      public int GroupId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Status")]
      public string Status { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Description")]
      public string Description { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Group Type")]
      public string GroupType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Display Format")]
      public string DisplayFormat { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Seq No")]
      public int SeqNo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"IsDeleted")]
      public string IsDeleted { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Code")]
      public string Code { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Application")]
      public string AppCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Tab")]
      public string TabCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Segment")]
      public string SegCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Function")]
      public string FuncCode { get; set; }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ;  // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ;   // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupCode"></param>
      /// <param name="roleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetAppDictLists(int groupCode, int roleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   var items = cloudEntities.PrivsAppRel
         //      .Where(c => c.APrivilegesID == groupCode && c.IsDeleted == isEnable && c.Privileges == "Group")
         //      .Select(c => c.AppCode).Distinct().ToList();
         //   var query = cloudEntities.AppDict.Where(c => items.Contains(c.Code)).Select(c => new { c.Code, c.Description }).ToList();
         //   var selectedItems = new List<SelectListItem>
         //   {
         //      new SelectListItem
         //      {
         //         Text = "---- Add New Application ----",
         //         Value = "Add", Disabled = true
         //      }
         //   };
         //   var roleItems = cloudEntities.PrivsAppRel
         //      .Where(c => c.PrivilegesID == roleCode && c.IsDeleted == isEnable && c.Privileges == "Role")
         //      .Select(c => c.AppCode).ToList();
         //   selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
         //   selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
         //   selectedItems.Where(c => roleItems.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

         //   return selectedItems;
         //}

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupCode"></param>
      /// <param name="roleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetTabDictLists(int groupCode, int roleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{

         //   var items = cloudEntities.PrivsTabRel.Where(c => c.IsDeleted == isEnable
         //   && c.PrivilegesID == groupCode && c.Privileges == "Group")
         //   .Select(c => c.TabCode)
         //   .Distinct().ToList();
         //   var query = cloudEntities.TabDict.Where(c => items.Contains(c.Code)).Select(c => new { c.Code, c.Description }).ToList();
         //   var selectedItems = new List<SelectListItem>
         //   {
         //      new SelectListItem
         //      {
         //         Text = "----- Add New Tab -----",
         //         Value = "Add", Disabled = true
         //      }
         //   };
         //   var roleItems = cloudEntities.PrivsTabRel
         //     .Where(c => c.PrivilegesID == roleCode && c.IsDeleted == isEnable && c.Privileges == "Role")
         //     .Select(c => c.TabCode).ToList();
         //   selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
         //   selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
         //   selectedItems.Where(c => roleItems.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

         //   return selectedItems;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get options for tab which selected by group
      /// </summary>
      /// <param name="groupCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetTabOptions(int groupCode)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   var optionItems = cloudEntities.PrivsTabRel.Where(c => c.IsDeleted == false
         //   && c.PrivilegesID == groupCode && c.Privileges == "Group")
         //   .Distinct().ToList();

         //   var selectedItems = new List<RoleEntity>
         //   {

         //   };
         //   foreach (var items in optionItems)
         //   {
         //      selectedItems.Add(new RoleEntity() { AppCode = items.AppCode, TabCode = items.TabCode });

         //   }
         //   return selectedItems;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get options for segment which selected by group
      /// </summary>
      /// <param name="groupCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetSegmentOptions(int groupCode)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   var optionItems = cloudEntities.PrivsSegRel.Where(c => c.IsDeleted == false
         //   && c.PrivilegesID == groupCode && c.Privileges == "Group")
         //   .Distinct().ToList();

         //   var selectedItems = new List<RoleEntity>();
         //   foreach (var items in optionItems)
         //   {
         //      selectedItems.Add(new RoleEntity() { AppCode = items.AppCode, TabCode = items.TabCode, SegCode = items.SegCode });


         //   }
         //   return selectedItems;
         //}
         return null;
      }
      ///****************************************************************************
      /// <summary>
      ///   Get options for function which selected by group
      /// </summary>
      /// <param name="groupCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetFunctionOptions(int groupCode)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   var optionItems = cloudEntities.PrivsFuncRel.Where(c => c.IsDeleted == false
         //   && c.PrivilegesID == groupCode && c.Privileges == "Group")
         //   .Distinct().ToList();

         //   var selectedItems = new List<RoleEntity>();
         //   foreach (var items in optionItems)
         //   {
         //      selectedItems.Add(new RoleEntity() { AppCode = items.AppCode, FuncCode = items.FuncCode, SegCode = items.SegCode });
         //   }
         //   return selectedItems;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupCode"></param>
      /// <param name="roleCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetSegmentDictLists(int groupCode, int roleCode)
      {
         //using (var cloudEntities = new QoskCloud())
         //{

         //   var items = cloudEntities.PrivsSegRel.Where(c => c.PrivilegesID == groupCode && c.Privileges == "Group" && c.IsDeleted == false)
         // .Select(c => c.SegCode).Distinct()
         // .ToList();
         //   var query = cloudEntities.SegmentDict.Where(c => items.Contains(c.Code)).Select(c => new { c.Code, c.Description }).ToList();
         //   var selectedItems = new List<SelectListItem>
         //   {
         //      new SelectListItem
         //      {
         //         Text = "---- Add New Segment ----",
         //         Value = "Add", Disabled = true
         //      }
         //   };

         //   var roleItems = cloudEntities.PrivsSegRel
         //    .Where(c => c.PrivilegesID == roleCode && c.IsDeleted == false && c.Privileges == "Role")
         //    .Select(c => c.SegCode).ToList();
         //   selectedItems.AddRange(query.Select(item => new SelectListItem { Value = item.Code, Text = item.Description }));
         //   selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
         //   selectedItems.Where(c => roleItems.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

         //   return selectedItems;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetRoleMgmtSeqEntity(bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from RoleConfig in cloudEntities.SegmentDict
                    where RoleConfig.IsDeleted == isEnable
                    select new RoleEntity()
                    {
                       AppCode = RoleConfig.Code,
                       Description = RoleConfig.Description,
                       DisplayFormat = RoleConfig.DisplayFormat
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupCode"></param>
      /// <param name="roleCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<SelectListItem> GetFunctionDictsLists(int groupCode, int roleCode)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   var items = cloudEntities.PrivsFuncRel
         //      .Where(c => c.PrivilegesID == groupCode && c.Privileges == "Group" && c.IsDeleted == false)
         //      .Select(c => c.FuncCode).Distinct()
         //      .ToList();
         //   var query = cloudEntities.FunctionDict.Where(c => items.Contains(c.Code)).Select(c => new { c.Code, c.Description }).ToList();
         //   var selectedItems = new List<SelectListItem>
         //   {
         //      new SelectListItem
         //      {
         //         Text = "---- Add New Function ----",
         //         Value = "Add",Disabled = true
         //      }
         //   };
         //   var roleItems = cloudEntities.PrivsFuncRel
         //  .Where(c => c.PrivilegesID == roleCode && c.IsDeleted == false && c.Privileges == "Role")
         //  .Select(c => c.FuncCode).ToList();
         //   selectedItems.AddRange(query.Select(i => new SelectListItem() { Value = i.Code, Text = i.Description }));
         //   selectedItems.Add(new SelectListItem { Text = "-------- Delete --------", Value = "Delete" });
         //   selectedItems.Where(c => roleItems.Contains(c.Value)).ToList().ForEach(c => c.Disabled = true);

         //   return selectedItems;
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetRoleMgmtFunctionEntity(bool isEnable)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from RoleConfig in cloudEntities.FunctionDict
                    where RoleConfig.IsDeleted == isEnable
                    select new RoleEntity()
                    {
                       AppCode = RoleConfig.Code,
                       Description = RoleConfig.Description
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetTabList(int roleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   return (from RoleConfig in cloudEntities.PrivsTabRel
         //           where RoleConfig.IsDeleted == isEnable && RoleConfig.PrivilegesID == roleCode
         //           && RoleConfig.Privileges == "Role"
         //           select new RoleEntity()
         //           {
         //              RoleId = roleCode.ToString(),
         //              Code = RoleConfig.TabCode,
         //              TabCode = RoleConfig.TabCode,
         //              AppCode = RoleConfig.AppCode
         //           }).ToList();
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="RoleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetAppList(int RoleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   return (from RoleConfig in cloudEntities.PrivsAppRel
         //           where RoleConfig.IsDeleted == isEnable && RoleConfig.PrivilegesID == RoleCode && RoleConfig.Privileges == "Role"
         //           //orderby RoleConfig.SeqNo
         //           select new RoleEntity()
         //           {
         //              Code = RoleConfig.AppCode,
         //              RoleId = RoleCode.ToString(),
         //              //SeqNo = RoleConfig.SeqNo,
         //              AppCode = RoleConfig.AppCode
         //           }).ToList();
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetSegmentList(int roleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   return (from RoleConfig in cloudEntities.PrivsSegRel
         //           where RoleConfig.IsDeleted == isEnable && RoleConfig.PrivilegesID == roleCode
         //           && RoleConfig.Privileges == "Role"
         //           select new RoleEntity()
         //           {
         //              RoleId = roleCode.ToString(),
         //              Code = RoleConfig.SegCode,
         //              IsDeleted = RoleConfig.IsDeleted.ToString(),
         //              SegCode = RoleConfig.SegCode,
         //              TabCode = RoleConfig.TabCode,
         //              AppCode = RoleConfig.AppCode
         //           }).ToList();
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleCode"></param>
      /// <param name="isEnable"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetFunctionList(int roleCode, bool isEnable)
      {
         //using (var cloudEntities = new QoskCloud())
         //{
         //   return (from RoleConfig in cloudEntities.PrivsFuncRel
         //           where RoleConfig.IsDeleted == isEnable && RoleConfig.PrivilegesID == roleCode
         //           && RoleConfig.Privileges == "Role"
         //           select new RoleEntity
         //           {
         //              RoleId = roleCode.ToString(),
         //              Code = RoleConfig.FuncCode,
         //              SegCode = RoleConfig.SegCode,
         //              AppCode = RoleConfig.AppCode,
         //              FuncCode = RoleConfig.FuncCode,
         //              IsDeleted = RoleConfig.IsDeleted.ToString()
         //           }).ToList();
         //}
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>    
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetGroupList(bool isDeleted, string strData, string browser)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return browser.Contains("Edge")
               ? (from groups in cloudEntities.Group
                  join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                  orderby groups.GroupID
                  where (groups.IsDeleted == isDeleted) && groups.Name.Contains(strData)
                  select new RoleEntity
                  {
                     GroupId = groups.GroupID,
                     GroupName = groups.Name,
                     GroupType = groupTypeDict.Description

                  }).ToList()
               : (from groups in cloudEntities.Group
                  join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                  orderby groups.GroupID
                  where (groups.IsDeleted == isDeleted) && groups.Name.Contains(strData)
                  select new RoleEntity
                  {
                     GroupId = groups.GroupID,
                     GroupName = groups.Name,
                     GroupType = groupTypeDict.Description

                  }).Take(40).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RoleEntity> GetRoleDetails(bool isDeleted)
      {
         using (var cloudEntities = new QoskCloud())
         {
            return (from RoleConfig in cloudEntities.QnexRole
                    join groups in cloudEntities.Group on RoleConfig.GroupID equals groups.GroupID
                    join groupTypeDict in cloudEntities.GroupTypeDict on groups.GrpTypeCode equals groupTypeDict.Code
                    where (RoleConfig.IsDeleted == isDeleted || RoleConfig.IsDeleted == false)
                    select new RoleEntity
                    {
                       GroupId = (int)RoleConfig.GroupID,
                       GroupName = groups.Name,
                       GroupType = groupTypeDict.Description,
                       RoleId = RoleConfig.RoleID.ToString(),
                       Status = (bool)RoleConfig.IsDeleted
                                    ? "Retired"
                                    : "Enabled",
                       RoleName = RoleConfig.Name,
                       Description = RoleConfig.Description
                    }).ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="inStr"></param>
      /// <param name="chkStr"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetOneValue(string inStr, string chkStr)
      {
         var outValue = "";
         char[] spComma = { ',' };
         char[] spColon = { ':' };

         if (inStr != null)
         {
            foreach (var onePair in from oneField in inStr.Replace("\"", "").Split(spComma)
                                    select oneField.Split(spColon)
                                    into onePair
                                    let id = onePair[0].Trim()
                                    where id.Length > 0
                                    where id == chkStr
                                    select onePair)
            {
               return onePair[1].Trim();
            }
         }

         return outValue;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleName"></param>
      /// /// <param name="roleId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckRoleName(string roleName, int roleId)
      {
         var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

         var count = roleId == 0
                           ? cloudEntities.QnexRole.Count(t => t.Name == roleName)
                           : cloudEntities.QnexRole.Count(t => t.Name == roleName && t.RoleID != roleId);

         return count > 0
                     ? roleName
                     : null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int CheckGroupCode(string groupCode)
      {
         var chkCode = 0;

         try
         {
            var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud();

            chkCode = groupCode == "0"
                              ? 0
                              : Convert.ToInt32(groupCode);

            var gCount = cloudEntities.Group.Count(t => t.GroupID == chkCode);

            if (gCount > 0)
            {
               chkCode = gCount;
            }
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }

         return chkCode;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool NewRole(string curUser, string bodyData, bool isDeleted)
      {
         var status = false;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            try
            {
               var role = new QnexRole
               {
                  Name = GetOneValue(bodyData, "ID_ROLENAME").ToString().Trim(),
                  GroupID = Convert.ToInt32(GetOneValue(bodyData, "ID_GROUPCODE").ToString().Trim()),
                  Version = 1,
                  IsDeleted = GetOneValue(bodyData, "ID_STATUS") != "0",
                  EffectiveStartDate = DateTime.UtcNow,
                  EffectiveEndDate = DateTime.UtcNow.AddDays(90),
                  CreatedBy = curUser,
                  Description = GetOneValue(bodyData, "ID_DESCRIPTION").ToString().Trim()
               };

               cloudEntities.QnexRole.Add(role);
               cloudEntities.SaveChanges();
               status = true;
            }
            catch (Exception ex)
            {
               Utility.Logger.Error(ex);
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="curUser"></param>
      /// <param name="groupId"></param>
      /// <param name="bodyData"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool EditRole(string curUser, int groupId, string bodyData, bool isDeleted)
      {
         var status = false;

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var existRole = (from roles in cloudEntities.QnexRole
                             where roles.GroupID == groupId
                             select roles).SingleOrDefault();

            if (existRole != null)
            {
               existRole.Name = GetOneValue(bodyData, "ID_GROUPNAME");
               existRole.ModifiedBy = curUser;
               existRole.IsDeleted = GetOneValue(bodyData, "ID_STATUS") != "0";

               try
               {
                  cloudEntities.SaveChanges();
                  status = true;
               }
               catch (Exception ex)
               {
                  Utility.Logger.Error(ex);
               }
            }
         }

         return status;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetMax(bool isDeleted)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (cloudEntities.QnexRole.Where(c => c.IsDeleted == isDeleted).Max(c => c.RoleID));
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int GetMin(bool isDeleted)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            return (cloudEntities.QnexRole.Any(c => c.IsDeleted == isDeleted) ? cloudEntities.QnexRole.Where(c => c.IsDeleted == isDeleted).Min(c => c.RoleID) : 0);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save configuration to database.
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SetConfiguration(List<RoleEntity> itemList, string curUser)
      {
         //var status = false;
         //var roleId = Convert.ToInt32(itemList.Select(c => c.RoleId).FirstOrDefault());
         //var newFunctionObjects = itemList.Where(c => c.FuncCode != null && c.SegCode != null).ToList();
         //var newSegmentObjects = itemList.Where(c => c.SegCode != null && c.FuncCode == null && c.TabCode != null).ToList();
         //var newTabObjects = itemList.Where(c => c.SegCode == null && c.FuncCode == null && c.TabCode != null).ToList();
         //var newAppObjects = itemList.Where(c => c.TabCode == null && c.FuncCode == null && c.SegCode == null).ToList();

         //using (var cloudEntities = new QoskCloud())
         //{
         //   try
         //   {
         //      var deleteFunctionObjects = cloudEntities.PrivsFuncRel
         //         .Where(c => c.PrivilegesID == roleId && c.Privileges == "Role").ToList();

         //      if (deleteFunctionObjects.Count > 0)
         //      {
         //         cloudEntities.PrivsFuncRel.RemoveRange(deleteFunctionObjects);
         //      }

         //      var deleteSegmentObjects = cloudEntities.PrivsSegRel
         //         .Where(c => c.PrivilegesID == roleId && c.Privileges == "Role").ToList();

         //      if (deleteSegmentObjects.Count > 0)
         //      {
         //         cloudEntities.PrivsSegRel.RemoveRange(deleteSegmentObjects);
         //      }

         //      var deleteTabObjects = cloudEntities.PrivsTabRel
         //         .Where(c => c.PrivilegesID == roleId && c.Privileges == "Role").ToList();

         //      if (deleteTabObjects.Count > 0)
         //      {
         //         cloudEntities.PrivsTabRel.RemoveRange(deleteTabObjects);
         //      }

         //      var deleteAppObjects = cloudEntities.PrivsAppRel
         //        .Where(c => c.PrivilegesID == roleId && c.Privileges == "Role").ToList();

         //      if (deleteAppObjects.Count > 0)
         //      {
         //         cloudEntities.PrivsAppRel.RemoveRange(deleteAppObjects);
         //      }

         //      cloudEntities.SaveChanges();

         //      if (newAppObjects.Count > 0)
         //      {
         //         SaveToAppTable(newAppObjects, cloudEntities, curUser);
         //      }

         //      if (newTabObjects.Count > 0)
         //      {
         //         SaveToTabTable(newTabObjects, cloudEntities, curUser);
         //      }

         //      if (newSegmentObjects.Count > 0)
         //      {
         //         SaveToSegTable(newSegmentObjects, cloudEntities, curUser);
         //      }

         //      if (newFunctionObjects.Count > 0)
         //      {
         //         SaveToFuncTable(newFunctionObjects, cloudEntities, curUser);
         //      }

         //      status = true;
         //   }
         //   catch (Exception ex)
         //   {
         //      Utility.Logger.Error(ex);
         //   }
         //}

         //return status;
         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   save to PrivilegesFuncRel Table
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToFuncTable(List<RoleEntity> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var funcObject = new PrivsFuncRel
               {
                  AppCode = items.AppCode,
                  SegCode = items.SegCode,
                  FuncCode = items.FuncCode,
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser,
                  Privileges = "Role",
                  //PrivilegesID = Convert.ToInt32(items.RoleId)
               };

               db.PrivsFuncRel.Add(funcObject);
            }

            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save to PrivilegesSegRel table
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToSegTable(List<RoleEntity> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var segObject = new PrivsSegRel()
               {
                  AppCode = items.AppCode,
                  SegCode = items.SegCode,
                  TabCode = items.TabCode,
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser,
                  //PrivilegesID = Convert.ToInt32(items.RoleId),
                  Privileges = "Role"
               };

               db.PrivsSegRel.Add(segObject);
            }

            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save to PrivilegesTabRel table.
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToTabTable(List<RoleEntity> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var tabObject = new PrivsTabRel()
               {
                  AppCode = items.AppCode,
                  TabCode = items.TabCode,
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser,
                  Privileges = "Role",
                  //PrivilegesID = Convert.ToInt32(items.RoleId)
               };

               db.PrivsTabRel.Add(tabObject);
            }

            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   save to PrivilegesAppRel table.
      /// </summary>
      /// <param name="itemList"></param>
      /// <param name="db"></param>
      /// <param name="curUser"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void SaveToAppTable(List<RoleEntity> itemList, QoskCloud db, string curUser)
      {
         try
         {
            foreach (var items in itemList)
            {
               var appObject = new PrivsAppRel()
               {
                  AppCode = items.AppCode,
                  Version = 1,
                  IsDeleted = false,
                  CreatedBy = curUser,
                  Privileges = "Role",
                  //PrivilegesID = Convert.ToInt32(items.RoleId),
                  //SeqNo = (byte)(items.SeqNo = ((itemList.Max(c => (int?)c.SeqNo) ?? 0) + 1))
               };

               db.PrivsAppRel.Add(appObject);
            }

            db.SaveChanges();
         }
         catch (Exception ex)
         {
            Utility.Logger.Error(ex);
         }
      }

      #endregion

   }
}