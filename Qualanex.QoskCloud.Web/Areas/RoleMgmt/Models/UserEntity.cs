using System.ComponentModel;
using Microsoft.Ajax.Utilities;

namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models
    {
        public class UserEntity
        {
            public UserEntity()
            {
            }

            [DisplayName(@"First Name")]
            public string FirstName { get; set; }

            [DisplayName(@"Last Name")]
            public string LastName { get; set; }

            [DisplayName(@"User Name")]
            public string UserName { get; set; }

            [DisplayName(@"Email")]
            public string Email { get; set; }

            [DisplayName(@"Phone Number")]
            public string PhoneNumber { get; set; }

            [DisplayName(@"Fax Number")]
            public string FaxNumber { get; set; }

            [DisplayName(@"User Type")]
            public string ProfileCode { get; set; }

            [DisplayName(@"Profile Name")]
            public string ProfileName { get; set; }

            public long UserId { get; set; }

            [DisplayName(@"IsDeleted")]
            public bool IsDeleted { get; set; }

            [DisplayName(@"Enabled")]
            public string Password { get; set; }

            [DisplayName(@"Account Status")]
            public string AccountStatus { get; set; }
            /*
                    [DisplayName(@"ID")]
                    public string ID { get; set; }
                */
        public bool Enabled { get; set; }

            public bool IsDefault()
            {
                if (!this.FirstName.IsNullOrWhiteSpace()
                    || !this.LastName.IsNullOrWhiteSpace()
                    || !this.UserName.IsNullOrWhiteSpace()
                    || !this.Email.IsNullOrWhiteSpace()
                    || !this.ProfileCode.IsNullOrWhiteSpace()
                    || !this.ProfileName.IsNullOrWhiteSpace())
                {
                    return false;
                }
                return true;
            }
        }
    }