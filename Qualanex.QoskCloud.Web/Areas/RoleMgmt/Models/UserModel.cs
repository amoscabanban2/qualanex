﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.QoskCloud.Web.Areas.RoleMgmt.ViewModels;
using Qualanex.QoskCloud.Web.Common;

namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models
{
    public class UserModel
    {
        public static List<UserEntity> GetUserDetails(bool isEnable)
        {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var userDetails = (from user in cloudEntities.User

                                   join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                                   join userTypeDict in cloudEntities.UserTypeDict on user.UserTypeCode equals userTypeDict.Code
                                   where user.IsDeleted == false || user.IsDeleted == isEnable
                                   select new UserEntity()
                                   {
                                       FirstName = user.FirstName,
                                       LastName = user.LastName,
                                       UserName = user.UserName,
                                       Email = user.Email,
                                       PhoneNumber = user.VoiceNumber,
                                       FaxNumber = user.FaxNumber,
                                       ProfileCode = userTypeDict.Description, //User Type
                                       ProfileName = profile.Name,
                                       Enabled = user.IsEnabled,
                                       Password = user.Password,
                                       AccountStatus = user.IsDeleted ? "Disabled" : !user.IsEnabled ? "Locked" : user.IsTemporaryPassword.Value ? "Pending Password" : "Enabled",
                                       IsDeleted = user.IsDeleted,
                                       UserId = user.UserID,
                                       // ID = user.Id,
                                   }).ToList();
                return userDetails;
            }
            //return null;
        }

        public static List<UserEntity> UpdateUserDetail(long userID)
        {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                //need to validate that userID is not null
                var userDetails = (from user in cloudEntities.User

                                   join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                                   join userTypeDict in cloudEntities.UserTypeDict on user.UserTypeCode equals userTypeDict.Code

                                   select new UserEntity()
                                   {
                                       FirstName = user.FirstName,
                                       LastName = user.LastName,
                                       UserName = user.UserName,
                                       Email = user.Email,
                                       PhoneNumber = user.VoiceNumber,
                                       FaxNumber = user.FaxNumber,
                                       ProfileCode = userTypeDict.Description, //User Type
                                       ProfileName = profile.Name,
                                       Enabled = user.IsEnabled,
                                       Password = user.Password,
                                       UserId = user.UserID,
                                    //   ID = user.Id
                                   }).ToList();
                return userDetails;
            }
            //return null;
        }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ActionMenuModel GetActionMenu()
      {
         var actionMenu = new ActionMenuModel
         {
            Name = "Select One",
            Links = new List<ActionMenuEntry>
            {
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 1",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 2",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 3",
                  Url = "#"
               },
               new ActionMenuEntry
               {
                  Name = "Action Menu Item 4",
                  Url = "#"
               }
            }
         };

         return actionMenu;
      }

      public static List<RoleEntity> GetGroupDetails(bool isEnable)
        {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var groupDetails = (from ProfileGroup in cloudEntities.ProfileGroup
                                    join ProfileGroupTypeDict in cloudEntities.ProfileGroupTypeDict on ProfileGroup.ProfileGroupTypeCode equals ProfileGroupTypeDict.Code
                                    where ProfileGroup.IsDeleted == true || ProfileGroup.IsDeleted == isEnable
                                    select new RoleEntity()
                                   {
                                       GroupId = ProfileGroup.ProfileGroupID,
                                       GroupName = ProfileGroup.GroupName,
                                        GroupType = ProfileGroupTypeDict.Description
                                    }).ToList();
                return groupDetails;
            }
            //return null;
        }

        public static List<RoleEntity> UpdateGroupDetail(long groupId)
        {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var groupDetails = (from ProfileGroup in cloudEntities.ProfileGroup

                                   join ProfileGroupTypeDict in cloudEntities.ProfileGroupTypeDict on ProfileGroup.ProfileGroupTypeCode equals ProfileGroupTypeDict.Code
                                    select new RoleEntity()
                                   {
                                       GroupId = ProfileGroup.ProfileGroupID,
                                       GroupName = ProfileGroup.GroupName,
                                       GroupType = ProfileGroupTypeDict.Description
                                   }).ToList();
                return groupDetails;
            }
            //return null;
        }
    }

}