﻿$(function()
{
   //*****************************************************************************
   //*
   //* RoleMgmt.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the RoleMgmt partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************

   var groupId;
   var table;
   var active;
   var roleId;
   var baseURL = "/RoleMgmt";
   var keyPressVal = 0;
   var validate = "Click";
   var valid = false;
   var cancel = false;
   var txtChanges = false;
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
   var tbodyApp = "#roleAppRelationControl table tbody tr";
   var tbodyTab = "#roleTabRelationControl table tbody tr";
   var tbodySeg = "#roleSegmentRelationControlItems table tbody tr";
   var tbodyFunc = "#roleFunctionRelationControlItems table tbody tr";

   // Page load functions.
   $(document).ready(function()
   {
      var table = window.dtHeaders("#searchGrid", "", [], []);
      $(document).off("click", ".relationDisplayRow");
      InitialState();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Edit button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#CMD_EDIT").unbind("click").click(function()
   {
      if (getEditMode())
      {
         return false;
      }
      enableActionbarControl("#CMD_REFRESH", false);
      setEditMode(true);
      editMode = "Edit";
      if ($("a.active").attr("href") === "#tabs-Config")
      {
         if ($("#roleAppRelationControl").val() === undefined)
         {
            return false;
         }
         active = "config";
         $("input[Type=Text],.inputEx").attr("readonly", "readonly");
         $(".formMax select").each(function()
         {
            if ($(this).hasClass("editable"))
            {
               $(this).prop("disabled", true);
            }
         });
         var appUnique = $(tbodyApp + ".selected ").attr("data-unique-selection");
         var appRowId = $(tbodyApp + ".selected ").attr("data-row-id");
         var tabUnique = $(tbodyTab + ".selected ").attr("data-unique-selection");
         var tabRowId = $(tbodyTab + ".selected ").attr("data-row-id");
         var segmentUnique = $(tbodySeg + ".selected ").attr("data-unique-selection");
         var segmentRowId = $(tbodySeg + ".selected ").attr("data-row-id");
         var functionUnique = $(tbodyFunc + ".selected ").attr("data-unique-selection");
         var functionRowId = $(tbodyFunc + ".selected ").attr("data-row-id");
         $(".relationControl").enableRelationEdit();
         $(tbodyApp + ".blank select[name='AppCode'] option")
           .each(function()
           {
              if ($(this).val() === "Delete")
              {
                 $(this).hide();
              }
           });
         $(tbodyTab + ".blank select[name='TabCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $(tbodySeg + ".blank select[name='SegCode'] option")
             .each(function()
             {
                if ($(this).val() === "Delete")
                {
                   $(this).hide();
                }
             });
         $(tbodyFunc + ".blank select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            });
         $(tbodyApp + " td select[name='AppCode']").css("width", "100%");
         $(tbodyApp + " td select[name='AppCode']").closest("td").css("width", "100%");
         $(tbodyTab + " td select[name='TabCode']").css("width", "100%");
         $(tbodyTab + " td select[name='TabCode']").closest("td").css("width", "100%");
         $(tbodySeg + " td select[name='SegCode']").css("width", "100%");
         $(tbodySeg + " td select[name='SegCode']").closest("td").css("width", "100%");
         $(tbodyFunc + " td select[name='FuncCode']").css("width", "100%");
         $(tbodyFunc + " td select[name='FuncCode']").closest("td").css("width", "100%");
         $(tbodyApp + " input.image_tab_relation")
             .each(function()
             {
                $(this).attr("data-field-type", "AppCode");
             });
         $(tbodyTab + " input.image_tab_relation")
            .each(function()
            {
               $(this).attr("data-field-type", "TabCode");
            });
         $(tbodySeg + " input.image_tab_relation")
            .each(function()
            {
               $(this).attr("data-field-type", "SegCode");
            });

         $(tbodyApp).each(function()
         {
            if ($(this).attr("data-unique-selection") === appUnique && $(this).attr("data-row-id") === appRowId && appRowId !== undefined)
            {
               $(this).addClass("selected");
            }
         });

         if (!$(tbodyApp).hasClass("selected"))
         {
            $("#roleTabRelationControl .relationControl").disableRelationEdit();
         }

         showTabOptions(tabUnique, tabRowId);
         showSegmentOptions(segmentUnique, segmentRowId);
         showFunctionOptions(functionUnique, functionRowId);
      }
      else if ($("a.active").attr("href") === "#tabs-summary")
      {
         active = "summary";
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Leave the Group Name textbox.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("blur", "#ID_ROLENAME", function()
   {
      if (getEditMode())
      {
         checkFieldDb();
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Check Profile code is exist.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function checkFieldDb()
   {
      if (valid && !txtChanges)
      {
         return false;
      }

      var rlName = $("#ID_ROLENAME").val();

      if (rlName.length === 0)
      {
         setColorAndClass("#FFFFC0CB", "#ID_ROLENAME", "inputError");
         enableActionbarControl("#CMD_SAVE", false);
         return false;
      }

      var url = "Check_With_DB";

      $.ajax({
         type: "POST",
         url: url,
         datatype: "JSON",
         data: "roleId=" + roleId + "&roleName=" + rlName +
            getAntiForgeryToken(),
         success: function(data)
         {
            if (data.Success)
            {
               valid = true;
               txtChanges = false;

               if ($("#ID_ROLENAME").hasClass("inputError"))
               {
                  removeColorAndClass("#ECDD9D", "#ID_ROLENAME", "inputError");
               }

               if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
               {
                  enableActionbarControl("#CMD_SAVE", true);
               }

               return false;
            }
            else
            {
               valid = false;
               validate = "Click";
               if ($(this).messageBoxStatus())
               {
                  $(this).closeMessageBox();
               }
               setColorAndClass("#FFFFC0CB", "#" + data.Input, "inputError");
               enableActionbarControl("#CMD_SAVE", false);
               return false;
            }
         }
      });

      return false;
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Add New Role detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("click", "#CMD_NEW", function(e)
   {
      if (!getEditMode())
      {
         if ($("a.active").attr("href") === "#tabs-summary")
         {
            active = "summary";
            roleId = 0;
            enableActionbarControl("#CMD_RETIRED", false);
            enableActionbarControl("#CMD_REFRESH", false);
            setEditMode(true);

            $(".formBody").find("input[type=Text]").val("");
            $("#ID_STATUS").val("0");

            if ($("#searchGrid tr.selected").hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }

            $("#searchGrid tbody tr.hidden").removeClass("hidden");
            $("#searchGrid tbody").find("tr:first").addClass("selected");
         }
         updateDataTable($("#ID_STATUS").val(), "ID_STATUS", roleId);
      }

      return false;
   });

   $(document).on("click", "#btn_dontSaveRoleDetails", function()
   {
      $(this).closeMessageBox();
      validate = 'Click';
   });

   $(document).on("click", "#btn_saveRoleDetails", function()
   {
      if (validate === "Click")
      {
         return false;
      }

      if ($("#ID_ROLENAME").val().length === 0)
      {
         return false;
      }

      $(this).closeMessageBox();
      if ($("#ID_DESCRIPTION").val().length === 0)
      {
         setColorAndClass("#FFFFC0CB", "#ID_DESCRIPTION", "inputError");
         enableActionbarControl("#CMD_SAVE", false);
         return false;
      }

      var groupName = $(".groupCodeField")[0].value;
      var grpCode = $("#groupList option").length>0? $("#groupList option[value='" + groupName + "']")
         .attr("data-field-name") : $("#searchGrid tr.selected td[data-field-name='GroupId']").closest("td").attr("data-field-value");


      if ((grpCode === undefined || grpCode.length === 0))
      {
         setColorAndClass("#FFFFC0CB", "#ID_GROUPCODE", "inputError");
         enableActionbarControl("#CMD_SAVE", false);
         return false;
      }

      var bodyData = getBodyData(grpCode);

      if (validate === "" && roleId !== undefined)
      {
         var isDelete = true;
         var attSrc = $("#CMD_RETIRED").attr("src");

         if (attSrc.indexOf("HideRetired(0)") >= 0)
         {
            isDelete = false;
         }

         $.ajax({
            type: "POST",
            url: baseURL + "/RoleMgmt/SaveRoleDetail",
            data: "roleId=" + roleId + "&bodyData=" + bodyData + "&isDeleted=" +
               isDelete + getAntiForgeryToken(),
            datatype: "JSON",
            success: function(data)
            {
               if (data.Success && validate === "")
               {
                  roleId = data.max > 0
                     ? data.max
                     : roleId = 0;
                  validate = "Click";
                  setEditMode(false);
                  $(".formBody .selectEx").removeAttr("required")
                  active = "";
                  clearTextBoxes();
                  refreshRoleGrid(roleId);
                  enableActionbarControl("#CMD_REFRESH", true);
                  enableActionbarControl("#CMD_RETIRED", true);
                  $(".formBody .inputEx,.selectEx")
                     .removeClass("editable")
                     .addClass("editable");
               }

               validate = "Click";
            },
            error: function(data)
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The controller call fails on save group request.</h3>"
               }];
               btnObjects = [{
                  "id": "btn_ok_error",
                  "name": "Ok",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }];

               titleobjects = [{ "title": "Invalid Entry" }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
            }
         });
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Clear Textboxes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function clearTextBoxes()
   {
      $(".formMax .inputEx").val("");
      $(".formMax .selectEx").val("0");
      $(".formMax .inputEx,.selectEx").removeAttr("style");
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Save Role detail procedure.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("click", "#CMD_SAVE", function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if (getEditMode() && !$(".formMax input").hasClass("inputError")
               && !$(".formMax select").hasClass("inputError"))
            {
               if (!window.getFormChanged())
               {
                  validate = "Click";
                  $(this).closeMessageBox();
                  return false;
               } else
               {
                  if ($(this).messageBoxStatus())
                  {
                     $(this).closeMessageBox();
                  }

                  if (window.getFormChanged() && !$(".formBody").filter(":input").hasClass("inputError"))
                  {
                     var roleName = $("#ID_ROLENAME").val();

                     if (roleName.length === 0)
                     {
                        setColorAndClass("#FFFFC0CB", "#ID_ROLENAME", "inputError");
                        enableActionbarControl("#CMD_SAVE", false);
                        return false;
                     }

                     var grpCode = $("#ID_GROUPTYPE").val();

                     if (grpCode === null || grpCode === "")
                     {
                        setColorAndClass("#FFFFC0CB", "#ID_GROUPTYPE", "inputError");
                        enableActionbarControl("#CMD_SAVE", false);
                        validate = "Click";
                        return false;
                     }

                     var groupName = $(".groupCodeField")[0].value;
                     grpCode =$("#groupList option").length>0? $("#groupList option[value='" + groupName + "']")
                        .attr("data-field-name") : $("#searchGrid tr.selected td[data-field-name='GroupId']").closest("td").attr("data-field-value");

                     if ((grpCode === undefined || grpCode.length === 0))
                     {
                        setColorAndClass("#FFFFC0CB", "#ID_GROUPCODE", "inputError");
                        enableActionbarControl("#CMD_SAVE", false);
                        return false;
                     }

                     validate = "";
                     messageObject = [{
                        "id": "lbl_messages",
                        "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to save the changes?</h3>"
                     }];
                     btnObjects = [{
                        "id": "btn_saveRoleDetails",
                        "name": "Yes",
                        "class": "btnErrorMessage",
                        "style": "margin:0 auto;text-align:center"
                     },
                     {
                        "id": "btn_dontSaveRoleDetails",
                        "name": "No",
                        "class": "btnErrorMessage",
                        "style": "margin:0 auto;text-align:center"
                     }];

                     titleobjects = [{ "title": "Confirmation" }];
                     $(this).addMessageButton(btnObjects, messageObject);
                     $(this).showMessageBox(titleobjects);
                  }

                  return false;
               }
            }
         }
         else if (active === "config")
         {
            enableActionbarControl("#CMD_SAVE", false);
            enableActionbarControl("#CMD_CANCEL", false);
            var itemLists = [];
            var relationObjects = {};
            $(tbodyApp + ".relationEditRow," + tbodyApp + ".updated")
               .each(function()
               {
                  relationObjects = {
                     "RoleId": roleId,
                     "AppCode": $(this).find("td input[name='Code']").val()
                  }
                  itemLists.push(relationObjects);
               });
            relationObjects = {};
            $(tbodyTab + ".relationEditRow," + tbodyTab + ".updated")
               .each(function()
               {
                  relationObjects = {
                     "RoleId": roleId,
                     "AppCode": $(this).find("td input[name='AppCode']").val(),
                     "TabCode": $(this).find("td input[name='Code']").val()
                  }
                  itemLists.push(relationObjects);
               });
            relationObjects = {};
            $(tbodySeg + ".relationEditRow," + tbodySeg + ".updated")
               .each(function()
               {
                  relationObjects = {
                     "RoleId": roleId,
                     "AppCode": $(this).find("td input[name='AppCode']").val(),
                     "TabCode": $(this).find("td input[name='TabCode']").val(),
                     "SegCode": $(this).find("td input[name='Code']").val()
                  }
                  itemLists.push(relationObjects);
               });
            relationObjects = {};
            $(tbodyFunc + ".relationEditRow," + tbodyFunc + ".updated")
               .each(function()
               {
                  relationObjects = {
                     "RoleId": roleId,
                     "AppCode": $(this).find("td input[name='AppCode']").val(),
                     "SegCode": $(this).find("td input[name='SegCode']").val(),
                     "FuncCode": $(this).find("td input[name=Code]").val()
                  }
                  itemLists.push(relationObjects);
               });
            if (itemLists.length === 0)
            {
               itemLists.push({ "RoleId": roleId });
            }

            $.ajax({
               dataType: "json",
               type: "POST",
               url: baseURL + "/RoleMgmt/SaveRoleConfigDetail",
               data: "roleConfigEntity=" + JSON.stringify(itemLists) + getAntiForgeryToken(),
               success: function(data)
               {
                  if (data.Success)
                  {
                     $(".relationControl").disableRelationEdit();

                     $(".relationControl table tbody tr").removeClass("selected");
                     enableActionbarControl("#CMD_REFRESH", true);
                     RoleConfiguration(roleId);
                     setEditMode(false);
                     active = "";
                  }

                  else
                  {
                     enableActionbarControl("#CMD_SAVE", true);
                     enableActionbarControl("#CMD_CANCEL", true);
                  }
                  return false;
               },
               error: function(data)
               {
                  enableActionbarControl("#CMD_SAVE", true);
                  enableActionbarControl("#CMD_CANCEL", true);

               }
            });
         }
      }
   });


   function refreshRoleGrid(rlId)
   {
      var url = "ViewDataRequest";

      if (validate === "")
      {
         return false;
      }

      if ($("#searchGrid tr").length < 3
         && $("#searchGrid_paginate span a").length === 1)
      {
         roleId = 0;
      }

      var isDelete = true;
      var attSrc = $("#CMD_RETIRED").attr("src");

      if (attSrc.indexOf("HideRetired(0)") >= 0)
      {
         isDelete = false;
      }

      $.ajax({
         type: "POST",
         url: url,
         data:
            {
               isDeleted: isDelete, __RequestVerificationToken:
                  $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
            },
         success: function(data)
         {
            var $target = $("#RoleGridDetail");
            var $newHtml = $(data);

            $target.replaceWith($newHtml);
            table = window.dtHeaders("#searchGrid", "", [], []);

            if (!table.$("tr.selected").hasClass("selected"))
            {
               var i = 0;

               if (rlId > 0)
               {
                  $newHtml.find("#searchGrid [data-row-id='"
                                + rlId + "']").click();
               }
               else
               {
                  $newHtml.find("#searchGrid tbody").find("tr:eq(1)").click();
               }
            }

            if (!$("#searchGrid tr.selected").hasClass("selected"))
            {
               $("#ID_STATUS").val("0");
            }
         }
      });

      return false;
   }
   //Refresh User detail procedure
   $(document).on("click", "#CMD_REFRESH", function(e)
   {
      if (!getEditMode() && $("a.active").attr("href") === "#tabs-summary")
      {
         var url = "ViewDataRequest";
         var isDelete = true;
         var attSrc = $("#CMD_RETIRED").attr("src");

         if (attSrc.indexOf("HideRetired(1)") >= 0)
         {
            isDelete = false;
         }

         $.ajax({
            type: "POST",
            url: url,
            data:
               {
                  isDeleted: isDelete, __RequestVerificationToken:
                     $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
               },
            success: function(data)
            {
               var $target = $("#RoleGridDetail");
               var $newHtml = $(data);

               $target.replaceWith($newHtml);
               table = window.dtHeaders("#searchGrid", "", [], []);
               $("#searchGrid tbody tr:eq(1)").click();
            }
         });

         return true;
      }
   });

   $(document).on("click", "#btn_cancel_saving", function()
   {
      document.getElementById("groupList").innerHTML = "";
      setEditMode(false);
      cancel = true;
      $(this).closeMessageBox();

      if (getEditMode() && validate === "")
      {
         return false;
      }

      clearTextBoxes();
      $(".formMax .inputEx,.selectEx").removeClass("inputError");
      refreshRoleGrid(roleId);
      enableActionbarControl("#CMD_REFRESH", true);
      enableActionbarControl("#CMD_RETIRED", true);
   });

   //Cancel User detail procedure
   $(document).on("click", "#CMD_CANCEL", function()
   {
      if (getEditMode())
      {
         if (active === "config")
         {
            setEditMode(false);
            RoleConfiguration(roleId === undefined ? 0 : roleId);
            enableActionbarControl("#CMD_REFRESH", true);

            active = "";
         }
         else if (active === "summary")
         {
            if (window.getFormChanged())
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> You have pending changes. Do you want to cancel?</h3>"
               }];
               btnObjects = [{
                  "id": "btn_cancel_saving",
                  "name": "Yes",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }, {
                  "id": "btn_no",
                  "name": "No",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center",
                  "function": "onclick='$(this).closeMessageBox();'"
               }
               ];

               titleobjects = [{ "title": "Confirm Cancellation" }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);

            }
            else
            {
               document.getElementById("groupList").innerHTML = "";
               $(".formBody").find("input[type=Text]").val("");
               if ($("#searchGrid tbody").find("tr:eq(1)").hasClass("selected")
                  && !getEditMode())
               {
                  $("#searchGrid tbody").find("tr:eq(1)").remove();
               }

               clearTextBoxes();
               $(".formMax .inputEx,.selectEx").removeClass("inputError");
               setEditMode(false);
               if (roleId === 0)
               {
                  $("#searchGrid tr[data-row-id=0]").addClass("hidden");
                  $("#searchGrid tr:eq(2)").trigger("click");
               }
               else
               {
                  $("#searchGrid tr[data-row-id=" + roleId + "]").trigger("click");
               }
               enableActionbarControl("#CMD_REFRESH", true);
               enableActionbarControl("#CMD_RETIRED", true);
               enableActionbarControl("#CMD_SAVE", false);
               active = "";
            }
         }
      }
   });
   //View disabled records/Hide disabled records User detail procedure

   $(document).on("click", "#CMD_RETIRED", function()
   {
      var isEnable = false;
      var $AttSrc = $("#btn_toolbar_hide").children().attr('src');
      var $newTit = "";
      var $newSrc = "";

      $("#btn_toolbar_hide").attr('title', $newTit);
      $("#btn_toolbar_hide").children().attr('src', $newSrc);
      $("#btn_toolbar_hide").show();

      var $url = $("#btn_toolbar_hide").attr("data-method");
      var $TargetID = $("#UserGridDetail");
      $.ajax({
         type: "GET",
         url: $url,    //Controllers Function UserAccountView_Hide
         data: {
            'isEnable': isEnable
         },
         success: function(data)
         {
            var $target = $("#UserGridDetail");
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            $newHtml.find("#searchGrid tbody").find('tr:first').click();
            table = $('#searchGrid').DataTable();
         },
         error: function(data)
         {
            alert("Fail");
         }
      });
   });

   //Set the Initial state of the active tool buttons
   function InitialState()
   {
      setEditMode(false);
      window.enableActionbarControl("#CMD_REFRESH", true);
      if (!$("#searchGrid tbody tr").hasClass("selected"))
      {
         $("#searchGrid tbody").find("tr:eq(1)").click();
      }
   }

   $(document).on("click", "#btn_Discard_Save", function()
   {
      if (!getEditMode())
      {
         return false;
      }
      active = "";
      cancel = true;

      $(this).closeMessageBox();
      $(".formBody").find("input[type=Text]").val("");

      if ($("#searchGrid tbody").find("tr:eq(1)").hasClass("selected")
         && !getEditMode())
      {
         $("#searchGrid tbody").find("tr:eq(1)").remove();
      }

      clearTextBoxes();

      $(".formMax .inputEx,.selectEx").removeClass("inputError");

      setEditMode(false);
      refreshRoleGrid(roleId);
      enableActionbarControl("#CMD_REFRESH", true);
      enableActionbarControl("#CMD_RETIRED", true);
   });

   //Function to handle when a user's detail is selected form the table:
   $(document).on("click", ".roleDetail-div-click", function(e)
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if ($("#CMD_NEW").attr("src").indexOf("(0)") > 0
               || $("#CMD_EDIT").attr("src").indexOf("(0)") > 0)
            {
               messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> Do you want to Save the change?</h3>"
               }];
               btnObjects = [{
                  "id": "CMD_SAVE",
                  "name": "Save",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center;margin-right:5px"
               },
               {
                  "id": "btn_Continue",
                  "name": "Continue",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center;margin-right:5px",
                  "function": "onclick='$(this).closeMessageBox();'"
               },
               {
                  "id": "btn_Discard_Save",
                  "name": "Discard",
                  "class": "btnErrorMessage",
                  "style": "margin:0 auto;text-align:center"
               }];

               titleobjects = [{ "title": "Confirmation" }];
               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
               return false;
            };

            return false;
         }
      }
      else
      {
         table = table === undefined ? window.dtHeaders("#searchGrid", "", [], []) : table;

         if (!cancel)
         {
            if (!$(this).hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }

            $(this).closest("tr").find("td").each(function()
            {
               if ($(this).attr("data-field-name") === "GroupName")
               {
                  setData("#ID_GROUPCODE", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "RoleName")
               {
                  setData("#ID_ROLENAME", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "Description")
               {
                  setData("#ID_DESCRIPTION", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "Status")
               {
                  if ($(this).attr("data-field-value") === "Enabled")
                  {
                     $("#ID_STATUS").val("0");
                  }
                  else
                  {
                     $("#ID_STATUS").val("1");
                  }
               }
            });

            $(this).closest("tr").addClass("selected");
            roleId = $(this).closest("tr").attr("data-row-id");
         }
         else
         {
            if (roleId === 0)
            {
               if (!$(this).hasClass("selected"))
               {
                  $("#searchGrid tr.selected").removeClass("selected");
               }

               if ($("#searchGrid tr").length > 2)
               {
                  roleId = $("#searchGrid tr:eq(2)").closest("tr").attr("data-row-id");
               }
            }

            if (roleId > 0)
            {
               $("#searchGrid tr[data-row-id=" + roleId + "]").closest("tr")
                  .find("td")
                  .each(function()
                  {
                     if ($(this).attr("data-field-name") === "GroupName")
                     {
                        setData("#ID_GROUPCODE", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "RoleName")
                     {
                        setData("#ID_ROLENAME", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "Description")
                     {
                        setData("#ID_DESCRIPTION", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "GroupType")
                     {
                        //setData("#ID_GROUPTYPE", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "Status")
                     {
                        if ($(this).attr("data-field-value") === "Enabled")
                        {
                           $("#ID_STATUS").val("0");
                        }
                        else
                        {
                           $("#ID_STATUS").val("1");
                        }
                     }
                  });

               if (!$("#searchGrid tr[data-row-id=" + roleId + "]")
                  .closest("tr")
                  .hasClass("selected"))
               {
                  $("#searchGrid tr[data-row-id=" + roleId + "]")
                     .closest("tr")
                     .addClass("selected");
               }
            }
            cancel = false;
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set data from input  to Grid.
   //*
   //* Parameters:
   //*   inValue - Description not available.
   //*   inField - Description not available.
   //*   gpId    - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   function updateDataTable(inValue, inField, rlId)
   {
      var row = $("#searchGrid tbody").find("tr[data-row-id=0]").closest("tr");

      if (rlId > 0)
      {
         row = $("#RoleGridDetail").find("#searchGrid [data-row-id=" + rlId + "]");
      }

      if (inField === "ID_ROLENAME")
      {
         row.find("td:eq(1)").html(inValue);
      }

      if (inField === "ID_GROUPCODE")
      {
         row.find("td:eq(2)").html(inValue);
      }

      if (inField === "ID_STATUS")
      {
         row.find("td:eq(3)").html(inValue === "0" ? "Enabled" : "Retired");
      }
   }

   //Take a Form Id then return the content of the Form as an Object 
   function GetFormData(formId)
   {
      var formData = $(formId).serializeObject();
      return JSON.stringify(formData);
   }

   //Get Anti Forgery token from the Form
   function getAntiForgeryToken()
   {
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize();
   }

   //SerializeObject Function to select a Form by ID, serialize the content, clean it, and send it back as an object
   (function($)
   {
      $.fn.serializeObject = function()
      {

         var self = this,
             json = {},
             push_counters = {},
             patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
             };


         this.build = function(base, key, value)
         {
            base[key] = value;
            return base;
         };

         this.push_counter = function(key)
         {
            if (push_counters[key] === undefined)
            {
               push_counters[key] = 0;
            }
            return push_counters[key]++;
         };

         $.each($(this).serializeArray(), function()
         {

            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
               return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while ((k = keys.pop()) !== undefined)
            {

               // adjust reverse_key
               reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

               // push
               if (k.match(patterns.push))
               {
                  merge = self.build([], self.push_counter(reverse_key), merge);
               }

                  // fixed
               else if (k.match(patterns.fixed))
               {
                  merge = self.build([], k, merge);
               }

                  // named
               else if (k.match(patterns.named))
               {
                  merge = self.build({}, k, merge);
               }
            }

            json = $.extend(true, json, merge);
         });
         return json;
      };
   })(jQuery);

   function RoleConfiguration(roleId)
   {
      var groupId = $("#RoleGridDetail #searchGrid tr.selected td[data-field-name='GroupId']").attr("data-field-value");
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         url: baseURL + "/RoleMgmt/RoleConfiguration",
         data: {
            roleCode: roleId, groupCode: groupId,
            __RequestVerificationToken: $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
         },
         success: function(data)
         {
            var $target = $("#tabs-Config #RoleMgmtConfiguration");
            var $newHtml = $(data);
            $newHtml.find("table tbody tr.selected").removeClass("selected");
            window.replaceSegments($target, $newHtml, false);
            $(tbodyApp + ".relationDisplayRow  td").css("width", "100%");
            $(tbodyTab + ".relationDisplayRow  td").css("width", "100%");
            $(tbodySeg + ".relationDisplayRow  td").css("width", "100%");
            $(tbodyFunc + ".relationDisplayRow  td").css("width", "100%");
            $newHtml.find(tbodySeg).parent().hide();
            $newHtml.find(tbodyFunc).parent().hide();
            $newHtml.find(tbodyTab).parent().hide();
            $newHtml.find("#roleTabRelationControl table tbody td input.image_tab_relation")
            .each(function()
            {
               $(this).attr("data-field-type", "TabCode");
            });

            $newHtml.find("#roleSegmentRelationControlItems table tbody td input.image_tab_relation")
               .each(function()
               {
                  $(this).attr("data-field-type", "SegCode");
               });
            $newHtml.find("option[value='Delete']").css("font-weight", "bold");
            if ($(tbodyApp + ".relationDisplayRow:first").length > 0)
            {
               selectFirstRow($(tbodyApp + ".relationDisplayRow:first"), "click");
            }

         }
      });
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click on Configuration Tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".tabContainer a", function()
   {
      if ($("a.active").attr("href") === "#tabs-Config")
      {
         $(".ql-body.active").css("overflow", "auto");
         if (!getEditMode())
         {
            setRoleIdCallConfiguration();
         }
         else
         {
            if (active === "summary")
            {
               $("#roleAppRelationControl,#roleTabRelationControl,#roleSegmentRelationControlItems,#roleFunctionRelationControlItems").css("display", "none");
            }
         }
      }
      else
      {
         $("#dataTables_scrollHeadInner").css("width", "100%");
         $("#dataTables_scrollHeadInner table").css("width", "100%");
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   set upaId after save and cancel or change tab.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setRoleIdCallConfiguration()
   {
      roleId = $("#searchGrid tr.selected td[data-field-name='RoleId']").attr("data-field-value");
      if (roleId !== undefined
         && roleId !== "0"
         && roleId.length > 0)
      {
         RoleConfiguration(roleId);
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   onChange ProfileCode.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".groupCodeField", function(e)
   {
      if (getEditMode() && !window.getFormChanged())
      {
         $("#groupList option").each(function()
         {
            if ($(this).val() === $(e.target).val())
            {
               if (active === "summary")
               {
                  $(this).attr("data-field-name");

                  updateDataTable($(this).attr("data-field-name")
                     , "ID_GROUPCODE"
                     , roleId);
               }
            }
         });
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set the Edit mode flag.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".inputEx", function()
   {
      if (getEditMode())
      {
         if (active === "summary")
         {
            if ($(this).closest("input").attr("id") === "ID_ROLENAME")
            {
               txtChanges = true;
            }
            if ($(this).closest("input").attr("id") === "ID_DESCRIPTION")
            {
               if ($(this).val().length > 0)
               {
                  removeColorAndClass("#ECDD9D", "#ID_DESCRIPTION", "inputError");
                  if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
                  {
                     enableActionbarControl("#CMD_SAVE", true);
                  }
               }
            }
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set the Edit mode flag on selectEx.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("change", ".selectEx", function()
   {
      if (getEditMode())
      {
         window.setFormChanged(true);

         if ($(this).closest("select").attr("id") === "ID_STATUS")
         {
            removeColorAndClass("#ECDD9D", "#ID_STATUS", "inputError");

            if (!$(".formBody .inputEx,.selectEx").hasClass("inputError"))
            {
               enableActionbarControl("#CMD_SAVE", true);
            }
            updateDataTable($("#ID_STATUS").val(), "ID_STATUS", roleId);
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   search profile name by input.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function addDataListOptions(dataListId, strData, keyPress)
   {
      document.getElementById(dataListId).innerHTML = "";
      if (strData !== "")
      {
         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "strData=" + strData + "&keyPressValue=" + keyPress + "&browser=" + navigator.userAgent + getAntiForgeryToken(),
            url: baseURL + "/RoleMgmt/GetGroupName",
            success: function(jsonData)
            {
               if (jsonData.keyPressValue !== keyPressVal)
               {
                  return false;
               }
               var itemLists = [];
               var options = "";

               for (var jsIndx = 0; jsIndx < jsonData.groupEntity.length; jsIndx++)
               {
                  if (itemLists.length === 0)
                  {
                     options += "<option data-field-name='" + jsonData.groupEntity[jsIndx].GroupId +
                     "' value='" + jsonData.groupEntity[jsIndx].GroupName + "' data-field-value='" + jsonData.groupEntity[jsIndx].GroupType + "'> "
                     + jsonData.groupEntity[jsIndx].GroupName + "</option>";
                  }
                  else
                  {
                     var bln = false;
                     for (var ind = 0; ind < itemLists.length; ind++)
                     {
                        if (String(jsonData.groupEntity[jsIndx].GroupId) === itemLists[ind].GroupId)
                        {
                           bln = true;
                        }
                     }
                     if (bln === false)
                     {
                        options += "<option data-field-name='" + jsonData.groupEntity[jsIndx].GroupId +
                     "' value='" + jsonData.groupEntity[jsIndx].GroupName + "' data-field-value='" + jsonData.groupEntity[jsIndx].GroupType + "'> "
                     + jsonData.groupEntity[jsIndx].GroupName + "</option>";
                     }
                  }


               }

               document.getElementById(dataListId).innerHTML = options;
            }

         });
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).on("input", ".inputEx", function()
   {
      if (active === "summary")
      {
         var id = $(this).attr("id");
         var value = $(this).val();
         updateDataTable(value, id, roleId);

         if ($(this).attr("id") === "ID_GROUPCODE")
         {
            keyPressVal++;
            if ($("#groupList option[value='"
                             + $(this).val() + "']").attr("data-field-name") !== undefined)
            {
               $(this).attr("name", $("#groupList option[value='"
                             + $(this).val() + "']").attr("data-field-name"));
               removeColorAndClass("#ECDD9D", "#ID_GROUPCODE", "inputError");
               if (!$(".formMax .inputEx,selectEx").hasClass("inputError"))
               {
                  enableActionbarControl("#CMD_SAVE", true);
               }
               return false;
            }
            else
            {
               setColorAndClass("#FFFFC0CB", "#ID_GROUPCODE", "inputError");
               enableActionbarControl("#CMD_SAVE", false);
               $(this).closest("tr").find("td input[name=GroupCode]").val('');
            }

            addDataListOptions("groupList", $(this).val(), keyPressVal);
         }
      }
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set data from grid to inputs.
   //*
   //* Parameters:
   //*   inputId - Description not available.
   //*   value   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   function setData(inputId, value)
   {
      $(inputId).val(value);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function setColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).addClass(className);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Remove class and color for exist items and null items.
   //*
   //* Parameters:
   //*   color     - Description not available.
   //*   id        - Description not available.
   //*   className - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function removeColorAndClass(color, id, className)
   {
      $(id).css("background-color", color).removeClass(className);
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Get All FormBody data to the string list for controller input.
   //*
   //* Parameters:
   //*   groupCode   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   function getBodyData(grpCode)
   {
      var bodyData = null;

      $(".formMax *").filter(":input").each(function()
      {
         if ($(this).attr("id") === "ID_GROUPCODE")
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + grpCode + '",';
         }
         else
         {
            bodyData += '"' + $(this).attr("id") + '" : "' + $(this).val() + '",';
         }
      });

      return bodyData;
   };

   //*****************************************************************************
   //*
   //* Summary:
   //*   onclick on arrows.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "input.image_tab_relation", function(e)
   {
      var appCode;
      var tabCode;
      var segCode;
      if ($(e.target).attr("data-field-type") === "AppCode")
      {
         appCode = $(e.target).attr("name");
         $(tbodyApp).each(function()
         {
            if ($(this).hasClass("selected"))
            {
               $(this).removeClass("selected");
            }
         });
         if (appCode === undefined)
         {
            $("#roleTabRelationControl .relationControl").disableRelationEdit();
            $(tbodyTab).parent().hide();

            $("#roleSegmentRelationControlItems .relationControl").disableRelationEdit();
            $(tbodySeg).parent().hide();

            $("#roleFunctionRelationControlItems .relationControl").disableRelationEdit();
            $(tbodyFunc).parent().hide();
            $(e.target).closest("tr").addClass("selected");





            return false;
         }

         $(tbodyTab).parent().show();
         $("#roleTabRelationControl .relationControl").enableRelationEdit();
         $(tbodySeg + ".selected").removeClass("selected");
         $(tbodySeg).parent().hide();
         $(tbodyFunc + ".selected").removeClass("selected");
         $(tbodyFunc).parent().hide();
         $(tbodyTab + ".selected").removeClass('selected');
         $(this).closest("tr").addClass("selected");
         showTabOptions();
      }

      else if ($(e.target).attr("data-field-type") === "TabCode")
      {
         appCode = $(e.target).attr("name");
         tabCode = $(e.target).attr("value");
         $(tbodyTab).each(function()
         {
            if ($(this).hasClass("selected"))
            {
               $(this).removeClass("selected");
            }
         });

         if (tabCode === undefined)
         {
            $("#roleSegmentRelationControlItems .relationControl").disableRelationEdit();
            $(tbodySeg).parent().hide();

            $("#roleFunctionRelationControlItems .relationControl").disableRelationEdit();
            $(tbodyFunc).parent().hide();
            $(e.target).closest("tr").addClass("selected");
            $(tbodySeg + ".selected").removeClass("selected");
            return false;
         }
         $(tbodySeg).parent().show();
         $("#roleSegmentRelationControlItems .relationControl").enableRelationEdit();
         $(tbodySeg + ".selected").removeClass("selected");
         $(this).closest("tr").addClass("selected");
         showSegmentOptions();
      }

      else if ($(e.target).attr("data-field-type") === "SegCode")
      {
         appCode = $(e.target).attr("name");
         segCode = $(e.target).attr("value");
         $(tbodySeg).each(function()
         {
            if ($(this).hasClass("selected"))
            {
               $(this).removeClass("selected");
            }
         });

         if (segCode === undefined)
         {
            $("#roleFunctionRelationControlItems .relationControl").disableRelationEdit();
            $(tbodyFunc).parent().hide();
            $(e.target).closest("tr").addClass("selected");
            $(tbodyFunc + ".selected").removeClass("selected");
            return false;
         }

         $(tbodyFunc).parent().show();
         $("#roleFunctionRelationControlItems .relationControl").enableRelationEdit();
         $(tbodyFunc + ".selected").removeClass("selected");
         $(this).closest("tr").addClass("selected");
         showFunctionOptions();
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onClick on roleAppRelationControl tr for addClass Selected and show tabs.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#roleAppRelationControl table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      var appCode = $(tbodyApp + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name=AppCode]").val();
      if (appCode === undefined)
      {
         return false;
      }

      $(tbodyApp).removeClass('selected');
      $(tbodyTab).removeClass('selected');
      $(tbodySeg).removeClass('selected');
      $(tbodyFunc).removeClass('selected');
      $(tbodyTab + ".relationDisplayRow").hide();
      $(tbodySeg + ".relationDisplayRow").hide();
      $(tbodyFunc + ".relationDisplayRow").hide();
      $(tbodyTab + ".relationEditRow td input[name='AppCode']")
          .each(function()
          {
             if ($(this).closest("input").val() === appCode)
             {
                var $tr = $(this).closest("tr");
                var rowId = $tr.attr("data-row-id");
                $(tbodyTab + ".relationDisplayRow[data-row-id=" + rowId + "]").closest("tr").show();
             }
          });

      $("#roleTabRelationControl table tbody").show();
      changeSelection($(this));
      if ($(tbodyTab + ".relationDisplayRow[data-row-id=" +
         $(tbodyTab + ".relationEditRow td input[name=AppCode][value='" + appCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $(tbodyTab + ".relationEditRow td input[name=AppCode][value=" + appCode + "]:first").closest("tr").attr("data-unique-selection")
         + "]").closest("tr").length > 0)
      {
         selectFirstRow($(tbodyTab + ".relationDisplayRow[data-row-id=" +
            $(tbodyTab + ".relationEditRow td input[name=AppCode][value='" + appCode + "']:first").closest("tr").attr("data-row-id")
            + "][data-unique-selection=" + $(tbodyTab + ".relationEditRow td input[name=AppCode][value=" + appCode + "]:first").closest("tr").attr("data-unique-selection")
            + "]").closest("tr"), "click");
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onClick on roleTabsRelationControl tr for addClass Selected and show Segments.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#roleTabRelationControl table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      var appCode = $(tbodyTab + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name='AppCode']").val();
      var tabCode = $(tbodyTab + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name='Code']").val();
      if (tabCode === undefined)
      {
         return false;
      }

      $(tbodySeg).removeClass('selected');
      $(tbodyFunc).removeClass('selected');
      $(tbodyFunc + ".relationDisplayRow").hide();
      $(tbodySeg + ".relationDisplayRow").hide();
      $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
         .each(function()
         {
            if ($(this).closest("tr").find("td input[name='TabCode']").val() === tabCode)
            {
               var $tr = $(this).closest("tr");
               var rowId = $tr.attr("data-row-id");
               $(tbodySeg + ".relationDisplayRow[data-row-id=" + rowId + "]").closest("tr").show();
            }
         });

      $("#roleSegmentRelationControlItems table tbody").show();
      changeSelection($(this));
      if ($(tbodySeg + ".relationDisplayRow[data-row-id=" +
         $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
         .closest("tr").find("td input[name='TabCode'][value='" + tabCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']").closest("tr")
         .find("td input[name='TabCode'][value='" + tabCode + "']:first").closest("tr").attr("data-unique-selection") + "]").closest("tr").length > 0)
      {
         selectFirstRow($(tbodySeg + ".relationDisplayRow[data-row-id=" +
         $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
         .closest("tr").find("td input[name='TabCode'][value='" + tabCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']").closest("tr")
         .find("td input[name='TabCode'][value='" + tabCode + "']:first").closest("tr").attr("data-unique-selection") + "]").closest("tr"), "click");
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   onClick on roleSegmentRelationControl tr for addClass Selected and show Functions.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#roleSegmentRelationControlItems table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }
      var appCode = $(tbodySeg + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name='AppCode']").val();
      var tabCode = $(tbodySeg + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name='TabCode']").val();
      var segCode = $(tbodySeg + "[data-row-id=" + $(e.target).closest("tr").attr("data-row-id") + "] [name='Code']").val();
      if (segCode === undefined)
      {
         return false;
      }

      $(tbodyFunc).removeClass('selected');
      $(tbodyFunc + ".relationDisplayRow").hide();
      $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
      .closest("tr").each(function()
      {
         if ($(this).find("td input[name='SegCode']").val() === segCode)
         {
            var $tr = $(this).closest("tr");
            var rowId = $tr.attr("data-row-id");
            $(tbodyFunc + ".relationDisplayRow[data-row-id=" + rowId + "]").closest("tr").show();
         }
      });

      $("#roleFunctionRelationControlItems table tbody").show();
      changeSelection($(this));
      if ($(tbodyFunc + ".relationDisplayRow[data-row-id=" +
         $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
         .closest("tr").find("td input[name='SegCode'][value='" + segCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']").closest("tr")
         .find("td input[name='SegCode'][value='" + segCode + "']:first").closest("tr").attr("data-unique-selection") + "]").closest("tr").length > 0)
      {
         selectFirstRow($(tbodyFunc + ".relationDisplayRow[data-row-id=" +
         $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']")
         .closest("tr").find("td input[name='SegCode'][value='" + segCode + "']:first").closest("tr").attr("data-row-id")
         + "][data-unique-selection=" + $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCode + "']").closest("tr")
         .find("td input[name='SegCode'][value='" + segCode + "']:first").closest("tr").attr("data-unique-selection") + "]").closest("tr"), "");
      }

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on function row in view mode.
   //*
   //* Parameters:
   //*   e                - Event for get row properties.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#roleFunctionRelationControlItems table tr.relationDisplayRow", function(e)
   {
      if (editMode === "Edit")
      {
         return false;
      }

      changeSelection($(this));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change appCode DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='AppCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var max = 0;
      var changedValue = this.value;
      var appCode = $(this).closest("tr").find("td input[name=Code]").val();
      var roleId = $("#searchGrid tr.selected td[data-field-name='RoleId']").attr("data-field-value");
      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow();
         $("select[name='AppCode'] option").each(function()
         {
            if ($(this).val() === appCode)
            {
               $(this).show();
            }
         });
      }

      else if (appCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name=Code]").val(changedValue);
         $("select[name='AppCode'] option")
            .each(function()
            {
               if ($(this).val() === appCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("tr").find("td input[name=AppCode]").attr("value", appCode);
         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").val(changedValue);
         $(this).closest("tr").find("input[type='image']").attr("name", changedValue);
         $(this).closest("tr").find("input[type='image']").addClass("image_tab_relation");
         $(this).closest("tr").find("input[type='image']").attr("data-field-type", "AppCode");
      }
      var tabObject = [];

      $(tbodyTab + ".relationEditRow td input[name='AppCode']," + tbodyTab + ".updated input[name='AppCode']")
      .closest("td").find("input[value='" + appCode + "']")
         .each(function()
         {
            $(this).deleteRelationRow();
         });
      $(tbodySeg + ".updated input[name='AppCode']," + tbodySeg + ".relationEditRow input[name='AppCode']")
      .closest("td").find("input[value='" + appCode + "']")
         .each(function()
         {
            $(this).deleteRelationRow();
         });

      $(tbodyFunc + ".updated input[name='AppCode']," + tbodyFunc + ".relationEditRow input[name='AppCode']")
       .closest("td").find("input[value='" + appCode + "']")
          .each(function()
          {
             $(this).deleteRelationRow();
          });






      $(tbodyApp).each(function()
      {
         if ($(this).hasClass("selected"))
         {
            $(this).removeClass("selected");
         }
      });
      $(this).closest("tr").addClass("selected");
      if ($(tbodyApp).hasClass("selected"))
      {
         $(this).closest("tr").find("input[type=image]").click();
      }

      else
      {
         $(tbodyTab).removeClass("selected");
         $(tbodyTab).hide();
         $(tbodySeg).removeClass("selected");
         $(tbodySeg).hide();
         $(tbodyFunc).removeClass("selected");
         $(tbodyFunc).hide();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change TabCode DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='TabCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var changedValue = this.value;
      var appCode = $(this).closest("tr").find("td input[name='AppCode']").val().length === 0
            ? ($(tbodyApp).hasClass("selected")
               ? $(tbodyApp + ".selected td select[name='AppCode']").val()
               : $(this).find("option:eq(0)").prop("selected", true))
            : $(this).closest("tr").find("td input[name='AppCode']").val();
      var tabCode = $(this).closest("tr").find("td input[name='Code']").val();

      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow();
         $("select[name='TabCode'] option").each(function()
         {
            if ($(this).val() === tabCode)
            {
               $(this).show();
            }
         });
      }

      else if (tabCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name=Code]").attr("value", changedValue);
         $(this).closest("tr").find("td input[name=AppCode]").attr("value", appCode);
         $("select[name='TabCode'] option")
            .each(function()
            {
               if ($(this).val() === tabCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").val(changedValue);
         $(this).closest("tr").find("input[type='image']").attr("name", appCode);
         $(this).closest("tr").find("input[type='image']").addClass("image_tab_relation");
         $(this).closest("tr").find("input[type='image']").attr("data-field-type", "TabCode");
      }


      var segmentObjects = [];
      $(tbodySeg + " td input[name='TabCode'][value='" + tabCode + "']")
         .closest("tr").find("input[name=AppCode][value='" + appCode + "']")
         .closest("tr")
         .each(function()
         {
            var $tr = $(this).closest("tr");
            var rowId = $tr.attr("data-row-id");
            if (rowId !== undefined)
            {
               $(tbodySeg + "[data-row-id=" + rowId + "]")
                  .each(function()
                  {
                     $(this).deleteRelationRow();
                  });
            }
            else
            {
               $(this).deleteRelationRow();
            }
            var segCodes = $(this).find("td input[name='Code']").val();
            $(tbodyFunc + ".relationAddRow.updated input[name='AppCode'][value='" + appCode + "'],"
            + tbodyFunc + ".relationEditRow input[name='AppCode'][value='" + appCode + "']")
            .closest("tr")
                    .each(function()
                    {
                       if ($(this).find("td input[name='SegCode']").val() === segCodes)
                       {
                          var $tr = $(this).closest("tr");
                          var rowId = $tr.attr("data-row-id");
                          $(tbodyFunc + "[data-row-id=" + rowId + "]")
                             .each(function()
                             {
                                $(this).deleteRelationRow();
                             });
                       }
                    });
         });

      $(tbodyTab + ".selected").removeClass("selected");
      if (appCode === "Add")
      {
         $(this).find("option:eq(0)").prop("selected", true);
         return false;
      }
      $(this).closest("tr").addClass("selected");
      $(tbodyTab).hasClass("selected")
         ? $(this).closest("tr").find("input[type=image]").click()
         : ($(tbodySeg).removeClass("selected"),
            $(tbodySeg).parent().hide(),
            $(tbodyFunc).parent().hide());



   });



   //*****************************************************************************
   //*
   //* Summary:
   //*   Change SegCode DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='SegCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var changedValue = this.value;
      var appCode = $(this).closest("tr").find("td input[name='AppCode']").val().length === 0
                  ? ($(tbodyApp).hasClass("selected")
                     ? $(tbodyApp + ".selected td select[name='AppCode']").val()
                     : $(this).find("option:eq(0)").prop("selected", true))
                  : $(this).closest("tr").find("td input[name='AppCode']").val();

      var segCode = $(this).closest("tr").find("td input[name='Code']").val();
      var tabCode = $(this).closest("tr").find("td input[name='TabCode']").val().length === 0
                  ? ($(tbodyTab).hasClass("selected")
                     ? $(tbodyTab + ".selected td select[name='TabCode']").val()
                     : $(this).find("option:eq(0)").prop("selected", true))
                  : $(this).closest("tr").find("td input[name='TabCode']").val();

      if (changedValue === "Delete")
      {
         changedValue = "";
         $(this).deleteRelationRow();
         $("select[name='SegCode'] option").each(function()
         {
            if ($(this).val() === segCode)
            {
               $(this).show();
            }
         });
      }

      else if (segCode !== changedValue)
      {
         $(this).closest("tr").find("td input[name=Code]").attr("value", changedValue);
         $(this).closest("tr").find("td input[name=TabCode]").attr("value", tabCode);
         $(this).closest("tr").find("td input[name=AppCode]").attr("value", appCode);
         $("select[name='SegCode'] option")
            .each(function()
            {
               if ($(this).val() === segCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("select").find("option[value='Delete']").show();
         $(this).closest("tr").find("input[type='image']").val(changedValue);
         $(this).closest("tr").find("input[type='image']").attr("name", appCode);
         $(this).closest("tr").find("input[type='image']").addClass("image_tab_relation");
         $(this).closest("tr").find("input[type='image']").attr("data-field-type", "SegCode");

      }

      $(tbodyFunc).find("td input[name='SegCode'][value='" + segCode + "']")
            .closest("tr").find("td input[name='AppCode'][value='" + appCode + "']")
            .each(function()
            {
               var $tr = $(this).closest("tr");
               var rowId = $tr.attr("data-row-id");
               $(tbodyFunc + "[data-row-id=" + rowId + "]")
                  .each(function()
                  {
                     $(this).deleteRelationRow();
                  });
            });

      $(tbodyFunc + ".relationAddRow.updated input[name='SegCode']")
         .closest("td").find(" input[value='" + segCode + "']").closest("tr")
         .find("td input[name='AppCode'][value='" + appCode + "']").closest("tr")
         .each(function()
         {
            $(this).deleteRelationRow();
         });

      $(tbodySeg + ".selected").removeClass("selected");
      if (tabCode === "Add")
      {
         $(this).find("option:eq(0)").prop("selected", true);
         return false;
      }
      $(this).closest("tr").addClass("selected");
      $(tbodySeg).hasClass("selected")
         ? $(this).closest("tr").find("input[type=image]").click()
         : ($(tbodyFunc).removeClass("selected"),
            $(tbodyFunc).parent().hide());

   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Change Function DropDown.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "[name='FuncCode']", function()
   {
      if (!window.getFormChanged())
      {
         window.setFormChanged(true);
      }

      var appCode = $(this).closest("tr").find("td input[name='AppCode']").val().length === 0
                  ? ($(tbodyApp).hasClass("selected")
                     ? $(tbodyApp + ".selected td select[name='AppCode']").val()
                     : $(this).find("option:eq(0)").prop("selected", true))
                  : $(this).closest("tr").find("td input[name='AppCode']").val();
      var changedValue = this.value;
      var segCode = $(this).closest("tr").find("td input[name='SegCode']").val().length === 0
                  ? ($(tbodySeg).hasClass("selected")
                     ? $(tbodySeg + ".selected td select[name='SegCode']").val()
                     : $(this).find("option:eq(0)").prop("selected", true))
                  : $(this).closest("tr").find("td input[name='SegCode']").val();
      var funcCode = $(this).closest("tr").find("td input[name=Code]").val();



      if (changedValue === "Delete")
      {
         $(this).deleteRelationRow();

         $("select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === funcCode)
               {
                  $(this).show();
               }
            });
      }

      else if (funcCode !== changedValue)
      {
         $("select[name='FuncCode'] option")
            .each(function()
            {
               if ($(this).val() === funcCode)
               {
                  $(this).show();
               }

               else if ($(this).val() === "Add")
               {
                  $(this).hide();
               }

               if ($(this).is(":selected") === false && $(this).val() === changedValue)
               {
                  $(this).hide();
               }
            });
         $(this).closest("tr").find("td input[name=Code]").attr("value", changedValue);
         $(this).closest('tr').find('td input[name=SegCode]').attr("value", segCode);
         $(this).closest('tr').find('td input[name=AppCode]').attr("value", appCode);
         $(this).closest("select").find("option[value='Delete']").show();

      }

      $(tbodyFunc).removeClass("selected");
      $(this).closest("tr").addClass("selected");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Show Tab Objects after clicking AppCode.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function showTabOptions(uniqSelection, rowId)
   {
      var tabObjects = [];
      if (!$(tbodyApp).hasClass("selected"))
      {
         $(tbodySeg).parent().hide();
         $(tbodyFunc).parent().hide();
         return false;
      }
      var appCodeValue = $(tbodyApp + ".selected").find("td select[name='AppCode']").val();
      $(tbodyTab + ".relationEditRow").hide();
      $(tbodyTab + ".relationEditRow td input[name='AppCode']")
         .each(function()
         {
            if ($(this).val() === appCodeValue)
            {
               tabObjects.push({ "tabCode": $(this).closest("tr").find("td select[name='TabCode']").val() });
               $(this).closest("tr").show();
            }

            else
            {
               $(this).closest("tr").hide();
            }
         });

      $(tbodyTab + ".relationAddRow.updated td input[name='AppCode']")
         .each(function()
         {
            if ($(this).val() === appCodeValue)
            {
               tabObjects.push({ "tabCode": $(this).closest("tr").find("td select[name='TabCode']").val() });
               $(this).closest("tr").show();
            }

            else
            {
               $(this).closest("tr").hide();
            }
         });

      $(tbodyTab + ".relationAddRow.updated td input[name='AppCode']")
      .closest("td").find("input[value='" + appCodeValue + "']").closest("tr")
      .find("select[name='TabCode'] option")
         .each(function()
         {
            tabObjects.push({ "tabCode": $(this).closest("tr").find("td select[name='TabCode']").val() });
            $(this).closest("tr").show();
         });

      var jsonObjects = JSON.parse(document.getElementById('RoleTabOption').value);
      $(tbodyTab + ".relationEditRow td input[name='AppCode']")
      .closest("input[value='" + appCodeValue + "']").closest("tr")
      .find("select[name='TabCode'] option")
         .each(function()
         {
            if ($(this).val() === "Add")
            {
               $(this).hide();
            }

            else
            {
               for (var j = 0; j < jsonObjects.length; j++)
               {
                  if ($(this).val() === jsonObjects[j].TabCode && $(this).val() !== "Delete")
                  {
                     if (jsonObjects[j].AppCode === appCodeValue)
                     {
                        $(this).show();
                        for (var i = 0; i < tabObjects.length; i++)
                        {
                           if ($(this).val() === tabObjects[i].tabCode && $(this).closest("select").val() !== $(this).val())
                           {
                              $(this).hide();
                           }
                        }
                        break;
                     }
                  }
                  else
                  {
                     if ($(this).val() === "Delete")
                     {
                        $(this).show();
                     }
                     else
                     {
                        $(this).hide();
                     }
                  }
               }
            }
         });


      $(tbodyTab + ".blank td select[name='TabCode'] option").show();
      $(tbodyTab + ".blank td select[name='TabCode'] option")
         .each(function()
         {
            if ($(this).val() !== "Add")
            {
               for (var j = 0; j < jsonObjects.length; j++)
               {
                  if ($(this).val() === jsonObjects[j].TabCode)
                  {
                     if (jsonObjects[j].AppCode === appCodeValue)
                     {
                        $(this).show();
                        for (var i = 0; i < tabObjects.length; i++)
                        {
                           if ($(this).val() === tabObjects[i].tabCode && $(this).closest("select").val() !== $(this).val())
                           {
                              $(this).hide();
                           }
                        }
                        break;
                     }
                  }
                  else
                  {
                     $(this).hide();
                  }
               }

               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            }
         });

      if (uniqSelection !== "" && rowId !== "" && rowId !== undefined)
      {
         $(tbodyTab).each(function()
         {
            if ($(this).attr("data-unique-selection") === uniqSelection && $(this).attr("data-row-id") === rowId && rowId !== undefined)
            {
               $(this).addClass("selected");
            }
         });
         if (!$(tbodyTab).hasClass("selected"))
         {
            $("#roleSegmentRelationControlItems .relationControl").disableRelationEdit();
         }
      }
      else if ($(tbodyTab + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']:first")
      .closest("tr").length > 0)
      {
         selectFirstRow($(tbodyTab + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']:first")
        .closest("tr").find("input.image_tab_relation").closest("input"), "click");
      }
      else
      {
         if ($(tbodyTab + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']:first")
         .closest("tr").length > 0)
         {
            selectFirstRow($(tbodyTab + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']:first")
           .closest("tr").find("input.image_tab_relation").closest("input"), "click");
         }
         else if ($(tbodyTab + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']:first")
         .closest("tr").length > 0)
         {
            selectFirstRow($(tbodyTab + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']:first")
           .closest("tr").find("input.image_tab_relation").closest("input"), "click");
         }
      }

   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Show Segment Objects after clicking TabCode.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function showSegmentOptions(uniqSelection, rowId)
   {
      var segmentObjects = [];
      if (!$(tbodyTab).hasClass("selected"))
      {
         $(tbodySeg).parent().hide();
         $(tbodyFunc).parent().hide();
         return false;
      }
      var appCodeValue = $(tbodyApp + ".selected").find("td select[name='AppCode']").val();
      var tabCodeValue = $(tbodyTab + ".selected").find("td input[name='Code']").val();
      $(tbodySeg + ".relationEditRow," + tbodySeg + ".relationAddRow.updated").hide();

      $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
         .each(function()
         {
            if ($(this).closest("tr").find("td input[name='TabCode']").val() === tabCodeValue)
            {
               segmentObjects.push({ "segCode": $(this).closest("tr").find("td select[name='SegCode']").val() });
               $(this).closest("tr").show();
            }

            else
            {
               $(this).closest("tr").hide();
            }
         });
      $(tbodySeg + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']")
       .each(function()
       {
          if ($(this).closest("tr").find("td input[name='TabCode']").val() === tabCodeValue)
          {
             segmentObjects.push({ "segCode": $(this).closest("tr").find("td select[name='SegCode']").val() });
             $(this).closest("tr").show();
          }

          else
          {
             $(this).closest("tr").hide();
          }
       });
      $(tbodyFunc).hide();

      var jsonObjects = JSON.parse(document.getElementById('RoleSegmentOption').value);


      $(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
            .closest("tr").find("td input[name='TabCode'][value='" + tabCodeValue + "']").closest("tr")
            .find("select[name='SegCode'] option")
               .each(function()
               {
                  if ($(this).val() === "Add")
                  {
                     $(this).hide();
                  }
                  else
                  {
                     for (var j = 0; j < jsonObjects.length; j++)
                     {
                        if ($(this).val() === jsonObjects[j].SegCode && $(this).val() !== "Delete")
                        {
                           if (jsonObjects[j].AppCode === appCodeValue
                                 && jsonObjects[j].TabCode === tabCodeValue)
                           {
                              $(this).show();
                              for (var i = 0; i < segmentObjects.length; i++)
                              {
                                 if ($(this).val() === segmentObjects[i].segCode
                                    && $(this).closest("select").val() !== $(this).val())
                                 {
                                    $(this).hide();
                                 }
                              }
                              break;
                           }
                        }
                        else
                        {
                           if ($(this).val() === "Delete")
                           {
                              $(this).show();
                           }
                           else
                           {
                              $(this).hide();
                           }
                        }
                     }
                  }
               });


      $(tbodySeg + ".blank td select[name='SegCode'] option").show();
      $(tbodySeg + ".blank td select[name='SegCode'] option")
         .each(function()
         {

            if ($(this).val() !== "Add")
            {
               for (var j = 0; j < jsonObjects.length; j++)
               {

                  if ($(this).val() === jsonObjects[j].SegCode)
                  {
                     if (jsonObjects[j].AppCode === appCodeValue
                           && jsonObjects[j].TabCode === tabCodeValue)
                     {
                        $(this).show();
                        for (var i = 0; i < segmentObjects.length; i++)
                        {
                           if ($(this).val() === segmentObjects[i].segCode
                              && $(this).closest("select").val() !== $(this).val())
                           {
                              $(this).hide();
                           }
                        }
                        break;
                     }
                  }
                  else
                  {
                     $(this).hide();
                  }
               }

               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            }
         });

      if (uniqSelection !== "" && rowId !== "" && rowId !== undefined)
      {
         $(tbodySeg).each(function()
         {
            if ($(this).attr("data-unique-selection") === uniqSelection && $(this).attr("data-row-id") === rowId && rowId !== undefined)
            {
               $(this).addClass("selected");
            }
         });
         if (!$(tbodySeg).hasClass("selected"))
         {
            $("#roleFunctionRelationControlItems .relationControl").disableRelationEdit();
         }
      }
      else
      {
         if ($(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
               .closest("tr").find("td input[name='TabCode'][value='" + tabCodeValue + "']:first").length > 0)
         {
            selectFirstRow($(tbodySeg + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
               .closest("tr").find("td input[name='TabCode'][value='" + tabCodeValue + "']:first")
               .closest("tr").first().find("input.image_tab_relation").closest("input"), "click");
         }
         else if ($(tbodySeg + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']")
               .closest("tr").find("td input[name='TabCode'][value='" + tabCodeValue + "']:first").length > 0)
         {
            selectFirstRow($(tbodySeg + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']")
               .closest("tr").find("td input[name='TabCode'][value='" + tabCodeValue + "']:first")
               .closest("tr").first().find("input.image_tab_relation").closest("input"), "click");
         }


      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Show Function Objects after clicking SegCode.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function showFunctionOptions(uniqSelection, rowId)
   {
      var functionObjects = [];
      if (!$(tbodySeg).hasClass("selected"))
      {
         return false;
      }
      var appCodeValue = $(tbodyApp + ".selected").find("td select[name='AppCode']").val();
      var segCodeValue = $(tbodySeg + ".selected").find("td input[name='Code']").val();
      $(tbodyFunc + ".relationEditRow").hide();
      $(tbodyFunc + ".relationAddRow.updated").hide();
      $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
         .each(function()
         {
            if ($(this).closest("tr").find("td input[name='SegCode']").val() === segCodeValue)
            {
               functionObjects.push({ "funcCode": $(this).closest("tr").find("td select[name='FuncCode']").val() });
               $(this).closest("tr").show();
            }

            else
            {
               $(this).closest("tr").hide();
            }
         });

      $(tbodyFunc + ".relationAddRow.updated td input[name='AppCode'][value='" + appCodeValue + "']")
         .closest("tr").find("td input[name='SegCode'][value='" + segCodeValue + "']")
         .each(function()
         {
            functionObjects.push({ "funcCode": $(this).closest("tr").find("td select[name='FuncCode']").val() });
            $(this).closest("tr").show();
         });

      var jsonObjects = JSON.parse(document.getElementById('RoleFunctionOption').value);

      $(tbodyFunc + ".relationEditRow td input[name='AppCode'][value='" + appCodeValue + "']")
            .closest("tr").find("td input[name='SegCode'][value='" + segCodeValue + "']").closest("tr")
            .find("select[name='FuncCode'] option")
               .each(function()
               {
                  if ($(this).val() === "Add")
                  {
                     $(this).hide();
                  }
                  else
                  {
                     for (var j = 0; j < jsonObjects.length; j++)
                     {
                        if ($(this).val() === jsonObjects[j].FuncCode && $(this).val() !== "Delete")
                        {
                           if (jsonObjects[j].AppCode === appCodeValue
                                 && jsonObjects[j].SegCode === segCodeValue)
                           {
                              $(this).show();
                              for (var i = 0; i < functionObjects.length; i++)
                              {
                                 if ($(this).val() === functionObjects[i].funcCode
                                    && $(this).closest("select").val() !== $(this).val())
                                 {
                                    $(this).hide();
                                 }
                              }
                              break;
                           }
                        }
                        else
                        {
                           if ($(this).val() === "Delete")
                           {
                              $(this).show();
                           }
                           else
                           {
                              $(this).hide();
                           }
                        }
                     }
                  }
               });

      $(tbodyFunc + ".blank td select[name='FuncCode'] option").show();
      $(tbodyFunc + ".blank td select[name='FuncCode'] option")
         .each(function()
         {
            if ($(this).val() !== "Add")
            {
               for (var j = 0; j < jsonObjects.length; j++)
               {
                  if ($(this).val() === jsonObjects[j].FuncCode)
                  {
                     if (jsonObjects[j].AppCode === appCodeValue
                           && jsonObjects[j].SegCode === segCodeValue)
                     {
                        $(this).show();
                        for (var i = 0; i < functionObjects.length; i++)
                        {
                           if ($(this).val() === functionObjects[i].funcCode
                              && $(this).closest("select").val() !== $(this).val())
                           {
                              $(this).hide();
                           }
                        }
                        break;
                     }
                  }
                  else
                  {
                     $(this).hide();
                  }
               }

               if ($(this).val() === "Delete")
               {
                  $(this).hide();
               }
            }
         });
      if (uniqSelection !== "" && rowId !== "" && rowId !== undefined)
      {
         $(tbodyFunc + ".relationEditRow").each(function()
         {
            if ($(this).attr("data-unique-selection") === uniqSelection && $(this).attr("data-row-id") === rowId && rowId !== undefined)
            {
               $(this).addClass("selected");
            }
         });
      }
      else if ($(tbodyFunc + ".relationAddRow.updated td input[name='SegCode'][value='" + segCodeValue + "']")
         .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").closest("tr").length > 0)
      {
         selectFirstRow($(tbodyFunc + ".relationAddRow.updated td input[name='SegCode'][value='" + segCodeValue + "']")
            .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").first().closest("tr"), "");
      }
      else
      {
         if ($(tbodyFunc + ".relationEditRow td input[name='SegCode'][value='" + segCodeValue + "']")
               .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").closest("tr").length > 0)
         {
            selectFirstRow($(tbodyFunc + ".relationEditRow td input[name='SegCode'][value='" + segCodeValue + "']")
               .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").first().closest("tr"), "");
         }
         else if ($(tbodyFunc + ".relationAddRow.updated td input[name='SegCode'][value='" + segCodeValue + "']")
               .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").closest("tr").length > 0)
         {
            selectFirstRow($(tbodyFunc + ".relationAddRow.updated td input[name='SegCode'][value='" + segCodeValue + "']")
               .closest("tr").find("td input[name='AppCode'][value='" + appCodeValue + "']").first().closest("tr"), "");
         }
      }




   }
});

