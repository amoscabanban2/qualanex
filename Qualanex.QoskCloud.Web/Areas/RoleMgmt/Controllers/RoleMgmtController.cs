﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="RoleMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.Controllers
{
   using System;
   using System.Linq;
   using System.Web.Mvc;
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Controllers;
   using Qualanex.QoskCloud.Web.Areas.RoleMgmt.ViewModels;
   using Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "ADMIN_ROLEMGMT")]
   public class RoleMgmtController : Controller, IQoskController, IUserProfileController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult RoleMgmtView()
      {
         var viewModel = new RoleMgmtViewModel
         {
            Entitys = RoleEntity.GetRoleDetails(false),
            //RoleMgmtMenuEntries = UserModel.GetUserMenuEntries()
         };

         if (viewModel.Entitys.Any())
         {
            viewModel.SelectedEntity = viewModel.Entitys[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateRoleDetail(string roleEntity)
      {
         var model = System.Web.Helpers.Json.Decode<RoleEntity>(roleEntity);

         return this.PartialView("_RoleMgmtForm", model);
      }




      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }







      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted">Show all records</param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         var viewModel = new RoleMgmtViewModel
         {
            Entitys = RoleEntity.GetRoleDetails(false),
            ActionMenu = UserModel.GetActionMenu()
         };

         return this.PartialView("_RoleMgmtSummary", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   GetGroupName by input.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult GetGroupName(string strData, int keyPressValue, string browser)
      {
         var groupEntity = string.IsNullOrWhiteSpace(strData)
                              ? new List<RoleEntity>()
                              : RoleEntity.GetGroupList(false, strData, browser);

         var jsonResult = this.Json(new { groupEntity = groupEntity, keyPressValue = keyPressValue }, JsonRequestBehavior.AllowGet);
         jsonResult.MaxJsonLength = int.MaxValue;

         return jsonResult;
      }

      ///****************************************************************************
      /// <summary>
      ///   save profile for each groups.
      /// </summary>
      /// <param name="profileEntity"></param>
      /// <param name="groupId"></param>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult SaveRoleDetail(int roleId, string bodyData, bool isDeleted)
      {
         var curUser = this.HttpContext.User.Identity.Name;
         var max = roleId;
         var checkValue = RoleEntity.GetOneValue(bodyData, "ID_ROLENAME");
         var checkResult = RoleEntity.CheckRoleName(checkValue, roleId);

         if (!string.IsNullOrWhiteSpace(checkResult))
         {
            return this.Json(new
            {
               Success = false,
               Error = "",
               max = 0
            }, JsonRequestBehavior.AllowGet);
         }

         checkValue = RoleEntity.GetOneValue(bodyData, "ID_GROUPCODE");

         if (!string.IsNullOrWhiteSpace(checkValue))
         {
            var pCode = RoleEntity.CheckGroupCode(checkValue);

            if (pCode == 0)
            {
               var errorMessage = "The Group code '" + checkValue +
                  "' does not exist. Please try another profile code.";

               if (!string.IsNullOrWhiteSpace(errorMessage))
               {
                  return this.Json(new
                  {
                     Success = false,
                     Error = errorMessage,
                     max = 0
                  }, JsonRequestBehavior.AllowGet);
               }
            }
         }

         var blnCheck = roleId == 0
                            ? RoleEntity.NewRole(curUser, bodyData, isDeleted)
                            : RoleEntity.EditRole(curUser, roleId, bodyData, isDeleted);

         if (RoleEntity.GetOneValue(bodyData, "ID_STATUS") == "0")
         {
            if (roleId == 0)
            {
               max = RoleEntity.GetMax(false);
            }
         }
         else
         {
            if (roleId > 0)
            {
               if (!isDeleted)
               {
                  max = RoleEntity.GetMin(false);
               }
            }
            else
            {
               max = isDeleted
                        ? RoleEntity.GetMax(true)
                        : RoleEntity.GetMin(false);
            }
         }

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck,
            max = max
         }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Check Group Type exist in DB or not.
      /// </summary>
      /// <param name="roleId"></param>
      /// <param name="roleName"></param>
      /// <returns>
      ///   Error message if Role Name does not exist in the DB, otherwise an
      ///   empty string.
      /// </returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult Check_With_DB(int roleId, string roleName)
      {
         var input = "";

         try
         {
            var chkResult = RoleEntity.CheckRoleName(roleName, roleId);

            if (!string.IsNullOrWhiteSpace(chkResult))
            {
               var errorMessage = "The group name '" + roleName +
                  "' has been taken. Please try another group name.";

               input = "ID_ROLENAME";

               if (!string.IsNullOrWhiteSpace(errorMessage))
               {
                  return this.Json(new
                  {
                     Success = false,
                     Error = errorMessage,
                     Input = input
                  }, JsonRequestBehavior.AllowGet);
               }
            }

            return this.Json(new
            {
               Success = true,
               Error = "",
               Input = ""
            }, JsonRequestBehavior.AllowGet);
         }
         catch (Exception ex)
         {
            return this.Json(new
            {
               Success = false,
               Error = ex.Message,
               Input = input
            }, JsonRequestBehavior.AllowGet);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnChangeUserProfileCode(int profileCode)
      {
         return this.Json(new { Success = UserProfileController.OnChangeUserProfileCode(this.HttpContext, profileCode) }, JsonRequestBehavior.AllowGet);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="roleCode"></param>
      /// <param name="groupCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult RoleConfiguration(string roleCode,string groupCode)
      {
         var viewModel = new RoleMgmtViewModel
         {
            AppRelationObjects = new RoleMgmtRelationControl
            {
               RoleConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        { nameof(RoleEntity.AppCode),
                           RoleEntity.GetAppDictLists(Convert.ToInt32(groupCode),Convert.ToInt32(roleCode), false) }
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(RoleEntity.Code),
                        nameof(RoleEntity.IsDisposed),
                        nameof(RoleEntity.GroupName),
                        nameof(RoleEntity.GroupId),
                        nameof(RoleEntity.GroupType),
                        nameof(RoleEntity.TabCode),
                        nameof(RoleEntity.SegCode),
                        nameof(RoleEntity.FuncCode),
                        nameof(RoleEntity.DisplayFormat),
                        nameof(RoleEntity.Description),
                        nameof(RoleEntity.IsDeleted),
                        nameof(RoleEntity.RoleId),
                        nameof(RoleEntity.RoleName),
                        nameof(RoleEntity.Status),
                        nameof(RoleEntity.SeqNo)
                     },
                     UpdateMethod = "console.log('not yet implemented');",
                     AddMethod = this.Url.Action(nameof(RoleEntity)),
                     InitialReadOnly = true,
                     RowProperty = new RoleEntity(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView = "_RoleMgmtConfigCustomControl",
                           AddView = "_RoleMgmtConfigCustomControl",
                           ReadOnlyView = ""
                        }
                     }
                  },
                  Rows = RoleEntity.GetAppList(Convert.ToInt32(roleCode), false)
               },
               RoleConfigRelationControlOptions=RoleEntity.GetTabOptions(Convert.ToInt32(groupCode))
            },
            TabRelationObjects = new RoleMgmtRelationControl
            {
               RoleConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        { nameof(RoleEntity.TabCode),
                           RoleEntity.GetTabDictLists(Convert.ToInt32(groupCode),Convert.ToInt32(roleCode), false) }
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(RoleEntity.Code),
                        nameof(RoleEntity.IsDisposed),
                        nameof(RoleEntity.GroupName),
                        nameof(RoleEntity.GroupId),
                        nameof(RoleEntity.GroupType),
                        nameof(RoleEntity.AppCode),
                        nameof(RoleEntity.SegCode),
                        nameof(RoleEntity.FuncCode),
                        nameof(RoleEntity.DisplayFormat),
                        nameof(RoleEntity.Description),
                        nameof(RoleEntity.IsDeleted),
                        nameof(RoleEntity.RoleId),
                        nameof(RoleEntity.RoleName),
                        nameof(RoleEntity.Status),
                        nameof(RoleEntity.SeqNo)
                     },
                     UpdateMethod = "console.log('not yet implemented');",
                     AddMethod = this.Url.Action(nameof(RoleEntity)),
                     InitialReadOnly = true,
                     RowProperty = new RoleEntity(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView = "_RoleMgmtConfigCustomControl",
                           AddView = "_RoleMgmtConfigCustomControl",
                           ReadOnlyView = ""
                        }
                     }
                  },
                  Rows = RoleEntity.GetTabList(Convert.ToInt32(roleCode), false)
               },
               RoleConfigRelationControlOptions=RoleEntity.GetSegmentOptions(Convert.ToInt32(groupCode))
            },
            SegmentRelationObjects = new RoleMgmtRelationControl
            {
               RoleConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        { nameof(RoleEntity.SegCode), RoleEntity.GetSegmentDictLists(Convert.ToInt32(groupCode),Convert.ToInt32(roleCode))}
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(RoleEntity.Code),
                        nameof(RoleEntity.IsDisposed),
                        nameof(RoleEntity.GroupName),
                        nameof(RoleEntity.GroupId),
                        nameof(RoleEntity.GroupType),
                        nameof(RoleEntity.TabCode),
                        nameof(RoleEntity.AppCode),
                        nameof(RoleEntity.FuncCode),
                        nameof(RoleEntity.DisplayFormat),
                        nameof(RoleEntity.Description),
                        nameof(RoleEntity.IsDeleted),
                        nameof(RoleEntity.RoleId),
                        nameof(RoleEntity.RoleName),
                        nameof(RoleEntity.Status),
                        nameof(RoleEntity.SeqNo)
                     },
                     UpdateMethod = "console.log('not yet implemented'); ",
                     AddMethod = this.Url.Action(nameof(RoleEntity)),
                     InitialReadOnly = true,
                     RowProperty = new RoleEntity(),
                     CustomControls = new List<Model.CustomControl>
                     {
                        new Model.CustomControl
                        {
                           EditView = "_RoleMgmtConfigCustomControl",
                           AddView = "_RoleMgmtConfigCustomControl",
                           ReadOnlyView = ""
                        }
                     }
                  },
                  Rows = RoleEntity.GetSegmentList(Convert.ToInt32(roleCode), false)
               },
               RoleConfigRelationControlOptions = RoleEntity.GetFunctionOptions(Convert.ToInt32(groupCode))
            },
            FunctionRelationObjects = new RoleMgmtRelationControl
            {
               RoleConfigRelationControl = new Model.RelationControl
               {
                  Options = new Model.RelationControlOptions
                  {
                     SelectLists = new Dictionary<string, IEnumerable<SelectListItem>>
                     {
                        {
                        nameof(RoleEntity.FuncCode), RoleEntity.GetFunctionDictsLists(Convert.ToInt32(groupCode),Convert.ToInt32(roleCode))
                        }
                     },
                     ShowRowControls = false,
                     HiddenProperties =
                     {
                        nameof(RoleEntity.Code),
                        nameof(RoleEntity.IsDisposed),
                        nameof(RoleEntity.GroupName),
                        nameof(RoleEntity.GroupId),
                        nameof(RoleEntity.GroupType),
                        nameof(RoleEntity.TabCode),
                        nameof(RoleEntity.SegCode),
                        nameof(RoleEntity.AppCode),
                        nameof(RoleEntity.DisplayFormat),
                        nameof(RoleEntity.Description),
                        nameof(RoleEntity.IsDeleted),
                        nameof(RoleEntity.RoleId),
                        nameof(RoleEntity.RoleName),
                        nameof(RoleEntity.Status),
                        nameof(RoleEntity.SeqNo)
                     },
                     UpdateMethod = "console.log('not yet implemented'); ",
                     AddMethod = this.Url.Action(nameof(RoleEntity)),
                     InitialReadOnly = true,
                     RowProperty = new RoleEntity()
                  },
                  Rows = RoleEntity.GetFunctionList(Convert.ToInt32(roleCode), false)
               }
            }
         };

         return this.PartialView("_RoleMgmtConfiguration", viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Save RoleMgmtConfiguration changes.
      /// </summary>
      /// <param name="roleConfigEntity"></param>
      /// <returns></returns>
      ///****************************************************************************
      [HttpPost]
      [ValidateAntiForgeryToken]
      public ActionResult SaveRoleConfigDetail(string roleConfigEntity)
      {
         var blnCheck = RoleEntity.SetConfiguration(
            System.Web.Helpers.Json.Decode<List<RoleEntity>>(roleConfigEntity),
            this.HttpContext.User.Identity.Name);

         return this.Json(new
         {
            Success = blnCheck,
            Error = !blnCheck
         }, JsonRequestBehavior.AllowGet);
      }

   }
}
