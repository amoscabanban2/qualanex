using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models;
using Qualanex.QoskCloud.Web.Common;

namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.ViewModels
{
    //using Qualanex.QoskCloud.Web.Areas.Product.Models;

    public class UserDetailViewModel
    {
        public UserEntity SelectedUserDetail { get; set; } = new UserEntity(); //used to pull only the first user record out.
        public List<UserEntity> UsersDetails { get; set; } = new List<UserEntity>(); //User list for the User detail panel
        public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel(); //Used to populate the three dot menu data
    }

}