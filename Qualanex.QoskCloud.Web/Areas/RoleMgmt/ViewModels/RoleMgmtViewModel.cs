﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="RoleMgmtViewModel.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.RoleMgmt.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Areas.RoleMgmt.Models;
   using Qualanex.QoskCloud.Web.Common;
   using Qualanex.QoskCloud.Web.Model;
   using Qualanex.QoskCloud.Web.ViewModels;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class RoleMgmtViewModel : QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public override string AppletKey { get; } = "ADMIN_ROLEMGMT";

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to pull only the first RoleConfig record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RoleEntity SelectedEntity { get; set; } = new RoleEntity();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RoleConfig list for the RoleConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<RoleEntity> Entitys { get; set; } = new List<RoleEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RoleConfig list for dropdown list the RoleConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<RoleEntity> RoleMgmtModelsItems { get; set; } = new List<RoleEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   RoleConfig list for dropdown list the RoleConfig detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<RoleEntity> GroupLists { get; set; } = new List<RoleEntity>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Used to populate the three dot menu data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get RoleConfig and Relation Control data for Tab
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RoleMgmtRelationControl AppRelationObjects { get; set; } = new RoleMgmtRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get RoleConfig and Relation Control data for Tab
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RoleMgmtRelationControl TabRelationObjects { get; set; } = new RoleMgmtRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get RoleConfig and Relation Control data for Segment
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RoleMgmtRelationControl SegmentRelationObjects { get; set; } = new RoleMgmtRelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get RoleConfig and Relation Control data for Function
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RoleMgmtRelationControl FunctionRelationObjects { get; set; } = new RoleMgmtRelationControl();
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class RoleMgmtRelationControl
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get Relation Control data
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public RelationControl RoleConfigRelationControl { get; set; } = new RelationControl();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get options from group for each parent
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<RoleEntity> RoleConfigRelationControlOptions { get; set; } = new List<RoleEntity>();
   }
}
