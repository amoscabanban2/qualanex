﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="RecallMgmtController.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.RecallMgmt.Controllers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;
   using System;

   using Qualanex.QoskCloud.Web.Areas.RecallMgmt.ViewModels;
   using Qualanex.QoskCloud.Web.Controllers;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   [Authorize(Roles = "PROD_RECALL")]
   public class RecallMgmtController : Controller, IQoskController
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult RecallMgmtView()
      {
         this.ModelState.Clear();

         var viewModel = new RecallMgmtViewModel
         {
            RecallDetails = RecallMgmtModel.GetRecallLists(false),
            ActionMenu = RecallMgmtModel.GetActionMenu()
         };

         if (viewModel.RecallDetails.Any())
         {
            viewModel.SelectedEntity = viewModel.RecallDetails[0];
         }

         return this.View(viewModel);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="recallDetails"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult UpdateRecallDetail(string recallDetails)
      {
         this.ModelState.Clear();

         var viewModel = new RecallMgmtViewModel
         {
            RecallDetails = RecallMgmtModel.GetRecallLists(false),
            ActionMenu = RecallMgmtModel.GetActionMenu()
         };

         if (viewModel.RecallDetails.Any())
         {
            viewModel.SelectedEntity = viewModel.RecallDetails[0];
         }

         return this.PartialView("_RecallMgmtForm", viewModel.SelectedEntity);
      }





      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityInsert(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityUpdate(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityDelete(string entity)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="entity"></param>
      /// <returns></returns>
      ///****************************************************************************
      public ActionResult OnMgmtEntityRefresh(string entity)
      {
         return null;
      }







      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="isDeleted"></param>
      /// <returns></returns>
      ///****************************************************************************
      [ValidateAntiForgeryToken]
      public ActionResult ViewDataRequest(bool isDeleted)
      {
         this.ModelState.Clear();

         var viewModel = new RecallMgmtViewModel
         {
            RecallDetails = RecallMgmtModel.GetRecallLists(false),
            ActionMenu = RecallMgmtModel.GetActionMenu()
         };

         if (viewModel.RecallDetails.Any())
         {
            viewModel.SelectedEntity = viewModel.RecallDetails[0];
         }

         return this.PartialView("_RecallMgmtSummary", viewModel);
      }
   }
}

