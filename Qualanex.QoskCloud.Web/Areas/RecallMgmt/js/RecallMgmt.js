﻿$(function()
{
   //*****************************************************************************************
   //*
   //* RecallMgmt.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the RecallMgmt partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************

   var recallId;
   var table;
   var validate = "Click";
   var cancel = false;
   var txtChanges = false;
   var valid = false;
   var baseURL = "/RecallMgmt";
   var active = "";
   var keyPressVal = 0;

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   $(document).ready(function()
   {
      table = window.dtHeaders("#searchGrid", '"order": [[1, "asc"]]', [], []);
      initialState();
      $("#ID_DESTRUCTION,#ID_START,#ID_CREDIT,#ID_END").datepicker();
   });

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Readonly mode and select first row of grid.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function initialState()
   {
      setEditMode(false);

      if (!$("#searchGrid tbody").hasClass("selected"))
      {
         $("#searchGrid tbody tr:eq(1)").unbind("click").click();
      }
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Handle when a Recalls's detail is selected form the table.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   $(document).on("click", ".RecallDetail-div-click", function()
   {
      if (!getEditMode())
      {
         table = table === undefined ? window.dtHeaders("#searchGrid", "", [], []) : table;

         if (!cancel)
         {
            if (!$(this).hasClass("selected"))
            {
               $("#searchGrid tr.selected").removeClass("selected");
            }

            $(this).closest("tr").find("td").each(function()
            {
               if ($(this).attr("data-field-name") === "Description")
               {
                  setData("#ID_DESCRIPTION", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "ProfileName")
               {
                  setData("#ID_MANUFACTORER", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "StartDate")
               {
                  setData("#ID_START", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "EndDate")
               {
                  setData("#ID_END", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "CreditDate")
               {
                  setData("#ID_CREDIT", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "RecallClass")
               {
                  setData("#ID_CLASS", $(this).attr("data-field-value"));
               }
               else if ($(this).attr("data-field-name") === "DestructionDate")
               {
                  setData("#ID_DESTRUCTION", $(this).attr("data-field-value"));
               }
            });

            $(this).closest("tr").addClass("selected");
            recallId = $(this).closest("tr").attr("data-row-id");
         }
         else
         {
            if (recallId === 0)
            {
               if (!$(this).hasClass("selected"))
               {
                  $("#searchGrid tr.selected").removeClass("selected");
               }

               if ($("#searchGrid tr").length > 2)
               {
                  recallId = $("#searchGrid tr:eq(2)").closest("tr").attr("data-row-id");
               }
            }

            if (recallId > 0)
            {
               $("#searchGrid tr[data-row-id=" + groupId + "]").closest("tr")
                  .find("td")
                  .each(function()
                  {
                     if ($(this).attr("data-field-name") === "Description")
                     {
                        setData("#ID_DESCRIPTION", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "ProfileName")
                     {
                        setData("#ID_MANUFACTORER", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "StartDate")
                     {
                        setData("#ID_START", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "EndDate")
                     {
                        setData("#ID_END", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "CreditDate")
                     {
                        setData("#ID_CREDIT", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "RecallClass")
                     {
                        setData("#ID_CLASS", $(this).attr("data-field-value"));
                     }
                     else if ($(this).attr("data-field-name") === "DestructionDate")
                     {
                        setData("#ID_DESTRUCTION", $(this).attr("data-field-value"));
                     }
                  });

               if (!$("#searchGrid tr[data-row-id=" + recallId + "]")
                  .closest("tr")
                  .hasClass("selected"))
               {
                  $("#searchGrid tr[data-row-id=" + recallId + "]")
                     .closest("tr")
                     .addClass("selected");
               }
            }
            cancel = false;
         }
         
      }

      return false;
   });   

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Set data from grid to inputs.
   //*
   //* Parameters:
   //*   inputId - Description not available.
   //*   value   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************************
   function setData(inputId, value)
   {
      $(inputId).val(value);
   }

   //#############################################################################
   //#   Actionbar event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.OnQnexRefresh = function()
   {
      if (!getEditMode())
      {
         var url = "ViewDataRequest";
         var isDelete = true;
         var attSrc = $("#CMD_RETIRED").attr("src");

         if (attSrc.indexOf("HideRetired(1)") >= 0)
         {
            isDelete = false;
         }

         $.ajax({
            type: "POST",
            url: url,
            data:
               {
                  isDeleted: isDelete, __RequestVerificationToken:
                     $("#__AjaxAntiForgeryForm [name=__RequestVerificationToken]").val()
               },
            success: function(data)
            {
               var $target = $("#RecallGridSummary");
               var $newHtml = $(data);

               $target.replaceWith($newHtml);
               table = window.dtHeaders("#searchGrid", "", [], []);
               $("#searchGrid tbody tr:eq(1)").click();
            }
         });

         return true;
      }
   }
});
