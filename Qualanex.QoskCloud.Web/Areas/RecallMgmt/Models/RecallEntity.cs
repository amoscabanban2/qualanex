///-----------------------------------------------------------------------------------------------
/// <copyright file="RecallEntity.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.RecallMgmt.Models
{
    using System;
    using System.ComponentModel;

    using Qualanex.Qosk.Library.Common.CodeCorrectness;

    using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

    ///============================================================================
    /// <summary>
    ///   Summary not available.
    /// </summary>
    ///----------------------------------------------------------------------------
    public class RecallEntity
   {
        private readonly object _disposedLock = new object();

        private bool _isDisposed;

        #region Constructors

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///   Summary not available.
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        public RecallEntity()
        {
        }

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /// <summary>
        ///   Summary not available.
        /// </summary>
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ~RecallEntity()
        {
            this.Dispose(false);
        }

        #endregion

        #region Properties

        ///////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///   Summary not available.
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////
        public bool IsDisposed
        {
            get
            {
                lock (this._disposedLock)
                {
                    return this._isDisposed;
                }
            }
        }

       #endregion

        #region Disposal

        ///****************************************************************************
        /// <summary>
        ///   Checks this object's Disposed flag for state.
        /// </summary>
        ///****************************************************************************
        private void CheckDisposal()
        {
            ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
        }

        ///****************************************************************************
        /// <summary>
        ///   Performs the object specific tasks for resource disposal.
        /// </summary>
        ///****************************************************************************
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ///****************************************************************************
        /// <summary>
        ///   Performs object-defined tasks associated with freeing, releasing, or
        ///   resetting unmanaged resources.
        /// </summary>
        /// <param name="programmaticDisposal">
        ///   If true, the method has been called directly or indirectly.  Managed
        ///   and unmanaged resources can be disposed.  When false, the method has
        ///   been called by the runtime from inside the finalizer and should not
        ///   reference other objects.  Only unmanaged resources can be disposed.
        /// </param>
        ///****************************************************************************
        private void Dispose(bool programmaticDisposal)
        {
            if (!this._isDisposed)
            {
                lock (this._disposedLock)
                {
                    if (programmaticDisposal)
                    {
                        try
                        {
                            ;
                            ;  // TODO: dispose managed state (managed objects).
                            ;
                        }
                        catch (Exception ex)
                        {
                            Log.Write(LogMask.Error, ex.ToString());
                        }
                        finally
                        {
                            this._isDisposed = true;
                        }
                    }

                    ;
                    ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                    ;   // TODO: set large fields to null.
                    ;

                }
            }
        }

        #endregion

        #region Methods

        ///////////////////////////////////////////////////////////////////////////////
        ///   No methods defined for this class.
        ///////////////////////////////////////////////////////////////////////////////

        #endregion
    }
}
