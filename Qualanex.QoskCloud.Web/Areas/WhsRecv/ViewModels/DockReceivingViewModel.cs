///-----------------------------------------------------------------------------------------------
/// <copyright file="DockReceivingViewModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.WhsRecv.ViewModels
{
   using System.Collections.Generic;

   using Qualanex.QoskCloud.Web.Common;
   using Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class DockReceivingViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to pull only the first Dock Receiving record out.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public DockReceivingModel SelectedDockModel { get; set; } = new DockReceivingModel();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Dock Receiving list for the Dock Receiving detail panel.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public List<DockReceivingModel> DockReceivingDetails { get; set; } = new List<DockReceivingModel>();

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///  Used to populate the three dot menu data.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public ActionMenuModel ActionMenu { get; set; } = new ActionMenuModel();
   }

}