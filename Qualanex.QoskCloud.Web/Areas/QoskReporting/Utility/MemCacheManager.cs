﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility
{
    public static class MemCacheManager
    {
        private static ObjectCache Cache
        {
            get
            {
                return MemoryCache.Default;
            }
        }

        public static string GetDateAsString(DateTime? nullableDateTime)
        {
            return nullableDateTime.HasValue ? nullableDateTime.Value.ToString("yyyy-MM-dd") : string.Empty;
        }

        public static T Get<T>(string key, Func<T> acquire)
        {
            return Get(key, 60, acquire);
        }

        public static T Get<T>(string key, int cacheTime, Func<T> acquire)
        {
            if (IsSet(key))
            {
                return Get<T>(key);
            }
            else
            {
                var result = acquire();
                Set(key, result, cacheTime);
                return result;
            }
        }

        public static async Task<T> GetAsync<T>(string key, int cacheTime, Func<Task<T>> acquire)
        {
            if (IsSet(key))
            {
                return Get<T>(key);
            }
            else
            {
                var result = await acquire();
                Set(key, result, cacheTime);
                return result;
            }
        }

        public static T Get<T>(string key)
        {
            return (T)Cache[key];
        }

        public static void Set(string key, object data, int cacheTime)
        {
            if (data == null)
            {
                return;
            }

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTime);

            Cache.Add(new CacheItem(key, data), policy);
        }

        public static bool IsSet(string key)
        {
            return (Cache.Contains(key));
        }

        public static void Remove(string key)
        {
            Cache.Remove(key);
        }

        public static void Clear()
        {
            foreach (var item in Cache)
            {
                Remove(item.Key);
            }
        }
    }
}