﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common
{
   public class ImageSasUriModel
   {
      public string BlobGuid { get; set; }
      public string ContentType { get; set; }
      public string ContentName { get; set; }
      public string ContentSasURI { get; set; }
      public string ImageURI { get; set; }
      public string ReturnAuthorizationNumber { get; set; }
      public string ItemID { get; set; }
    }
}