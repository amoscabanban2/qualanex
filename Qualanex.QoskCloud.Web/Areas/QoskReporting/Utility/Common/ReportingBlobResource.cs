﻿using ImageResizer;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Async;
using Constants = Qualanex.QoskCloud.Utility.Constants;
using System.Threading;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common
{
    public static class ReportingBlobResource
    {      
        public static async Task<List<ImageSasUriModel>> GetBlobThumbnailsAndOriginalsSASUrlsAsync(CloudBlobClient imagesStorageClient, CloudBlobClient thumbnailStorageClient, string ItemGuid, SharedAccessBlobPolicy sasConstraints)
        {
            var loweredGuid = ItemGuid.ToLower();
            var containerNameThumbnail = loweredGuid + "-thumbnails";

            // Note: settings storage service properties should not be required
            //SetStorageClientServiceProperties(blobClient);

            var blobListingResult = new List<ImageSasUriModel>();

            BlobContinuationToken listBlobsToken = null;
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();
            do
            {
                var thumbsContainer = thumbnailStorageClient.GetContainerReference(containerNameThumbnail);
                if (await thumbsContainer.ExistsAsync())
                {
                    var listOfBlobThumbnails = await thumbsContainer.ListBlobsSegmentedAsync(listBlobsToken);
                    listBlobsToken = listOfBlobThumbnails.ContinuationToken;

                    foreach (var item in listOfBlobThumbnails.Results)
                    {
                        var thumbBlob = item as CloudBlockBlob;
                        if (thumbBlob != null && thumbBlob.Uri.AbsoluteUri.ToLower().Contains(loweredGuid))
                        {
                            // Note: settings blob properties should not be required
                            //blobThumbnails.Properties.ContentType = "image/jpeg";
                            //blobThumbnails.SetProperties();

                            var contentName = thumbBlob.Name.Replace("-thumbnail", "");
                            var sasUriModel = new ImageSasUriModel()
                            {
                                ContentSasURI = thumbBlob.Uri.AbsoluteUri + thumbBlob.GetSharedAccessSignature(sasConstraints),
                                ContentName = contentName,
                                ImageURI = await GetOriginalImageLinkAsync(imagesStorageClient, loweredGuid, contentName, sasConstraints)
                            };

                            blobListingResult.Add(sasUriModel);
                        }
                    }
                }

            } while (listBlobsToken != null);

            return blobListingResult;
        }

        private static void SetStorageClientServiceProperties(CloudBlobClient blobClient)
        {
            var serviceProperties = blobClient.GetServiceProperties();

            serviceProperties.Cors.CorsRules.Clear();

            serviceProperties.Cors.CorsRules.Add(new CorsRule()
            {
                AllowedHeaders = { Constants.Triple_Dot },
                AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
                AllowedOrigins = { Constants.Triple_Dot },
                ExposedHeaders = { Constants.Triple_Dot },
                MaxAgeInSeconds = 600,

            });

            blobClient.SetServiceProperties(serviceProperties);
        }

        private static async Task<string> GetOriginalImageLinkAsync(CloudBlobClient imagesStorageClient, string containerName, string blobName, SharedAccessBlobPolicy sasConstraints)
        {
            var container = imagesStorageClient.GetContainerReference(containerName);
            var originalBlob = await container.GetBlobReferenceFromServerAsync(blobName);
            return originalBlob.Uri.AbsoluteUri + originalBlob.GetSharedAccessSignature(sasConstraints);
        }

        public static async Task<AzureThumbInfo> GenerateAzureImageThumbsAsync(CancellationToken cancellationToken, AzureThumbInfo thumbInfo)
        {
            thumbInfo.StartTime = DateTime.Now;
            thumbInfo.Status = "Connecting...";

            //get image containers
            var accountName = "qoskimages";
            var connectString = ConfigurationManager.GetConnectionString(accountName);
            var storageAccount = CloudStorageAccount.Parse(connectString);
            var blobClient = storageAccount.CreateCloudBlobClient();

            thumbInfo.ContainersProcessed = 0;
            thumbInfo.ContainerSetsFetched = 0;
            thumbInfo.ContainersAlreadyExisted = 0;
            thumbInfo.ImagesCreated = 0;

            var targetAccountName = "qoskimagesthumbnails";
            var targetConnectString = ConfigurationManager.GetConnectionString(targetAccountName);
            CloudStorageAccount targetStorageAccount = CloudStorageAccount.Parse(targetConnectString);
            CloudBlobClient targetCloudBlobClient = targetStorageAccount.CreateCloudBlobClient();

            SetRuntime(thumbInfo);
            thumbInfo.Status = "Connected! Fetching Container names...";

            if (!thumbInfo.RunWithNoPrefix)
            {
                int prefixInt = int.Parse(thumbInfo.PrefixHex, System.Globalization.NumberStyles.HexNumber);
                int prefixLength = thumbInfo.PrefixHex.Length;
                thumbInfo.NextPrefixHex = (prefixInt + 1).ToString("x" + prefixLength);
            }

            BlobContinuationToken listContainersToken = null;
            var blobRequestOptions = new BlobRequestOptions();
            var operationContext = new OperationContext();
            do
            {
                SetRuntime(thumbInfo);

                var listOfContainers = await blobClient.ListContainersSegmentedAsync(
                    thumbInfo.PrefixHex, ContainerListingDetails.None, thumbInfo.ContainersPerFetch,
                    listContainersToken, blobRequestOptions, operationContext);
                listContainersToken = listOfContainers.ContinuationToken;

                if (cancellationToken.IsCancellationRequested || thumbInfo.Stop)
                    break;

                SetRuntime(thumbInfo);
                thumbInfo.ContainerSetsFetched++;
                thumbInfo.Status = string.Format("Fetched {0} set of container names! Processing containers...", thumbInfo.ContainerSetsFetched);

                var bag = new ConcurrentBag<string>();

                await listOfContainers.Results.ParallelForEachAsync(async container =>
                {
                    if ((!cancellationToken.IsCancellationRequested && !thumbInfo.Stop)
                        && (!thumbInfo.RunDaily || container.Properties.LastModified > thumbInfo.StartTime.Value.AddDays(-1).AddHours(-1)))
                    {
                        var processedContainer = await ProcessContainer(thumbInfo, blobClient, targetCloudBlobClient, container.Name);
                        bag.Add(processedContainer);
                    }
                }, maxDegreeOfParalellism: thumbInfo.MaxParallelThreads);
                var count = bag.Count;

            } while (listContainersToken != null && thumbInfo.ContainersProcessed < thumbInfo.MaxContainers && !cancellationToken.IsCancellationRequested && !thumbInfo.Stop);

            //return variables
            SetRuntime(thumbInfo);
            thumbInfo.EndTime = DateTime.Now;

            if (thumbInfo.Stop || cancellationToken.IsCancellationRequested)
            {
                thumbInfo.IsComplete = false;
                thumbInfo.Stop = true;
                thumbInfo.Status = cancellationToken.IsCancellationRequested ? "Cancelled by Azure IIS!" : "Stopped Manually!";
            }
            else
                thumbInfo.IsComplete = true;

            return thumbInfo;
        }

        private static async Task<string>ProcessContainer(AzureThumbInfo thumbInfo, CloudBlobClient blobClient, CloudBlobClient targetCloudBlobClient, string containerName)
        {
            SetRuntime(thumbInfo);
            thumbInfo.Status = string.Format("Fetched {0} set of container names! Processing container: {1}...", thumbInfo.ContainerSetsFetched, thumbInfo.ContainersProcessed);

            if (!containerName.Contains("webjobs") && IsGuid(containerName))
            {
                CloudBlobContainer sourceContainer = blobClient.GetContainerReference(containerName);

                CloudBlobContainer targetContainer = GetThumbnailsContainer(targetCloudBlobClient, containerName);

                // returns true if created, false if it already existed
                var containerCreated = await targetContainer.CreateIfNotExistsAsync();

                if (!containerCreated)
                {
                    // skip if thumbnails container already exists
                    thumbInfo.ContainersAlreadyExisted++;
                    if (!thumbInfo.RunForExistingContainers)
                        return containerName;
                }

                //copy images                    
                BlobContinuationToken listOfBlobsToken = null;
                var maxBlobs = 12;
                var blobCount = 0;
                do
                {
                    SetRuntime(thumbInfo);
                    var listOfBlobs = await sourceContainer.ListBlobsSegmentedAsync(listOfBlobsToken);
                    listOfBlobsToken = listOfBlobs.ContinuationToken;

                    foreach (var item in listOfBlobs.Results)
                    {
                        SetRuntime(thumbInfo);
                        blobCount++;
                        thumbInfo.Status = string.Format("Fetched {0} set of container names! Processing container: {1}. Processing blob: {2}...", thumbInfo.ContainerSetsFetched, thumbInfo.ContainersProcessed, blobCount);

                        var sourceBlob = item as CloudBlockBlob;
                        if (sourceBlob != null)
                        {
                            await GenerateThumbnailForBlob(sourceBlob, targetContainer);
                            thumbInfo.ImagesCreated++;
                        }
                    }
                } while (listOfBlobsToken != null && blobCount < maxBlobs);
            }

            SetRuntime(thumbInfo);
            thumbInfo.ContainersProcessed++;
            return containerName;
        }

        public static CloudBlobContainer GetThumbnailsContainer(CloudBlobClient targetCloudBlobClient, string guidContainerName)
        {
            return targetCloudBlobClient.GetContainerReference(guidContainerName + "-thumbnails");
        }

        public static async Task GenerateThumbnailForBlob(CloudBlockBlob sourceBlob, CloudBlobContainer targetContainer, bool createContainer = false)
        {
            if(createContainer)
                await targetContainer.CreateIfNotExistsAsync();

            var thumbnailGuid = sourceBlob.Name;
            var newBlobName = thumbnailGuid.Replace(".jpg", "-thumbnail.jpg");
            CloudBlockBlob targetBlob = targetContainer.GetBlockBlobReference(newBlobName);
            targetBlob.Properties.ContentType = "image/jpg";

            using (var sourceBlobStream = await sourceBlob.OpenReadAsync())
            {
                using (var targetBlobStream = await targetBlob.OpenWriteAsync())
                {
                    ResizeImage(sourceBlobStream, targetBlobStream, 200);
                }
            }
        }

        private static void SetRuntime(AzureThumbInfo thumbInfo)
        {
            var time = (DateTime.Now - thumbInfo.StartTime.Value);
            thumbInfo.RunTime = string.Format("{0} minutes, {1} seconds", (time.Hours * 60) + time.Minutes, time.Seconds);
        }

        public static bool IsGuid(string value)
        {
            Guid x;
            return Guid.TryParse(value, out x);
        }

        private static void ResizeImage(Stream input, Stream output, int width)
        {
            var instructions = new Instructions
            {
                Width = width,
                Mode = FitMode.Carve,
                Scale = ScaleMode.Both
            };
            var imageJob = new ImageJob(input, output, instructions);

            // Not sure if we should dispose the source object or not
            //imageJob.DisposeSourceObject = false;
            imageJob.Build();
        }

        public static string SaveFileGetAccessURL(string account, string containerName, int timeoutInDays, byte[] documentBytes, string fileExtension, string contentType, Stream documentStream = null)
        {
            // get connection string
            var connectString = ConfigurationManager.GetConnectionString(account);
            var storageAccount = CloudStorageAccount.Parse(connectString);

            // connect to azure
            var blobClient = storageAccount.CreateCloudBlobClient();

            // get container by name
            var today = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
            CloudBlobContainer container = blobClient.GetContainerReference(today + "-reports");
            container.CreateIfNotExists();
           
            // generate filename & reference
            string fileName = GetGuidFilename(fileExtension);            
            var blockBlob = container.GetBlockBlobReference(fileName);            

            // generate access signature that will allow creating a file
            var createFileAccessPolicy = new SharedAccessBlobPolicy
            {
                Permissions = SharedAccessBlobPermissions.Create | SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(20)
            };
            var createBlobAccessSig = blockBlob.GetSharedAccessSignature(createFileAccessPolicy);

            // generate access signature that will allow reading
            var accessPolicy = new SharedAccessBlobPolicy
            {
                SharedAccessExpiryTime = DateTime.UtcNow.AddDays(timeoutInDays),
                Permissions = SharedAccessBlobPermissions.Read
            };
            var readFileAccessSig = blockBlob.GetSharedAccessSignature(accessPolicy);

            // upload file using create access signature
            var uriWithSig = string.Concat(blockBlob.Uri.AbsoluteUri, createBlobAccessSig);
            var createBlob = new CloudBlockBlob(new Uri(uriWithSig));
            createBlob.Properties.ContentType = contentType;

            if (documentStream != null)
            {
                documentStream.Position = 0;
                createBlob.UploadFromStream(documentStream);
            }
            else
                createBlob.UploadFromByteArray(documentBytes, 0, documentBytes.Length);


            // concat the uri and the access signature to make a user accessable URL
            return string.Concat(createBlob.Uri.AbsoluteUri, readFileAccessSig);
        }

        private static string GetGuidFilename(string fileExtension)
        {
            if (!fileExtension.StartsWith("."))
                fileExtension = string.Format(".{0}", fileExtension);

            var today = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
            var fileGuid = Guid.NewGuid();
            var fileName = string.Concat(today, "_", fileGuid.ToString().ToLower(), fileExtension.ToLower());
            
            //return "2019-03-22_c4a5a043-8143-4ebd-bc80-da6777234253.pdf";
            return fileName;
        }

        internal static bool DeleteExpiredReports()
        {
            // get connection string
            var connectString = ConfigurationManager.GetConnectionString("qoskreports");
            var storageAccount = CloudStorageAccount.Parse(connectString);

            // connect to azure
            var blobClient = storageAccount.CreateCloudBlobClient();
            // get container by name
            var today = DateTime.UtcNow.Date;

            var urlTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays"));

            // delete reports that are one day older than the day timeout
            urlTimeoutInDays = urlTimeoutInDays + 1;

            var expiredDate = today.AddDays(-urlTimeoutInDays).ToString("yyyy-MM-dd");
            CloudBlobContainer container = blobClient.GetContainerReference(expiredDate + "-reports");
            container.DeleteIfExists();
            return true;
        }

        internal static object SendScheduledEmails()
        {
            throw new NotImplementedException();
        }
    }
}