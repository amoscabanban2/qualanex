namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports
{
    partial class CreditComparison
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.DetailSection DetailArea1;
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditComparison));
            Telerik.Reporting.InstanceReportSource instanceReportSource2 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group3 = new Telerik.Reporting.Group();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.DetailSection2 = new Telerik.Reporting.Panel();
            this.panel4 = new Telerik.Reporting.Panel();
            this.ReturnAuthorizationsItemLotNumber1 = new Telerik.Reporting.TextBox();
            this.ReturnAuthorizationsItemOriginalQuantity1 = new Telerik.Reporting.TextBox();
            this.ReturnAuthorizationsItemSealedOpenCase1 = new Telerik.Reporting.TextBox();
            this.DebitMemoReturnValue1 = new Telerik.Reporting.TextBox();
            this.ItemLotNumber1 = new Telerik.Reporting.TextBox();
            this.ItemQuantity1 = new Telerik.Reporting.TextBox();
            this.ItemSealedOpenCase1 = new Telerik.Reporting.TextBox();
            this.ReturnAuthorizationsItemExpirationMonthYear1 = new Telerik.Reporting.TextBox();
            this.ItemExpirationMonthYear1 = new Telerik.Reporting.TextBox();
            this.ManufacturerOverrideYesNo1 = new Telerik.Reporting.TextBox();
            this.StateLawOverrideYesNo1 = new Telerik.Reporting.TextBox();
            this.ReturnableItemValue1 = new Telerik.Reporting.TextBox();
            this.ItemCount1 = new Telerik.Reporting.TextBox();
            this.ReturnableItemValueTotal1 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.NonReturnableReasonDetail1 = new Telerik.Reporting.TextBox();
            this.Text45 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.NR1 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.ManufacturerPriceOverrideNotes1 = new Telerik.Reporting.TextBox();
            this.Text44 = new Telerik.Reporting.TextBox();
            this.panel5 = new Telerik.Reporting.Panel();
            this.panel7 = new Telerik.Reporting.Panel();
            this.subReport2 = new Telerik.Reporting.SubReport();
            this.subReportImages1 = new Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports.SubReportImages();
            this.subReportLogo1 = new Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports.SubReportLogo();
            this.GroupFooterArea1 = new Telerik.Reporting.GroupFooterSection();
            this.SumofDebitMemoReturnValue1 = new Telerik.Reporting.TextBox();
            this.Line8 = new Telerik.Reporting.Shape();
            this.SumofReturnableItemValueTotal2 = new Telerik.Reporting.TextBox();
            this.GroupHeaderArea1 = new Telerik.Reporting.GroupHeaderSection();
            this.Box2 = new Telerik.Reporting.Shape();
            this.Text24 = new Telerik.Reporting.TextBox();
            this.Text23 = new Telerik.Reporting.TextBox();
            this.Text14 = new Telerik.Reporting.TextBox();
            this.Text11 = new Telerik.Reporting.TextBox();
            this.Text10 = new Telerik.Reporting.TextBox();
            this.Text32 = new Telerik.Reporting.TextBox();
            this.Text16 = new Telerik.Reporting.TextBox();
            this.Text18 = new Telerik.Reporting.TextBox();
            this.Text17 = new Telerik.Reporting.TextBox();
            this.Text25 = new Telerik.Reporting.TextBox();
            this.Text22 = new Telerik.Reporting.TextBox();
            this.Text15 = new Telerik.Reporting.TextBox();
            this.Text20 = new Telerik.Reporting.TextBox();
            this.Text19 = new Telerik.Reporting.TextBox();
            this.Line3 = new Telerik.Reporting.Shape();
            this.Line6 = new Telerik.Reporting.Shape();
            this.Name1 = new Telerik.Reporting.TextBox();
            this.PhoneNumber1 = new Telerik.Reporting.TextBox();
            this.EmailAddress1 = new Telerik.Reporting.TextBox();
            this.DEANumber1 = new Telerik.Reporting.TextBox();
            this.DebitMemoNumber1 = new Telerik.Reporting.TextBox();
            this.PrintAccountTitle1 = new Telerik.Reporting.TextBox();
            this.PrintAccount1 = new Telerik.Reporting.TextBox();
            this.ThirdPartyReverseDistributorName1 = new Telerik.Reporting.TextBox();
            this.ThirdPartyReverseDistributorDEANumber1 = new Telerik.Reporting.TextBox();
            this.RequestorName1 = new Telerik.Reporting.TextBox();
            this.RequestorPhoneNumber1 = new Telerik.Reporting.TextBox();
            this.RequestorEmailAddress1 = new Telerik.Reporting.TextBox();
            this.ReturnAuthorizationNumber1 = new Telerik.Reporting.TextBox();
            this.RequestDate1 = new Telerik.Reporting.TextBox();
            this.ReceivedDate1 = new Telerik.Reporting.TextBox();
            this.Text6 = new Telerik.Reporting.TextBox();
            this.CreditIssuedDate1 = new Telerik.Reporting.TextBox();
            this.Text21 = new Telerik.Reporting.TextBox();
            this.Text58 = new Telerik.Reporting.TextBox();
            this.Text63 = new Telerik.Reporting.TextBox();
            this.ReturnAuthorizationNumberSplit1 = new Telerik.Reporting.TextBox();
            this.Text64 = new Telerik.Reporting.TextBox();
            this.PrintName1 = new Telerik.Reporting.TextBox();
            this.PrimaryCreditMemoNumber1 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.subReportLogo = new Telerik.Reporting.SubReport();
            this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.shape4 = new Telerik.Reporting.Shape();
            this.shape3 = new Telerik.Reporting.Shape();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.GroupFooterArea2 = new Telerik.Reporting.GroupFooterSection();
            this.Line9 = new Telerik.Reporting.Shape();
            this.SumofCaseRAQuantity1 = new Telerik.Reporting.TextBox();
            this.Text60 = new Telerik.Reporting.TextBox();
            this.Text59 = new Telerik.Reporting.TextBox();
            this.SumofOpenedItemQtyCount1 = new Telerik.Reporting.TextBox();
            this.SumofCaseAmount1 = new Telerik.Reporting.TextBox();
            this.SumofOpenedAmount1 = new Telerik.Reporting.TextBox();
            this.SumofSealedAmount1 = new Telerik.Reporting.TextBox();
            this.SumofCaseItemCount1 = new Telerik.Reporting.TextBox();
            this.SumofSealedItemCount1 = new Telerik.Reporting.TextBox();
            this.SumofCaseDebitMemoValue1 = new Telerik.Reporting.TextBox();
            this.SumofOpenedDebitMemoValue1 = new Telerik.Reporting.TextBox();
            this.SumofSealedDebitMemoValue1 = new Telerik.Reporting.TextBox();
            this.Text62 = new Telerik.Reporting.TextBox();
            this.SumofOpenedRAQuantity1 = new Telerik.Reporting.TextBox();
            this.SumofSealedRAQuantity1 = new Telerik.Reporting.TextBox();
            this.Text9 = new Telerik.Reporting.TextBox();
            this.Text8 = new Telerik.Reporting.TextBox();
            this.Text7 = new Telerik.Reporting.TextBox();
            this.SumofReturnableItemValueTotal1 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.GroupHeaderArea2 = new Telerik.Reporting.GroupHeaderSection();
            this.LongDescription1 = new Telerik.Reporting.TextBox();
            this.ReportFooterArea1 = new Telerik.Reporting.ReportFooterSection();
            this.PageFooterArea1 = new Telerik.Reporting.PageFooterSection();
            this.Text2 = new Telerik.Reporting.TextBox();
            this.Text3 = new Telerik.Reporting.TextBox();
            this.Text4 = new Telerik.Reporting.TextBox();
            this.Text5 = new Telerik.Reporting.TextBox();
            this.Field1 = new Telerik.Reporting.TextBox();
            this.Field2 = new Telerik.Reporting.TextBox();
            this.Line2 = new Telerik.Reporting.Shape();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.ccDataSource = new Telerik.Reporting.ObjectDataSource();
            DetailArea1 = new Telerik.Reporting.DetailSection();
            ((System.ComponentModel.ISupportInitialize)(this.subReportImages1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // DetailArea1
            // 
            DetailArea1.CanShrink = true;
            DetailArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.359D);
            DetailArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.DetailSection2});
            DetailArea1.KeepTogether = false;
            DetailArea1.Name = "DetailArea1";
            DetailArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            DetailArea1.Style.Visible = true;
            // 
            // DetailSection2
            // 
            this.DetailSection2.CanShrink = true;
            this.DetailSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel4,
            this.ReturnAuthorizationsItemLotNumber1,
            this.ReturnAuthorizationsItemOriginalQuantity1,
            this.ReturnAuthorizationsItemSealedOpenCase1,
            this.DebitMemoReturnValue1,
            this.ItemLotNumber1,
            this.ItemQuantity1,
            this.ItemSealedOpenCase1,
            this.ReturnAuthorizationsItemExpirationMonthYear1,
            this.ItemExpirationMonthYear1,
            this.ManufacturerOverrideYesNo1,
            this.StateLawOverrideYesNo1,
            this.ReturnableItemValue1,
            this.ItemCount1,
            this.ReturnableItemValueTotal1,
            this.panel1,
            this.panel2,
            this.panel3,
            this.panel5});
            this.DetailSection2.KeepTogether = false;
            this.DetailSection2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.DetailSection2.Name = "DetailSection2";
            this.DetailSection2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.8D), Telerik.Reporting.Drawing.Unit.Cm(3.359D));
            this.DetailSection2.Style.BackgroundColor = System.Drawing.Color.White;
            this.DetailSection2.Style.Visible = true;
            // 
            // panel4
            // 
            this.panel4.KeepTogether = true;
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.7D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.panel4.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // ReturnAuthorizationsItemLotNumber1
            // 
            this.ReturnAuthorizationsItemLotNumber1.CanGrow = false;
            this.ReturnAuthorizationsItemLotNumber1.CanShrink = false;
            this.ReturnAuthorizationsItemLotNumber1.KeepTogether = true;
            this.ReturnAuthorizationsItemLotNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.109D));
            this.ReturnAuthorizationsItemLotNumber1.Name = "ReturnAuthorizationsItemLotNumber1";
            this.ReturnAuthorizationsItemLotNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.328D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ReturnAuthorizationsItemLotNumber1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnAuthorizationsItemLotNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemLotNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationsItemLotNumber1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Bold = false;
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Italic = false;
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationsItemLotNumber1.Style.Font.Underline = false;
            this.ReturnAuthorizationsItemLotNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ReturnAuthorizationsItemLotNumber1.Style.Visible = true;
            this.ReturnAuthorizationsItemLotNumber1.Value = "=Fields.[ReturnAuthorizationsItemLotNumber]";
            // 
            // ReturnAuthorizationsItemOriginalQuantity1
            // 
            this.ReturnAuthorizationsItemOriginalQuantity1.CanGrow = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.CanShrink = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.KeepTogether = true;
            this.ReturnAuthorizationsItemOriginalQuantity1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.555D), Telerik.Reporting.Drawing.Unit.Cm(0.109D));
            this.ReturnAuthorizationsItemOriginalQuantity1.Name = "ReturnAuthorizationsItemOriginalQuantity1";
            this.ReturnAuthorizationsItemOriginalQuantity1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Bold = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Italic = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Font.Underline = false;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ReturnAuthorizationsItemOriginalQuantity1.Style.Visible = true;
            this.ReturnAuthorizationsItemOriginalQuantity1.Value = "=Fields.[ReturnAuthorizationsItemOriginalQuantity]";
            // 
            // ReturnAuthorizationsItemSealedOpenCase1
            // 
            this.ReturnAuthorizationsItemSealedOpenCase1.CanGrow = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.CanShrink = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.KeepTogether = true;
            this.ReturnAuthorizationsItemSealedOpenCase1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.627D), Telerik.Reporting.Drawing.Unit.Cm(0.109D));
            this.ReturnAuthorizationsItemSealedOpenCase1.Name = "ReturnAuthorizationsItemSealedOpenCase1";
            this.ReturnAuthorizationsItemSealedOpenCase1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.101D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Bold = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Italic = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Font.Underline = false;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ReturnAuthorizationsItemSealedOpenCase1.Style.Visible = true;
            this.ReturnAuthorizationsItemSealedOpenCase1.Value = "=Fields.[ReturnAuthorizationsItemSealedOpenCase]";
            // 
            // DebitMemoReturnValue1
            // 
            this.DebitMemoReturnValue1.CanGrow = false;
            this.DebitMemoReturnValue1.CanShrink = false;
            this.DebitMemoReturnValue1.Format = "{0:C2}";
            this.DebitMemoReturnValue1.KeepTogether = true;
            this.DebitMemoReturnValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.852D), Telerik.Reporting.Drawing.Unit.Cm(0.109D));
            this.DebitMemoReturnValue1.Name = "DebitMemoReturnValue1";
            this.DebitMemoReturnValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.DebitMemoReturnValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.DebitMemoReturnValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.DebitMemoReturnValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.DebitMemoReturnValue1.Style.Color = System.Drawing.Color.Black;
            this.DebitMemoReturnValue1.Style.Font.Bold = false;
            this.DebitMemoReturnValue1.Style.Font.Italic = false;
            this.DebitMemoReturnValue1.Style.Font.Name = "Arial";
            this.DebitMemoReturnValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.DebitMemoReturnValue1.Style.Font.Strikeout = false;
            this.DebitMemoReturnValue1.Style.Font.Underline = false;
            this.DebitMemoReturnValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.DebitMemoReturnValue1.Style.Visible = true;
            this.DebitMemoReturnValue1.Value = "=Fields.[DebitMemoReturnValue]";
            // 
            // ItemLotNumber1
            // 
            this.ItemLotNumber1.CanGrow = false;
            this.ItemLotNumber1.CanShrink = false;
            this.ItemLotNumber1.KeepTogether = false;
            this.ItemLotNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.95D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ItemLotNumber1.Name = "ItemLotNumber1";
            this.ItemLotNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ItemLotNumber1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ItemLotNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ItemLotNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ItemLotNumber1.Style.Color = System.Drawing.Color.Black;
            this.ItemLotNumber1.Style.Font.Bold = false;
            this.ItemLotNumber1.Style.Font.Italic = false;
            this.ItemLotNumber1.Style.Font.Name = "Arial";
            this.ItemLotNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ItemLotNumber1.Style.Font.Strikeout = false;
            this.ItemLotNumber1.Style.Font.Underline = false;
            this.ItemLotNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ItemLotNumber1.Style.Visible = true;
            this.ItemLotNumber1.Value = "=Fields.[ItemLotNumber]";
            // 
            // ItemQuantity1
            // 
            this.ItemQuantity1.CanGrow = false;
            this.ItemQuantity1.CanShrink = false;
            this.ItemQuantity1.KeepTogether = false;
            this.ItemQuantity1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.228D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ItemQuantity1.Name = "ItemQuantity1";
            this.ItemQuantity1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ItemQuantity1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ItemQuantity1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ItemQuantity1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ItemQuantity1.Style.Color = System.Drawing.Color.Black;
            this.ItemQuantity1.Style.Font.Bold = false;
            this.ItemQuantity1.Style.Font.Italic = false;
            this.ItemQuantity1.Style.Font.Name = "Arial";
            this.ItemQuantity1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ItemQuantity1.Style.Font.Strikeout = false;
            this.ItemQuantity1.Style.Font.Underline = false;
            this.ItemQuantity1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ItemQuantity1.Style.Visible = true;
            this.ItemQuantity1.Value = "=Fields.[ItemQuantity]";
            // 
            // ItemSealedOpenCase1
            // 
            this.ItemSealedOpenCase1.CanGrow = false;
            this.ItemSealedOpenCase1.CanShrink = false;
            this.ItemSealedOpenCase1.KeepTogether = false;
            this.ItemSealedOpenCase1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.157D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ItemSealedOpenCase1.Name = "ItemSealedOpenCase1";
            this.ItemSealedOpenCase1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.101D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ItemSealedOpenCase1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ItemSealedOpenCase1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ItemSealedOpenCase1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ItemSealedOpenCase1.Style.Color = System.Drawing.Color.Black;
            this.ItemSealedOpenCase1.Style.Font.Bold = false;
            this.ItemSealedOpenCase1.Style.Font.Italic = false;
            this.ItemSealedOpenCase1.Style.Font.Name = "Arial";
            this.ItemSealedOpenCase1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ItemSealedOpenCase1.Style.Font.Strikeout = false;
            this.ItemSealedOpenCase1.Style.Font.Underline = false;
            this.ItemSealedOpenCase1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ItemSealedOpenCase1.Style.Visible = true;
            this.ItemSealedOpenCase1.Value = "=Fields.[ItemSealedOpenCase]";
            // 
            // ReturnAuthorizationsItemExpirationMonthYear1
            // 
            this.ReturnAuthorizationsItemExpirationMonthYear1.CanGrow = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.CanShrink = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.KeepTogether = true;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.712D), Telerik.Reporting.Drawing.Unit.Cm(0.109D));
            this.ReturnAuthorizationsItemExpirationMonthYear1.Name = "ReturnAuthorizationsItemExpirationMonthYear1";
            this.ReturnAuthorizationsItemExpirationMonthYear1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.693D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Bold = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Italic = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Font.Underline = false;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Style.Visible = true;
            this.ReturnAuthorizationsItemExpirationMonthYear1.Value = "=Fields.[ReturnAuthorizationsItemExpirationMonthYear]";
            // 
            // ItemExpirationMonthYear1
            // 
            this.ItemExpirationMonthYear1.CanGrow = false;
            this.ItemExpirationMonthYear1.CanShrink = false;
            this.ItemExpirationMonthYear1.KeepTogether = false;
            this.ItemExpirationMonthYear1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.5D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ItemExpirationMonthYear1.Name = "ItemExpirationMonthYear1";
            this.ItemExpirationMonthYear1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.693D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ItemExpirationMonthYear1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ItemExpirationMonthYear1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ItemExpirationMonthYear1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ItemExpirationMonthYear1.Style.Color = System.Drawing.Color.Black;
            this.ItemExpirationMonthYear1.Style.Font.Bold = false;
            this.ItemExpirationMonthYear1.Style.Font.Italic = false;
            this.ItemExpirationMonthYear1.Style.Font.Name = "Arial";
            this.ItemExpirationMonthYear1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ItemExpirationMonthYear1.Style.Font.Strikeout = false;
            this.ItemExpirationMonthYear1.Style.Font.Underline = false;
            this.ItemExpirationMonthYear1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ItemExpirationMonthYear1.Style.Visible = true;
            this.ItemExpirationMonthYear1.Value = "=Fields.[ItemExpirationMonthYear]";
            // 
            // ManufacturerOverrideYesNo1
            // 
            this.ManufacturerOverrideYesNo1.CanGrow = false;
            this.ManufacturerOverrideYesNo1.CanShrink = false;
            this.ManufacturerOverrideYesNo1.KeepTogether = false;
            this.ManufacturerOverrideYesNo1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(24.88D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ManufacturerOverrideYesNo1.Name = "ManufacturerOverrideYesNo1";
            this.ManufacturerOverrideYesNo1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.609D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ManufacturerOverrideYesNo1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ManufacturerOverrideYesNo1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ManufacturerOverrideYesNo1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ManufacturerOverrideYesNo1.Style.Color = System.Drawing.Color.Black;
            this.ManufacturerOverrideYesNo1.Style.Font.Bold = true;
            this.ManufacturerOverrideYesNo1.Style.Font.Italic = false;
            this.ManufacturerOverrideYesNo1.Style.Font.Name = "Arial";
            this.ManufacturerOverrideYesNo1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ManufacturerOverrideYesNo1.Style.Font.Strikeout = false;
            this.ManufacturerOverrideYesNo1.Style.Font.Underline = false;
            this.ManufacturerOverrideYesNo1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ManufacturerOverrideYesNo1.Style.Visible = true;
            this.ManufacturerOverrideYesNo1.Value = "= Fields.ManufacturerOverRideYesNo";
            // 
            // StateLawOverrideYesNo1
            // 
            this.StateLawOverrideYesNo1.CanGrow = false;
            this.StateLawOverrideYesNo1.CanShrink = false;
            this.StateLawOverrideYesNo1.KeepTogether = false;
            this.StateLawOverrideYesNo1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(25.612D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.StateLawOverrideYesNo1.Name = "StateLawOverrideYesNo1";
            this.StateLawOverrideYesNo1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.609D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.StateLawOverrideYesNo1.Style.BackgroundColor = System.Drawing.Color.White;
            this.StateLawOverrideYesNo1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.StateLawOverrideYesNo1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.StateLawOverrideYesNo1.Style.Color = System.Drawing.Color.Black;
            this.StateLawOverrideYesNo1.Style.Font.Bold = true;
            this.StateLawOverrideYesNo1.Style.Font.Italic = false;
            this.StateLawOverrideYesNo1.Style.Font.Name = "Arial";
            this.StateLawOverrideYesNo1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.StateLawOverrideYesNo1.Style.Font.Strikeout = false;
            this.StateLawOverrideYesNo1.Style.Font.Underline = false;
            this.StateLawOverrideYesNo1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.StateLawOverrideYesNo1.Style.Visible = true;
            this.StateLawOverrideYesNo1.Value = "= Fields.StateLawOverrideYesNo";
            // 
            // ReturnableItemValue1
            // 
            this.ReturnableItemValue1.CanGrow = false;
            this.ReturnableItemValue1.CanShrink = false;
            this.ReturnableItemValue1.Format = "{0:C2}";
            this.ReturnableItemValue1.KeepTogether = false;
            this.ReturnableItemValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.696D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ReturnableItemValue1.Name = "ReturnableItemValue1";
            this.ReturnableItemValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.244D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ReturnableItemValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnableItemValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnableItemValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnableItemValue1.Style.Color = System.Drawing.Color.Black;
            this.ReturnableItemValue1.Style.Font.Bold = false;
            this.ReturnableItemValue1.Style.Font.Italic = false;
            this.ReturnableItemValue1.Style.Font.Name = "Arial";
            this.ReturnableItemValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnableItemValue1.Style.Font.Strikeout = false;
            this.ReturnableItemValue1.Style.Font.Underline = false;
            this.ReturnableItemValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.ReturnableItemValue1.Style.Visible = true;
            this.ReturnableItemValue1.Value = "=Fields.[ReturnableItemValue]";
            // 
            // ItemCount1
            // 
            this.ItemCount1.CanGrow = false;
            this.ItemCount1.CanShrink = false;
            this.ItemCount1.KeepTogether = false;
            this.ItemCount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(20.355D), Telerik.Reporting.Drawing.Unit.Cm(0.113D));
            this.ItemCount1.Name = "ItemCount1";
            this.ItemCount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.244D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ItemCount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ItemCount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ItemCount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ItemCount1.Style.Color = System.Drawing.Color.Black;
            this.ItemCount1.Style.Font.Bold = false;
            this.ItemCount1.Style.Font.Italic = false;
            this.ItemCount1.Style.Font.Name = "Arial";
            this.ItemCount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ItemCount1.Style.Font.Strikeout = false;
            this.ItemCount1.Style.Font.Underline = false;
            this.ItemCount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ItemCount1.Style.Visible = true;
            this.ItemCount1.Value = "=Fields.[ItemCount]";
            // 
            // ReturnableItemValueTotal1
            // 
            this.ReturnableItemValueTotal1.CanGrow = false;
            this.ReturnableItemValueTotal1.CanShrink = false;
            this.ReturnableItemValueTotal1.Format = "{0:C2}";
            this.ReturnableItemValueTotal1.KeepTogether = false;
            this.ReturnableItemValueTotal1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.973D), Telerik.Reporting.Drawing.Unit.Cm(0.125D));
            this.ReturnableItemValueTotal1.Name = "ReturnableItemValueTotal1";
            this.ReturnableItemValueTotal1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.ReturnableItemValueTotal1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReturnableItemValueTotal1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnableItemValueTotal1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnableItemValueTotal1.Style.Color = System.Drawing.Color.Black;
            this.ReturnableItemValueTotal1.Style.Font.Bold = false;
            this.ReturnableItemValueTotal1.Style.Font.Italic = false;
            this.ReturnableItemValueTotal1.Style.Font.Name = "Arial";
            this.ReturnableItemValueTotal1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ReturnableItemValueTotal1.Style.Font.Strikeout = false;
            this.ReturnableItemValueTotal1.Style.Font.Underline = false;
            this.ReturnableItemValueTotal1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.ReturnableItemValueTotal1.Style.Visible = true;
            this.ReturnableItemValueTotal1.Value = "=Fields.[ReturnableItemValueTotal]";
            // 
            // panel1
            // 
            this.panel1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Len(Fields.NonReturnableReasonDetail) > 0"));
            this.panel1.CanShrink = true;
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.NonReturnableReasonDetail1,
            this.Text45});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.7D), Telerik.Reporting.Drawing.Unit.Cm(1.79D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.1D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.panel1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.Visible = true;
            // 
            // NonReturnableReasonDetail1
            // 
            this.NonReturnableReasonDetail1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Len(Fields.NonReturnableReasonDetail) > 0"));
            this.NonReturnableReasonDetail1.CanGrow = true;
            this.NonReturnableReasonDetail1.CanShrink = false;
            this.NonReturnableReasonDetail1.KeepTogether = false;
            this.NonReturnableReasonDetail1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.NonReturnableReasonDetail1.Name = "NonReturnableReasonDetail1";
            this.NonReturnableReasonDetail1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.288D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.NonReturnableReasonDetail1.Style.BackgroundColor = System.Drawing.Color.White;
            this.NonReturnableReasonDetail1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.NonReturnableReasonDetail1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.NonReturnableReasonDetail1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.NonReturnableReasonDetail1.Style.Color = System.Drawing.Color.Black;
            this.NonReturnableReasonDetail1.Style.Font.Bold = false;
            this.NonReturnableReasonDetail1.Style.Font.Italic = false;
            this.NonReturnableReasonDetail1.Style.Font.Name = "Arial";
            this.NonReturnableReasonDetail1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.NonReturnableReasonDetail1.Style.Font.Strikeout = false;
            this.NonReturnableReasonDetail1.Style.Font.Underline = false;
            this.NonReturnableReasonDetail1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.NonReturnableReasonDetail1.Style.Visible = true;
            this.NonReturnableReasonDetail1.Value = "=Fields.[NonReturnableReasonDetail]";
            // 
            // Text45
            // 
            this.Text45.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Len(Fields.NonReturnableReasonDetail) > 0"));
            this.Text45.CanGrow = false;
            this.Text45.CanShrink = false;
            this.Text45.KeepTogether = false;
            this.Text45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.1D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Text45.Name = "Text45";
            this.Text45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.563D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.Text45.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text45.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text45.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.Text45.Style.Color = System.Drawing.Color.Black;
            this.Text45.Style.Font.Bold = true;
            this.Text45.Style.Font.Italic = false;
            this.Text45.Style.Font.Name = "Arial";
            this.Text45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text45.Style.Font.Strikeout = false;
            this.Text45.Style.Font.Underline = false;
            this.Text45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text45.Style.Visible = true;
            this.Text45.Value = "Non-Returnable Reasons";
            // 
            // panel2
            // 
            this.panel2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Len(Fields.NR) > 0"));
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.NR1});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.7D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.1D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.panel2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel2.Style.Visible = true;
            // 
            // NR1
            // 
            this.NR1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "=Len(Fields.NR) > 0"));
            this.NR1.CanGrow = false;
            this.NR1.CanShrink = false;
            this.NR1.KeepTogether = false;
            this.NR1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.1D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.NR1.Name = "NR1";
            this.NR1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.288D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.NR1.Style.BackgroundColor = System.Drawing.Color.White;
            this.NR1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.NR1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.NR1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.NR1.Style.Color = System.Drawing.Color.Black;
            this.NR1.Style.Font.Bold = true;
            this.NR1.Style.Font.Italic = false;
            this.NR1.Style.Font.Name = "Arial";
            this.NR1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.NR1.Style.Font.Strikeout = false;
            this.NR1.Style.Font.Underline = false;
            this.NR1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.NR1.Style.Visible = true;
            this.NR1.Value = "=Fields.[NR]";
            // 
            // panel3
            // 
            this.panel3.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Len(Fields.ManufacturerPriceOverrideNotes) > 0"));
            this.panel3.CanShrink = true;
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.ManufacturerPriceOverrideNotes1,
            this.Text44});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.1D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.panel3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel3.Style.Visible = true;
            // 
            // ManufacturerPriceOverrideNotes1
            // 
            this.ManufacturerPriceOverrideNotes1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Len(Fields.ManufacturerPriceOverrideNotes) > 0"));
            this.ManufacturerPriceOverrideNotes1.CanGrow = true;
            this.ManufacturerPriceOverrideNotes1.CanShrink = false;
            this.ManufacturerPriceOverrideNotes1.KeepTogether = false;
            this.ManufacturerPriceOverrideNotes1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.098D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.ManufacturerPriceOverrideNotes1.Name = "ManufacturerPriceOverrideNotes1";
            this.ManufacturerPriceOverrideNotes1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.288D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.ManufacturerPriceOverrideNotes1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ManufacturerPriceOverrideNotes1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ManufacturerPriceOverrideNotes1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ManufacturerPriceOverrideNotes1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.ManufacturerPriceOverrideNotes1.Style.Color = System.Drawing.Color.Black;
            this.ManufacturerPriceOverrideNotes1.Style.Font.Bold = false;
            this.ManufacturerPriceOverrideNotes1.Style.Font.Italic = false;
            this.ManufacturerPriceOverrideNotes1.Style.Font.Name = "Arial";
            this.ManufacturerPriceOverrideNotes1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.ManufacturerPriceOverrideNotes1.Style.Font.Strikeout = false;
            this.ManufacturerPriceOverrideNotes1.Style.Font.Underline = false;
            this.ManufacturerPriceOverrideNotes1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ManufacturerPriceOverrideNotes1.Style.Visible = true;
            this.ManufacturerPriceOverrideNotes1.Value = "=Fields.[ManufacturerPriceOverrideNotes]";
            // 
            // Text44
            // 
            this.Text44.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Len(Fields.ManufacturerPriceOverrideNotes) > 0"));
            this.Text44.CanGrow = false;
            this.Text44.CanShrink = false;
            this.Text44.KeepTogether = false;
            this.Text44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.098D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Text44.Name = "Text44";
            this.Text44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.681D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.Text44.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text44.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text44.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.Text44.Style.Color = System.Drawing.Color.Black;
            this.Text44.Style.Font.Bold = true;
            this.Text44.Style.Font.Italic = false;
            this.Text44.Style.Font.Name = "Arial";
            this.Text44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text44.Style.Font.Strikeout = false;
            this.Text44.Style.Font.Underline = false;
            this.Text44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text44.Style.Visible = true;
            this.Text44.Value = "Manufacturer Notes";
            // 
            // panel5
            // 
            this.panel5.CanShrink = true;
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel7});
            this.panel5.KeepTogether = false;
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(2.63D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.8D), Telerik.Reporting.Drawing.Unit.Cm(0.729D));
            this.panel5.Style.BackgroundColor = System.Drawing.Color.White;
            this.panel5.Style.Visible = true;
            // 
            // panel7
            // 
            this.panel7.Action = null;
            this.panel7.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.ImagesPermitted.Value"));
            this.panel7.CanShrink = true;
            this.panel7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport2});
            this.panel7.KeepTogether = false;
            this.panel7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.7D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.panel7.Name = "panel7";
            this.panel7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.521D), Telerik.Reporting.Drawing.Unit.Cm(0.729D));
            this.panel7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel7.Style.Visible = false;
            // 
            // subReport2
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=(Fields.ItemIDs)", Telerik.Reporting.FilterOperator.Like, "\"-\""));
            formattingRule1.Style.Visible = false;
            this.subReport2.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.subReport2.KeepTogether = false;
            this.subReport2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.057D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.subReport2.Name = "subReport2";
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("ReturnAuthorizationNumber", "=Fields.[ReturnAuthorizationNumber]"));
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("ImagesPermitted", "= Parameters.ImagesPermitted.Value"));
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("DisplayImages", "= Parameters.DisplayImages.Value"));
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("ItemIDs", "= Fields.[ItemIDs]"));
            instanceReportSource1.ReportDocument = this.subReportImages1;
            this.subReport2.ReportSource = instanceReportSource1;
            this.subReport2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.563D), Telerik.Reporting.Drawing.Unit.Inch(0.287D));
            this.subReport2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.02D);
            this.subReport2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.subReport2.Style.Visible = true;
            this.subReport2.ItemDataBound += new System.EventHandler(this.subReport2_ItemDataBound);
            // 
            // subReportImages1
            // 
            this.subReportImages1.Name = "SubReportImages";
            // 
            // subReportLogo1
            // 
            this.subReportLogo1.Name = "SubReportLogo";
            // 
            // GroupFooterArea1
            // 
            this.GroupFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.824D);
            this.GroupFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.SumofDebitMemoReturnValue1,
            this.Line8,
            this.SumofReturnableItemValueTotal2});
            this.GroupFooterArea1.KeepTogether = false;
            this.GroupFooterArea1.Name = "GroupFooterArea1";
            this.GroupFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.GroupFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupFooterArea1.Style.Visible = true;
            // 
            // SumofDebitMemoReturnValue1
            // 
            this.SumofDebitMemoReturnValue1.CanGrow = false;
            this.SumofDebitMemoReturnValue1.CanShrink = false;
            this.SumofDebitMemoReturnValue1.Format = "{0:C2}";
            this.SumofDebitMemoReturnValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.245D));
            this.SumofDebitMemoReturnValue1.Name = "SumofDebitMemoReturnValue1";
            this.SumofDebitMemoReturnValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofDebitMemoReturnValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofDebitMemoReturnValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofDebitMemoReturnValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofDebitMemoReturnValue1.Style.Color = System.Drawing.Color.Black;
            this.SumofDebitMemoReturnValue1.Style.Font.Bold = true;
            this.SumofDebitMemoReturnValue1.Style.Font.Italic = false;
            this.SumofDebitMemoReturnValue1.Style.Font.Name = "Arial";
            this.SumofDebitMemoReturnValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.SumofDebitMemoReturnValue1.Style.Font.Strikeout = false;
            this.SumofDebitMemoReturnValue1.Style.Font.Underline = false;
            this.SumofDebitMemoReturnValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofDebitMemoReturnValue1.Style.Visible = true;
            this.SumofDebitMemoReturnValue1.Value = "=Sum(Fields.DebitMemoReturnValue)";
            // 
            // Line8
            // 
            this.Line8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.833D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Line8.Name = "Line8";
            this.Line8.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.003D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line8.Style.Color = System.Drawing.Color.Black;
            this.Line8.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.Line8.Style.Visible = true;
            // 
            // SumofReturnableItemValueTotal2
            // 
            this.SumofReturnableItemValueTotal2.CanGrow = false;
            this.SumofReturnableItemValueTotal2.CanShrink = false;
            this.SumofReturnableItemValueTotal2.Format = "{0:C2}";
            this.SumofReturnableItemValueTotal2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.202D), Telerik.Reporting.Drawing.Unit.Cm(0.245D));
            this.SumofReturnableItemValueTotal2.Name = "SumofReturnableItemValueTotal2";
            this.SumofReturnableItemValueTotal2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.473D));
            this.SumofReturnableItemValueTotal2.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofReturnableItemValueTotal2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofReturnableItemValueTotal2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofReturnableItemValueTotal2.Style.Color = System.Drawing.Color.Black;
            this.SumofReturnableItemValueTotal2.Style.Font.Bold = true;
            this.SumofReturnableItemValueTotal2.Style.Font.Italic = false;
            this.SumofReturnableItemValueTotal2.Style.Font.Name = "Arial";
            this.SumofReturnableItemValueTotal2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.SumofReturnableItemValueTotal2.Style.Font.Strikeout = false;
            this.SumofReturnableItemValueTotal2.Style.Font.Underline = false;
            this.SumofReturnableItemValueTotal2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofReturnableItemValueTotal2.Style.Visible = true;
            this.SumofReturnableItemValueTotal2.Value = "=Sum(Fields.ReturnableItemValueTotal)";
            // 
            // GroupHeaderArea1
            // 
            this.GroupHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(8.9D);
            this.GroupHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Box2,
            this.Text24,
            this.Text23,
            this.Text14,
            this.Text11,
            this.Text10,
            this.Text32,
            this.Text16,
            this.Text18,
            this.Text17,
            this.Text25,
            this.Text22,
            this.Text15,
            this.Text20,
            this.Text19,
            this.Line3,
            this.Line6,
            this.Name1,
            this.PhoneNumber1,
            this.EmailAddress1,
            this.DEANumber1,
            this.DebitMemoNumber1,
            this.PrintAccountTitle1,
            this.PrintAccount1,
            this.ThirdPartyReverseDistributorName1,
            this.ThirdPartyReverseDistributorDEANumber1,
            this.RequestorName1,
            this.RequestorPhoneNumber1,
            this.RequestorEmailAddress1,
            this.ReturnAuthorizationNumber1,
            this.RequestDate1,
            this.ReceivedDate1,
            this.Text6,
            this.CreditIssuedDate1,
            this.Text21,
            this.Text58,
            this.Text63,
            this.ReturnAuthorizationNumberSplit1,
            this.Text64,
            this.PrintName1,
            this.PrimaryCreditMemoNumber1,
            this.htmlTextBox1,
            this.htmlTextBox2,
            this.textBox2,
            this.textBox1,
            this.pictureBox4,
            this.subReportLogo});
            this.GroupHeaderArea1.KeepTogether = false;
            this.GroupHeaderArea1.Name = "GroupHeaderArea1";
            this.GroupHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.GroupHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupHeaderArea1.Style.Visible = true;
            // 
            // Box2
            // 
            this.Box2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            this.Box2.Name = "Box2";
            this.Box2.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
            this.Box2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.141D), Telerik.Reporting.Drawing.Unit.Cm(5.3D));
            this.Box2.Stretch = true;
            this.Box2.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Box2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.Box2.Style.Color = System.Drawing.Color.Black;
            this.Box2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Box2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Box2.Style.Visible = true;
            // 
            // Text24
            // 
            this.Text24.CanGrow = false;
            this.Text24.CanShrink = false;
            this.Text24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(7.123D));
            this.Text24.Multiline = false;
            this.Text24.Name = "Text24";
            this.Text24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text24.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text24.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text24.Style.Color = System.Drawing.Color.Black;
            this.Text24.Style.Font.Bold = false;
            this.Text24.Style.Font.Italic = false;
            this.Text24.Style.Font.Name = "Arial";
            this.Text24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text24.Style.Font.Strikeout = false;
            this.Text24.Style.Font.Underline = false;
            this.Text24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text24.Style.Visible = true;
            this.Text24.Value = "Debit Number:";
            // 
            // Text23
            // 
            this.Text23.CanGrow = false;
            this.Text23.CanShrink = false;
            this.Text23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(6.665D));
            this.Text23.Multiline = false;
            this.Text23.Name = "Text23";
            this.Text23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text23.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text23.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text23.Style.Color = System.Drawing.Color.Black;
            this.Text23.Style.Font.Bold = false;
            this.Text23.Style.Font.Italic = false;
            this.Text23.Style.Font.Name = "Arial";
            this.Text23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text23.Style.Font.Strikeout = false;
            this.Text23.Style.Font.Underline = false;
            this.Text23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text23.Style.Visible = true;
            this.Text23.Value = "DEA Number:";
            // 
            // Text14
            // 
            this.Text14.CanGrow = false;
            this.Text14.CanShrink = false;
            this.Text14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(6.18D));
            this.Text14.Multiline = false;
            this.Text14.Name = "Text14";
            this.Text14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.27D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text14.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text14.Style.Color = System.Drawing.Color.Black;
            this.Text14.Style.Font.Bold = false;
            this.Text14.Style.Font.Italic = false;
            this.Text14.Style.Font.Name = "Arial";
            this.Text14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text14.Style.Font.Strikeout = false;
            this.Text14.Style.Font.Underline = false;
            this.Text14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text14.Style.Visible = true;
            this.Text14.Value = "Email:";
            // 
            // Text11
            // 
            this.Text11.CanGrow = false;
            this.Text11.CanShrink = false;
            this.Text11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(5.748D));
            this.Text11.Multiline = false;
            this.Text11.Name = "Text11";
            this.Text11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.27D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text11.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text11.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text11.Style.Color = System.Drawing.Color.Black;
            this.Text11.Style.Font.Bold = false;
            this.Text11.Style.Font.Italic = false;
            this.Text11.Style.Font.Name = "Arial";
            this.Text11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text11.Style.Font.Strikeout = false;
            this.Text11.Style.Font.Underline = false;
            this.Text11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text11.Style.Visible = true;
            this.Text11.Value = "Phone:";
            // 
            // Text10
            // 
            this.Text10.CanGrow = false;
            this.Text10.CanShrink = false;
            this.Text10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(3.596D));
            this.Text10.Multiline = false;
            this.Text10.Name = "Text10";
            this.Text10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.056D), Telerik.Reporting.Drawing.Unit.Cm(0.406D));
            this.Text10.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text10.Style.Color = System.Drawing.Color.Black;
            this.Text10.Style.Font.Bold = true;
            this.Text10.Style.Font.Italic = false;
            this.Text10.Style.Font.Name = "Arial";
            this.Text10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text10.Style.Font.Strikeout = false;
            this.Text10.Style.Font.Underline = false;
            this.Text10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text10.Style.Visible = true;
            this.Text10.Value = "CUSTOMER INFORMATION";
            // 
            // Text32
            // 
            this.Text32.CanGrow = false;
            this.Text32.CanShrink = false;
            this.Text32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(8.037D));
            this.Text32.Multiline = false;
            this.Text32.Name = "Text32";
            this.Text32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.322D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text32.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text32.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text32.Style.Color = System.Drawing.Color.Black;
            this.Text32.Style.Font.Bold = false;
            this.Text32.Style.Font.Italic = false;
            this.Text32.Style.Font.Name = "Arial";
            this.Text32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text32.Style.Font.Strikeout = false;
            this.Text32.Style.Font.Underline = false;
            this.Text32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text32.Style.Visible = true;
            this.Text32.Value = "=Iif(Fields.ThirdPartyReverseDistributorName IS Null,\"\",\"DEA Number:\")";
            // 
            // Text16
            // 
            this.Text16.CanGrow = false;
            this.Text16.CanShrink = false;
            this.Text16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(5.995D));
            this.Text16.Name = "Text16";
            this.Text16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.885D), Telerik.Reporting.Drawing.Unit.Cm(0.415D));
            this.Text16.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text16.Style.Color = System.Drawing.Color.Black;
            this.Text16.Style.Font.Bold = true;
            this.Text16.Style.Font.Italic = false;
            this.Text16.Style.Font.Name = "Arial";
            this.Text16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text16.Style.Font.Strikeout = false;
            this.Text16.Style.Font.Underline = false;
            this.Text16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text16.Style.Visible = true;
            this.Text16.Value = "=Iif(Fields.ThirdPartyReverseDistributorName IS Null,\"\",\"Third Party Returns Serv" +
    "ice\")";
            // 
            // Text18
            // 
            this.Text18.CanGrow = false;
            this.Text18.CanShrink = false;
            this.Text18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(3.596D));
            this.Text18.Name = "Text18";
            this.Text18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.184D), Telerik.Reporting.Drawing.Unit.Cm(0.406D));
            this.Text18.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text18.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text18.Style.Color = System.Drawing.Color.Black;
            this.Text18.Style.Font.Bold = true;
            this.Text18.Style.Font.Italic = false;
            this.Text18.Style.Font.Name = "Arial";
            this.Text18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text18.Style.Font.Strikeout = false;
            this.Text18.Style.Font.Underline = false;
            this.Text18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text18.Style.Visible = true;
            this.Text18.Value = "RETURN AUTHORIZATION REQUESTOR";
            // 
            // Text17
            // 
            this.Text17.CanGrow = false;
            this.Text17.CanShrink = false;
            this.Text17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(5.13D));
            this.Text17.Multiline = false;
            this.Text17.Name = "Text17";
            this.Text17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text17.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text17.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text17.Style.Color = System.Drawing.Color.Black;
            this.Text17.Style.Font.Bold = false;
            this.Text17.Style.Font.Italic = false;
            this.Text17.Style.Font.Name = "Arial";
            this.Text17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text17.Style.Font.Strikeout = false;
            this.Text17.Style.Font.Underline = false;
            this.Text17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text17.Style.Visible = true;
            this.Text17.Value = "Date Received:";
            // 
            // Text25
            // 
            this.Text25.CanGrow = false;
            this.Text25.CanShrink = false;
            this.Text25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(4.667D));
            this.Text25.Multiline = false;
            this.Text25.Name = "Text25";
            this.Text25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text25.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text25.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text25.Style.Color = System.Drawing.Color.Black;
            this.Text25.Style.Font.Bold = false;
            this.Text25.Style.Font.Italic = false;
            this.Text25.Style.Font.Name = "Arial";
            this.Text25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text25.Style.Font.Strikeout = false;
            this.Text25.Style.Font.Underline = false;
            this.Text25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text25.Style.Visible = true;
            this.Text25.Value = "Date Requested:";
            // 
            // Text22
            // 
            this.Text22.CanGrow = false;
            this.Text22.CanShrink = false;
            this.Text22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(4.204D));
            this.Text22.Multiline = false;
            this.Text22.Name = "Text22";
            this.Text22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text22.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text22.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text22.Style.Color = System.Drawing.Color.Black;
            this.Text22.Style.Font.Bold = true;
            this.Text22.Style.Font.Italic = false;
            this.Text22.Style.Font.Name = "Arial";
            this.Text22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text22.Style.Font.Strikeout = false;
            this.Text22.Style.Font.Underline = false;
            this.Text22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text22.Style.Visible = true;
            this.Text22.Value = "RA Number:";
            // 
            // Text15
            // 
            this.Text15.CanGrow = false;
            this.Text15.CanShrink = false;
            this.Text15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(5.024D));
            this.Text15.Multiline = false;
            this.Text15.Name = "Text15";
            this.Text15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.482D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text15.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text15.Style.Color = System.Drawing.Color.Black;
            this.Text15.Style.Font.Bold = false;
            this.Text15.Style.Font.Italic = false;
            this.Text15.Style.Font.Name = "Arial";
            this.Text15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text15.Style.Font.Strikeout = false;
            this.Text15.Style.Font.Underline = false;
            this.Text15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text15.Style.Visible = true;
            this.Text15.Value = "Email:";
            // 
            // Text20
            // 
            this.Text20.CanGrow = false;
            this.Text20.CanShrink = false;
            this.Text20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(4.601D));
            this.Text20.Multiline = false;
            this.Text20.Name = "Text20";
            this.Text20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.482D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text20.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text20.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text20.Style.Color = System.Drawing.Color.Black;
            this.Text20.Style.Font.Bold = false;
            this.Text20.Style.Font.Italic = false;
            this.Text20.Style.Font.Name = "Arial";
            this.Text20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text20.Style.Font.Strikeout = false;
            this.Text20.Style.Font.Underline = false;
            this.Text20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text20.Style.Visible = true;
            this.Text20.Value = "Phone:";
            // 
            // Text19
            // 
            this.Text19.CanGrow = false;
            this.Text19.CanShrink = false;
            this.Text19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(4.187D));
            this.Text19.Multiline = false;
            this.Text19.Name = "Text19";
            this.Text19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.482D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text19.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text19.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text19.Style.Color = System.Drawing.Color.Black;
            this.Text19.Style.Font.Bold = false;
            this.Text19.Style.Font.Italic = false;
            this.Text19.Style.Font.Name = "Arial";
            this.Text19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text19.Style.Font.Strikeout = false;
            this.Text19.Style.Font.Underline = false;
            this.Text19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text19.Style.Visible = true;
            this.Text19.Value = "Contact:";
            // 
            // Line3
            // 
            this.Line3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.669D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            this.Line3.Name = "Line3";
            this.Line3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.Line3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Point(4D), Telerik.Reporting.Drawing.Unit.Cm(5.3D));
            this.Line3.Style.Color = System.Drawing.Color.Black;
            this.Line3.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line3.Style.Visible = true;
            // 
            // Line6
            // 
            this.Line6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.779D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            this.Line6.Name = "Line6";
            this.Line6.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.Line6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Point(4D), Telerik.Reporting.Drawing.Unit.Cm(5.3D));
            this.Line6.Style.Color = System.Drawing.Color.Black;
            this.Line6.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line6.Style.Visible = true;
            // 
            // Name1
            // 
            this.Name1.CanGrow = false;
            this.Name1.CanShrink = false;
            this.Name1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(4.151D));
            this.Name1.Multiline = false;
            this.Name1.Name = "Name1";
            this.Name1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.461D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Name1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Name1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name1.Style.Color = System.Drawing.Color.Black;
            this.Name1.Style.Font.Bold = false;
            this.Name1.Style.Font.Italic = false;
            this.Name1.Style.Font.Name = "Arial";
            this.Name1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Name1.Style.Font.Strikeout = false;
            this.Name1.Style.Font.Underline = false;
            this.Name1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name1.Style.Visible = true;
            this.Name1.Value = "= Fields.Name";
            // 
            // PhoneNumber1
            // 
            this.PhoneNumber1.CanGrow = false;
            this.PhoneNumber1.CanShrink = false;
            this.PhoneNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.473D), Telerik.Reporting.Drawing.Unit.Cm(5.783D));
            this.PhoneNumber1.Multiline = false;
            this.PhoneNumber1.Name = "PhoneNumber1";
            this.PhoneNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.024D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.PhoneNumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.PhoneNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.PhoneNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.PhoneNumber1.Style.Color = System.Drawing.Color.Black;
            this.PhoneNumber1.Style.Font.Bold = false;
            this.PhoneNumber1.Style.Font.Italic = false;
            this.PhoneNumber1.Style.Font.Name = "Arial";
            this.PhoneNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.PhoneNumber1.Style.Font.Strikeout = false;
            this.PhoneNumber1.Style.Font.Underline = false;
            this.PhoneNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.PhoneNumber1.Style.Visible = true;
            this.PhoneNumber1.Value = "=Fields.[PhoneNumber]";
            // 
            // EmailAddress1
            // 
            this.EmailAddress1.CanGrow = false;
            this.EmailAddress1.CanShrink = false;
            this.EmailAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.473D), Telerik.Reporting.Drawing.Unit.Cm(6.215D));
            this.EmailAddress1.Multiline = false;
            this.EmailAddress1.Name = "EmailAddress1";
            this.EmailAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.024D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.EmailAddress1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.EmailAddress1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.EmailAddress1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.EmailAddress1.Style.Color = System.Drawing.Color.Black;
            this.EmailAddress1.Style.Font.Bold = false;
            this.EmailAddress1.Style.Font.Italic = false;
            this.EmailAddress1.Style.Font.Name = "Arial";
            this.EmailAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.EmailAddress1.Style.Font.Strikeout = false;
            this.EmailAddress1.Style.Font.Underline = false;
            this.EmailAddress1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.EmailAddress1.Style.Visible = true;
            this.EmailAddress1.Value = "=Fields.[EmailAddress]";
            // 
            // DEANumber1
            // 
            this.DEANumber1.CanGrow = false;
            this.DEANumber1.CanShrink = false;
            this.DEANumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.69D), Telerik.Reporting.Drawing.Unit.Cm(6.735D));
            this.DEANumber1.Multiline = false;
            this.DEANumber1.Name = "DEANumber1";
            this.DEANumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.789D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.DEANumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.DEANumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.DEANumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.DEANumber1.Style.Color = System.Drawing.Color.Black;
            this.DEANumber1.Style.Font.Bold = false;
            this.DEANumber1.Style.Font.Italic = false;
            this.DEANumber1.Style.Font.Name = "Arial";
            this.DEANumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.DEANumber1.Style.Font.Strikeout = false;
            this.DEANumber1.Style.Font.Underline = false;
            this.DEANumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.DEANumber1.Style.Visible = true;
            this.DEANumber1.Value = "=Fields.[DEANumber]";
            // 
            // DebitMemoNumber1
            // 
            this.DebitMemoNumber1.CanGrow = false;
            this.DebitMemoNumber1.CanShrink = false;
            this.DebitMemoNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.69D), Telerik.Reporting.Drawing.Unit.Cm(7.194D));
            this.DebitMemoNumber1.Multiline = false;
            this.DebitMemoNumber1.Name = "DebitMemoNumber1";
            this.DebitMemoNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.789D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.DebitMemoNumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.DebitMemoNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.DebitMemoNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.DebitMemoNumber1.Style.Color = System.Drawing.Color.Black;
            this.DebitMemoNumber1.Style.Font.Bold = false;
            this.DebitMemoNumber1.Style.Font.Italic = false;
            this.DebitMemoNumber1.Style.Font.Name = "Arial";
            this.DebitMemoNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.DebitMemoNumber1.Style.Font.Strikeout = false;
            this.DebitMemoNumber1.Style.Font.Underline = false;
            this.DebitMemoNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.DebitMemoNumber1.Style.Visible = true;
            this.DebitMemoNumber1.Value = "=Fields.[DebitMemoNumber]";
            // 
            // PrintAccountTitle1
            // 
            this.PrintAccountTitle1.CanGrow = false;
            this.PrintAccountTitle1.CanShrink = false;
            this.PrintAccountTitle1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(7.573D));
            this.PrintAccountTitle1.Multiline = false;
            this.PrintAccountTitle1.Name = "PrintAccountTitle1";
            this.PrintAccountTitle1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.387D), Telerik.Reporting.Drawing.Unit.Cm(0.406D));
            this.PrintAccountTitle1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.PrintAccountTitle1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.PrintAccountTitle1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.PrintAccountTitle1.Style.Color = System.Drawing.Color.Black;
            this.PrintAccountTitle1.Style.Font.Bold = false;
            this.PrintAccountTitle1.Style.Font.Italic = false;
            this.PrintAccountTitle1.Style.Font.Name = "Arial";
            this.PrintAccountTitle1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.PrintAccountTitle1.Style.Font.Strikeout = false;
            this.PrintAccountTitle1.Style.Font.Underline = false;
            this.PrintAccountTitle1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.PrintAccountTitle1.Style.Visible = true;
            this.PrintAccountTitle1.Value = "=Fields.[PrintAccountTitle]";
            // 
            // PrintAccount1
            // 
            this.PrintAccount1.CanGrow = false;
            this.PrintAccount1.CanShrink = false;
            this.PrintAccount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(8.058D));
            this.PrintAccount1.Multiline = false;
            this.PrintAccount1.Name = "PrintAccount1";
            this.PrintAccount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.352D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.PrintAccount1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.PrintAccount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.PrintAccount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.PrintAccount1.Style.Color = System.Drawing.Color.Black;
            this.PrintAccount1.Style.Font.Bold = false;
            this.PrintAccount1.Style.Font.Italic = false;
            this.PrintAccount1.Style.Font.Name = "Arial";
            this.PrintAccount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.PrintAccount1.Style.Font.Strikeout = false;
            this.PrintAccount1.Style.Font.Underline = false;
            this.PrintAccount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.PrintAccount1.Style.Visible = true;
            this.PrintAccount1.Value = "=Fields.[PrintAccount]";
            // 
            // ThirdPartyReverseDistributorName1
            // 
            this.ThirdPartyReverseDistributorName1.CanGrow = false;
            this.ThirdPartyReverseDistributorName1.CanShrink = false;
            this.ThirdPartyReverseDistributorName1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.078D), Telerik.Reporting.Drawing.Unit.Cm(6.444D));
            this.ThirdPartyReverseDistributorName1.Name = "ThirdPartyReverseDistributorName1";
            this.ThirdPartyReverseDistributorName1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.352D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.ThirdPartyReverseDistributorName1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.ThirdPartyReverseDistributorName1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ThirdPartyReverseDistributorName1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ThirdPartyReverseDistributorName1.Style.Color = System.Drawing.Color.Black;
            this.ThirdPartyReverseDistributorName1.Style.Font.Bold = false;
            this.ThirdPartyReverseDistributorName1.Style.Font.Italic = false;
            this.ThirdPartyReverseDistributorName1.Style.Font.Name = "Arial";
            this.ThirdPartyReverseDistributorName1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ThirdPartyReverseDistributorName1.Style.Font.Strikeout = false;
            this.ThirdPartyReverseDistributorName1.Style.Font.Underline = false;
            this.ThirdPartyReverseDistributorName1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ThirdPartyReverseDistributorName1.Style.Visible = true;
            this.ThirdPartyReverseDistributorName1.Value = "=Fields.[ThirdPartyReverseDistributorName]";
            // 
            // ThirdPartyReverseDistributorDEANumber1
            // 
            this.ThirdPartyReverseDistributorDEANumber1.CanGrow = false;
            this.ThirdPartyReverseDistributorDEANumber1.CanShrink = false;
            this.ThirdPartyReverseDistributorDEANumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.6D), Telerik.Reporting.Drawing.Unit.Cm(7.997D));
            this.ThirdPartyReverseDistributorDEANumber1.Multiline = false;
            this.ThirdPartyReverseDistributorDEANumber1.Name = "ThirdPartyReverseDistributorDEANumber1";
            this.ThirdPartyReverseDistributorDEANumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.742D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.ThirdPartyReverseDistributorDEANumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.ThirdPartyReverseDistributorDEANumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ThirdPartyReverseDistributorDEANumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Color = System.Drawing.Color.Black;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Bold = false;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Italic = false;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Name = "Arial";
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Strikeout = false;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Font.Underline = false;
            this.ThirdPartyReverseDistributorDEANumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ThirdPartyReverseDistributorDEANumber1.Style.Visible = true;
            this.ThirdPartyReverseDistributorDEANumber1.Value = "=Fields.[ThirdPartyReverseDistributorDEANumber]";
            // 
            // RequestorName1
            // 
            this.RequestorName1.CanGrow = false;
            this.RequestorName1.CanShrink = false;
            this.RequestorName1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.728D), Telerik.Reporting.Drawing.Unit.Cm(4.222D));
            this.RequestorName1.Multiline = false;
            this.RequestorName1.Name = "RequestorName1";
            this.RequestorName1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.235D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.RequestorName1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.RequestorName1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RequestorName1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.RequestorName1.Style.Color = System.Drawing.Color.Black;
            this.RequestorName1.Style.Font.Bold = false;
            this.RequestorName1.Style.Font.Italic = false;
            this.RequestorName1.Style.Font.Name = "Arial";
            this.RequestorName1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.RequestorName1.Style.Font.Strikeout = false;
            this.RequestorName1.Style.Font.Underline = false;
            this.RequestorName1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.RequestorName1.Style.Visible = true;
            this.RequestorName1.Value = "=Fields.[RequestorName]";
            // 
            // RequestorPhoneNumber1
            // 
            this.RequestorPhoneNumber1.CanGrow = false;
            this.RequestorPhoneNumber1.CanShrink = false;
            this.RequestorPhoneNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.728D), Telerik.Reporting.Drawing.Unit.Cm(4.636D));
            this.RequestorPhoneNumber1.Multiline = false;
            this.RequestorPhoneNumber1.Name = "RequestorPhoneNumber1";
            this.RequestorPhoneNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.235D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.RequestorPhoneNumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.RequestorPhoneNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RequestorPhoneNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.RequestorPhoneNumber1.Style.Color = System.Drawing.Color.Black;
            this.RequestorPhoneNumber1.Style.Font.Bold = false;
            this.RequestorPhoneNumber1.Style.Font.Italic = false;
            this.RequestorPhoneNumber1.Style.Font.Name = "Arial";
            this.RequestorPhoneNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.RequestorPhoneNumber1.Style.Font.Strikeout = false;
            this.RequestorPhoneNumber1.Style.Font.Underline = false;
            this.RequestorPhoneNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.RequestorPhoneNumber1.Style.Visible = true;
            this.RequestorPhoneNumber1.Value = "=Fields.[RequestorPhoneNumber]";
            // 
            // RequestorEmailAddress1
            // 
            this.RequestorEmailAddress1.CanGrow = false;
            this.RequestorEmailAddress1.CanShrink = false;
            this.RequestorEmailAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.728D), Telerik.Reporting.Drawing.Unit.Cm(5.06D));
            this.RequestorEmailAddress1.Multiline = false;
            this.RequestorEmailAddress1.Name = "RequestorEmailAddress1";
            this.RequestorEmailAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.235D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.RequestorEmailAddress1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.RequestorEmailAddress1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RequestorEmailAddress1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.RequestorEmailAddress1.Style.Color = System.Drawing.Color.Black;
            this.RequestorEmailAddress1.Style.Font.Bold = false;
            this.RequestorEmailAddress1.Style.Font.Italic = false;
            this.RequestorEmailAddress1.Style.Font.Name = "Arial";
            this.RequestorEmailAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.RequestorEmailAddress1.Style.Font.Strikeout = false;
            this.RequestorEmailAddress1.Style.Font.Underline = false;
            this.RequestorEmailAddress1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.RequestorEmailAddress1.Style.Visible = true;
            this.RequestorEmailAddress1.Value = "=Fields.[RequestorEmailAddress]";
            // 
            // ReturnAuthorizationNumber1
            // 
            this.ReturnAuthorizationNumber1.CanGrow = false;
            this.ReturnAuthorizationNumber1.CanShrink = false;
            this.ReturnAuthorizationNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.046D), Telerik.Reporting.Drawing.Unit.Cm(4.239D));
            this.ReturnAuthorizationNumber1.Multiline = false;
            this.ReturnAuthorizationNumber1.Name = "ReturnAuthorizationNumber1";
            this.ReturnAuthorizationNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.972D), Telerik.Reporting.Drawing.Unit.Cm(0.381D));
            this.ReturnAuthorizationNumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.ReturnAuthorizationNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationNumber1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationNumber1.Style.Font.Bold = true;
            this.ReturnAuthorizationNumber1.Style.Font.Italic = false;
            this.ReturnAuthorizationNumber1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.ReturnAuthorizationNumber1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationNumber1.Style.Font.Underline = false;
            this.ReturnAuthorizationNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ReturnAuthorizationNumber1.Style.Visible = true;
            this.ReturnAuthorizationNumber1.Value = "=Fields.[ReturnAuthorizationNumber]";
            // 
            // RequestDate1
            // 
            this.RequestDate1.CanGrow = false;
            this.RequestDate1.CanShrink = false;
            this.RequestDate1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.117D), Telerik.Reporting.Drawing.Unit.Cm(4.725D));
            this.RequestDate1.Multiline = false;
            this.RequestDate1.Name = "RequestDate1";
            this.RequestDate1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.729D), Telerik.Reporting.Drawing.Unit.Cm(0.36D));
            this.RequestDate1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.RequestDate1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RequestDate1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.RequestDate1.Style.Color = System.Drawing.Color.Black;
            this.RequestDate1.Style.Font.Bold = false;
            this.RequestDate1.Style.Font.Italic = false;
            this.RequestDate1.Style.Font.Name = "Arial";
            this.RequestDate1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.RequestDate1.Style.Font.Strikeout = false;
            this.RequestDate1.Style.Font.Underline = false;
            this.RequestDate1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.RequestDate1.Style.Visible = true;
            this.RequestDate1.Value = "=Fields.[RequestDate]";
            // 
            // ReceivedDate1
            // 
            this.ReceivedDate1.CanGrow = false;
            this.ReceivedDate1.CanShrink = false;
            this.ReceivedDate1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.117D), Telerik.Reporting.Drawing.Unit.Cm(5.189D));
            this.ReceivedDate1.Multiline = false;
            this.ReceivedDate1.Name = "ReceivedDate1";
            this.ReceivedDate1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.729D), Telerik.Reporting.Drawing.Unit.Cm(0.36D));
            this.ReceivedDate1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.ReceivedDate1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReceivedDate1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReceivedDate1.Style.Color = System.Drawing.Color.Black;
            this.ReceivedDate1.Style.Font.Bold = false;
            this.ReceivedDate1.Style.Font.Italic = false;
            this.ReceivedDate1.Style.Font.Name = "Arial";
            this.ReceivedDate1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.ReceivedDate1.Style.Font.Strikeout = false;
            this.ReceivedDate1.Style.Font.Underline = false;
            this.ReceivedDate1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ReceivedDate1.Style.Visible = true;
            this.ReceivedDate1.Value = "=Fields.[ReceivedDate]";
            // 
            // Text6
            // 
            this.Text6.CanGrow = false;
            this.Text6.CanShrink = false;
            this.Text6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(5.593D));
            this.Text6.Multiline = false;
            this.Text6.Name = "Text6";
            this.Text6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text6.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text6.Style.Color = System.Drawing.Color.Black;
            this.Text6.Style.Font.Bold = false;
            this.Text6.Style.Font.Italic = false;
            this.Text6.Style.Font.Name = "Arial";
            this.Text6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text6.Style.Font.Strikeout = false;
            this.Text6.Style.Font.Underline = false;
            this.Text6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text6.Style.Visible = true;
            this.Text6.Value = "Date Credited:";
            // 
            // CreditIssuedDate1
            // 
            this.CreditIssuedDate1.CanGrow = false;
            this.CreditIssuedDate1.CanShrink = false;
            this.CreditIssuedDate1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.117D), Telerik.Reporting.Drawing.Unit.Cm(5.654D));
            this.CreditIssuedDate1.Multiline = false;
            this.CreditIssuedDate1.Name = "CreditIssuedDate1";
            this.CreditIssuedDate1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.729D), Telerik.Reporting.Drawing.Unit.Cm(0.36D));
            this.CreditIssuedDate1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.CreditIssuedDate1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.CreditIssuedDate1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.CreditIssuedDate1.Style.Color = System.Drawing.Color.Black;
            this.CreditIssuedDate1.Style.Font.Bold = false;
            this.CreditIssuedDate1.Style.Font.Italic = false;
            this.CreditIssuedDate1.Style.Font.Name = "Arial";
            this.CreditIssuedDate1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.CreditIssuedDate1.Style.Font.Strikeout = false;
            this.CreditIssuedDate1.Style.Font.Underline = false;
            this.CreditIssuedDate1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.CreditIssuedDate1.Style.Visible = true;
            this.CreditIssuedDate1.Value = "=Fields.[CreditIssuedDate]";
            // 
            // Text21
            // 
            this.Text21.CanGrow = false;
            this.Text21.CanShrink = false;
            this.Text21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(3.596D));
            this.Text21.Name = "Text21";
            this.Text21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.705D), Telerik.Reporting.Drawing.Unit.Cm(0.406D));
            this.Text21.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text21.Style.Color = System.Drawing.Color.Black;
            this.Text21.Style.Font.Bold = true;
            this.Text21.Style.Font.Italic = false;
            this.Text21.Style.Font.Name = "Arial";
            this.Text21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text21.Style.Font.Strikeout = false;
            this.Text21.Style.Font.Underline = false;
            this.Text21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text21.Style.Visible = true;
            this.Text21.Value = "RETURN AUTHORIZATION INFORMATION";
            // 
            // Text58
            // 
            this.Text58.CanGrow = false;
            this.Text58.CanShrink = false;
            this.Text58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(6.056D));
            this.Text58.Multiline = false;
            this.Text58.Name = "Text58";
            this.Text58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text58.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text58.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text58.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text58.Style.Color = System.Drawing.Color.Black;
            this.Text58.Style.Font.Bold = false;
            this.Text58.Style.Font.Italic = false;
            this.Text58.Style.Font.Name = "Arial";
            this.Text58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text58.Style.Font.Strikeout = false;
            this.Text58.Style.Font.Underline = false;
            this.Text58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text58.Style.Visible = true;
            this.Text58.Value = "Credit Memo #:";
            // 
            // Text63
            // 
            this.Text63.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.ShowAdditionalRAsIssued.Value"));
            this.Text63.CanGrow = false;
            this.Text63.CanShrink = false;
            this.Text63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.044D), Telerik.Reporting.Drawing.Unit.Cm(7.6D));
            this.Text63.Multiline = false;
            this.Text63.Name = "Text63";
            this.Text63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.849D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text63.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text63.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text63.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text63.Style.Color = System.Drawing.Color.Black;
            this.Text63.Style.Font.Bold = false;
            this.Text63.Style.Font.Italic = false;
            this.Text63.Style.Font.Name = "Arial";
            this.Text63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text63.Style.Font.Strikeout = false;
            this.Text63.Style.Font.Underline = false;
            this.Text63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text63.Style.Visible = true;
            this.Text63.Value = "Additional RAs Issued:";
            // 
            // ReturnAuthorizationNumberSplit1
            // 
            this.ReturnAuthorizationNumberSplit1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.ShowAdditionalRAsIssued.Value"));
            this.ReturnAuthorizationNumberSplit1.CanGrow = false;
            this.ReturnAuthorizationNumberSplit1.CanShrink = false;
            this.ReturnAuthorizationNumberSplit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.044D), Telerik.Reporting.Drawing.Unit.Cm(8.041D));
            this.ReturnAuthorizationNumberSplit1.Multiline = false;
            this.ReturnAuthorizationNumberSplit1.Name = "ReturnAuthorizationNumberSplit1";
            this.ReturnAuthorizationNumberSplit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.237D), Telerik.Reporting.Drawing.Unit.Cm(0.344D));
            this.ReturnAuthorizationNumberSplit1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.ReturnAuthorizationNumberSplit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReturnAuthorizationNumberSplit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ReturnAuthorizationNumberSplit1.Style.Color = System.Drawing.Color.Black;
            this.ReturnAuthorizationNumberSplit1.Style.Font.Bold = false;
            this.ReturnAuthorizationNumberSplit1.Style.Font.Italic = false;
            this.ReturnAuthorizationNumberSplit1.Style.Font.Name = "Arial";
            this.ReturnAuthorizationNumberSplit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.ReturnAuthorizationNumberSplit1.Style.Font.Strikeout = false;
            this.ReturnAuthorizationNumberSplit1.Style.Font.Underline = false;
            this.ReturnAuthorizationNumberSplit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ReturnAuthorizationNumberSplit1.Style.Visible = true;
            this.ReturnAuthorizationNumberSplit1.Value = "=Fields.[ReturnAuthorizationNumberSplit]";
            // 
            // Text64
            // 
            this.Text64.CanGrow = false;
            this.Text64.CanShrink = false;
            this.Text64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(6.577D));
            this.Text64.Multiline = false;
            this.Text64.Name = "Text64";
            this.Text64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text64.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.Text64.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text64.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text64.Style.Color = System.Drawing.Color.Black;
            this.Text64.Style.Font.Bold = false;
            this.Text64.Style.Font.Italic = false;
            this.Text64.Style.Font.Name = "Arial";
            this.Text64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text64.Style.Font.Strikeout = false;
            this.Text64.Style.Font.Underline = false;
            this.Text64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text64.Style.Visible = true;
            this.Text64.Value = "Direct Account:";
            // 
            // PrintName1
            // 
            this.PrintName1.CanGrow = false;
            this.PrintName1.CanShrink = false;
            this.PrintName1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.977D), Telerik.Reporting.Drawing.Unit.Cm(7.088D));
            this.PrintName1.Multiline = false;
            this.PrintName1.Name = "PrintName1";
            this.PrintName1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.387D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.PrintName1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.PrintName1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.PrintName1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.PrintName1.Style.Color = System.Drawing.Color.Black;
            this.PrintName1.Style.Font.Bold = false;
            this.PrintName1.Style.Font.Italic = false;
            this.PrintName1.Style.Font.Name = "Arial";
            this.PrintName1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.PrintName1.Style.Font.Strikeout = false;
            this.PrintName1.Style.Font.Underline = false;
            this.PrintName1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.PrintName1.Style.Visible = true;
            this.PrintName1.Value = "=Fields.[PrintName]";
            // 
            // PrimaryCreditMemoNumber1
            // 
            this.PrimaryCreditMemoNumber1.CanGrow = false;
            this.PrimaryCreditMemoNumber1.CanShrink = false;
            this.PrimaryCreditMemoNumber1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.126D), Telerik.Reporting.Drawing.Unit.Cm(6.118D));
            this.PrimaryCreditMemoNumber1.Multiline = false;
            this.PrimaryCreditMemoNumber1.Name = "PrimaryCreditMemoNumber1";
            this.PrimaryCreditMemoNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.519D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.PrimaryCreditMemoNumber1.Style.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.PrimaryCreditMemoNumber1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.PrimaryCreditMemoNumber1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.PrimaryCreditMemoNumber1.Style.Color = System.Drawing.Color.Black;
            this.PrimaryCreditMemoNumber1.Style.Font.Bold = false;
            this.PrimaryCreditMemoNumber1.Style.Font.Italic = false;
            this.PrimaryCreditMemoNumber1.Style.Font.Name = "Arial";
            this.PrimaryCreditMemoNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.PrimaryCreditMemoNumber1.Style.Font.Strikeout = false;
            this.PrimaryCreditMemoNumber1.Style.Font.Underline = false;
            this.PrimaryCreditMemoNumber1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.PrimaryCreditMemoNumber1.Style.Visible = true;
            this.PrimaryCreditMemoNumber1.Value = "=Fields.[PrimaryCreditMemoNumber]";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.053D), Telerik.Reporting.Drawing.Unit.Cm(4.636D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.461D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.htmlTextBox1.Value = "= Replace(Fields.RAAddress, \"</br>\", \"<br />\")";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(6.9D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.242D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.htmlTextBox2.Value = "= Replace(Fields.ThirdPartyReverseDistributorAddress, \"</br>\", \"<br />\")";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.CanShrink = false;
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(0.783D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.144D), Telerik.Reporting.Drawing.Unit.Cm(1.27D));
            this.textBox2.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox2.Style.Color = System.Drawing.Color.Black;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Italic = false;
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox2.Style.Font.Strikeout = false;
            this.textBox2.Style.Font.Underline = false;
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.Visible = true;
            this.textBox2.Value = "Credit Comparison Report For Estimated Vs Processed Items (With Reasons)";
            // 
            // textBox1
            // 
            this.textBox1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= IIf(Parameters.StartDate.Value Is Null Or Parameters.EndDate.Value Is Null, fal" +
            "se, true)"));
            this.textBox1.CanGrow = false;
            this.textBox1.CanShrink = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(2.283D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.144D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox1.Style.Color = System.Drawing.Color.Black;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Italic = false;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Font.Strikeout = false;
            this.textBox1.Style.Font.Underline = false;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.Visible = false;
            this.textBox1.Value = "= Fields.ReportPeriod";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.48D), Telerik.Reporting.Drawing.Unit.Cm(0.768D));
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.08D), Telerik.Reporting.Drawing.Unit.Cm(2.057D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.pictureBox4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.pictureBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pictureBox4.Style.Visible = true;
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // subReportLogo
            // 
            this.subReportLogo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.394D), Telerik.Reporting.Drawing.Unit.Inch(0.118D));
            this.subReportLogo.Name = "subReportLogo";
            instanceReportSource2.Parameters.Add(new Telerik.Reporting.Parameter("ManufacturerProfileCode", "= Fields.ManufacturerProfileCode"));
            instanceReportSource2.ReportDocument = this.subReportLogo1;
            this.subReportLogo.ReportSource = instanceReportSource2;
            this.subReportLogo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.835D), Telerik.Reporting.Drawing.Unit.Inch(1.031D));
            // 
            // groupFooterSection
            // 
            this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.132D);
            this.groupFooterSection.KeepTogether = false;
            this.groupFooterSection.Name = "groupFooterSection";
            this.groupFooterSection.Style.Visible = false;
            // 
            // groupHeaderSection
            // 
            this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Cm(1.694D);
            this.groupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape4,
            this.shape3,
            this.textBox24,
            this.textBox23,
            this.textBox16,
            this.textBox15,
            this.textBox14,
            this.textBox13,
            this.textBox12,
            this.textBox11,
            this.shape2,
            this.textBox10,
            this.textBox9,
            this.textBox8,
            this.textBox7,
            this.shape1,
            this.textBox6,
            this.textBox5,
            this.textBox4,
            this.textBox3});
            this.groupHeaderSection.KeepTogether = false;
            this.groupHeaderSection.Name = "groupHeaderSection";
            this.groupHeaderSection.PageBreak = Telerik.Reporting.PageBreak.None;
            this.groupHeaderSection.PrintOnEveryPage = true;
            // 
            // shape4
            // 
            this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.243D), Telerik.Reporting.Drawing.Unit.Cm(0.094D));
            this.shape4.Name = "shape4";
            this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
            this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.57D), Telerik.Reporting.Drawing.Unit.Cm(1.508D));
            this.shape4.Stretch = true;
            this.shape4.Style.BackgroundColor = System.Drawing.Color.White;
            this.shape4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.shape4.Style.Color = System.Drawing.Color.Black;
            this.shape4.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape4.Style.Visible = true;
            // 
            // shape3
            // 
            this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.094D));
            this.shape3.Name = "shape3";
            this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
            this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.498D), Telerik.Reporting.Drawing.Unit.Cm(1.508D));
            this.shape3.Stretch = true;
            this.shape3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.shape3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.shape3.Style.Color = System.Drawing.Color.Black;
            this.shape3.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape3.Style.Visible = true;
            // 
            // textBox24
            // 
            this.textBox24.CanGrow = false;
            this.textBox24.CanShrink = false;
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.887D), Telerik.Reporting.Drawing.Unit.Cm(0.241D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.182D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox24.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.Color = System.Drawing.Color.Black;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Italic = false;
            this.textBox24.Style.Font.Name = "Arial";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox24.Style.Font.Strikeout = false;
            this.textBox24.Style.Font.Underline = false;
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.Visible = true;
            this.textBox24.Value = "Reported on RA";
            // 
            // textBox23
            // 
            this.textBox23.CanGrow = false;
            this.textBox23.CanShrink = false;
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.223D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.101D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.Color = System.Drawing.Color.Black;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Italic = false;
            this.textBox23.Style.Font.Name = "Arial";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox23.Style.Font.Strikeout = false;
            this.textBox23.Style.Font.Underline = false;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.Style.Visible = true;
            this.textBox23.Value = "Unit Condition";
            // 
            // textBox16
            // 
            this.textBox16.CanGrow = false;
            this.textBox16.CanShrink = false;
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.621D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.542D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox16.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.Color = System.Drawing.Color.Black;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Italic = false;
            this.textBox16.Style.Font.Name = "Arial";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox16.Style.Font.Strikeout = false;
            this.textBox16.Style.Font.Underline = false;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.Visible = true;
            this.textBox16.Value = "Return Value";
            // 
            // textBox15
            // 
            this.textBox15.CanGrow = false;
            this.textBox15.CanShrink = false;
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.448D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.Color = System.Drawing.Color.Black;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Italic = false;
            this.textBox15.Style.Font.Name = "Arial";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox15.Style.Font.Strikeout = false;
            this.textBox15.Style.Font.Underline = false;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox15.Style.Visible = true;
            this.textBox15.Value = "Debit Memo  Value";
            // 
            // textBox14
            // 
            this.textBox14.CanGrow = false;
            this.textBox14.CanShrink = false;
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.151D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.Color = System.Drawing.Color.Black;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Italic = false;
            this.textBox14.Style.Font.Name = "Arial";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox14.Style.Font.Strikeout = false;
            this.textBox14.Style.Font.Underline = false;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.Visible = true;
            this.textBox14.Value = "Quantity";
            // 
            // textBox13
            // 
            this.textBox13.CanGrow = false;
            this.textBox13.CanShrink = false;
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.308D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.693D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox13.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox13.Style.Color = System.Drawing.Color.Black;
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Italic = false;
            this.textBox13.Style.Font.Name = "Arial";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox13.Style.Font.Strikeout = false;
            this.textBox13.Style.Font.Underline = false;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.Visible = true;
            this.textBox13.Value = "Exp. Date";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.CanShrink = false;
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.887D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.328D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.Color = System.Drawing.Color.Black;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Italic = false;
            this.textBox12.Style.Font.Name = "Arial";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Style.Font.Strikeout = false;
            this.textBox12.Style.Font.Underline = false;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.Visible = true;
            this.textBox12.Value = "Lot Number";
            // 
            // textBox11
            // 
            this.textBox11.CanGrow = false;
            this.textBox11.CanShrink = false;
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(20.86D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.244D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox11.Style.Color = System.Drawing.Color.Black;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Italic = false;
            this.textBox11.Style.Font.Name = "Arial";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.Font.Strikeout = false;
            this.textBox11.Style.Font.Underline = false;
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.Visible = true;
            this.textBox11.Value = "Unit Count";
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.787D), Telerik.Reporting.Drawing.Unit.Cm(0.62D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.483D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.shape2.Style.Color = System.Drawing.Color.Black;
            this.shape2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape2.Style.Visible = true;
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.CanShrink = false;
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.349D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.328D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Italic = false;
            this.textBox10.Style.Font.Name = "Arial";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.Font.Strikeout = false;
            this.textBox10.Style.Font.Underline = false;
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.Visible = true;
            this.textBox10.Value = "Lot Number";
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.CanShrink = false;
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.671D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.101D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox9.Style.Color = System.Drawing.Color.Black;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Italic = false;
            this.textBox9.Style.Font.Name = "Arial";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox9.Style.Font.Strikeout = false;
            this.textBox9.Style.Font.Underline = false;
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.Visible = true;
            this.textBox9.Value = "Unit\nCond";
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.CanShrink = false;
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.74D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox8.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.Color = System.Drawing.Color.Black;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Italic = false;
            this.textBox8.Style.Font.Name = "Arial";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox8.Style.Font.Strikeout = false;
            this.textBox8.Style.Font.Underline = false;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.Visible = true;
            this.textBox8.Value = "Quantity";
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.CanShrink = false;
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.861D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.693D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox7.Style.Color = System.Drawing.Color.Black;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Italic = false;
            this.textBox7.Style.Font.Name = "Arial";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.Font.Strikeout = false;
            this.textBox7.Style.Font.Underline = false;
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.Visible = true;
            this.textBox7.Value = "Exp. Date";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.27D), Telerik.Reporting.Drawing.Unit.Cm(0.62D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.526D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.shape1.Style.Color = System.Drawing.Color.Black;
            this.shape1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape1.Style.Visible = true;
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.CanShrink = false;
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.376D), Telerik.Reporting.Drawing.Unit.Cm(0.241D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.314D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.Color = System.Drawing.Color.Black;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Italic = false;
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.Font.Strikeout = false;
            this.textBox6.Style.Font.Underline = false;
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Style.Visible = true;
            this.textBox6.Value = "Received and Processed";
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.CanShrink = false;
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(25.287D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.732D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Italic = false;
            this.textBox5.Style.Font.Name = "Arial";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox5.Style.Font.Strikeout = false;
            this.textBox5.Style.Font.Underline = false;
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.Visible = true;
            this.textBox5.Value = "MO";
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.CanShrink = false;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(26.019D), Telerik.Reporting.Drawing.Unit.Cm(1.158D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.741D), Telerik.Reporting.Drawing.Unit.Cm(0.388D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.Color = System.Drawing.Color.Black;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Italic = false;
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox4.Style.Font.Strikeout = false;
            this.textBox4.Style.Font.Underline = false;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.Visible = true;
            this.textBox4.Value = "SO";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.CanShrink = false;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.28D), Telerik.Reporting.Drawing.Unit.Cm(0.699D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.244D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.Color = System.Drawing.Color.Black;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Italic = false;
            this.textBox3.Style.Font.Name = "Arial";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox3.Style.Font.Strikeout = false;
            this.textBox3.Style.Font.Underline = false;
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.Visible = true;
            this.textBox3.Value = "Unit Price";
            // 
            // GroupFooterArea2
            // 
            this.GroupFooterArea2.Height = Telerik.Reporting.Drawing.Unit.Cm(2.285D);
            this.GroupFooterArea2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Line9,
            this.SumofCaseRAQuantity1,
            this.Text60,
            this.Text59,
            this.SumofOpenedItemQtyCount1,
            this.SumofCaseAmount1,
            this.SumofOpenedAmount1,
            this.SumofSealedAmount1,
            this.SumofCaseItemCount1,
            this.SumofSealedItemCount1,
            this.SumofCaseDebitMemoValue1,
            this.SumofOpenedDebitMemoValue1,
            this.SumofSealedDebitMemoValue1,
            this.Text62,
            this.SumofOpenedRAQuantity1,
            this.SumofSealedRAQuantity1,
            this.Text9,
            this.Text8,
            this.Text7,
            this.SumofReturnableItemValueTotal1,
            this.textBox21});
            this.GroupFooterArea2.KeepTogether = true;
            this.GroupFooterArea2.Name = "GroupFooterArea2";
            this.GroupFooterArea2.PageBreak = Telerik.Reporting.PageBreak.None;
            this.GroupFooterArea2.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupFooterArea2.Style.Visible = true;
            // 
            // Line9
            // 
            this.Line9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Line9.Name = "Line9";
            this.Line9.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(25.976D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line9.Style.Color = System.Drawing.Color.Black;
            this.Line9.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line9.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line9.Style.Visible = true;
            // 
            // SumofCaseRAQuantity1
            // 
            this.SumofCaseRAQuantity1.CanGrow = false;
            this.SumofCaseRAQuantity1.CanShrink = false;
            this.SumofCaseRAQuantity1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.043D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.SumofCaseRAQuantity1.Name = "SumofCaseRAQuantity1";
            this.SumofCaseRAQuantity1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofCaseRAQuantity1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofCaseRAQuantity1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofCaseRAQuantity1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofCaseRAQuantity1.Style.Color = System.Drawing.Color.Black;
            this.SumofCaseRAQuantity1.Style.Font.Bold = false;
            this.SumofCaseRAQuantity1.Style.Font.Italic = false;
            this.SumofCaseRAQuantity1.Style.Font.Name = "Arial";
            this.SumofCaseRAQuantity1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofCaseRAQuantity1.Style.Font.Strikeout = false;
            this.SumofCaseRAQuantity1.Style.Font.Underline = false;
            this.SumofCaseRAQuantity1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.SumofCaseRAQuantity1.Style.Visible = true;
            this.SumofCaseRAQuantity1.Value = "= Sum(Fields.CaseRAQuantity)";
            // 
            // Text60
            // 
            this.Text60.CanGrow = false;
            this.Text60.CanShrink = false;
            this.Text60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(0.696D));
            this.Text60.Name = "Text60";
            this.Text60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text60.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text60.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text60.Style.Color = System.Drawing.Color.Black;
            this.Text60.Style.Font.Bold = false;
            this.Text60.Style.Font.Italic = false;
            this.Text60.Style.Font.Name = "Arial";
            this.Text60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text60.Style.Font.Strikeout = false;
            this.Text60.Style.Font.Underline = false;
            this.Text60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text60.Style.Visible = true;
            this.Text60.Value = "Opened Totals:";
            // 
            // Text59
            // 
            this.Text59.CanGrow = false;
            this.Text59.CanShrink = false;
            this.Text59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(1.198D));
            this.Text59.Name = "Text59";
            this.Text59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text59.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text59.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text59.Style.Color = System.Drawing.Color.Black;
            this.Text59.Style.Font.Bold = false;
            this.Text59.Style.Font.Italic = false;
            this.Text59.Style.Font.Name = "Arial";
            this.Text59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text59.Style.Font.Strikeout = false;
            this.Text59.Style.Font.Underline = false;
            this.Text59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text59.Style.Visible = true;
            this.Text59.Value = "Case Totals:";
            // 
            // SumofOpenedItemQtyCount1
            // 
            this.SumofOpenedItemQtyCount1.CanGrow = false;
            this.SumofOpenedItemQtyCount1.CanShrink = false;
            this.SumofOpenedItemQtyCount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.563D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.SumofOpenedItemQtyCount1.Name = "SumofOpenedItemQtyCount1";
            this.SumofOpenedItemQtyCount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.SumofOpenedItemQtyCount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofOpenedItemQtyCount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofOpenedItemQtyCount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofOpenedItemQtyCount1.Style.Color = System.Drawing.Color.Black;
            this.SumofOpenedItemQtyCount1.Style.Font.Bold = false;
            this.SumofOpenedItemQtyCount1.Style.Font.Italic = false;
            this.SumofOpenedItemQtyCount1.Style.Font.Name = "Arial";
            this.SumofOpenedItemQtyCount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofOpenedItemQtyCount1.Style.Font.Strikeout = false;
            this.SumofOpenedItemQtyCount1.Style.Font.Underline = false;
            this.SumofOpenedItemQtyCount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofOpenedItemQtyCount1.Style.Visible = true;
            this.SumofOpenedItemQtyCount1.Value = "=Sum(Fields.OpenedItemQtyCount)";
            // 
            // SumofCaseAmount1
            // 
            this.SumofCaseAmount1.CanGrow = false;
            this.SumofCaseAmount1.CanShrink = false;
            this.SumofCaseAmount1.Format = "{0:C2}";
            this.SumofCaseAmount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.202D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.SumofCaseAmount1.Name = "SumofCaseAmount1";
            this.SumofCaseAmount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofCaseAmount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofCaseAmount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofCaseAmount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofCaseAmount1.Style.Color = System.Drawing.Color.Black;
            this.SumofCaseAmount1.Style.Font.Bold = false;
            this.SumofCaseAmount1.Style.Font.Italic = false;
            this.SumofCaseAmount1.Style.Font.Name = "Arial";
            this.SumofCaseAmount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofCaseAmount1.Style.Font.Strikeout = false;
            this.SumofCaseAmount1.Style.Font.Underline = false;
            this.SumofCaseAmount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofCaseAmount1.Style.Visible = true;
            this.SumofCaseAmount1.Value = "=Sum(Fields.CaseAmount)";
            // 
            // SumofOpenedAmount1
            // 
            this.SumofOpenedAmount1.CanGrow = false;
            this.SumofOpenedAmount1.CanShrink = false;
            this.SumofOpenedAmount1.Format = "{0:C2}";
            this.SumofOpenedAmount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.202D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.SumofOpenedAmount1.Name = "SumofOpenedAmount1";
            this.SumofOpenedAmount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofOpenedAmount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofOpenedAmount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofOpenedAmount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofOpenedAmount1.Style.Color = System.Drawing.Color.Black;
            this.SumofOpenedAmount1.Style.Font.Bold = false;
            this.SumofOpenedAmount1.Style.Font.Italic = false;
            this.SumofOpenedAmount1.Style.Font.Name = "Arial";
            this.SumofOpenedAmount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofOpenedAmount1.Style.Font.Strikeout = false;
            this.SumofOpenedAmount1.Style.Font.Underline = false;
            this.SumofOpenedAmount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofOpenedAmount1.Style.Visible = true;
            this.SumofOpenedAmount1.Value = "=Sum(Fields.OpenedAmount)";
            // 
            // SumofSealedAmount1
            // 
            this.SumofSealedAmount1.CanGrow = false;
            this.SumofSealedAmount1.CanShrink = false;
            this.SumofSealedAmount1.Format = "{0:C2}";
            this.SumofSealedAmount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.202D), Telerik.Reporting.Drawing.Unit.Cm(0.221D));
            this.SumofSealedAmount1.Name = "SumofSealedAmount1";
            this.SumofSealedAmount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofSealedAmount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofSealedAmount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofSealedAmount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofSealedAmount1.Style.Color = System.Drawing.Color.Black;
            this.SumofSealedAmount1.Style.Font.Bold = false;
            this.SumofSealedAmount1.Style.Font.Italic = false;
            this.SumofSealedAmount1.Style.Font.Name = "Arial";
            this.SumofSealedAmount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofSealedAmount1.Style.Font.Strikeout = false;
            this.SumofSealedAmount1.Style.Font.Underline = false;
            this.SumofSealedAmount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofSealedAmount1.Style.Visible = true;
            this.SumofSealedAmount1.Value = "=Sum(Fields.SealedAmount)";
            // 
            // SumofCaseItemCount1
            // 
            this.SumofCaseItemCount1.CanGrow = false;
            this.SumofCaseItemCount1.CanShrink = false;
            this.SumofCaseItemCount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.563D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.SumofCaseItemCount1.Name = "SumofCaseItemCount1";
            this.SumofCaseItemCount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofCaseItemCount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofCaseItemCount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofCaseItemCount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofCaseItemCount1.Style.Color = System.Drawing.Color.Black;
            this.SumofCaseItemCount1.Style.Font.Bold = false;
            this.SumofCaseItemCount1.Style.Font.Italic = false;
            this.SumofCaseItemCount1.Style.Font.Name = "Arial";
            this.SumofCaseItemCount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofCaseItemCount1.Style.Font.Strikeout = false;
            this.SumofCaseItemCount1.Style.Font.Underline = false;
            this.SumofCaseItemCount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofCaseItemCount1.Style.Visible = true;
            this.SumofCaseItemCount1.Value = "= Sum(Fields.CaseItemCount)";
            // 
            // SumofSealedItemCount1
            // 
            this.SumofSealedItemCount1.CanGrow = false;
            this.SumofSealedItemCount1.CanShrink = false;
            this.SumofSealedItemCount1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.563D), Telerik.Reporting.Drawing.Unit.Cm(0.221D));
            this.SumofSealedItemCount1.Name = "SumofSealedItemCount1";
            this.SumofSealedItemCount1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofSealedItemCount1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofSealedItemCount1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofSealedItemCount1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofSealedItemCount1.Style.Color = System.Drawing.Color.Black;
            this.SumofSealedItemCount1.Style.Font.Bold = false;
            this.SumofSealedItemCount1.Style.Font.Italic = false;
            this.SumofSealedItemCount1.Style.Font.Name = "Arial";
            this.SumofSealedItemCount1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofSealedItemCount1.Style.Font.Strikeout = false;
            this.SumofSealedItemCount1.Style.Font.Underline = false;
            this.SumofSealedItemCount1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofSealedItemCount1.Style.Visible = true;
            this.SumofSealedItemCount1.Value = "=Sum(Fields.SealedItemCount)";
            // 
            // SumofCaseDebitMemoValue1
            // 
            this.SumofCaseDebitMemoValue1.CanGrow = false;
            this.SumofCaseDebitMemoValue1.CanShrink = false;
            this.SumofCaseDebitMemoValue1.Format = "{0:C2}";
            this.SumofCaseDebitMemoValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.SumofCaseDebitMemoValue1.Name = "SumofCaseDebitMemoValue1";
            this.SumofCaseDebitMemoValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofCaseDebitMemoValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofCaseDebitMemoValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofCaseDebitMemoValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofCaseDebitMemoValue1.Style.Color = System.Drawing.Color.Black;
            this.SumofCaseDebitMemoValue1.Style.Font.Bold = false;
            this.SumofCaseDebitMemoValue1.Style.Font.Italic = false;
            this.SumofCaseDebitMemoValue1.Style.Font.Name = "Arial";
            this.SumofCaseDebitMemoValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofCaseDebitMemoValue1.Style.Font.Strikeout = false;
            this.SumofCaseDebitMemoValue1.Style.Font.Underline = false;
            this.SumofCaseDebitMemoValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofCaseDebitMemoValue1.Style.Visible = true;
            this.SumofCaseDebitMemoValue1.Value = "= Sum(Fields.CaseDebitMemoValue)";
            // 
            // SumofOpenedDebitMemoValue1
            // 
            this.SumofOpenedDebitMemoValue1.CanGrow = false;
            this.SumofOpenedDebitMemoValue1.CanShrink = false;
            this.SumofOpenedDebitMemoValue1.Format = "{0:C2}";
            this.SumofOpenedDebitMemoValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.SumofOpenedDebitMemoValue1.Name = "SumofOpenedDebitMemoValue1";
            this.SumofOpenedDebitMemoValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofOpenedDebitMemoValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofOpenedDebitMemoValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofOpenedDebitMemoValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofOpenedDebitMemoValue1.Style.Color = System.Drawing.Color.Black;
            this.SumofOpenedDebitMemoValue1.Style.Font.Bold = false;
            this.SumofOpenedDebitMemoValue1.Style.Font.Italic = false;
            this.SumofOpenedDebitMemoValue1.Style.Font.Name = "Arial";
            this.SumofOpenedDebitMemoValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofOpenedDebitMemoValue1.Style.Font.Strikeout = false;
            this.SumofOpenedDebitMemoValue1.Style.Font.Underline = false;
            this.SumofOpenedDebitMemoValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofOpenedDebitMemoValue1.Style.Visible = true;
            this.SumofOpenedDebitMemoValue1.Value = "= Sum(Fields.OpenedDebitMemoValue)";
            // 
            // SumofSealedDebitMemoValue1
            // 
            this.SumofSealedDebitMemoValue1.CanGrow = false;
            this.SumofSealedDebitMemoValue1.CanShrink = false;
            this.SumofSealedDebitMemoValue1.Format = "{0:C2}";
            this.SumofSealedDebitMemoValue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.221D));
            this.SumofSealedDebitMemoValue1.Name = "SumofSealedDebitMemoValue1";
            this.SumofSealedDebitMemoValue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofSealedDebitMemoValue1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofSealedDebitMemoValue1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofSealedDebitMemoValue1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofSealedDebitMemoValue1.Style.Color = System.Drawing.Color.Black;
            this.SumofSealedDebitMemoValue1.Style.Font.Bold = false;
            this.SumofSealedDebitMemoValue1.Style.Font.Italic = false;
            this.SumofSealedDebitMemoValue1.Style.Font.Name = "Arial";
            this.SumofSealedDebitMemoValue1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofSealedDebitMemoValue1.Style.Font.Strikeout = false;
            this.SumofSealedDebitMemoValue1.Style.Font.Underline = false;
            this.SumofSealedDebitMemoValue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofSealedDebitMemoValue1.Style.Visible = true;
            this.SumofSealedDebitMemoValue1.Value = "= Sum(Fields.SealedDebitMemoValue)";
            // 
            // Text62
            // 
            this.Text62.CanGrow = false;
            this.Text62.CanShrink = false;
            this.Text62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(0.226D));
            this.Text62.Name = "Text62";
            this.Text62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text62.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text62.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text62.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text62.Style.Color = System.Drawing.Color.Black;
            this.Text62.Style.Font.Bold = false;
            this.Text62.Style.Font.Italic = false;
            this.Text62.Style.Font.Name = "Arial";
            this.Text62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text62.Style.Font.Strikeout = false;
            this.Text62.Style.Font.Underline = false;
            this.Text62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text62.Style.Visible = true;
            this.Text62.Value = "Sealed Totals:";
            // 
            // SumofOpenedRAQuantity1
            // 
            this.SumofOpenedRAQuantity1.CanGrow = false;
            this.SumofOpenedRAQuantity1.CanShrink = false;
            this.SumofOpenedRAQuantity1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.043D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.SumofOpenedRAQuantity1.Name = "SumofOpenedRAQuantity1";
            this.SumofOpenedRAQuantity1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofOpenedRAQuantity1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofOpenedRAQuantity1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofOpenedRAQuantity1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofOpenedRAQuantity1.Style.Color = System.Drawing.Color.Black;
            this.SumofOpenedRAQuantity1.Style.Font.Bold = false;
            this.SumofOpenedRAQuantity1.Style.Font.Italic = false;
            this.SumofOpenedRAQuantity1.Style.Font.Name = "Arial";
            this.SumofOpenedRAQuantity1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofOpenedRAQuantity1.Style.Font.Strikeout = false;
            this.SumofOpenedRAQuantity1.Style.Font.Underline = false;
            this.SumofOpenedRAQuantity1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.SumofOpenedRAQuantity1.Style.Visible = true;
            this.SumofOpenedRAQuantity1.Value = "= Sum(Fields.OpenedRAQuantity)";
            // 
            // SumofSealedRAQuantity1
            // 
            this.SumofSealedRAQuantity1.CanGrow = false;
            this.SumofSealedRAQuantity1.CanShrink = false;
            this.SumofSealedRAQuantity1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.043D), Telerik.Reporting.Drawing.Unit.Cm(0.221D));
            this.SumofSealedRAQuantity1.Name = "SumofSealedRAQuantity1";
            this.SumofSealedRAQuantity1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.914D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofSealedRAQuantity1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofSealedRAQuantity1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofSealedRAQuantity1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofSealedRAQuantity1.Style.Color = System.Drawing.Color.Black;
            this.SumofSealedRAQuantity1.Style.Font.Bold = false;
            this.SumofSealedRAQuantity1.Style.Font.Italic = false;
            this.SumofSealedRAQuantity1.Style.Font.Name = "Arial";
            this.SumofSealedRAQuantity1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.SumofSealedRAQuantity1.Style.Font.Strikeout = false;
            this.SumofSealedRAQuantity1.Style.Font.Underline = false;
            this.SumofSealedRAQuantity1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.SumofSealedRAQuantity1.Style.Visible = true;
            this.SumofSealedRAQuantity1.Value = "= Sum(Fields.SealedRAQuantity)";
            // 
            // Text9
            // 
            this.Text9.CanGrow = false;
            this.Text9.CanShrink = false;
            this.Text9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.778D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.Text9.Name = "Text9";
            this.Text9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text9.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text9.Style.Color = System.Drawing.Color.Black;
            this.Text9.Style.Font.Bold = false;
            this.Text9.Style.Font.Italic = false;
            this.Text9.Style.Font.Name = "Arial";
            this.Text9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text9.Style.Font.Strikeout = false;
            this.Text9.Style.Font.Underline = false;
            this.Text9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text9.Style.Visible = true;
            this.Text9.Value = "Case Totals:";
            // 
            // Text8
            // 
            this.Text8.CanGrow = false;
            this.Text8.CanShrink = false;
            this.Text8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.778D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.Text8.Name = "Text8";
            this.Text8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text8.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text8.Style.Color = System.Drawing.Color.Black;
            this.Text8.Style.Font.Bold = false;
            this.Text8.Style.Font.Italic = false;
            this.Text8.Style.Font.Name = "Arial";
            this.Text8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text8.Style.Font.Strikeout = false;
            this.Text8.Style.Font.Underline = false;
            this.Text8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text8.Style.Visible = true;
            this.Text8.Value = "Opened Totals:";
            // 
            // Text7
            // 
            this.Text7.CanGrow = false;
            this.Text7.CanShrink = false;
            this.Text7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.838D), Telerik.Reporting.Drawing.Unit.Cm(0.228D));
            this.Text7.Name = "Text7";
            this.Text7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.115D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Text7.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text7.Style.Color = System.Drawing.Color.Black;
            this.Text7.Style.Font.Bold = false;
            this.Text7.Style.Font.Italic = false;
            this.Text7.Style.Font.Name = "Arial";
            this.Text7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.Text7.Style.Font.Strikeout = false;
            this.Text7.Style.Font.Underline = false;
            this.Text7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text7.Style.Visible = true;
            this.Text7.Value = "Sealed Totals:";
            // 
            // SumofReturnableItemValueTotal1
            // 
            this.SumofReturnableItemValueTotal1.CanGrow = false;
            this.SumofReturnableItemValueTotal1.CanShrink = false;
            this.SumofReturnableItemValueTotal1.Format = "{0:C2}";
            this.SumofReturnableItemValueTotal1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.202D), Telerik.Reporting.Drawing.Unit.Cm(1.72D));
            this.SumofReturnableItemValueTotal1.Name = "SumofReturnableItemValueTotal1";
            this.SumofReturnableItemValueTotal1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.SumofReturnableItemValueTotal1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SumofReturnableItemValueTotal1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SumofReturnableItemValueTotal1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SumofReturnableItemValueTotal1.Style.Color = System.Drawing.Color.Black;
            this.SumofReturnableItemValueTotal1.Style.Font.Bold = true;
            this.SumofReturnableItemValueTotal1.Style.Font.Italic = false;
            this.SumofReturnableItemValueTotal1.Style.Font.Name = "Arial";
            this.SumofReturnableItemValueTotal1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.SumofReturnableItemValueTotal1.Style.Font.Strikeout = false;
            this.SumofReturnableItemValueTotal1.Style.Font.Underline = false;
            this.SumofReturnableItemValueTotal1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.SumofReturnableItemValueTotal1.Style.Visible = true;
            this.SumofReturnableItemValueTotal1.Value = "=Sum(Fields.ReturnableItemValue)";
            // 
            // textBox21
            // 
            this.textBox21.Format = "{0:C2}";
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(1.72D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.621D), Telerik.Reporting.Drawing.Unit.Cm(0.416D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox21.Value = "=Sum(Fields.DebitMemoReturnValue)";
            // 
            // GroupHeaderArea2
            // 
            this.GroupHeaderArea2.Height = Telerik.Reporting.Drawing.Unit.Cm(0.7D);
            this.GroupHeaderArea2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.LongDescription1});
            this.GroupHeaderArea2.KeepTogether = false;
            this.GroupHeaderArea2.Name = "GroupHeaderArea2";
            this.GroupHeaderArea2.PageBreak = Telerik.Reporting.PageBreak.None;
            this.GroupHeaderArea2.PrintOnEveryPage = true;
            this.GroupHeaderArea2.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupHeaderArea2.Style.Visible = true;
            // 
            // LongDescription1
            // 
            this.LongDescription1.CanGrow = false;
            this.LongDescription1.CanShrink = false;
            this.LongDescription1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.1D));
            this.LongDescription1.Name = "LongDescription1";
            this.LongDescription1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.141D), Telerik.Reporting.Drawing.Unit.Cm(0.508D));
            this.LongDescription1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.LongDescription1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.LongDescription1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.LongDescription1.Style.Color = System.Drawing.Color.Black;
            this.LongDescription1.Style.Font.Bold = true;
            this.LongDescription1.Style.Font.Italic = false;
            this.LongDescription1.Style.Font.Name = "Arial";
            this.LongDescription1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.LongDescription1.Style.Font.Strikeout = false;
            this.LongDescription1.Style.Font.Underline = false;
            this.LongDescription1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.LongDescription1.Style.Visible = true;
            this.LongDescription1.Value = "=Fields.[LongDescription]";
            // 
            // ReportFooterArea1
            // 
            this.ReportFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.ReportFooterArea1.KeepTogether = false;
            this.ReportFooterArea1.Name = "ReportFooterArea1";
            this.ReportFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.ReportFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportFooterArea1.Style.Visible = true;
            // 
            // PageFooterArea1
            // 
            this.PageFooterArea1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Count(1)>0"));
            this.PageFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.374D);
            this.PageFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text2,
            this.Text3,
            this.Text4,
            this.Text5,
            this.Field1,
            this.Field2,
            this.Line2});
            this.PageFooterArea1.Name = "PageFooterArea1";
            this.PageFooterArea1.PrintOnFirstPage = true;
            this.PageFooterArea1.PrintOnLastPage = true;
            this.PageFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageFooterArea1.Style.Visible = true;
            // 
            // Text2
            // 
            this.Text2.CanGrow = false;
            this.Text2.CanShrink = false;
            this.Text2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(0.247D));
            this.Text2.Name = "Text2";
            this.Text2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.503D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text2.Style.Color = System.Drawing.Color.Black;
            this.Text2.Style.Font.Bold = true;
            this.Text2.Style.Font.Italic = false;
            this.Text2.Style.Font.Name = "Arial";
            this.Text2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text2.Style.Font.Strikeout = false;
            this.Text2.Style.Font.Underline = false;
            this.Text2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text2.Style.Visible = true;
            this.Text2.Value = "1410 Harris Road";
            // 
            // Text3
            // 
            this.Text3.CanGrow = false;
            this.Text3.CanShrink = false;
            this.Text3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.437D), Telerik.Reporting.Drawing.Unit.Cm(0.67D));
            this.Text3.Name = "Text3";
            this.Text3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.969D), Telerik.Reporting.Drawing.Unit.Cm(0.377D));
            this.Text3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text3.Style.Color = System.Drawing.Color.Black;
            this.Text3.Style.Font.Bold = true;
            this.Text3.Style.Font.Italic = false;
            this.Text3.Style.Font.Name = "Arial";
            this.Text3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text3.Style.Font.Strikeout = false;
            this.Text3.Style.Font.Underline = false;
            this.Text3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text3.Style.Visible = true;
            this.Text3.Value = "Libertyville, IL  60048";
            // 
            // Text4
            // 
            this.Text4.CanGrow = false;
            this.Text4.CanShrink = false;
            this.Text4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.664D), Telerik.Reporting.Drawing.Unit.Cm(0.247D));
            this.Text4.Name = "Text4";
            this.Text4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.188D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text4.Style.Color = System.Drawing.Color.Black;
            this.Text4.Style.Font.Bold = true;
            this.Text4.Style.Font.Italic = false;
            this.Text4.Style.Font.Name = "Arial";
            this.Text4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text4.Style.Font.Strikeout = false;
            this.Text4.Style.Font.Underline = false;
            this.Text4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text4.Style.Visible = true;
            this.Text4.Value = "Phone: 800-505-9291";
            // 
            // Text5
            // 
            this.Text5.CanGrow = false;
            this.Text5.CanShrink = false;
            this.Text5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.09D), Telerik.Reporting.Drawing.Unit.Cm(0.67D));
            this.Text5.Name = "Text5";
            this.Text5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.762D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text5.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text5.Style.Color = System.Drawing.Color.Black;
            this.Text5.Style.Font.Bold = true;
            this.Text5.Style.Font.Italic = false;
            this.Text5.Style.Font.Name = "Arial";
            this.Text5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Text5.Style.Font.Strikeout = false;
            this.Text5.Style.Font.Underline = false;
            this.Text5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text5.Style.Visible = true;
            this.Text5.Value = "Fax Number:  847-775-7258";
            // 
            // Field1
            // 
            this.Field1.CanGrow = false;
            this.Field1.CanShrink = false;
            this.Field1.Format = "{0:d}";
            this.Field1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.876D), Telerik.Reporting.Drawing.Unit.Cm(0.247D));
            this.Field1.Name = "Field1";
            this.Field1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.298D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Field1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Field1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Field1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Field1.Style.Color = System.Drawing.Color.Black;
            this.Field1.Style.Font.Bold = true;
            this.Field1.Style.Font.Italic = false;
            this.Field1.Style.Font.Name = "Arial";
            this.Field1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Field1.Style.Font.Strikeout = false;
            this.Field1.Style.Font.Underline = false;
            this.Field1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Field1.Style.Visible = true;
            this.Field1.Value = "=Now()";
            // 
            // Field2
            // 
            this.Field2.CanGrow = false;
            this.Field2.CanShrink = false;
            this.Field2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.894D), Telerik.Reporting.Drawing.Unit.Cm(0.727D));
            this.Field2.Name = "Field2";
            this.Field2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.858D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Field2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Field2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Field2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Field2.Style.Color = System.Drawing.Color.Black;
            this.Field2.Style.Font.Bold = true;
            this.Field2.Style.Font.Italic = false;
            this.Field2.Style.Font.Name = "Arial";
            this.Field2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Field2.Style.Font.Strikeout = false;
            this.Field2.Style.Font.Underline = false;
            this.Field2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Field2.Style.Visible = true;
            this.Field2.Value = "=\"Page \" + PageNumber + \" of \" + PageCount";
            // 
            // Line2
            // 
            this.Line2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.833D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Line2.Name = "Line2";
            this.Line2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(26.003D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line2.Style.Color = System.Drawing.Color.Black;
            this.Line2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.Line2.Style.Visible = true;
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Count(1)=0"));
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.6D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Visible = false;
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.071D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.781D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Value = "No Data Found";
            // 
            // ccDataSource
            // 
            this.ccDataSource.CalculatedFields.AddRange(new Telerik.Reporting.CalculatedField[] {
            new Telerik.Reporting.CalculatedField("ReportPeriod", typeof(string), "=\'Report Period From \' + Fields.StartDate + \' Through \' + Fields.EndDate "),
            new Telerik.Reporting.CalculatedField("CaseAmount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Case\', Fields.ReturnableItemValueTotal, 0)"),
            new Telerik.Reporting.CalculatedField("CaseDebitMemoValue", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Case\', Fields.DebitMemoRet" +
                    "urnValue, 0)"),
            new Telerik.Reporting.CalculatedField("CaseItemCount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Case\', Fields.ItemCount, 0)"),
            new Telerik.Reporting.CalculatedField("CaseRAQuantity", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Case\', Fields.ReturnAuthor" +
                    "izationsItemOriginalQuantity, 0)"),
            new Telerik.Reporting.CalculatedField("ManufacturerOverRideYesNo", typeof(bool), "=iif (Fields.ManufacturerPriceOverride = true, \'X\', \'\')"),
            new Telerik.Reporting.CalculatedField("OpenedAmount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Opened\', Fields.ReturnableItemValueTotal, 0)"),
            new Telerik.Reporting.CalculatedField("OpenedDebitMemoValue", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Opened\', Fields.DebitMemoR" +
                    "eturnValue, 0)"),
            new Telerik.Reporting.CalculatedField("OpenedItemCount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Opened\', Fields.ItemCount, 0)"),
            new Telerik.Reporting.CalculatedField("OpenedItemQtyCount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Opened\', Fields.ItemQuantity, 0)"),
            new Telerik.Reporting.CalculatedField("OpenedRAQuantity", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Opened\', Fields.ReturnAuth" +
                    "orizationsItemOriginalQuantity, 0)"),
            new Telerik.Reporting.CalculatedField("SealedAmount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Case\', Fields.ReturnableItemValueTotal, 0)"),
            new Telerik.Reporting.CalculatedField("SealedDebitMemoValue", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Sealed\', Fields.DebitMemoR" +
                    "eturnValue, 0)"),
            new Telerik.Reporting.CalculatedField("SealedItemCount", typeof(int), "=iif (Fields.ItemSealedOpenCase = \'Case\', Fields.ItemCount, 0)"),
            new Telerik.Reporting.CalculatedField("SealedRAQuantity", typeof(int), "=iif (Fields.ReturnAuthorizationsItemSealedOpenCase = \'Sealed\', Fields.ReturnAuth" +
                    "orizationsItemOriginalQuantity, 0)"),
            new Telerik.Reporting.CalculatedField("StateLawOverrideYesNo", typeof(bool), "=iif (Fields.PartialsReturnableLawOverride = 1, \'X\', \'\')")});
            this.ccDataSource.DataMember = "GetCreditComparisonResultsForReport";
            this.ccDataSource.DataSource = typeof(Qualanex.QoskCloud.Web.Areas.QoskReporting.Services.CreditComparisonService);
            this.ccDataSource.Name = "ccDataSource";
            this.ccDataSource.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("manufacturerProfileCode", typeof(System.Nullable<long>), "= Parameters.ManufacturerProfileCode"),
            new Telerik.Reporting.ObjectDataSourceParameter("startDate", typeof(string), "= Parameters.StartDate"),
            new Telerik.Reporting.ObjectDataSourceParameter("endDate", typeof(string), "= Parameters.EndDate"),
            new Telerik.Reporting.ObjectDataSourceParameter("excludeCreditAlreadyIssued", typeof(System.Nullable<bool>), "= Parameters.ExcludeCreditAlreadyIssued"),
            new Telerik.Reporting.ObjectDataSourceParameter("returnAuthorizationNumber", typeof(string), "= Parameters.ReturnAuthorizationNumber"),
            new Telerik.Reporting.ObjectDataSourceParameter("lowestPrice", typeof(System.Nullable<bool>), "= Parameters.LowestPrice"),
            new Telerik.Reporting.ObjectDataSourceParameter("showPrimaryReturnCode", typeof(System.Nullable<bool>), "= Parameters.ShowPrimaryReturnCode"),
            new Telerik.Reporting.ObjectDataSourceParameter("debitMemoNumber", typeof(string), "= Parameters.DebitMemoNumber"),
            new Telerik.Reporting.ObjectDataSourceParameter("closedOnly", typeof(System.Nullable<bool>), "= Parameters.ClosedOnly"),
            new Telerik.Reporting.ObjectDataSourceParameter("showAdditionalRAsIssued", typeof(System.Nullable<bool>), "= Parameters.ShowAdditionalRAsIssued"),
            new Telerik.Reporting.ObjectDataSourceParameter("displayImages", typeof(System.Nullable<bool>), "= Parameters.DisplayImages"),
            new Telerik.Reporting.ObjectDataSourceParameter("imagesPermitted", typeof(System.Nullable<bool>), "= Parameters.ImagesPermitted")});
            // 
            // CreditComparison
            // 
            this.DataSource = this.ccDataSource;
            group1.GroupFooter = this.GroupFooterArea1;
            group1.GroupHeader = this.GroupHeaderArea1;
            group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.[ReturnAuthorizationNumber]"));
            group1.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.[ReturnAuthorizationNumber]", Telerik.Reporting.SortDirection.Asc));
            group2.GroupFooter = this.groupFooterSection;
            group2.GroupHeader = this.groupHeaderSection;
            group2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ReturnAuthorizationNumber"));
            group2.Name = "Middle";
            group2.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.ReturnAuthorizationNumber", Telerik.Reporting.SortDirection.Asc));
            group3.GroupFooter = this.GroupFooterArea2;
            group3.GroupHeader = this.GroupHeaderArea2;
            group3.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.[ReturnAuthorizationsItemNDCUPC]"));
            group3.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.[ReturnAuthorizationsItemNDCUPC]", Telerik.Reporting.SortDirection.Asc));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2,
            group3});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.GroupHeaderArea1,
            this.GroupFooterArea1,
            this.groupHeaderSection,
            this.groupFooterSection,
            this.GroupHeaderArea2,
            this.GroupFooterArea2,
            DetailArea1,
            this.ReportFooterArea1,
            this.PageFooterArea1,
            this.pageHeaderSection1});
            this.Name = "CreditComparisonEstimatedVsProcessedReportWithReasons";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.305D), Telerik.Reporting.Drawing.Unit.Cm(0.305D), Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowNull = true;
            reportParameter1.Name = "ManufacturerProfileCode";
            reportParameter1.Text = "Manufacturer Profile Code";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter1.Value = "67877";
            reportParameter1.Visible = true;
            reportParameter2.AllowNull = true;
            reportParameter2.Name = "StartDate";
            reportParameter2.Text = "Start Date";
            reportParameter2.Visible = true;
            reportParameter3.AllowNull = true;
            reportParameter3.Name = "EndDate";
            reportParameter3.Text = "End Date";
            reportParameter3.Visible = true;
            reportParameter4.Name = "ExcludeCreditAlreadyIssued";
            reportParameter4.Text = "Exclude Credit Already Issued";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter4.Value = "false";
            reportParameter4.Visible = true;
            reportParameter5.AllowNull = true;
            reportParameter5.Name = "ReturnAuthorizationNumber";
            reportParameter5.Text = "Return Authorization Number";
            reportParameter5.Value = "749273";
            reportParameter5.Visible = true;
            reportParameter6.Name = "LowestPrice";
            reportParameter6.Text = "Lowest Price";
            reportParameter6.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter6.Value = "true";
            reportParameter6.Visible = true;
            reportParameter7.Name = "ShowPrimaryReturnCode";
            reportParameter7.Text = "Show Primary Return Code";
            reportParameter7.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter7.Value = "true";
            reportParameter7.Visible = true;
            reportParameter8.AllowNull = true;
            reportParameter8.Name = "DebitMemoNumber";
            reportParameter8.Text = "Debit Memo Number";
            reportParameter8.Value = "";
            reportParameter8.Visible = true;
            reportParameter9.Name = "ClosedOnly";
            reportParameter9.Text = "Closed Only";
            reportParameter9.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter9.Value = "true";
            reportParameter9.Visible = true;
            reportParameter10.Name = "ImagesPermitted";
            reportParameter10.Text = "Images Permitted";
            reportParameter10.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter10.Value = "True";
            reportParameter11.Name = "ShowAdditionalRAsIssued";
            reportParameter11.Text = "Show Additional RAs Issued";
            reportParameter11.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter11.Value = "false";
            reportParameter11.Visible = true;
            reportParameter12.Name = "DisplayImages";
            reportParameter12.Text = "Display Images";
            reportParameter12.Type = Telerik.Reporting.ReportParameterType.Boolean;
            reportParameter12.Value = "True";
            reportParameter12.Visible = true;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Inch;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(10.787D);
            ((System.ComponentModel.ISupportInitialize)(this.subReportImages1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subReportLogo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.ReportFooterSection ReportFooterArea1;
        private Telerik.Reporting.PageFooterSection PageFooterArea1;
        private Telerik.Reporting.TextBox Text2;
        private Telerik.Reporting.TextBox Text3;
        private Telerik.Reporting.TextBox Text4;
        private Telerik.Reporting.TextBox Text5;
        private Telerik.Reporting.TextBox Field1;
        private Telerik.Reporting.TextBox Field2;
        private Telerik.Reporting.Shape Line2;
        private Telerik.Reporting.GroupHeaderSection GroupHeaderArea1;
        private Telerik.Reporting.GroupFooterSection GroupFooterArea1;
        private Telerik.Reporting.TextBox SumofDebitMemoReturnValue1;
        private Telerik.Reporting.Shape Line8;
        private Telerik.Reporting.TextBox SumofReturnableItemValueTotal2;
        private Telerik.Reporting.GroupHeaderSection GroupHeaderArea2;
        private Telerik.Reporting.GroupFooterSection GroupFooterArea2;
        private Telerik.Reporting.Shape Line9;
        private Telerik.Reporting.TextBox LongDescription1;
        private Telerik.Reporting.TextBox SumofReturnableItemValueTotal1;
        private Telerik.Reporting.TextBox Text7;
        private Telerik.Reporting.TextBox Text8;
        private Telerik.Reporting.TextBox Text9;
        private Telerik.Reporting.TextBox SumofSealedRAQuantity1;
        private Telerik.Reporting.TextBox SumofOpenedRAQuantity1;
        private Telerik.Reporting.TextBox SumofCaseRAQuantity1;
        private Telerik.Reporting.TextBox SumofSealedDebitMemoValue1;
        private Telerik.Reporting.TextBox SumofOpenedDebitMemoValue1;
        private Telerik.Reporting.TextBox SumofCaseDebitMemoValue1;
        private Telerik.Reporting.TextBox SumofSealedItemCount1;
        private Telerik.Reporting.TextBox SumofCaseItemCount1;
        private Telerik.Reporting.TextBox SumofSealedAmount1;
        private Telerik.Reporting.TextBox SumofOpenedAmount1;
        private Telerik.Reporting.TextBox SumofCaseAmount1;
        private Telerik.Reporting.TextBox SumofOpenedItemQtyCount1;
        private Telerik.Reporting.TextBox Text59;
        private Telerik.Reporting.TextBox Text60;
        private Telerik.Reporting.TextBox Text62;
        private Telerik.Reporting.Panel DetailSection2;
        private Telerik.Reporting.TextBox ReturnAuthorizationsItemLotNumber1;
        private Telerik.Reporting.TextBox ReturnAuthorizationsItemOriginalQuantity1;
        private Telerik.Reporting.TextBox ReturnAuthorizationsItemSealedOpenCase1;
        private Telerik.Reporting.TextBox DebitMemoReturnValue1;
        private Telerik.Reporting.TextBox ItemLotNumber1;
        private Telerik.Reporting.TextBox ItemQuantity1;
        private Telerik.Reporting.TextBox ItemSealedOpenCase1;
        private Telerik.Reporting.TextBox ReturnAuthorizationsItemExpirationMonthYear1;
        private Telerik.Reporting.TextBox ItemExpirationMonthYear1;
        private Telerik.Reporting.TextBox ManufacturerOverrideYesNo1;
        private Telerik.Reporting.TextBox StateLawOverrideYesNo1;
        private Telerik.Reporting.TextBox ReturnableItemValue1;
        private Telerik.Reporting.TextBox ItemCount1;
        private Telerik.Reporting.TextBox ReturnableItemValueTotal1;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.TextBox NR1;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.TextBox ManufacturerPriceOverrideNotes1;
        private Telerik.Reporting.TextBox Text44;
        private Telerik.Reporting.Panel panel4;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.Shape Box2;
        private Telerik.Reporting.TextBox Text24;
        private Telerik.Reporting.TextBox Text23;
        private Telerik.Reporting.TextBox Text14;
        private Telerik.Reporting.TextBox Text11;
        private Telerik.Reporting.TextBox Text10;
        private Telerik.Reporting.TextBox Text32;
        private Telerik.Reporting.TextBox Text16;
        private Telerik.Reporting.TextBox Text18;
        private Telerik.Reporting.TextBox Text17;
        private Telerik.Reporting.TextBox Text25;
        private Telerik.Reporting.TextBox Text22;
        private Telerik.Reporting.TextBox Text15;
        private Telerik.Reporting.TextBox Text20;
        private Telerik.Reporting.TextBox Text19;
        private Telerik.Reporting.Shape Line3;
        private Telerik.Reporting.Shape Line6;
        private Telerik.Reporting.TextBox Name1;
        private Telerik.Reporting.TextBox PhoneNumber1;
        private Telerik.Reporting.TextBox EmailAddress1;
        private Telerik.Reporting.TextBox DEANumber1;
        private Telerik.Reporting.TextBox DebitMemoNumber1;
        private Telerik.Reporting.TextBox PrintAccountTitle1;
        private Telerik.Reporting.TextBox PrintAccount1;
        private Telerik.Reporting.TextBox ThirdPartyReverseDistributorName1;
        private Telerik.Reporting.TextBox ThirdPartyReverseDistributorDEANumber1;
        private Telerik.Reporting.TextBox RequestorName1;
        private Telerik.Reporting.TextBox RequestorPhoneNumber1;
        private Telerik.Reporting.TextBox RequestorEmailAddress1;
        private Telerik.Reporting.TextBox ReturnAuthorizationNumber1;
        private Telerik.Reporting.TextBox RequestDate1;
        private Telerik.Reporting.TextBox ReceivedDate1;
        private Telerik.Reporting.TextBox Text6;
        private Telerik.Reporting.TextBox CreditIssuedDate1;
        private Telerik.Reporting.TextBox Text21;
        private Telerik.Reporting.TextBox Text58;
        private Telerik.Reporting.TextBox Text63;
        private Telerik.Reporting.TextBox ReturnAuthorizationNumberSplit1;
        private Telerik.Reporting.TextBox Text64;
        private Telerik.Reporting.TextBox PrintName1;
        private Telerik.Reporting.TextBox PrimaryCreditMemoNumber1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection;
        private Telerik.Reporting.Shape shape4;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.GroupFooterSection groupFooterSection;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox NonReturnableReasonDetail1;
        private Telerik.Reporting.TextBox Text45;
        private SubReportImages subReportImages1;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.Panel panel7;
        private Telerik.Reporting.SubReport subReport2;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.ObjectDataSource ccDataSource;
        private Telerik.Reporting.SubReport subReportLogo;
        private SubReportLogo subReportLogo1;
    }
}