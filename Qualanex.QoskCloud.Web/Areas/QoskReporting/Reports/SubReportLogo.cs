namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for SubReportLogo.
    /// </summary>
    public partial class SubReportLogo : Telerik.Reporting.Report
    {
        public SubReportLogo()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}