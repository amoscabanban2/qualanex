namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for SubReportImages.
    /// </summary>
    public partial class SubReportImages : Telerik.Reporting.Report
    {
        public SubReportImages()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public static Image CreateImageFromUri(string uri, bool displayImages, bool imagesPermitted)
        {
            if (!String.IsNullOrEmpty(uri) && displayImages && imagesPermitted)
            {
                using (WebClient wc = new WebClient())
                {
                    using (Stream s = wc.OpenRead(uri))
                    {
                        return Image.FromStream(s);
                    }
                }
            }
            return new Bitmap(1,1);
        } 
        
        public static string GetLastofGuid(string myString)
        {
            if (myString.Length > 10)
                return myString.Substring(myString.Length - 10);
            else
                return myString;
        }
    }
}