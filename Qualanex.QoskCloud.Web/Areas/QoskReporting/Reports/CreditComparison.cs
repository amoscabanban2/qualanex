namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports
{
    using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for Report2.
    /// </summary>
    public partial class CreditComparison : Telerik.Reporting.Report
    {
        public CreditComparison()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void subReport2_ItemDataBound(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.SubReport txt = (Telerik.Reporting.Processing.SubReport)sender;
            Telerik.Reporting.Processing.IDataObject dataObject = (Telerik.Reporting.Processing.IDataObject)txt.DataObject;
            if (dataObject["ItemIDs"] != null) 
            {
                var itemID = dataObject["ItemIDs"];
                if(itemID.ToString().Contains("-"))
                {

                }
                else
                {
                    Telerik.Reporting.Processing.ReportItemBase item = (Telerik.Reporting.Processing.ReportItemBase)sender;
                    item.Parent.Visible = false;
                }
            }
            else
            {
                Telerik.Reporting.Processing.ReportItemBase item = (Telerik.Reporting.Processing.ReportItemBase)sender;
                item.Parent.Visible = false; ;
            }
        }
    }
}