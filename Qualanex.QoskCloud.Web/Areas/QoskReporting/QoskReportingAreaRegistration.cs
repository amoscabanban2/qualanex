﻿using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting
{
    public class QoskReportingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "QoskReporting";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "QoskReporting_default",
                "QoskReporting/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

