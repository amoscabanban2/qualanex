﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public partial class CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result
    {
        private List<LinkInfo> _imageLinksList = new List<LinkInfo>();
        public List<LinkInfo> ImageLinksList
        {
            get { return _imageLinksList; }
            set
            {
                _imageLinksList = value;
            }
        }
        public string ImageLinks { get; set; }
    }

    public class LinkInfo
    {
        public string URL { get; set; }
        public string Text { get; set; }
    }

}