//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    using System;
    
    public partial class CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result
    {
        public int ID { get; set; }
        public Nullable<long> ReturnAuthorizationNumber { get; set; }
        public string Name { get; set; }
        public string RAAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string DEANumber { get; set; }
        public string DebitMemoNumber { get; set; }
        public string PrintAccountTitle { get; set; }
        public string PrintAccount { get; set; }
        public string PrintName { get; set; }
        public string ThirdPartyReverseDistributorName { get; set; }
        public string ThirdPartyReverseDistributorAddress { get; set; }
        public string ThirdPartyReverseDistributorDEANumber { get; set; }
        public string RequestorName { get; set; }
        public string RequestorPhoneNumber { get; set; }
        public string RequestorEmailAddress { get; set; }
        public string RequestorFaxNumber { get; set; }
        public Nullable<System.DateTime> RequestDate { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public Nullable<System.DateTime> CreditIssuedDate { get; set; }
        public Nullable<long> ReturnAuthorizationsItemsID { get; set; }
        public Nullable<long> ReturnAuthorizationsItemProductID { get; set; }
        public string ReturnAuthorizationsItemDescription { get; set; }
        public Nullable<long> ReturnAuthorizationsItemNDCUPC { get; set; }
        public string ReturnAuthorizationsItemLotNumber { get; set; }
        public Nullable<byte> ReturnAuthorizationsItemExpirationMonth { get; set; }
        public Nullable<int> ReturnAuthorizationsItemExpirationYear { get; set; }
        public string ReturnAuthorizationsItemSealedOpenCase { get; set; }
        public Nullable<decimal> ReturnAuthorizationsItemOriginalQuantity { get; set; }
        public Nullable<int> ReturnAuthorizationsItemsMatchAccuracy { get; set; }
        public Nullable<decimal> ReturnAuthorizationsItemReturnValue { get; set; }
        public Nullable<bool> ManufacturerPriceOverride { get; set; }
        public Nullable<decimal> ManufacturerOverrideReturnValue { get; set; }
        public Nullable<decimal> DebitMemoReturnValue { get; set; }
        public Nullable<decimal> DebitMemoUnitReturnValue { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<long> ItemNDCUPC { get; set; }
        public Nullable<long> ItemProductID { get; set; }
        public string ItemLotNumber { get; set; }
        public Nullable<int> ItemExpirationMonth { get; set; }
        public Nullable<int> ItemExpirationYear { get; set; }
        public string ItemSealedOpenCase { get; set; }
        public Nullable<decimal> ItemQuantity { get; set; }
        public Nullable<bool> Returnable { get; set; }
        public Nullable<decimal> ReturnableItemValue { get; set; }
        public Nullable<decimal> ItemUnitPrice { get; set; }
        public Nullable<int> ItemCount { get; set; }
        public string ManufacturerPriceOverrideNotes { get; set; }
        public Nullable<int> PrimaryWholesalerProfileCode { get; set; }
        public string SecondaryCreditMemoNumber { get; set; }
        public Nullable<int> PartialsReturnableLawOverride { get; set; }
        public string NonReturnableReasonDetail { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string LongDescription { get; set; }
        public string ReturnAuthorizationsItemExpirationMonthYear { get; set; }
        public string ItemExpirationMonthYear { get; set; }
        public Nullable<decimal> ItemCountTotal { get; set; }
        public Nullable<decimal> ReturnableItemValueTotal { get; set; }
        public string ProductStrength { get; set; }
        public Nullable<decimal> ProductPackageSize { get; set; }
        public string ProductUnitOfMeasure { get; set; }
        public string RAStatus { get; set; }
        public Nullable<decimal> ManufacturerOriginalUnitPrice { get; set; }
        public Nullable<int> CaseSize { get; set; }
        public string ReturnAuthorizationNumberSplit { get; set; }
        public string PrimaryCreditMemoNumber { get; set; }
        public string NR { get; set; }
        public string ItemIDs { get; set; }
        public Nullable<long> ManufacturerProfileCode { get; set; }
        public string ManufacturerProfileName { get; set; }
    }
}
