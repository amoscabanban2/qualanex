﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class AzureThumbInfo
    {
        public int ImagesCreated { get; set; }
        public int ContainersProcessed { get; set; }
        public int ContainerSetsFetched { get; set; }
        public int ContainersAlreadyExisted { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string RunTime { get; set; }
        public bool RunDaily { get; set; }
        public bool IsComplete { get; set; }
        public string Status { get; set; }
        public string PrefixHex { get; set; }
        public string NextPrefixHex { get; set; }
        public int MaxParallelThreads { get; set; }
        public int MaxContainers { get; set; }
        public int ContainersPerFetch { get; set; }
        public bool RunWithNoPrefix { get; set; }
        public bool RunForExistingContainers { get; set; }
        public bool Stop { get; set; }
    }

    public class AzureThumbResult
    {
        public int ImagesCreated { get; set; }
        public int ContainersProcessed { get; set; }
        public int ContainerSetsFetched { get; set; }
        public int ContainersAlreadyExisted { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string RunTime { get; set; }
        public bool IsComplete { get; set; }
        public string Status { get; set; }
        public string PrefixHex { get; set; }
        public string NextPrefixHex { get; set; }
    }
}