﻿namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{

    public class Image
    {
        int returnAuthorizationNumber;
        int groupId;
        string name;
        string imageUrl;
        
        public Image(int returnAuthorizationNumber, int groupId, string name, string imageUrl)
        {
            this.returnAuthorizationNumber = returnAuthorizationNumber;
            this.groupId = groupId;
            this.name = name;
            this.imageUrl = imageUrl;
        }

        public int ReturnAuthorizationNumber
        {
            get { return this.returnAuthorizationNumber; }
            set { this.returnAuthorizationNumber = value; }

        }
        public int GroupId
        {
            get { return this.groupId; }
            set { this.groupId = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string ImageUrl
        {
            get { return this.imageUrl; }
            set { this.imageUrl = value; }
        }
    }
}