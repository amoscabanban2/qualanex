﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class CreditComparisonViewModel : ReportInfoViewModel
    {
        public CreditComparisonViewModel()
        {
            ScheduleParams = new CreditComparisonScheduleParams();
        }
        public CreditComparisonParams Params { get; set; }
        public CreditComparisonScheduleParams ScheduleParams { get; set; }
    }

    public class CreditComparisonParams
    {
        public long? ManufacturerProfileCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool ExcludeCreditAlreadyIssued { get; set; }
        public string ReturnAuthorizationNumber { get; set; }
        public bool LowestPrice { get; set; }
        public bool ShowPrimaryReturnCode { get; set; }
        public string DebitMemoNumber { get; set; }
        public bool ClosedOnly { get; set; }
        public bool ShowAdditionalRAsIssued { get; set; }
        public bool ImagesPermitted { get; set; }
        public bool DisplayImages { get; set; }
    }
}