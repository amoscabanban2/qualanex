﻿using Kendo.Mvc.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class CreditComparisonScheduleParams
    {
        public bool? SplitByRANumber { get; set; }
        public string DaysToInclude { get; set; }
    }
}