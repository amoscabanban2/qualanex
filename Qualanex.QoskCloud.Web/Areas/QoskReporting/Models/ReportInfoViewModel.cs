﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Reporting;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class ReportInfoViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string UniqueName { get; set; }
        public ReportSource ReportSource { get; set; }
        public string Category { get; set; }
        public UserInfo UserInfo { get; set; }
        public string SendToEmail { get; set; }
        public ReportSchedule ReportSchedule { get; set; }

        public bool IsEditScheduleMode()
        {
            return ReportSchedule != null;
        }
    }
}