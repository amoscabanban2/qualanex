﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;
using System;
using System.Collections.Async;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Logger = Qualanex.QoskCloud.Utility.Logger;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{    
    public class Images
    {
        public List<Image> ImagesList = new List<Image>();

        public List<ImageSasUriModel> GetImages(string ItemIDs, string ReturnAuthorizationNumber, bool ImagesPermitted, bool DisplayImages)
        {
            if (ImagesPermitted && !string.IsNullOrEmpty(ItemIDs) && ItemIDs.Length > 10)
            {
                try
                {
                    var sasConstraints = new SharedAccessBlobPolicy
                    {
                        //SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
                        SharedAccessExpiryTime = DateTime.UtcNow.AddDays(90),
                        Permissions = "Read".ToLower() == Constants.FileAccess_Methods_Write ? SharedAccessBlobPermissions.Read |
                        SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List :
                        SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
                    };

                    var splitItemIDs = new string[] { };
                    var splitReturnAuthorizationNumber = new string[] { };
                    if (ItemIDs.Contains(','))
                    {
                        splitItemIDs = ItemIDs.Split(',');
                    }
                    else
                    {
                        splitItemIDs = new string[] { ItemIDs };
                    }

                    var thumbsConnectString = ConfigurationManager.GetConnectionString("qoskimagesthumbnails");
                    var thumbsStorageAccount = CloudStorageAccount.Parse(thumbsConnectString);
                    var thumbsBlobClient = thumbsStorageAccount.CreateCloudBlobClient();

                    var imagesConnectString = ConfigurationManager.GetConnectionString("qoskimages");
                    var imagesStorageAccount = CloudStorageAccount.Parse(imagesConnectString);
                    var imagesBlobClient = imagesStorageAccount.CreateCloudBlobClient();

                    var listImages = new ConcurrentBag<ImageSasUriModel>();

                    var task = splitItemIDs.ParallelForEachAsync(async itemGuid =>
                    {
                        var container = itemGuid;
                        var imageInfos =  await ReportingBlobResource.GetBlobThumbnailsAndOriginalsSASUrlsAsync(imagesBlobClient, thumbsBlobClient, container, sasConstraints);

                        foreach (var image in imageInfos)
                        {
                            var name = image.ContentName.Substring(image.ContentName.LastIndexOf('_') + 1, image.ContentName.Length - 4 - image.ContentName.LastIndexOf('_') - 1);
                            name = string.Concat(name.Select(x => char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                            image.ContentType = "image/jpeg";
                            image.ContentName = name;
                            image.BlobGuid = container;
                            image.ReturnAuthorizationNumber = ReturnAuthorizationNumber;
                            image.ItemID = itemGuid;

                            listImages.Add(image);
                        }

                    }, maxDegreeOfParalellism: 10);

                    task.Wait();

                    var list = listImages.OrderBy(x => x.ItemID).ThenByDescending(x => x.ContentName).ToList();

                    return list;
                }
                catch
                {
                    // do nothing
                }
            }
            return new List<ImageSasUriModel>();
        }
    }
}