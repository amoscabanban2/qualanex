﻿using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public partial class ReportSchedule
    {
        public string ScheduleTimeString {
            get
            {
                return DateTime.Today.Add(ScheduleTime).ToString("hh:mm tt");
            }
            set
            {
                DateTime dt;
                if (DateTime.TryParseExact(value, "hh:mm tt", new CultureInfo("en-US"), DateTimeStyles.AllowWhiteSpaces, out dt))
                {
                    ScheduleTime = dt.TimeOfDay;
                }
            }
        }

        [CLSCompliant(false)]
        public IEnumerable<GridColumnSettings> ReportColumns { get; set; }
    }
}