﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class CreditComparisonEmailInfo
    {
        public Nullable<long> RANum { get; set; }
        public string Subject { get; set; }
    }
}