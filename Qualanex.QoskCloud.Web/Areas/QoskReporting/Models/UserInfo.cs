﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
	public class UserInfo
	{
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}