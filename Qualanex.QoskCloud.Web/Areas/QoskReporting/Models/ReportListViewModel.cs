﻿using Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Reporting;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public class ReportListViewModel
    {
        public List<ReportInfoViewModel> ReportInfoList { get; set; }
        public IEnumerable<IGrouping<string, ReportInfoViewModel>> Groups { get; set; }
    }
}