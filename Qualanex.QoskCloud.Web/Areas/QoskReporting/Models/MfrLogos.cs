﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Models
{
    public partial class QIPSReportingEntities : DbContext
    {
        public MfrLogo GetLogo(int manufacturerProfileCode)
        {
            return this.MfrLogoes.FirstOrDefault(x => x.ManufacturerProfileCode == manufacturerProfileCode);
        }
    }
}