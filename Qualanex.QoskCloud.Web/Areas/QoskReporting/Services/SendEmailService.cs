﻿using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Services
{
    public static class SendEmailService
    {
        public static void AddToAddresses(string addresses, SendGridMessage msg)
        {
            var recipients = addresses.Split(new char[] { ',', ';', ' '});
            foreach (var r in recipients)
            {
                // simple email validation
                if (r.Contains("@") && r.Length > 2)
                    msg.AddTo(r);
            }
        }

        public static UserInfo GetUserInfoByID(long userID)
        {
            var user = new UserInfo();
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                user = (from u in cloudModelEntities.User
                        where u.UserID == userID
                        select new UserInfo
                        {
                            Email = u.Email,
                            FullName = u.FirstName + " " + u.LastName
                        }).FirstOrDefault();
            }
            return user;
        }

        public static string GetManufacturerInfoByProfileCode(long? manufacturerProfileCode)
        {
            var manufacturerName = String.Empty;
            using (var cloudModelEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                manufacturerName = (from p in cloudModelEntities.Profile
                                        where p.ProfileCode == manufacturerProfileCode
                                        select p.Name).SingleOrDefault();
            }
            return manufacturerName;
        }

        public static void SendEmail(SendGridMessage msg)
        {
            var apiKey1 = QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("SENDGRID_APIKEY");
            var client1 = new SendGridClient(apiKey1);

            client1.SendEmailAsync(msg);
        }
    }
}