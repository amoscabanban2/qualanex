﻿using Newtonsoft.Json;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Telerik.Reporting.Processing;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Services
{
    public class CreditComparisonService
    {
        public const string DateFormat = "yyyy-MM-dd";
        public const int CacheTimeInMin = 15;

        public static class DaysToIncludeList
        {
            public const string OneDay = "Previous 1 Day";
            public const string SevenDays = "Previous 7 Days";
            public const string FourteenDays = "Previous 14 Days";
            public const string PrevMonth = "Previous Month";
        }

        private QIPSReportingEntities _db;
        public QIPSReportingEntities db
        {
            get
            {
                if (_db == null)
                    _db = new QIPSReportingEntities();

                return _db;
            }
            set { _db = value; }
        }

        public List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result> GetCreditComparisonResultsForReport(Nullable<long> manufacturerProfileCode, string startDate, string endDate, Nullable<bool> excludeCreditAlreadyIssued, string returnAuthorizationNumber, Nullable<bool> lowestPrice, Nullable<bool> showPrimaryReturnCode, string debitMemoNumber, Nullable<bool> closedOnly, Nullable<bool> showAdditionalRAsIssued, Nullable<bool> displayImages, Nullable<bool> imagesPermitted)
        {
            var ccParams = new CreditComparisonParams
            {
                ManufacturerProfileCode = manufacturerProfileCode,
                StartDate = string.IsNullOrEmpty(startDate) ? null : (DateTime?)DateTime.Parse(startDate),
                EndDate = string.IsNullOrEmpty(endDate) ? null : (DateTime?)DateTime.Parse(endDate),
                ExcludeCreditAlreadyIssued = excludeCreditAlreadyIssued == true,
                ReturnAuthorizationNumber = returnAuthorizationNumber,
                LowestPrice = lowestPrice == true,
                ShowPrimaryReturnCode = showPrimaryReturnCode == true,
                DebitMemoNumber = debitMemoNumber,
                ClosedOnly = closedOnly == true,
                ShowAdditionalRAsIssued = showAdditionalRAsIssued == true,
                DisplayImages = displayImages == true,
                ImagesPermitted = imagesPermitted == true
            };

            var result = GetCachedCreditComparisonResult(ccParams);

            return result;
        }

        private async Task<List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result>> QueryForCreditComparisonResultsAsync(
            long? manufacturerProfileCode, string startDate, string endDate, 
            bool? excludeCreditAlreadyIssued, string returnAuthorizationNumber, 
            bool? lowestPrice, bool? showPrimaryReturnCode, string debitMemoNumber, 
            bool? closedOnly, bool? showAdditionalRAsIssued)
        {           
            var ct = new CancellationToken();

            var longQueryTimeoutInSeconds = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("LongQueryTimeoutInSeconds"));
            db.Database.CommandTimeout = longQueryTimeoutInSeconds;

            var result = await db.CreditComparisonEstimatedVsProcessedReportWithReasonsQosk(
                manufacturerProfileCode, startDate, endDate,
                excludeCreditAlreadyIssued, returnAuthorizationNumber,
                lowestPrice, showPrimaryReturnCode, debitMemoNumber,
                closedOnly, showAdditionalRAsIssued).ToListAsync(ct);

            return result;
        }

        private List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result> QueryForCreditComparisonResults(
            long? manufacturerProfileCode, string startDate, string endDate,
            bool? excludeCreditAlreadyIssued, string returnAuthorizationNumber,
            bool? lowestPrice, bool? showPrimaryReturnCode, string debitMemoNumber,
            bool? closedOnly, bool? showAdditionalRAsIssued)
        {
            var longQueryTimeoutInSeconds = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("LongQueryTimeoutInSeconds"));
            db.Database.CommandTimeout = longQueryTimeoutInSeconds;

            var result = db.CreditComparisonEstimatedVsProcessedReportWithReasonsQosk(
                manufacturerProfileCode, startDate, endDate,
                excludeCreditAlreadyIssued, returnAuthorizationNumber,
                lowestPrice, showPrimaryReturnCode, debitMemoNumber,
                closedOnly, showAdditionalRAsIssued).ToList();

            return result;
        }

        public async Task<List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result>> GetCachedCreditComparisonResultAsync(CreditComparisonParams reportParams)
        {
            string key = GetKeyFromParams(reportParams);

            return await MemCacheManager.GetAsync(key, CacheTimeInMin, async () =>
            {
                return await QueryForCreditComparisonResultsAsync(reportParams.ManufacturerProfileCode, MemCacheManager.GetDateAsString(reportParams.StartDate), MemCacheManager.GetDateAsString(reportParams.EndDate), reportParams.ExcludeCreditAlreadyIssued, reportParams.ReturnAuthorizationNumber, reportParams.LowestPrice, reportParams.ShowPrimaryReturnCode, reportParams.DebitMemoNumber, reportParams.ClosedOnly, reportParams.ShowAdditionalRAsIssued);
            });
        }
        public List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result> GetCachedCreditComparisonResult(CreditComparisonParams reportParams)
        {
            string key = GetKeyFromParams(reportParams);

            return MemCacheManager.Get(key, CacheTimeInMin, () =>
            {
                return QueryForCreditComparisonResults(reportParams.ManufacturerProfileCode, MemCacheManager.GetDateAsString(reportParams.StartDate), MemCacheManager.GetDateAsString(reportParams.EndDate), reportParams.ExcludeCreditAlreadyIssued, reportParams.ReturnAuthorizationNumber, reportParams.LowestPrice, reportParams.ShowPrimaryReturnCode, reportParams.DebitMemoNumber, reportParams.ClosedOnly, reportParams.ShowAdditionalRAsIssued);
            });
        }

        public static string GetKeyFromParams(CreditComparisonParams reportParams)
        {
            PropertyInfo[] properties = typeof(CreditComparisonParams).GetProperties();
            var values = new List<string>();

            values.Add(typeof(CreditComparisonParams).Name);

            foreach (PropertyInfo property in properties)
            {
                var value = property.GetValue(reportParams);
                if (value == null)
                    values.Add("-");
                else if (property.PropertyType == typeof(DateTime?))
                    values.Add(((DateTime?)value).Value.ToString(DateFormat));
                else if (property.PropertyType == typeof(DateTime))
                    values.Add(((DateTime)value).ToString(DateFormat));
                else
                {
                    var stringVal = value.ToString();
                    values.Add(string.IsNullOrEmpty(stringVal) ? "-" : stringVal);
                }
            }

            var key = string.Concat(values);
            return key;
        }

        public static string GetPDFEmailSubject(CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result firstRA)
        {
            return string.Format("{0} RA#: {1} - {2} : Credit Comparison Report", firstRA.ManufacturerProfileName, firstRA.ReturnAuthorizationNumber, firstRA.DebitMemoNumber);
        }

        public static string GetPDFEmailSubject(string manufacturerProfileName, DateTime? startDate, DateTime? endDate)
        {
            var start = Convert.ToDateTime(startDate);
            var end = Convert.ToDateTime(endDate);
            return string.Format("{0} - Report Period From {1} Through {2}.", manufacturerProfileName, start.ToString(DateFormat), end.ToString(DateFormat));
        }

        public static string GetPDFEmailPlainTextContents(CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result firstRA, string downloadUrl, string expireDate, CreditComparisonParams ccReportParams, bool includeHeader)
        {
            var uRLTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays"));
            var reportPeriod = string.Empty;
            var header = string.Empty;
            if (!string.IsNullOrEmpty(firstRA.StartDate) && !string.IsNullOrEmpty(firstRA.EndDate))
            {
                reportPeriod = string.Format("Report Period From {0} Through {1}.", firstRA.StartDate, firstRA.EndDate);
            }

            if (includeHeader)
            {
                header = string.Format(@"Credit Comparison Report For Estimated Vs Processed Items (With Reasons)</br>{0}</br></br>", reportPeriod);
            }

            var result = string.Format(@"
{25}

Link to Download PDF Report: {21}
(Download will be available for {26} days or until {22})

CUSTOMER INFORMATION
Name: {0}
Address: {1}
Phone: {2}
Email: {3}
DEA Number: {4}
Debit Number: {5}
{24}
{6}
RETURN AUTHORIZATION REQUESTOR
Contact: {7}
Phone: {8}
Email: {9}


Name: {10}
Address: {11}
DEA Number: {12}

RETURN AUTHORIZATION INFORMATION
RA Number: {13}
Date Requested: {14}
Date Received: {15}
Date Credited: {16}
Credit Memo #: {17}
Direct Account:
{18}
{19}
{20}",
            firstRA.Name, // {0}
            firstRA.RAAddress.Replace("</br>", " "), // {1}
            firstRA.PhoneNumber, // {2}
            firstRA.EmailAddress, // {3}
            firstRA.DEANumber, // {4}
            firstRA.DebitMemoNumber, // {5}            
            firstRA.ReturnAuthorizationNumberSplit, // {6}
            firstRA.RequestorName, // {7}
            firstRA.RequestorPhoneNumber, // {8}
            firstRA.RequestorEmailAddress, // {9}
            firstRA.ThirdPartyReverseDistributorName, // {10}
            firstRA.ThirdPartyReverseDistributorAddress != null ? firstRA.ThirdPartyReverseDistributorAddress.Replace("</br>", " ") : String.Empty, // {11}
            firstRA.ThirdPartyReverseDistributorDEANumber, // {12}
            firstRA.ReturnAuthorizationNumber, // {13}
            firstRA.RequestDate.HasValue ? firstRA.RequestDate.Value.ToString(DateFormat) : string.Empty, // {14}
            firstRA.ReceivedDate.HasValue ? firstRA.ReceivedDate.Value.ToString(DateFormat) : string.Empty, // {15}
            firstRA.CreditIssuedDate.HasValue ? firstRA.CreditIssuedDate.Value.ToString(DateFormat) : string.Empty, // {16}
            firstRA.PrimaryCreditMemoNumber, // {17}
            firstRA.PrintName, // {18}
            firstRA.PrintAccountTitle, // {19}
            firstRA.PrintAccount, // {20}
            downloadUrl, // {21}
            expireDate, // {22}
            reportPeriod, // {23}
            ccReportParams.ShowAdditionalRAsIssued ? "Additional RAs Issued:</br>" : string.Empty,// {24}
            header, //{25}
            uRLTimeoutInDays //{26}
            );
            return result;
        }

        public static string GetPDFEmailHtmlContents(CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result firstRA, string downloadUrl, string expireDate, CreditComparisonParams ccReportParams, bool? includeHeader)
        {
            var uRLTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays"));
            var reportPeriod = string.Empty;
            var header = string.Empty;
            if (!string.IsNullOrEmpty(firstRA.StartDate) && !string.IsNullOrEmpty(firstRA.EndDate))
            {
                reportPeriod = string.Format("<div style='font-size:14px'>Report Period From {0} Through {1} </div>", firstRA.StartDate, firstRA.EndDate);
            }

            if (includeHeader == true)
            {
                header = string.Format(@"<tr><td style = 'border-collapse:collapse;border:0;font-weight:bold;font-size:16px;padding:6px;text-align:center;' colspan = '3' >
Credit Comparison Report For Estimated Vs Processed Items(With Reasons){0}</ td ></ tr >",reportPeriod);
            }

            var result = string.Format(@"
<table style='min-width:740px;max-width:740px;width:740px;border-collapse:collapse;border:0;border-spacing:0;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;font-size:12px;line-height:20px;color:#000;'>
    {25}
    <tr style='background-color:#F0F8FF;border-collapse:collapse;border:2px solid #000;vertical-align:top;'>
        <td style='border-collapse:collapse;border:2px solid #000;padding:0 5px;'>
            <div style='font-weight:bold;'>CUSTOMER INFORMATION</div>
            {0} <br />
            {1}
            <table style='border-collapse:collapse;border:0;font-size:12px;border-spacing:0;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;'>
                <tr>
                    <td style='padding-right:10px;'>Phone:</td>
                    <td>{2}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Email:</td>
                    <td>{3}</td>
                </tr>
            </table>
            <table style='border-collapse:collapse;border:0;font-size:12px;border-spacing:0;font-family: ""Helvetica Neue"",Helvetica,Arial,sans-serif;'>
                <tr>
                    <td style='padding-right:10px;'>DEA Number:</td>
                    <td>{4}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Debit Number:</td>
                    <td>{5}</td>
                </tr>
            </table>
            {24}
            {6}
        </td>
        <td style='border-collapse:collapse;border:2px solid #000;padding:0 5px;border-spacing:0;'>
            <div style='font-weight:bold;'>RETURN AUTHORIZATION REQUESTOR</div>
            <table style='border-collapse:collapse;border:0;font-size:12px;border-spacing:0;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;'>
                <tr>
                    <td style='padding-right:10px;'>Contact:</td>
                    <td>{7}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Phone:</td>
                    <td>{8}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Email:</td>
                    <td>{9}</td>
                </tr>
            </table>

            <div style='font-weight:bold;padding-top:15px;display:none;'>Third Party Return Service</div><br/>
            {10}<br/>
            {11}

            <table style='border-collapse:collapse;border:0;font-size:12px;margin-top:10px;border-spacing:0;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;'>
                <tr>
                    <td style='padding-right:10px;'>DEA Number:</td>
                    <td>{12}</td>
                </tr>
            </table>

        </td>
        <td style='border-collapse:collapse;border:2px solid #000;padding:0 5px;'>
            <div style='font-weight:bold;'>RETURN AUTHORIZATION INFORMATION</div>

            <table style='border-collapse:collapse;border:0;font-size:12px;border-spacing:0;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;'>
                <tr style='font-weight:bold'>
                    <td style='padding-right:10px;'>RA Number:</td>
                    <td>{13}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Date Requested:</td>
                    <td>{14}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Date Received:</td>
                    <td>{15}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Date Credited:</td>
                    <td>{16}</td>
                </tr>
                <tr>
                    <td style='padding-right:10px;'>Credit Memo #:</td>
                    <td>{17}</td>
                </tr>
            </table>
            Direct Account:<br />
            {18}<br />
            {19}<br />
            {20}
        </td>
    </tr>
    <tr>
        <td style='border-collapse:collapse;border:0;font-weight:bold;font-size:16px;padding:8px;text-align:center;' colspan='3'>
            <a href='{21}' target='_blank' style='margin:5px auto;text-align:center;font-weight:bold;font-size:16px;font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;color:#ffffff;text-decoration:none;border-radius:3px;background-color:#008CBA;border-top:12px solid #008CBA;border-bottom:12px solid #008CBA;border-right:18px solid #008CBA;border-left:18px solid #008CBA;display:inline-block;'> &nbsp; Download Report &nbsp; </a>
            <div style='font-size:12px;font-weight:normal;margin-top:5px'>Download will be available for {26} days or until {22}.</div>
        </td>
    </tr>
</table>",
            firstRA.Name, // {0}
            firstRA.RAAddress.Replace("</br>", "<br />"), // {1}
            firstRA.PhoneNumber, // {2}
            firstRA.EmailAddress, // {3}
            firstRA.DEANumber, // {4}
            firstRA.DebitMemoNumber, // {5}
            firstRA.ReturnAuthorizationNumberSplit, // {6}
            firstRA.RequestorName, // {7}
            firstRA.RequestorPhoneNumber, // {8}
            firstRA.RequestorEmailAddress, // {9}
            firstRA.ThirdPartyReverseDistributorName, // {10}
            firstRA.ThirdPartyReverseDistributorAddress != null ? firstRA.ThirdPartyReverseDistributorAddress.Replace("</br>", "<br />") : String.Empty, // {11}
            firstRA.ThirdPartyReverseDistributorDEANumber, // {12}
            firstRA.ReturnAuthorizationNumber, // {13}
            firstRA.RequestDate.HasValue ? firstRA.RequestDate.Value.ToString(DateFormat) : string.Empty, // {14}
            firstRA.ReceivedDate.HasValue ? firstRA.ReceivedDate.Value.ToString(DateFormat) : string.Empty, // {15}
            firstRA.CreditIssuedDate.HasValue ? firstRA.CreditIssuedDate.Value.ToString(DateFormat) : string.Empty, // {16}
            firstRA.PrimaryCreditMemoNumber, // {17}
            firstRA.PrintName, // {18}
            firstRA.PrintAccountTitle, // {19}
            firstRA.PrintAccount, // {20}
            downloadUrl, // {21}
            expireDate, // {22}
            reportPeriod, // {23}
            ccReportParams.ShowAdditionalRAsIssued ? "Additional RAs Issued:<br />" : string.Empty,// {24}
            header, //{25}
            uRLTimeoutInDays //{26}
            ); 
            return result;
        }
    }
}