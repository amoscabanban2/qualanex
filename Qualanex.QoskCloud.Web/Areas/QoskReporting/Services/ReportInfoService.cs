﻿using Kendo.Mvc.Export;
using Newtonsoft.Json;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Telerik.Documents.SpreadsheetStreaming;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Services
{
    public class ReportInfoService
    {
        public static class ReportFileType
        {
            public const string PDF = "PDF";
            public const string Excel = "Excel";
            public const string CSV = "CSV";
        }

        public static class ScheduleFrequency
        {
            public const string Daily = "Daily";
            public const string Weekly = "Weekly";
            public const string Monthly = "Monthly";
        }

        public List<ReportInfoViewModel> ReportInfoList;
        public ReportInfoService()
        {
            ReportInfoList = new List<ReportInfoViewModel>
            {
                new CreditComparisonViewModel { UniqueName="CreditComparison", Title = "Credit Comparison", Category = "Processed Reports",
                    ReportSource = new TypeReportSource() { TypeName = typeof(CreditComparison).AssemblyQualifiedName },
                    Description ="Estimated vs. Processed Credit Comparison Report With Reasons"
                },
                new ReportInfoViewModel { UniqueName="SampleReport", Title = "Another Sample Report", Category = "Data Extracts",
                    ReportSource = new UriReportSource() { Uri = "SampleReport.trdp" },
                    Description ="Description of a second report could go here"
                },
                new ReportInfoViewModel { UniqueName="SampleReport", Title = "Processed Items Extract", Category = "Data Extracts",
                    ReportSource = new UriReportSource() { Uri = "SampleReport.trdp" },
                    Description ="Here is a description for Processed Items Extract"
                },
                new ReportInfoViewModel { UniqueName="SampleReport", Title = "Short Pay Extract", Category = "Data Extracts",
                    ReportSource = new UriReportSource() { Uri = "SampleReport.trdp" },
                    Description ="It would be great if there was more information, you could put it here"
                },
                new ReportInfoViewModel { UniqueName="SampleReport", Title = "Processed Items by RA", Category = "Processed Reports",
                    ReportSource = new UriReportSource() { Uri = "SampleReport.trdp" },
                    Description ="Listing of all RAs with Items and what they are processed by"
                }
            };
        }

        public static T JsonDeserialize<T>(string obj)
        {
            return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(obj));
        }

        public static object JsonDeserialize(string obj)
        {
            return JsonConvert.DeserializeObject(HttpUtility.UrlDecode(obj));
        }

        public static string ToXml<T>(T obj)
        {
            using (var sw = new StringWriter())
            {                
                var xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, obj);
                return sw.ToString();
            }
        }
        public static string JsonToXml<T>(string json)
        {
            using (var sw = new StringWriter())
            {
                var typedObj = JsonDeserialize<T>(json);
                var xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, typedObj);
                return sw.ToString();
            }
        }
        public static T XmlDeserialize<T>(string xml)
        {
            using (var sr = new StringReader(xml))
            {
                var xs = new XmlSerializer(typeof(T));

                using (var xmlReader = XmlReader.Create(sr))
                {
                    return (T)xs.Deserialize(xmlReader);
                }
            }
        }

        public ReportSchedule GetNewReportSchedule(string reportUniqueName, string emailTo)
        {
            return new ReportSchedule()
            {
                StartDate = DateTime.Today,
                ScheduleFrequency = ScheduleFrequency.Weekly,
                ReportFileType = ReportFileType.PDF,
                ReportUniqueName = reportUniqueName,
                ScheduleTime = new TimeSpan(2,0,0),
                EmailTo = emailTo
            };
        }

        public static RenderingResult GetTelerikReportPDF(ReportSource typeReportSource, object ccReportParams, Type reportParamsType)
        {
            PropertyInfo[] properties = reportParamsType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                typeReportSource.Parameters.Add(new Telerik.Reporting.Parameter(property.Name, property.GetValue(ccReportParams)));
            }

            var reportProcessor = new ReportProcessor();

            // set any deviceInfo settings if necessary
            var deviceInfo = new System.Collections.Hashtable();
            RenderingResult renderingResult = reportProcessor.RenderReport("PDF", typeReportSource, deviceInfo);
            return renderingResult;
        }

        List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result> _queryData;

        public static SpreadDocumentFormat GetExportFormat(string format)
        {
            return format == "csv" ? SpreadDocumentFormat.Csv : SpreadDocumentFormat.Xlsx;
        }

        public static string GetMimeType(string format)
        {
            return Kendo.Mvc.Export.Helpers.GetMimeType(GetExportFormat(format));
        }

        public Stream GetDocumentStream(string format, string title, 
            List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result>  queryData, IList<ExportColumnSettings> columnsData)
        {
            _queryData = queryData;
            _columnsData = columnsData;
            var exportFormat = GetExportFormat(format);

            Action<ExportCellStyle> cellStyle = new Action<ExportCellStyle>(ChangeCellStyle);
            Action<ExportRowStyle> rowStyle = new Action<ExportRowStyle>(ChangeRowStyle);
            Action<ExportColumnStyle> columnStyle = new Action<ExportColumnStyle>(ChangeColumnStyle);

            if (exportFormat == SpreadDocumentFormat.Xlsx)
            {
                _columnsData.Remove(_columnsData.FirstOrDefault(x => x.Field == "ImageLinks"));
                if (_queryData.Count > 0)
                {
                    var maxImageCount = _queryData.Max(x => x.ImageLinksList.Count);
                    for (int i = 1; i <= maxImageCount; i++)
                    {
                        _columnsData.Add(new ExportColumnSettings { Title = "Image " + i, Field = "ImageLinks", Hidden = false, Width = 100 });
                    }
                }
            }
            else
            {
                var csvColumnData = new List<ExportColumnSettings>();
                csvColumnData.AddRange(_columnsData.Where(x => !x.Hidden));
                _columnsData = csvColumnData;
            }

            return exportFormat == SpreadDocumentFormat.Xlsx ?
                _queryData.ToXlsxStream(_columnsData, title, cellStyleAction: cellStyle, rowStyleAction: rowStyle, columnStyleAction: columnStyle) :
                _queryData.ToCsvStream(_columnsData);
        }

        private IList<ExportColumnSettings> _columnsData;

        private void ChangeCellStyle(ExportCellStyle e)
        {
            bool isHeader = e.Row == 0;
            var currentColumn = _columnsData.ElementAt(e.Column);
            bool isDate = currentColumn.Field.EndsWith("Date");
            bool isMonthYear = currentColumn.Field.EndsWith("MonthYear");
            bool isImageColumn = currentColumn.Title.StartsWith("Image ");

            SpreadCellFormat format = new SpreadCellFormat
            {
                //Fill = SpreadPatternFill.CreateSolidFill(isHeader ? new SpreadColor(240, 248, 255) : new SpreadColor(255,255,255)),
                NumberFormat = isHeader ? null : isDate ? "yyyy-MM-dd" : isMonthYear ? "MM/yyyy" : null
            };
            e.Cell.SetFormat(format);

            if (isImageColumn)
            {
                //not header
                if (!isHeader)
                {
                    int imageNumber = int.Parse(currentColumn.Title.Substring(currentColumn.Title.IndexOf(' ') + 1));
                    var currentRow = _queryData[e.Row - 1];
                    if (imageNumber <= currentRow.ImageLinksList.Count)
                    {
                        var text = currentRow.ImageLinksList[imageNumber - 1].Text;
                        var url = currentRow.ImageLinksList[imageNumber - 1].URL;
                        e.Cell.SetFormula("=HYPERLINK(\"" + url + "\",\"" + text + "\")");
                    }
                    else
                    {
                        e.Cell.SetValue("");
                    }
                }
            }
        }

        private void ChangeRowStyle(ExportRowStyle e)
        {
            // -- Set row styles here
            //e.Row.SetHeightInPixels(30);
        }

        private void ChangeColumnStyle(ExportColumnStyle e)
        {
            // -- Set column width based on width of header
            if (e != null && e.Column != null && e.Name != null)
            {
                switch (e.Name)
                {
                    case "Name":
                        e.Column.SetWidthInPixels(250);
                        break;
                    default:
                        e.Column.SetWidthInCharacters(e.Name.Length);
                        break;
                }
            }
            // -- Set column width based on width from preview
            //var currentColumn = columnsData.ElementAt(e.Index);
            //e.Column.SetWidthInPixels(currentColumn.Width.Value);
        }
    }
}