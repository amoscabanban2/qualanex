﻿using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    public class DeleteExpiredReportsController : Controller
    {
        // POST: QoskReporting/DeleteExpiredReports/Delete
        // GET: QoskReporting/DeleteExpiredReports/Delete
        [AllowAnonymous]
        public ActionResult Delete()
        {
            var result = ReportingBlobResource.DeleteExpiredReports();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}