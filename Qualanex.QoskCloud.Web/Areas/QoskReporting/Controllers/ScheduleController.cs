﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Export;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Services;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    public class ScheduleController : Controller
    {
        private QIPSReportingEntities db = new QIPSReportingEntities();
        private ReportInfoService reportInfoService = new ReportInfoService();

        private UserRegistrationRequest GetUserData()
        {
            return Qualanex.QoskCloud.Web.Areas.Product.Controllers.UpaController.GetUserLoginData(HttpContext).UserReturnData;
        }

        public ActionResult ReportSchedules_Read([DataSourceRequest]DataSourceRequest request, string reportUniqueName)
        {
            var userData = GetUserData();

            IQueryable<ReportSchedule> reportschedules =
                db.ReportSchedules.Where(r => r.UserID == userData.UserID && r.ReportUniqueName == reportUniqueName && r.IsDeleted != true);
            DataSourceResult result = reportschedules.ToDataSourceResult(request);

            return Json(result);
        }

        [HttpPost]
        public ActionResult Save(ReportSchedule reportSchedule)
        {
            var userData = GetUserData();

            var ccReportParamsXml = ReportInfoService.JsonToXml<CreditComparisonParams>(reportSchedule.ReportParamsXml);
            var columnsDataXml = ReportInfoService.JsonToXml<List<ExportColumnSettings>>(reportSchedule.ReportColumnsXml);
            var ccScheduleParamsXml = ReportInfoService.JsonToXml<CreditComparisonScheduleParams>(reportSchedule.ScheduleParamsXml);
            // create
            if (reportSchedule.ReportScheduleID == 0)
            {
                var entity = new ReportSchedule
                {
                    UserID = userData.UserID.Value,
                    IsDeleted = false,
                    ScheduleName = reportSchedule.ScheduleName,
                    ReportUniqueName = reportSchedule.ReportUniqueName,
                    ScheduleTime = reportSchedule.ScheduleTime,
                    ScheduleFrequency = reportSchedule.ScheduleFrequency,
                    StartDate = reportSchedule.StartDate,
                    EndDate = reportSchedule.EndDate,
                    EmailTo = reportSchedule.EmailTo,
                    TargetTimeZone = reportSchedule.TargetTimeZone,
                    ReportFileType = reportSchedule.ReportFileType,
                    ScheduleParamsXml = ccScheduleParamsXml,
                    ReportColumnsXml = columnsDataXml,
                    ReportParamsXml = ccReportParamsXml,

                    CreatedBy = userData.UserName,
                    CreatedDate = DateTime.UtcNow,
                    ModifiedBy = null,
                    ModifiedDate = null
                };

                db.ReportSchedules.Add(entity);
                db.SaveChanges();
                reportSchedule.ReportScheduleID = entity.ReportScheduleID;
            }
            // edit
            else
            {
                var entity = db.ReportSchedules.Where(x => x.ReportScheduleID == reportSchedule.ReportScheduleID).SingleOrDefault();
                entity.ScheduleName = reportSchedule.ScheduleName;
                entity.ReportUniqueName = reportSchedule.ReportUniqueName;
                entity.ScheduleTime = reportSchedule.ScheduleTime;
                entity.ScheduleFrequency = reportSchedule.ScheduleFrequency;
                entity.StartDate = reportSchedule.StartDate;
                entity.EndDate = reportSchedule.EndDate;
                entity.EmailTo = reportSchedule.EmailTo;
                entity.TargetTimeZone = reportSchedule.TargetTimeZone;
                entity.ReportFileType = reportSchedule.ReportFileType;
                entity.ScheduleParamsXml = ccScheduleParamsXml;
                entity.ReportColumnsXml = columnsDataXml;
                entity.ReportParamsXml = ccReportParamsXml;
                entity.ModifiedBy = userData.UserName;
                entity.ModifiedDate = DateTime.UtcNow;
                db.SaveChanges();
            }

            return new RedirectResult(Url.Action("Report", "Home", new { id = reportSchedule.ReportUniqueName }) + "#Schedule");
        }
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportSchedules_Destroy([DataSourceRequest]DataSourceRequest request, ReportSchedule reportSchedule)
        {

            var userData = GetUserData();
            var reportSch = db.ReportSchedules.Find(reportSchedule.ReportScheduleID);
            if (reportSch != null)
            {
                reportSch.ModifiedBy = userData.UserName;
                reportSch.ModifiedDate = DateTime.UtcNow;
                reportSch.IsDeleted = true;
                db.ReportSchedules.Attach(reportSch);
                db.Entry(reportSch).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new[] { reportSchedule }.ToDataSourceResult(request, ModelState));
            }
            else
            {
                ModelState.AddModelError("ReportSchedule", "Not Found");
                return Json(new[] { reportSchedule}.ToDataSourceResult(request, ModelState));
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
