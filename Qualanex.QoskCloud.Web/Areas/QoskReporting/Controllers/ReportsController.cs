namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Views.Controllers
{
    using Utility;
    using System.IO;
    using System.Web;
    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.Engine;
    using Telerik.Reporting.Services.WebApi;
    using Qualanex.QoskCloud.Utility;

    //The class name determines the service URL. 
    //ReportsController class name defines /api/report/ service URL.
    public class ReportsController : ReportsControllerBase
    {
        static ReportServiceConfiguration preservedConfiguration;

        static IReportServiceConfiguration PreservedConfiguration
        {
            get
            {
                if (null == preservedConfiguration)
                {
                    preservedConfiguration = new ReportServiceConfiguration
                    {
                        HostAppId = Constants.TelerikReportsController_MvcDemoApp,
                        Storage = new FileStorage(),
                        ReportResolver = CreateResolver(),
                    };
                }
                return preservedConfiguration;
            }
        }

        public ReportsController()
        {
            this.ReportServiceConfiguration = PreservedConfiguration;
        }
        static IReportResolver CreateResolver()
        {
            var appPath = HttpContext.Current.Server.MapPath("~/Areas/QoskReporting/");
            var reportsPath = Path.Combine(appPath, "Reports");

            return new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());
        }
    }
}