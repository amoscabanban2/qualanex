﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Kendo.Mvc.Export;
using System.IO;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Services;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    public class GridController : Controller
    {
        private readonly QIPSReportingEntities db = new QIPSReportingEntities();
        private readonly CreditComparisonService _creditComparisonService = new CreditComparisonService();

        public ActionResult CreditComparison()
        {
            return View();
        }

        private async Task<List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result>> ReadCreditComparisonEstimatedVsProcessedReportWithReasonsOskAsync(CreditComparisonParams creditComparisonParams, bool export)
        {
            var resultList = await _creditComparisonService.GetCachedCreditComparisonResultAsync(creditComparisonParams);

            if (creditComparisonParams.ImagesPermitted && creditComparisonParams.DisplayImages)
            {
                var QueryList = GetImageLinksForQueryList(resultList, export);
            }

            return resultList;
        }

        public async Task<ActionResult> CreditComparison_Read([DataSourceRequest]DataSourceRequest request, Models.CreditComparisonParams creditComparisonParams)
        {            
            var queryList = await ReadCreditComparisonEstimatedVsProcessedReportWithReasonsOskAsync(creditComparisonParams, false);

            DataSourceResult result = queryList.ToDataSourceResult(request);

            return Json(result);
        }

        private object GetImageLinksForQueryList(List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result> queryResult, bool export)
        {
            string imageLinks = "";
            if (export)
            {
                foreach (var row in queryResult)
                {
                    row.ImageLinksList = new List<LinkInfo>();
                    var images = new Images();
                    var imageList = images.GetImages(row.ItemIDs, row.ReturnAuthorizationNumber.ToString(), true, true);
                    foreach (var image in imageList)
                    {
                        imageLinks += image.ContentSasURI + "   |   ";
                        row.ImageLinksList.Add(new LinkInfo { Text = GetLastTenChars(image.BlobGuid) + " " + image.ContentName, URL = image.ContentSasURI });
                    }
                    row.ImageLinks = imageLinks;
                    imageLinks = "";
                }
            }
            else
            {
                foreach (var query in queryResult)
                {
                    var images = new Images();
                    var imageList = images.GetImages(query.ItemIDs, query.ReturnAuthorizationNumber.ToString(), true, true);
                    foreach (var image in imageList)
                    {
                        var last = imageList.Last();
                        if (!imageLinks.Contains(GetLastTenChars(image.BlobGuid)))
                        {
                            imageLinks += GetLastTenChars(image.BlobGuid) + " : <a target='_blank' href='" + image.ContentSasURI + "'>" + image.ContentName + "</a>";
                            if (imageList.Count > 1 && !image.Equals(last))
                            {
                                imageLinks += "  ";
                            }
                            else
                            {
                                imageLinks += "   ";
                            }
                        }
                        else
                        {
                            imageLinks += "<a target='_blank' href='" + image.ContentSasURI + "'>" + image.ContentName + "</a>";
                            if (imageList.Count > 1 && !image.Equals(last))
                            {
                                imageLinks += "    ";
                            }
                            else
                            {
                                imageLinks += "<br />";
                            }
                        }
                    }
                    query.ImageLinks = imageLinks;
                    imageLinks = "";
                }
            }
            return queryResult;
        }


        private string GetLastTenChars(string blobGuid)
        {
            if (blobGuid.Length > 10)
                return blobGuid.Substring(blobGuid.Length - 10);
            else
                return blobGuid;
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
    
        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }      
        

        [HttpPost]
        public async Task<FileStreamResult> ExportServer(string columns, string options, string reportParams)
        {
            var ccReportParams = ReportInfoService.JsonDeserialize<CreditComparisonParams>(reportParams);
            var columnsData = ReportInfoService.JsonDeserialize<IList<ExportColumnSettings>>(columns); ;

            dynamic fileOptions = ReportInfoService.JsonDeserialize(options);
            var format = (string)fileOptions.format;
            var title = (string)fileOptions.title;

            var queryData = await ReadCreditComparisonEstimatedVsProcessedReportWithReasonsOskAsync(ccReportParams, true);

            var reportInfoService = new ReportInfoService();
            Stream exportStream = reportInfoService.GetDocumentStream(format, title, queryData, columnsData);

            string fileName = string.Format("{0}.{1}", title, format);
            string mimeType = ReportInfoService.GetMimeType(format);

            var fileStreamResult = new FileStreamResult(exportStream, mimeType);
            fileStreamResult.FileDownloadName = fileName;
            fileStreamResult.FileStream.Seek(0, SeekOrigin.Begin);

            return fileStreamResult;
        }        

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
