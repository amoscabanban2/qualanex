﻿using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Services;
using Qualanex.QoskCloud.Entity;
using Kendo.Mvc.Export;
using Telerik.Reporting.Processing;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Text;
using System.Web.Hosting;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    public class SendScheduledEmailsController : Controller
    {
        private QIPSReportingEntities db = new QIPSReportingEntities();
        private ReportInfoService reportInfoService = new ReportInfoService();

        // POST: QoskReporting/SendScheduledEmails/Send
        // GET: QoskReporting/SendScheduledEmails/Send
        [AllowAnonymous]
        public ActionResult Send()
        {
            // using QueueBackgroundWorkItem will help prevent IIS from ending the background task
            // note: we not not currently using the CancellationToken (ct)
            HostingEnvironment.QueueBackgroundWorkItem(ct => SendScheduledEmailsAsync());
                        
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        
        private async Task SendScheduledEmailsAsync()
        {
            List<ReportSchedule> reportScheduleList = await GetScheduledReportsAsync();

            foreach (var report in reportScheduleList)
            {                
                var reportParams = ReportInfoService.XmlDeserialize<CreditComparisonParams>(report.ReportParamsXml);
                var reportColumns = ReportInfoService.XmlDeserialize<List<ExportColumnSettings>>(report.ReportColumnsXml);
                var scheduleParams = ReportInfoService.XmlDeserialize<CreditComparisonScheduleParams>(report.ScheduleParamsXml);

                var reportInfo = reportInfoService.ReportInfoList.Find(x => x.UniqueName == report.ReportUniqueName);

                var reportResults = await GetReportQueryResultsAsync(reportParams, scheduleParams);

                var msg = new SendGridMessage();
                //get user from db
                var userInfo = SendEmailService.GetUserInfoByID(report.UserID);
                var manufacturerName = SendEmailService.GetManufacturerInfoByProfileCode(reportParams.ManufacturerProfileCode);

                msg.SetFrom(new EmailAddress(userInfo.Email, userInfo.FullName));
                SendEmailService.AddToAddresses(report.EmailTo, msg);

                if (reportResults == null || reportResults.Count <= 0)
                {
                    msg.SetSubject(manufacturerName + " - No data found for " + reportInfo.Title + " Report");
                    msg.AddContent(MimeType.Text, manufacturerName + " - No data found for " + reportInfo.Title + " Report between " + reportParams.StartDate + " and " + reportParams.EndDate + ".");
                    SendEmailService.SendEmail(msg);

                    continue;
                }
                else
                {
                    if (scheduleParams.SplitByRANumber == false)
                    {

                        msg.SetSubject(CreditComparisonService.GetPDFEmailSubject(manufacturerName, reportParams.StartDate, reportParams.EndDate));
                        var urlTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays"));
                        var expireDate = DateTime.Now.AddDays(urlTimeoutInDays).ToString(CreditComparisonService.DateFormat);
                        var downloadUrl = string.Empty;
                        if (report.ReportFileType == "PDF")
                        {
                            RenderingResult renderingResult = ReportInfoService.GetTelerikReportPDF(reportInfo.ReportSource, reportParams, typeof(CreditComparisonParams));
                            downloadUrl = ReportingBlobResource.SaveFileGetAccessURL("qoskreports", "qoskreports", urlTimeoutInDays + 1, renderingResult.DocumentBytes, ".pdf", "application/pdf", null);
                        }
                        else
                        {
                            
                            var fileType = report.ReportFileType.Contains("Excel") ? "xlsx" : "csv";

                            var reportInfoService = new ReportInfoService();
                            Stream exportStream = reportInfoService.GetDocumentStream(fileType, "Report", reportResults, reportColumns);

                            
                            downloadUrl = ReportingBlobResource.SaveFileGetAccessURL("qoskreports", "qoskreports", urlTimeoutInDays + 1, 
                                null, "." + fileType, ReportInfoService.GetMimeType(fileType), exportStream);
                        }
                        // only use the 1st RA for making the header that appears in the email
                        var firstRA = reportResults.FirstOrDefault();
                        msg.AddContent(MimeType.Text, CreditComparisonService.GetPDFEmailPlainTextContents(firstRA, downloadUrl, expireDate, reportParams, true));
                        msg.AddContent(MimeType.Html, CreditComparisonService.GetPDFEmailHtmlContents(firstRA, downloadUrl, expireDate, reportParams, true));

                        SendEmailService.SendEmail(msg);
                    }
                    else
                    {
                        var groupedReportResults = reportResults.GroupBy(u => u.ReturnAuthorizationNumber).ToList();
                        msg.SetSubject(CreditComparisonService.GetPDFEmailSubject(manufacturerName, reportParams.StartDate, reportParams.EndDate));

                        var plainTextString = new StringBuilder();
                        var htmlString = new StringBuilder();

                        for (var i = 0; i <= groupedReportResults.Count - 1; i++)
                        {
                            var firstRaInGroup = groupedReportResults[i].FirstOrDefault();
                            var includeHeader = true;
                            if (i > 0) { includeHeader = false; }
                            var urlTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays")); 
                            var expireDate = DateTime.Now.AddDays(urlTimeoutInDays).ToString(CreditComparisonService.DateFormat);
                            reportParams.ReturnAuthorizationNumber = firstRaInGroup.ReturnAuthorizationNumber.ToString(); // make sure we only get 1 RA number
                            reportParams.DebitMemoNumber = firstRaInGroup.DebitMemoNumber.ToString();
                            var downloadUrl = string.Empty;
                            if (report.ReportFileType == "PDF") {

                                RenderingResult renderingResult = ReportInfoService.GetTelerikReportPDF(reportInfo.ReportSource, reportParams, typeof(CreditComparisonParams));
                                downloadUrl = ReportingBlobResource.SaveFileGetAccessURL("qoskreports", "qoskreports", urlTimeoutInDays + 1, renderingResult.DocumentBytes, ".pdf", "application/pdf");
                            }
                            else
                            {
                                var fileType = report.ReportFileType.Contains("Excel") ? "xlsx" : "csv";

                                var reportInfoService = new ReportInfoService();
                                Stream exportStream = reportInfoService.GetDocumentStream(fileType, "Report", groupedReportResults[i].ToList(), reportColumns);

                                downloadUrl = ReportingBlobResource.SaveFileGetAccessURL("qoskreports", "qoskreports", urlTimeoutInDays + 1,
                                    null, "." + fileType, ReportInfoService.GetMimeType(fileType), exportStream);
                            }
                            plainTextString.Append(CreditComparisonService.GetPDFEmailPlainTextContents(firstRaInGroup, downloadUrl, expireDate, reportParams, includeHeader));
                            htmlString.Append(CreditComparisonService.GetPDFEmailHtmlContents(firstRaInGroup, downloadUrl, expireDate, reportParams, includeHeader));

                            if (i < reportResults.Count - 1)
                            {
                                plainTextString.Append("</br></br>");
                                htmlString.Append("<br /><br />");
                            }
                        }

                        msg.AddContent(MimeType.Text, plainTextString.ToString());
                        msg.AddContent(MimeType.Html, htmlString.ToString());
                        SendEmailService.SendEmail(msg);
                    }
                }
            }
        }

        private static async Task<List<CreditComparisonEstimatedVsProcessedReportWithReasonsQosk_Result>> GetReportQueryResultsAsync(CreditComparisonParams reportParams, CreditComparisonScheduleParams scheduleParams)
        {
            var today = DateTime.Today;
            //var today = new DateTime(2019, 1, 22);
            var creditComparisonService = new CreditComparisonService();
            reportParams.ReturnAuthorizationNumber = null;

            switch (scheduleParams.DaysToInclude)
            {
                case CreditComparisonService.DaysToIncludeList.PrevMonth:
                    //var lastDayOfLastMonth = DateTime.DaysInMonth(today.Year, today.Month - 1);
                    reportParams.StartDate = today.AddMonths(-1);
                    reportParams.EndDate = today.AddDays(-1);
                    break;
                case CreditComparisonService.DaysToIncludeList.FourteenDays:
                    reportParams.StartDate = today.AddDays(-14);
                    reportParams.EndDate = today.AddDays(-1);
                    break;
                case CreditComparisonService.DaysToIncludeList.SevenDays:
                    reportParams.StartDate = today.AddDays(-7);
                    reportParams.EndDate = today.AddDays(-1);
                    break;
                case CreditComparisonService.DaysToIncludeList.OneDay:
                    reportParams.StartDate = today.AddDays(-1);
                    reportParams.EndDate = today.AddDays(-1);
                    break;
            }

            return await creditComparisonService.GetCachedCreditComparisonResultAsync(reportParams);
        }

        private async Task<List<ReportSchedule>> GetScheduledReportsAsync()
        {
            var today = DateTime.Today;
            var now = DateTime.Now;

            List<ReportSchedule> fallsInDateRange = await
                           db.ReportSchedules.Where(r => (r.StartDate <= today && (r.EndDate >= today || r.EndDate == null))).ToListAsync();

            var reportScheduleList = new List<ReportSchedule>();
            foreach (var item in fallsInDateRange)
            {
                switch (item.ScheduleFrequency)
                {
                    case ReportInfoService.ScheduleFrequency.Monthly:
                        if (item.StartDate.Day == today.Day && item.ScheduleTime.Hours == now.Hour)
                            reportScheduleList.Add(item);
                        break;
                    case ReportInfoService.ScheduleFrequency.Weekly:
                        if (item.StartDate.DayOfWeek == today.DayOfWeek && item.ScheduleTime.Hours == now.Hour)
                            reportScheduleList.Add(item);
                        break;
                    case ReportInfoService.ScheduleFrequency.Daily:
                        if (item.ScheduleTime.Hours == now.Hour)
                            reportScheduleList.Add(item);
                        break;
                }
            }

            return reportScheduleList;
        }
    }
}