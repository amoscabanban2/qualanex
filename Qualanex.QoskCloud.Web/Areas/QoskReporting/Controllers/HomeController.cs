﻿using Newtonsoft.Json;
using Qualanex.QoskCloud.Web.Common;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Telerik.Reporting;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility;
using Qualanex.QoskCloud.Entity;
using System.Data.Entity.Core.Objects;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Services;
using SendGrid.Helpers.Mail;
using SendGrid;
using Telerik.Reporting.Processing;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;
using Kendo.Mvc.Export;
using Kendo.Mvc.UI;
using System.Web.Hosting;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    [QoskAuthorize]
    public class HomeController : Controller
    {
        private ReportInfoService reportInfoService = new ReportInfoService();
        private QIPSReportingEntities db = new QIPSReportingEntities();

        public ActionResult Index()
        {
            var model = new ReportListViewModel()
            {
                ReportInfoList = reportInfoService.ReportInfoList,
                Groups = reportInfoService.ReportInfoList.GroupBy(x => x.Category)
            };

            return View(model);
        }


        public ActionResult Report(string id, long? scheduleId)
        {
            var userData = GetUserData();
            //todo getImagePermission
            bool imagesPermitted = true;
            var reportInfo = reportInfoService.ReportInfoList.Find(x => x.UniqueName == id) as CreditComparisonViewModel;

            if(scheduleId.HasValue)
                reportInfo.ReportSchedule = scheduleId > 0 ? 
                    db.ReportSchedules.FirstOrDefault(r => r.ReportScheduleID == scheduleId.Value && r.UserID == userData.UserID && r.ReportUniqueName == id) 
                    : reportInfoService.GetNewReportSchedule(id, userData.Email);

            reportInfo.UserInfo = new UserInfo
            {
                FullName = string.Format("{0} {1}", userData.FirstName, userData.LastName),
                Email = userData.Email
            };

            CreditComparisonParams reportParams;
            if(reportInfo.ReportSchedule != null && reportInfo.ReportSchedule.ReportScheduleID > 0)
            {
                reportParams = ReportInfoService.XmlDeserialize<CreditComparisonParams>(reportInfo.ReportSchedule.ReportParamsXml);

                // setup params that will be adjusted later by the scheduler, just so the preview / download buttons can work
                reportParams.StartDate = DateTime.Today.AddDays(-1);
                reportParams.EndDate = DateTime.Today.AddDays(-1);
                reportParams.ReturnAuthorizationNumber = null;

                reportInfo.ScheduleParams = ReportInfoService.XmlDeserialize<CreditComparisonScheduleParams>(reportInfo.ReportSchedule.ScheduleParamsXml);
                reportInfo.ReportSchedule.ReportColumns = ReadGridColumns(reportInfo.ReportSchedule.ReportColumnsXml);
            }
            else
            {
                reportParams = SetDefaultParams();
            }

            reportParams.ManufacturerProfileCode = userData.ProfileCode ?? 0;
            
            //Hardcode here to view other profile codes
            //reportParams.ManufacturerProfileCode = 10019;
            //reportParams.ReturnAuthorizationNumber = "858539";

            reportParams.ImagesPermitted = imagesPermitted;
            reportInfo.Params = reportParams;

            return View(reportInfo);
        }

        private static List<GridColumnSettings> ReadGridColumns(string reportColumnsXml)
        {
            var exportColumns = ReportInfoService.XmlDeserialize<List<ExportColumnSettings>>(reportColumnsXml);
            var gridColumnsList = new List<GridColumnSettings>();
            foreach (var exportColumn in exportColumns)
            {
                gridColumnsList.Add(new GridColumnSettings()
                {
                    Title = exportColumn.Title,
                    Width = string.IsNullOrWhiteSpace(exportColumn.Width.ToString()) ? "100px" : exportColumn.Width.ToString(),
                    Member = exportColumn.Field,
                    Hidden = exportColumn.Hidden,
                    Format = exportColumn.Field.EndsWith("Date") ? "{0:yyyy-MM-dd}" : null,
                    ClientTemplate = exportColumn.Field == "ImageLinks" ? "#=getButtonTemplate(data,'buttonsTemplate')#" : null
                });
            }

            return gridColumnsList;
        }

        private UserRegistrationRequest GetUserData()
        {
            return Qualanex.QoskCloud.Web.Areas.Product.Controllers.UpaController.GetUserLoginData(HttpContext).UserReturnData;
        }

        private CreditComparisonParams SetDefaultParams()
        {
            var parameters = new CreditComparisonParams
            {
                ManufacturerProfileCode = null,
                ReturnAuthorizationNumber = null,
                ShowPrimaryReturnCode = false,
                ImagesPermitted = true,
                DisplayImages = true,
                ShowAdditionalRAsIssued = false,
                ExcludeCreditAlreadyIssued = false,
                LowestPrice = true,
                DebitMemoNumber = "",
                ClosedOnly = false
            };
            return parameters;
        }

        [HttpPost]
        public ActionResult SendReportEmail(string id, string reportParams, string sendToEmails)
        {
            var reportInfo = reportInfoService.ReportInfoList.Find(x => x.UniqueName == id);            
            var ccReportParams = ReportInfoService.JsonDeserialize<CreditComparisonParams>(reportParams);
            var userData = GetUserData();

            // using QueueBackgroundWorkItem will help prevent IIS from ending the background task
            // note: we not not currently using the CancellationToken (ct)
            HostingEnvironment.QueueBackgroundWorkItem(ct => SendSinglePDFReportEmailAsync(sendToEmails, reportInfo, ccReportParams, userData));

            var result = true;
            return Json(result);
        }

        private async Task SendSinglePDFReportEmailAsync(string sendToEmails, ReportInfoViewModel reportInfo, CreditComparisonParams ccReportParams, UserRegistrationRequest userData)
        {
            var creditComparisonService = new CreditComparisonService();

            var queryResult = await creditComparisonService.GetCachedCreditComparisonResultAsync(ccReportParams);
            var firstRA = queryResult.FirstOrDefault();

            if (firstRA == null)
                return;

            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress(userData.Email, string.Format("{0} {1}", userData.FirstName, userData.LastName)));
            SendEmailService.AddToAddresses(sendToEmails, msg);

            msg.SetSubject(CreditComparisonService.GetPDFEmailSubject(firstRA));

            var urlTimeoutInDays = Convert.ToInt32(QoskCloud.Utility.Common.ConfigurationManager.GetAppSettingValue("URLTimeoutInDays")); 
            ccReportParams.ReturnAuthorizationNumber = firstRA.ReturnAuthorizationNumber.ToString(); // make sure we only get 1 RA number
            RenderingResult renderingResult = ReportInfoService.GetTelerikReportPDF(reportInfo.ReportSource, ccReportParams, typeof(CreditComparisonParams));

            var downloadUrl = ReportingBlobResource.SaveFileGetAccessURL("qoskreports", "qoskreports", urlTimeoutInDays + 1, renderingResult.DocumentBytes, ".pdf", "application/pdf");
            //var downloadUrl = "#";
            var expireDate = DateTime.Now.AddDays(urlTimeoutInDays).ToString(CreditComparisonService.DateFormat);

            msg.AddContent(MimeType.Text, CreditComparisonService.GetPDFEmailPlainTextContents(firstRA, downloadUrl, expireDate, ccReportParams, false));
            msg.AddContent(MimeType.Html, CreditComparisonService.GetPDFEmailHtmlContents(firstRA, downloadUrl, expireDate, ccReportParams, false));

            SendEmailService.SendEmail(msg);
        }

        [HttpPost]
        public async Task<ActionResult> GetEmailInfo(string id, string reportParams)
        {
            // may need the id to make this more flexible in the future
            //var reportInfo = ReportInfoList.Find(x => x.UniqueName == id);

            var creditComparisonService = new CreditComparisonService();
            var ccReportParams = ReportInfoService.JsonDeserialize<CreditComparisonParams>(reportParams);

            var queryResult = await creditComparisonService.GetCachedCreditComparisonResultAsync(ccReportParams);
            var firstRA = queryResult.FirstOrDefault();

            var result = new CreditComparisonEmailInfo();
            if (firstRA == null)
            {
                result.Subject = "No Data";
                result.RANum = null;
            }
            else
            {
                result.Subject = CreditComparisonService.GetPDFEmailSubject(firstRA);
                result.RANum = firstRA.ReturnAuthorizationNumber;
            }            

            return Json(result);
        }        

        [HttpPost]
        public ActionResult PdfReport(string id, string key)
        {
            var reportInfo = reportInfoService.ReportInfoList.Find(x => x.UniqueName == id);

            if (MemCacheManager.IsSet(key))
            {
                var renderingResult = MemCacheManager.Get<RenderingResult>(key);

                MemoryStream ms = new MemoryStream();
                ms.Write(renderingResult.DocumentBytes, 0, renderingResult.DocumentBytes.Length);
                ms.Flush();

                FileContentResult result = new FileContentResult(ms.GetBuffer(), renderingResult.MimeType);
                result.FileDownloadName = reportInfo.UniqueName + ".pdf";

                // add cookie so the spinner on the page knows when to stop spinning
                HttpCookie qfdCookie = new HttpCookie("QoskFileDownload");
                qfdCookie.Value = "1";
                qfdCookie.Expires = DateTime.Now.AddHours(2);
                Response.Cookies.Add(qfdCookie);

                return result;
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult StartPdfReport(string id, string reportParams)
        {
            var reportInfo = reportInfoService.ReportInfoList.Find(x => x.UniqueName == id);
            var ccReportParams = ReportInfoService.JsonDeserialize<CreditComparisonParams>(reportParams);

            var key = GetReportKey(ccReportParams);

            if (MemCacheManager.IsSet(key))
                return Json(new { done = true, key });

            HostingEnvironment.QueueBackgroundWorkItem(ct => BeginReportRendering(reportInfo, ccReportParams));

            return Json(new { done = false, key });
        }

        [HttpPost]
        public ActionResult CheckPdfReportStatus(string key)
        {
            var done = MemCacheManager.IsSet(key);
            return Json(new { done, key });
        }

        private const string PdfReportKey = "pdfReport";
        private const int PdfReportCacheTimeInMin = 15;
        private static string GetReportKey(CreditComparisonParams reportParams)
        {
            return CreditComparisonService.GetKeyFromParams(reportParams) + PdfReportKey;
        }

        private void BeginReportRendering(ReportInfoViewModel reportInfo, CreditComparisonParams ccReportParams)
        {
            var report = ReportInfoService.GetTelerikReportPDF(reportInfo.ReportSource, ccReportParams, typeof(CreditComparisonParams));
            MemCacheManager.Set(GetReportKey(ccReportParams), report, PdfReportCacheTimeInMin);            
        }
    }
}
