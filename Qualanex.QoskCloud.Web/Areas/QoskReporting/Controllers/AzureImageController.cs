﻿using Qualanex.QoskCloud.Web.Areas.QoskReporting.Models;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility;
using Qualanex.QoskCloud.Web.Areas.QoskReporting.Utility.Common;
using Qualanex.QoskCloud.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Areas.QoskReporting.Controllers
{
    public class AzureImageController : AsyncController
    {
        public const string AzureThumbInfoCache = "AzureThumbInfoCache";

        // POST: QoskReporting/AzureImage/Json
        // GET: QoskReporting/AzureImage/Json
        [AllowAnonymous]
        public ActionResult Json(string prefixHex, bool runDaily)
        {
            AzureThumbInfo thumbInfo = new AzureThumbInfo()
            {
                PrefixHex = prefixHex,
                RunDaily = runDaily
            };
            GenerateThumbs(thumbInfo);

            return Json(thumbInfo, JsonRequestBehavior.AllowGet);
        }

        // POST: QoskReporting/AzureImage
        // GET: QoskReporting/AzureImage
        [AllowAnonymous]
        public ActionResult Index(AzureThumbInfo thumbInfo)
        {            
            if(!GenerateThumbs(thumbInfo))
                SetDefaults(thumbInfo);

            return View(thumbInfo);
        }

        private static bool GenerateThumbs(AzureThumbInfo thumbInfo)
        {
            // see if we are still running a previous job
            var previousRun = MemCacheManager.Get<AzureThumbInfo>(AzureThumbInfoCache);
            if (previousRun != null && (previousRun.IsComplete || previousRun.Stop))
                MemCacheManager.Remove(AzureThumbInfoCache);


            if (!MemCacheManager.IsSet(AzureThumbInfoCache) && (thumbInfo.RunWithNoPrefix || !string.IsNullOrWhiteSpace(thumbInfo.PrefixHex)))
            {
                SetDefaults(thumbInfo);

                MemCacheManager.Set(AzureThumbInfoCache, thumbInfo, 300);

                // using QueueBackgroundWorkItem will help prevent IIS from ending the background task
                HostingEnvironment.QueueBackgroundWorkItem(ct => ReportingBlobResource.GenerateAzureImageThumbsAsync(ct, thumbInfo));
                return true;
            }

            return false;
        }

        private static void SetDefaults(AzureThumbInfo thumbInfo)
        {
            if (thumbInfo.RunWithNoPrefix)
                thumbInfo.PrefixHex = string.Empty;

            if (thumbInfo.MaxContainers <= 0)
                thumbInfo.MaxContainers = 10000;
            if (thumbInfo.MaxParallelThreads <= 0)
                thumbInfo.MaxParallelThreads = 10;
            if (thumbInfo.ContainersPerFetch <= 0)
                thumbInfo.ContainersPerFetch = 500;
        }

        [AllowAnonymous]
        public ActionResult Progress()
        {
            var info = MemCacheManager.Get<AzureThumbInfo>(AzureThumbInfoCache);

            var result = info == null ? new AzureThumbResult() : new AzureThumbResult()
            {
                ImagesCreated = info.ImagesCreated,
                ContainersProcessed = info.ContainersProcessed,
                ContainerSetsFetched = info.ContainerSetsFetched,
                ContainersAlreadyExisted = info.ContainersAlreadyExisted,
                StartTime = info.StartTime.ToString(),
                EndTime = info.EndTime.HasValue ? info.EndTime.ToString() : null,
                RunTime = info.RunTime,
                Status = info.Status,
                IsComplete = info.IsComplete,
                PrefixHex = info.PrefixHex,
                NextPrefixHex = info.NextPrefixHex
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Stop()
        {
            var info = MemCacheManager.Get<AzureThumbInfo>(AzureThumbInfoCache);
            info.Stop = true;

            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}