﻿
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Http;
using Telerik.Reporting.Services.WebApi;

namespace Qualanex.QoskCloud.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //RouteTable.Routes.MapHubs(new HubConfiguration {  = true });            
            ReportsControllerConfiguration.RegisterRoutes(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(ControlViewModel), new ControlModelBinder());
        }

      public void Session_OnStart()
      {
         Session["startTime"] = DateTime.Now;
      }
   }    
}
