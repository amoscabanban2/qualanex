﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UpaViewModel.cs">
///   Copyright (c) 2016 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Models for displaying the qosk page.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

using Qualanex.QoskCloud.Web.Areas.Sidebar.Models;

namespace Qualanex.QoskCloud.Web.ViewModels
{
   using System;
   using System.Collections.Generic;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Model.UPAModel;
   using Qualanex.QoskCloud.Web.Areas.Product.Models;
   using Areas.Product.ViewModels;
   using Areas.WrhsCtrl2.ViewModels;
   using Model;

   ///============================================================================
   /// <summary>
   ///   Enumeration to select which sections need to be updated
   /// </summary>
   ///----------------------------------------------------------------------------
   [Flags]
   public enum UpdateOptions
   {
      policy = 1 << 0,
      waste = 1 << 1,
      search = 1 << 2,
      productDetail = 1 << 3,
      notifications = 1 << 4
   }

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UpaViewModel
   {
      public UPAApplication UpaApplication { get; set; }
      public ProductDetailViewModel ProductDetail { get; set; } = new ProductDetailViewModel();
      public SelectListItem SelectedRx { get; set; } = new SelectListItem();
      public SearchResult SearchResults { get; set; } = new SearchResult();
      public WasteResult WasteResult { get; set; } = new WasteResult();
      public PolicyForm PolicyForm { get; set; } = new PolicyForm();
      public Notification Notification { get; set; } = new Notification();
      public HashSet<string> AdditionalSegments { get; set; } = new HashSet<string>();
      public List<ISidebarContainer> GrayInduction { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> BlueInduction { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> BlackSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> BlueSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> ClearSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> GreenSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> RedSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> WhiteSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> YellowSortBin { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> LargeGaylord { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> SmallGaylord { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> OrangeGaylord { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> WasteGaylord { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> RecallGaylord { get; set; } = new List<ISidebarContainer>();
      public List<ISidebarContainer> BlackGaylord { get; set; } = new List<ISidebarContainer>();
      public bool logout { get; set; } = false;
      public BinventoryViewModel Binventory { get; set; } = new BinventoryViewModel();
      public WrhsCtrl2ViewModel WrhsCtrl2 { get; set; } = new WrhsCtrl2ViewModel();
      public ContainerInstance CurrentInbound { get; set; } = new ContainerInstance();
   }
}
