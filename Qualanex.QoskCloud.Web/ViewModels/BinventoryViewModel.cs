﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace Qualanex.QoskCloud.Web.ViewModels
{
   public class BinventoryViewModel
   {
      /// <summary>
      /// List of objects whose properties will be used as the display elements in the binventory
      /// </summary>
      public IEnumerable<object> Rows { get; set; }

      /// <summary>
      /// Optional set of options for enhancing the display of the binventory
      /// </summary>
      public BinventoryOptions Options { get; set; } = new BinventoryOptions();

      /// <summary>
      /// Optional set of options for enhancing the display of the binventory
      /// </summary>
      public Boolean DoNotAudit { get; set; } = false;
   }

   /// <summary>
   /// A class for setting optional properties of a binventory
   /// </summary>
   public class BinventoryOptions
   {
      private List<string> _BaseProperties = new List<string>() {
               nameof(Model.BinventoryDataAccess.NDC),
               nameof(Model.BinventoryDataAccess.ItemGUID),
               nameof(Model.BinventoryDataAccess.StationId),
               nameof(Model.BinventoryDataAccess.Destination),
               nameof(Model.BinventoryDataAccess.OutboundContainerID)
         };
      /// Get Property of Row.
      /// If null or empty, add will be disabled.
      /// </summary>
      public object RowProperty { get; set; }

      /// <summary>
      /// List of properties to not display in the Hidden
      /// These will still be returned in update and delete calls
      /// making them useful for internal IDs
      /// </summary>
      public List<string> HiddenProperties { get; set; } = new List<string>();

      /// <summary>
      /// List of properties to not display in the Base Properties
      /// </summary>
      public List<string> BaseProperties
      {
         get { return _BaseProperties; }
         set { _BaseProperties.AddRange(value); }
      }

      /// <summary>
      /// List of properties that will remain readonly in views
      /// </summary>
      public List<string> ReadOnlyProperties { get; set; } = new List<string>();

      /// <summary>
      /// How many items we have.
      /// </summary>
      public float ItemCount { get; set; } = 0;

      /// <summary>
      /// All Items Count
      /// </summary>
      public float AllItemsCount { get; set; } = 0;

   }
}