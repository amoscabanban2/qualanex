///-----------------------------------------------------------------------------------------------
/// <copyright file="QoskViewModel.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Models for displaying the qosk page
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.ViewModels
{
   using System;
   using System.Data.SqlClient;
   using System.Linq;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public abstract class QoskViewModel
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Get the applet key (must match database definition).
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public abstract string AppletKey { get; }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public string GetTitle()
      {
         try
         {
            using (var cloudEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               return cloudEntities.Database.SqlQuery<string>("SELECT DisplayFormat FROM AppDiagram WHERE AppCode=@appCode",
                  new SqlParameter("@appCode", this.AppletKey)).FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return this.AppletKey;
      }

   }
}
