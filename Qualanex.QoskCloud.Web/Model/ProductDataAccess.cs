﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProductDataAccess.cs">
///   Copyright (c) 2015 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Core.Objects;
   using System.Data.Entity.Validation;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class ProductDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   Add Product in database.
      /// </summary>
      /// <param name="productModel"></param>
      /// <param name="actionBy"></param>
      /// <param name="prodID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveProduct(ProductsML productModel, string actionBy, out long prodID)
      {
         ParameterValidation.Begin().IsNotNull(() => productModel);

         //ProductsAudit prdAuditItem = new ProductsAudit();

         try
         {
            productModel.CreatedBy = actionBy;

            using (var cloudEntities = new QoskCloud())
            {
               var prdItem = new Product
               {
               RollupProductID = productModel.RollupProductID,
               ProfileCode = productModel.ProfileCode,
               //MFGLabelerCode = productModel.MFGLabelerCode,
               NDC = productModel.NDC,
                  NDCUPCWithDashes = productModel.NDCUPCWithDashes,
               UPC = productModel.UPC,
               MFGProductNumber = productModel.MFGProductNumber,
               PhysicianSample = productModel.PhysicianSample,
               PhysicianSampleProductNumber = productModel.PhysicianSampleProductNumber,
               //CaseSize1 = productModel.CaseSize1,
               //CaseSize2 = productModel.CaseSize2,
               //CaseSize3 = productModel.CaseSize3,
               UnitOfMeasure = productModel.UnitOfMeasure,
               Description = productModel.Description,
               //DosageCode = productModel.DosageCode,
               ControlNumber = productModel.ControlNumber,
               IndividualCountWeight = productModel.IndividualCountWeight,
               ContainerWeight = productModel.ContainerWeight,
               FullContainerWeightWithContents = productModel.FullContainerWeightWithContents,
               Strength = productModel.Strength,
               UnitDose = productModel.UnitDose,
               UnitsPerPackage = productModel.UnitsPerPackage,
               PackageSize = productModel.PackageSize,
               RXorOTC = productModel.RXorOTC,
               //ShortSupply = productModel.ShortSupply,
               TamperResistantSealExists = productModel.TamperResistantSealExists,
               SpecialHandlingInstructions = productModel.SpecialHandlingInstructions,
               DEAWatchList = productModel.DEAWatchList,
               ARCOSReportable = productModel.ARCOSReportable,
               Withdrawn = productModel.Withdrawn,
               ExcludeFromExternalInterface = productModel.ExcludeFromExternalInterface,
               ExcludeFromDropDown = productModel.ExcludeFromDropDown,
               AlwaysSortToQuarantine = productModel.AlwaysSortToQuarantine,
               ForceQuantityCount = productModel.ForceQuantityCount,
               ForceQuantityCountNotes = productModel.ForceQuantityCountNotes,
               //IsRepackager = productModel.IsRepackager,
               //DiverseManufacturerProfileCode = productModel.DiverseManufacturerProfileCode,
               CreatedBy = productModel.CreatedBy,
               CreatedDate = DateTime.UtcNow,
               Refrigerated = productModel.Refrigerated,
               //DivestedByLot = productModel.DivestedByLot,
               //WholesalerNumber = productModel.WholesalerNumber,
               //EDINumber = productModel.EDINumber,
               //IsOriginal = true,
               Version = 0,
               //PillType = prouctModel.PillType
            };

               cloudEntities.Product.Add(prdItem);

               if (cloudEntities.SaveChanges() > 0)
               {
                  prodID = prdItem.ProductID;
                  return true;
               }

               prodID = 0;
               return false;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);

            prodID = 0;
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);

            prodID = 0;
            return false;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update the Product using conditional Switch.
      /// </summary>
      /// <param name="productModel"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdateProduct(ProductsML productModel, string actionBy)
      {
         productModel.CreatedBy = actionBy;

         switch (productModel.SectionNumber)
         {
            case 1:
               return UpdateProductDescriptionSection(productModel);

            case 2:
               return UpdatePhysicianSection(productModel);

            case 3:
               return UpdateAdditionalDescription(productModel);
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<string> BindColumnListAvailableUsers(long productId)
      {
         List<string> result = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //result = (from usr in cloudEntities.ProductsAudit
               //          where usr.ProductID == productId
               //          select usr.ActionBy).Distinct().ToList();
            }

            return result;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update the ProductDescription based on productID.
      /// </summary>
      /// <param name="productModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response UpdateProductDescriptionSection(ProductsML productModel)
      {
         //ProductsAudit prdAuditItem = null;
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  var prdItem = cloudEntities.Product.Single(p => p.ProductID == productModel.ProductID);

                  if (prdItem.IsDeleted != true)
                  {
                     if (prdItem.Version != productModel.Version)
                     {
                        //prdItem.Profile = new Profile();

                        objclsResponse.Status = Status.VersionChange;
                        objclsResponse.response = prdItem;

                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        //prdAuditItem = new ProductsAudit();
                        prdItem.ProfileCode = productModel.ProfileCode;
                        prdItem.Strength = productModel.Strength;
                        prdItem.PackageSize = productModel.PackageSize;
                        prdItem.UnitsPerPackage = productModel.UnitsPerPackage;
                        prdItem.NDC = productModel.NDC;
                        prdItem.NDCUPCWithDashes = productModel.NDCUPCWithDashes;
                        prdItem.UPC = productModel.UPC;
                        prdItem.UnitOfMeasure = productModel.UnitOfMeasure;
                        prdItem.Description = productModel.Description.Trim();
                        prdItem.RXorOTC = productModel.RXorOTC;
                        //prdItem.DosageCode = prouctModel.DosageCode;
                        prdItem.ControlNumber = productModel.ControlNumber;
                        prdItem.ARCOSReportable = productModel.ARCOSReportable;
                        prdItem.ModifiedDate = DateTime.UtcNow;
                        prdItem.ModifiedBy = productModel.ModifiedBy;
                        //prdItem.PillType = prouctModel.PillType;

                        // Add version
                        //prdItem.Version = prouctModel.Version + 1;

                        // Update product Audit table
                        //ProductsAudit PrdAudit = MapObject(prdAuditItem, prdItem);
                        //PrdAudit.ActionBy = prouctModel.ModifiedBy;

                        //cloudEntities.ProductsAudit.Add(PrdAudit);

                        // Add update records to change tracker table.
                        //var objSaveTracker = new SaveChangeTracker(cloudEntities, Convert.ToString(productModel.ProductID));

                        //objSaveTracker.AddChangeTrackingToDB(objSaveTracker.Update(prdItem, null));

                        if (cloudEntities.SaveChanges() > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = prdItem.Version;
                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Rollback;
                           dbContextTransaction.Rollback();
                        }
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update physician  Details in Database based on productID.
      /// </summary>
      /// <param name="prouctModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response UpdatePhysicianSection(ProductsML prouctModel)
      {
         //ProductsAudit prdAuditItem = new ProductsAudit();
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  var prdItem = cloudEntities.Product.Single(p => p.ProductID == prouctModel.ProductID);

                  if (prdItem.IsDeleted != true)
                  {
                     if (prdItem.Version != prouctModel.Version)
                     {
                        //prdItem.Profile = new Profile();

                        objclsResponse.Status = Status.VersionChange;
                        objclsResponse.response = prdItem;

                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        prdItem.PhysicianSample = prouctModel.PhysicianSample;
                        prdItem.PhysicianSampleProductNumber = prouctModel.PhysicianSampleProductNumber;
                        prdItem.TamperResistantSealExists = prouctModel.TamperResistantSealExists;
                        prdItem.DEAWatchList = prouctModel.DEAWatchList;
                        prdItem.Withdrawn = prouctModel.Withdrawn;
                        prdItem.IndividualCountWeight = prouctModel.IndividualCountWeight;
                        prdItem.ContainerWeight = prouctModel.ContainerWeight;
                        prdItem.FullContainerWeightWithContents = prouctModel.FullContainerWeightWithContents;
                        prdItem.UnitDose = prouctModel.UnitDose;
                        //prdItem.CaseSize1 = prouctModel.CaseSize1;
                        //prdItem.CaseSize2 = prouctModel.CaseSize2;
                        //prdItem.CaseSize3 = prouctModel.CaseSize3;
                        prdItem.ModifiedBy = prouctModel.ModifiedBy;
                        prdItem.ModifiedDate = DateTime.UtcNow;

                        // Add version
                        //prdItem.Version = prouctModel.Version + 1;

                        // Update Audit
                        //ProductsAudit PrdAudit = MapObject(prdAuditItem, prdItem);
                        //cloudModelEntities.ProductsAudit.Add(PrdAudit);

                        // Modify change tracker for showing updated records
                        //var objSaveTracker = new SaveChangeTracker(cloudEntities, Convert.ToString(prouctModel.ProductID));

                        //objSaveTracker.AddChangeTrackingToDB(objSaveTracker.Update(prdItem, null));

                        if (cloudEntities.SaveChanges() > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = prdItem.Version;
                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Rollback;
                           dbContextTransaction.Rollback();
                        }
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update Additional Description based on productID.
      /// </summary>
      /// <param name="productModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response UpdateAdditionalDescription(ProductsML productModel)
      {
         Product prdItem = null;
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  prdItem = cloudEntities.Product.Single(p => p.ProductID == productModel.ProductID);

                  if (prdItem.IsDeleted != true)
                  {
                     if (prdItem.Version != productModel.Version)
                     {
                        objclsResponse.Status = Status.VersionChange;
                        //prdItem.Profile = new Profile();
                        objclsResponse.response = prdItem;
                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        prdItem.MFGProductNumber = productModel.MFGProductNumber;
                        prdItem.RollupProductID = productModel.RollupProductID;
                        //prdItem.EDINumber = prouctModel.EDINumber;
                        //prdItem.WholesalerNumber = prouctModel.WholesalerNumber;
                        prdItem.Refrigerated = productModel.Refrigerated;
                        //prdItem.DivestedByLot = prouctModel.DivestedByLot;
                        prdItem.ExcludeFromExternalInterface = productModel.ExcludeFromExternalInterface;
                        prdItem.ExcludeFromDropDown = productModel.ExcludeFromDropDown;
                        prdItem.AlwaysSortToQuarantine = productModel.AlwaysSortToQuarantine;
                        prdItem.ForceQuantityCount = productModel.ForceQuantityCount;
                        prdItem.ForceQuantityCountNotes = productModel.ForceQuantityCountNotes;
                        prdItem.SpecialHandlingInstructions = productModel.SpecialHandlingInstructions;
                        prdItem.ModifiedDate = DateTime.UtcNow;
                        prdItem.ModifiedBy = productModel.ModifiedBy;

                        // Add Version
                        prdItem.Version = (int)productModel.Version + 1;

                        // Update Audit table
                        //ProductsAudit PrdAudit = MapObject(prdAuditItem, prdItem);
                        //cloudModelEntities.ProductsAudit.Add(PrdAudit);

                        // Update Change tracker table
                        //var objSaveTracker = new SaveChangeTracker(cloudEntities, Convert.ToString(productModel.ProductID));

                        //objSaveTracker.AddChangeTrackingToDB(objSaveTracker.Update(prdItem, null));

                        if (cloudEntities.SaveChanges() > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = prdItem.Version;
                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Rollback;
                           dbContextTransaction.Rollback();
                        }
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   MapObject method used to map product table data to product audit table
      /// </summary>
      /// <param name="prdItem"></param>
      /// <param name="prouctModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      //private ProductsAudit MapObject(ProductsAudit prdItem, Products prouctModel)
      //{
      //   prdItem.ProductID = prouctModel.ProductID;
      //   prdItem.ProfileCode = prouctModel.ProfileCode;
      //   prdItem.Strength = prouctModel.Strength;
      //   prdItem.PackageSize = prouctModel.PackageSize;
      //   prdItem.UnitsPerPackage = prouctModel.UnitsPerPackage;
      //   prdItem.NDC = prouctModel.NDC;
      //   prdItem.NDCUPCWithDashes = prouctModel.NDCUPCWithDashes;
      //   prdItem.UPC = prouctModel.UPC;
      //   prdItem.UnitOfMeasure = prouctModel.UnitOfMeasure;
      //   prdItem.Description = prouctModel.Description;
      //   prdItem.RXorOTC = prouctModel.RXorOTC;
      //   prdItem.DosageCode = prouctModel.DosageCode;
      //   prdItem.ControlNumber = prouctModel.ControlNumber;
      //   prdItem.ARCOSReportable = prouctModel.ARCOSReportable;
      //   prdItem.PhysicianSample = prouctModel.PhysicianSample;
      //   prdItem.PhysicianSampleProductNumber = prouctModel.PhysicianSampleProductNumber;
      //   prdItem.TamperResistantSealExists = prouctModel.TamperResistantSealExists;
      //   prdItem.DEAWatchList = prouctModel.DEAWatchList;
      //   prdItem.Withdrawn = prouctModel.Withdrawn;
      //   prdItem.IndividualCountWeight = prouctModel.IndividualCountWeight;
      //   prdItem.ContainerWeight = prouctModel.ContainerWeight;
      //   prdItem.FullContainerWeightWithContents = prouctModel.FullContainerWeightWithContents;
      //   prdItem.UnitDose = prouctModel.UnitDose;
      //   prdItem.CaseSize1 = prouctModel.CaseSize1;
      //   prdItem.CaseSize2 = prouctModel.CaseSize2;
      //   prdItem.CaseSize3 = prouctModel.CaseSize3;
      //   prdItem.MFGProductNumber = prouctModel.MFGProductNumber;
      //   prdItem.RollupProductID = prouctModel.RollupProductID;
      //   prdItem.EDINumber = prouctModel.EDINumber;
      //   prdItem.WholesalerNumber = prouctModel.WholesalerNumber;
      //   prdItem.Refrigerated = prouctModel.Refrigerated;
      //   prdItem.DivestedByLot = prouctModel.DivestedByLot;
      //   prdItem.ExcludeFromExternalInterface = prouctModel.ExcludeFromExternalInterface;
      //   prdItem.ExcludeFromDropDown = prouctModel.ExcludeFromDropDown;
      //   prdItem.AlwaysSortToQuarantine = prouctModel.AlwaysSortToQuarantine;
      //   prdItem.ForceQuantityCount = prouctModel.ForceQuantityCount;
      //   prdItem.ForceQuantityCountNotes = prouctModel.ForceQuantityCountNotes;
      //   prdItem.SpecialHandlingInstructions = prouctModel.SpecialHandlingInstructions;
      //   prdItem.ModifiedBy = prouctModel.ModifiedBy;
      //   prdItem.ModifiedDate = DateTime.UtcNow;
      //   prdItem.Action = Constants.Model_PolicyDataAccess_Update;
      //   prdItem.ActionDate = DateTime.UtcNow;
      //   prdItem.Version = Convert.ToInt64(prouctModel.Version);

      //   return prdItem;
      //}

      ///****************************************************************************
      /// <summary>
      ///   Map wholesaler number class properties to wholesaler number audit table.
      /// </summary>
      /// <param name="wholesalerNumberAudit"></param>
      /// <param name="wholesalerNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      //private WholesalerNumberAudit MapObject(WholesalerNumberAudit wholesalerNumberAudit, WholesalerNumber wholesalerNumber)
      //{
      //   wholesalerNumberAudit.WholesalerNumberID = wholesalerNumber.WholesalerNumberID;
      //   wholesalerNumberAudit.ProductID = wholesalerNumber.ProductID;
      //   wholesalerNumberAudit.ProfileCode = wholesalerNumber.ProfileCode;
      //   wholesalerNumberAudit.NDCUPCWithDashes = wholesalerNumber.NDCUPCWithDashes;
      //   wholesalerNumberAudit.Number = wholesalerNumber.Number;
      //   wholesalerNumberAudit.Version = wholesalerNumber.Version;
      //   wholesalerNumberAudit.ModifiedBy = wholesalerNumber.ModifiedBy;
      //   wholesalerNumberAudit.ModifiedDate = DateTime.UtcNow;
      //   return wholesalerNumberAudit;
      //}

      ///****************************************************************************
      /// <summary>
      ///   Set the status true false in iSdeleted  column that mean used to remove product
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DeleteProduct(long productID, string actionBy)
      {
         //ProductsAudit objProductAudit = new ProductsAudit();
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var prdItem = (from prdt in cloudEntities.Product
                                   where prdt.ProductID == productID
                                   select prdt).SingleOrDefault();

               if ((from prd in cloudEntities.Item where prd.ProductID == productID select prd).Any())
               {
                  objclsResponse.Status = Status.Fail;
                  //objclsResponse.Message = Product.ProductCanNotDelete;
                  return objclsResponse;
               }

               if (prdItem != null && prdItem.IsDeleted != true)
               {
                  prdItem.IsDeleted = true;
                  //var mapResult = MapObject(objProductAudit, prdItem);
                  //mapResult.Action = Constants.Model_PolicyDataAccess_Delete;
                  //mapResult.ActionBy = Actionby;
                  //mapResult.ActionDate = DateTime.UtcNow;
                  //cloudModelEntities.ProductsAudit.Add(mapResult);
                  objclsResponse.Status = cloudEntities.SaveChanges() > 0
                                                   ? Status.OK
                                                   : Status.Fail;
               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      /// Delete Product Image
      /// </summary>
      /// <param name="productimageID"></param>
      /// <param name="orginal"></param>
      /// <param name="content"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool DeleteProductImage(long productimageID, string orginal, string content)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var prdItem = (from prdt in cloudEntities.ProductImage
                                             where prdt.ProductImageID == productimageID
                                             select prdt).SingleOrDefault();

               if (!string.IsNullOrEmpty(content))
               {
                  //prdItem.ContentFileName = null;
               }
               if (!string.IsNullOrEmpty(orginal) && !string.IsNullOrEmpty(content))
               {
                  prdItem.IsDeleted = true;
               }

               int DeletedCount = cloudEntities.SaveChanges();
               if (DeletedCount > 0)
                  return true;

               return false;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// GetProductByID methods Used to Get Product Details based on Product ID
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ProductsML GetProductByID(long productID)
      {
         var objProductsML = new ProductsML();
         var lstImage = new List<ImageDetails>();

         using (var cloudEntities = new QoskCloud())
         {
            lstImage = (from imag in cloudEntities.ProductImage
                        join prd in cloudEntities.Product on imag.ProductID equals prd.ProductID
                        //where (imag.ProductID == ProductID && (imag.PackagingOrContainerType.Equals(Constants.ProductController_Original) || imag.PackagingOrContainerType.Equals(Constants.ProductController_Repackager)) && (imag.IsDeleted == false || imag.IsDeleted == null))
                        select new ImageDetails
                        {
                           ProductImageDetailID = imag.ProductImageID,
                           Image = imag.FileName,
                           //ContentImage = imag.ContentFileName,
                           //Container = imag.PackagingOrContainerType,
                           NDCforImage = prd.NDC,
                           NDCUPCWithDashesforImage = prd.NDCUPCWithDashes,
                           DescriptionforImage = prd.Description,
                           ProductID = imag.ProductID,
                           Version = imag.Version
                        }).ToList();
         }

         using (var cloudEntities = new QoskCloud())
         {
            objProductsML = (from prdt in cloudEntities.Product
                             join prf in cloudEntities.Profile on prdt.ProfileCode equals prf.ProfileCode
                             //join prfd in cloudModelEntities.Profile on prdt.DiverseManufacturerProfileCode equals prfd.ProfileCode into divest from divestedProd in divest.DefaultIfEmpty()
                             where prdt.ProductID == productID
                             select new ProductsML
                             {
                                ProductID = prdt.ProductID,
                                //ProfileTypeName = prf.Type,
                                RollupProductID = prdt.RollupProductID,
                                ProfileCode = prdt.ProfileCode,
                                ModifiedBy = prdt.ModifiedBy,
                                //MFGLabelerCode = prdt.MFGLabelerCode,
                                NDC = prdt.NDC,
                                NDCforImageTab = prdt.NDC,
                                NDCUPCWithDashesforImageTab = prdt.NDCUPCWithDashes.Trim(),
                                NDCUPCWithDashes = prdt.NDCUPCWithDashes.Trim(),
                                UPC = prdt.UPC,
                                MFGProductNumber = prdt.MFGProductNumber,
                                PhysicianSample = prdt.PhysicianSample,
                                PhysicianSampleProductNumber = prdt.PhysicianSampleProductNumber,
                                //CaseSize1 = prdt.CaseSize1,
                                //CaseSize2 = prdt.CaseSize2,
                                //CaseSize3 = prdt.CaseSize3,
                                PackageSize = prdt.PackageSize,
                                UnitOfMeasure = prdt.UnitOfMeasure,
                                Description = prdt.Description,
                                DescriptionforImageTab = prdt.Description,
                                //DosageCode = prdt.DosageCode,
                                ControlNumber = prdt.ControlNumber,
                                IndividualCountWeight = prdt.IndividualCountWeight,
                                ContainerWeight = prdt.ContainerWeight,
                                FullContainerWeightWithContents = prdt.FullContainerWeightWithContents,
                                Strength = prdt.Strength,
                                UnitDose = prdt.UnitDose,
                                UnitsPerPackage = prdt.UnitsPerPackage,
                                RXorOTC = prdt.RXorOTC,
                                BrandorGeneric = prdt.BrandorGeneric,
                                TherapeuticClass = prdt.TherapeuticClass,
                                //ShortSupply = prdt.ShortSupply,
                                TamperResistantSealExists = prdt.TamperResistantSealExists,
                                SpecialHandlingInstructions = prdt.SpecialHandlingInstructions,
                                DEAWatchList = prdt.DEAWatchList,
                                ARCOSReportable = prdt.ARCOSReportable,
                                Withdrawn = prdt.Withdrawn,
                                ExcludeFromExternalInterface = prdt.ExcludeFromExternalInterface,
                                ExcludeFromDropDown = prdt.ExcludeFromDropDown,
                                AlwaysSortToQuarantine = prdt.AlwaysSortToQuarantine,
                                ForceQuantityCount = prdt.ForceQuantityCount,
                                ForceQuantityCountNotes = prdt.ForceQuantityCountNotes,
                                //IsRepackager = prdt.IsRepackager,
                                //DiverseManufacturerProfileCode = prdt.DiverseManufacturerProfileCode,
                                //DiverseManufacturerName = divestedProd.Name,
                                Refrigerated = prdt.Refrigerated,
                                //DivestedByLot = prdt.DivestedByLot,
                                //DivestProductID = prdt.DivestProductID,
                                //WholesalerNumber = prdt.WholesalerNumber,
                                //EDINumber = prdt.EDINumber,
                                ManufactureRepackagerName = prf.Name,
                                ModifiedDate = prdt.ModifiedDate ?? DateTime.MinValue,
                                Version = prdt.Version,
                                //IsOriginal = prdt.IsOriginal,
                                //PillType = prdt.PillType
                             }).FirstOrDefault();

            if (objProductsML.DivestedByLot != null && objProductsML.DivestedByLot == true && objProductsML.DiverseManufacturerProfileCode != null)
            {
               var lstManufactureDetails = GetOwenerShipManufacureNameNDCByLot(productID);

               if (!string.IsNullOrWhiteSpace(lstManufactureDetails))
               {
                  objProductsML.lstOwnerShipManufacture = lstManufactureDetails;
               }
            }

            objProductsML.lstProductImage = new List<ImageDetails>();
            objProductsML.diverseLotNumbersDetails = LotNumbersDataAccess.GetDivestedLotNumber(productID);
            objProductsML.lstProductImage = lstImage.OrderBy(p => p.Container).ToList();
            objProductsML.PackagingOrContainerType = lstImage.Select(p => p.Container).FirstOrDefault();
         }

         return objProductsML;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all divested owner name for product
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetOwenerShipManufacureNameNDCByLot(long productID)
      {
         var DivestedOwnerName = string.Empty;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //return DivestedOwnerName = (from lot in cloudEntities.Lot
               //                            join prf in cloudEntities.Profile on lot.DiverseManufacturerProfileCode equals prf.ProfileCode
               //                            where lot.ProductID == productID
               //                            select prf.Name).Distinct().FirstOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return DivestedOwnerName;
      }

      ///****************************************************************************
      /// <summary>
      /// GeDosageList Used to get Dosage list from Database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Entity.Dosage> GetDosageList()
      {
         List<Entity.Dosage> objDosage = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //    objDosage = (from dosage in cloudEntities.Dosages
               //                 select new Dosage
               //                 {
               //                     Code = dosage.DosageCode,
               //                     Description = dosage.Description,
               //                 }).OrderBy(pp => pp.Description).ToList();
            }

            return objDosage;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Insert image details into database
      /// </summary>
      /// <param name="productModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ImageDetails InsertProductImage(ProductsML productModel)
      {
         ImageDetails objProductImageDetail = null;
         //ProductImageDetailAudit objProductImageDetailAudit = new ProductImageDetailAudit();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  int productID = 0;
                  if (productModel.ProductID == 0)
                  {
                     productID = (from p in cloudEntities.Product
                                  where p.NDCUPCWithDashes.Equals(productModel.NDCUPCWithDashesforImageTab)
                                          || p.Description.Equals(productModel.DescriptionforImageTab)
                                  select p.ProductID).FirstOrDefault();
                  }
                  else
                  {
                     productID = productModel.ProductID;
                  }

                  var prdItem = new ProductImage();
                  prdItem.ProductID = Convert.ToInt32(productID);
                  //prdItem.ProfileCode = productModel.ProfileCode;
                  //prdItem.PackagingOrContainerType = productModel.PackagingOrContainerType;
                  //prdItem.ContentFileName = productModel.ContentImage;
                  prdItem.FileName = productModel.Image;
                  //prdItem.ConditionID = Constants.Model_PolicyDataAccess_PolicyTypeOne;
                  prdItem.Version = 0;
                  prdItem.CreatedDate = DateTime.UtcNow;
                  prdItem.CreatedBy = prdItem.CreatedBy;
                  cloudEntities.ProductImage.Add(prdItem);
                  int result = cloudEntities.SaveChanges();
                  long lastInsertedID = prdItem.ProductImageID;
                  if (result > 0)
                  {
                     var imageProduct = cloudEntities.ProductImage.Single(u => u.ProductImageID == lastInsertedID);
                     imageProduct.FileName = lastInsertedID + "_" + productModel.Image;
                     if (!string.IsNullOrEmpty(productModel.ContentImage))
                     {
                        //imageProduct.ContentFileName = lastInsertedID + "_" + productModel.ContentImage;
                     }
                     result = cloudEntities.SaveChanges();
                     if (result > 0)
                     {

                        objProductImageDetail = new ImageDetails();
                        objProductImageDetail.ProductImageDetailID = lastInsertedID;
                        //ProductImageDetailAudit objproductImageDetailAudit = MapProductImage(objProductImageDetailAudit, prdItem);
                        //objproductImageDetailAudit.ProductImageDetailID = lastInsertedID;
                        //objproductImageDetailAudit.Action = Constants.Model_PolicyDataAccess_Insert;
                        //objproductImageDetailAudit.ActionBy = prdItem.ModifiedBy;
                        //objproductImageDetailAudit.ActionDate = DateTime.UtcNow;
                        //cloudModelEntities.ProductImageDetailAudit.Add(objproductImageDetailAudit);
                        int auditresult = cloudEntities.SaveChanges();
                        if (auditresult > 0)
                        {
                           dbContextTransaction.Commit();
                           return objProductImageDetail;
                        }
                     }
                     else
                     {
                        dbContextTransaction.Rollback();
                        return objProductImageDetail;
                     }
                  }
                  else
                  {
                     dbContextTransaction.Rollback();
                     return objProductImageDetail;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objProductImageDetail;
      }

      ///****************************************************************************
      /// <summary>
      ///  UpdateProductImage methods used to Update Image Details into Database
      /// </summary>
      /// <param name="productModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdateProductImage(ProductsML productModel)
      {
         //ProductImageDetailAudit objProductImageDetailAudit = new ProductImageDetailAudit();
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  var prdItem = cloudEntities.ProductImage.Single(p => p.ProductImageID == productModel.HiddenProductImageDetailID);
                  var objProducts = cloudEntities.Product.Single(p => p.ProductID == productModel.ProductID);

                  if (objProducts.IsDeleted != true)
                  {
                     if (objProducts.Version != productModel.Version)
                     {
                        objclsResponse.Status = Status.VersionChange;
                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        objclsResponse.response = GenericUtility.MapObject(prdItem, new ProductImage());

                        //prdItem.PackagingOrContainerType = productModel.PackagingOrContainerType;

                        if (!string.IsNullOrWhiteSpace(productModel.Image))
                        {
                           prdItem.FileName = productModel.HiddenProductImageDetailID + "_" + productModel.Image;
                        }

                        if (!string.IsNullOrWhiteSpace(productModel.ContentImage))
                        {
                           //prdItem.ContentFileName = productModel.HiddenProductImageDetailID + "_" + productModel.ContentImage;
                        }

                        //prdItem.ConditionID = Constants.Model_PolicyDataAccess_PolicyTypeOne;
                        prdItem.CreatedDate = DateTime.UtcNow;
                        prdItem.ModifiedBy = productModel.ModifiedBy;

                        objProducts.Version = objProducts.Version + 1;

                        //ProductImageDetailAudit objproductImageDetailAudit = MapProductImage(objProductImageDetailAudit, prdItem);
                        //objproductImageDetailAudit.Action = Constants.Model_PolicyDataAccess_Update;
                        //objproductImageDetailAudit.ActionBy = prdItem.CreatedBy;
                        //objproductImageDetailAudit.ActionDate = DateTime.UtcNow;
                        //cloudModelEntities.ProductImageDetailAudit.Add(objproductImageDetailAudit);
                        int result = cloudEntities.SaveChanges();
                        if (result > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = objProducts.Version;
                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Fail;
                           dbContextTransaction.Rollback();
                        }
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Wholesaler Name and Number
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfileRequest> GetWholesalerNumber()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from profile in cloudEntities.Profile
                       where profile.WholesalerAccountNumber != null
                             && profile.WholesalerAccountNumber != string.Empty
                             && profile.Name != string.Empty
                       select new ProfileRequest
                       {
                          Name = profile.Name + " " + profile.WholesalerAccountNumber,
                          WholesalerAccountNumber = profile.WholesalerAccountNumber,
                       }).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="objProduct"></param>
      /// <param name="ProductsSearch"></param>
      /// <param name="LotNumbersSearch"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProductsML> ListAllProductssforserver(RequestGetProduct objProduct, List<FilterClass> ProductsSearch, List<FilterClass> LotNumbersSearch, int skipFrom, int takeTo, System.Collections.Specialized.NameValueCollection query, string pageType, string sortdatafield, string sortorder, out int totalRecord)
      {
         ICollection<ProductsML> lsProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {

               var whereConditions = CommonDataAccessLayer.BuildQueryProduct(query, ProductsSearch, LotNumbersSearch);
               var objParam = new ObjectParameter(Constants.Model_ProductDataAccess_TotalRecord, typeof(Int32));

               //var dbResult = cloudModelEntities.USP_ProductSearch(pageType, whereConditions, skipFrom, takeTo, sortdatafield, sortorder, objParam);

               //var result = (from prdt in dbResult
               //              select new ProductsML
               //              {
               //                 Name = prdt.Name,
               //                 ProfileTypeName = prdt.ProfileTypeName,
               //                 ProductID = prdt.ProductID,
               //                 RollupProductID = prdt.RollupProductID,
               //                 ProfileCode = prdt.ProfileCode,
               //                 ProductStartDate = prdt.ProductStartDate,
               //                 ProductEndDate = prdt.ProductEndDate,
               //                 ModifiedBy = prdt.ModifiedBy,
               //                 MFGLabelerCode = prdt.MFGLabelerCode,
               //                 NDC = prdt.NDC,
               //                 NDCUPCWithDashes = prdt.NDCUPCWithDashes,
               //                 UPC = prdt.UPC,
               //                 MFGProductNumber = prdt.MFGProductNumber,
               //                 PhysicianSample = prdt.PhysicianSample,
               //                 PhysicianSampleProductNumber = prdt.PhysicianSampleProductNumber,
               //                 CaseSize1 = prdt.CaseSize1,
               //                 CaseSize2 = prdt.CaseSize2,
               //                 CaseSize3 = prdt.CaseSize3,
               //                 PackageSize = prdt.PackageSize,
               //                 UnitsPerPackage = prdt.UnitsPerPackage,
               //                 UnitOfMeasure = prdt.UnitOfMeasure,
               //                 Description = prdt.Description,
               //                 DosageCode = prdt.DosageCode,
               //                 ControlNumber = prdt.ControlNumber,
               //                 IndividualCountWeight = prdt.IndividualCountWeight,
               //                 ContainerWeight = prdt.ContainerWeight,
               //                 FullContainerWeightWithContents = prdt.FullContainerWeightWithContents,
               //                 Strength = prdt.Strength,
               //                 UnitDose = prdt.UnitDose,
               //                 RXorOTC = prdt.RXorOTC,
               //                 BrandorGeneric = prdt.BrandorGeneric,
               //                 TherapeuticClass = prdt.TherapeuticClass,
               //                 ShortSupply = prdt.ShortSupply,
               //                 TamperResistantSealExists = prdt.TamperResistantSealExists,
               //                 SpecialHandlingInstructions = prdt.SpecialHandlingInstructions,
               //                 DEAWatchList = prdt.DEAWatchList,
               //                 ARCOSReportable = prdt.ARCOSReportable,
               //                 Withdrawn = prdt.Withdrawn,
               //                 ExcludeFromExternalInterface = prdt.ExcludeFromExternalInterface,
               //                 ExcludeFromDropDown = prdt.ExcludeFromDropDown,
               //                 AlwaysSortToQuarantine = prdt.AlwaysSortToQuarantine,
               //                 ForceQuantityCount = prdt.ForceQuantityCount,
               //                 ForceQuantityCountNotes = prdt.ForceQuantityCountNotes,
               //                 IsRepackager = prdt.IsRepackager,
               //                 DiverseManufacturerProfileCode = prdt.DiverseManufacturerProfileCode,
               //                 Refrigerated = prdt.Refrigerated,
               //                 DivestedByLot = prdt.DivestedByLot,
               //                 WholesalerNumber = prdt.WholesalerNumber,
               //                 EDINumber = prdt.EDINumber,
               //                 LotNumber = prdt.LotNumber,
               //                 DosagesDescription = prdt.DosagesDescription,
               //                 DivestProductID = prdt.DivestProductID
               //              });

               //lsProfiles = result.ToList();

               totalRecord = Convert.ToInt32(objParam.Value);
               return lsProfiles;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="From"></param>
      /// <param name="To"></param>
      /// <param name="ColumnNM"></param>
      /// <param name="UserNM"></param>
      /// <param name="skipFrom"></param>
      /// <param name="takeTo"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="totalRecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProductsML> BindProductAudit(long productId, DateTime From, DateTime To, string ColumnNM, string UserNM, int skipFrom, int takeTo, string sortdatafield, string sortorder, out int totalRecord)
      {
         ICollection<ProductsML> lsProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var result = (from prdt in cloudModelEntities.ProductsAudit
               //              where prdt.ProductID == productId &&
               //                    prdt.ActionDate >= From &&
               //                    prdt.ActionDate <= To
               //              orderby prdt.ActionDate descending
               //              select new ProductsML
               //              {
               //                 ProductID = prdt.ProductID,
               //                 RollupProductID = prdt.RollupProductID,
               //                 ProfileCode = prdt.ProfileCode,
               //                 ProductStartDate = prdt.ProductStartDate,
               //                 ProductEndDate = prdt.ProductEndDate,
               //                 ModifiedBy = prdt.ModifiedBy,
               //                 ModifiedDate = prdt.ModifiedDate,
               //                 MFGLabelerCode = prdt.MFGLabelerCode,
               //                 NDC = prdt.NDC,
               //                 NDCUPCWithDashes = prdt.NDCUPCWithDashes,
               //                 UPC = prdt.UPC,
               //                 MFGProductNumber = prdt.MFGProductNumber,
               //                 PhysicianSample = prdt.PhysicianSample,
               //                 PhysicianSampleProductNumber = prdt.PhysicianSampleProductNumber,
               //                 CaseSize1 = prdt.CaseSize1,
               //                 CaseSize2 = prdt.CaseSize2,
               //                 CaseSize3 = prdt.CaseSize3,
               //                 PackageSize = prdt.PackageSize,
               //                 UnitsPerPackage = prdt.UnitsPerPackage,
               //                 UnitOfMeasure = prdt.UnitOfMeasure,
               //                 Description = prdt.Description,
               //                 DosageCode = prdt.DosageCode,
               //                 ControlNumber = prdt.ControlNumber,
               //                 IndividualCountWeight = prdt.IndividualCountWeight,
               //                 ContainerWeight = prdt.ContainerWeight,
               //                 FullContainerWeightWithContents = prdt.FullContainerWeightWithContents,
               //                 Strength = prdt.Strength,
               //                 UnitDose = prdt.UnitDose,
               //                 RXorOTC = prdt.RXorOTC,
               //                 BrandorGeneric = prdt.BrandorGeneric,
               //                 TherapeuticClass = prdt.TherapeuticClass,
               //                 ShortSupply = prdt.ShortSupply,
               //                 TamperResistantSealExists = prdt.TamperResistantSealExists,
               //                 SpecialHandlingInstructions = prdt.SpecialHandlingInstructions,
               //                 DEAWatchList = prdt.DEAWatchList,
               //                 ARCOSReportable = prdt.ARCOSReportable,
               //                 Withdrawn = prdt.Withdrawn,
               //                 ExcludeFromExternalInterface = prdt.ExcludeFromExternalInterface,
               //                 ExcludeFromDropDown = prdt.ExcludeFromDropDown,
               //                 AlwaysSortToQuarantine = prdt.AlwaysSortToQuarantine,
               //                 ForceQuantityCount = prdt.ForceQuantityCount,
               //                 ForceQuantityCountNotes = prdt.ForceQuantityCountNotes,
               //                 IsRepackager = prdt.IsRepackager,
               //                 DiverseManufacturerProfileCode = prdt.DiverseManufacturerProfileCode,
               //                 Refrigerated = prdt.Refrigerated,
               //                 DivestedByLot = prdt.DivestedByLot,
               //                 WholesalerNumber = prdt.WholesalerNumber,
               //                 EDINumber = prdt.EDINumber,
               //                 Version = prdt.Version,
               //              });

               //if (!string.IsNullOrEmpty(ColumnNM))
               //{
               //   string prdtid = Convert.ToString(productId);

               //var products = (from prdt in cloudModelEntities.ChangeTracker
               //                where prdt.ColumnName == ColumnNM && prdt.PrimayColumnId == prdtid
               //                select prdt.Version).ToList();

               //result = (from r in result where products.Contains(r.Version ?? -1) select r); //r.Version ?? -1, because assumping for null there will not be any matched rows and -1 not be any version number.
               //}

               //if (!string.IsNullOrEmpty(UserNM))
               //{
               //result = (from r in result where r.ModifiedBy == UserNM select r);
               //}

               totalRecord = 0; //result.Count();
               //lsProfiles = result.Skip(skipFrom).Take(takeTo).ToList();
            }

            return lsProfiles;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// This method used to get Lot detail based on specific and default search
      /// </summary>
      /// <param name="objProduct"></param>
      /// <param name="ProductsSearch"></param>
      /// <param name="LotNumbersSearch"></param>
      /// <param name="skipFrom"></param>
      /// <param name="takeTo"></param>
      /// <param name="query"></param>
      /// <param name="pageType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="totalRecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProductsML> ListAllLotWithProductssforserver(RequestGetProduct objProduct, List<FilterClass> ProductsSearch, List<FilterClass> LotNumbersSearch, int skipFrom, int takeTo, System.Collections.Specialized.NameValueCollection query, string pageType, string sortdatafield, string sortorder, out int totalRecord)
      {
         ICollection<ProductsML> lsProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var whereConditions = CommonDataAccessLayer.BuildQuery(query, ProductsSearch, LotNumbersSearch);
               var objParam = new ObjectParameter(Constants.Model_ProductDataAccess_TotalRecord, typeof(Int32));

               //var dbResult = cloudModelEntities.USP_LotSearch(pageType, whereConditions, skipFrom, takeTo, sortdatafield, sortorder, objParam);

               //var result = (from prdt in dbResult
               //              select new ProductsML
               //              {
               //                 Name = prdt.ProfileName,
               //                 ProductID = prdt.ProductID ?? 0,
               //                 RollupProductID = prdt.RollupProductID,
               //                 ProfileCode = prdt.ProfileCode ?? 0,
               //                 ProductStartDate = prdt.ProductStartDate,
               //                 ProductEndDate = prdt.ProductEndDate,
               //                 MFGLabelerCode = prdt.MFGLabelerCode,
               //                 NDC = prdt.NDC,
               //                 NDCUPCWithDashes = prdt.NDCUPCWithDashes,
               //                 UPC = prdt.UPC,
               //                 MFGProductNumber = prdt.MFGProductNumber,
               //                 PhysicianSample = prdt.PhysicianSample ?? false,
               //                 PhysicianSampleProductNumber = prdt.PhysicianSampleProductNumber,
               //                 CaseSize1 = prdt.CaseSize1,
               //                 CaseSize2 = prdt.CaseSize2,
               //                 CaseSize3 = prdt.CaseSize3,
               //                 PackageSize = prdt.PackageSize ?? 0,
               //                 UnitsPerPackage = prdt.UnitsPerPackage ?? 0,
               //                 UnitOfMeasure = prdt.UnitOfMeasure,
               //                 Description = prdt.Description,
               //                 DosageCode = prdt.DosageCode,
               //                 DosagesDescription = prdt.DosagesDescription,
               //                 ControlNumber = prdt.ControlNumber ?? 0,
               //                 IndividualCountWeight = prdt.IndividualCountWeight,
               //                 ContainerWeight = prdt.ContainerWeight,
               //                 FullContainerWeightWithContents = prdt.FullContainerWeightWithContents,
               //                 Strength = prdt.Strength,
               //                 UnitDose = prdt.UnitDose ?? false,
               //                 RXorOTC = prdt.RXorOTC ?? false,
               //                 BrandorGeneric = prdt.BrandorGeneric,
               //                 TherapeuticClass = prdt.TherapeuticClass,
               //                 ShortSupply = prdt.ShortSupply,
               //                 TamperResistantSealExists = prdt.TamperResistantSealExists ?? false,
               //                 SpecialHandlingInstructions = prdt.SpecialHandlingInstructions,
               //                 DEAWatchList = prdt.DEAWatchList ?? false,
               //                 ARCOSReportable = prdt.ARCOSReportable,
               //                 Withdrawn = prdt.Withdrawn,
               //                 ExcludeFromExternalInterface = prdt.ExcludeFromExternalInterface,
               //                 ExcludeFromDropDown = prdt.ExcludeFromDropDown,
               //                 AlwaysSortToQuarantine = prdt.AlwaysSortToQuarantine ?? false,
               //                 ForceQuantityCount = prdt.ForceQuantityCount ?? false,
               //                 ForceQuantityCountNotes = prdt.ForceQuantityCountNotes,
               //                 IsRepackager = prdt.IsRepackager,
               //                 DiverseManufacturerProfileCode = prdt.DiverseManufacturerProfileCode,
               //                 Refrigerated = prdt.Refrigerated,
               //                 DivestedByLot = prdt.DivestedByLot,
               //                 WholesalerNumber = prdt.WholesalerNumber,
               //                 EDINumber = prdt.EDINumber,
               //                 LotNumber = prdt.LotNumber,
               //                 RecallId = prdt.RecallId,
               //                 lotNumberID = prdt.LotNumbersId
               //              });

               //lsProfiles = result.ToList();

               totalRecord = Convert.ToInt32(objParam.Value);
               return lsProfiles;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

      }

      ///****************************************************************************
      /// <summary>
      ///   Check the existence of NDC With Dashes
      /// </summary>
      /// <param name="NDCUPCWithDashes"></param>
      /// <param name="PrdID"></param>
      /// <param name="ProfileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckNDCUPCWithDashesExist(string NDCUPCWithDashes, long PrdID, out int ProfileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var result = (from p in cloudEntities.Product
                             where p.NDCUPCWithDashes.Equals(NDCUPCWithDashes) && p.ProductID != PrdID
                             select p).ToList();

               if (result.Count > 0)
               {
                  ProfileCode = result[0].ProfileCode;
                  return result[0].Description.ToString();
               }

               ProfileCode = 0;
               return null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            ProfileCode = 0;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            ProfileCode = 0;
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the existence of NDC/UPC/NDC With Dashes and return the search type
      /// </summary>
      /// <param name="NDCUPCWithDashes"></param>
      /// <param name="PrdID"></param>
      /// <param name="Srchtype"></param>
      /// <param name="ProfileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckNDCUPCWithDashesExist(string NDCUPCWithDashes, long PrdID, int Srchtype, out int ProfileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (Srchtype)
               {
                  case 0:
                  {
                     var result = (from p in cloudEntities.Product
                                   where p.NDCUPCWithDashes.Equals(NDCUPCWithDashes) && p.ProductID != PrdID
                                   select p).ToList();

                     if (result.Count > 0)
                     {
                        ProfileCode = result[0].ProfileCode;
                        return result[0].Description;
                     }

                     ProfileCode = 0;
                     return null;
                  }

                  case 1:
                  {
                     var result = (from p in cloudEntities.Product
                                   where p.NDC == Convert.ToInt64(NDCUPCWithDashes) && p.ProductID != PrdID
                                   select p).ToList();

                     if (result.Count > 0)
                     {
                        ProfileCode = result[0].ProfileCode;
                        return result[0].Description;
                     }

                     ProfileCode = 0;
                     return null;
                  }

                  case 2:
                  {
                     var result = (from p in cloudEntities.Product
                                   where p.NDC == Convert.ToInt64(NDCUPCWithDashes) && p.ProductID != PrdID
                                   select p).ToList();

                     if (result.Count > 0)
                     {
                        ProfileCode = result[0].ProfileCode;
                        return result[0].Description.ToString();
                     }

                     ProfileCode = 0;
                     return null;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         ProfileCode = 0;
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the NDC existence.
      /// </summary>
      /// <param name="NDC"></param>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string CheckNDCExist(long NDC, long productID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var result = (from p in cloudEntities.Product
                             where p.NDC == NDC && p.ProductID != productID
                             select p.Description).ToList();

               return result.Count > 0
                           ? result[0]
                           : null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the Modified status details from change tracker table based on
      ///   search criteria.
      /// </summary>
      /// <param name="fromdate"></param>
      /// <param name="to"></param>
      /// <param name="prdID"></param>
      /// <param name="TableName"></param>
      /// <param name="ColumnName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ChangeTrackerSearch GetChangeTrackerDetails(DateTime? fromdate, DateTime? to, long prdID, string TableName, string ColumnName)
      {
         var lstresult = new List<ChangeTrackerEntity>();
         var objChangeTrackerSearch = new ChangeTrackerSearch();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (fromdate != null && to != null && !string.IsNullOrEmpty(TableName)
                   && !string.IsNullOrEmpty(ColumnName))
               {
                  //lstresult = (from p in cloudModelEntities.ChangeTracker
                  //             where p.ModifyDateTime >= fromdate && p.ModifyDateTime < to
                  //                   && p.TableName.Equals(TableName) && p.ColumnName.Equals(ColumnName)
                  //                   && p.PrimayColumnId == prdID.ToString()
                  //                   && p.ColumnName != Constants.Model_ProductDataAccess_ModifiedDate
                  //             select new ChangeTrackerEntity
                  //             {
                  //                TableName = p.TableName,
                  //                ColumnName = p.ColumnName,
                  //                OldValue = p.OldValue,
                  //                NewValue = p.NewValue,
                  //                ModifiedBy = p.ModifyBy,
                  //             }).ToList();
               }
               else if (fromdate != null && to != null && !string.IsNullOrEmpty(TableName)
                        && string.IsNullOrEmpty(ColumnName))
               {
                  //lstresult = (from p in cloudModelEntities.ChangeTracker
                  //             where p.ModifyDateTime >= fromdate && p.ModifyDateTime < to
                  //                   && p.TableName.Equals(TableName) && p.PrimayColumnId == prdID.ToString()
                  //                   && p.ColumnName != Constants.Model_ProductDataAccess_ModifiedDate
                  //             select new ChangeTrackerEntity
                  //             {
                  //                TableName = p.TableName,
                  //                ColumnName = p.ColumnName,
                  //                OldValue = p.OldValue,
                  //                NewValue = p.NewValue,
                  //                ModifiedBy = p.ModifyBy,
                  //                ModifiedDate = p.ModifyDateTime
                  //             }).ToList();
               }
               else if (fromdate != null && to != null && string.IsNullOrEmpty(TableName) && string.IsNullOrEmpty(ColumnName))
               {
                  //lstresult = (from p in cloudModelEntities.ChangeTracker
                  //             where p.ModifyDateTime >= fromdate && p.ModifyDateTime < to
                  //                   && p.PrimayColumnId == prdID.ToString()
                  //                   && p.ColumnName != Constants.Model_ProductDataAccess_ModifiedDate
                  //             select new ChangeTrackerEntity
                  //             {
                  //                TableName = p.TableName,
                  //                ColumnName = p.ColumnName,
                  //                OldValue = p.OldValue,
                  //                NewValue = p.NewValue,
                  //                ModifiedBy = p.ModifyBy,
                  //                ModifiedDate = p.ModifyDateTime
                  //             }).ToList();
               }
               else if (!string.IsNullOrEmpty(TableName) && string.IsNullOrEmpty(ColumnName))
               {
                  //lstresult = (from p in cloudModelEntities.ChangeTracker
                  //             where p.TableName.Equals(TableName) && p.PrimayColumnId == prdID.ToString() && p.ColumnName != Constants.Model_ProductDataAccess_ModifiedDate
                  //             select new ChangeTrackerEntity
                  //             {
                  //                TableName = p.TableName,
                  //                ColumnName = p.ColumnName,
                  //                OldValue = p.OldValue,
                  //                NewValue = p.NewValue,
                  //                ModifiedBy = p.ModifyBy,
                  //                ModifiedDate = p.ModifyDateTime
                  //             }).ToList();
               }
               else if (!string.IsNullOrEmpty(TableName) && !string.IsNullOrEmpty(ColumnName))
               {
                  //lstresult = (from p in cloudModelEntities.ChangeTracker
                  //             where p.TableName.Equals(TableName) && p.ColumnName.Equals(ColumnName)
                  //                   && p.PrimayColumnId == prdID.ToString()
                  //                   && p.ColumnName != Constants.Model_ProductDataAccess_ModifiedDate
                  //             select new ChangeTrackerEntity
                  //             {
                  //                TableName = p.TableName,
                  //                ColumnName = p.ColumnName,
                  //                OldValue = p.OldValue,
                  //                NewValue = p.NewValue,
                  //                ModifiedBy = p.ModifyBy,
                  //                ModifiedDate = p.ModifyDateTime
                  //             }).ToList();
               }

            }

            objChangeTrackerSearch.lstChangeTrackentity = lstresult;

            return objChangeTrackerSearch;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="version"></param>
      /// <param name="pageType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ChangeTrackerSearch GetChangeTrackerDetails2(string productId, long version, string pageType)
      {
         var lstresult = new List<ChangeTrackerEntity>();
         var objChangeTrackerSearch = new ChangeTrackerSearch();

         try
         {

            using (var cloudEntities = new QoskCloud())
            {
               //lstresult = (from p in cloudModelEntities.ChangeTracker
               //             where p.PrimayColumnId == productId && p.Version == version && p.TableName == pageType
               //             select new ChangeTrackerEntity
               //             {
               //                TableName = p.TableName,
               //                ColumnName = p.ColumnName,
               //                OldValue = p.OldValue,
               //                NewValue = p.NewValue,
               //                ModifiedBy = p.ModifyBy,
               //                ModifiedDate = p.ModifyDateTime
               //             }).ToList();
            }

            objChangeTrackerSearch.lstChangeTrackentity = lstresult;

            return objChangeTrackerSearch;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get NDC With Dashes and Description based on Product ID
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ChangeTrackerSearch GetProductNDCDescription(long productID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from prdt in cloudEntities.Product
                       where prdt.ProductID == productID
                       select new ChangeTrackerSearch
                       {
                          NDCUPCWithDashes = prdt.NDCUPCWithDashes,
                          Description = prdt.Description
                       }).FirstOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Search wholesaler number details based on specific search and default
      ///   search
      /// </summary>
      /// <param name="objProductML"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<AddWholesalerNumber> GetAllProductForWholesalerNumber(AddWholesalerNumber objProductML)
      {
         var lstProductML = new List<AddWholesalerNumber>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var nDCUPC = 0L;
               var result = long.TryParse(objProductML.NDCUPCWithDashes, out nDCUPC);

               if (!string.IsNullOrWhiteSpace(objProductML.NDCUPCWithDashes))
               {
                  //lstProductML = (from WN in cloudEntities.WholesalerNumber.Where(w => w.IsDeleted == false || w.IsDeleted == null)
                  //                join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
                  //                join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
                  //                where result == true ? prd.NDC == nDCUPC || prd.UPC == nDCUPC : prd.NDCUPCWithDashes.Contains(objProductML.NDCUPCWithDashes)
                  //                select new AddWholesalerNumber
                  //                {
                  //                   WholesalerNumberID = WN.WholesalerNumberID,
                  //                   ProductID = prd.ProductID,
                  //                   NDCUPCWithDashes = prd.NDCUPCWithDashes,
                  //                   Description = prd.Description,
                  //                   Number = WN.Number,
                  //                   WholesalerName = prf.Name,
                  //                   Strength = prd.Strength,
                  //                   NDC = prd.NDC ?? 0,
                  //                   UPC = prd.UPC ?? 0,
                  //                }).ToList();
               }
               else if (!string.IsNullOrWhiteSpace(objProductML.Description))
               {
                  //lstProductML = (from WN in cloudEntities.WholesalerNumber.Where(w => w.IsDeleted == false || w.IsDeleted == null)
                  //                join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
                  //                join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
                  //                where WN.Description.Equals(objProductML.Description)
                  //                select new AddWholesalerNumber
                  //                {
                  //                   WholesalerNumberID = WN.WholesalerNumberID,
                  //                   ProductID = prd.ProductID,
                  //                   NDCUPCWithDashes = prd.NDCUPCWithDashes,
                  //                   Description = prd.Description,
                  //                   Number = WN.Number,
                  //                   WholesalerName = prf.Name,
                  //                   Strength = prd.Strength,
                  //                   NDC = prd.NDC ?? 0,
                  //                   UPC = prd.UPC ?? 0,
                  //                }).ToList();
               }
               else if (objProductML.ProfileCode > 0)
               {
                  //lstProductML = (from WN in cloudEntities.WholesalerNumber.Where(w => w.IsDeleted == false || w.IsDeleted == null)
                  //                join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
                  //                join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
                  //                where WN.ProfileCode == objProductML.ProfileCode
                  //                select new AddWholesalerNumber
                  //                {
                  //                   WholesalerNumberID = WN.WholesalerNumberID,
                  //                   ProductID = prd.ProductID,
                  //                   NDCUPCWithDashes = prd.NDCUPCWithDashes,
                  //                   Description = prd.Description,
                  //                   Number = WN.Number,
                  //                   WholesalerName = prf.Name,
                  //                   Strength = prd.Strength,
                  //                   NDC = prd.NDC ?? 0,
                  //                   UPC = prd.UPC ?? 0,
                  //                }).ToList();
               }
               else if (!string.IsNullOrWhiteSpace(objProductML.Number))
               {
                  //lstProductML = (from WN in cloudEntities.WholesalerNumber.Where(w => w.IsDeleted == false || w.IsDeleted == null)
                  //                join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
                  //                join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
                  //                where WN.Number.Equals(objProductML.Number)
                  //                select new AddWholesalerNumber
                  //                {
                  //                   WholesalerNumberID = WN.WholesalerNumberID,
                  //                   ProductID = prd.ProductID,
                  //                   NDCUPCWithDashes = prd.NDCUPCWithDashes,
                  //                   Description = prd.Description,
                  //                   Number = WN.Number,
                  //                   WholesalerName = prf.Name,
                  //                   Strength = prd.Strength,
                  //                   NDC = prd.NDC ?? 0,
                  //                   UPC = prd.UPC ?? 0,
                  //                }).ToList();
               }
               else
               {
                  //lstProductML = (from WN in cloudEntities.WholesalerNumber.Where(w => w.IsDeleted == false || w.IsDeleted == null) //join prd in cloudModelEntities.Products on WN.ProductID equals prd.Product
                  //                join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
                  //                join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
                  //                select new AddWholesalerNumber
                  //                {
                  //                   WholesalerNumberID = WN.WholesalerNumberID,
                  //                   ProductID = prd.ProductID,
                  //                   NDCUPCWithDashes = prd.NDCUPCWithDashes,
                  //                   Description = prd.Description,
                  //                   Number = WN.Number,
                  //                   WholesalerName = prf.Name,
                  //                   Strength = prd.Strength,
                  //                   NDC = prd.NDC ?? 0,
                  //                   UPC = prd.UPC ?? 0,
                  //                }).ToList();
               }
            }

            return lstProductML;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save Wholesaler number into database.
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response AddWholesalerNumberToDB(AddWholesalerNumber objAddWholesalerNumber, string actionBy)
      {
         var objclsResponse = new Response();
         //WholesalerNumberAudit objWholesalerNumberAudit = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               objAddWholesalerNumber.CreatedBY = actionBy;

               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  //WholesalerNumber objWholesalerNumber = new WholesalerNumber();

                  //objWholesalerNumber.ProductID = objAddWholesalerNumber.ProductID;
                  //objWholesalerNumber.ProfileCode = objAddWholesalerNumber.ProfileCode;
                  //objWholesalerNumber.Number = objAddWholesalerNumber.Number;
                  //objWholesalerNumber.Version = 0;
                  //objWholesalerNumber.CreatedBy = objAddWholesalerNumber.CreatedBY;
                  //objWholesalerNumber.CreatedDate = DateTime.UtcNow;
                  //objWholesalerNumberAudit = new WholesalerNumberAudit();
                  //objWholesalerNumberAudit = MapObject(objWholesalerNumberAudit, objWholesalerNumber);
                  //objWholesalerNumberAudit.Action = Constants.Model_PolicyDataAccess_Insert;
                  //objWholesalerNumberAudit.ActionBy = objAddWholesalerNumber.CreatedBY;
                  //objWholesalerNumberAudit.ActionDate = DateTime.UtcNow;

                  //cloudEntities.WholesalerNumberAudit.Add(objWholesalerNumberAudit);
                  //cloudEntities.WholesalerNumber.Add(objWholesalerNumber);

                  if (cloudEntities.SaveChanges() > 0)
                  {
                     objclsResponse.Status = Status.OK;
                     //objclsResponse.response = objWholesalerNumber;
                     dbContextTransaction.Commit();
                  }
                  else
                  {
                     objclsResponse.Status = Status.Fail;
                     dbContextTransaction.Rollback();
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      /// EditWholesalerNumberToDB method used to Edit Wholesaler NUmber
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response EditWholesalerNumberToDB(AddWholesalerNumber objAddWholesalerNumber, string actionBy)
      {
         var objclsResponse = new Response();
         //WholesalerNumberAudit objWholesalerNumberAudit = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               objAddWholesalerNumber.CreatedBY = actionBy;

               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  //WholesalerNumber result = cloudEntities.WholesalerNumber.Single(p => p.WholesalerNumberID == objAddWholesalerNumber.WholesalerNumberID);

                  //if (result.IsDeleted != true)
                  //{
                  //   if (result.Version != objAddWholesalerNumber.WholesalerVersion)
                  //   {
                  //      objclsResponse.Status = Status.VersionChange;
                  //      objclsResponse.response = result;
                  //      dbContextTransaction.Rollback();
                  //   }
                  //   else
                  //   {
                  //      objWholesalerNumberAudit = new WholesalerNumberAudit();
                  //      result.ProfileCode = objAddWholesalerNumber.ProfileCode;
                  //      result.Number = objAddWholesalerNumber.Number;

                  //      result.ModifiedDate = DateTime.UtcNow;
                  //      result.ModifiedBy = objAddWholesalerNumber.ModifiedBy;
                  //      // Add version
                  //      result.Version = result.Version + 1;
                  //      // Update product Audit table
                  //      objWholesalerNumberAudit = MapObject(objWholesalerNumberAudit, result);
                  //      objWholesalerNumberAudit.Action = Constants.Model_PolicyDataAccess_Update;
                  //      objWholesalerNumberAudit.ActionBy = objAddWholesalerNumber.ModifiedBy;
                  //      objWholesalerNumberAudit.ActionDate = DateTime.UtcNow;
                  //      cloudModelEntities.WholesalerNumberAudit.Add(objWholesalerNumberAudit);

                  //      int IsSave = cloudModelEntities.SaveChanges();
                  //      if (IsSave > 0)
                  //      {
                  //         objclsResponse.Status = Status.OK;
                  //         objclsResponse.UpdateVersion = result.Version;
                  //         dbContextTransaction.Commit();
                  //      }
                  //      else
                  //      {
                  //         objclsResponse.Status = Status.Rollback;
                  //         dbContextTransaction.Rollback();
                  //      }

                  //   }
                  //}
                  //else
                  //{
                  //   objclsResponse.Status = Status.Deleted;
                  //}
               }
            }

            return objclsResponse;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get wholesaler details based on wholesaler ID for showing in edit mode.
      /// </summary>
      /// <param name="WID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static AddWholesalerNumber GetWholesalerNumberByID(long WID)
      {
         var objAddWholesalerNumber = new AddWholesalerNumber();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //objAddWholesalerNumber = (from WN in cloudEntities.WholesalerNumber.Where(p => p.WholesalerNumberID == WID)
               //                          select new AddWholesalerNumber
               //                          {
               //                             WholesalerNumberID = WN.WholesalerNumberID,
               //                             ProductID = WN.ProductID,
               //                             NDCUPCWithDashes = WN.NDCUPCWithDashes,
               //                             Description = WN.Description,
               //                             Number = WN.Number,
               //                             ProfileCode = WN.ProfileCode ?? 0,
               //                             OperationType = Constants.Model_PolicyDataAccess_Update,
               //                             WholesalerVersion = WN.Version
               //                          }).FirstOrDefault();
            }

            return objAddWholesalerNumber;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the existence of wholesaler number to specific product.
      /// </summary>
      /// <param name="objAddWholesalerNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response CheckWholesalerExist(AddWholesalerNumber objAddWholesalerNumber)
      {
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var result = (from p in cloudEntities.WholesalerNumber
               //              join prd in cloudEntities.Products on p.ProductID equals prd.ProductID
               //              where prd.ProductID == objAddWholesalerNumber.ProductID &&
               //                    p.ProfileCode == objAddWholesalerNumber.ProfileCode &&
               //                    p.Number == objAddWholesalerNumber.Number
               //              select p).ToList();

               //if (result.Count > 0)
               //{
               //   objclsResponse.Status = Status.Fail;
               //}
               //else
               //{
               //   objclsResponse.Status = Status.OK;
               //}
            }

            return objclsResponse;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="lotNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response CheckExistanceOfLotNumber(long productID, string lotNumber)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return new Response
               {
                  Status = cloudEntities.Lot.Any(l => l.LotNumber == lotNumber && l.ProductID == productID)
                     ? Status.Fail
                     : Status.OK
               };
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Set the status true false in iSdeleted column that mean used to remove product
      /// </summary>
      /// <param name="wholesalerNumberID"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DeleteWholesalerNumber(long wholesalerNumberID, string actionBy)
      {
         //WholesalerNumberAudit objWholesalerNumberAudit = new WholesalerNumberAudit();
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  //WholesalerNumber whlnbrItem = (from whs in cloudModelEntities.WholesalerNumber
                  //                               where whs.WholesalerNumberID == wholesalerNumberID
                  //                               select whs).SingleOrDefault();
                  //if (whlnbrItem != null && whlnbrItem.IsDeleted != true)
                  //{
                  //   whlnbrItem.IsDeleted = true;
                  //   WholesalerNumberAudit mapResult = MapObject(objWholesalerNumberAudit, whlnbrItem);
                  //   mapResult.Action = Constants.Model_PolicyDataAccess_Delete;
                  //   mapResult.ActionBy = Actionby;
                  //   mapResult.ActionDate = DateTime.UtcNow;
                  //   cloudModelEntities.WholesalerNumberAudit.Add(mapResult);
                  //   int DeletedCount = cloudModelEntities.SaveChanges();
                  //   if (DeletedCount > 0)
                  //   {
                  //      objclsResponse.Status = Status.OK;
                  //      dbContextTransaction.Commit();
                  //   }
                  //   else
                  //   {
                  //      objclsResponse.Status = Status.Fail;
                  //      dbContextTransaction.Rollback();
                  //   }
                  //}
                  //else
                  //{
                  //   objclsResponse.Status = Status.Deleted;
                  //   dbContextTransaction.Rollback();
                  //}
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get All Lot Number for a product when user click on Product grid in
      ///   multiple lot link.
      /// </summary>
      /// <param name="ProductID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<LotNumbersDataEntity> GetLotByProductID(long ProductID)
      {
         var lstLotNumbersDataEntity = new List<LotNumbersDataEntity>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstLotNumbersDataEntity = (from p in cloudEntities.Lot
               //                           join prod in cloudEntities.Product on p.ProductID equals prod.ProductID
               //                           join prf in cloudEntities.Profile on prod.ProfileCode equals prf.ProfileCode
               //                           join lotdivestedManufacturer in cloudEntities.Profile on p.DiverseManufacturerProfileCode equals lotdivestedManufacturer.ProfileCode into manufacturer from manu in manufacturer.DefaultIfEmpty()
               //                           join lotRepackager in cloudEntities.Profiles on p.RepackagerProfileCode equals lotRepackager.ProfileCode into repackager from repack in repackager.DefaultIfEmpty()
               //                           where (p.ProductID == ProductID
               //                                  && p.IsDeleted != true)
               //                           select new LotNumbersDataEntity
               //                           {
               //                              LotID = p.LotNumbersID,
               //                              LotNumber = p.LotNumber,
               //                              Description = prod.Description,
               //                              ExpirationDate = p.ExpirationDate,
               //                              ProfileName = prf.Name,
               //                              ProfileCode = prf.ProfileCode,
               //                              IsDiverseLotNumber = p.DiverseManufacturerProfileCode == null ? false : true,
               //                              DiverseManufactureProfileCode = p.DiverseManufacturerProfileCode,
               //                              DivestedName = manu.Name,
               //                              IsRepackaged = p.RepackagerProfileCode == null ? false : true,
               //                              RepackagerProfileCode = p.RepackagerProfileCode,
               //                              RepackagerName = repack.Name,
               //                              ReturnAllowed = (bool)p.NoReturnsAllowed,
               //                              Version = p.Version
               //                           }).ToList();
            }

            return lstLotNumbersDataEntity;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the search type when user enter NDC/UPC/NDC with dashes using
      ///   single control in wholesaler number and product photo module
      /// </summary>
      /// <param name="searchValue"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static SearchStatusType GetSearchResultStatus(string searchValue)
      {
         var searchType = SearchStatusType.Invalid;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var NDCUPC = 0L;
               var Islong = long.TryParse(searchValue, out NDCUPC);

               if (searchValue.Contains("-"))
               {
                  var result = (from p in cloudEntities.Product
                                where p.NDCUPCWithDashes == searchValue
                                select p).FirstOrDefault();

                  if (result != null)
                  {
                     searchType = SearchStatusType.NDCUPCWithDashes;
                     return searchType;
                  }
               }

               if (Islong && NDCUPC > 0)
               {
                  var result = (from p in cloudEntities.Product
                                where p.NDC == NDCUPC
                                select p).FirstOrDefault();

                  if (result != null)
                  {
                     searchType = SearchStatusType.NDC;
                     return searchType;
                  }
               }

               if (Islong && NDCUPC > 0)
               {
                  var result = (from p in cloudEntities.Product
                                where p.UPC == NDCUPC
                                select p).FirstOrDefault();

                  if (result != null)
                  {
                     searchType = SearchStatusType.UPC;
                     return searchType;
                  }
               }

               return searchType;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all wholesaler number for specific product when user click on
      ///   Wholesaler number link in product grid
      /// </summary>
      /// <param name="prodID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="total"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<AddWholesalerNumber> GetWholesalerNumberByID(long prodID, string sortdatafield, string sortorder, int pagesize, int pagenum, out int total)
      {
         var lstAddWholesalerNumber = new List<AddWholesalerNumber>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var returnResult = (from WN in cloudEntities.WholesalerNumber.Where(p => p.ProductID == prodID && p.IsDeleted != true)
               //                    join prd in cloudEntities.Products on WN.ProductID equals prd.ProductID
               //                    join prf in cloudEntities.Profiles on WN.ProfileCode equals prf.ProfileCode
               //                    join prfcode in cloudEntities.Profiles on prd.ProfileCode equals prfcode.ProfileCode
               //                    select new AddWholesalerNumber
               //                    {
               //                       WholesalerNumberID = WN.WholesalerNumberID,
               //                       WholesalerVersion = WN.Version,
               //                       ProductID = WN.ProductID,
               //                       NDCUPCWithDashes = WN.NDCUPCWithDashes,
               //                       Description = WN.Description,
               //                       Number = WN.Number,
               //                       WholesalerName = prf.Name,
               //                       ManufacturerRepackagerName = prfcode.Name
               //                    });
               total = 0; //returnResult.Count();
               //var orders = returnResult.OrderBy(p => p.Description).Skip(pagesize * pagenum).Take(pagesize);
               //if (!string.IsNullOrEmpty(sortorder))
               //{
               //   if (sortorder == Constants.Model_GroupDataAccess_Asc)
               //   {
               //      orders = orders.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //   }
               //   else
               //   {
               //      orders = orders.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //   }
               //}
               //lstAddWholesalerNumber = orders.ToList();
            }

            return lstAddWholesalerNumber;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Policy details specific to single product in product grid
      ///   policy link.
      /// </summary>
      /// <param name="prodID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ResponsePolicy> GetPolicyByID(long prodID)
      {
         var lstResponsePolicy = new List<ResponsePolicy>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var returnResult = (from policy in cloudEntities.USP_GetPolicyByProductID(prodID)
               //                    select new ResponsePolicy
               //                    {
               //                       PolicyID = policy.PolicyID,
               //                       PolicyTypeName = policy.PolicyTypeName,
               //                       PolicyType = policy.PolicyType,
               //                       PolicyProfileType = policy.ProfileType,
               //                       GroupName = policy.GroupName,
               //                       ProfileCode = policy.ProfileCode,
               //                       PharmacyName = policy.Name,
               //                       Address1 = policy.Address1,
               //                       City = policy.City,
               //                       State = policy.State,
               //                       RegionCode = policy.RegionCode,
               //                       ZipCode = policy.ZipCode,
               //                       ProductID = policy.ProductID,
               //                       LotNumber = policy.LotNumber,
               //                       PharmacyProfileGroupType = policy.PharmacyProfileGroupType,
               //                       ProfileType = policy.ProfileType,
               //                       PolicyForGroupProfileID = policy.PolicyForGroupProfileID
               //                    });
               //lstResponsePolicy = returnResult.ToList();
            }

            return lstResponsePolicy;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="groupID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfileRequest> GroupProfileDetailsByGroupID(int groupID)
      {
         var  lstPr = new List<ProfileRequest>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstPr = (from grp in cloudEntities.GroupProfiles
               //         join prf in cloudEntities.Profiles on grp.ProfileCode equals prf.ProfileCode
               //         where grp.ProfileGroupID == groupID
               //         select new ProfileRequest
               //         {
               //            Name = prf.Name,
               //            City = prf.City,
               //            Address1 = prf.Address1,
               //            State = prf.State,
               //            ProfileCode = prf.ProfileCode,
               //         }).ToList();

               return lstPr;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="type"></param>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response CheckPublishedPolicyStatus(string type, int profileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return new Response
               {
                  Status = Status.OK
                  //Status = (from policy in cloudEntities.Policy
                  //          join ppt in cloudEntities.PolicyRel on policy.PolicyCode equals ppt.PolicyCode
                  //          where policy.PolicyTypeName.Equals(Constants.PublishedPolicy)
                  //                && ppt.ProfileType.Equals(type)
                  //                && ppt.ProfileCode == ProfileCode
                  //                && ppt.ProductID == null select policy).ToList().Count() > 0 ? Status.OK : Status.Fail
               };
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

   }
}
