﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="LotNumbersDataAccess.cs">
///   Copyright (c) 2015 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;
   using System.Text.RegularExpressions;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class LotNumbersDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="objLotNumberSearch"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<LotNumbersDataEntity> GetAllLotNumbers(LotNumberSearch objLotNumberSearch)
      {
         ParameterValidation.Begin().IsNotNull(() => objLotNumberSearch);

         try
         {
            var adminUserType = string.Empty;
            var objUserAcessbilitydata = new UserAccessbilityHelper();

            using (var cloudEntities = new QoskCloud())
            {
               if (objLotNumberSearch.UserTypeCode == "ADMN")
               {
                  adminUserType = Constants.HomeController_ADMIN;
               }

               var selectedprofileCodes = objUserAcessbilitydata.GetUserLotNumbersList(objLotNumberSearch).ToArray();

               var lotNumbersItem = from lot in cloudEntities.Lot
                                    join prdt in cloudEntities.Product on lot.ProductID equals prdt.ProductID
                                    where lot.IsDeleted == false && ((selectedprofileCodes.Contains(prdt.ProfileCode) || (objLotNumberSearch.UserTypeName.Equals(adminUserType))))
                                    select new {lot, prdt};

               if (objLotNumberSearch.ProfileCode != 0)
               {
                  lotNumbersItem = lotNumbersItem.Where(q => q.prdt.ProfileCode == objLotNumberSearch.ProfileCode);
               }

               if (objLotNumberSearch.NDC != 0)
               {
                  lotNumbersItem = lotNumbersItem.Where(q => q.prdt.NDC == objLotNumberSearch.NDC);
               }

               if (!string.IsNullOrWhiteSpace(objLotNumberSearch.Description))
               {
                  lotNumbersItem = lotNumbersItem.Where(q => q.prdt.NDC == objLotNumberSearch.NDC);
               }

               return (from lotNumrs in lotNumbersItem
                       select new LotNumbersDataEntity
                       {
                          LotID = lotNumrs.lot.LotNumberID,
                          ProductID = lotNumrs.lot.ProductID,
                          LotNumber = lotNumrs.lot.LotNumber,
                          ExpirationDate = lotNumrs.lot.ExpirationDate ?? DateTime.MinValue,
                          NDC = lotNumrs.prdt.NDC ?? 0,
                          Description = lotNumrs.prdt.Description,
                          // TODO: Fix commented code
                          //IsRepackaged = (lotNumrs.lot.RepackagerProfileCode != null) ? false : true
                       }).Distinct().OrderBy(pp => pp.Description).ToList();

            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save LotNumber in database.
      /// </summary>
      /// <param name="lotNumberViewModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveLotNumber(LotNumbersDataEntity lotNumberViewModel)
      {
         ParameterValidation.Begin().IsNotNull(() => lotNumberViewModel);

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var lstInsertLot = new List<Lot>();

               foreach (var lotNumber in Regex.Split(lotNumberViewModel.LotNumber, ","))
               {
                  var lotItem = new Lot
                  {
                     ProductID = lotNumberViewModel.ProductID,
                     LotNumber = lotNumber,
                     ExpirationDate = lotNumberViewModel.ExpirationDate.Equals(DateTime.MinValue)
                        ? null
                        : lotNumberViewModel.ExpirationDate,
                     //RepackagerProfileCode = lotNumberViewModel.RepackagerProfileCode,
                     //DiverseManufacturerProfileCode = lotNumberViewModel.DiverseManufactureProfileCode,
                     NoReturnsAllowed = lotNumberViewModel.ReturnAllowed,
                     CreatedBy = lotNumberViewModel.CreatedBy,
                     CreatedDate = DateTime.UtcNow,
                     Version = 0
                  };

                  lstInsertLot.Add(lotItem);

                  if (lotNumberViewModel.DiverseManufactureProfileCode != null)
                  {
                     lotItem = new Lot
                     {
                        //ProductID = lotNumberViewModel.DivestProductID,
                        LotNumber = lotNumber,
                        ExpirationDate = lotNumberViewModel.ExpirationDate.Equals(DateTime.MinValue)
                           ? null
                           : lotNumberViewModel.ExpirationDate,
                        //RepackagerProfileCode = null,
                        //DiverseManufacturerProfileCode = null,
                        NoReturnsAllowed = lotNumberViewModel.ReturnAllowed,
                        CreatedBy = lotNumberViewModel.CreatedBy,
                        CreatedDate = DateTime.UtcNow,
                        Version = 0
                     };

                     lstInsertLot.Add(lotItem);
                  }
               }

               cloudEntities.Lot.AddRange(lstInsertLot);

               return cloudEntities.SaveChanges() > 0;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update LotNumbers based on lot Numbers View Model
      /// </summary>
      /// <param name="lotNumberViewModel"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdateLotNumbers(LotNumbersDataEntity lotNumberViewModel)
      {
         ParameterValidation.Begin().IsNotNull(() => lotNumberViewModel);

         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  var lotItem = cloudEntities.Lot.SingleOrDefault(l => l.LotNumberID == lotNumberViewModel.LotID);

                  if (lotItem != null && lotItem.IsDeleted != true)
                  {
                     if (lotItem.Version != lotNumberViewModel.Version)
                     {

                        objclsResponse.Status = Status.VersionChange;

                        objclsResponse.response = lotItem;
                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        lotItem.LotNumber = lotNumberViewModel.LotNumber;
                        lotItem.ExpirationDate = lotNumberViewModel.ExpirationDate == DateTime.MinValue ? null : lotNumberViewModel.ExpirationDate;

                        // Add Version
                        lotItem.Version = lotNumberViewModel.Version + 1;
                        lotItem.ModifiedBy = lotNumberViewModel.ModifiedBy;
                        lotItem.ModifiedDate = DateTime.UtcNow;
                        //LotItem.RepackagerProfileCode = lotNumberViewModel.RepackagerProfileCode;
                        //LotItem.DiverseManufacturerProfileCode = lotNumberViewModel.DiverseManufactureProfileCode;
                        lotItem.NoReturnsAllowed = lotNumberViewModel.ReturnAllowed;

                        if (lotNumberViewModel.DiverseManufactureProfileCode != null)
                        {
                           var newLotItem = new Lot
                           {
                              //ProductID = lotNumberViewModel.DivestProductID,
                              LotNumber = lotNumberViewModel.LotNumber,
                              ExpirationDate = lotNumberViewModel.ExpirationDate,
                              //RepackagerProfileCode = null,
                              //DiverseManufacturerProfileCode = null,
                              NoReturnsAllowed = lotNumberViewModel.ReturnAllowed,
                              CreatedBy = lotNumberViewModel.ModifiedBy,
                              CreatedDate = DateTime.UtcNow,
                              Version = 0
                           };

                           if (lotNumberViewModel.ExpirationDate.Equals(DateTime.MinValue))
                           {
                              lotNumberViewModel.ExpirationDate = null;
                           }

                           cloudEntities.Lot.Add(newLotItem);
                        }
                        else
                        {
                           //var product = cloudEntities.Product.Where(m => m.ProductID == lotNumberViewModel.ProductID).FirstOrDefault();
                           //var lot = cloudEntities.Lot.Where(l => l.ProductID == product.DivestProductID && l.LotNumber == oldLotNumber).FirstOrDefault();
                           //cloudEntities.Lot.Remove(lot);
                        }

                        if (cloudEntities.SaveChanges() > 0)
                        {
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = lotItem.Version;

                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.Status = Status.Rollback;
                           dbContextTransaction.Rollback();
                        }

                        UpdateProductDivestAndRepackager(Convert.ToInt64(lotItem.ProductID));
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }

               return objclsResponse;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Update DivestbyLot and IsRepackager
      /// </summary>
      /// <param name="_prodId"></param>
      ///****************************************************************************
      public static void UpdateProductDivestAndRepackager(long _prodId)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //   var _productDetails = cloudEntities.Product.First(p => p.ProductID == _prodId);

               //   if (_productDetails != null)
               //   {
               //      var _divestedLotNumbers = cloudEntities.Lot.Where(l => l.ProductID == _prodId && l.DiverseManufacturerProfileCode != null);

               //      if (_divestedLotNumbers.Count() > 0)
               //      {
               //         _productDetails.DivestedByLot = true;
               //         _productDetails.DiverseManufacturerProfileCode = _divestedLotNumbers.First().DiverseManufacturerProfileCode;
               //      }
               //      else
               //      {
               //         _productDetails.DivestedByLot = false;
               //         _productDetails.DiverseManufacturerProfileCode = null;
               //      }

               //      var _repackagers = cloudEntities.Lot.Where(l => l.ProductID == _prodId && l.RepackagerProfileCode != null);

               //      if (_repackagers.Count() > 0)
               //      {
               //         _productDetails.IsRepackager = true;
               //      }
               //      else
               //      {
               //         _productDetails.IsRepackager = false;
               //      }
               //   }

               //   cloudEntities.SaveChanges();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete LotNumbers based on LotID.
      /// </summary>
      /// <param name="lotID"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DeleteLotNumbers(long lotID, string actionBy)
      {
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var lotItem = cloudEntities.Lot.Single(l => l.LotNumberID == lotID);

               if (lotItem != null && lotItem.IsDeleted != true)
               {
                  lotItem.IsDeleted = true;

                  objclsResponse.Status = cloudEntities.SaveChanges() > 0
                        ? Status.OK
                        : Status.Fail;
               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }

               return objclsResponse;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="objLotNumberSeach"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static LotNumbersDataEntity GetLotNumberByID(long lotID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from lot in cloudEntities.Lot
                       join prdt in cloudEntities.Product on lot.ProductID equals prdt.ProductID
                       where lot.LotNumberID == lotID
                       select new LotNumbersDataEntity
                       {
                          LotID = lot.LotNumberID,
                          ProductID = lot.ProductID,
                          LotNumber = lot.LotNumber,
                          ExpirationDate = lot.ExpirationDate ?? DateTime.MinValue,
                          NDC = prdt.NDC ?? 0,
                          //DiverseManufactureProfileCode = lot.DiverseManufacturerProfileCode ?? -1,
                          //IsDiverseLotNumber = (lot.DiverseManufacturerProfileCode != null) ? true : false,
                          Description = prdt.Description,
                          //IsRepackaged = (lot.RepackagerProfileCode != null) ? false : true,
                          Version = lot.Version
                       }).SingleOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get DiverseLotNumbers with profile
      /// </summary>
      /// <param name="prdID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static DiverseLotNumbersDetails GetDivestedLotNumber(long prdID)
      {
         try
         {
            var objDiverseLotNumbersDetails = new DiverseLotNumbersDetails();

            using (var cloudEntities = new QoskCloud())
            {
               var lotNumbersItem = from lot in cloudEntities.Lot
                                    join prdt in cloudEntities.Product on lot.ProductID equals prdt.ProductID
                                    join pfl in cloudEntities.Profile on prdt.ProfileCode equals pfl.ProfileCode
                                    where prdt.ProductID == prdID && (lot.IsDeleted != true)
                                    select new { lot, prdt, pfl };

               if (lotNumbersItem.Any())
               {
                  //originalManufactureLots = new DiverseLotNumbers();
                  //diverseManufactureLots = new DiverseLotNumbers();
                  //var orginalManfLotNumbers = lotNumbersItem.Where(q => q.lot.DiverseManufacturerProfileCode == null && q.lot.RepackagerProfileCode == null);
                  //if (orginalManfLotNumbers.Count() > 0)
                  //{

                  //   lotNumberlist = (from lotNumrs in orginalManfLotNumbers

                  //                    select new LotNumbersDataEntity
                  //                    {
                  //                       LotID = lotNumrs.lot.LotNumbersID,
                  //                       ProductID = lotNumrs.lot.ProductID ?? 0,
                  //                       LotNumber = lotNumrs.lot.LotNumber,
                  //                       ExpirationDate = lotNumrs.lot.ExpirationDate ?? DateTime.MinValue,
                  //                       NDC = lotNumrs.prdt.NDC ?? 0,
                  //                       Description = lotNumrs.prdt.Description,
                  //                       IsRepackaged = (lotNumrs.lot.RepackagerProfileCode != null) ? false : true,
                  //                       DiverseManufactureProfileCode = lotNumrs.lot.DiverseManufacturerProfileCode ?? 0
                  //                    }).Distinct().OrderBy(pp => pp.Description).ToList();

                  //   var originalprofile = orginalManfLotNumbers.Where(p => p.prdt.ProductID == prdID).ToList();
                  //   if (originalprofile.Count > 0)
                  //   {
                  //      originalManufactureLots.ProfileCode = originalprofile[0].pfl.ProfileCode;
                  //      originalManufactureLots.Name = originalprofile[0].pfl.Name;
                  //      originalManufactureLots.ProductID = originalprofile[0].prdt.ProductID;
                  //   }
                  //   originalManufactureLots.LotNumbersList = lotNumberlist;
                  //   objDiverseLotNumbersDetails.OriginalManufactureLotNumbers = originalManufactureLots;
                  //   objDiverseLotNumbersDetails.DivestedProfileCode = (from p in cloudModelEntities.Products where p.ProductID == prdID select p.DiverseManufacturerProfileCode).FirstOrDefault();
                  //}
                  //else
                  //{
                  //   originalManufactureLots = new DiverseLotNumbers();
                  //   originalManufactureLots.ProfileCode = -1;
                  //   objDiverseLotNumbersDetails.OriginalManufactureLotNumbers = originalManufactureLots;
                  //}
                  //var diverseManfucatureLotNumbers = lotNumbersItem.Where(q => q.lot.DiverseManufacturerProfileCode != null);
                  //if (diverseManfucatureLotNumbers.Count() > 0)
                  //{
                  //   lotNumberlist = null;
                  //   lotNumberlist = (from lotNumrs in diverseManfucatureLotNumbers
                  //                    select new LotNumbersDataEntity
                  //                    {
                  //                       LotID = lotNumrs.lot.LotNumbersID,
                  //                       ProductID = lotNumrs.lot.ProductID ?? 0,
                  //                       ProfileName = (string.IsNullOrEmpty(lotNumrs.pfl.Name)) ? lotNumrs.pfl.ProfileCode.ToString() : lotNumrs.pfl.Name,
                  //                       LotNumber = lotNumrs.lot.LotNumber,
                  //                       ExpirationDate = lotNumrs.lot.ExpirationDate ?? DateTime.MinValue,
                  //                       NDC = lotNumrs.prdt.NDC ?? 0,
                  //                       Description = lotNumrs.prdt.Description,
                  //                       IsRepackaged = (lotNumrs.lot.RepackagerProfileCode != null) ? false : true,
                  //                       DiverseManufactureProfileCode = lotNumrs.lot.DiverseManufacturerProfileCode ?? 0
                  //                    }).Take(50).Distinct().OrderBy(pp => pp.Description).ToList();
                  //   diverseManufactureLots.LotNumbersList = lotNumberlist;
                  //   var diverseprofile = diverseManfucatureLotNumbers.Where(p => p.prdt.ProductID == prdID).ToList();
                  //   if (diverseprofile.Count > 0)
                  //   {
                  //      int diversprofileCode = diverseprofile[0].lot.DiverseManufacturerProfileCode ?? 0;
                  //      diverseManufactureLots.ProfileCode = diversprofileCode;
                  //      diverseManufactureLots.Name = GetDiverseProfileCodeWithName(diversprofileCode);
                  //   }

                  //   objDiverseLotNumbersDetails.DiverseManufactureLotNumbers = diverseManufactureLots;
                  //}
                  //else
                  //{
                  //   diverseManufactureLots = new DiverseLotNumbers();
                  //   diverseManufactureLots.ProfileCode = -1;
                  //   objDiverseLotNumbersDetails.DiverseManufactureLotNumbers = diverseManufactureLots;
                  //}

               }
               else
               {
                  objDiverseLotNumbersDetails.OriginalManufactureLotNumbers = new DiverseLotNumbers { ProfileCode = -1 };
                  objDiverseLotNumbersDetails.DiverseManufactureLotNumbers = new DiverseLotNumbers { ProfileCode = -1 };
               }

               return objDiverseLotNumbersDetails;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get GetDiverseProfile Name based on Product ID
      /// </summary>
      /// <param name="divestedProfileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetDivestedProfileCodeWithName(int divestedProfileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from profile in cloudEntities.Profile
                       where profile.ProfileCode == divestedProfileCode
                       select profile.Name).SingleOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductIDWithName> GetProductIDWithName()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from prdt in cloudEntities.Product
                       select new ProductIDWithName
                       {
                          ProductID = prdt.ProductID,
                          ProductName = prdt.Description
                       }).Distinct().OrderBy(pp => pp.ProductName).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="terms"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductIDWithName> GetProductIDWithName(string terms)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from prdt in cloudEntities.Product
                       where prdt.Description.Contains(terms)
                       select new ProductIDWithName
                       {
                          ProductID = prdt.ProductID,
                          ProductName = prdt.Description
                       }).Take(20).Distinct().OrderBy(pp => pp.ProductName).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="productID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<RecallDDLWithName> GetRecallID(long productID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from recall in cloudEntities.Recall
                       join profile in cloudEntities.Profile on recall.ManufacturerProfileCode equals profile.ProfileCode
                       join prdt in cloudEntities.Product on profile.ProfileCode equals prdt.ProfileCode
                       where prdt.ProductID == productID
                       select new RecallDDLWithName
                       {
                          RecallID = recall.RecallID,
                          ProductName = profile.Name + Constants.Model_LotNumbersDataAccess_BigLeftBraket + prdt.Description + Constants.Model_LotNumbersDataAccess_BigRightBraket
                       }).Distinct().OrderBy(pp => pp.ProductName).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   API controller for Get Dosage Details
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfilesName> GetReapackagerProfileName()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from prdt in cloudEntities.Product
                       join profile in cloudEntities.Profile on prdt.ProfileCode equals profile.ProfileCode
                       select new ProfilesName
                       {
                          ProfileCode = prdt.ProfileCode,
                          Name = profile.Name
                       }).Distinct().OrderBy(pp => pp.Name).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Changed Lot Number Ownership
      /// </summary>
      /// <param name="divestedLotNumbers"></param>
      /// <returns></returns>
      ///****************************************************************************
      public long ChangedDiverseLotNumber(ChangeDiverseLotNumbers divestedLotNumbers)
      {
         try
         {
            var productID = 0L;

            using (var cloudEntities = new QoskCloud())
            {
               using (var cloudTransaction = cloudEntities.Database.BeginTransaction())
               {
                  try
                  {
                     var lotResult = 0;

                     foreach (var lotID in divestedLotNumbers.LotIDs.Split(','))
                     {
                        var lotItem = cloudEntities.Lot.Single(l => l.LotNumberID == Convert.ToInt64(lotID));

                        productID = lotItem.ProductID;
                        //lotItem.DiverseManufacturerProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;
                        lotResult = cloudEntities.SaveChanges();
                     }

                     if (divestedLotNumbers.Updatestatus && lotResult > 0)
                     {
                        cloudTransaction.Commit();
                     }
                     else if (!divestedLotNumbers.Updatestatus && lotResult > 0)
                     {
                        cloudTransaction.Commit();
                     }
                     else
                     {
                        cloudTransaction.Rollback();
                     }

                     UpdateProductDivestAndRepackager(Convert.ToInt64(divestedLotNumbers.ProductID));

                     return productID;
                  }
                  catch (DbEntityValidationException dbEx)
                  {
                     foreach (var validationErrors in dbEx.EntityValidationErrors)
                     {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                           Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                                                   validationErrors.Entry.Entity.GetType().FullName,
                                                    validationError.PropertyName,
                                                     validationError.ErrorMessage);
                        }
                     }

                     cloudTransaction.Rollback();
                     throw;
                  }
                  catch (Exception ex)
                  {
                     Logger.Error(ex.Message);

                     cloudTransaction.Rollback();
                  }
               }
            }

            return 0L;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="divestedLotNumbers"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DiversionOfProductWithLot(ChangeDiverseLotNumbers divestedLotNumbers)
      {
         var objclsResponse = new Response();

         using (var cloudEntities = new QoskCloud())
         {
            using (var cloudTransaction = cloudEntities.Database.BeginTransaction())
            {
               try
               {
                  var prdresult = cloudEntities.Product.FirstOrDefault(p => p.ProductID == divestedLotNumbers.ProductID);
                  var originalProfileCode = prdresult?.ProfileCode;

                  //if (!prdresult.DivestedByLot ?? false)
                  //{
                  //   prdresult.DiverseManufacturerProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;
                  //   prdresult.DivestedByLot = true;
                  //   int updateResult = cloudModelEntities.SaveChanges();
                  //   if (updateResult > 0)
                  //   {
                  //      prdresult.ProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;
                  //      prdresult.DiverseManufacturerProfileCode = null;
                  //      prdresult.DivestedByLot = false;
                  //      prdresult.IsOriginal = false;
                  //      cloudModelEntities.Products.Add(prdresult);
                  //      int savedResult = cloudModelEntities.SaveChanges();
                  //      if (savedResult > 0)
                  //      {
                  //         var productID = prdresult.ProductID;
                  //         objclsResponse = UpdateDivestProductIDColumn(divestedLotNumbers.ProductID, productID, ref cloudModelEntities);
                  //         if (objclsResponse.Status.Equals(Status.OK))
                  //         {

                  //            Response objclsResponseLot = CreateDivestedLotReplica(productID, divestedLotNumbers, originalProfileCode, ref cloudModelEntities);
                  //            Response objclsResponseWholesaler = ReplicateWholesalerDetails(divestedLotNumbers.ProductID, productID, ref cloudModelEntities);
                  //            if (objclsResponseLot.Status.Equals(Status.OK) && objclsResponseWholesaler.Status.Equals(Status.OK))
                  //            {
                  //               objclsResponse = objclsResponseLot;
                  //               dbContextTransaction.Commit();
                  //            }
                  //            else
                  //            {
                  //               dbContextTransaction.Rollback();
                  //            }
                  //         }
                  //         else
                  //         {
                  //            dbContextTransaction.Rollback();
                  //         }
                  //      }
                  //      else
                  //      {
                  //         dbContextTransaction.Rollback();
                  //      }
                  //   }
                  //}
                  //else
                  //{
                  //   var prd = cloudModelEntities.Product.Where(p => (p.NDCUPCWithDashes.Equals(prdresult.NDCUPCWithDashes) || p.NDC == prdresult.NDC || p.UPC == prdresult.UPC) && p.ProductID != prdresult.ProductID && p.ProfileCode == prdresult.DiverseManufacturerProfileCode).FirstOrDefault();
                  //   if (prd != null)
                  //   {
                  //      objclsResponse = CreateDivestedLotReplica(prd.ProductID, divestedLotNumbers, prd.ProfileCode, ref cloudModelEntities);
                  //      if (objclsResponse.Status.Equals(Status.OK))
                  //      {
                  //         dbContextTransaction.Commit();
                  //      }
                  //      else
                  //      {
                  //         dbContextTransaction.Rollback();
                  //      }
                  //   }
                  //}
               }
               catch (Exception)
               {
                  cloudTransaction.Rollback();
               }
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update product.
      /// </summary>
      /// <param name="oldproductID"></param>
      /// <param name="newproductID"></param>
      /// <param name="cloudEntities"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response UpdateDivestProductIDColumn(long oldproductID, long newproductID, ref QoskCloud cloudEntities)
      {
         var objclsResponse = new Response();
         var prdresult = cloudEntities.Product.FirstOrDefault(p => p.ProductID == oldproductID);
         //prdresult.DivestProductID = newproductID;

         objclsResponse.Status = cloudEntities.SaveChanges() > 0
                                    ? Status.OK
                                    : Status.Fail;
         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create replica of lot number and also modified the existing lot and set
      ///   to divested manufacturer profilecode.
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="divestedLotNumbers"></param>
      /// <param name="profilecode"></param>
      /// <param name="cloudEntities"></param>
      /// <returns></returns>
      ///****************************************************************************
      private Response CreateDivestedLotReplica(int productID, ChangeDiverseLotNumbers divestedLotNumbers, int profilecode, ref QoskCloud cloudEntities)
      {
         var objResponse = new Response { Status = Status.OK };

         foreach (var lotID in divestedLotNumbers.LotIDs.Split(','))
         {
            var lotItem = cloudEntities.Lot.Single(l => l.LotNumberID == Convert.ToInt64(lotID));
            var objLotNumbers = GenericUtility.MapObject(lotItem, new Lot());

            //lotItem.DiverseManufacturerProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;

            if (cloudEntities.SaveChanges() > 0)
            {
               var objclsResponsepolicy = ReplicateProductSpecificLotDetails(divestedLotNumbers, productID, profilecode, lotItem.LotNumber, ref cloudEntities);

               if (objclsResponsepolicy.Status.Equals(Status.OK))
               {
                  objLotNumbers.ProductID = productID;
                  //objLotNumbers.DiverseManufacturerProfileCode = null;

                  cloudEntities.Lot.Add(objLotNumbers);

                  if (cloudEntities.SaveChanges() < 1)
                  {
                     objResponse.Status = Status.Fail;
                  }
               }
            }
         }

         return objResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Change product profile and call UpdatePolicyProfileNameInCaseOfAllLot
      ///   method to update policy
      /// </summary>
      /// <param name="divestedLotNumbers"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DivestProductforALLLOT(ChangeDiverseLotNumbers divestedLotNumbers)
      {
         var objclsResponse = new Response();

         using (var cloudEntities = new QoskCloud())
         {
            using (var cloudTransaction = cloudEntities.Database.BeginTransaction())
            {
               //DivestedInfo objDivestedInfo = new DivestedInfo();
               //Products prdresult = cloudModelEntities.Products.Where(p => p.ProductID == divestedLotNumbers.ProductID).FirstOrDefault();
               //objDivestedInfo.ProductID = prdresult.ProductID;
               //objDivestedInfo.ProfileCode = prdresult.ProfileCode;
               //cloudModelEntities.DivestedInfo.Add(objDivestedInfo);
               //prdresult.ProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;
               //int _updateResult = cloudModelEntities.SaveChanges();
               //if (_updateResult > 0)
               //{
               //   var prdimage = cloudModelEntities.ProductImageDetail.Where(l => l.ProductID == prdresult.ProductID).ToList();
               //   if (prdimage.Count > 0)
               //   {
               //      prdimage.ForEach(a => a.ProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode);
               //      int _updateImageresult = cloudModelEntities.SaveChanges();
               //      if (_updateImageresult > 0)
               //      {
               //         objclsResponse = UpdatePolicyProfileNameInCaseOfAllLot(prdresult.ProductID, prdresult.ProfileCode, ref cloudModelEntities);
               //         if (objclsResponse.Status.Equals(Status.OK))
               //         {
               //            dbContextTransaction.Commit();
               //            return objclsResponse;
               //         }
               //         else
               //         {
               //            objclsResponse.Status = Status.Fail;
               //            dbContextTransaction.Rollback();
               //            return objclsResponse;
               //         }
               //      }
               //      else
               //      {
               //         dbContextTransaction.Rollback();
               //         objclsResponse.Status = Status.Fail;
               //         return objclsResponse;
               //      }
               //   }
               //   else
               //   {
               //      dbContextTransaction.Commit();
               //      objclsResponse.Status = Status.OK;
               //      return objclsResponse;
               //   }
               //}
               //else
               //{
               //   dbContextTransaction.Rollback();
               //   objclsResponse.Status = Status.Fail;
               //   return objclsResponse;
               //}
            }
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update profile code if the product divested(All Lot)
      /// </summary>
      /// <param name="productID"></param>
      /// <param name="profileCode"></param>
      /// <param name="cloudEntities"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response UpdatePolicyProfileNameInCaseOfAllLot(long productID, int profileCode, ref QoskCloud cloudEntities)
      {
         var objResponse = new Response();
         var productPolicy = cloudEntities.PolicyRel.Where(l => l.ProductID == productID).ToList();

         if (productPolicy.Count > 0)
         {
            productPolicy.ForEach(a => a.ProfileCode = profileCode);

            objResponse.Status = cloudEntities.SaveChanges() > 0
                                       ? Status.OK
                                       : Status.Fail;
         }
         else
         {
            objResponse.Status = Status.OK;
         }

         return objResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Copy the policy specific to lot in policy and policy profiletype with
      ///   different product id and also delete the original policy
      /// </summary>
      /// <param name="divestedLotNumbers"></param>
      /// <param name="productID">New Product ID</param>
      /// <param name="profileCode"></param>
      /// <param name="lotNumber"></param>
      /// <param name="cloudEntities"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response ReplicateProductSpecificLotDetails(ChangeDiverseLotNumbers divestedLotNumbers, long productID, int profileCode, string lotNumber, ref QoskCloud cloudEntities)
      {
         var objclsResponse = new Response();
         var productIDold = divestedLotNumbers.ProductID;
         var prdpolicy = cloudEntities.PolicyRel.Where(l => l.ProductID == productIDold && l.LotNumber.Equals(lotNumber) && l.ProfileCode == profileCode).ToList();

         if (prdpolicy.Count > 0)
         {
            foreach (var item in prdpolicy)
            {
               var policyID = 0L;
               //policyID = item.PolicyID;
               //// Check the existence of published policy for divested manufacturer
               //var isexist = (from ppt in QualanexCloudModelEntities.PolicyRel.Where(u => u.ProfileCode == divestedLotNumbers.DiverseOwnshipProfileCode && u.ProductID == null)
               //               join p in QualanexCloudModelEntities.Policy.Where(u => u.PolicyType == 1) on ppt.PolicyID equals p.PolicyID
               //               select p).FirstOrDefault();
               //if (isexist != null)
               //{
               //   // Insert new  policy Rule for divested lot in policy table
               //   Policy objnewPolicy = new Policy();
               //   var resPolicy = QualanexCloudModelEntities.Policy.Where(pp => pp.PolicyID == policyID).FirstOrDefault();
               //   objnewPolicy = GenericUtility.MapObject<Policy>(resPolicy, objnewPolicy);
               //   objnewPolicy.PolicyID = 0;

               //   QualanexCloudModelEntities.Policy.Add(objnewPolicy);
               //   int result = QualanexCloudModelEntities.SaveChanges();

               //   if (result > 0)
               //   {
               //      //Deleted Divested Lot Policy
               //      item.IsDeleted = true;

               //      PolicyRel objPolicyProfileType = new PolicyRel();
               //      objPolicyProfileType = GenericUtility.MapObject<PolicyRel>(item, objPolicyProfileType);
               //      //Insert new lot specific policy with divested profilecode in policy profile type
               //      objPolicyProfileType.PolicyProfileID = 0;
               //      objPolicyProfileType.PolicyID = objnewPolicy.PolicyID;
               //      objPolicyProfileType.ProductID = _productIDnew;
               //      objPolicyProfileType.LotNumber = _LotNumber;
               //      objPolicyProfileType.ProfileCode = divestedLotNumbers.DiverseOwnshipProfileCode;
               //      QualanexCloudModelEntities.PolicyRel.Add(objPolicyProfileType);

               //      int saveresult = QualanexCloudModelEntities.SaveChanges();
               //      if (saveresult > 0)
               //      {
               //         objclsResponse.Status = Status.OK;
               //      }
               //      else
               //      {
               //         objclsResponse.Status = Status.Fail;
               //      }
               //   }
               //   else
               //   {
               //      objclsResponse.Status = Status.Fail;
               //   }
               //}
               //else
               //{
               //   objclsResponse.Status = Status.OK;
               //}
            }
         }
         else
         {
            objclsResponse.Status = Status.OK;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create replica of wholesaler number with different Product ID for
      ///   divested product.
      /// </summary>
      /// <param name="productIDold"></param>
      /// <param name="productIDnew"></param>
      /// <param name="cloudEntities"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Response ReplicateWholesalerDetails(long productIDold, long productIDnew, ref QoskCloud cloudEntities)
      {
         var objclsResponse = new Response();
         //List<WholesalerNumber> _lstReplicateWholesalerNumber = new List<WholesalerNumber>();
         //var _prdwholesalerNumber = QualanexCloudModelEntities.WholesalerNumber.Where(l => l.ProductID == _productIDold).ToList();
         //if (_prdwholesalerNumber.Count > 0)
         //{
         //   foreach (var item in _prdwholesalerNumber)
         //   {
         //      WholesalerNumber objWholesalerNumber = QualanexCloudModelEntities.WholesalerNumber.Where(p => p.WholesalerNumberID == item.WholesalerNumberID).FirstOrDefault();
         //      objWholesalerNumber.ProductID = _productIDnew;
         //      _lstReplicateWholesalerNumber.Add(objWholesalerNumber);
         //   }
         //   if (_lstReplicateWholesalerNumber.Count > 0)
         //   {
         //      QualanexCloudModelEntities.WholesalerNumber.AddRange(_lstReplicateWholesalerNumber);
         //      int _successcount = QualanexCloudModelEntities.SaveChanges();
         //      if (_successcount > 0)
         //      {
         //         objclsResponse.Status = Status.OK;
         //      }
         //      else
         //      {
         //         objclsResponse.Status = Status.Fail;
         //      }
         //   }
         //}
         //else
         //{
         //   objclsResponse.Status = Status.OK;
         //}

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="productId"></param>
      /// <param name="lotNumber"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal static List<object> GetLots(long productId, string lotNumber = null)
      {
         var result = new List<object>();

         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               result = new List<object>(
                  from lot in cloudEntities.Lot
                  where lot.ProductID == productId
                        && (lotNumber == null || lotNumber == String.Empty || lot.LotNumber == lotNumber)
                        && !lot.IsDeleted
                  select
                     new Areas.Product.Models.LotResults()
                     {
                        ExpirationDate = lot.ExpirationDate,
                        LotNumber = lot.LotNumber,
                        ProductId = lot.ProductID,
                        RecallId = lot.RecallID
                     });
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return result;
      }
   }
}