﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="ProfileDataAccess.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   internal class ProfilesDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="query"></param>
      /// <param name="objProfileSeach"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfileRequest> ListSearchAllProfiles(System.Collections.Specialized.NameValueCollection query, ProfileSeach objProfileSeach, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         ParameterValidation.Begin().IsNotNull(() => objProfileSeach);

         IEnumerable<ProfileRequest> lsProfiles = null;

         var AdminUserType = UserTypes.ADMIN.ToString();

         try
         {
            if (objProfileSeach.UserTypeName.Equals(AdminUserType))
            {
               AdminUserType = UserTypes.ADMIN.ToString();
            }

            using (var cloudEntities = new QoskCloud())
            {
               var _SearchParameters = new List<FilterClass>();

               if (!string.IsNullOrEmpty(objProfileSeach.SerachProfileType))
               {
                  _SearchParameters.Add(new FilterClass()
                  {
                     Property = Constants.Model_PolicyDataAccess_Type,
                     Value = objProfileSeach.SerachProfileType,
                     FilterType = Constants.Model_PolicyDataAccess_Equal
                  });
               }

               if (objProfileSeach.ProfileCode > 0)
               {
                  _SearchParameters.Add(new FilterClass()
                  {
                     Property = Constants.Model_PolicyDataAccess_ProfileCode,
                     Value = objProfileSeach.ProfileCode,
                     FilterType = Constants.Model_PolicyDataAccess_Equal
                  });
               }
               if (!string.IsNullOrEmpty(objProfileSeach.City))
               {
                  _SearchParameters.Add(new FilterClass()
                  {
                     Property = Constants.Model_ProfilesDataAccess_City,
                     Value = objProfileSeach.City,
                     FilterType = Constants.PolicyController_CONTAINS
                  });
               }

               var whereCondition = CommonDataAccessLayer.BuildQuery(query, _SearchParameters, null);
               //var detailsresult = cloudEntities.USP_ProfileSearch(whereCondition);

               //lsProfiles = (from profile in detailsresult
               //              select new ProfileRequest
               //              {
               //                 ProfileCode = profile.ProfileCode,
               //                 Name = profile.Name,
               //                 Type = profile.Type,
               //                 City = profile.City,
               //                 State = profile.State,
               //                 ZipCode = profile.ZipCode,
               //                 PhoneNumber = profile.PhoneNumber,

               //              }).Distinct().OrderBy(pp => pp.Name).ToList();

               if (lsProfiles != null)
               {
                  if (!string.IsNullOrEmpty(sortorder))
                  {
                     if (sortorder == Constants.Model_GroupDataAccess_Asc)
                     {
                        lsProfiles = lsProfiles.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                     }
                     else
                     {
                        lsProfiles = lsProfiles.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                     }
                  }
                  totalrecord = lsProfiles.Count();
                  lsProfiles = lsProfiles.Skip(pagesize * pagenum).Take(pagesize);
                  return lsProfiles.ToList();
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         totalrecord = 0;
         var emptydata = new List<ProfileRequest>();
         return emptydata.ToList();
      }

      ///****************************************************************************
      /// <summary>
      /// Get Profiles MasterData
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfileRequest> GetProfilesMasterData()
      {
         ICollection<ProfileRequest> lsProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from pprofile in cloudEntities.Profile
                       select new ProfileRequest
                       {
                          //Type = pprofile.Type,
                          City = pprofile.City,
                          State = pprofile.State,
                          ZipCode = pprofile.ZipCode,
                          RegionCode = pprofile.RegionCode
                       }).Distinct().ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// ListAllProfiles Types from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfileType> ListProfileType()
      {
         ICollection<ProfileType> lsProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var profileTypeList = (from types in cloudEntities.ProfileTypeDict
                                      select types).ToList();
               if (profileTypeList.Count > 0)
               {
                  lsProfiles = new List<ProfileType>();

                  foreach (var itm in profileTypeList)
                  {
                     var objProfileType = new ProfileType
                     {
                        Type = itm.Description,
                        ID = itm.Code
                     };
                     lsProfiles.Add(objProfileType);
                  }
               }

               return lsProfiles;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /// <summary>
      ///  Get ManufactureByType
      /// </summary>
      /// <returns></returns>
      public static ICollection<ProfilesName> GetManufactureByType()
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstManufacture = (from manufacture in cloudEntities.Profile
               //                  where manufacture.Type.Equals(Constants.ReportController_Manufacturer)
               //                  select new ProfilesName
               //                  {
               //                     ProfileCode = manufacture.ProfileCode,
               //                     Name = manufacture.Name + Constants.Model_LotNumbersDataAccess_BigLeftBraket + manufacture.ProfileCode + Constants.Model_LotNumbersDataAccess_BigRightBraket,
               //                  }).OrderBy(pp => pp.Name).ToList();

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetProfileCode(string jsonData, ProfileSeach objProfileSeach)
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (objProfileSeach.UserTypeCode)
               {
                  // For Site user get list of profiles
                  case "SITE":
                     //lstManufacture = (from profile in cloudEntities.Profile
                     //                  join usrprofile in cloudEntities.UserProfiles
                     //                     on profile.ProfileCode equals usrprofile.ProfileCode
                     //                  where profile.Status.Equals(Constants.Model_ProfilesDataAccess_Active) && profile.Type.Equals(jsonData) && usrprofile.UserID == objProfileSeach.UserID
                     //                  orderby profile.Name ascending
                     //                  select new ProfilesName
                     //                  {
                     //                     ProfileCode = profile.ProfileCode,
                     //                     Name = profile.Name + Constants.Model_ProfilesDataAccess_LeftBraces + profile.ProfileCode.ToString() + Constants.Model_ProfilesDataAccess_RightBraces
                     //                  }).Distinct().ToList();
                     break;

                  case "CORP":
                     //lstManufacture = (from profile in cloudEntities.Profile
                     //                  where profile.Status.Equals(Constants.Model_ProfilesDataAccess_Active) && profile.Type.Equals(jsonData) && profile.RollupProfileCode == objProfileSeach.RollupProfileCode
                     //                  orderby profile.Name ascending
                     //                  select new ProfilesName
                     //                  {
                     //                     ProfileCode = profile.ProfileCode,
                     //                     Name = profile.Name + Constants.Model_ProfilesDataAccess_LeftBraces + profile.ProfileCode.ToString() + Constants.Model_ProfilesDataAccess_RightBraces
                     //                  }).Distinct().ToList();
                     break;

                  // For Regional user get list of profiles
                  case "REGN":
                     //lstManufacture = (from profile in cloudEntities.Profile
                     //                  join usrprofile in cloudEntities.UserProfiles
                     //                     on profile.ProfileCode equals usrprofile.ProfileCode
                     //                  where profile.Status.Equals(Constants.Model_ProfilesDataAccess_Active) && profile.Type.Equals(jsonData) && usrprofile.UserID == objProfileSeach.UserID
                     //                  orderby profile.Name ascending
                     //                  select new ProfilesName
                     //                  {
                     //                     ProfileCode = profile.ProfileCode,
                     //                     Name = profile.Name + Constants.Model_ProfilesDataAccess_LeftBraces + profile.ProfileCode.ToString() + Constants.Model_ProfilesDataAccess_RightBraces
                     //                  }).Distinct().ToList();
                     break;

                  // For Administrator user get list of profiles
                  case "ADMN":
                     //lstManufacture = (from profile in cloudEntities.Profile
                     //                  where profile.Status.Equals(Constants.Model_ProfilesDataAccess_Active) && profile.Type.Equals(jsonData)
                     //                  orderby profile.Name ascending
                     //                  select new ProfilesName
                     //                  {
                     //                     ProfileCode = profile.ProfileCode,
                     //                     Name = profile.Name + Constants.Model_ProfilesDataAccess_LeftBraces + profile.ProfileCode.ToString() + Constants.Model_ProfilesDataAccess_RightBraces
                     //                  }).Distinct().ToList();
                     break;

               }

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetProfiles()
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               lstManufacture = (from manufacture in cloudEntities.Profile
                                 where manufacture.Status.Equals(Constants.Model_ProfilesDataAccess_Active) && manufacture.ProfileCode == (manufacture.RollupProfileCode ?? 0)
                                 orderby manufacture.Name
                                 select new ProfilesName
                                 {
                                    ProfileCode = manufacture.ProfileCode,
                                    Name = manufacture.Name,
                                 }).Distinct().OrderBy(pp => pp.Name).ToList();

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetProfilesUsingAutoComplete(string terms)
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               lstManufacture = (from P1 in cloudEntities.Profile
                                 join P2 in cloudEntities.Profile on P1.ProfileCode equals P2.RollupProfileCode
                                 where P1.ProfileCode == P2.RollupProfileCode && P2.ProfileCode != P2.RollupProfileCode && P1.Name.Contains(terms)
                                 select new ProfilesName
                                 {
                                    ProfileCode = P2.RollupProfileCode,
                                    Name = P1.Name,
                                 }).Distinct().Take(20).ToList();

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetProfilesByType(string profileType)
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (profileType.Equals(Constants.Model_ProfilesDataAccess_QualanexInternal))
               {
                  lstManufacture = (from manufacture in cloudEntities.Profile
                                    where manufacture.Status != Constants.Model_ProfilesDataAccess_Deleted && manufacture.ProfileCode == 0
                                    orderby manufacture.Name
                                    select new ProfilesName
                                    {
                                       ProfileCode = manufacture.ProfileCode,
                                       Name = manufacture.Name,
                                    }).OrderBy(pp => pp.Name).ToList();
               }
               //else if (profileType.Equals(Constants.Model_ProfilesDataAccess_RetailPharmacy))
               //{
               //   lstManufacture = (from manufacture in cloudEntities.Profile
               //                     where manufacture.Status != Constants.Model_ProfilesDataAccess_Deleted && manufacture.Type.Equals(profileType)
               //                     orderby manufacture.Name
               //                     select new ProfilesName
               //                     {
               //                        ProfileCode = manufacture.ProfileCode,
               //                        Name = manufacture.Name,
               //                     }).Take(12000).Distinct().OrderBy(pp => pp.Name).ToList();
               //}
               else
               {
                  var profiles = new List<int>
                  {
                     1000489,
                     1003235,
                     1040390,
                     1003208,
                     1012009,
                     1057454,
                     1044310,
                     1005117,
                     0,
                     100002,
                     1022935,
                     1029697,
                     1024312,
                     1003810,
                     1013513,
                     1013516,
                     1016769,
                     1027778,
                     1016645,
                     1029070,
                     1015266,
                     1055905,
                     1001711
                  };

                  lstManufacture = (from manufacture in cloudEntities.Profile
                                    where manufacture.Status != Constants.Model_ProfilesDataAccess_Deleted && profiles.Contains(manufacture.ProfileCode)
                                    orderby manufacture.Name
                                    select new ProfilesName
                                    {
                                       ProfileCode = manufacture.ProfileCode,
                                       Name = manufacture.Name,
                                    }).Distinct().OrderBy(pp => pp.Name).ToList();

                  //var typeDict = (from profile in cloudEntities.ProfileTypeDict
                  //                where profile.Description == profileType
                  //                select profile.Code).SingleOrDefault();

                  //lstManufacture = (from manufacture in cloudEntities.Profile
                  //                  where manufacture.Status != Constants.Model_ProfilesDataAccess_Deleted && manufacture.ProfileTypeCode.Equals(typeDict)
                  //                  orderby manufacture.Name
                  //                  select new ProfilesName
                  //                  {
                  //                     ProfileCode = manufacture.ProfileCode,
                  //                     Name = manufacture.Name,
                  //                  }).Distinct().OrderBy(pp => pp.Name).ToList();
               }

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="groupType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetProfilesByGroupType(string groupType)
      {
         ICollection<ProfilesName> lstManufacture = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstManufacture = (from grpProfile in cloudEntities.Group
               //                  join manufacture in cloudEntities.Profile on grpProfile.ProfileCode equals manufacture.ProfileCode
               //                  where manufacture.Status != Constants.Model_ProfilesDataAccess_Deleted && grpProfile.GroupType.Equals(groupType)
               //                  orderby manufacture.Name
               //                  select new ProfilesName
               //                  {
               //                     ProfileCode = grpProfile.ProfileGroupID,
               //                     Name = grpProfile.GroupName,
               //                  }).OrderBy(pp => pp.Name).ToList();

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// GetProfile based on profile Code
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ProfileRequest GetProfile(int profileCode, int userRollupProfileCode)
      {
         ProfileRequest objprofile = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               objprofile = (from profile in cloudEntities.Profile
                             where profile.ProfileCode == profileCode
                             select new ProfileRequest
                             {
                                ProfileCode = profile.ProfileCode,
                                Name = profile.Name.Trim(),
                                //Type = profile.Type.Trim(),
                                DisplayOrder = profile.DisplayOrder,
                                DEANumber = profile.DEANumber.Trim(),
                                DEAExpirationDate = profile.DEAExpirationDate, EmailAddress = profile.EmailAddress.Trim(),
                                Address1 = profile.Address1.Trim(),
                                Address2 = profile.Address2.Trim(),
                                Address3 = profile.Address3.Trim(),
                                City = profile.City.Trim(),
                                State = profile.State.Trim(),
                                ZipCode = profile.ZipCode.Trim(),
                                PhoneNumber = profile.PhoneNumber.Trim(),
                                ContactFirstName = profile.ContactFirstName.Trim(),
                                ContactLastName = profile.ContactLastName.Trim(),
                                ContactPhoneNumber = profile.ContactPhoneNumber.Trim(),
                                ContactEmailAddress = profile.ContactEmailAddress.Trim(),
                                ContactFaxNumber = profile.ContactFaxNumber.Trim(),
                                DirectAccountsOnly = profile.DirectAccountsOnly,
                                RollupProfileCode = profile.RollupProfileCode ?? 0,
                                TollFreePhoneNumber = profile.TollFreePhoneNumber,
                                Notes = profile.Notes.Trim(),
                                ReturnProcedures = profile.ReturnProcedures.Trim(),
                                RepresentativeNumber = profile.RepresentativeNumber.Trim(),
                                WholesalerAccountNumber = profile.WholesalerAccountNumber.Trim(),
                                FaxNumber = profile.FaxNumber.Trim(),
                                LotNumberExpirationDateValidationMethod = profile.LotNumberExpirationDateValidationMethod.Trim(),
                                CreatedBy = profile.CreatedBy.Trim(),
                                //CreatedDate = profile.CreatedDate,
                                ModifiedBy = profile.ModifiedBy.Trim(),
                                //ModifiedDate = profile.ModifiedDate,
                                AlertPendingReturnAuthorizations = profile.AlertPendingReturnAuthorizations,
                                AlertCompletedReturnAuthorizations = profile.AlertCompletedReturnAuthorizations,
                                Status = profile.Status.Trim(),
                                RegionCode = profile.RegionCode.Trim(),
                                VendorID = profile.VendorID ?? 0,
                                VendorLoadDate = profile.VendorLoadDate,
                                SalesOrg = profile.SalesOrg,
                                QualanexComDisplay = profile.QualanexComDisplay ?? false,
                                PDMACoordinatorEmailAddress = profile.PDMACoordinatorEmailAddress.Trim(),
                                PDMACoordinatorEmailAddressProcessCompleted = profile.PDMACoordinatorEmailAddressProcessCompleted.Trim(),
                                Version = profile.Version,
                                SpecialHandling = profile.SpecialHandling
                             }).SingleOrDefault();

               //Here we are converting UTC time to local time
               objprofile.DEAExpirationDate = objprofile.DEAExpirationDate != null ? objprofile.DEAExpirationDate.Value.ToLocalTime() : objprofile.DEAExpirationDate;
               objprofile.CreatedDate = objprofile.CreatedDate != null ? objprofile.CreatedDate.ToLocalTime() : objprofile.CreatedDate;
               objprofile.ModifiedDate = objprofile.ModifiedDate != null ? objprofile.ModifiedDate.ToLocalTime() : objprofile.ModifiedDate;

               return objprofile;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Delete Profile
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DeleteProfile(int profileCode, string modifiedby)
      {
         Response objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var prodleItem = (from profle in cloudEntities.Profile
                                     where profle.ProfileCode == profileCode
                                     select profle).SingleOrDefault();

               if (prodleItem != null && prodleItem.Status != Constants.Model_ProfilesDataAccess_Deleted)
               {
                  prodleItem.Status = Constants.Model_ProfilesDataAccess_Deleted;
                  int status = cloudEntities.SaveChanges();

                  if (status > 0)
                  {
                     cloudEntities.SaveChanges();
                     objclsResponse.Status = Status.OK;
                  }
                  else
                  {
                     objclsResponse.Status = Status.Fail;
                  }
               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            objclsResponse.Status = Status.Fail;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="profile"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Profile MapObjectForProfile(Profile profile)
      {
         Profile profileauditItem = new Profile()
         {
            ProfileCode = profile.ProfileCode,
            Name = profile.Name,
            //Type = profile.Type,
            DisplayOrder = profile.DisplayOrder,
            DEANumber = profile.DEANumber,
            DEAExpirationDate = profile.DEAExpirationDate,
            EmailAddress = profile.EmailAddress,
            Address1 = profile.Address1,
            Address2 = profile.Address2,
            Address3 = profile.Address3,
            City = profile.City,
            State = profile.State,
            ZipCode = profile.ZipCode,
            PhoneNumber = profile.PhoneNumber,
            ContactFirstName = profile.ContactFirstName,
            ContactLastName = profile.ContactLastName,
            ContactPhoneNumber = profile.ContactPhoneNumber,
            ContactEmailAddress = profile.ContactEmailAddress,
            ContactFaxNumber = profile.ContactFaxNumber,
            DirectAccountsOnly = profile.DirectAccountsOnly,
            RollupProfileCode = profile.RollupProfileCode ?? 0,
            TollFreePhoneNumber = profile.TollFreePhoneNumber,
            Notes = profile.Notes,
            ReturnProcedures = profile.ReturnProcedures,
            RepresentativeNumber = profile.RepresentativeNumber,
            WholesalerAccountNumber = profile.WholesalerAccountNumber,
            FaxNumber = profile.FaxNumber,
            LotNumberExpirationDateValidationMethod = profile.LotNumberExpirationDateValidationMethod,
            CreatedBy = profile.CreatedBy,
            CreatedDate = profile.CreatedDate,
            ModifiedBy = profile.ModifiedBy,
            ModifiedDate = profile.ModifiedDate,
            AlertPendingReturnAuthorizations = profile.AlertPendingReturnAuthorizations,
            AlertCompletedReturnAuthorizations = profile.AlertCompletedReturnAuthorizations,
            Status = profile.Status,
            RegionCode = profile.RegionCode,
            VendorID = profile.VendorID ?? 0,
            VendorLoadDate = profile.VendorLoadDate,
            SalesOrg = profile.SalesOrg,
            QualanexComDisplay = profile.QualanexComDisplay ?? false,
            PDMACoordinatorEmailAddress = profile.PDMACoordinatorEmailAddress,
            PDMACoordinatorEmailAddressProcessCompleted = profile.PDMACoordinatorEmailAddressProcessCompleted,
            Version = profile.Version
         };

         return profileauditItem;
      }

      ///****************************************************************************
      /// <summary>
      /// Update Profile
      /// </summary>
      /// <param name="profileCode">Profile Code</param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdateProfile(ProfileRequest objProfileRequest)
      {
         Profile profileItem = null;
         Response objclsResponse = new Response();
         //ProfilesAudit profileaudititem = null;
         int SectionNumber = objProfileRequest.SectionNumber;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  profileItem = (from profle in cloudEntities.Profile
                                 where profle.ProfileCode == objProfileRequest.ProfileCode
                                 select profle).SingleOrDefault();

                  if (profileItem.Status != Constants.Model_ProfilesDataAccess_Deleted)
                  {
                     if (profileItem.Version != objProfileRequest.Version)
                     {
                        objclsResponse.response = MapObjectForProfile(profileItem);
                        objclsResponse.Status = Status.VersionChange;
                        objclsResponse.UpdateVersion = profileItem.Version;
                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        switch (SectionNumber)
                        {
                           case 1:
                           {
                              profileItem.Name = !string.IsNullOrEmpty(objProfileRequest.Name) ? objProfileRequest.Name.Trim() : objProfileRequest.Name;
                              //profileItem.Type = !string.IsNullOrEmpty(objProfileRequest.Type) ? objProfileRequest.Type.Trim() : objProfileRequest.Type;
                              profileItem.DEAExpirationDate = objProfileRequest.DEAExpirationDate;
                              profileItem.EmailAddress = objProfileRequest.EmailAddress;
                              profileItem.Address1 = objProfileRequest.Address1;
                              profileItem.Address2 = objProfileRequest.Address2;
                              profileItem.Address3 = objProfileRequest.Address3;
                              profileItem.RegionCode = objProfileRequest.RegionCode;
                              profileItem.DEANumber = objProfileRequest.DEANumber;
                              profileItem.Version = profileItem.Version + 1;

                              //Update Profile Audit table

                              //profileaudititem = MapObject(profileItem);
                              //profileaudititem.Action = Constants.Model_PolicyDataAccess_Update;
                              //profileaudititem.ActionBy = objProfileRequest.ModifiedBy;
                              //profileaudititem.ActionDate = DateTime.UtcNow;
                              //QualanexCloudModelEntities.ProfilesAudit.Add(profileaudititem);
                              int result = cloudEntities.SaveChanges();

                              if (result > 0)
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.OK;
                                 objclsResponse.UpdateVersion = profileItem.Version;
                                 dbContextTransaction.Commit();
                              }
                              else
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.Rollback;
                                 objclsResponse.UpdateVersion = objProfileRequest.Version;
                                 dbContextTransaction.Rollback();
                              }
                           }
                              break;

                           case 2:
                           {
                              profileItem.City = objProfileRequest.City;
                              profileItem.State = objProfileRequest.State;
                              profileItem.ZipCode = objProfileRequest.ZipCode;
                              profileItem.ContactFirstName = objProfileRequest.ContactFirstName;
                              profileItem.ContactLastName = objProfileRequest.ContactLastName;
                              profileItem.ContactPhoneNumber = objProfileRequest.ContactPhoneNumber;
                              profileItem.ContactEmailAddress = objProfileRequest.ContactEmailAddress;
                              profileItem.PhoneNumber = objProfileRequest.PhoneNumber;
                              profileItem.Version = profileItem.Version + 1;

                              //Update Profile Audit table

                              //profileaudititem = MapObject(profileItem);
                              //profileaudititem.Action = Constants.Model_PolicyDataAccess_Update;
                              //profileaudititem.ActionBy = objProfileRequest.ModifiedBy;
                              //profileaudititem.ActionDate = DateTime.UtcNow;
                              //QualanexCloudModelEntities.ProfilesAudit.Add(profileaudititem);

                              int result = cloudEntities.SaveChanges();
                              if (result > 0)
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.OK;
                                 objclsResponse.UpdateVersion = profileItem.Version;
                                 dbContextTransaction.Commit();
                              }
                              else
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.Rollback;
                                 objclsResponse.UpdateVersion = objProfileRequest.Version;
                                 dbContextTransaction.Rollback();
                              }
                           }
                              break;

                           case 3:
                           {
                              profileItem.RollupProfileCode = objProfileRequest.RollupProfileCode;
                              profileItem.TollFreePhoneNumber = !string.IsNullOrEmpty(objProfileRequest.TollFreePhoneNumber) ? objProfileRequest.TollFreePhoneNumber.Trim() : objProfileRequest.TollFreePhoneNumber;
                              profileItem.Notes = !string.IsNullOrEmpty(objProfileRequest.Notes) ? objProfileRequest.Notes.Trim() : objProfileRequest.Notes;
                              profileItem.RepresentativeNumber = !string.IsNullOrEmpty(objProfileRequest.RepresentativeNumber) ? objProfileRequest.RepresentativeNumber.Trim() : objProfileRequest.RepresentativeNumber;
                              profileItem.WholesalerAccountNumber = !string.IsNullOrEmpty(objProfileRequest.WholesalerAccountNumber) ? objProfileRequest.WholesalerAccountNumber.Trim() : objProfileRequest.WholesalerAccountNumber;
                              profileItem.FaxNumber = !string.IsNullOrEmpty(objProfileRequest.FaxNumber) ? objProfileRequest.FaxNumber.Trim() : objProfileRequest.FaxNumber;
                              profileItem.DirectAccountsOnly = objProfileRequest.DirectAccountsOnly;
                              profileItem.LotNumberExpirationDateValidationMethod = objProfileRequest.LotNumberExpirationDateValidationMethod;
                              profileItem.Version = profileItem.Version + 1;

                              //Update Profile Audit table

                              //profileaudititem = MapObject(profileItem);
                              //profileaudititem.Action = Constants.Model_PolicyDataAccess_Update;
                              //profileaudititem.ActionBy = objProfileRequest.ModifiedBy;
                              //profileaudititem.ActionDate = DateTime.UtcNow;
                              //QualanexCloudModelEntities.ProfilesAudit.Add(profileaudititem);

                              int result = cloudEntities.SaveChanges();
                              if (result > 0)
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.OK;
                                 objclsResponse.UpdateVersion = profileItem.Version;
                                 dbContextTransaction.Commit();
                              }
                              else
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.Rollback;
                                 objclsResponse.UpdateVersion = objProfileRequest.Version;
                                 dbContextTransaction.Rollback();
                              }
                           }
                              break;

                           case 4:
                           {
                              profileItem.ReturnProcedures = !string.IsNullOrEmpty(objProfileRequest.ReturnProcedures) ? objProfileRequest.ReturnProcedures.Trim() : objProfileRequest.ReturnProcedures;
                              profileItem.CreatedBy = !string.IsNullOrEmpty(objProfileRequest.CreatedBy) ? objProfileRequest.CreatedBy.Trim() : objProfileRequest.CreatedBy;
                              profileItem.ModifiedBy = objProfileRequest.ModifiedBy;
                              profileItem.ModifiedDate = DateTime.UtcNow;
                              profileItem.AlertPendingReturnAuthorizations = objProfileRequest.AlertPendingReturnAuthorizations;
                              profileItem.AlertCompletedReturnAuthorizations = objProfileRequest.AlertCompletedReturnAuthorizations;
                              profileItem.Status = !string.IsNullOrEmpty(objProfileRequest.Status) ? objProfileRequest.Status.Trim() : objProfileRequest.Status;
                              profileItem.VendorID = objProfileRequest.VendorID;
                              profileItem.VendorLoadDate = objProfileRequest.VendorLoadDate;
                              profileItem.SalesOrg = !string.IsNullOrEmpty(objProfileRequest.SalesOrg) ? objProfileRequest.SalesOrg.Trim() : objProfileRequest.SalesOrg;
                              profileItem.QualanexComDisplay = objProfileRequest.QualanexComDisplay;
                              profileItem.PDMACoordinatorEmailAddress = objProfileRequest.PDMACoordinatorEmailAddress;
                              profileItem.PDMACoordinatorEmailAddressProcessCompleted = objProfileRequest.PDMACoordinatorEmailAddressProcessCompleted;
                              profileItem.Version = profileItem.Version + 1;

                              //Update Profile Audit table
                              //profileaudititem = MapObject(profileItem);
                              //profileaudititem.Action = Constants.Model_PolicyDataAccess_Update;
                              //profileaudititem.ActionBy = objProfileRequest.ModifiedBy;
                              //profileaudititem.ActionDate = DateTime.UtcNow;
                              //QualanexCloudModelEntities.ProfilesAudit.Add(profileaudititem);

                              int result = cloudEntities.SaveChanges();
                              if (result > 0)
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.OK;
                                 objclsResponse.UpdateVersion = profileItem.Version;
                                 dbContextTransaction.Commit();
                              }
                              else
                              {
                                 objclsResponse.response = profileItem;
                                 objclsResponse.Status = Status.Rollback;
                                 objclsResponse.UpdateVersion = objProfileRequest.Version;
                                 dbContextTransaction.Rollback();
                              }
                           }
                              break;
                        }
                     }
                     return objclsResponse;
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }
               }
            }

            return objclsResponse;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);

            objclsResponse.response = profileItem;
            objclsResponse.Status = Status.Fail;
            objclsResponse.UpdateVersion = objProfileRequest.Version;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);

            objclsResponse.response = profileItem;
            objclsResponse.Status = Status.Fail;
            objclsResponse.UpdateVersion = objProfileRequest.Version;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      /// Create Profile and save in database
      /// </summary>
      /// <param name="objProfileRequest"></param>
      ///****************************************************************************
      public static bool RecordProfile(ProfileRequest objProfileRequest)
      {
         using (var cloudEntities = new QoskCloud())
         {
            using (var dbTran = cloudEntities.Database.BeginTransaction())
            {
               try
               {
                  if (objProfileRequest.RollupProfileCode == -1)
                  {
                     objProfileRequest.RollupProfileCode = 0;
                  }
                  var profile = new Profile
                  {
                     Name = objProfileRequest.Name.Trim(),
                     //Type = objProfileRequest.Type.Trim(),
                     DEANumber = objProfileRequest.DEANumber,
                     DEAExpirationDate = objProfileRequest.DEAExpirationDate,
                     EmailAddress = objProfileRequest.EmailAddress,
                     Address1 = objProfileRequest.Address1.Trim(),
                     Address2 = objProfileRequest.Address2,
                     Address3 = objProfileRequest.Address3,
                     City = objProfileRequest.City.Trim(),
                     State = objProfileRequest.State.Trim(),
                     ZipCode = objProfileRequest.ZipCode.Trim(),
                     PhoneNumber = objProfileRequest.PhoneNumber.Trim(),
                     ContactFirstName = objProfileRequest.ContactFirstName.Trim(),
                     ContactLastName = objProfileRequest.ContactLastName.Trim(),
                     ContactPhoneNumber = objProfileRequest.ContactPhoneNumber.Trim(),
                     ContactEmailAddress = objProfileRequest.ContactEmailAddress.Trim(),
                     ContactFaxNumber = objProfileRequest.ContactFaxNumber,
                     DirectAccountsOnly = objProfileRequest.DirectAccountsOnly,
                     RollupProfileCode = objProfileRequest.RollupProfileCode,
                     TollFreePhoneNumber = objProfileRequest.TollFreePhoneNumber,
                     Notes = objProfileRequest.Notes,
                     ReturnProcedures = objProfileRequest.ReturnProcedures,
                     RepresentativeNumber = objProfileRequest.RepresentativeNumber,
                     WholesalerAccountNumber = objProfileRequest.WholesalerAccountNumber,
                     FaxNumber = objProfileRequest.FaxNumber,
                     LotNumberExpirationDateValidationMethod = objProfileRequest.LotNumberExpirationDateValidationMethod,
                     CreatedBy = objProfileRequest.CreatedBy,
                     CreatedDate = DateTime.UtcNow,
                     ModifiedBy = objProfileRequest.ModifiedBy,
                     ModifiedDate = DateTime.UtcNow,
                     AlertPendingReturnAuthorizations = objProfileRequest.AlertPendingReturnAuthorizations,
                     AlertCompletedReturnAuthorizations = objProfileRequest.AlertCompletedReturnAuthorizations,
                     Status = Constants.Model_ProfilesDataAccess_Active,
                     DisplayOrder = 1,
                     RegionCode = objProfileRequest.RegionCode,
                     VendorID = objProfileRequest.VendorID,
                     VendorLoadDate = objProfileRequest.VendorLoadDate,
                     SalesOrg = objProfileRequest.SalesOrg,
                     PDMACoordinatorEmailAddress = objProfileRequest.PDMACoordinatorEmailAddress,
                     PDMACoordinatorEmailAddressProcessCompleted = objProfileRequest.PDMACoordinatorEmailAddressProcessCompleted
                  };

                  cloudEntities.Profile.Add(profile);
                  cloudEntities.SaveChanges();

                  // Saves rollup profile code as profile in case it is not chain
                  if (objProfileRequest.RollupProfileCode == -1)
                  {
                     var profileCode = profile.ProfileCode;
                     var objGroupProfiles = (from p in cloudEntities.Profile
                                             where p.ProfileCode == profileCode
                                             select p).SingleOrDefault();
                     objGroupProfiles.RollupProfileCode = profileCode;

                     cloudEntities.SaveChanges();
                  }

                  // Commit transaction
                  dbTran.Commit();
                  return true;
               }
               catch (DbEntityValidationException dbEx)
               {
                  Logger.DBError(dbEx);
               }
               catch (Exception ex)
               {
                  Logger.Error(ex.Message);
               }

               dbTran.Rollback();
               return false;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get MutipleProfileNameWithCode
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<UserProfileRegion> GetMutipleProfileNameWithCode(int profileCode)
      {
         ICollection<UserProfileRegion> userProfileList = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var userrollupProfileCode = (from p in cloudEntities.Profile
                                            where p.ProfileCode == profileCode
                                            select p.RollupProfileCode ?? 0).SingleOrDefault();

               userProfileList = (from profle in cloudEntities.Profile
                                  where (profle.RollupProfileCode == userrollupProfileCode)
                                  orderby profle.Name
                                  select new UserProfileRegion
                                  {
                                     ProfileCode = profle.ProfileCode,
                                     ProfileName = profle.Name
                                  }).ToList();

               return userProfileList;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// GetManufactureByUserID
      /// </summary>
      /// <param name="objUserSearch"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ManufactureNameList GetManufactureByUserID(UserSearch objUserSearch)
      {
         ICollection<ProfilesName> lstManufacture = null;
         var objManufactureNameList = new ManufactureNameList();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {

               switch (objUserSearch.UserTypeCode)
               {
                  case "ADMN":

                     var grpqry = (from m in cloudEntities.Profile
                                   group m by new {m.RollupProfileCode}
                                   into grp
                                   where grp.Count() > 1
                                   select grp.Key.RollupProfileCode.ToString()).ToArray();

                     lstManufacture = (from manufacture in cloudEntities.Profile
                                       where grpqry.Contains(manufacture.ProfileCode.ToString())
                                       select new ProfilesName
                                       {
                                          ProfileCode = manufacture.ProfileCode,
                                          Name = string.IsNullOrEmpty(manufacture.Name) ? manufacture.ProfileCode.ToString() : manufacture.Name,
                                       }).OrderBy(pp => pp.Name).ToList();
                     break;

                  case "REGN":
                     //var userprofile = (from grps in QualanexCloudModelEntities.Group
                     //                   where grps.ProfileCode == objUserSearch.ProfileCode
                     //                   select grps.ProfileCode.ToString()).ToArray();

                     //lstManufacture = (from manufacture in QualanexCloudModelEntities.Profiles
                     //                  where userprofile.Contains(manufacture.ProfileCode.ToString())
                     //                  select new ProfilesName
                     //                  {
                     //                     ProfileCode = manufacture.ProfileCode,
                     //                     Name = string.IsNullOrEmpty(manufacture.Name) ? manufacture.ProfileCode.ToString() : manufacture.Name,
                     //                  }).OrderBy(pp => pp.Name).ToList();

                     break;

                  case "CORP":
                     var profiles = (from userprl in cloudEntities.Profile
                                     where userprl.RollupProfileCode == objUserSearch.ProfileCode
                                     select userprl.ProfileCode.ToString()).ToArray();

                     lstManufacture = (from manufacture in cloudEntities.Profile
                                       where profiles.Contains(manufacture.ProfileCode.ToString())
                                       select new ProfilesName
                                       {
                                          ProfileCode = manufacture.ProfileCode,
                                          Name = string.IsNullOrEmpty(manufacture.Name) ? manufacture.ProfileCode.ToString() : manufacture.Name,
                                       }).OrderBy(pp => pp.Name).ToList();
                     break;

                  case "SITE":
                     lstManufacture = (from manufacture in cloudEntities.Profile
                                       where manufacture.ProfileCode == objUserSearch.ProfileCode
                                       select new ProfilesName
                                       {
                                          ProfileCode = manufacture.ProfileCode,
                                          Name = string.IsNullOrEmpty(manufacture.Name) ? manufacture.ProfileCode.ToString() : manufacture.Name,
                                       }).OrderBy(pp => pp.Name).ToList();
                     break;

               }

               objManufactureNameList.lstManufacturer = lstManufacture;
               return objManufactureNameList;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get  all RegionCodes
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfileReason> GetRegionCodes(int selectedProfileCode)
      {
         ICollection<ProfileReason> lstRegion = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               lstRegion = (from p in cloudEntities.Profile
                            where p.RollupProfileCode == selectedProfileCode
                            select new ProfileReason
                            {
                               RegionCode = p.RegionCode,
                               RegionName = p.RegionCode
                            }).Distinct().OrderBy(pp => pp.RegionName).ToList();

            }
            return lstRegion;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetAccountManufactureRepackagerType()
      {

         ICollection<ProfilesName> lstManufacture = null;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstManufacture = (from manufacture in cloudEntities.Profile
               //                  where manufacture.Type.Equals(Constants.Model_GroupDataAccess_Manufacturer) || manufacture.Type.Equals(Constants.Model_GroupDataAccess_Repackager)
               //                  orderby manufacture.Name
               //                  select new ProfilesName
               //                  {
               //                     ProfileCode = manufacture.ProfileCode,
               //                     Name = manufacture.Name,
               //                  }).Distinct().OrderBy(pp => pp.Name).ToList();

               return lstManufacture;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetAccountNumber(int manufactureCode, string accountType, int profileCode, int groupId)
      {
         string accountNumber = string.Empty;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //if (profileCode > 0)
               //{
               //   accountNumber = (from account in cloudEntities.DirectIndirectAccounts
               //                    where account.AccountType.Equals(accountType) && account.ManufacturerRepackagerProfileCode == manufactureCode && account.ProfileCode == profileCode
               //                    select account.AccountNumber).FirstOrDefault();
               //}
               //else if (groupId > 0)
               //{
               //   accountNumber = (from account in cloudEntities.DirectIndirectAccounts
               //                    where account.AccountType.Equals(accountType) && account.ManufacturerRepackagerProfileCode == manufactureCode && account.GroupID == groupId
               //                    select account.AccountNumber).FirstOrDefault();
               //}

               return accountNumber;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <param name="accountNumber"></param>
      /// <param name="isupdate"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveUpdateAccountNumber(int manufactureCode, string accountType, int profileCode, int groupId, string accountNumber, string editedBy, bool isupdate, string type)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (!isupdate) // save the account information
               {
                  //DirectIndirectAccounts objDirectIndirectAccounts = new DirectIndirectAccounts
                  //{
                  //   AccountType = accountType.Trim(),
                  //   GroupID = groupId,
                  //   AccountNumber = accountNumber.Trim(),
                  //   ManufacturerRepackagerProfileCode = manufactureCode,
                  //   ProfileCode = profileCode,
                  //   CreatedBy = editedBy.Trim(),
                  //   CreatedDate = DateTime.UtcNow,
                  //   ModifiedDate = DateTime.UtcNow,
                  //   Type = type
                  //};

                  //entities.DirectIndirectAccounts.Add(objDirectIndirectAccounts);
               }
               else
               {
                  //var profileAccount = (from account in cloudEntities.DirectIndirectAccounts
                  //                      where account.AccountType.Equals(accountType) && account.ManufacturerRepackagerProfileCode == manufactureCode
                  //                      select account).FirstOrDefault();

                  //profileAccount.AccountNumber = accountNumber.Trim();
                  //profileAccount.ModifiedDate = DateTime.UtcNow;
                  //profileAccount.ModifiedBy = editedBy.Trim();
               }

               return cloudEntities.SaveChanges() > 0;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="manufactureCode"></param>
      /// <param name="accountType"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupId"></param>
      /// <param name="accountNumber"></param>
      /// <param name="skipFrom"></param>
      /// <param name="takeTo"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfileAccountResponse> SearchAccountPrifile(int manufactureCode, string accountType, int profileCode, string groupId, string accountNumber, string type, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<ProfileAccountResponse> lsProfiles;
         int grpID;
         bool isGroupType = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (manufactureCode > 0)
               {
                  buildquery = buildquery + Constants.Model_ProfilesDataAccess_ManufacturerRepackagerProfileCode + manufactureCode + "";
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (!string.IsNullOrEmpty(accountType))
               {
                  buildquery = buildquery + Constants.Model_ProfilesDataAccess_AccountType + accountType + Constants.PolicyController_COMMA;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (!string.IsNullOrEmpty(type))
               {
                  buildquery = buildquery + Constants.Model_ProfilesDataAccess_TypeWithEqual + type + Constants.PolicyController_COMMA;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (profileCode > 0)
               {
                  buildquery = buildquery + Constants.Model_PolicyDataAccess_ProfileCodeWithEqual + profileCode + "";
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (!string.IsNullOrEmpty(accountNumber))
               {
                  buildquery = buildquery + Constants.Model_ProfilesDataAccess_AccountNumber + accountNumber + Constants.PolicyController_COMMA;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (!string.IsNullOrEmpty(groupId) && groupId != Constants.Model_ProfilesDataAccess_Zero)
               {
                  if (int.TryParse(groupId, out grpID))
                  {
                     buildquery = buildquery + Constants.Model_ProfilesDataAccess_AccountNumber + grpID + "";
                  }
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               isGroupType = type.Contains(Constants.Model_ProfilesDataAccess_GRP);

               if ((!string.IsNullOrEmpty(groupId) && !groupId.Equals(Constants.Model_ProfilesDataAccess_Zero)) || isGroupType)
               {
                  //var detailsresult = cloudModelEntities.USP_ProfileAccountSearch(Constants.Model_ProfilesDataAccess_Profile, buildquery);
                  //lsProfiles = (from profileData in detailsresult
                  //              select new ProfileAccountResponse
                  //              {
                  //                 PharmacyName = profileData.PharmacyName,
                  //                 Type = profileData.AccountGroupType,
                  //                 City = profileData.City,
                  //                 State = profileData.State,
                  //                 AccountType = profileData.AccountType,
                  //                 AccountNumber = profileData.AccountNumber
                  //              }).Distinct().OrderBy(pp => pp.PharmacyName).ToList();
               }
               else
               {
                  //var detailsresult = cloudModelEntities.USP_ProfileAccountSearch(Constants.Model_ProfilesDataAccess_ProfileGroup, buildquery).ToList();

                  //lsProfiles = (detailsresult != null && detailsresult.Count() != 0) ? ((from profileData in detailsresult
                  //                                                                       select new ProfileAccountResponse
                  //                                                                       {
                  //                                                                          PharmacyName = profileData.PharmacyName,
                  //                                                                          Type = profileData.AccountGroupType,
                  //                                                                          City = profileData.City,
                  //                                                                          State = profileData.State,
                  //                                                                          AccountType = profileData.AccountType,
                  //                                                                          AccountNumber = profileData.AccountNumber
                  //                                                                       }).Distinct().OrderBy(pp => pp.PharmacyName).ToList()) : new List<ProfileAccountResponse>();
               }

               if (!string.IsNullOrEmpty(sortorder))
               {
                  //if (sortorder == Constants.Model_GroupDataAccess_Asc)
                  //{
                  //   lsProfiles = lsProfiles.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                  //}
                  //else
                  //{
                  //   lsProfiles = lsProfiles.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                  //}
               }

               //totalrecord = lsProfiles.Count();
               //lsProfiles = lsProfiles.Skip(pagesize * pagenum).Take(pagesize);
               //totalrecord = lsProfiles.Count();
               //return lsProfiles.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         totalrecord = 0;
         return null;
      }
   }
}
