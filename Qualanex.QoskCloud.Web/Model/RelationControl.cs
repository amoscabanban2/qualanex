﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace Qualanex.QoskCloud.Web.Model
{
   public class RelationControl
   {

      /// <summary>
      /// List of objects whose properties will be used as the display elements in the relation control
      /// </summary>
      public IEnumerable<object> Rows { get; set; }

      /// <summary>
      /// Optional set of options for enhancing the display of the relation control
      /// </summary>
      public RelationControlOptions Options { get; set; } = new RelationControlOptions();

   }
   /// <summary>
   /// A class for setting optional properties of a relation control
   /// </summary>
   public class RelationControlOptions
   {
      /// <summary>
      /// Javascript method or Ajax endpoint for the method when selecting a row.
      /// If null or empty, update will be disabled.
      /// </summary>
      public string SelectMethod { get; set; }

      /// <summary>
      /// Javascript method or Ajax endpoint for the method when updating a row.
      /// If null or empty, update will be disabled.
      /// </summary>
      public string UpdateMethod { get; set; }

      /// <summary>
      /// Javascript method or Ajax endpoint for the method when adding a row.
      /// If null or empty, add will be disabled.
      /// </summary>
      public string AddMethod { get; set; }

      /// Get Property of Row.
      /// If null or empty, add will be disabled.
      /// </summary>
      public object RowProperty { get; set; }

      /// <summary>
      /// Javascript method or Ajax endpoint for the method when deleting a row
      /// If null or empty, Delete will be disabled.
      /// </summary>
      public string DeleteMethod { get; set; }

      /// <summary>
      /// List of properties to not display in the relation control
      /// These will still be returned in update and delete calls
      /// making them useful for internal IDs
      /// </summary>
      public List<string> HiddenProperties { get; set; } = new List<string>();

      /// <summary>
      /// Provides options for any columns that should allow edit/add as Select lists,
      /// otherwise editing will be free text
      /// </summary>
      public Dictionary<string, IEnumerable<SelectListItem>> SelectLists { get; set; } = new Dictionary<string, IEnumerable<SelectListItem>>();

      /// <summary>
      /// List of properties that will remain readonly in edit/add views
      /// </summary>
      public List<string> ReadOnlyProperties { get; set; } = new List<string>();


      /// <summary>
      /// Set false to enable available add/edit functionality on initial load
      /// </summary>
      public bool InitialReadOnly { get; set; } = true;

      /// <summary>
      /// Set false to hide row level edit/add/update/cancel controls
      /// </summary>
      public bool ShowRowControls { get; set; }

      /// <summary>
      /// Pixel height of scrollable element, if 0 or less, the control will show all 
      /// elements without scrolling
      /// </summary>
      public int ScrollHeight { get; set; } = 0;

      /// <summary>
      /// Pixel height of scrollable element, if 0 or less, the control will show all 
      /// elements without scrolling
      /// </summary>
      public List<CustomControl> CustomControls { get; set; } = new List<CustomControl>();
      
   }

   /// <summary>
   /// A class for create custom control for relation control
   /// </summary>
   public class CustomControl
   {
      /// <summary>
      /// List of button/icon/text that will remain enable available in edit views
      /// </summary>
      public string EditView { get; set; }

      /// <summary>
      /// List of button/icon/text that will remain enable available in add views
      /// </summary>
      public string AddView { get; set; }

      /// <summary>
      /// List of button/icon/text that will remain readonly in edit/add views
      /// </summary>
      public string ReadOnlyView { get; set; }
   }
}