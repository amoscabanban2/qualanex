﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qualanex.QoskCloud.Web.Model
{
   public class VisualReport
   {
      // Get List of result as String
      public string lst { get; set; }
      // Get result from rows
      public List<object> rows { get; set; } = new List<object>();
      // Get Columns
      public List< object> cols { get; set; } = new List<object>();
      // return count of row
      public int rowsCount { get; set; }
   }
}