﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace Qualanex.QoskCloud.Web.Model
{
    /// <summary>
    ///  Application Db Context
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_DefaultConnection)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_User);
            modelBuilder.Entity<IdentityUserRole>().ToTable(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_UserRole);
            modelBuilder.Entity<IdentityUserLogin>().ToTable(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_UserLogin);
            modelBuilder.Entity<IdentityUserClaim>().ToTable(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_UserClaim).Property(p => p.Id).HasColumnName(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_UserClaimId);
            modelBuilder.Entity<IdentityRole>().ToTable(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_Role).Property(p => p.Id).HasColumnName(Qualanex.QoskCloud.Utility.Constants.Model_ApplicationUser_RoleId);
        }

    }
}