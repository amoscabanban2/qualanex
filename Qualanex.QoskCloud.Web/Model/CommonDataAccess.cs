﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="CommonDataAccess.cs">
///   Copyright (c) 2015 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Model
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class CommonDataAccessLayer
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="tableName"></param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static List<string> GetColumnListOftable(string tableName)
      {
         switch (tableName)
         {
            // ...
            //=============================================================================
            case Constants.Model_CommonDataAccess_Products:
               return GetColumnDetails<Product>();

            // ...
            //=============================================================================
            case Constants.Model_CommonDataAccess_LotNumbers:
               return GetColumnDetails<Lot>();
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static List<string> GetColumnDetails<T>()
      {
         return typeof(T).GetProperties().Select(a => a.Name).ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///   Get configuration setting(s).
      /// </summary>
      /// <param name="groupName"></param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static List<AppConfigurationSetting> QoskCloudConfigurationSetting(string groupName)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from ac in cloudEntities.AppConfig
                       where string.Compare(ac.GroupName, groupName, StringComparison.OrdinalIgnoreCase) == 0
                       select new AppConfigurationSetting
                       {
                          Value = ac.Value,
                          keyName = ac.KeyName,
                          GroupName = ac.GroupName
                       }).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Logged-in User Information.
      /// </summary>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UserRegistrationRequest GetUserData()
      {
         return ((ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink)).UserReturnData;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="query">
      ///   Description not available.
      /// </param>
      /// <param name="productsSearch">
      ///   Description not available.
      /// </param>
      /// <param name="lotSearch">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string BuildQuery(System.Collections.Specialized.NameValueCollection query, List<FilterClass> productsSearch, List<FilterClass> lotSearch)
      {
         // User Level Access
         var userRegistrationRequest = CommonDataAccessLayer.GetUserData();

         if (userRegistrationRequest.ProfileCode != 0)
         {
            var profilecode = CommonDataAccessLayer.GetProfileCode(userRegistrationRequest.ProfileCode);

            productsSearch.Add(new FilterClass()
            {
               Property = Constants.Model_CommonDataAccess_ProfileCode,
               Value = profilecode,
               FilterType = Constants.Model_CommonDataAccess_In
            });
         }

         if (lotSearch != null)
         {
            productsSearch.AddRange(lotSearch);
         }

         var filtersCount = int.Parse(query.GetValues(Constants.PolicyController_filterscount)[0]);
         var queryString = "";
         var tmpDataField = "";
         var tmpFilterOperator = "";
         var where = "";

         if (productsSearch.Count > 0 || filtersCount > 0)
         {
            where = Constants.PolicyController_WHERE;
         }

         if (productsSearch.Count > 0)
         {
            for (var i = 0; i < productsSearch.Count; i += 1)
            {
               var filterDataField = productsSearch[i].Property;
               var filterCondition = productsSearch[i].FilterType.ToUpper();
               var filterValue = productsSearch[i].Value;

               if (string.IsNullOrEmpty(tmpDataField))
               {
                  tmpDataField = filterDataField;
               }
               else
               {
                  where += Constants.PolicyController_ANDWITHBREKET;
               }

               where += CommonDataAccessLayer.GetFilterCondition(filterCondition.ToString(), filterDataField.ToString(), filterValue.ToString());

               if (i == productsSearch.Count - 1)
               {
                  if (filtersCount == 0)
                  {
                     where += Constants.PolicyController_BREKET;
                  }
               }

               tmpDataField = filterDataField;
            }

            if (filtersCount > 0)
            {
               for (var i = 0; i < filtersCount; i += 1)
               {
                  var filterValue = query.GetValues(Constants.PolicyController_filtervalue + i)[0];
                  var filterCondition = query.GetValues(Constants.PolicyController_filtercondition + i)[0];
                  var filterDataField = query.GetValues(Constants.PolicyController_filterdatafield + i)[0];
                  var filterOperator = query.GetValues(Constants.PolicyController_filteroperator + i)[0];

                  if (string.IsNullOrEmpty(tmpDataField))
                  {
                     tmpDataField = filterDataField;
                  }
                  else if (tmpDataField != filterDataField)
                  {
                     where += Constants.PolicyController_ANDWITHBREKET;
                  }
                  else if (tmpDataField == filterDataField)
                  {
                     if (string.IsNullOrEmpty(tmpFilterOperator))
                     {
                        where += Constants.PolicyController_ANDWITHSPACE;
                     }
                     else
                     {
                        where += Constants.PolicyController_OR;
                     }
                  }

                  where += CommonDataAccessLayer.GetFilterCondition(filterCondition, filterDataField, filterValue);

                  if (i == filtersCount - 1)
                  {
                     where += Constants.PolicyController_BREKET;
                  }

                  tmpFilterOperator = filterOperator;
                  tmpDataField = filterDataField;
               }
            }
         }
         else
         {
            if (filtersCount > 0)
            {
               for (var i = 0; i < filtersCount; i += 1)
               {
                  var filterValue = query.GetValues(Constants.PolicyController_filtervalue + i)[0];
                  var filterCondition = query.GetValues(Constants.PolicyController_filtercondition + i)[0];
                  var filterDataField = query.GetValues(Constants.PolicyController_filterdatafield + i)[0];
                  var filterOperator = query.GetValues(Constants.PolicyController_filteroperator + i)[0];

                  if (string.IsNullOrEmpty(tmpDataField))
                  {
                     tmpDataField = filterDataField;
                  }
                  else if (tmpDataField != filterDataField)
                  {
                     where += Constants.PolicyController_ANDWITHBREKET;
                  }
                  else if (tmpDataField == filterDataField)
                  {
                     if (string.IsNullOrEmpty(tmpFilterOperator))
                     {
                        where += Constants.PolicyController_ANDWITHSPACE;
                     }
                     else
                     {
                        where += Constants.PolicyController_OR;
                     }
                  }

                  where += CommonDataAccessLayer.GetFilterCondition(filterCondition, filterDataField, filterValue);

                  if (i == filtersCount - 1)
                  {
                     where += Constants.PolicyController_BREKET;
                  }

                  tmpFilterOperator = filterOperator;
                  tmpDataField = filterDataField;
               }
            }
         }

         queryString += where;
         return queryString;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="query">
      ///   Description not available.
      /// </param>
      /// <param name="productsSearch">
      ///   Description not available.
      /// </param>
      /// <param name="lotSearch">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string BuildQueryProduct(System.Collections.Specialized.NameValueCollection query, List<FilterClass> productsSearch, List<FilterClass> lotSearch)
      {
         //User Level Access
         var userRegistrationRequest = GetUserData();

         if (userRegistrationRequest.ProfileCode != 0)
         {
            var _profilecode = GetProfileCode(userRegistrationRequest.ProfileCode);

            productsSearch.Add(new FilterClass
            {
               Property = Constants.Model_CommonDataAccess_ProfileCode,
               Value = _profilecode,
               FilterType = Constants.Model_CommonDataAccess_In
            });
         }

         if (lotSearch.Count > 0)
         {
            var productwithcommaseparated = GetProductIDWithComma(lotSearch[0].Value.ToString());

            productsSearch.Add(new FilterClass
            {
               Property = Constants.Model_CommonDataAccess_ProductID,
               Value = productwithcommaseparated,
               FilterType = Constants.Model_CommonDataAccess_In
            });
         }

         var filtersCount = int.Parse(query.GetValues(Constants.PolicyController_filterscount)[0]);
         var queryString = "";
         var tmpDataField = "";
         var tmpFilterOperator = "";
         var where = "";

         if (productsSearch.Count > 0 || filtersCount > 0)
         {
            where = Constants.PolicyController_WHERE;
         }

         if (productsSearch.Count > 0)
         {
            for (var i = 0; i < productsSearch.Count; i += 1)
            {
               var filterDataField = productsSearch[i].Property;
               var filterCondition = productsSearch[i].FilterType.ToUpper();
               var filterValue = productsSearch[i].Value;

               if (string.IsNullOrEmpty(tmpDataField))
               {
                  tmpDataField = filterDataField;
               }
               else
               {
                  where += Constants.PolicyController_ANDWITHBREKET;
               }

               where += CommonDataAccessLayer.GetFilterCondition(filterCondition.ToString(), filterDataField.ToString(), filterValue.ToString());

               if (i == productsSearch.Count - 1)
               {
                  if (filtersCount == 0)
                  {
                     where += Constants.PolicyController_BREKET;
                  }
               }

               tmpDataField = filterDataField;
            }

            if (filtersCount > 0)
            {
               for (var i = 0; i < filtersCount; i += 1)
               {
                  var filterValue = query.GetValues(Constants.PolicyController_filtervalue + i)[0];
                  var filterCondition = query.GetValues(Constants.PolicyController_filtercondition + i)[0];
                  var filterDataField = query.GetValues(Constants.PolicyController_filterdatafield + i)[0];
                  var filterOperator = query.GetValues(Constants.PolicyController_filteroperator + i)[0];

                  if (string.IsNullOrEmpty(tmpDataField))
                  {
                     tmpDataField = filterDataField;
                  }
                  else if (tmpDataField != filterDataField)
                  {
                     where += Constants.PolicyController_ANDWITHBREKET;
                  }
                  else if (tmpDataField == filterDataField)
                  {
                     if (string.IsNullOrEmpty(tmpFilterOperator))
                     {
                        where += Constants.PolicyController_ANDWITHSPACE;
                     }
                     else
                     {
                        where += Constants.PolicyController_OR;
                     }
                  }

                  where += CommonDataAccessLayer.GetFilterCondition(filterCondition, filterDataField, filterValue);

                  if (i == filtersCount - 1)
                  {
                     where += Constants.PolicyController_BREKET;
                  }

                  tmpFilterOperator = filterOperator;
                  tmpDataField = filterDataField;
               }
            }
         }
         else
         {
            if (filtersCount > 0)
            {
               for (var i = 0; i < filtersCount; i += 1)
               {
                  var filterValue = query.GetValues(Constants.PolicyController_filtervalue + i)[0];
                  var filterCondition = query.GetValues(Constants.PolicyController_filtercondition + i)[0];
                  var filterDataField = query.GetValues(Constants.PolicyController_filterdatafield + i)[0];
                  var filterOperator = query.GetValues(Constants.PolicyController_filteroperator + i)[0];

                  if (string.IsNullOrEmpty(tmpDataField))
                  {
                     tmpDataField = filterDataField;
                  }
                  else if (tmpDataField != filterDataField)
                  {
                     where += Constants.PolicyController_ANDWITHBREKET;
                  }
                  else if (filterDataField == Constants.Model_CommonDataAccess_ProfileCode)
                  {
                     if (string.IsNullOrEmpty(tmpFilterOperator))
                     {
                        where += Constants.PolicyController_ANDWITHSPACE;
                     }
                     else
                     {
                        where += Constants.PolicyController_OR;
                     }
                  }
                  else if (tmpDataField == filterDataField)
                  {
                     if (string.IsNullOrEmpty(tmpFilterOperator))
                     {
                        where += Constants.PolicyController_ANDWITHSPACE;
                     }
                     else
                     {
                        where += Constants.PolicyController_OR;
                     }
                  }

                  where += CommonDataAccessLayer.GetFilterCondition(filterCondition, filterDataField, filterValue);

                  if (i == filtersCount - 1)
                  {
                     where += Constants.PolicyController_BREKET;
                  }

                  tmpFilterOperator = filterOperator;
                  tmpDataField = filterDataField;
               }
            }
         }

         queryString += where;
         return queryString;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="filterCondition">
      ///   Description not available.
      /// </param>
      /// <param name="filterDataField">
      ///   Description not available.
      /// </param>
      /// <param name="filterValue">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string GetFilterCondition(string filterCondition, string filterDataField, string filterValue)
      {
         switch (filterCondition)
         {
            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EMPTY:
            case Constants.PolicyController_NOT_NULL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKE;

            // ...
            //=============================================================================
            case Constants.PolicyController_EMPTY:
            case Constants.PolicyController_NULL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKE;

            // ...
            //=============================================================================
            case Constants.PolicyController_CONTAINS_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_CONTAINS:
               if (filterDataField.Contains(Constants.Model_CommonDataAccess_NDCUPC))
               {
                  return Constants.PolicyController_Space + Constants.Model_CommonDataAccess_NDCUPCWithDashes + filterValue + Constants.Model_CommonDataAccess_NDCLikePercent + filterValue + Constants.Model_CommonDataAccess_UPCLikePercent + filterValue + Constants.PolicyController_PERCENT;
               }

               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_DOES_NOT_CONTAIN_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKEPERCENT + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_DOES_NOT_CONTAIN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTLIKEPERCENT + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_EQUAL_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_EQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_EQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EQUAL_CASE_SENSITIVE:
               return Constants.PolicyController_BINARY + filterDataField + Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_NOT_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_NOTEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_GREATER_THAN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_GRATERTHENWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_LESS_THAN:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LESSTHENWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_GREATER_THAN_OR_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_GRATERTHENEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_LESS_THAN_OR_EQUAL:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LESSTHENEQUALWITHCOMMA + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.PolicyController_STARTS_WITH_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEWITHCOMMA + filterValue + Constants.PolicyController_PERCENT + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_STARTS_WITH:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEWITHCOMMA + filterValue + Constants.PolicyController_PERCENT;

            // ...
            //=============================================================================
            case Constants.PolicyController_ENDS_WITH_CASE_SENSITIVE:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_COMMA + Constants.PolicyController_SQLLATIN1;

            // ...
            //=============================================================================
            case Constants.PolicyController_ENDS_WITH:
               return Constants.PolicyController_Space + filterDataField + Constants.PolicyController_LIKEPERCENT + filterValue + Constants.PolicyController_COMMA;

            // ...
            //=============================================================================
            case Constants.Model_CommonDataAccess_In:
               return Constants.PolicyController_Space + filterDataField + Constants.Model_CommonDataAccess_INWithLeftBraket + filterValue + Constants.Model_CommonDataAccess_RightBraket;
         }

         return "";
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="profileCode">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string GetProfileCode(int? profileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return string.Join(",", cloudEntities.Profile.Where(p => p.RollupProfileCode == profileCode).Select(p => p.ProfileCode));
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="lotNumber">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string GetProductIDWithComma(string lotNumber)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return string.Join(",", cloudEntities.Lot.Where(p => p.LotNumber.Equals(lotNumber)).Select(p => p.ProductID));
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="GUIDUserID">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static long GetCurrentUserID(string GUIDUserID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var objUser = (from user in cloudEntities.User
                              where user.Id.Equals(GUIDUserID)
                              select user).FirstOrDefault();

               return objUser?.UserID ?? -1;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }
   }

}