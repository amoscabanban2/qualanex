﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="PolicyDataAccess.cs">
///   Copyright (c) 2015 - 2017, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;
   using System.Linq.Dynamic;
   using System.Runtime.InteropServices;
   using System.Text.RegularExpressions;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;
   using Qualanex.QoskCloud.Web.Model;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   internal class PolicyDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   Get Profile details for assigning policy in policy screen
      /// </summary>
      /// <param name="profileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfileRequest> GetProfileDetailsBasedOnProfileType(string profileType)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from p in cloudEntities.Profile
                       join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
                       where profileType.Contains(ptd.Description)
                       select new ProfileRequest
                       {
                          Name = p.Name,
                          ProfileCode = p.ProfileCode,
                          Address1 = p.Address1,
                          Address2 = p.Address2,
                          Address3 = p.Address3,
                          City = p.City,
                          State = p.State,
                          ZipCode = p.ZipCode,
                          Type = ptd.Description
                       }).Distinct().OrderBy(xp => xp.Name).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get List of Manufacture, Repackager, Wholesaler based on parameter type
      ///   like wholesaler,manufacturer etc
      /// </summary>
      /// <param name="profileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IList<ProfilesName> GetProfileListByType(string profileType)
      {
         try
         {
            if (!string.IsNullOrEmpty(profileType))
            {
               //=============================================================================
               // This function makes the assumption that if any of the elements in the array
               // of profiles contains the word ‘Group’, then the list of names returned is    ----+
               // from the ProfileGroup table and not the actual Profile table.                    |
               //-----------------------------------------------------------------------------     |
               //                                                                                  |
               //                                                                                  |
               var profileTypeArray = profileType.Split(',');                    //                |
                                                                                 //                |
               using (var cloudEntities = new QoskCloud())                       //                |
               {                                                                 //                |
                  if (profileTypeArray.Any(x => x.Contains(Constants.Group)))    // <--------------+
                  {
                     return (from pg in cloudEntities.ProfileGroup
                             join pgtd in cloudEntities.ProfileGroupTypeDict on pg.ProfileGroupTypeCode equals pgtd.Code
                             where profileTypeArray.Contains(pgtd.Description)
                             select new ProfilesName
                             {
                                ProfileGroupID = pg.ProfileGroupID,
                                GroupName = pg.GroupName,
                             }).Distinct().OrderBy(xp => xp.GroupName).ToList();
                  }

                  return (from p in cloudEntities.Profile
                          join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
                          where profileTypeArray.Contains(ptd.Description)
                          select new ProfilesName
                          {
                             ProfileCode = p.ProfileCode,
                             Name = profileType == Constants.Wholesaler
                                ? p.Name + Constants.Model_PolicyDataAccess_LeftBraketWithSpace + p.City + Constants.Model_PolicyDataAccess_RightBraketWithSpace
                                : p.Name,
                             City = p.City,
                             State = p.State
                          }).Distinct().OrderBy(xp => xp.Name).ToList();
               }
            }

            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      /////****************************************************************************
      ///// <summary>
      /////
      ///// </summary>
      ///// <param name="profileType"></param>
      ///// <param name="POneType"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static IList<ProfilesName> GetProfileListByType(string profileType, string POneType)
      //{
      //   try
      //   {
      //      if (!string.IsNullOrEmpty(profileType))
      //      {
      //         var profileCodeArray = profileType.Split(',');

      //         using (var cloudEntities = new QoskCloud())
      //         {
      //            return (from p in cloudEntities.Profile
      //                    join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
      //                    where profileCodeArray.Contains(ptd.Description)
      //                    select new ProfilesName
      //                    {
      //                       ProfileCode = p.ProfileCode,
      //                       Name = p.Name,
      //                    }).Distinct().OrderBy(xp => xp.Name).ToList();
      //         }
      //      }

      //      return null;
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      /////****************************************************************************
      ///// <summary>
      ///// Get Profile List ByType
      ///// </summary>
      ///// <param name="profileType"></param>
      ///// <param name="policyID"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static IList<ProfilesName> GetProfileListByType(string profileType, long policyID)
      //{
      //   try
      //   {
      //      switch (profileType)
      //      {
      //         // ...
      //         //=============================================================================
      //         case Constants.ManufacturerGroup:
      //            using (var cloudEntities = new QoskCloud())
      //            {
      //               return (from grp in cloudEntities.ProfileGroup
      //                       where grp.ProfileGroupTypeCode.Equals(Constants.MFGRGRP)
      //                       select new ProfilesName
      //                       {
      //                          ProfileGroupID = grp.ProfileGroupID,
      //                          GroupName = grp.GroupName,
      //                          //ProfileCode = grp.ProfileCode,
      //                       }).Distinct().OrderBy(p => p.GroupName).ToList();
      //            }

      //         // ...
      //         //=============================================================================
      //         case Constants.WholesalerGroup:
      //            using (var cloudEntities = new QoskCloud())
      //            {
      //               return (from grp in cloudEntities.ProfileGroup
      //                       where grp.ProfileGroupTypeCode.Equals(Constants.WHLSGRP)
      //                       select new ProfilesName
      //                       {
      //                          ProfileGroupID = grp.ProfileGroupID,
      //                          GroupName = grp.GroupName,
      //                          //ProfileCode = grp.ProfileCode,
      //                       }).Distinct().OrderBy(p => p.GroupName).ToList();
      //            }

      //         // ...
      //         //=============================================================================
      //         default:
      //            using (var cloudEntities = new QoskCloud())
      //            {
      //               return null;
      //               //return (from manufacture in cloudEntities.Profile
      //               //         join ptd in cloudEntities.ProfileTypeDict on manufacture.ProfileTypeCode equals ptd.Code
      //               //         join ppt in cloudEntities.PolicyRel.Where(pptt => pptt.PolicyCode == policyCode) on manufacture.ProfileCode equals ppt.ProfileCode into leftprofile
      //               //          from pleft in leftprofile.DefaultIfEmpty()
      //               //           where ptd.Description.Equals(profileType)
      //               //            select new ProfilesName
      //               //                  {
      //               //                     ProfileCode = manufacture.ProfileCode,
      //               //                     SelectedProfileCode = pleft.ProfileCode,
      //               //                     Name = profileType == Constants.Wholesaler
      //               //                                             ? manufacture.Name + Constants.Model_PolicyDataAccess_LeftBraketWithSpace + manufacture.City + Constants.Model_PolicyDataAccess_RightBraketWithSpace
      //               //                                             : manufacture.Name,
      //               //                  }).Distinct().OrderBy(xp => xp.Name).ToList();
      //            }
      //      }
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      /////****************************************************************************
      ///// <summary>
      ///// GetProfileType get Profile details based on Profile Type
      ///// </summary>
      ///// <param name="profileType"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static IList<ProfilesName> GetProfileType(string profileType)
      //{
      //   try
      //   {
      //      using (var cloudEntities = new QoskCloud())
      //      {
      //         return (from p in cloudEntities.Profile
      //                 join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
      //                 where ptd.Description.Equals(profileType)
      //                 select new ProfilesName
      //                 {
      //                    ProfileCode = p.ProfileCode,
      //                    Name = p.Name,
      //                 }).OrderBy(xp => xp.Name).ToList();
      //      }
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      /////****************************************************************************
      ///// <summary>
      /////   Get All Product Details  based on selected manufacture profileCode
      ///// </summary>
      ///// <param name="profileCode"></param>
      /////****************************************************************************
      //public static List<ProductsML> GetProductDetailsBasedOnMultipleManufacture(string profileCode)
      //{
      //   try
      //   {
      //      string[] ProfileCodeArray = null;

      //      if (!string.IsNullOrEmpty(profileCode))
      //      {
      //         ProfileCodeArray = profileCode.Split(',');
      //      }

      //      using (var cloudEntities = new QoskCloud())
      //      {
      //         return (from p in cloudEntities.Product
      //                 join prf in cloudEntities.Profile on p.ProfileCode equals prf.ProfileCode
      //                 where ProfileCodeArray.Contains(p.ProfileCode.ToString())
      //                 select new ProductsML
      //                 {
      //                    ProductID = p.ProductID,
      //                    ManufactureRepackagerName = prf.Name,
      //                    ProfileCode = p.ProfileCode,
      //                    NDCUPCWithDashes = p.NDCUPCWithDashes,
      //                    UPC = p.UPC,
      //                    Description = p.Description,
      //                    Strength = p.Strength,
      //                    //DosageCode = p.DosageCode,
      //                    ControlNumber = p.ControlNumber
      //                 }).ToList();
      //      }
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      /////****************************************************************************
      ///// <summary>
      /////   Get Product with Associated Lot Number
      ///// </summary>
      ///// <param name="profileCode"></param>
      ///// <param name="typeDetails"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static List<ProductWithLot> GetProductWithAssociatedLot(string profileCode, string typeDetails)
      //{
      //   try
      //   {
      //      string[] ProfileCodeArray = null;

      //      if (!string.IsNullOrEmpty(profileCode))
      //      {
      //         ProfileCodeArray = profileCode.Split(',');
      //      }

      //      var allOrder = new List<ProductWithLot>();

      //      // here MyDatabaseEntities is our data context
      //      using (var cloudEntities = new QoskCloud())
      //      {
      //         var o = (cloudEntities.Product.OrderByDescending(a => a.ProductID)).Where(p => ProfileCodeArray.Contains(p.ProfileCode.ToString()));

      //         foreach (var i in o)
      //         {
      //            var od = (from l in cloudEntities.Lot where l.ProductID == i.ProductID select l).ToList();
      //            //allOrder.Add(new ProductWithLot { Order = i, OrderDetails = od });
      //         }
      //      }

      //      return allOrder;
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      /////****************************************************************************
      ///// <summary>
      /////   Get Product With Associated LotNumber
      ///// </summary>
      ///// <param name="profileType"></param>
      ///// <returns></returns>
      /////****************************************************************************
      //public static List<EditProductWithLot> GetProductWithAssociatedLotNew(string profileType)
      //{
      //   try
      //   {
      //      string[] ProfileCodeArray = null;

      //      if (!string.IsNullOrEmpty(profileType))
      //      {
      //         ProfileCodeArray = profileType.Split(',');
      //      }

      //      var allOrder = new List<EditProductWithLot>();

      //      // here MyDatabaseEntities is our data context
      //      using (var cloudEntities = new QoskCloud())
      //      {
      //         //var o = (cloudEntities.Types.OrderBy(a => a.Type)).Where(p => ProfileCodeArray.Contains(p.Type.ToString()));

      //         //foreach (var i in o)
      //         //{
      //         //   var od = (from l in cloudEntities.Profiles
      //         //             where l.Type == i.Type
      //         //             select new ProfileRequest
      //         //             {
      //         //                Name = l.Name,
      //         //                City = l.City,
      //         //                Address1 = l.Address1,
      //         //                State = l.State,
      //         //                ProfileCode = l.ProfileCode,
      //         //             }).Distinct().OrderBy(p => p.Name).ToList();
      //         //   allOrder.Add(new EditProductWithLot {PrfType = i, PrfDetails = od});
      //         //}
      //      }

      //      return allOrder;
      //   }
      //   catch (DbEntityValidationException dbEx)
      //   {
      //      Logger.DBError(dbEx);
      //      throw;
      //   }
      //   catch (Exception ex)
      //   {
      //      Logger.Error(ex.Message);
      //      throw;
      //   }
      //}

      ///****************************************************************************
      /// <summary>
      ///   Get Pharmacy profile for create policy screen
      /// </summary>
      /// <param name="profileType"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfileRequest> GetPharmacyForCreatePolicy(string profileType, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         totalrecord = 0;

         try
         {
            IEnumerable<ProfileRequest> lstPr = new List<ProfileRequest>();

            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (!string.IsNullOrEmpty(profileType))
               {
                  buildquery += (Constants.Model_PolicyDataAccess_Type + profileType + Constants.PolicyController_COMMA);
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var objPharmacy = cloudEntities.USP_GetNDCDetailsWithProfiles(buildquery);

               //lstPr = (from manufacture in objPharmacy
               //         select new ProfileRequest
               //         {
               //            Name = manufacture.Name,
               //            City = manufacture.City,
               //            Address1 = manufacture.Address1,
               //            State = manufacture.State,
               //            ProfileCode = manufacture.ProfileCode,
               //         }).ToList();

               if (!string.IsNullOrEmpty(sortorder))
               {
                  lstPr = sortorder == Constants.Model_GroupDataAccess_Asc
                              ? lstPr.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                              : lstPr.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               }

               totalrecord = lstPr.Count();
               lstPr = lstPr.Skip(pagesize * pagenum).Take(pagesize);

               return lstPr.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Lot Details Based On ProductID selected
      /// </summary>
      /// <param name="productIDs"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProductsML> GetLotBasedOnProductID(string productIDs)
      {
         try
         {
            if (!string.IsNullOrEmpty(productIDs))
            {
               var productCodeArray = productIDs.Split(',');

               using (var cloudEntities = new QoskCloud())
               {
                  return (from lot in cloudEntities.Lot
                          join prd in cloudEntities.Product on lot.ProductID equals prd.ProductID
                          join prf in cloudEntities.Profile on prd.ProfileCode equals prf.ProfileCode
                          where productCodeArray.Contains(prd.ProductID.ToString())
                          select new ProductsML
                          {
                             ProductID = prd.ProductID,
                             NDCUPCWithDashes = prd.NDCUPCWithDashes,
                             LotNumber = lot.LotNumber,
                             ManufactureRepackagerName = prf.Name
                          }).ToList();
               }
            }

            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get TypeDetails
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static IList<string> GetTypeDetails()
      {
         try
         {
            // Create the static list of types we're looking for
            var profileTypeArray = new[]
            {
               Constants.Model_PolicyDataAccess_RetailPharmacy,
               Constants.Model_PolicyDataAccess_Wholesaler,
               Constants.Model_PolicyDataAccess_ChainPharmacy,
               Constants.Model_PolicyDataAccess_HospitalClinic
            };

            using (var cloudEntities = new QoskCloud())
            {
               return (from p in cloudEntities.Profile
                       join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
                       where profileTypeArray.Contains(ptd.Description)
                       select ptd.Description).Distinct().ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get TypeDetails
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static IList<string> GetPublishProfileType()
      {
         try
         {
            // Create the static list of types we're looking for
            var profileTypeArray = new[]
            {
               Constants.Manufacturer,
               Constants.Wholesaler,
               Constants.Repackager
            };

            using (var cloudEntities = new QoskCloud())
            {
               return (from p in cloudEntities.Profile
                       join ptd in cloudEntities.ProfileTypeDict on p.ProfileTypeCode equals ptd.Code
                       where profileTypeArray.Contains(ptd.Description)
                       select ptd.Description).Distinct().ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Apply the policy for wholesaler, repackager based on selected pharmacy
      ///   and wholesaler in policy screen.
      /// </summary>
      /// <param name="Pharmacyprofile"></param>
      /// <param name="PolicyID"></param>
      /// <param name="MWRProfileCode"></param>
      /// <param name="MWRProfileType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyRel> PolicyForWholesalerRepckager(string Pharmacyprofile, long PolicyID, string MWRProfileCode, string MWRProfileType, string ProfileGroupType)
      {
         try
         {
            var lstPolicyProfileType = new List<PolicyRel>();

            foreach (var pharmacy in Regex.Split(Pharmacyprofile, ","))
            {
               foreach (var itemWR in Regex.Split(MWRProfileCode, ","))
               {
                  var objPolicyforProfileProductLot = new PolicyRel();
                  //objPolicyforProfileProductLot.PolicyID = PolicyID;
                  //objPolicyforProfileProductLot.ProfileCode = Convert.ToInt32(itemWR);

                  //if (!string.IsNullOrEmpty(pharmacy))
                  //{
                  //   objPolicyforProfileProductLot.PolicyForProfileCode = Convert.ToInt32(pharmacy);
                  //}

                  //if (!string.IsNullOrEmpty(pharmacy))
                  //{
                  //   if (ProfileGroupType.Equals(Constants.GroupType))
                  //   {
                  //      objPolicyforProfileProductLot.PolicyForGroupProfileID = Convert.ToInt32(pharmacy);
                  //   }
                  //   else
                  //   {
                  //      objPolicyforProfileProductLot.PolicyForProfileCode = Convert.ToInt32(pharmacy);
                  //   }
                  //}

                  //objPolicyforProfileProductLot.ProfileType = MWRProfileType;
                  //objPolicyforProfileProductLot.CreatedDate = DateTime.UtcNow;

                  //lstPolicyProfileType.Add(objPolicyforProfileProductLot);
               }
            }

            return lstPolicyProfileType;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Policy Details
      /// </summary>
      /// <param name="manufactureProfile"></param>
      /// <param name="profileType"></param>
      /// <param name="policyType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static PolicyPartialGrid GetPolicyDetails(int manufactureProfile, string profileType, string policyType, System.Collections.Specialized.NameValueCollection query)
      {
         try
         {
            var _SearchParameters = new List<FilterClass>();

            if (manufactureProfile > 0)
            {
               _SearchParameters.Add(new FilterClass()
               {
                  Property = Constants.Model_CommonDataAccess_ProfileCode,
                  Value = manufactureProfile,
                  FilterType = Constants.Model_PolicyDataAccess_Equal
               });
            }

            if (!string.IsNullOrEmpty(policyType) && Convert.ToInt32(policyType) > 0)
            {
               _SearchParameters.Add(new FilterClass()
               {
                  Property = Constants.Model_PolicyDataAccess_PolicyType,
                  Value = policyType,
                  FilterType = Constants.Model_PolicyDataAccess_Equal
               });
            }

            var whereCondition = CommonDataAccessLayer.BuildQuery(query, _SearchParameters, null);

            var objPolicyPartialGrid = new PolicyPartialGrid();
            var lstResponse = new List<ResponsePolicy>();

            using (var cloudEntities = new QoskCloud())
            {













               //var dbResult = cloudEntities.USP_PolicySearch(profileType, whereCondition);

               //switch (profileType)
               //{
               //   // ...
               //   //=============================================================================
               //   case Constants.ManufacturerGroup:
               //      lstResponse = (from p in dbResult
               //                     select new ResponsePolicy
               //                     {
               //                        PolicyID = p.PolicyID,
               //                        PolicyType = p.PolicyType,
               //                        PolicyNameforGrid = p.PolicyType == 1 ? Constants.AllProducts : p.PolicyType == 2 ? Constants.ProductsSpecifictoNDCUPC : p.PolicyType == 3 ? Constants.ProductsSpecifictoNDCUPCandLotNumber : "N/A",
               //                        PolicyTypeName = p.PolicyTypeName,
               //                        SelectedMWRType = p.ProfileType,
               //                        ManufactureName = p.Name,
               //                        ProfileCode = p.ProfileCode,
               //                        PharmacyName = p.PharmacyName
               //                     }).OrderByDescending(p => p.SelectedPolicyType).ToList();
               //      objPolicyPartialGrid.lstPolicy = lstResponse;

               //      break;

               //   // ...
               //   //=============================================================================
               //   case Constants.WholesalerGroup:
               //      lstResponse = (from p in dbResult
               //                     select new ResponsePolicy
               //                     {
               //                        PolicyID = p.PolicyID,
               //                        PolicyType = p.PolicyType,
               //                        PolicyNameforGrid = p.PolicyType == 1 ? Constants.AllProducts : p.PolicyType == 2 ? Constants.ProductsSpecifictoNDCUPC : p.PolicyType == 3 ? Constants.ProductsSpecifictoNDCUPCandLotNumber : "N/A",
               //                        PolicyTypeName = p.PolicyTypeName,
               //                        SelectedPolicyType = p.PolicyTypeName,
               //                        SelectedMWRType = p.ProfileType,
               //                        ProfileCode = p.ProfileCode
               //                     }).OrderByDescending(p => p.SelectedPolicyType).ToList();
               //      objPolicyPartialGrid.lstPolicy = lstResponse;

               //      break;

               //   // ...
               //   //=============================================================================
               //    default:
               //       lstResponse = (from p in dbResult
               //                      select new ResponsePolicy
               //                      {
               //                         PolicyID = p.PolicyID,
               //                         PolicyType = p.PolicyType,
               //                         PolicyNameforGrid = p.PolicyType == 1 ? Constants.AllProducts : p.PolicyType == 2 ? Constants.ProductsSpecifictoNDCUPC : p.PolicyType == 3 ? Constants.ProductsSpecifictoNDCUPCandLotNumber : "N/A",
               //                         PolicyTypeName = p.PolicyTypeName,
               //                         SelectedMWRType = p.ProfileType,
               //                         ManufactureName = p.Name,
               //                         ProfileCode = p.ProfileCode,
               //                         PharmacyName = p.PharmacyName
               //                      }).Distinct().OrderByDescending(p => p.SelectedPolicyType).ToList();
               //      objPolicyPartialGrid.lstPolicy = lstResponse;

               //      break;
               //   }
            }

            return objPolicyPartialGrid;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   A fake implementation of the old lookup method.  If this is being
      ///   called, then the calling method needs to be refactored to use
      ///   GetPolicy() below.
      /// </summary>
      /// <param name="PolicyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ResponsePolicy GetPolicyBasedOnPolicyID(long PolicyID)
      {
         return null;
      }

      ///****************************************************************************
      /// <summary>
      /// Get Policy Based on PolicyID
      /// </summary>
      /// <param name="policyCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ResponsePolicy GetPolicy(string policyCode)
      {
         try
         {
            var lstResponsePolicy = new ResponsePolicy();

            using (var cloudEntities = new QoskCloud())
            {
               lstResponsePolicy = (from p in cloudEntities.Policy
                                    join policyRelation in cloudEntities.PolicyRel on p.PolicyCode equals policyRelation.PolicyCode
                                    where p.PolicyCode == policyCode
                                    select new ResponsePolicy
                                    {
                                       //PolicyID = p.PolicyCode,
                                       //PolicyType = p.PolicyType,
                                       //PolicyName = p.PolicyName,
                                       ProfileCode = p.ProfileCode,
                                       ProductID = policyRelation.ProductID ?? 0,
                                       ManufacturerProfileCode = policyRelation.ProfileCode ?? 0,
                                       //RepackagerProfileCode = policyRelation.RepackagerProfileCode ?? 0,
                                       //WholesalerProfileCode = policyRelation.WholesalerProfileCode ?? 0,
                                       LotNumber = policyRelation.LotNumber,
                                       //DirectAccount = p.DirectAccount ?? false,
                                       //AuthorizedForCreditConsideration = p.AuthorizedForCreditConsideration,
                                       NumberOfDaysToReturnProductReceivedInError = p.NumberOfDaysToReturnProductReceivedInError ?? 0,
                                       ExpiresOnFirstOfCurrentMonth = p.ExpiresOnFirstOfCurrentMonth ?? false,
                                       NumberOfMonthsAfterExpirationReturnable = p.NumberOfMonthsAfterExpirationReturnable ?? 0,
                                       NumberOfMonthsBeforeExpirationReturnableSealed = p.NumberOfMonthsBeforeExpirationReturnableSealed ?? 0,
                                       NumberOfMonthsBeforeExpirationReturnableOpened = p.NumberOfMonthsBeforeExpirationReturnableOpened ?? 0,
                                       BrokenSealReturnable = p.BrokenSealReturnable ?? false,
                                       //PartialsReturnable = p.PartialsReturnable,
                                       //PartialPercent1 = p.PartialPercent1,
                                       //CreditPercent1 = p.CreditPercent1,
                                       //PartialPercent2 = p.PartialPercent2,
                                       //CreditPercent2 = p.CreditPercent2,
                                       //PartialPercent3 = p.PartialPercent3,
                                       //CreditPercent3 = p.CreditPercent3,
                                       //MaximumPartialCredit = p.MaximumPartialCredit ?? 0,
                                       PrescriptionVialsReturnable = p.PrescriptionVialsReturnable,
                                       RepackagedIntoUnitDoseReturnable = p.RepackagedIntoUnitDoseReturnable,
                                       MissingLotNumberReturnable = p.MissingLotNumberReturnable,
                                       MissingExpDateReturnable = p.MissingExpDateReturnable,
                                       //CustomerReimbursementAmount = p.CustomerReimbursementAmount ?? 0,
                                       UseDebitPricing = p.UseDebitPricing,
                                       //DoNotUseFallThroughPrice = p.DoNotUseFallThroughPrice,
                                       MatchAccuracyPrecision = p.MatchAccuracyPrecision,
                                       MinimumDollarAmountReturnable = p.MinimumDollarAmountReturnable,
                                       //UseWholeSalerDirectPriceWhenNoIndirect = p.UseWholeSalerDirectPriceWhenNoIndirect,
                                       AllowQuantityOverPackageSize = p.AllowQuantityOverPackageSize,
                                       OverstockApprovalRequired = p.OverstockApprovalRequired ?? false,
                                       //OrderErrorAdministrativeFee = p.OrderErrorAdministrativeFee ?? 0,
                                       //HandlingFee = p.HandlingFee ?? 0,
                                       //HandlingFeeExceptionForStatePartialPolicyLaw = p.HandlingFeeExceptionForStatePartialPolicyLaw ?? 0,
                                       CreditIssuedAtWACLess = p.CreditIssuedAtWACLess ?? 0,
                                       //CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw = p.PolicyType ?? 0,
                                       NonDirectCreditIssuedBy = p.NonDirectCreditIssuedBy,
                                       ReturnAuthorizationExpirationDays = p.ReturnAuthorizationExpirationDays ?? 0,
                                       ReturnAuthorizationCompletionPercent = p.ReturnAuthorizationCompletionPercent ?? 0,
                                       //InnerPack = p.InnerPack,
                                       Form41Allowed = p.Form41Allowed ?? false,
                                       PriorReturnAuthorizationRequired = p.PriorReturnAuthorizationRequired,
                                       //PolicyStartDate = p.PolicyStartDate,
                                       //PolicyEndDate = p.PolicyEndDate,
                                       Version = p.Version,
                                    }).FirstOrDefault();

               // Here we are converting UTC time to local time
               lstResponsePolicy.PolicyStartDate = lstResponsePolicy.PolicyStartDate?.ToLocalTime() ?? lstResponsePolicy.PolicyStartDate;
               lstResponsePolicy.PolicyEndDate = lstResponsePolicy.PolicyEndDate?.ToLocalTime() ?? lstResponsePolicy.PolicyEndDate;

               return lstResponsePolicy;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Update Policy
      /// </summary>
      /// <param name="policy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdatePolicy(ResponsePolicy policy)
      {
         Policy policyitem = null;

         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
               {
                  // TODO: policy.PolicyID.ToString() needs to be refactored to be a string
                  policyitem = cloudEntities.Policy.Single(p => p.PolicyCode == policy.PolicyID.ToString());

                  if (policyitem.IsDeleted != true)
                  {
                     if (policyitem.Version != policy.Version)
                     {
                        objclsResponse.response = MapObject(new Policy(), policyitem);
                        objclsResponse.Status = Status.VersionChange;
                        dbContextTransaction.Rollback();
                     }
                     else
                     {
                        policyitem = MapObject(policyitem, policy);
                        policyitem.ModifiedBy = policy.ModifiedBy;
                        policyitem.ModifiedDate = DateTime.UtcNow;
                        policyitem.Version = policyitem.Version + 1;

                        if (cloudEntities.SaveChanges() > 0)
                        {
                           objclsResponse.response = policyitem;
                           objclsResponse.Status = Status.OK;
                           objclsResponse.UpdateVersion = policyitem.Version;
                           dbContextTransaction.Commit();
                        }
                        else
                        {
                           objclsResponse.response = policyitem;
                           objclsResponse.Status = Status.Rollback;
                           dbContextTransaction.Rollback();
                        }
                     }
                  }
                  else
                  {
                     objclsResponse.Status = Status.Deleted;
                  }

               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);

            objclsResponse.response = policyitem;
            objclsResponse.Status = Status.Fail;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      /// MapObject method used for mapping properties
      /// </summary>
      /// <param name="policyitem"></param>
      /// <param name="policy"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Policy MapObject(Policy policyitem, ResponsePolicy policy)
      {
         //policyitem.DirectAccount = policy.DirectAccount;
         //policyitem.AuthorizedForCreditConsideration = policy.AuthorizedForCreditConsideration;
         //policyitem.InnerPack = policy.InnerPack;
         policyitem.Form41Allowed = policy.Form41Allowed;
         policyitem.NumberOfDaysToReturnProductReceivedInError = policy.NumberOfDaysToReturnProductReceivedInError;
         policyitem.ExpiresOnFirstOfCurrentMonth = policy.ExpiresOnFirstOfCurrentMonth;
         policyitem.NumberOfMonthsAfterExpirationReturnable = policy.NumberOfMonthsAfterExpirationReturnable;
         policyitem.NumberOfMonthsBeforeExpirationReturnableSealed = policy.NumberOfMonthsBeforeExpirationReturnableSealed;
         policyitem.NumberOfMonthsBeforeExpirationReturnableOpened = policy.NumberOfMonthsBeforeExpirationReturnableOpened;
         policyitem.BrokenSealReturnable = policy.BrokenSealReturnable;
         //policyitem.PartialsReturnable = policy.PartialsReturnable;
         //policyitem.PartialPercent1 = policy.PartialPercent1;
         //policyitem.CreditPercent1 = policy.CreditPercent1;
         //policyitem.PartialPercent2 = policy.PartialPercent2;
         //policyitem.CreditPercent2 = policy.CreditPercent2;
         //policyitem.PartialPercent3 = policy.PartialPercent3;
         //policyitem.CreditPercent3 = policy.CreditPercent3;
         //policyitem.MaximumPartialCredit = policy.MaximumPartialCredit;
         policyitem.PrescriptionVialsReturnable = policy.PrescriptionVialsReturnable;
         policyitem.RepackagedIntoUnitDoseReturnable = policy.RepackagedIntoUnitDoseReturnable;
         policyitem.MissingLotNumberReturnable = policy.MissingLotNumberReturnable;
         policyitem.MissingExpDateReturnable = policy.MissingExpDateReturnable;
         //policyitem.CustomerReimbursementAmount = policy.CustomerReimbursementAmount;
         policyitem.UseDebitPricing = policy.UseDebitPricing;
         //policyitem.DoNotUseFallThroughPrice = policy.DoNotUseFallThroughPrice;
         policyitem.MatchAccuracyPrecision = 3;
         policyitem.MinimumDollarAmountReturnable = policy.MinimumDollarAmountReturnable ?? 0;
         //policyitem.UseWholeSalerDirectPriceWhenNoIndirect = policy.UseWholeSalerDirectPriceWhenNoIndirect;
         policyitem.AllowQuantityOverPackageSize = policy.AllowQuantityOverPackageSize;
         policyitem.OverstockApprovalRequired = policy.OverstockApprovalRequired;
         //policyitem.OrderErrorAdministrativeFee = policy.OrderErrorAdministrativeFee;
         //policyitem.HandlingFee = policy.HandlingFee;
         //policyitem.HandlingFeeExceptionForStatePartialPolicyLaw = policy.HandlingFeeExceptionForStatePartialPolicyLaw;
         policyitem.CreditIssuedAtWACLess = policy.CreditIssuedAtWACLess;
         policyitem.CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw = policy.CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw;
         policyitem.NonDirectCreditIssuedBy = policy.NonDirectCreditIssuedBy;
         policyitem.ReturnAuthorizationExpirationDays = policy.ReturnAuthorizationExpirationDays;
         policyitem.ReturnAuthorizationCompletionPercent = policy.ReturnAuthorizationCompletionPercent;
         policyitem.PriorReturnAuthorizationRequired = policy.PriorReturnAuthorizationRequired;
         //policyitem.PolicyStartDate = policy.PolicyStartDate;
         //policyitem.PolicyEndDate = policy.PolicyEndDate;
         policyitem.ModifiedBy = policy.ModifiedBy;

         return policyitem;
      }

      ///****************************************************************************
      /// <summary>
      /// MapObject overloaded method with different parameter used for mapping
      /// </summary>
      /// <param name="policyitem"></param>
      /// <param name="policy"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Policy MapObject(Policy policyitem, Policy policy)
      {
         //policyitem.DirectAccount = policy.DirectAccount;
         //policyitem.AuthorizedForCreditConsideration = policy.AuthorizedForCreditConsideration;
         //policyitem.InnerPack = policy.InnerPack;
         policyitem.Form41Allowed = policy.Form41Allowed;
         policyitem.NumberOfDaysToReturnProductReceivedInError = policy.NumberOfDaysToReturnProductReceivedInError;
         policyitem.ExpiresOnFirstOfCurrentMonth = policy.ExpiresOnFirstOfCurrentMonth;
         policyitem.NumberOfMonthsAfterExpirationReturnable = policy.NumberOfMonthsAfterExpirationReturnable;
         policyitem.NumberOfMonthsBeforeExpirationReturnableSealed = policy.NumberOfMonthsBeforeExpirationReturnableSealed;
         policyitem.NumberOfMonthsBeforeExpirationReturnableOpened = policy.NumberOfMonthsBeforeExpirationReturnableOpened;
         policyitem.BrokenSealReturnable = policy.BrokenSealReturnable;
         policyitem.PartialsReturnable = policy.PartialsReturnable;
         //policyitem.PartialPercent1 = policy.PartialPercent1;
         //policyitem.CreditPercent1 = policy.CreditPercent1;
         //policyitem.PartialPercent2 = policy.PartialPercent2;
         //policyitem.CreditPercent2 = policy.CreditPercent2;
         //policyitem.PartialPercent3 = policy.PartialPercent3;
         //policyitem.CreditPercent3 = policy.CreditPercent3;
         //policyitem.MaximumPartialCredit = policy.MaximumPartialCredit;
         policyitem.PrescriptionVialsReturnable = policy.PrescriptionVialsReturnable;
         policyitem.RepackagedIntoUnitDoseReturnable = policy.RepackagedIntoUnitDoseReturnable;
         policyitem.MissingLotNumberReturnable = policy.MissingLotNumberReturnable;
         policyitem.MissingExpDateReturnable = policy.MissingExpDateReturnable;
         //policyitem.CustomerReimbursementAmount = policy.CustomerReimbursementAmount;
         policyitem.UseDebitPricing = policy.UseDebitPricing;
         //policyitem.DoNotUseFallThroughPrice = policy.DoNotUseFallThroughPrice;
         policyitem.MatchAccuracyPrecision = 3;
         policyitem.MinimumDollarAmountReturnable = policy.MinimumDollarAmountReturnable;
         //policyitem.UseWholeSalerDirectPriceWhenNoIndirect = policy.UseWholeSalerDirectPriceWhenNoIndirect;
         policyitem.AllowQuantityOverPackageSize = policy.AllowQuantityOverPackageSize;
         policyitem.OverstockApprovalRequired = policy.OverstockApprovalRequired;
         //policyitem.OrderErrorAdministrativeFee = policy.OrderErrorAdministrativeFee;
         //policyitem.HandlingFee = policy.HandlingFee;
         //policyitem.HandlingFeeExceptionForStatePartialPolicyLaw = policy.HandlingFeeExceptionForStatePartialPolicyLaw;
         policyitem.CreditIssuedAtWACLess = policy.CreditIssuedAtWACLess;
         policyitem.CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw = policy.CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw;
         policyitem.NonDirectCreditIssuedBy = policy.NonDirectCreditIssuedBy;
         policyitem.ReturnAuthorizationExpirationDays = policy.ReturnAuthorizationExpirationDays;
         policyitem.ReturnAuthorizationCompletionPercent = policy.ReturnAuthorizationCompletionPercent;
         policyitem.PriorReturnAuthorizationRequired = policy.PriorReturnAuthorizationRequired;
         //policyitem.PolicyStartDate = policy.PolicyStartDate;
         //policyitem.PolicyEndDate = policy.PolicyEndDate;
         policyitem.ModifiedBy = policy.ModifiedBy;
         policyitem.ModifiedDate = DateTime.UtcNow;

         return policyitem;
      }

      ///****************************************************************************
      /// <summary>
      /// Delete Policy
      /// </summary>
      /// <param name="policyCode"></param>
      /// <param name="actionBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response DeletePolicy(string policyCode, string actionBy)
      {
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var policyItem = (from prdt in cloudEntities.Policy
                                 where prdt.PolicyCode == policyCode
                                 select prdt).SingleOrDefault();

               if (policyItem != null && policyItem.IsDeleted != true)
               {
                  policyItem.IsDeleted = true;
                  objclsResponse.Status = cloudEntities.SaveChanges() > 0 ? Status.OK : Status.Fail;
               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      /// Get Group Name
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<MLGroup> GetProfileGroup()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //ICollection<MLGroup> groupList = (from grp in cloudEntities.Group
               //                                  where grp.GroupType.Equals(Constants.PHARGRP)
               //                                  select new MLGroup
               //                                  {
               //                                     ProfileGroupID = grp.ProfileGroupID,
               //                                     GroupName = grp.GroupName,
               //                                     ProfileCode = grp.ProfileCode,
               //                                  }).OrderBy(pp => pp.GroupName).ToList();
               //return groupList;
            }

            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get All GroupMember Based On Group
      /// </summary>
      /// <param name="ProfileType"></param>
      /// <param name="ProfileCodeWithGroupID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<AssignedProfileGroup> GetAllGroupMemberBasedOnGroup(string ProfileType, string ProfileCodeWithGroupID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<AssignedProfileGroup> lstGroupList = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (!string.IsNullOrEmpty(ProfileCodeWithGroupID))
               {
                  buildquery += string.Format(Constants.Model_PolicyDataAccess_ProfileGroupID, ProfileCodeWithGroupID);
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var objPharmacy = cloudEntities.USP_GetPharmacyDetailsBasedOnProfileTypeAndGroupType(Constants.GroupType, buildquery);
               //lstGroupList = (from pg in objPharmacy
               //                select new AssignedProfileGroup
               //                {
               //                   ProfileGroupID = pg.ProfileGroupID,
               //                   City = pg.City,
               //                   State = pg.State,
               //                   GroupProfileName = pg.GroupProfileName,
               //                   GroupProfileCode = pg.ProfileCode,
               //                   Region = (string.IsNullOrEmpty(pg.RegionCode)) ? Constants.Model_GroupDataAccess_NA : pg.RegionCode,
               //                }).ToList();

               if (!string.IsNullOrEmpty(sortorder))
               {
                  lstGroupList = sortorder == Constants.Model_GroupDataAccess_Asc ? lstGroupList.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null)) : lstGroupList.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               }

               totalrecord = lstGroupList.Count();
               lstGroupList = lstGroupList.Skip(pagesize * pagenum).Take(pagesize);

               return lstGroupList.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get pharmacy Details
      /// </summary>
      /// <param name="PolicyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLPharamcyPolicy> GetPolicypharmacyDetails(MLPolicyProfileSearch objPolicysearch)
      {
         var lstresult = new List<MLPharamcyPolicy>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (!objPolicysearch.IsSerach)
               {
                  lstresult = (from polictPrfile in cloudEntities.PolicyRel
                               join prl in cloudEntities.Profile on polictPrfile.ProfileCode ?? 0 equals prl.ProfileCode into temp
                               from tempprofile in temp.DefaultIfEmpty()
                               join prd in cloudEntities.Product on polictPrfile.ProductID equals prd.ProductID into tempp
                               from temprd in tempp.DefaultIfEmpty()
                               join Lot in cloudEntities.Lot on polictPrfile.LotNumber equals Lot.LotNumber into templl
                               from templot in templl.DefaultIfEmpty()
                               where polictPrfile.PolicyRelID == objPolicysearch.PolicyID
                               select new MLPharamcyPolicy
                               {
                                  //PolicyType = polictPrfile.ProfileType,
                                  ProductID = polictPrfile.ProductID ?? 0,
                                  NDCUPCWithDashes = temprd.NDCUPCWithDashes,
                                  Description = temprd.Description,
                                  Strength = temprd.Strength,
                                  LotNumber = polictPrfile.LotNumber,
                                  ExpirationDate = templot.ExpirationDate,
                                  Address = tempprofile.Address1 + " " + tempprofile.Address2 + " " + tempprofile.Address3,
                                  City = tempprofile.City,
                                  //GroupID = polictPrfile.PolicyForGroupProfileID ?? 0,
                                  ProfileName = tempprofile.Name,
                                  //PharamacyProfileCode = (polictPrfile.ProfileCode != null) ? polictPrfile.ProfileCode ?? 0 : ((polictPrfile.RepackagerProfileCode != null) ? polictPrfile.RepackagerProfileCode ?? 0 : polictPrfile.WholesalerProfileCode ?? 0),
                                  //PolicyForProfileName = (from m in cloudModelEntities.Profile where m.ProfileCode == polictPrfile.PolicyForProfileCode select m.Name).FirstOrDefault(),
                                  //PolicyForProfileCode = polictPrfile.PolicyForProfileCode ?? 0,
                                  //PolicyID = polictPrfile.PolicyID,
                                  //PolicyProfileID = polictPrfile.PolicyProfileID,
                                  RegionCode = tempprofile.RegionCode
                               }).Distinct().ToList();
               }
               else
               {
                  //var lstSearchPolicyProfiles = (from polictPrfile in cloudModelEntities.PolicyRel
                  //                               join prl in cloudModelEntities.Profile on polictPrfile.PolicyForProfileCode equals prl.ProfileCode into temp from tempprofile in temp.DefaultIfEmpty()
                  //                               join prd in cloudModelEntities.Product on polictPrfile.ProductID equals prd.ProductID into tempp from temprd in tempp.DefaultIfEmpty()
                  //                               join Lot in cloudModelEntities.Lot on polictPrfile.LotNumber equals Lot.LotNumber into templl from templot in templl.DefaultIfEmpty()
                  //                               where polictPrfile.PolicyID == objPolicysearch.PolicyID
                  //                               select new MLPharamcyPolicy
                  //                               {
                  //                                  PolicyType = polictPrfile.ProfileType,
                  //                                  ProductID = polictPrfile.ProductID ?? 0,
                  //                                  Description = temprd.Description,
                  //                                  Strength = temprd.Strength,
                  //                                  LotNumber = polictPrfile.LotNumber,
                  //                                  ExpirationDate = templot.ExpirationDate,
                  //                                  Address = tempprofile.Address1 + " " + tempprofile.Address2 + " " + tempprofile.Address3,
                  //                                  City = tempprofile.City,
                  //                                  GroupID = polictPrfile.PolicyForGroupProfileID ?? 0,
                  //                                  ProfileName = tempprofile.Name,
                  //                                  PharamacyProfileCode = (polictPrfile.ProfileCode != null) ? polictPrfile.ProfileCode ?? 0 : ((polictPrfile.RepackagerProfileCode != null) ? polictPrfile.RepackagerProfileCode ?? 0 : polictPrfile.WholesalerProfileCode ?? 0),
                  //                                  PolicyForProfileName = (from m in cloudModelEntities.Profiles where m.ProfileCode == polictPrfile.PolicyForProfileCode select m.Name).FirstOrDefault(),
                  //                                  PolicyForProfileCode = polictPrfile.PolicyForProfileCode ?? 0,
                  //                                  PolicyID = polictPrfile.PolicyID,
                  //                                  PolicyProfileID = polictPrfile.PolicyProfileID,
                  //                                  RegionCode = tempprofile.RegionCode
                  //                               }).AsQueryable();

                  //if (!string.IsNullOrEmpty(objPolicysearch.LotNumber))
                  //{
                  //   lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.LotNumber.StartsWith(objPolicysearch.LotNumber));
                  //}

                  //if (objPolicysearch.NDCUPC > 0)
                  //{
                  //   lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.NDC == objPolicysearch.NDCUPC);

                  //   if (lstSearchPolicyProfiles == null)
                  //   {
                  //      lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.UPC == objPolicysearch.NDCUPC);
                  //   }
                  //}

                  //if (objPolicysearch.PolicyAppliedForProfileCode > 0)
                  //{
                  //   lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.PolicyForProfileCode == objPolicysearch.PolicyAppliedForProfileCode);
                  //}

                  //if (objPolicysearch.GroupID > 0)
                  //{
                  //   lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.PolicyForProfileCode == objPolicysearch.PolicyAppliedForProfileCode);
                  //}

                  //if (objPolicysearch.PolicyProfileCode > 0)
                  //{
                  //   lstSearchPolicyProfiles = lstSearchPolicyProfiles.Where(p => p.PharamacyProfileCode == objPolicysearch.PolicyProfileCode);
                  //}

                  //foreach (var item in lstSearchPolicyProfiles.ToList())
                  //{
                  //   lstresult.Add(item);
                  //}
               }
            }

            return lstresult;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Delete Policy Profile
      /// </summary>
      /// <param name="policyProfileID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static long DeletePolicyProfile(long policyProfileID)
      {
         var selectedPolicyID = 0L;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var objPolicyProfileType = (from polictPrfile in cloudEntities.PolicyRel
                                           where polictPrfile.PolicyRelID == policyProfileID
                                           select polictPrfile).SingleOrDefault();

               if (objPolicyProfileType != null)
               {
                  //selectedPolicyID = objPolicyProfileType.PolicyID;
                  //cloudEntities.PolicyProfileType.Remove(objPolicyProfileType);
                  cloudEntities.SaveChanges();
               }
            }

            return selectedPolicyID;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetPolicyProfileCode(string profileType, long policyID)
      {
         ICollection<ProfilesName> listPolicyProfile = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //listPolicyProfile = (from policyProfile in cloudEntities.PolicyRel
               //                     join p in cloudEntities.Profile on policyProfile.ProfileCode equals p.ProfileCode
               //                     where policyProfile.PolicyID == policyID && policyProfile.ProfileType.Equals(profileType)
               //                     select new ProfilesName
               //                     {
               //                        ProfileCode = (policyProfile.ProfileCode != null) ? policyProfile.ProfileCode : 0,
               //                        Name = p.Name,
               //                     }).Distinct().OrderBy(pp => pp.Name).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return listPolicyProfile;
      }

      ///****************************************************************************
      /// <summary>
      /// Get ProfileCode for Applied for
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetAllPolicyForProfiles(long policyID)
      {
         ICollection<ProfilesName> listPolicyProfile = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //listPolicyProfile = (from policyProfile in cloudEntities.PolicyRel
               //                     join p in cloudEntities.Profile on policyProfile.PolicyForProfileCode equals p.ProfileCode
               //                     where policyProfile.PolicyID == policyID
               //                     select new ProfilesName
               //                     {
               //                        ProfileCode = policyProfile.PolicyForProfileCode ?? 0,
               //                        Name = p.Name,
               //                     }).Distinct().OrderBy(pp => pp.Name).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return listPolicyProfile;
      }

      ///****************************************************************************
      /// <summary>
      /// Get NDCLOT Product With Associated Lot Number
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="CurrentServerPageNo"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductNDCWithLot> GetNDCLOTProductWithAssociatedLot(string ProfileCode, string mwrtype, long PolicyID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<ProductNDCWithLot> objPRDNDCWith = null;
         var Prfcodewithcomma = string.Empty;

         if (!string.IsNullOrEmpty(ProfileCode))
         {
            Prfcodewithcomma = ProfileCode;
         }

         if (!string.IsNullOrEmpty(mwrtype) && mwrtype.Equals(Constants.ManufacturerGroup))
         {
            Prfcodewithcomma = GetGroupProfile(Convert.ToInt32(ProfileCode));
         }

         using (var cloudEntities = new QoskCloud())
         {
            if (string.IsNullOrEmpty(buildquery))
            {
               buildquery = Constants.Model_GroupDataAccess_Where;
            }
            else
            {
               buildquery += Constants.PolicyController_ANDWITHSPACE;
            }

            if (Prfcodewithcomma != null)
            {
               buildquery += string.Format(Constants.Model_PolicyDataAccess_ProductsProfileCode, Prfcodewithcomma);
            }
            else
            {
               buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
            }

            buildquery += Constants.PolicyController_ANDWITHSPACE;

            if (PolicyID > 0)
            {
               buildquery += Constants.Model_PolicyDataAccess_PolicyIDWithEqual + PolicyID + "";
            }
            else
            {
               buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
            }

            //var detailsresult = cloudEntities.USP_GetNDCLOTWithProfilesName("", buildquery);

            //objPRDNDCWith = (from i in detailsresult
            //                 select new ProductNDCWithLot
            //                 {
            //                    Description = i.Description,
            //                    MFGLabelerCode = i.MFGLabelerCode,
            //                    MFGProductNumber = i.MFGProductNumber,
            //                    NDC = i.NDC,
            //                    NDCUPCWithDashes = i.NDCUPCWithDashes,
            //                    ProductID = i.ProductID,
            //                    ProfileCode = i.ProfileCode,
            //                    Name = i.Name,
            //                 }).ToList();

            if (!string.IsNullOrEmpty(sortorder))
            {
               objPRDNDCWith = sortorder == Constants.Model_GroupDataAccess_Asc ? objPRDNDCWith.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null)) : objPRDNDCWith.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
            }

            totalrecord = objPRDNDCWith.Count();
            objPRDNDCWith = objPRDNDCWith.Skip(pagesize * pagenum).Take(pagesize);

            return objPRDNDCWith.ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="PolicyID"></param>
      /// <param name="ProductID"></param>
      /// <param name="mwrtype"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<LotNumbersDataEntity> GetNDCLOTProductWithAssociatedLotDetails(int ProfileCode, long PolicyID, long ProductID, string mwrtype, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<LotNumbersDataEntity> obLotNumberEntity = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (ProductID > 0 && PolicyID == 0)
               {
                  buildquery = buildquery + Constants.Model_PolicyDataAccess_ProductID + ProductID + Constants.Model_PolicyDataAccess_DiverseMsg;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (PolicyID > 0)
               {
                  buildquery += Constants.Model_PolicyDataAccess_PolicyIDWithEqual + PolicyID + "";
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var detailsresult = cloudEntities.USP_GetLOTDetailsForSelectedNDCAndProfile(Constants.Model_PolicyDataAccess_NDC, buildquery);

               //obLotNumberEntity = (from lot in detailsresult
               //                     select new LotNumbersDataEntity
               //                     {
               //                        LotNumber = lot.LotNumber,
               //                        ProfileCode = Convert.ToInt32(ProfileCode),
               //                        LotID = lot.LotNumbersID,
               //                        ProductID = lot.ProductID ?? 0,
               //                        ExpirationDate = lot.ExpirationDate,
               //                     }).ToList();

               if (!string.IsNullOrEmpty(sortorder))
               {
                  obLotNumberEntity = sortorder == Constants.Model_GroupDataAccess_Asc ? obLotNumberEntity.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null)) : obLotNumberEntity.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               }

               totalrecord = obLotNumberEntity.Count();
               obLotNumberEntity = obLotNumberEntity.Skip(pagesize * pagenum).Take(pagesize);

               return obLotNumberEntity.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="mwrtype"></param>
      /// <param name="PolicyID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProfileProductData> GetNDCProductWithAssociatedLotForExistingPolicy(string ProfileCode, string mwrtype, long PolicyID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         var ProfileCodeArray = string.Empty;
         IEnumerable<ProfileProductData> profileData = null;

         if (!string.IsNullOrEmpty(ProfileCode))
         {
            ProfileCodeArray = ProfileCode;
         }

         if (!string.IsNullOrEmpty(mwrtype) && mwrtype.Equals(Constants.ManufacturerGroup))
         {
            ProfileCodeArray = GetGroupProfile(Convert.ToInt32(ProfileCode));
         }

         using (var cloudEntities = new QoskCloud())
         {

            if (string.IsNullOrEmpty(buildquery))
            {
               buildquery = Constants.Model_GroupDataAccess_Where;
            }
            else
            {
               buildquery += Constants.PolicyController_ANDWITHSPACE;
            }

            if (ProfileCodeArray != null)
            {
               buildquery += string.Format(Constants.Model_PolicyDataAccess_ProfileCodeInZero, ProfileCodeArray);
            }
            else
            {
               buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
            }

            //var detailsresult = cloudEntities.USP_GetNDCDetailsWithProfiles(buildquery);

            //profileData = (from i in detailsresult
            //               select new ProfileProductData
            //               {
            //                  Name = i.Name,
            //                  Type = i.Type,
            //                  ProfileCode = i.ProfileCode,
            //                  Address1 = i.Address1,
            //                  City = i.City,
            //                  State = i.State,
            //                  RegionCode = i.RegionCode,
            //               }).ToList();

            if (!string.IsNullOrEmpty(sortorder))
            {
               profileData = sortorder == Constants.Model_GroupDataAccess_Asc
                                 ? profileData.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                                 : profileData.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
            }

            totalrecord = profileData.Count();
            profileData = profileData.Skip(pagesize * pagenum).Take(pagesize);

         }

         return profileData.ToList();
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="PolicyID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductsML> GetNDCProductWithAssociatedLotForExistingPolicyDetails(int ProfileCode, long PolicyID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<ProductsML> prductList = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (ProfileCode > 0 && PolicyID > 0)
               {
                  buildquery = buildquery + Constants.Model_PolicyDataAccess_PolicyProfileTypeProfileCode + ProfileCode + "";
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }
               else
               {
                  if (ProfileCode > 0)
                  {
                     buildquery += Constants.Model_PolicyDataAccess_ProfileCodeWithEqual + ProfileCode + "";
                  }
                  else
                  {
                     buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
                  }

                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (PolicyID > 0)
               {
                  buildquery += Constants.Model_PolicyDataAccess_PolicyIDWithEqual + PolicyID + "";
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               if (PolicyID > 0)
               {
                  //var detailsresult = cloudEntities.USP_GetNDCDetailsForSelectedProfile(Constants.Model_PolicyDataAccess_NDC, buildquery);

                  //prductList = (from p in detailsresult
                  //              select new ProductsML
                  //              {
                  //                 ProfileCode = p.ProfileCode ?? 0,
                  //                 ProductID = p.ProductID ?? 0,
                  //                 Description = p.Description,
                  //                 NDC = p.NDC,
                  //                 NDCUPCWithDashes = p.NDCUPCWithDashes,
                  //                 MFGLabelerCode = p.MFGLabelerCode,
                  //                 Strength = p.Strength
                  //              }).ToList();
               }
               else
               {
                  //var detailsresult = cloudEntities.USP_GetNDCDetailsForSelectedProfile(Constants.Model_PolicyDataAccess_wrw, buildquery);

                  //prductList = (from p in detailsresult
                  //              select new ProductsML
                  //              {
                  //                 ProfileCode = p.ProfileCode ?? 0,
                  //                 ProductID = p.ProductID ?? 0L,
                  //                 Description = p.Description,
                  //                 NDC = p.NDC,
                  //                 NDCUPCWithDashes = p.NDCUPCWithDashes,
                  //                 MFGLabelerCode = p.MFGLabelerCode,
                  //                 Strength = p.Strength
                  //              }).ToList();
               }

               if (!string.IsNullOrEmpty(sortorder))
               {
                  prductList = sortorder == Constants.Model_GroupDataAccess_Asc
                     ? prductList.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                     : prductList.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               }

               totalrecord = prductList.Count();
               prductList = prductList.Skip(pagesize * pagenum).Take(pagesize);

               return prductList.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="mwrtype"></param>
      /// <param name="PolicyID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<ProductNDCWithLot> GetNDCLOTProductWithAssociatedLotForExistingPolicy(string ProfileCode, string mwrtype, long PolicyID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<ProductNDCWithLot> ProductlotData = null;
         var ProfileCodeArray = string.Empty;

         if (!string.IsNullOrEmpty(ProfileCode))
         {
            ProfileCodeArray = ProfileCode;
         }

         if (!string.IsNullOrEmpty(mwrtype) && mwrtype.Equals(Constants.ManufacturerGroup))
         {
            ProfileCodeArray = GetGroupProfile(Convert.ToInt32(ProfileCode));
         }

         using (var cloudEntities = new QoskCloud())
         {
            if (string.IsNullOrEmpty(buildquery))
            {
               buildquery = Constants.Model_GroupDataAccess_Where;
            }
            else
            {
               buildquery += Constants.PolicyController_ANDWITHSPACE;
            }

            if (ProfileCodeArray != null)
            {
               var pgrpIDS = string.Format(Constants.Model_PolicyDataAccess_PolicyProfileTypeProfileCodeIn, ProfileCodeArray);
               pgrpIDS += Constants.Model_PolicyDataAccess_PolicyProfileTypeWithMsg;
               buildquery += pgrpIDS;
            }
            else
            {
               buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
            }

            buildquery += Constants.PolicyController_ANDWITHSPACE;

            if (PolicyID > 0)
            {
               buildquery += Constants.Model_PolicyDataAccess_PolicyIDWithEqual + PolicyID + "";
            }
            else
            {
               buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
            }

            //var detailsresult = cloudEntities.USP_GetNDCLOTWithProfilesName(Constants.Model_PolicyDataAccess_NDCView, buildquery);

            //ProductlotData = (from i in detailsresult
            //                  select new ProductNDCWithLot
            //                  {
            //                     Description = i.Description,
            //                     MFGLabelerCode = i.MFGLabelerCode,
            //                     MFGProductNumber = i.MFGProductNumber,
            //                     NDC = i.NDC,
            //                     NDCUPCWithDashes = i.NDCUPCWithDashes,
            //                     ProductID = i.ProductID,
            //                     ProfileCode = i.ProfileCode,
            //                     Name = i.Name
            //                  }).ToList();

            if (!string.IsNullOrEmpty(sortorder))
            {
               ProductlotData = sortorder == Constants.Model_GroupDataAccess_Asc
                                    ? ProductlotData.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                                    : ProductlotData.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
            }

            totalrecord = ProductlotData.Count();
            ProductlotData = ProductlotData.Skip(pagesize * pagenum).Take(pagesize);

            return ProductlotData.ToList();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProductID"></param>
      /// <param name="lotNumber"></param>
      /// <param name="ProfileCode"></param>
      /// <param name="PolicyID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<LotNumbersDataEntity> GetNDCLOTProductWithAssociatedLotForExistingPolicyDetails(long ProductID, string lotNumber, int ProfileCode, long PolicyID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<LotNumbersDataEntity> prductlotList = new List<LotNumbersDataEntity>();
         var ProductlotData = new List<ProductNDCWithLot>();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (ProductID > 0)
               {
                  buildquery += Constants.Model_PolicyDataAccess_PolicyProfileTypeProductID + ProductID + "";
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (PolicyID > 0)
               {
                  buildquery += Constants.Model_PolicyDataAccess_PolicyProfileTypePolicyID + PolicyID + "";
               }
               else
               {
                  buildquery += Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var detailsresult = cloudEntities.USP_GetLOTDetailsForSelectedNDCAndProfile("", buildquery);

               //prductlotList = (from lot in detailsresult
               //                 select new LotNumbersDataEntity
               //                 {
               //                    LotNumber = lot.LotNumber,
               //                    ProfileCode = ProfileCode,
               //                    LotID = lot.LotNumbersID,
               //                    ProductID = lot.ProductID ?? 0,
               //                    ExpirationDate = lot.ExpirationDate,
               //                 }).Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(sortorder))
            {
               prductlotList = sortorder == Constants.Model_GroupDataAccess_Asc
                  ? prductlotList.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null))
                  : prductlotList.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
            }

            totalrecord = ProductlotData.Count();
            prductlotList = prductlotList.Skip(pagesize * pagenum).Take(pagesize);

            return prductlotList;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Insert customer specific and published policy to database.
      /// </summary>
      /// <param name="objResponsePolicy"></param>
      /// <param name="MWRProfileCode"></param>
      /// <param name="Pharmacyprofile"></param>
      /// <param name="LotNumberWithProductID"></param>
      /// <param name="ProductBasedPolicy"></param>
      /// <param name="MWRProfileType"></param>
      /// <param name="PolicyType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SavePolicyCustomerSpecificAndPublishedPolicy(ResponsePolicy objResponsePolicy, string MWRProfileCode, string Pharmacyprofile, string LotNumberWithProductID, string ProductBasedPolicy, string MWRProfileType, string PolicyType)
      {
         var LasteInsertedpolicyID = 0L;
         return true;

         using (var cloudEntities = new QoskCloud())
         {
            using (var dbContextTransaction = cloudEntities.Database.BeginTransaction())
            {
               try
               {
                  if (objResponsePolicy.PolicyID < 1)
                  {
                     var policyItem = new Policy();
                     //PolicyAudit policyauditItem = new PolicyAudit();
                     //policyItem.PolicyType = objResponsePolicy.PolicyType;
                     policyItem.ProfileCode = string.IsNullOrEmpty(MWRProfileCode) ? 0 : Convert.ToInt32(MWRProfileCode);
                     //policyItem.PolicyTypeName = objResponsePolicy.SelectedPolicyType.Equals(1) ? Constants.PublishedPolicy : Constants.CustomerSpecific;
                     ;
                     policyItem.ExpiresOnFirstOfCurrentMonth = objResponsePolicy.ExpiresOnFirstOfCurrentMonth;
                     policyItem.NumberOfMonthsAfterExpirationReturnable = objResponsePolicy.NumberOfMonthsAfterExpirationReturnable;
                     policyItem.NumberOfMonthsBeforeExpirationReturnableSealed = objResponsePolicy.NumberOfMonthsBeforeExpirationReturnableSealed;
                     policyItem.NumberOfMonthsBeforeExpirationReturnableOpened = objResponsePolicy.NumberOfMonthsBeforeExpirationReturnableOpened;
                     policyItem.OverstockApprovalRequired = objResponsePolicy.OverstockApprovalRequired;
                     policyItem.BrokenSealReturnable = objResponsePolicy.BrokenSealReturnable;
                     policyItem.PrescriptionVialsReturnable = objResponsePolicy.PrescriptionVialsReturnable;
                     policyItem.RepackagedIntoUnitDoseReturnable = objResponsePolicy.RepackagedIntoUnitDoseReturnable;
                     policyItem.MissingLotNumberReturnable = objResponsePolicy.MissingLotNumberReturnable;
                     policyItem.MissingExpDateReturnable = objResponsePolicy.MissingExpDateReturnable;
                     //policyItem.PartialsReturnable = objResponsePolicy.PartialsReturnable;

                     //pricing Rule

                     //policyItem.HandlingFee = objResponsePolicy.HandlingFee;
                     policyItem.UseDebitPricing = objResponsePolicy.UseDebitPricing;
                     //policyItem.HandlingFeeExceptionForStatePartialPolicyLaw = objResponsePolicy.HandlingFeeExceptionForStatePartialPolicyLaw;
                     policyItem.AllowQuantityOverPackageSize = objResponsePolicy.AllowQuantityOverPackageSize;
                     //policyItem.DoNotUseFallThroughPrice = objResponsePolicy.DoNotUseFallThroughPrice;
                     //policyItem.UseWholeSalerDirectPriceWhenNoIndirect = objResponsePolicy.UseWholeSalerDirectPriceWhenNoIndirect;

                     //Event rule

                     policyItem.PriorReturnAuthorizationRequired = objResponsePolicy.PriorReturnAuthorizationRequired;
                     policyItem.ReturnAuthorizationCompletionPercent = objResponsePolicy.ReturnAuthorizationCompletionPercent;

                     //HardCoded because no field

                     policyItem.ReturnAuthorizationApprovalThreshold = 1;
                     policyItem.MinimumDollarAmountReturnable = 1;
                     policyItem.MatchAccuracyPrecision = 3;
                     //policyItem.AuthorizedForCreditConsideration = objResponsePolicy.AuthorizedForCreditConsideration;

                     //policyItem.PolicyStartDate = Convert.ToDateTime(objResponsePolicy.PolicyStartDate).ToUniversalTime();
                     //policyItem.PolicyEndDate = Convert.ToDateTime(objResponsePolicy.PolicyEndDate).ToUniversalTime();
                     policyItem.Version = 0;
                     policyItem.CreatedDate = DateTime.UtcNow;
                     policyItem.CreatedBy = objResponsePolicy.ModifiedBy;

                     cloudEntities.Policy.Add(policyItem);

                     int savedResult = cloudEntities.SaveChanges();
                     if (savedResult > 0)
                     {
                        //LasteInsertedpolicyID = policyItem.PolicyCode;
                        cloudEntities.SaveChanges();
                     }
                  }
                  else
                  {
                     LasteInsertedpolicyID = objResponsePolicy.PolicyID;
                  }

                  // Apply policy based on selection
                  List<PolicyRel> lstPolicyProfileType = null;

                  switch (Convert.ToInt32(MWRProfileType))
                  {
                     // ...
                     //=============================================================================
                     case 1:
                        lstPolicyProfileType = GetListofRecordsforInsertIntoDatabase(LotNumberWithProductID, Pharmacyprofile, LasteInsertedpolicyID, MWRProfileCode, Constants.Manufacturer, objResponsePolicy.ProfileGroupSelectionType, PolicyType, ProductBasedPolicy);
                        break;

                     // ...
                     //=============================================================================
                     case 2:
                        lstPolicyProfileType = GetListofRecordsforInsertIntoDatabase(LotNumberWithProductID, Pharmacyprofile, LasteInsertedpolicyID, MWRProfileCode, Constants.ManufacturerGroup, objResponsePolicy.ProfileGroupSelectionType, PolicyType, ProductBasedPolicy);
                        break;

                     // ...
                     //=============================================================================
                     case 3:
                        lstPolicyProfileType = GetListofRecordsforInsertIntoDatabase(LotNumberWithProductID, Pharmacyprofile, LasteInsertedpolicyID, MWRProfileCode, Constants.Wholesaler, objResponsePolicy.ProfileGroupSelectionType, PolicyType, ProductBasedPolicy);
                        break;

                     // ...
                     //=============================================================================
                     case 4:
                        lstPolicyProfileType = PolicyForWholesalerRepckager(Pharmacyprofile, LasteInsertedpolicyID, MWRProfileCode, Constants.WholesalerGroup, objResponsePolicy.ProfileGroupSelectionType);
                        break;

                     // ...
                     //=============================================================================
                     case 5:
                        lstPolicyProfileType = PolicyForWholesalerRepckager(Pharmacyprofile, LasteInsertedpolicyID, MWRProfileCode, Constants.Repackager, objResponsePolicy.ProfileGroupSelectionType);
                        break;
                  }

                  cloudEntities.PolicyRel.AddRange(lstPolicyProfileType);
                  var result = cloudEntities.SaveChanges();

                  if (result > 0)
                  {
                     dbContextTransaction.Commit();
                     return true;
                  }

                  dbContextTransaction.Rollback();
                  return false;
               }

               catch (Exception Ex)
               {
                  dbContextTransaction.Rollback();
                  Logger.Error(Ex.Message);
               }
            }
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the number of records to insert into database for all products
      ///   policy for customer specific and published policy
      /// </summary>
      /// <param name="MWRProfileCode"></param>
      /// <param name="PolicyID"></param>
      /// <param name="MWRProfileType"></param>
      /// <param name="ProfileGroupSelection"></param>
      /// <param name="pharmacy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyRel> PolicyForAllProducts(string MWRProfileCode, long PolicyID, string MWRProfileType, string ProfileGroupSelection, [Optional] string pharmacy)
      {
         var lstPolicyProfileType = new List<PolicyRel>();
         var objPolicyforProfileProductLot = new PolicyRel();

         //objPolicyforProfileProductLot.PolicyID = PolicyID;
         objPolicyforProfileProductLot.ProfileCode = Convert.ToInt32(MWRProfileCode);
         //objPolicyforProfileProductLot.ProfileType = MWRProfileType;

         if (!string.IsNullOrEmpty(pharmacy))
         {
            if (ProfileGroupSelection.Equals(Constants.GroupType))
            {
               //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
               //objPolicyforProfileProductLot.PolicyForGroupProfileID = Convert.ToInt32(pharmacy);
            }
            else
            {
               //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
               //objPolicyforProfileProductLot.PolicyForProfileCode = Convert.ToInt32(pharmacy);
            }
         }

         objPolicyforProfileProductLot.CreatedDate = DateTime.UtcNow;
         lstPolicyProfileType.Add(objPolicyforProfileProductLot);

         return lstPolicyProfileType;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the number of records will insert into database in case of
      ///   published and customer for ProductSpecificToNDCUPCLOT.
      /// </summary>
      /// <param name="MWRProfileCode"></param>
      /// <param name="nDCUPCLOT"></param>
      /// <param name="PolicyID"></param>
      /// <param name="MWRProfileType"></param>
      /// <param name="ProfileGroupSelection"></param>
      /// <param name="pharmacy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyRel> PolicyForProductSpecificToNDCUPCLOT(string MWRProfileCode, string nDCUPCLOT, long PolicyID, string MWRProfileType, string ProfileGroupSelection, [Optional] string pharmacy)
      {
         var lstPolicyProfileType = new List<PolicyRel>();
         var ProductListwithLot = Regex.Split(nDCUPCLOT, ",");

         foreach (var itemPRdLot in ProductListwithLot)
         {
            var objPolicyforProfileProductLot = new PolicyRel();
            var ProductlotManu = Regex.Split(itemPRdLot, "_");

            if (ProductlotManu.Count() > 1)
            {
               //objPolicyforProfileProductLot.PolicyID = PolicyID;
               objPolicyforProfileProductLot.ProfileCode = Convert.ToInt32(MWRProfileCode);
               objPolicyforProfileProductLot.ProductID = Convert.ToInt32(ProductlotManu[1]);

               if (ProductlotManu.Count() > 2)
               {
                  objPolicyforProfileProductLot.LotNumber = (ProductlotManu[2]);
               }

               //objPolicyforProfileProductLot.ProfileType = (MWRProfileType);

               if (!string.IsNullOrEmpty(pharmacy))
               {
                  if (ProfileGroupSelection.Equals(Constants.GroupType))
                  {
                     //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
                     //objPolicyforProfileProductLot.PolicyForGroupProfileID = Convert.ToInt32(pharmacy);
                  }
                  else
                  {
                     //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
                     //objPolicyforProfileProductLot.PolicyForProfileCode = Convert.ToInt32(pharmacy);
                  }
               }
            }

            objPolicyforProfileProductLot.CreatedDate = DateTime.UtcNow;
            lstPolicyProfileType.Add(objPolicyforProfileProductLot);
         }

         return lstPolicyProfileType;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the number of records will insert into database in case of
      ///   published and customer for PolicyForProductSpecificToNDCUPC.
      /// </summary>
      /// <param name="MWRProfileCode"></param>
      /// <param name="nDCUPC"></param>
      /// <param name="PolicyID"></param>
      /// <param name="MWRProfileType"></param>
      /// <param name="ProfileGroupSelection"></param>
      /// <param name="pharmacy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyRel> PolicyForProductSpecificToNDCUPC(string MWRProfileCode, string nDCUPC, long PolicyID, string MWRProfileType, string ProfileGroupSelection, [Optional] string pharmacy)
      {
         var lstPolicyProfileType = new List<PolicyRel>();
         var ProductNDCList = Regex.Split(nDCUPC, ",");

         foreach (var itemPRdNDC in ProductNDCList)
         {
            var objPolicyforProfileProductLot = new PolicyRel();
            var ProductNDCManu = Regex.Split(itemPRdNDC, "_");

            if (ProductNDCManu.Count() > 1)
            {
               //objPolicyforProfileProductLot.PolicyID = PolicyID;
               objPolicyforProfileProductLot.ProfileCode = Convert.ToInt32(MWRProfileCode);
               objPolicyforProfileProductLot.ProductID = Convert.ToInt32(ProductNDCManu[1]);

               //objPolicyforProfileProductLot.ProfileType = (MWRProfileType);
            }

            if (!string.IsNullOrEmpty(pharmacy))
            {
               if (!string.IsNullOrEmpty(ProfileGroupSelection) && ProfileGroupSelection.Equals(Constants.GroupType))
               {
                  //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
                  //objPolicyforProfileProductLot.PolicyForGroupProfileID = Convert.ToInt32(pharmacy);
               }
               else
               {
                  //objPolicyforProfileProductLot.PharmacyProfileGroupType = ProfileGroupSelection;
                  //objPolicyforProfileProductLot.PolicyForProfileCode = Convert.ToInt32(pharmacy);
               }
            }

            objPolicyforProfileProductLot.CreatedDate = DateTime.UtcNow;
            lstPolicyProfileType.Add(objPolicyforProfileProductLot);
         }

         return lstPolicyProfileType;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get the number of combination of records to insert into database for
      ///   manufacturer and repackager, manufacturer group and wholesaler group.
      /// </summary>
      /// <param name="NDCUPCLOT"></param>
      /// <param name="Pharmacyprofile"></param>
      /// <param name="LasteInsertedpolicyID"></param>
      /// <param name="MWRProfileCode"></param>
      /// <param name="MWRProfileType"></param>
      /// <param name="ProfileGroupSelection"></param>
      /// <param name="PolicyType"></param>
      /// <param name="NDCUPC"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<PolicyRel> GetListofRecordsforInsertIntoDatabase(string NDCUPCLOT, string Pharmacyprofile, long LasteInsertedpolicyID, string MWRProfileCode, string MWRProfileType, string ProfileGroupSelection, string PolicyType, string NDCUPC)
      {
         try
         {
            //if (PolicyType == Constants.Model_PolicyDataAccess_PolicyTypeOne)
            //{
            //   return PolicyForAllProducts(MWRProfileCode, LasteInsertedpolicyID, MWRProfileType, ProfileGroupSelection, Pharmacyprofile);
            //}
            //else if (PolicyType == Constants.Model_PolicyDataAccess_PolicyTypeTwo)
            //{
            //   return PolicyForProductSpecificToNDCUPC(MWRProfileCode, NDCUPC, LasteInsertedpolicyID, MWRProfileType, ProfileGroupSelection, Pharmacyprofile);
            //}
            //else if (PolicyType == Constants.Model_PolicyDataAccess_PolicyTypeThree)
            //{
            //   return PolicyForProductSpecificToNDCUPCLOT(MWRProfileCode, NDCUPCLOT, LasteInsertedpolicyID, MWRProfileType, ProfileGroupSelection, Pharmacyprofile);
            //}
         }
         catch (Exception)
         {
            throw;
         }

         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the policy existence to customer specific
      /// </summary>
      /// <param name="WMRProfileCode"></param>
      /// <param name="PharmacyProfileCode"></param>
      /// <param name="ProductNDC"></param>
      /// <param name="ProductNDCWithLOt"></param>
      /// <param name="policyType"></param>
      /// <param name="SelectionType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckPolicyExistanceForCustomerSpecific(int WMRProfileCode, int PharmacyProfileCode, string ProductNDC, string ProductNDCWithLOt, string policyType, string SelectionType)
      {
         var IsExist = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var ProfileCode = 0;
               var Product = 0L;
               var Lot = string.Empty;

               switch (policyType)
               {
                  // ...
                  //=============================================================================
                  case Constants.AllProducts:
                     {
                        if (!string.IsNullOrEmpty(SelectionType) && SelectionType.Equals(Constants.GroupType))
                        {
                           //var isexist = (from ppt in cloudModelEntities.PolicyProfileType.Where(u => u.ProfileCode == WMRProfileCode && u.PolicyForGroupProfileID == PharmacyProfileCode && u.ProductID == null)
                           //               join p in cloudModelEntities.Policy.Where(u => u.PolicyType == 1) on ppt.PolicyID equals p.PolicyID
                           //               select p).FirstOrDefault();
                           //if (isexist != null)
                           //{
                           //   IsExist = true;
                           //   return IsExist;
                           //}
                           //   return IsExist;
                        }

                        //var isexist = (from ppt in cloudModelEntities.PolicyProfileType.Where(u => u.ProfileCode == WMRProfileCode && u.PolicyForProfileCode == PharmacyProfileCode && u.ProductID == null)
                        //               join p in cloudModelEntities.Policy.Where(u => u.PolicyType == 1) on ppt.PolicyID equals p.PolicyID
                        //               select p).FirstOrDefault();
                        //if (isexist != null)
                        //{
                        //   IsExist = true;
                        //   return IsExist;
                        //}
                        //   return IsExist;
                        break;
                     }

                  // ...
                  //=============================================================================
                  case Constants.ProductsSpecifictoNDCUPC:
                     {
                        if (!string.IsNullOrEmpty(SelectionType) && SelectionType.Equals(Constants.GroupType))
                        {
                           if (!string.IsNullOrEmpty(ProductNDC))
                           {
                              var ProductList = Regex.Split(ProductNDC, "_");

                              if (ProductList.Count() > 1)
                              {
                                 ProfileCode = Convert.ToInt32(ProductList[0]);
                                 Product = Convert.ToInt64(ProductList[1]);
                              }

                              //var isexist = cloudModelEntities.PolicyProfileType
                              //   .FirstOrDefault(u => u.ProfileCode == ProfileCode
                              //                        && u.PolicyForGroupProfileID == PharmacyProfileCode && u.ProductID == Product && u.LotNumber == null);
                              //if (isexist != null)
                              //{
                              //   IsExist = true;
                              //   return IsExist;
                              //}
                              //else
                              //{
                              //   return IsExist;
                              //}
                           }
                        }
                        else
                        {
                           if (!string.IsNullOrEmpty(ProductNDC))
                           {
                              var ProductList = Regex.Split(ProductNDC, "_");

                              if (ProductList.Count() > 1)
                              {
                                 ProfileCode = Convert.ToInt32(ProductList[0]);
                                 Product = Convert.ToInt64(ProductList[1]);
                              }

                              //var isexist = cloudModelEntities.PolicyProfileType
                              //   .FirstOrDefault(u => u.ProfileCode == ProfileCode
                              //                        && u.PolicyForProfileCode == PharmacyProfileCode && u.ProductID == Product && u.LotNumber == null);
                              //if (isexist != null)
                              //{
                              //   IsExist = true;
                              //   return IsExist;
                              //}
                              //else
                              //{
                              //   return IsExist;
                              //}
                           }
                        }

                        break;
                     }

                  // ...
                  //=============================================================================
                  case Constants.ProductsSpecifictoNDCUPCandLotNumber:
                     {
                        if (!string.IsNullOrEmpty(SelectionType) && SelectionType.Equals(Constants.GroupType))
                        {
                           if (!string.IsNullOrEmpty(ProductNDCWithLOt))
                           {
                              var ProductListwithLot = Regex.Split(ProductNDCWithLOt, "_");

                              if (ProductListwithLot.Count() > 1)
                              {
                                 ProfileCode = Convert.ToInt32(ProductListwithLot[0]);
                                 Product = Convert.ToInt64(ProductListwithLot[1]);
                                 Lot = ProductListwithLot[2];
                              }
                           }

                           //var isexist = cloudModelEntities.PolicyProfileType
                           //   .FirstOrDefault(u => u.ProfileCode == ProfileCode
                           //                        && u.PolicyForGroupProfileID == PharmacyProfileCode && u.ProductID == Product && u.LotNumber == Lot);
                           //if (isexist != null)
                           //{
                           //   IsExist = true;
                           //   return IsExist;
                           //}
                           //   return IsExist;
                        }
                        else
                        {
                           if (!string.IsNullOrEmpty(ProductNDCWithLOt))
                           {
                              var ProductListwithLot = Regex.Split(ProductNDCWithLOt, "_");

                              if (ProductListwithLot.Count() > 1)
                              {
                                 ProfileCode = Convert.ToInt32(ProductListwithLot[0]);
                                 Product = Convert.ToInt64(ProductListwithLot[1]);
                                 Lot = ProductListwithLot[2];
                              }
                           }

                           //var isexist = cloudModelEntities.PolicyProfileType
                           //   .FirstOrDefault(u => u.ProfileCode == ProfileCode
                           //                        && u.PolicyForProfileCode == PharmacyProfileCode && u.ProductID == Product && u.LotNumber == Lot);
                           //if (isexist != null)
                           //{
                           //   IsExist = true;
                           //   return IsExist;
                           //}
                           //return IsExist;
                        }

                        break;
                     }


               }

               return IsExist;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Check the policy existence for published policy.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="productNDC"></param>
      /// <param name="productNDCWithLot"></param>
      /// <param name="policyType"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool CheckPolicyExistanceForPublished(int profileCode, string productNDC, string productNDCWithLot, string policyType)
      {
         try
         {
            long? productID = null;
            string lotNo = null;

            switch (policyType)
            {
               // ...
               //=============================================================================
               case Constants.ProductsSpecifictoNDCUPC:
               {
                  if (!string.IsNullOrEmpty(productNDC))
                  {
                     var paramList = Regex.Split(productNDC, "_");

                     if (paramList.Length > 1)
                     {
                        profileCode = Convert.ToInt32(paramList[0]);
                        productID = Convert.ToInt64(paramList[1]);
                     }
                  }

                  break;
               }

               // ...
               //=============================================================================
               case Constants.ProductsSpecifictoNDCUPCandLotNumber:
               {
                  if (!string.IsNullOrEmpty(productNDCWithLot))
                  {
                     var paramList = Regex.Split(productNDCWithLot, "_");

                     if (paramList.Length > 1)
                     {
                        profileCode = Convert.ToInt32(paramList[0]);
                        productID = Convert.ToInt64(paramList[1]);
                        lotNo = paramList[2];
                     }
                  }

                  break;
               }
            }

            // ...
            ///////////////////////////////////////////////////////////////////////////////
            using (var cloudEntities = new QoskCloud())
            {
               return (from ppt in cloudEntities.PolicyRel
                       where ppt.ManufacturerProfileCode == profileCode &&
                              ppt.ProductID == productID &&
                               ppt.LotNumber == lotNo &&
                                ppt.IsDeleted == false
                       select ppt).FirstOrDefault() != null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Profile list of group member like manufacturer group and wholesaler group
      /// </summary>
      /// <param name="groupID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetGroupProfile(int groupID)
      {
         var ProfileCodeArr = string.Empty;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //int[] arr = (from p in cloudEntities.GroupProfiles where p.ProfileGroupID == groupID select p.ProfileCode).ToArray();
               //ProfileCodeArr = string.Join(",", arr);
               return ProfileCodeArr;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="policyID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ViewEdit GetPolicyDetails(long policyID)
      {
         var lstViewEdit = new ViewEdit();

         try
         {
            //using (var cloudEntities = new QoskCloud())
            //{
            //   lstViewEdit = (from p in cloudEntities.Policy.Where(p => p.PolicyID == policyID)
            //                  join ppt in cloudEntities.PolicyRel on p.PolicyID equals ppt.PolicyRelID
            //                  select new ViewEdit
            //                  {
            //                     PolicyID = p.PolicyID,
            //                     //SelectedPolicyType = p.PolicyTypeName,
            //                     //PolicyTypeName = p.PolicyTypeName.Equals(Constants.PublishedPolicy) ? 1 : 2,
            //                     //PolicyType = p.PolicyType,
            //                     //PolicyTypeText = p.PolicyType == 1 ? Constants.AllProducts : p.PolicyType == 2 ? Constants.ProductsSpecifictoNDCUPC : p.PolicyType == 3 ? Constants.ProductsSpecifictoNDCUPCandLotNumber : null,
            //                     //SelectedMWWRProfileType = ppt.ProfileType,
            //                     //MWWRProfileType = ppt.ProfileType.Equals(Constants.Manufacturer) ? 1 : ppt.ProfileType.Equals(Constants.ManufacturerGroup) ? 2 : ppt.ProfileType.Equals(Constants.Wholesaler) ? 3 : ppt.ProfileType.Equals(Constants.WholesalerGroup) ? 4 : ppt.ProfileType.Equals(Constants.Repackager) ? 5 : 0,
            //                     SelectedMWWRProfileCode = ppt.ProfileCode,
            //                     //SelectedPharmacyGroupProfileCode = ppt.PolicyForProfileCode,
            //                     //ProfileGroupSelectionType = ppt.PharmacyProfileGroupType == null ? Constants.Model_PolicyDataAccess_NA : ppt.PharmacyProfileGroupType
            //                  }).FirstOrDefault();
            //}

            if (lstViewEdit.SelectedMWWRProfileType.Equals(Constants.ManufacturerGroup))
            {
               lstViewEdit.MWRName = GetGroupName(lstViewEdit.SelectedMWWRProfileCode, Constants.MFGRGRP);
               lstViewEdit.SelectedPharmacyGroupName = GetProfileName(lstViewEdit.SelectedPharmacyGroupProfileCode);
               lstViewEdit.SelectedPharmacyGroupName = lstViewEdit.SelectedPharmacyGroupName ?? Constants.Model_PolicyDataAccess_NA;

               return lstViewEdit;
            }

            lstViewEdit.MWRName = GetProfileName(lstViewEdit.SelectedMWWRProfileCode);
            lstViewEdit.SelectedPharmacyGroupName = GetProfileName(lstViewEdit.SelectedPharmacyGroupProfileCode);
            lstViewEdit.SelectedPharmacyGroupName = lstViewEdit.SelectedPharmacyGroupName ?? Constants.Model_PolicyDataAccess_NA;

            return lstViewEdit;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetProfileName(int? profileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from p in cloudEntities.Profile
                       where p.ProfileCode == profileCode
                       select p.Name).FirstOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="groupID"></param>
      /// <param name="grpType"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static string GetGroupName(int? groupID, string grpType)
      {
         try
         {
            //using (var cloudEntities = new QoskCloud())
            //{
            //   return (from p in cloudEntities.ProfileGroup
            //           where p.ProfileGroupID == groupID && p.GroupType.Equals(grpType)
            //           select p.GroupName).FirstOrDefault();
            //}
            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

   }
}
