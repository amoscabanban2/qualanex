﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="Binventory.cs">
///   Copyright (c) 2016 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Model
{
   using System;
   using System.IO;
   using System.ComponentModel;
   using System.Collections.Generic;
   using System.Linq;
   using System.Web.Mvc;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.QoskCloud;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Microsoft.WindowsAzure.Storage;
   using Microsoft.WindowsAzure.Storage.Blob;
   using Microsoft.WindowsAzure.Storage.Shared.Protocol;

   using Qualanex.QoskCloud.Web.Common;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;
   using Model;
   using Utility.Common;
   using Utility;
   using System.Data.SqlClient;

   public class BinventoryDataAccess
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public BinventoryDataAccess()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~BinventoryDataAccess()
      {
         this.Dispose(false);
      }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"NDC/UPC")]
      public string NDC { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"MFG")]
      public string MFG { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"MFG Profile Code")]
      public int MfgProfileCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Green Bin Reason")]
      public bool IsGrnBinReason { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Brand")]
      public string Brand { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Generic")]
      public string Generic { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Dosage")]
      public string Dosage { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Ctrl#")]
      public string CtrlNumber { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Unit/Pkg")]
      public string Unit { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Rx/OTC")]
      public string Rx { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"PkgSize")]
      public decimal? PackageSize { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"WrhsContrID")]
      public string WrhsContrID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Item Guid")]
      public Guid ItemGUID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Product ID")]
      public int ProductID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Rollup Product ID")]
      public int RollupProductId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Status Date")]
      public DateTime StatusDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Product")]
      public string ProductName { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"TrackingID")]
      public string TrackingID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"RA or WPT")]
      public string RAorWPT { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Station ID")]
      public string StationId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Destination")]
      public string Destination { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Recall ID")]
      public long? RecallID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Inductor")]
      public string Inductor { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Warehouse container type")]
      public string WrhsContrType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"InboundBoxID")]
      public string InboundBoxID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Debit Memo Number")]
      public string DebitMemo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"SeqNo")]
      public short? SeqNo { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Wrhs Container InstanceId")]
      public string WrhsContrInst { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Status")]
      public string Status { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Sorting Location")]
      public string OutboundContainerID { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created Date")]
      public DateTime? CreatedDate { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Created By")]
      public string CreatedBy { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Code")]
      public string WasteCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Time Processing")]
      public string TimeProcessing { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////

      [DisplayName(@"All Items Count")]

      public float AllItemsCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Category")]
      public string Category { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"ApprovalCode")]
      public string ApprovalCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"DestinationCode")]
      public string DestinationCode { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Sorted")]
      public int Sorted { get; set; } = 0;

      /////////////////////////////////////////////////////////////////////////////// 
      /// <summary>
      ///   Summary not available. 
      /// </summary> 
      /////////////////////////////////////////////////////////////////////////////// 
      [DisplayName(@"DestinationType")]
      public string DestinationType { get; set; }

      ///////////////////////////////////////////////////////////////////////////////  
      /// <summary> 
      ///   Summary not available.   
      /// </summary>        
      /////////////////////////////////////////////////////////////////////////////// 
      [DisplayName(@"Original Path")]
      public string OriginalPath { get; set; }

      ///////////////////////////////////////////////////////////////////////////////  
      /// <summary>    
      ///   Summary not available.  
      /// </summary>     
      ///////////////////////////////////////////////////////////////////////////////   
      [DisplayName(@"OriginalCategory")]
      public string OriginalCategory { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Profile ID")]
      public string WasteProfileId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"Waste Strea, Profile ID")]
      public string WasteStreamProfileId { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"SpecialHandling")]
      public string SpecialHandling { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      [DisplayName(@"isForeignContainer")]
      public string isForeignContainer { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      #endregion 

      #region Properties



      ///////////////////////////////////////////////////////////////////////////////

      /// <summary>

      ///   Summary not available.

      /// </summary>

      ///////////////////////////////////////////////////////////////////////////////

      public bool IsDisposed

      {

         get

         {

            lock (this._disposedLock)

            {

               return this._isDisposed;

            }

         }

      }



      #endregion 

      #region Disposal



      ///****************************************************************************

      /// <summary>

      ///   Checks this object's Disposed flag for state.

      /// </summary>

      ///****************************************************************************

      private void CheckDisposal()

      {

         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);

      }



      ///****************************************************************************

      /// <summary>

      ///   Performs the object specific tasks for resource disposal.

      /// </summary>

      ///****************************************************************************

      public void Dispose()

      {

         this.Dispose(true);

         GC.SuppressFinalize(this);

      }



      ///****************************************************************************

      /// <summary>

      ///   Performs object-defined tasks associated with freeing, releasing, or

      ///   resetting unmanaged resources.

      /// </summary>

      /// <param name="programmaticDisposal">

      ///   If true, the method has been called directly or indirectly.  Managed

      ///   and unmanaged resources can be disposed.  When false, the method has

      ///   been called by the runtime from inside the finalizer and should not

      ///   reference other objects.  Only unmanaged resources can be disposed.

      /// </param>

      ///****************************************************************************

      private void Dispose(bool programmaticDisposal)

      {

         if (!this._isDisposed)

         {

            lock (this._disposedLock)

            {

               if (programmaticDisposal)

               {

                  try

                  {

                     ;

                     ;  // TODO: dispose managed state (managed objects).

                     ;

                  }

                  catch (Exception ex)

                  {

                     Log.Write(LogMask.Error, ex.ToString());

                  }

                  finally

                  {

                     this._isDisposed = true;

                  }

               }



              ;

               ;   // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.

               ;   // TODO: set large fields to null.

               ;



            }

         }

      }



      #endregion   

      #region Methods 

      ///**************************************************************************** 
      /// <summary> 
      ///   Get List of items. 
      /// </summary>  
      /// <param name="binId"></param>
      /// <param name="stationId"></param>
      /// <param name="stateCode"></param>
      /// <param name="appcode"></param>
      /// <returns></returns>

      ///****************************************************************************
      public static List<BinventoryDataAccess> GetBinventoryList(string binId, string stationId, string stateCode, string appcode)
      {
         List<BinventoryDataAccess> lst = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {

               var sqlQuery = " select distinct it.ItemGUID,it.ProductID,ISNULL(it.RollupProductID,0) as RollupProductId,it.TrackingID,it.RecallID,prdct.Description as ProductName ," +
                  " wrhsType.Description as WrhsContrType, prdct.Description as Brand ,dsg.Description as Dosage ," +
                  " prdct.GenericName as Generic , convert(nvarchar, prdct.UnitsPerPackage) as Unit, qosk.MachineID as StationId," +
                  " case when convert(bit, prdct.RXorOTC)= 1 then 'RX' else 'OTC' end as Rx, it.TrackingID as TrackingID, " +
                  " mfrPrfl.Name as MFG, (select case when SpecialHandling = 0 then 0 else ProfileCode end from profile where profilecode in " +
                  " (select MfrProfileCode from DebitMemo where DebitMemoID in (select DebitMemoID from ReturnAuthorization where RtrnAuthID in " +
                  " (select associd from Tracking where TrackingID in (select TrackingID from Item where ItemGUID = it.ItemGuid))))) as MfgProfileCode, " +
                  //" prdct.PackageSize as PackageSize, wrhsStatus.StatusCode as Status, wrhsRel.WrhsContrID as InBoundBoxID, " +
                  " prdct.PackageSize as PackageSize, 'S' as Status, wrhsRel.WrhsContrID as InBoundBoxID, " +
                  " convert(nvarchar, prdct.ControlNumber) as CtrlNumber, prdct.NDCUPCWithDashes as NDC, " +
                  " (select top(1)wrhscontrid from WrhsContrItemRel rl where rl.ItemGUID = it.ItemGUID and rl.IsActive = 1 " +
                  " order by rl.CreatedDate desc, rl.ModifiedDate desc) as WrhsContrId, it.WasteStreamProfileID as WasteStreamProfileId, " +
                  " case when(select top(1)wrhscontrid from WrhsContrItemRel rl where rl.ItemGUID = it.ItemGUID and rl.IsActive = 1 " +
                  " order by rl.CreatedDate desc, rl.ModifiedDate desc) <> wrhsRel.WrhsContrID then 1 else 0 end as Sorted ," +
                  " case when @appcode = 'WAREHS_SORT' then (select case when " +
                  " exists(select rsn.PropertyCode from PropertyValueRel rsn where rsn.RelationCode = convert(nvarchar(max), it.ItemGUID) " +
                  " and rsn.IsDeleted = 0 and rsn.SValue = 'R') then cast(1 as bit) else cast(0 as bit) end) else cast(0 as bit) end as IsGrnBinReason, " +
                  " convert(nvarchar,it.RecallID) as WasteProfileID ,'RCL_GAYLRD' as DestinationType, convert(nvarchar,it.RecallID) as DestinationCode, " +
                  " case when it.RecallID is not null and it.RecallID <> 0 then 'Recall -' + convert(nvarchar, (select top(1) RecallNumber from Recall rcl where it.RecallID = rcl.RecallID)) " +
                  " else '' end as Destination, it.CreatedBy as CreatedBy, " +
                  " case when exists (select * from ItemConditionRel where ItemGUID = it.ItemGUID and ConditionCode = 'ForeignContainer' and AppCode = 'MASTER' and isDeleted = 0) then 'true' else 'false' end as isForeignContainer, " +
                  " case when mfrPrfl.SpecialHandling = 1 then 'True' else 'False' end as SpecialHandling" +
                  " from item it " +
                  " join product prdct on it.ProductID = prdct.ProductID " +
                  " join qosk on it.QoskID = qosk.QoskID " +
                  " join Profile mfrPrfl  on prdct.ProfileCode = mfrPrfl.ProfileCode and mfrPrfl.ProfileTypeCode = 'MFGR' " +
                  " join DosageDict dsg on prdct.DosageCode = dsg.Code " +
                  " left outer join WrhsContrItemRel wrhsRel on it.ItemGUID = wrhsRel.ItemGUID " +
                  //" join WrhsContrStatusHist wrhsStatus on wrhsRel.WrhsContrID = wrhsStatus.WrhsContrID and wrhsRel.WrhsInstID = wrhsStatus.WrhsInstID " +
                  " join WrhsContr wrhsCntr on wrhsRel.WrhsContrID = wrhsCntr.WrhsContrID " +
                  " join WrhsContrTypeDict wrhsType on wrhsCntr.Type = wrhsType.Code " +
                  " where wrhsRel.WrhsContrID = @bin " +
                  //" and wrhsCntr.WrhsContrID = @bin " +
                  " and wrhsType.Designation = 'B' " +
                  //" and wrhsStatus.StatusCode = 'S' " +
                  " and wrhsRel.WrhsInstID = (select top(1)wrhsStst.WrhsInstID from WrhsContrStatusHist " +
                  " wrhsStst where wrhsStst.WrhsContrID = wrhsCntr.WrhsContrID order by wrhsStst.CreatedDate desc ) ";

                cloudEntities.Database.CommandTimeout = 600;
                lst = cloudEntities.Database.SqlQuery<BinventoryDataAccess>(sqlQuery,
                  new SqlParameter("@appcode", appcode),
                  new SqlParameter("@bin", binId)).ToList();

               SetDestinationInformation(stationId, stateCode, cloudEntities, lst);
               lst = lst.OrderBy(s => s.Sorted).ToList();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return lst;
      }


      ///**************************************************************************** 
      /// <summary> 
      ///   Set Destination and sorting location. 
      /// </summary>  
      /// <param name="stationId"></param>
      /// <param name="stateCode"></param>
      /// <param name="cloudEntities"></param>
      /// <param name="lst"></param>
      /// <returns>list of items</returns>        
      ///****************************************************************************
      private static void SetDestinationInformation(string stationId, string stateCode, QoskCloud cloudEntities, List<BinventoryDataAccess> lst)
      {
         foreach (var items in lst)
         {
            var wasteStreamCodes = GetWasteStreamInfo(items.RollupProductId > 0 ? items.RollupProductId : items.ProductID, stateCode, items.ItemGUID.ToString());

            if ((items.RecallID == 0 || string.IsNullOrWhiteSpace(items.RecallID.ToString())) && !items.IsGrnBinReason)
            {
               if (wasteStreamCodes == null)
               {
                  items.WasteProfileId = "";
                  items.DestinationType = "LRG_GAYLRD";
                  items.DestinationCode = "Large Gaylord";
                  items.Destination = "Gaylord";
               }
               else
               {
                  items.WasteProfileId = wasteStreamCodes.Category;
                  items.DestinationCode = wasteStreamCodes.DestinationCode;
                  items.Destination = wasteStreamCodes.Destination;

                  if (wasteStreamCodes.WasteCode.StartsWith("Q9"))
                  {
                     switch (items.CtrlNumber)
                     {
                        case "0":
                           {
                              items.DestinationType = "LRG_GAYLRD";
                              items.Destination = "Control 0 Gaylord";
                              break;
                           }
                        case "2":
                           {
                              items.DestinationType = "BLK_GAYLRD";
                              items.Destination = "Control 2 Gaylord";
                              break;
                           }
                        default:
                           {
                              items.DestinationType = "ORG_GAYLRD";
                              items.Destination = "Control 3 4 5 Gaylord";
                              break;
                           }
                     }
                  }
                  else
                  {
                     items.DestinationType = "WST_GAYLRD";
                  }
               }

            }
            else if ((items.RecallID > 0 || !string.IsNullOrWhiteSpace(items.RecallID.ToString())) && !items.IsGrnBinReason)
            {
               if (wasteStreamCodes == null)
               {
                  items.WasteProfileId = "";
                  items.DestinationType = "RCL_GAYLRD";
                  items.DestinationCode = items.RecallID.ToString();
                  items.Destination = "Recall " + items.RecallID.ToString() + " Gaylord";
               }
               else
               {
                  items.WasteProfileId = wasteStreamCodes.Category;
               }
            }
            else if (items.IsGrnBinReason)
            {
               items.OriginalCategory = items.WasteProfileId;
               items.OriginalPath = items.DestinationType;
               items.WasteProfileId = "";
               items.ApprovalCode = "0";
               items.DestinationCode = "Green Bin";
               items.Destination = "Green Bin";
               items.DestinationType = "GRN_BIN";
            }

            if (items.Sorted == 1)
            {
               items.OutboundContainerID = items.WrhsContrID;
               items.Destination = (from wrhsCntr in cloudEntities.WrhsContr
                                    join wrhsCntrType in cloudEntities.WrhsContrTypeDict on wrhsCntr.Type equals wrhsCntrType.Code
                                    where wrhsCntr.WrhsContrID == items.WrhsContrID && wrhsCntrType.Designation == "V"
                                    select wrhsCntrType.Description).FirstOrDefault() ?? items.Destination;
            }

            //comment this stationID assignment; station ID is the current machine's name, not the machine name where the item was inducted
            //items.StationId = stationId;
         }
      }

      ///****************************************************************************

      /// <summary>  
      ///   Get Waste Stream record.   
      /// </summary>    
      /// <param name="productId"></param> 
      /// <param name="stateCode"></param> 
      /// <param name="itemGuid"></param> 
      /// <returns></returns>

      ///****************************************************************************    
      public static BinventoryDataAccess GetWasteStreamInfo(long productId, string stateCode, string itemGuid)
      {
         using (var cloudEntities = new QoskCloud())
         {
            var sqlQuery =
                "select top(1) prdct.WasteCode as WasteCode,prdct.WasteStreamCode as DestinationCode , wstStreamProfileRel.WasteStreamProfileID as Category," +
                " wstStreamProfile.ApprovalCode as ApprovalCode , wrhsStream.Description as Destination " +
                " from ProductWasteStream prdct" +
                " join WasteStreamProfileRel wstStreamProfileRel on prdct.WasteStreamCode = wstStreamProfileRel.WasteStreamCode" +
                " join WasteStreamProfile wstStreamProfile on wstStreamProfileRel.WasteStreamProfileID = wstStreamProfile.WasteProfileID" +
                " join WasteStreamDict wrhsStream on prdct.WasteStreamCode = wrhsStream.Code" +
                " left outer join StateDict stateDict on prdct.StateCode = stateDict.Code" +
                " where prdct.ProductID = @prdctId and(prdct.StateCode = @stCode or prdct.StateCode = '') " +
                " and wstStreamProfileRel.WasteStreamProfileID in (select WasteStreamProfileID from item where itemguid = @guid) " +
                " and wstStreamProfileRel.IsDeleted = 0 and wstStreamProfile.IsDeleted = 0 and wrhsStream.IsDeleted = 0 and stateDict.IsDeleted = 0 " +
                " order by prdct.SeverityOrder desc";

            return cloudEntities.Database.SqlQuery<BinventoryDataAccess>(sqlQuery,
               new SqlParameter("@prdctId", productId),
               new SqlParameter("@stCode", stateCode),
               new SqlParameter("@guid", itemGuid)).FirstOrDefault();
         }
      }

      ///**************************************************************************** 
      /// <summary> 
      ///   Get info for a missorted item
      /// </summary> 
      /// <param name="itemGUID"></param> 
      /// <param name="stationId"></param> 
      /// <param name="stateCode"></param> 
      /// <param name="appcode"></param> 
      /// <returns></returns> 
      ///**************************************************************************** 
      public static BinventoryDataAccess GetItemBinInfo(string itemGUID, string stationId, string stateCode, string appcode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var sqlQuery =
                  " select distinct it.ItemGUID,it.ProductID,ISNULL(it.RollupProductID,0) as RollupProductId,it.TrackingID,it.RecallID,prdct.Description as ProductName ," +
                  " wrhsType.Code as WrhsContrType, prdct.Description as Brand ,dsg.Description as Dosage ," +
                  " prdct.GenericName as Generic , convert(nvarchar, prdct.UnitsPerPackage) as Unit, qosk.MachineID as StationId," +
                  " case when convert(bit, prdct.RXorOTC)= 1 then 'RX' else 'OTC' end as Rx, it.TrackingID as TrackingID, " +
                  " mfrPrfl.Name as MFG, (select case when SpecialHandling = 0 then 0 else ProfileCode end from profile where profilecode in " +
                  " (select MfrProfileCode from DebitMemo where DebitMemoID in (select DebitMemoID from ReturnAuthorization where RtrnAuthID in " +
                  " (select associd from Tracking where TrackingID in (select TrackingID from Item where ItemGUID = it.ItemGuid))))) as MfgProfileCode, " +
                  " prdct.PackageSize as PackageSize, wrhsStatus.StatusCode as Status, wrhsRel.WrhsContrID as InBoundBoxID, " +
                  " convert(nvarchar, prdct.ControlNumber) as CtrlNumber, prdct.NDCUPCWithDashes as NDC, " +
                  " (select top(1)wrhscontrid from WrhsContrItemRel rl where rl.ItemGUID = it.ItemGUID and rl.IsActive = 1 " +
                  " order by rl.CreatedDate desc, rl.ModifiedDate desc) as WrhsContrId, it.WasteStreamProfileID as WasteStreamProfileId, " +
                  " case when(select top(1)wrhscontrid from WrhsContrItemRel rl where rl.ItemGUID = it.ItemGUID and rl.IsActive = 1 " +
                  " order by rl.CreatedDate desc, rl.ModifiedDate desc) <> wrhsRel.WrhsContrID then 1 else 0 end as Sorted ," +
                  " case when @appcode = 'WAREHS_SORT' then (select case when " +
                  " exists(select rsn.PropertyCode from PropertyValueRel rsn where rsn.RelationCode = convert(nvarchar(max), it.ItemGUID) " +
                  " and rsn.IsDeleted = 0 and rsn.SValue = 'R') then cast(1 as bit) else cast(0 as bit) end) else cast(0 as bit) end as IsGrnBinReason, " +
                  " convert(nvarchar,it.RecallID) as WasteProfileID ,'RCL_GAYLRD' as DestinationType, convert(nvarchar,it.RecallID) as DestinationCode, " +
                  " case when it.RecallID is not null and it.RecallID <> 0 then 'Recall -' + convert(nvarchar, (select top(1) RecallNumber from Recall rcl where it.RecallID = rcl.RecallID)) " +
                  " else '' end as Destination, it.CreatedBy as CreatedBy" +
                  " from item it " +
                  " join qosk on it.QoskID = qosk.QoskID " +
                  " join product prdct on it.ProductID = prdct.ProductID " +
                  " join Profile mfrPrfl  on prdct.ProfileCode = mfrPrfl.ProfileCode and mfrPrfl.ProfileTypeCode = 'MFGR' " +
                  " join DosageDict dsg on prdct.DosageCode = dsg.Code " +
                  " left outer join WrhsContrItemRel wrhsRel on it.ItemGUID = wrhsRel.ItemGUID " +
                  " join WrhsContrStatusHist wrhsStatus on wrhsRel.WrhsContrID = wrhsStatus.WrhsContrID and wrhsRel.WrhsInstID = wrhsStatus.WrhsInstID " +
                  " join WrhsContr wrhsCntr on wrhsRel.WrhsContrID = wrhsCntr.WrhsContrID " +
                  " join WrhsContrTypeDict wrhsType on wrhsCntr.Type = wrhsType.Code " +
                  " where cast(wrhsRel.ItemGUID as nvarchar(128)) = @itemGUID " +
                  " and wrhsType.Designation in ('B','G') " +
                  " and wrhsRel.IsActive = 1 " +
                  " and wrhsRel.WrhsInstID = (select top(1)wrhsStst.WrhsInstID from WrhsContrStatusHist " +
                  " wrhsStst where wrhsStst.WrhsContrID = wrhsCntr.WrhsContrID order by wrhsStst.CreatedDate desc ) ";

               var lst = cloudEntities.Database.SqlQuery<BinventoryDataAccess>(sqlQuery,
                  new SqlParameter("@appcode", appcode),
                  new SqlParameter("@itemGUID", itemGUID)).ToList();

               SetDestinationInformation(stationId, stateCode, cloudEntities, lst);
               return lst.FirstOrDefault();
            }
         }
         catch (Exception ex)
         {
            Log.Write(LogMask.Error, ex.ToString());
         }

         return null;
      }

      #endregion
   }
}