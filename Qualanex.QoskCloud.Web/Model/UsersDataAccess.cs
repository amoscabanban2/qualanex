﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserDataAccess.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   internal class UsersDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userName">
      ///   Description not available.
      /// </param>
      /// <param name="userPassword">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UserRegistrationRequest ValidateUserLogin(string userName, string userPassword)
      {
         ParameterValidation.Begin().IsStringNotNullOrEmpty(() => userName)
                                     .IsStringNotNullOrEmpty(() => userPassword);

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from user in cloudEntities.User
                       join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                       join profileType in cloudEntities.ProfileTypeDict on profile.ProfileTypeCode equals profileType.Code
                       join userType in cloudEntities.UserTypeDict on user.UserTypeCode equals userType.Code
                       where user.UserName.Equals(userName.Trim()) && user.Password.Equals(userPassword) && !user.IsDeleted && user.IsEnabled && !user.IsLocked
                       select new UserRegistrationRequest
                       {
                          UserID = user.UserID,
                          UserName = user.UserName,
                          Email = user.Email,
                          UserProfileType = profileType.Description,
                          FirstName = user.FirstName,
                          LastName = user.LastName,
                          ProfileCode = user.ProfileCode,
                          RollupProfileCode = profile.RollupProfileCode,
                          UserTypeCode = userType.Code,
                          IsTemporaryPassword = user.IsTemporaryPassword ?? false
                       }).SingleOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userName">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UserRegistrationRequest ValidateUserLogin(string userName)
      {
         ParameterValidation.Begin().IsStringNotNullOrEmpty(() => userName);

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from user in cloudEntities.User
                       join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                       join profileType in cloudEntities.ProfileTypeDict on profile.ProfileTypeCode equals profileType.Code
                       join userType in cloudEntities.UserTypeDict on user.UserTypeCode equals userType.Code
                       where user.UserName.Equals(userName.Trim()) && !user.IsDeleted && user.IsEnabled && !user.IsLocked
                       select new UserRegistrationRequest
                       {
                          UserID = user.UserID,
                          UserName = user.UserName,
                          Email = user.Email,
                          UserProfileType = profileType.Description,
                          FirstName = user.FirstName,
                          LastName = user.LastName,
                          ProfileCode = user.ProfileCode,
                          RollupProfileCode = profile.RollupProfileCode,
                          UserTypeCode = userType.Code,
                          IsTemporaryPassword = user.IsTemporaryPassword ?? false
                       }).SingleOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userName">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static long ValidateUserName(string userName)
      {
         ParameterValidation.Begin().IsStringNotNullOrEmpty(() => userName);

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from user in cloudEntities.User
                       where user.UserName.Equals(userName.Trim())
                       select user.UserID).FirstOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="parentMenuID">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static List<LeftMenuLinks> GetLeftLinksData(int parentMenuID = 1)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return (from menuLinks in cloudEntities.Menu
                       where menuLinks.ParentMenuID == parentMenuID && (menuLinks.IsActive ?? false)
                       select new LeftMenuLinks
                       {
                          PageName = menuLinks.PageName,
                          PageURL = menuLinks.PageURL,
                          PageURLClass = menuLinks.PageURLClass,
                          Application = menuLinks.Application,
                          ParentMenuID = menuLinks.ParentMenuID ?? 0
                       }).ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UsersEntity GetUserDefaultData()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               return new UsersEntity
               {
                  UserType = (from usrType in cloudEntities.UserTypeDict
                              select new UserType
                              {
                                 UserTypeCode = usrType.Code,
                                 UserTypeName = usrType.Description
                              }).OrderBy(pp => pp.UserTypeName).ToList()
               };
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserSearch">
      ///   Description not available.
      /// </param>
      /// <param name="sortdatafield">
      ///   Description not available.
      /// </param>
      /// <param name="sortorder">
      ///   Description not available.
      /// </param>
      /// <param name="pagesize">
      ///   Description not available.
      /// </param>
      /// <param name="pagenum">
      ///   Description not available.
      /// </param>
      /// <param name="buildquery">
      ///   Description not available.
      /// </param>
      /// <param name="totalrecord">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UsersEntity GetUserData(UserSearch objUserSearch, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         var adminUserType = string.Empty;
         IEnumerable<UserRegistrationRequest> UsersReturnData;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var objUsersEntity = new UsersEntity();
               var selectedUserName = objUserSearch.UserName;
               var selectedUserTypeCode = objUserSearch.UserSearchTypeCode;
               var objUserAcessbilitydata = new UserAccessbilityHelper();

               //Get Assigned Profiles from UserProfile table based on current User ID
               long[] selectedUserIDs = objUserAcessbilitydata.GetUserList(objUserSearch).ToArray();
               //Filter the data base user Type -Site,Corporate user and Regional user

               if (objUserSearch.UserTypeName.Equals(Constants.HomeController_ADMIN))
               {
                  adminUserType = Constants.HomeController_ADMIN;
               }

               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (!string.IsNullOrEmpty(selectedUserName))
               {
                  buildquery = buildquery + Constants.Model_UsersDataAccess_User_UserName + selectedUserName + Constants.PolicyController_PERCENT;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.PolicyController_ANDWITHSPACE;

               if (!string.IsNullOrEmpty(selectedUserTypeCode) && selectedUserTypeCode != Constants.Model_UsersDataAccess_User_MinusOne)
               {
                  buildquery = buildquery + Constants.Model_UsersDataAccess_User_UserType + selectedUserTypeCode + Constants.PolicyController_COMMA;
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               buildquery += Constants.Model_UsersDataAccess_User_UserMsg;

               UsersReturnData = (from user in cloudEntities.User
                                  join userType in cloudEntities.UserTypeDict
                                  on user.UserTypeCode equals userType.Code
                                  where selectedUserIDs.Contains(user.UserID) && !user.IsDeleted //|| objUserSearch.UserTypeName.Equals(adminUserType)
                                  select new UserRegistrationRequest
                                  {
                                     UserID = user.UserID,
                                     UserName = user.UserName,
                                     Email = user.Email,
                                     //Name = user.Name,
                                     FirstName = user.FirstName,
                                     LastName = user.LastName,
                                     ProfileCode = user.ProfileCode,
                                     PhoneNumber = user.VoiceNumber,
                                     UserTypeCode = user.UserTypeCode ?? "UNKN",
                                     FaxNumber = user.FaxNumber,
                                     UserTypeName = userType.Description,
                                     CreatedBy = user.CreatedBy,
                                     CreatedDate = user.CreatedDate,
                                     ModifiedBy = user.ModifiedBy,
                                     ModifiedDate = user.ModifiedDate
                                  }).OrderBy(pp => pp.UserName).ToList();

               // Retrieve to view corporate user when site and region user login
               if (objUserSearch.UserTypeCode == "SITE" || objUserSearch.UserTypeCode == "REGN")
               {
                  //UsersReturnData = (from usrdata in UsersReturnData
                  //                   where usrdata.UserTypeCode != 2
                  //                   select usrdata).OrderBy(pp => pp.UserName).ToList();
               }

               if (UsersReturnData.Any())
               {
                  if (!string.IsNullOrEmpty(sortorder))
                  {
                     if (sortorder == Constants.Model_GroupDataAccess_Asc)
                     {
                        UsersReturnData = UsersReturnData.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                     }
                     else
                     {
                        UsersReturnData = UsersReturnData.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
                     }
                  }
                  totalrecord = UsersReturnData.Count();
                  UsersReturnData = UsersReturnData.Skip(pagesize * pagenum).Take(pagesize);
                  objUsersEntity.UsersReturnData = UsersReturnData.ToList();
                  return objUsersEntity;
               }
               else
               {
                  totalrecord = 0;
                  var emptydata = new List<UserRegistrationRequest>();
                  objUsersEntity.UsersReturnData = emptydata;
                  return objUsersEntity;
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         totalrecord = 0;
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userData">
      ///   Description not available.
      /// </param>
      /// <param name="userID">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static bool UserRegistration(UserRegistrationRequest userData, string userID)
      {
         User userentity = null;

         using (var cloudEntities = new QoskCloud())
         {
            using (var dbTran = cloudEntities.Database.BeginTransaction())
            {
               try
               {
                  //Get last insert data in user table
                  var newuserID = (from user in cloudEntities.User
                                   select user.UserID).Max();

                  userentity = (from usr in cloudEntities.User
                                where usr.Id.Equals(userID)
                                select usr).SingleOrDefault();

                  userentity.UserID = ++newuserID;

                  if (userData.MLUserGroups != null)
                  {
                     foreach (var userAssignedGroups in userData.MLUserGroups)
                     {
                        var objUserGroups = new UserGroups
                        {
                           UserID = newuserID,
                           ProfileGroupID = userAssignedGroups.ProfileGroupID,
                           CreatedBy = userData.CreatedBy,
                           CreatedDateTime = DateTime.UtcNow
                        };

                        cloudEntities.UserGroups.Add(objUserGroups);
                     }
                  }

                  if (userData.MLProfileGroup != null)
                  {
                     foreach (var userAssignedProfiles in userData.MLProfileGroup)
                     {
                        var objUserProfiles = new UserProfiles
                        {
                           UserID = newuserID,
                           ProfileCode = userAssignedProfiles.GroupProfileCode,
                           RegionCode = userAssignedProfiles.Region,
                           ProfileGroupID = userAssignedProfiles.ProfileGroupID
                        };

                        cloudEntities.UserProfiles.Add(objUserProfiles);
                     }
                  }

                  cloudEntities.SaveChanges();
                  dbTran.Commit();

                  return true;
               }
               catch (DbEntityValidationException dbEx)
               {
                  Logger.DBError(dbEx);
               }
               catch (Exception ex)
               {
                  Logger.Error(ex.Message);
               }

               dbTran.Rollback();
               cloudEntities.User.Remove(userentity);
               return false;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete User based on userID.
      /// </summary>
      /// <param name="userID">
      ///   Description not available.
      /// </param>
      /// <param name="modifiedBy">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static Response DeleteUser(long userID, string modifiedBy)
      {
         var objclsResponse = new Response { Status = Status.Fail };

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var userItem = (from user in cloudEntities.User
                               where user.UserID == userID
                                     && user.IsDeleted == false
                               select user).SingleOrDefault();

               if (userItem != null)
               {
                  userItem.IsDeleted = true;

                  objclsResponse.Status = cloudEntities.SaveChanges() > 0
                     ? Status.OK
                     : Status.Fail;
               }
               else
               {
                  objclsResponse.Status = Status.Deleted;
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="userdata">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      private static User MapObjectforUser(User userdata)
      {
         var userauditItem = new User()
         {
            Id = userdata.Id,
            UserID = userdata.UserID,
            UserName = userdata.UserName,
            Email = userdata.Email,
            FirstName = userdata.FirstName,
            LastName = userdata.LastName,
            UserTypeCode = userdata.UserTypeCode ?? "",
            IsEnabled = userdata.IsEnabled,
            ProfileCode = userdata.ProfileCode,
            VoiceNumber = userdata.VoiceNumber,
            FaxNumber = userdata.FaxNumber,
            CreatedBy = userdata.CreatedBy,
            CreatedDate = userdata.CreatedDate,
            ModifiedDate = userdata.ModifiedDate,
            Version = userdata.Version
         };

         return userauditItem;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User based on userID.
      /// </summary>
      /// <param name="userdata">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static Response UpdateUser(UserRegistrationRequest userdata)
      {
         var objclsResponse = new Response();
         User usersItem = null;

         using (var cloudEntities = new QoskCloud())
         {
            using (var dbTran = cloudEntities.Database.BeginTransaction())
            {
               try
               {
                  usersItem = (from user in cloudEntities.User
                               where user.UserID == userdata.UserID
                               select user).SingleOrDefault();

                  if (usersItem.IsDeleted != true)
                  {

                     if (usersItem.Version != userdata.Version)
                     {
                        objclsResponse.response = MapObjectforUser(usersItem);
                        objclsResponse.UpdateVersion = usersItem.Version;
                        objclsResponse.Status = Status.VersionChange;
                        dbTran.Rollback();
                     }
                     else
                     {
                        usersItem.UserName = userdata.UserName;
                        usersItem.FirstName = userdata.FirstName;
                        usersItem.LastName = userdata.LastName;
                        usersItem.Email = userdata.Email;
                        usersItem.VoiceNumber = userdata.PhoneNumber;
                        usersItem.FaxNumber = userdata.FaxNumber;
                        usersItem.ProfileCode = userdata.ProfileCode;
                        usersItem.UserTypeCode = userdata.UserTypeCode;
                        usersItem.ModifiedDate = DateTime.UtcNow;
                        usersItem.IsEnabled = true;
                        usersItem.Version = usersItem.Version + 1;

                        //Update Profile Audit table

                        //userAudititem = MapObject(usersItem);
                        //userAudititem.Action = Constants.Model_PolicyDataAccess_Update;
                        //userAudititem.ActionBy = userdata.ModifiedBy;
                        //userAudititem.ActionDate = DateTime.UtcNow;
                        //cloudModelEntities.UserAudit.Add(userAudititem);

                        var userProfileList = (from userprl in cloudEntities.UserProfiles
                                               where userprl.UserID == userdata.UserID
                                               select userprl).ToList();
                        foreach (var AssignedProfile in userProfileList)
                        {
                           var objUserProfilesData = AssignedProfile;
                           cloudEntities.UserProfiles.Remove(objUserProfilesData);
                        }

                        int result = cloudEntities.SaveChanges();

                        if (userdata.MLUserGroups != null)
                        {
                           foreach (var userAssignedGroups in userdata.MLUserGroups)
                           {
                              //UserGroups objUserGroups = new UserGroups
                              //{
                              //   UserID = Convert.ToInt64(userdata.UserID),
                              //   ProfileGroupID = userAssignedGroups.ProfileGroupID,
                              //   CreatedBy = userdata.CreatedBy,
                              //   CreatedDateTime = DateTime.UtcNow

                              //};

                              //cloudEntities.UserGroups.Add(objUserGroups);
                           }

                           cloudEntities.SaveChanges();
                        }

                        if (userdata.MLProfileGroup != null)
                        {
                           foreach (var userAssignedProfiles in userdata.MLProfileGroup)
                           {
                              var objUserProfiles = new UserProfiles
                              {
                                 UserID = Convert.ToInt64(userdata.UserID),
                                 ProfileCode = userAssignedProfiles.GroupProfileCode,
                                 RegionCode = userAssignedProfiles.Region,
                                 ProfileGroupID = userAssignedProfiles.ProfileGroupID
                              };

                              cloudEntities.UserProfiles.Add(objUserProfiles);
                           }

                           cloudEntities.SaveChanges();
                        }

                        if (result > 0)
                        {
                           objclsResponse.response = usersItem;
                           objclsResponse.UpdateVersion = usersItem.Version;
                           objclsResponse.Status = Status.OK;

                           dbTran.Commit();
                        }
                        else
                        {
                           objclsResponse.response = usersItem;
                           objclsResponse.Status = Status.Rollback;

                           dbTran.Rollback();
                        }
                     }

                     return objclsResponse;
                  }

                  objclsResponse.Status = Status.Deleted;
               }
               catch (DbEntityValidationException dbEx)
               {
                  Logger.DBError(dbEx);

                  objclsResponse.response = usersItem;
                  objclsResponse.Status = Status.Fail;
                  objclsResponse.UpdateVersion = userdata.Version;
               }
               catch (Exception ex)
               {
                  Logger.Error(ex.Message);

                  dbTran.Rollback();

                  objclsResponse.response = usersItem;
                  objclsResponse.Status = Status.Fail;
               }

               return objclsResponse;
            }

         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static UserRegistrationRequest GetUserByUserId(int userID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var usersData = (from user in cloudEntities.User
                                join usrType in cloudEntities.UserTypeDict on user.UserTypeCode equals usrType.Code
                                join profile in cloudEntities.Profile on user.ProfileCode equals profile.ProfileCode
                                where user.UserID == userID
                                select new UserRegistrationRequest
                                {
                                   UserID = user.UserID,
                                   UserName = user.UserName,
                                   Email = user.Email,
                                   Name = profile.Name,
                                   Password = user.Password,
                                   FirstName = user.FirstName,
                                   LastName = user.LastName,
                                   UserTypeName = usrType.Description,
                                   UserTypeCode = user.UserTypeCode ?? "",
                                   Enabled = user.IsEnabled,
                                   ProfileCode = user.ProfileCode,
                                   PhoneNumber = user.VoiceNumber,
                                   FaxNumber = user.FaxNumber,
                                   CreatedBy = user.CreatedBy,
                                   CreatedDate = user.CreatedDate,
                                   ModifiedBy = user.ModifiedBy,
                                   ModifiedDate = user.ModifiedDate,
                                   Version = user.Version
                                }).SingleOrDefault();

               if (usersData != null)
               {
                  usersData.MLProfileGroup = new List<AssignedProfileGroup>();

                  var userProfile = (from userprofile in cloudEntities.UserProfiles
                                     join profile in cloudEntities.Profile on userprofile.ProfileCode equals profile.ProfileCode
                                     where userprofile.UserID == userID
                                     select new AssignedProfileGroup
                                     {
                                        GroupProfileCode = userprofile.ProfileCode,
                                        userID = userprofile.UserID,
                                        GroupProfileName = profile.Name,
                                        Region = userprofile.RegionCode
                                     }).OrderBy(pp => pp.GroupProfileName).ToList();

                  usersData.MLProfileGroup = userProfile;

                  var userGrpProfile = (from usergrpprofile in cloudEntities.UserGroups
                                        join grp in cloudEntities.ProfileGroup on usergrpprofile.ProfileGroupID equals grp.ProfileGroupID
                                        where usergrpprofile.UserID == userID
                                        select new MLGroup
                                        {
                                           ProfileGroupID = usergrpprofile.ProfileGroupID,
                                           GroupName = grp.GroupName
                                        }).OrderBy(pp => pp.GroupName).ToList();

                  usersData.MLUserGroups = userGrpProfile;
               }

               return usersData;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User based on userID.
      /// </summary>
      /// <param name="userID">
      ///   Description not available.
      /// </param>
      /// <param name="newPwd">
      ///   Description not available.
      /// </param>
      /// <param name="oldPwd">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static bool UpdateUserPasword(string userID, string newPwd, out string oldPwd)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var usersItem = (from user in cloudEntities.User
                                where user.Id.Equals(userID)
                                select user).SingleOrDefault();

               usersItem.Password = newPwd;
               usersItem.IsTemporaryPassword = true;

               //=============================================================================
               // I'm not really sure what this logic is, or should be doing.  It appears
               // that the old password (variable) is set to the new password regardless if
               // the user information is persisted to the database or not.  I'm leery to
               // re -factor this too much since it may break something else that's
               // dependent on this bizarre behavior.
               //-----------------------------------------------------------------------------

               if (cloudEntities.SaveChanges() > 0)
               {
                  oldPwd = usersItem.Password;
                  return true;
               }

               oldPwd = usersItem.Password;
               return false;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
         }

         oldPwd = string.Empty;
         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Change Password.
      /// </summary>
      /// <param name="ChangePssswordViewModel">
      ///   Description not available.
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool ChangePassword(ChangeUserPassword ChangePssswordViewModel)
      {
         var result = false;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var usersItem = (from user in cloudEntities.User
                                where user.UserID == ChangePssswordViewModel.UserID && user.Password.Equals(ChangePssswordViewModel.OldPassword)
                                select user).SingleOrDefault();

               if (usersItem != null)
               {
                  usersItem.Password = ChangePssswordViewModel.NewPassword;
                  usersItem.IsTemporaryPassword = false;
                  cloudEntities.SaveChanges();
                  result = true;
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Validate User Forgot Password.
      /// </summary>
      /// <param name="changePssswordViewModel">
      ///   Description not available.
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool ValidateUserForgotPassword(ChangeUserPassword changePssswordViewModel)
      {
         try
         {
            using (var cloudModelEntities = new QoskCloud())
            {
               var usersItem = (from user in cloudModelEntities.User
                                where user.UserID == changePssswordViewModel.UserID && user.Password.Equals(changePssswordViewModel.OldPassword)
                                select user).SingleOrDefault();

               return usersItem == null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update User based on userID.
      /// </summary>
      /// <param name="profileType">
      ///   Description not available.
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<Entity.UserType> GetUserTypes(string profileType)
      {
         IEnumerable<Entity.UserType> userTypes = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (profileType.Equals(Constants.Model_GroupDataAccess_Manufacturer) || profileType.Equals(Constants.Model_GroupDataAccess_Repackager))
               {
                  userTypes = (from user in cloudEntities.UserTypeDict
                               where user.Code == "CORP"
                               select new Entity.UserType
                               {
                                  UserTypeCode = user.Code,
                                  UserTypeName = user.Description
                               }).OrderBy(pp => pp.UserTypeName).ToList();
               }
               else if (profileType.Equals(Constants.Model_ProfilesDataAccess_QualanexInternal))
               {
                  userTypes = (from user in cloudEntities.UserTypeDict
                               where user.Code == "ADMN"
                               select new Entity.UserType
                               {
                                  UserTypeCode = user.Code,
                                  UserTypeName = user.Description
                               }).OrderBy(pp => pp.UserTypeName).ToList();
               }
               else
               {
                  userTypes = (from user in cloudEntities.UserTypeDict
                               where user.Code != "ADMN"
                               select new Entity.UserType
                               {
                                  UserTypeCode = user.Code,
                                  UserTypeName = user.Description
                               }).OrderBy(pp => pp.UserTypeName).ToList();
               }
            }

            return userTypes;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Update ProfileCode in User based on userID.
      /// </summary>
      /// <param name="profileCode">
      /// </param>
      /// <param name="userId">
      /// </param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool SaveUserProfile(int profileCode, long userId)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var userEntity = cloudEntities.User
                  .Where(c => c.UserID == userId)
                  .FirstOrDefault();
               userEntity.ProfileCode = profileCode;
               cloudEntities.SaveChanges();
            }
            return true;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            return false;
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static string ConnectionTest()
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var testResult = Constants.Model_UsersDataAccess_User_FourHundred;

               var productData = (from p in cloudEntities.Product
                                  select p).ToList();

               if (productData.Count > 0)
               {
                  testResult = Constants.Model_UsersDataAccess_User_TwoHundred;
               }

               return testResult;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

   }
}
