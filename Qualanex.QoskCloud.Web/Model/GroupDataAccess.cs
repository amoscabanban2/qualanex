﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="GroupDataAccess.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Diagnostics;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   internal class GroupDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   CreateGroup method used to create group.
      /// </summary>
      /// <param name="groupName"></param>
      /// <param name="profileCode"></param>
      /// <param name="groupType"></param>
      /// <param name="createdBy"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int CreateGroup(string groupName, int profileCode, string groupType, string createdBy)
      {
         var groupID = 0;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //int secprofileCode = (from groupsec in cloudModelEntities.Group
               //                      where (groupsec.GroupName.Equals(groupName) && groupsec.ProfileCode == profileCode)
               //                      select groupsec.ProfileCode).SingleOrDefault();
               //if (secprofileCode != profileCode)
               //{
               //   var objGroup = new Group();
               //   objGroup.GroupName = groupName.Trim();
               //   objGroup.ProfileCode = profileCode;
               //   objGroup.GroupType = groupType;
               //   objGroup.Created = createdBy;
               //   objGroup.CreatedDateTime = DateTime.UtcNow;
               //   cloudModelEntities.Group.Add(objGroup);
               //   cloudModelEntities.SaveChanges();
               //   groupID = objGroup.ProfileGroupID;
               //   return groupID;
               //}
               //else
               //{
               //   groupID = secprofileCode;
               //}
               return groupID;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get all Group profile based on current puser profile
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static GroupProfileViewModel GetGroupProfileCode(int profileCode, string GroupList, string userprofile, string groupName, string GroupTypeName)
      {
         ICollection<MLGroupProfiles> listGroupProfiles = null;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var selectedProfileCode = Convert.ToInt32(userprofile);

               if (string.IsNullOrEmpty(GroupList) && string.IsNullOrEmpty(userprofile) && string.IsNullOrEmpty(GroupTypeName))
               {
                  selectedProfileCode = -1;
               }
               //var objlistGroupProfiles = from profile in cloudModelEntities.Profile
               //                           join grpProfile in cloudModelEntities.GroupProfiles on profile.ProfileCode equals grpProfile.ProfileCode into grpprllist from list in grpprllist.DefaultIfEmpty()
               //                           join grp in cloudModelEntities.Group on list.ProfileGroupID equals grp.ProfileGroupID into grplist from grpsection in grplist.DefaultIfEmpty()
               //                           where profile.RollupProfileCode == selectedProfileCode
               //                           orderby profile.Name
               //                           select new {profile, list, grpsection};

               if (!string.IsNullOrWhiteSpace(GroupList))
               {
                  var groupID = Convert.ToInt32(GroupList);
                  //objlistGroupProfiles = objlistGroupProfiles.Where(q => q.grpsection.ProfileGroupID == groupID);
               }

               if (!string.IsNullOrWhiteSpace(GroupTypeName))
               {
                  //objlistGroupProfiles = objlistGroupProfiles.Where(q => q.grpsection.GroupType.Contains(GroupTypeName));
               }
               //listGroupProfiles = (from profile in objlistGroupProfiles
               //                     orderby profile.profile.Name
               //                     select new MLGroupProfiles
               //                     {
               //                        ProfileGroupID = (profile.list.ProfileGroupID == 0) ? 0 : profile.list.ProfileGroupID,
               //                        ProfileName = profile.profile.Name,
               //                        ProfileType = profile.profile.Type,
               //                        RegionCode = profile.profile.RegionCode,
               //                        State = profile.profile.State,
               //                        City = profile.profile.City,
               //                        ProfileCode = profile.profile.ProfileCode,
               //                        ModifiedDateTime = profile.list.ModifiedDateTime,
               //                        ModifiedBy = profile.list.ModifiedBy,
               //                        ID = (profile.list.ID == 0) ? -1 : profile.list.ID,
               //                        IsAssigned = profile.list.IsAssigned ?? false,
               //                        ProfileGroupName = (profile.grpsection.GroupName == null) ? string.Empty : profile.grpsection.GroupName,
               //                        CreatedBy = profile.list.Created,
               //                     }).OrderBy(pp => pp.ProfileName).ToList();

               //List of group data
               var objGroupProfileViewModel = new GroupProfileViewModel { GroupProfileData = listGroupProfiles };
               //Create Group
               var obj = new List<MLGroup>();
               obj.Add(new MLGroup { GroupName = Constants.Model_GroupDataAccess_SelectGroup, ProfileCode = 0, ProfileGroupID = 0 });
               objGroupProfileViewModel.GroupData = obj;

               //var profileType = (from p in cloudModelEntities.Types
               //                   where p.Type.Equals(Constants.Model_GroupDataAccess_ChainPharmacy) || p.Type.Equals(Constants.Model_GroupDataAccess_Manufacturer) || p.Type.Equals(Constants.Model_GroupDataAccess_RetailPharmacy) || p.Type.Equals(Constants.Model_GroupDataAccess_Repackager) || p.Type.Equals(Constants.Model_GroupDataAccess_Wholesaler)
               //                   select p).ToList();
               //objGroupProfileViewModel.ProfileTpe = profileType.Select(n => new SelectListItem
               //{
               //   Text = n.Type,
               //   Value = n.Type
               //});

               return objGroupProfileViewModel;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all Group profile based on current user profile.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLGroupProfiles> ShowProfileGroupData(int profileCode, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {

               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               if (profileCode > 0)
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_ProfilesProfileCode + profileCode + "";
               }
               else
               {
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var detailsresult = cloudModelEntities.USP_GetProfileGroupData(Constants.Model_GroupDataAccess_ProfileGroup, buildquery);
               //listGroupProfiles = (from profile in detailsresult
               //                     orderby profile.Name
               //                     select new MLGroupProfiles
               //                     {
               //                        ProfileGroupID = profile.ProfileGroupID ?? 0,
               //                        ProfileName = profile.Name,
               //                        ProfileType = profile.Type,
               //                        RegionCode = profile.RegionCode,
               //                        State = profile.State,
               //                        City = profile.City,
               //                        ProfileCode = profile.ProfileCode,
               //                        ModifiedDateTime = profile.ModifiedDateTime,
               //                        ModifiedBy = profile.ModifiedBy,
               //                        ID = profile.ID ?? -1,
               //                        IsAssigned = profile.IsAssigned ?? false,
               //                        ProfileGroupName = (profile.GroupName == null) ? string.Empty : profile.GroupName,
               //                        CreatedBy = profile.Created,
               //                     }).OrderBy(pp => pp.ProfileName).ToList();
               //totalrecord = listGroupProfiles.Count();

               //return listGroupProfiles;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         totalrecord = 0;
         return null;
      }

      ///****************************************************************************
      /// <summary>
      ///   Get all Group profile based on current user profile.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLGroupProfiles> GetPartialGroupProfile(int profileCode, string GroupList, string userprofile, string groupName, string GroupTypeName, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         IEnumerable<MLGroupProfiles> listGroupProfiles = null;

         var selectedProfileCode = -1;
         var selectedgrpProfileCode = -1;
         var groupID = 0;
         var caseType = 0;
         var totalnorecord = 0;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {

               if (string.IsNullOrEmpty(GroupList) && string.IsNullOrEmpty(userprofile) && string.IsNullOrEmpty(GroupTypeName))
               {
                  selectedProfileCode = selectedgrpProfileCode = -1;
                  caseType = 0;
               }
               else if (string.IsNullOrEmpty(GroupList) && !string.IsNullOrEmpty(userprofile) && string.IsNullOrEmpty(GroupTypeName) && string.IsNullOrEmpty(groupName))
               {
                  // Group: Select Group
                  caseType = 1;
                  selectedProfileCode = selectedgrpProfileCode = Convert.ToInt32(userprofile);
               }
               else if (GroupList.Equals(Constants.Model_UsersDataAccess_User_MinusOne) && !string.IsNullOrEmpty(userprofile) && !string.IsNullOrEmpty(GroupTypeName) && !string.IsNullOrEmpty(groupName))
               {
                  caseType = 2;
                  selectedProfileCode = Convert.ToInt32(userprofile);
                  selectedgrpProfileCode = Convert.ToInt32(userprofile);

                  groupID = Convert.ToInt32(GroupList);
               }
               else
               {
                  selectedProfileCode = selectedgrpProfileCode = Convert.ToInt32(userprofile);
                  groupID = !string.IsNullOrEmpty(GroupList) ? Convert.ToInt32(GroupList) : 0;
                  caseType = 3;
               }

               switch (caseType)
               {
                  case 0:
                     listGroupProfiles = null;
                     break;

                  case 1:
                     listGroupProfiles = GetGroupProfiles(selectedProfileCode, selectedgrpProfileCode, groupID, GroupTypeName, buildquery, pagesize, pagenum, out totalnorecord);
                     break;

                  case 2:
                     listGroupProfiles = GetGroupProfiles(selectedProfileCode, selectedgrpProfileCode, groupID, GroupTypeName, buildquery, pagesize, pagenum, out totalnorecord);
                     break;

                  case 3:
                     listGroupProfiles = GetGroupProfiles(selectedProfileCode, selectedgrpProfileCode, groupID, GroupTypeName, buildquery, pagesize, pagenum, out totalnorecord);
                     break;
               }

               totalrecord = listGroupProfiles.Count();
               listGroupProfiles = listGroupProfiles.Skip(pagesize * pagenum).Take(pagesize);

               return listGroupProfiles.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Group Profiles.
      /// </summary>
      /// <param name="selectedProfileCode"></param>
      /// <param name="selectedgrpProfileCode"></param>
      /// <param name="groupID"></param>
      /// <param name="groupType"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static List<MLGroupProfiles> GetGroupProfiles(int selectedProfileCode, int selectedgrpProfileCode, int groupID, string groupType, string buildquery, int pagesize, int pagenum, out int totalrecord)
      {
         IEnumerable<MLGroupProfiles> listGroupProfiles = null;

         try
         {
            if (string.IsNullOrEmpty(buildquery))
            {
               buildquery = Constants.Model_GroupDataAccess_Where;
            }
            else
            {
               buildquery += Constants.PolicyController_ANDWITHSPACE;
            }

            if (selectedgrpProfileCode > 0)
            {
               buildquery = buildquery + Constants.Model_GroupDataAccess_ProfilesRollupProfileCode + selectedgrpProfileCode + "";
            }
            else
            {
               buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
            }

            //var detailsresult = cloudModelEntities.USP_GetProfileGroupData(Constants.Model_GroupDataAccess_GroupProfileData, buildquery);

            //listGroupProfiles = (from profile in detailsresult
            //                     orderby profile.Name
            //                     select new MLGroupProfiles
            //                     {
            //                        ProfileGroupID = profile.ProfileGroupID ?? 0,
            //                        ProfileName = profile.Name,
            //                        ProfileType = profile.Type,
            //                        RegionCode = profile.RegionCode,
            //                        State = profile.State,
            //                        City = profile.City,
            //                        ProfileCode = profile.ProfileCode,
            //                        ModifiedDateTime = profile.ModifiedDateTime,
            //                        ModifiedBy = profile.ModifiedBy,
            //                        ID = profile.ID ?? -1,
            //                        IsAssigned = (profile.IsAssigned.HasValue || profile.IsAssigned == true) ? true : false,
            //                        ProfileGroupName = (profile.GroupName == null) ? string.Empty : profile.GroupName,
            //                        CreatedBy = profile.Created,
            //                        GroupType = profile.GroupType
            //                     }).Distinct().OrderBy(pp => pp.ProfileName).ToList();

            if (listGroupProfiles != null && listGroupProfiles.Any())
            {
               if (groupID != -1 && groupID > 0)
               {
                  var listGroupProfiles1 = listGroupProfiles.Where(q => q.ProfileGroupID == groupID);

                  var listGroupProfilestemp = (from p in listGroupProfiles
                                               where p.ProfileGroupID != groupID
                                               select new MLGroupProfiles
                                               {
                                                  ProfileName = p.ProfileName,
                                                  ProfileType = p.ProfileType,
                                                  RegionCode = p.RegionCode,
                                                  State = p.State,
                                                  City = p.City,
                                                  ProfileCode = p.ProfileCode,
                                                  ModifiedDateTime = p.ModifiedDateTime,
                                                  ModifiedBy = p.ModifiedBy,
                                                  ID = p.ID,
                                                  IsAssigned = false,
                                                  ProfileGroupName = p.ProfileGroupName,
                                                  CreatedBy = p.CreatedBy,
                                                  GroupType = p.GroupType
                                               }).ToList();
                  if (listGroupProfilestemp != null && listGroupProfiles1 != null)
                     listGroupProfiles = listGroupProfiles1.Union(listGroupProfilestemp);

               }
               if (groupID != -1 && !string.IsNullOrWhiteSpace(groupType))
               {
                  var listGroupProfiles1 = listGroupProfiles.Where(q => q.GroupType == groupType);

                  var listGroupProfilestemp = (from p in listGroupProfiles
                                               where p.GroupType != groupType
                                               select new MLGroupProfiles
                                               {
                                                  ProfileName = p.ProfileName,
                                                  ProfileType = p.ProfileType,
                                                  RegionCode = p.RegionCode,
                                                  State = p.State,
                                                  City = p.City,
                                                  ProfileCode = p.ProfileCode,
                                                  ModifiedDateTime = p.ModifiedDateTime,
                                                  ModifiedBy = p.ModifiedBy,
                                                  ID = p.ID,
                                                  IsAssigned = false,
                                                  ProfileGroupName = p.ProfileGroupName,
                                                  CreatedBy = p.CreatedBy,
                                                  GroupType = p.GroupType
                                               }).ToList();
                  if (listGroupProfilestemp != null && listGroupProfiles1 != null)
                     listGroupProfiles = listGroupProfiles1.Union(listGroupProfilestemp);
               }

               totalrecord = listGroupProfiles.Count();
               return listGroupProfiles.ToList();
            }
            else
            {
               totalrecord = 0;
               return new List<MLGroupProfiles>();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Get Filter the User profile.
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="objMLGroupProfiles"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<MLGroupProfiles> GetFilterPartialGroupProfile(int profileCode, MLGroupProfiles objMLGroupProfiles)
      {
         ICollection<MLGroupProfiles> listGroupProfiles = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               var groupID = 0;

               if (objMLGroupProfiles.ProfileGroupID != -1)
               {
                  groupID = objMLGroupProfiles.ProfileGroupID;
               }

               var selectedProfileCode = objMLGroupProfiles.ProfileCode;

               //var objlistGroupProfiles1 = from profile in cloudModelEntities.Profiles
               //                            where profile.RollupProfileCode == selectedProfileCode
               //                            select new {profile};
               //var objlistGroupProfiles2 = from grpProfile in cloudModelEntities.GroupProfiles
               //                            join grp in cloudModelEntities.Group
               //                               on grpProfile.ProfileGroupID equals grp.ProfileGroupID
               //                            where grp.ProfileCode == selectedProfileCode
               //                            select new {grpProfile, grp};

               //if (groupID != -1)
               //{
               //   objlistGroupProfiles2 = objlistGroupProfiles2.Where(q => q.grp.ProfileGroupID == groupID);
               //}
               //if (groupID != -1 && !string.IsNullOrWhiteSpace(objMLGroupProfiles.GroupType))
               //{
               //   objlistGroupProfiles2 = objlistGroupProfiles2.Where(q => q.grp.GroupType.Contains(objMLGroupProfiles.GroupType));
               //}

               //var objlistGroupProfiles = from profile in objlistGroupProfiles1
               //                           join grpprofile in objlistGroupProfiles2
               //                              on profile.profile.ProfileCode equals grpprofile.grp.ProfileCode into grpprllist
               //                           from grpprofile in grpprllist.DefaultIfEmpty()
               //                           select new {profile, grpprofile};

               //if (!string.IsNullOrWhiteSpace(objMLGroupProfiles.RegionCode))
               //{
               //   objlistGroupProfiles = objlistGroupProfiles.Where(q => q.profile.profile.RegionCode.Contains(objMLGroupProfiles.RegionCode));
               //}
               //if (!string.IsNullOrWhiteSpace(objMLGroupProfiles.City))
               //{
               //   objlistGroupProfiles = objlistGroupProfiles.Where(q => q.profile.profile.City.Contains(objMLGroupProfiles.City));
               //}
               //if (!string.IsNullOrWhiteSpace(objMLGroupProfiles.State))
               //{
               //   objlistGroupProfiles = objlistGroupProfiles.Where(q => q.profile.profile.State.Contains(objMLGroupProfiles.State));
               //}
               //listGroupProfiles = (from profile in objlistGroupProfiles
               //                     orderby profile.profile.profile.Name
               //                     select new MLGroupProfiles
               //                     {
               //                        ProfileGroupID = (profile.grpprofile.grpProfile.ProfileGroupID == 0) ? 0 : profile.grpprofile.grpProfile.ProfileGroupID,
               //                        ProfileName = profile.profile.profile.Name,
               //                        ProfileType = profile.profile.profile.Type,
               //                        RegionCode = profile.profile.profile.RegionCode,
               //                        State = profile.profile.profile.State,
               //                        City = profile.profile.profile.City,
               //                        ProfileCode = profile.profile.profile.ProfileCode,
               //                        ModifiedDateTime = profile.grpprofile.grpProfile.ModifiedDateTime,
               //                        ModifiedBy = profile.grpprofile.grpProfile.ModifiedBy,
               //                        ID = (profile.grpprofile.grpProfile.ID == 0) ? -1 : profile.grpprofile.grpProfile.ID,
               //                        IsAssigned = profile.grpprofile.grpProfile.IsAssigned ?? false,
               //                        ProfileGroupName = (profile.grpprofile.grp.GroupName == null) ? string.Empty : profile.grpprofile.grp.GroupName,
               //                        CreatedBy = profile.grpprofile.grpProfile.Created,
               //                     }).OrderBy(pp => pp.ProfileName).ToList();

               return listGroupProfiles;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete the group and related user profiles.
      /// </summary>
      /// <param name="groupIDs"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static bool DeleteGroups(string groupIDs)
      {
         var selectedGroupID = groupIDs.Split(',');

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //foreach (string grp in selectedGroupID)
               //{
               //   int groupID = Convert.ToInt32(grp);
               //   if (!string.IsNullOrWhiteSpace(grp))
               //   {
               //      var grop = (from grup in cloudModelEntities.Group
               //                  where grup.ProfileGroupID == groupID
               //                  select grup).Single();
               //      cloudModelEntities.Group.Remove(grop);

               //      var profilegrop = (from grupProfile in cloudModelEntities.GroupProfiles
               //                         where grupProfile.ProfileGroupID == groupID
               //                         select grupProfile).ToList();
               //      cloudModelEntities.GroupProfiles.RemoveRange(profilegrop);
               //   }
               //}

               //cloudModelEntities.SaveChanges();
               return true;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Update group profile
      /// </summary>
      /// <param name="GroupId"></param>
      /// <param name="ProfileIDS"></param>
      /// <param name="currentProfile"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static int UpdateGroupProfiles(string GroupId, string ProfileIDS, int currentProfile, string GroupName, string selecteProfileCode, string groupType)
      {
         var createdgroupID = 0;
         var userprofileCode = 0;
         var groupID = 0;
         int[] selectedProfileID = null;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               using (var dbTran = cloudEntities.Database.BeginTransaction())
               {
                  try
                  {
                     if (!string.IsNullOrEmpty(ProfileIDS))
                     {
                        var list = ProfileIDS.Split(',');
                        selectedProfileID = Array.ConvertAll(list, int.Parse);

                        groupID = Convert.ToInt32(GroupId.Trim());
                        userprofileCode = Convert.ToInt32(selecteProfileCode);

                        if (GroupId.Equals(Constants.Model_UsersDataAccess_User_MinusOne))
                        {
                           createdgroupID = CreateGroup(GroupName, userprofileCode, groupType, "");
                           groupID = createdgroupID;

                           if (InsertNewGroupProfiles(groupID, selectedProfileID))
                           {
                              return groupID;
                           }
                        }
                     }

                     //Find the existing group profile list
                     //var profilegrop = (from grupProfile in cloudModelEntities.GroupProfiles
                     //                   join grp in cloudModelEntities.Group
                     //                      on grupProfile.ProfileGroupID equals grp.ProfileGroupID
                     //                   where grp.ProfileCode == userprofileCode && grp.GroupName.Equals(GroupName) && grp.GroupType.Equals(groupType)
                     //                   select grupProfile.ProfileCode).ToList();
                     /////Find unselected profile from selected group list
                     //var unselecetdGrpProfile = profilegrop.Except(selectedProfileID);
                     //foreach (int profileID in unselecetdGrpProfile)
                     //{
                     //   var profile = (from ungrpprl in cloudModelEntities.GroupProfiles
                     //                  where ungrpprl.ProfileGroupID == groupID && ungrpprl.ProfileCode == profileID
                     //                  select ungrpprl).ToList();
                     //   cloudModelEntities.GroupProfiles.RemoveRange(profile);
                     //   //
                     //}
                     //cloudModelEntities.SaveChanges();
                     /////Find new profile from selected group list
                     //var newSelectGrpProfile = selectedProfileID.Except(profilegrop);
                     ////Profile infirmation
                     //foreach (int profileID in newSelectGrpProfile)
                     //{
                     //   var profile = (from prl in cloudModelEntities.Profiles
                     //                  where prl.ProfileCode == profileID
                     //                  select new MLGroupProfiles
                     //                  {
                     //                     ProfileGroupID = groupID,
                     //                     ProfileCode = prl.ProfileCode,
                     //                     ProfileName = prl.Name,
                     //                     ProfileType = prl.Type,
                     //                     RegionCode = prl.RegionCode,
                     //                     City = prl.City,
                     //                     State = prl.State,
                     //                     CreatedDateTime = DateTime.UtcNow
                     //                  }).Single();

                     //   var objGroupProfiles = new GroupProfiles();
                     //   objGroupProfiles.ProfileGroupID = profile.ProfileGroupID;
                     //   objGroupProfiles.ProfileCode = profile.ProfileCode;
                     //   objGroupProfiles.IsAssigned = true;
                     //   cloudModelEntities.GroupProfiles.Add(objGroupProfiles);

                     //}
                     //cloudModelEntities.SaveChanges();
                     //dbTran.Commit();

                     return 0;
                  }
                  catch (DbEntityValidationException dbEx)
                  {
                     foreach (var validationErrors in dbEx.EntityValidationErrors)
                     {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                           Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                        }
                     }

                     dbTran.Rollback();
                     return -1;
                  }
                  catch (Exception ex)
                  {
                     Logger.Error(ex.Message);

                     dbTran.Rollback();
                     return -1;
                  }
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Save created group into Inserted selected profiles.
      /// </summary>
      /// <param name="groupID"></param>
      /// <param name="selectedProfileID"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static bool InsertNewGroupProfiles(int groupID, int[] selectedProfileID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //foreach (int profileID in selectedProfileID)
               //{
               //   int profileCode = Convert.ToInt32(profileID);
               //   var profile = (from prl in cloudModelEntities.Profiles
               //                  where prl.ProfileCode == profileCode
               //                  select new MLGroupProfiles
               //                  {
               //                     ProfileGroupID = groupID,
               //                     ProfileCode = prl.ProfileCode,
               //                     ProfileName = prl.Name,
               //                     ProfileType = prl.Type,
               //                     RegionCode = prl.RegionCode,
               //                     City = prl.City,
               //                     State = prl.State,
               //                     CreatedDateTime = DateTime.UtcNow
               //                  }).Single();

               //   GroupProfiles objGroupProfiles = new GroupProfiles();
               //   objGroupProfiles.ProfileGroupID = profile.ProfileGroupID;
               //   objGroupProfiles.ProfileCode = profile.ProfileCode;
               //   objGroupProfiles.IsAssigned = true;
               //   cloudModelEntities.GroupProfiles.Add(objGroupProfiles);
               //   cloudModelEntities.SaveChanges();
               //}
            }

            return true;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Group Name
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<MLGroup> GetProfileGroup(int profileCode)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //ICollection<MLGroup> groupList = (from grp in cloudModelEntities.Group
               //                                  where grp.ProfileCode == profileCode
               //                                  select new MLGroup
               //                                  {
               //                                     ProfileGroupID = grp.ProfileGroupID,
               //                                     GroupName = grp.GroupName,
               //                                  }).ToList();

               //groupList.Add(new MLGroup {GroupName = Constants.Model_GroupDataAccess_AddNewGroup, ProfileCode = -1, ProfileGroupID = -1});

               return null; //groupList;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Group Name
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static IEnumerable<MLGroup> EditProfileGroup(int profileCode, long userID)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //ICollection<MLGroup> groupList = (from grp in cloudModelEntities.Group
               //                                  join usergrp in cloudModelEntities.UserGroups
               //                                     on grp.ProfileGroupID equals usergrp.ProfileGroupID into tempusergrp
               //                                  from temp in tempusergrp.DefaultIfEmpty()
               //                                  where grp.ProfileCode == profileCode && temp.UserID == userID
               //                                  select new MLGroup
               //                                  {
               //                                     ProfileGroupID = grp.ProfileGroupID,
               //                                     GroupName = grp.GroupName,
               //                                     IsAssigned = (temp.UserID == 0) ? false : true
               //                                  }).OrderBy(pp => pp.GroupName).ToList();

               //groupList.Add(new MLGroup {GroupName = Constants.Model_GroupDataAccess_AddNewGroup, ProfileCode = -1, ProfileGroupID = -1});

               return null; //groupList;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Profiles Group Type
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetGroupType(int profileGroupID)
      {
         string groupProfileTypr = string.Empty;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //groupProfileTypr = (from grp in cloudModelEntities.Group
               //                    where grp.ProfileGroupID == profileGroupID
               //                    select grp.GroupType
               //   ).SingleOrDefault();
               return groupProfileTypr;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Profiles Group
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<AssignedProfileGroup> GetMutipleGroup(int profileGroupID)
      {
         ICollection<AssignedProfileGroup> objAssignedProfileGroup = null;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               objAssignedProfileGroup = (from pg in cloudEntities.ProfileGroup
                                          where pg.ProfileGroupID == profileGroupID
                                          join p in cloudEntities.Profile
                                             on pg.ProfileGroupTypeCode equals p.ProfileTypeCode
                                          select new AssignedProfileGroup
                                          {
                                             ProfileGroupID = pg.ProfileGroupID,
                                             City = p.City,
                                             State = p.State,
                                             Region = p.RegionCode,
                                             GroupProfileName = p.Name,
                                             GroupProfileCode = p.ProfileCode,
                                             Assigned = false
                                          }).OrderBy(pp => pp.GroupProfileName).ToList();
               return objAssignedProfileGroup;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Get Profiles Group
      /// </summary>
      /// <param name="profileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<AssignedProfileGroup> GetSelectGrouRegionProfileCode(int profileGroupID, string selectedGrp, string selectedRegion)
      {
         ICollection<AssignedProfileGroup> objAssignedProfileGroup = null;
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //objAssignedProfileGroup = (from grpprofile in cloudModelEntities.GroupProfiles
               //                           where grpprofile.ProfileGroupID == profileGroupID
               //                           join p in cloudModelEntities.Profiles
               //                              on grpprofile.ProfileCode equals p.ProfileCode
               //                           select new AssignedProfileGroup
               //                           {
               //                              ProfileGroupID = grpprofile.ProfileGroupID,
               //                              City = p.City,
               //                              State = p.State,
               //                              Region = p.RegionCode,
               //                              GroupProfileName = p.Name,
               //                              GroupProfileCode = grpprofile.ProfileCode,
               //                              Assigned = false
               //                           }).OrderBy(pp => pp.GroupProfileName).ToList();
               return objAssignedProfileGroup;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLGroup> GetPartialGroupProfileDetails(string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
                  buildquery = buildquery + Constants.Model_GroupDataAccess_OneEqualOne;
               }

               //var detailsresult = cloudModelEntities.USP_GetALLGroups(buildquery);

               //IEnumerable<MLGroup> groupList = (from grp in detailsresult
               //                                  select new MLGroup
               //                                  {
               //                                     IsAssigned = false,
               //                                     ProfileGroupID = grp.ProfileGroupID,
               //                                     GroupProfilesName = grp.Name,
               //                                     GroupName = grp.GroupName,
               //                                     Created = grp.Created
               //                                  }).OrderBy(pp => pp.GroupProfilesName).ToList();

               //if (string.IsNullOrEmpty(sortorder) == false)
               //{
               //   if (sortorder == Constants.Model_GroupDataAccess_Asc)
               //   {
               //      groupList = groupList.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //   }
               //   else
               //   {
               //      groupList = groupList.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //   }
               //}
               totalrecord = 0; //groupList.Count();
               //groupList = groupList.Skip(pagesize * pagenum).Take(pagesize);
               return new List<MLGroup>(); //groupList.ToList();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///  Get  Edit Group Profiles based on profileCode
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLAssignedGroupProfiles> GetGroupProfiles(string ProfileCode, string selectgrpID, string selectedRegionCode, long userID)
      {
         var selecteProfileCode = -1;

         try
         {
            if (!string.IsNullOrEmpty(ProfileCode))
            {
               selecteProfileCode = Convert.ToInt32(ProfileCode);
            }

            string[] GroupIDArray = null;

            if (!string.IsNullOrEmpty(selectgrpID))
            {
               GroupIDArray = selectgrpID.Split(',');
            }

            string[] RegionCodeArray = null;

            if (!string.IsNullOrEmpty(selectedRegionCode))
            {
               RegionCodeArray = selectedRegionCode.Split(',');
            }

            var allOrder = new List<MLAssignedGroupProfiles>();

            using (var cloudEntities = new QoskCloud())
            {
               //var grplist = (cloudModelEntities.Group.OrderByDescending(a => a.GroupName).Where(p => p.ProfileCode == selecteProfileCode));

               //if (!string.IsNullOrEmpty(ProfileCode) && GroupIDArray != null)
               //{
               //   grplist = grplist.Where(g => GroupIDArray.Contains(g.ProfileGroupID.ToString()));
               //}

               //foreach (var i in grplist)
               //{
               //   var od = (from l in cloudModelEntities.GroupProfiles
               //             join p in cloudModelEntities.Profiles
               //                on l.ProfileCode equals p.ProfileCode
               //             where l.ProfileGroupID == i.ProfileGroupID
               //             select new AssignedProfileGroup
               //             {
               //                ProfileGroupID = l.ProfileGroupID,
               //                City = p.City,
               //                State = p.State,
               //                Region = (string.IsNullOrEmpty(p.RegionCode)) ? Constants.Model_GroupDataAccess_NA : p.RegionCode,
               //                GroupProfileName = p.Name,
               //                GroupProfileCode = l.ProfileCode,
               //                Assigned = false
               //             }).OrderBy(pp => pp.GroupProfileName).ToList();
               //   if (userID != 0)
               //   {
               //      var userselectedProfile = (from sgp in cloudModelEntities.UserProfiles
               //                                 where sgp.UserID == userID
               //                                 select new AssignedProfileGroup
               //                                 {
               //                                    GroupProfileCode = sgp.ProfileCode,
               //                                    ProfileGroupID = sgp.ProfileGroupID,
               //                                    Assigned = true
               //                                 }).ToList();
               //      if (userselectedProfile != null)
               //      {
               //         od = (from sl in od
               //               join sp in userselectedProfile
               //                  on sl.GroupProfileCode equals sp.GroupProfileCode into temp
               //               from sptemp in temp.DefaultIfEmpty()
               //               select new AssignedProfileGroup
               //               {
               //                  ProfileGroupID = sl.ProfileGroupID,
               //                  City = sl.City,
               //                  State = sl.State,
               //                  Region = (string.IsNullOrEmpty(sl.Region)) ? Constants.Model_GroupDataAccess_NA : sl.Region,
               //                  GroupProfileName = sl.GroupProfileName,
               //                  GroupProfileCode = sl.GroupProfileCode,
               //                  Assigned = (sptemp == null) ? false : true
               //               }).OrderBy(pp => pp.GroupProfileName).ToList();
               //      }
               //   }

               //   if (!string.IsNullOrEmpty(selectedRegionCode))
               //   {
               //      od = od.Where(g => RegionCodeArray.Contains(g.Region.ToString())).ToList();
               //   }
               //   allOrder.Add(new MLAssignedGroupProfiles {group = i, groupDetails = od});
               //}
            }
            return allOrder;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      /// ValiadateProfileGroupName method
      /// </summary>
      /// <param name="profile"></param>
      /// <param name="groupName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string ValiadateProfileGroupName(string profile, string groupName)
      {
         var IsgroupExist = string.Empty;

         try
         {
            if (!string.IsNullOrEmpty(groupName))
            {
               var profileCode = Convert.ToInt32(profile);

               using (var cloudEntities = new QoskCloud())
               {
                  //IsgroupExist = (from grp in cloudModelEntities.Group
                  //                where grp.ProfileCode == profileCode && grp.GroupName.Equals(groupName)
                  //                select grp.GroupName
                  //   ).SingleOrDefault();
               }
            }

            return IsgroupExist;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///  Get Group Profiles based on profileCode
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<MLAssignedGroupProfilestest> GetGroupProfileNested(string ProfileCode, string selectgrpID, string selectedRegionCode, long userID)
      {
         var selecteProfileCode = -1;

         try
         {
            if (!string.IsNullOrEmpty(ProfileCode))
            {
               selecteProfileCode = Convert.ToInt32(ProfileCode);
            }

            string[] GroupIDArray = null;

            if (!string.IsNullOrEmpty(selectgrpID))
            {
               GroupIDArray = selectgrpID.Split(',');
            }

            string[] RegionCodeArray = null;

            if (!string.IsNullOrEmpty(selectedRegionCode))
            {
               RegionCodeArray = selectedRegionCode.Split(',');
            }

            var allOrder = new List<MLAssignedGroupProfilestest>();

            using (var cloudEntities = new QoskCloud())
            {
            //   List<MLGroup> grplist = (from g in cloudModelEntities.Group
            //                            join p in cloudModelEntities.Profiles on g.ProfileCode equals p.ProfileCode
            //                            where g.ProfileCode == selecteProfileCode
            //                            orderby g.GroupName
            //                            select new MLGroup
            //                            {
            //                               ProfileGroupID = g.ProfileGroupID,
            //                               GroupName = g.GroupName,
            //                               ProfileCode = g.ProfileCode,
            //                               ProfileName = p.Name,
            //                               GroupType = g.GroupType
            //                            }).ToList();

            //   if (!string.IsNullOrEmpty(ProfileCode) && GroupIDArray != null)
            //   {
            //      grplist = (from m in grplist
            //                 where GroupIDArray.Contains(m.ProfileGroupID.ToString())
            //                 select m).ToList();
            //   }
            //   foreach (var i in grplist)
            //   {
            //      var od = (from l in cloudModelEntities.GroupProfiles
            //                join p in cloudModelEntities.Profiles
            //                   on l.ProfileCode equals p.ProfileCode
            //                where l.ProfileGroupID == i.ProfileGroupID
            //                select new AssignedProfileGroup
            //                {
            //                   ProfileGroupID = l.ProfileGroupID,
            //                   City = p.City,
            //                   State = p.State,
            //                   Region = (string.IsNullOrEmpty(p.RegionCode)) ? Constants.Model_GroupDataAccess_NA : p.RegionCode,
            //                   GroupProfileName = p.Name,
            //                   GroupProfileCode = l.ProfileCode,
            //                   Assigned = false
            //                }).OrderBy(pp => pp.GroupProfileName).ToList();
            //      if (userID != 0)
            //      {
            //         var userselectedProfile = (from sgp in cloudModelEntities.UserProfiles
            //                                    where sgp.UserID == userID
            //                                    select new AssignedProfileGroup
            //                                    {
            //                                       GroupProfileCode = sgp.ProfileCode,
            //                                       ProfileGroupID = sgp.ProfileGroupID,
            //                                       Assigned = true
            //                                    }).ToList();
            //         if (userselectedProfile != null)
            //         {
            //            od = (from sl in od
            //                  join sp in userselectedProfile
            //                     on sl.GroupProfileCode equals sp.GroupProfileCode into temp
            //                  from sptemp in temp.DefaultIfEmpty()
            //                  select new AssignedProfileGroup
            //                  {
            //                     ProfileGroupID = sl.ProfileGroupID,
            //                     City = sl.City,
            //                     State = sl.State,
            //                     Region = (string.IsNullOrEmpty(sl.Region)) ?
            //                        Constants.Model_GroupDataAccess_NA : sl.Region,
            //                     GroupProfileName = sl.GroupProfileName,
            //                     GroupProfileCode = sl.GroupProfileCode,
            //                     Assigned = (sptemp == null) ? false : true
            //                  }).OrderBy(pp => pp.GroupProfileName).ToList();
            //         }
            //      }

            //      if (!string.IsNullOrEmpty(selectedRegionCode))
            //      {
            //         od = od.Where(g => RegionCodeArray.Contains(g.Region.ToString())).ToList();
            //      }

            //      MLAssignedGroupProfilesData groupData = new MLAssignedGroupProfilesData();
            //      groupData.groupDetails = od;
            //      allOrder.Add(new MLAssignedGroupProfilestest
            //      {
            //         ProfileGroupID = i.ProfileGroupID,
            //         GroupName = i.GroupName,
            //         ProfileCode = i.ProfileCode,
            //         ProfileName = i.ProfileName,
            //         GroupType = i.GroupType,
            //         groupDetail = groupData
            //      });
            //   }
            }

            return allOrder;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="ProfileCode"></param>
      /// <param name="selectgrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<AssignedProfileGroup> GetUserGroupProfilesData(string ProfileCode, string selectgrpID, string selectedRegionCode, long userID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         try
         {
            string[] RegionCodeArray = null;

            if (!string.IsNullOrEmpty(selectedRegionCode))
            {
               RegionCodeArray = selectedRegionCode.Split(',');
            }

            using (var cloudEntities = new QoskCloud())
            {
               var pgrpIDS = string.Empty;

               if (string.IsNullOrEmpty(buildquery))
               {
                  buildquery = Constants.Model_GroupDataAccess_Where;
               }
               else
               {
                  buildquery += Constants.PolicyController_ANDWITHSPACE;
               }

               var pcode = string.Format(Constants.Model_GroupDataAccess_GProfileCode, ProfileCode);

               if (!string.IsNullOrEmpty(selectgrpID))
               {
                  pgrpIDS = string.Format(Constants.Model_GroupDataAccess_AndGProfileCode, selectgrpID);
               }

               buildquery += pcode + pgrpIDS;

               //var result = cloudModelEntities.USP_UserGroupSearch(buildquery);
               //if (result != null)
               //{
               //   IEnumerable<AssignedProfileGroup> allgroup = (from g in result

               //                                                 select new AssignedProfileGroup
               //                                                 {
               //                                                    ProfileGroupID = g.ProfileGroupID,
               //                                                    GroupName = g.GroupName,
               //                                                    GroupProfileCode = g.ProfileCode,
               //                                                    GroupProfileName = g.ProfileName,
               //                                                    GroupType = g.GroupType,
               //                                                    Region = g.RegionCode,
               //                                                    Assigned = false

               //                                                 }).ToList();
               //   if (!string.IsNullOrEmpty(selectedRegionCode))
               //   {
               //      allgroup = (from i in allgroup
               //                  where RegionCodeArray.Contains(i.Region)
               //                  select i).ToList();
               //   }

               //   if (allgroup != null)
               //   {

               //      if (userID != 0)
               //      {
               //         var userselectedProfile = (from sgp in cloudModelEntities.UserProfiles
               //                                    where sgp.UserID == userID
               //                                    select new AssignedProfileGroup
               //                                    {
               //                                       GroupProfileCode = sgp.ProfileCode,
               //                                       ProfileGroupID = sgp.ProfileGroupID,
               //                                       Assigned = true
               //                                    }).ToList();
               //         if (userselectedProfile != null)
               //         {
               //            allgroup = (from sl in allgroup
               //                        join sp in userselectedProfile
               //                           on sl.GroupProfileCode equals sp.GroupProfileCode into temp
               //                        from sptemp in temp.DefaultIfEmpty()
               //                        select new AssignedProfileGroup
               //                        {
               //                           ProfileGroupID = sl.ProfileGroupID,
               //                           GroupName = sl.GroupName,
               //                           GroupType = sl.GroupType,
               //                           Region = (string.IsNullOrEmpty(sl.Region)) ? Constants.Model_GroupDataAccess_NA : sl.Region,
               //                           GroupProfileName = sl.GroupProfileName,
               //                           GroupProfileCode = sl.GroupProfileCode,
               //                           Assigned = (sptemp == null) ? false : true
               //                        }).ToList();
               //         }
               //      }

               //      if (!string.IsNullOrEmpty(sortorder))
               //      {
               //         if (sortorder == Constants.Model_GroupDataAccess_Asc)
               //         {
               //            allgroup = allgroup.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //         }
               //         else
               //         {
               //            allgroup = allgroup.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //         }
               //      }
               //      totalrecord = allgroup.Count();
               //      allgroup = allgroup.Skip(pagesize * pagenum).Take(pagesize);

               //      return allgroup.ToList();
               //   }
               //   totalrecord = 0;
               //   var emptydata = new List<AssignedProfileGroup>();
               //   return emptydata;
               //}
               //else
               //{
                  totalrecord = 0;
                  var emptydata = new List<AssignedProfileGroup>();
                  return emptydata;
               //}
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///  Get UserGroupProfileDetails based on profileCode and groupID
      /// </summary>
      /// <param name="profileCode"></param>
      /// <param name="selectgrpID"></param>
      /// <param name="selectedRegionCode"></param>
      /// <param name="userID"></param>
      /// <param name="sortdatafield"></param>
      /// <param name="sortorder"></param>
      /// <param name="pagesize"></param>
      /// <param name="pagenum"></param>
      /// <param name="buildquery"></param>
      /// <param name="totalrecord"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<AssignedProfileGroup> UserGroupProfileDetails(int profileCode, int selectgrpID, string selectedRegionCode, long userID, string sortdatafield, string sortorder, int pagesize, int pagenum, string buildquery, out int totalrecord)
      {
         var allOrder = new List<AssignedProfileGroup>();

         try
         {
            string[] RegionCodeArray = null;
            string pgrpIDS = string.Empty;

            if (!string.IsNullOrEmpty(selectedRegionCode))
            {
               RegionCodeArray = selectedRegionCode.Split(',');
            }

            using (var cloudEntities = new QoskCloud())
            {
               //if (string.IsNullOrEmpty(buildquery))
               //{
               //   buildquery = Constants.Model_GroupDataAccess_Where;
               //}
               //else
               //{
               //   buildquery += Constants.PolicyController_ANDWITHSPACE;
               //}
               //string pcode = string.Format(Constants.Model_GroupDataAccess_GroupProfileCode, profileCode);
               //if (selectgrpID > 0)
               //{
               //   pgrpIDS = string.Format(Constants.Model_GroupDataAccess_AndGroupProfileCode, selectgrpID);
               //}

               //buildquery += pcode + pgrpIDS;
               //var detailsresult = cloudModelEntities.USP_UserGroupProfilesSearch(buildquery);

               //if (detailsresult != null)
               //{
               //   IEnumerable<AssignedProfileGroup> grpDetails = (from g in detailsresult
               //                                                   select new AssignedProfileGroup
               //                                                   {
               //                                                      ProfileGroupID = g.ProfileGroupID,
               //                                                      GroupName = g.GroupName,
               //                                                      GroupProfileCode = g.ProfileCode,
               //                                                      PProfileCode = g.PProfileCode,
               //                                                      GroupProfileName = g.ProfileName,
               //                                                      City = g.City,
               //                                                      State = g.State,
               //                                                      GroupType = g.GroupType,
               //                                                      Assigned = false,
               //                                                      Region = (string.IsNullOrEmpty(g.RegionCode)) ? Constants.Model_GroupDataAccess_NA : g.RegionCode
               //                                                   }).Distinct().ToList();

               //   if (!string.IsNullOrEmpty(selectedRegionCode))
               //   {
               //      grpDetails = (from i in grpDetails
               //                    where RegionCodeArray.Contains(i.Region)
               //                    select i).ToList();
               //   }
               //   if (grpDetails != null)
               //   {
               //      if (userID != 0)
               //      {
               //         var userselectedProfile = (from sgp in cloudModelEntities.UserProfiles
               //                                    where sgp.UserID == userID && sgp.ProfileGroupID == selectgrpID
               //                                    select new AssignedProfileGroup
               //                                    {
               //                                       GroupProfileCode = sgp.ProfileCode,
               //                                       ProfileGroupID = sgp.ProfileGroupID,
               //                                       Assigned = true
               //                                    }).ToList();
               //         if (userselectedProfile != null)
               //         {
               //            grpDetails = (from sl in grpDetails
               //                          join sp in userselectedProfile
               //                             on sl.PProfileCode equals sp.GroupProfileCode into temp
               //                          from sptemp in temp.DefaultIfEmpty()
               //                          select new AssignedProfileGroup
               //                          {
               //                             ProfileGroupID = sl.ProfileGroupID,
               //                             City = sl.City,
               //                             State = sl.State,
               //                             Region = (string.IsNullOrEmpty(sl.Region)) ? Constants.Model_GroupDataAccess_NA : sl.Region,
               //                             GroupProfileName = sl.GroupProfileName,
               //                             GroupProfileCode = sl.GroupProfileCode,
               //                             PProfileCode = sl.PProfileCode,
               //                             Assigned = (sptemp == null) ? false : (sptemp.ProfileGroupID == sl.ProfileGroupID) ? true : false
               //                          }).OrderBy(pp => pp.GroupProfileName).ToList();
               //         }
               //      }

               //      if (!string.IsNullOrEmpty(selectedRegionCode))
               //      {
               //         grpDetails = grpDetails.Where(g => RegionCodeArray.Contains(g.Region.ToString())).ToList();
               //      }

               //      if (!string.IsNullOrEmpty(sortorder))
               //      {
               //         if (sortorder == Constants.Model_GroupDataAccess_Asc)
               //         {
               //            grpDetails = grpDetails.OrderBy(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //         }
               //         else
               //         {
               //            grpDetails = grpDetails.OrderByDescending(o => o.GetType().GetProperty(sortdatafield).GetValue(o, null));
               //         }
               //      }
               //      totalrecord = grpDetails.Count();
               //      grpDetails = grpDetails.Skip(pagesize * pagenum).Take(pagesize);
               //      allOrder = grpDetails.ToList();
               //      return allOrder;
               //   }
               //   else
               //   {
               //      totalrecord = 0;
               //      var emptydata = new List<AssignedProfileGroup>();
               //      return emptydata;
               //   }
               //}
               //else
               //{
                  totalrecord = 0;
                  var emptydata = new List<AssignedProfileGroup>();
                  return emptydata;
               //}
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

   }
}