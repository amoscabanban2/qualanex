﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="UserAccessibility.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class UserAccessbilityHelper
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objProfileSeach">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public List<int> GetUserProfilesList(ProfileSeach objProfileSeach)
      {
         ParameterValidation.Begin().IsNotNull(() => objProfileSeach);

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (objProfileSeach.UserTypeCode)
               {
                  // For Site user get list of profiles
                  //=============================================================================
                  case "SITE":
                     return (from usrprofile in cloudEntities.UserProfiles
                             where usrprofile.UserID == objProfileSeach.UserID
                             select usrprofile.ProfileCode).ToList();

                  // For Corporate user get list of profiles
                  //=============================================================================
                  case "CORP":
                     return (from profile in cloudEntities.Profile
                             where profile.RollupProfileCode == objProfileSeach.RollupProfileCode
                             select profile.ProfileCode).ToList();

                  // For Regional user get list of profiles
                  //=============================================================================
                  case "REGN":
                     return (from usrprofile in cloudEntities.UserProfiles
                             where usrprofile.UserID == objProfileSeach.UserID
                             select usrprofile.ProfileCode).ToList();
               }

               return null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserData">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public List<int> GetUserProductList(UserSearch objUserData)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (objUserData.UserTypeCode)
               {
                  // For Site user get list of profiles
                  //=============================================================================
                  case "SITE":
                     return (from product in cloudEntities.Product
                             join usrprofile in cloudEntities.UserProfiles on product.ProfileCode equals usrprofile.ProfileCode
                             where usrprofile.UserID == objUserData.UserID
                             select product.ProductID).ToList();

                  // For Corporate user get list of profiles
                  //=============================================================================
                  case "CORP":
                     return (from product in cloudEntities.Product
                             join profile in cloudEntities.Profile on product.ProfileCode equals profile.ProfileCode
                             where profile.RollupProfileCode == objUserData.RollupProfileCode
                             select product.ProductID).ToList();

                  // For Regional user get list of profiles
                  //=============================================================================
                  case "REGN":
                     return (from product in cloudEntities.Product
                             join usrprofile in cloudEntities.UserProfiles on product.ProfileCode equals usrprofile.ProfileCode
                             where usrprofile.UserID == objUserData.UserID
                             select product.ProductID).ToList();
               }
            }

            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserData">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public List<long> GetUserList(UserSearch objUserData)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (objUserData.UserTypeCode)
               {
                  // For Site user get list of profiles
                  //=============================================================================
                  case "SITE":
                  //return (from user in cloudEntities.User
                  //        join usrprofile in cloudEntities.UserProfiles on user.UserID equals usrprofile.UserID
                  //        where usrprofile.UserID == objUserData.UserID
                  //        select usrprofile.UserID).Distinct().ToList();
                  return new List<long> {objUserData.UserID}; // TODO: Find out what the behavior of a site user should be

                  // For Corporate user get list of profiles
                  //=============================================================================
                  case "CORP":
                  var childprofileCode = (from userprl in cloudEntities.Profile
                                          where userprl.RollupProfileCode == objUserData.RollupProfileCode
                                          select userprl.ProfileCode.ToString()).ToArray();

                  return (from usr in cloudEntities.User
                          where childprofileCode.Contains(usr.ProfileCode.ToString())
                          select usr.UserID).Distinct().ToList();

                  // For Regional user get list of profiles
                  //=============================================================================
                  case "REGN":
                     var profileCode = (from user in cloudEntities.User
                                        join usrprofile in cloudEntities.UserProfiles on user.UserID equals usrprofile.UserID
                                        where usrprofile.UserID == objUserData.UserID
                                        select usrprofile.ProfileCode).ToArray();

                     return (from user in cloudEntities.UserProfiles
                             where profileCode.Contains(user.ProfileCode)
                             select user.UserID).Distinct().ToList();

                  case "ADMN":
                     return (from user in cloudEntities.User
                             select user.UserID).Distinct().ToList();
               }
            }

            return null;
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="objUserData">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public List<int> GetUserLotNumbersList(LotNumberSearch objUserData)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               switch (objUserData.UserTypeCode)
               {
                  // For Site user get list of profiles
                  //=============================================================================
                  case "SITE":
                     return (from lot in cloudEntities.Lot
                             join prd in cloudEntities.Product on lot.ProductID equals prd.ProductID
                             join usrprofile in cloudEntities.UserProfiles on prd.ProfileCode equals usrprofile.ProfileCode
                             where usrprofile.UserID == objUserData.UserID
                             select prd.ProfileCode).ToList();

                  // For Corporate user get list of profiles
                  //=============================================================================
                  case "CORP":
                     return (from lot in cloudEntities.Lot
                             join prd in cloudEntities.Product on lot.ProductID equals prd.ProductID
                             join profile in cloudEntities.Profile on prd.ProfileCode equals profile.ProfileCode
                             where profile.RollupProfileCode == objUserData.RollupProfileCode
                             select prd.ProfileCode).ToList();

                  // For Regional user get list of profiles
                  //=============================================================================
                  case "REGN":
                     return (from lot in cloudEntities.Lot
                             join prd in cloudEntities.Product on lot.ProductID equals prd.ProductID
                             join usrprofile in cloudEntities.UserProfiles on prd.ProfileCode equals usrprofile.ProfileCode
                             where usrprofile.UserID == objUserData.UserID
                             select prd.ProfileCode).ToList();
               }

               return null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

   }
}