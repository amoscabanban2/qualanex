///-----------------------------------------------------------------------------------------------
/// <copyright file="TrackingModel.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Model.TrackingModel
{
   using System;
   using System.Data.SqlClient;
   using System.Linq;
   using System.Threading;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using static Qualanex.Qosk.Library.LogTraceManager.LogTraceManager;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class TrackingModel
   {
      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public TrackingModel()
      {
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~TrackingModel()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {
                  try
                  {
                     ;
                     ; // TODO: dispose managed state (managed objects).
                     ;
                  }
                  catch (Exception ex)
                  {
                     Log.Write(LogMask.Error, ex.ToString());
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }

               ;
               ; // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
               ; // TODO: set large fields to null.
               ;

            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Update/Modify the series of Tracking records associated with a specific
      ///   Carrier and Date.
      /// </summary>
      /// <param name="oldTrckNo"></param>
      /// <param name="newTrckNo"></param>
      /// <param name="carrierCode"></param>
      /// <param name="rcvdDate"></param>
      /// <param name="queryMode"></param>
      /// <returns>
      ///   The number of records Updated or -1 upon failure.
      /// </returns>
      ///****************************************************************************
      public static int UpdateTrackingNumber(string oldTrckNo, string newTrckNo, string carrierCode, DateTime rcvdDate, QueryMode queryMode = QueryMode.Synchronous)
      {
         var result = 0;

         try
         {
            if (oldTrckNo != newTrckNo)
            {
               const string sqlQuery = "UPDATE Tracking " +
                                        "SET TrackingNumber=@newTrckNo " +
                                         "WHERE TrackingNumber=@oldTrckNo AND CarrierCode=@carrierCode AND CAST(RcvdDate as date)=@rcvdDate AND IsDeleted = 0 " +
                                          "SELECT @@ROWCOUNT";
               switch (queryMode)
               {
                  // ...
                  //=============================================================================
                  case QueryMode.Asynchronous:
                     {
                        new Thread(() =>
                        {
                           var _oldTrckNo = new SqlParameter("@newTrckNo", newTrckNo);
                           var _newTrckNo = new SqlParameter("@oldTrckNo", oldTrckNo);
                           var _carrierCode = new SqlParameter("@carrierCode", carrierCode);
                           var _rcvdDate = new SqlParameter("@rcvdDate", rcvdDate.Date);

                           try
                           {
                              using (var cloudEntities = new QoskCloud())
                              {
                                 result = cloudEntities.Database.SqlQuery<int>(sqlQuery,
                                    _newTrckNo,
                                    _oldTrckNo,
                                    _carrierCode,
                                    _rcvdDate).FirstOrDefault();
                              }
                           }
                           catch (Exception ex)
                           {
                              Log.WriteError(ex);
                           }
                        }).Start();

                        break;
                     }

                  // ...
                  //=============================================================================
                  case QueryMode.Synchronous:
                     {
                        using (var cloudEntities = new QoskCloud())
                        {
                           result = cloudEntities.Database.SqlQuery<int>(sqlQuery,
                                                                          new SqlParameter("@newTrckNo", newTrckNo),
                                                                           new SqlParameter("@oldTrckNo", oldTrckNo),
                                                                            new SqlParameter("@carrierCode", carrierCode),
                                                                             new SqlParameter("@rcvdDate", rcvdDate.Date)).FirstOrDefault();
                        }

                        break;
                     }
               }
            }
            else
            {
               result = -2;
            }
         }
         catch (Exception ex)
         {
            Log.WriteError(ex);
            return -1;
         }

         return result;
      }

      #endregion
   }
}
