﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="Extensions.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Model
{
   using System;
   using System.Globalization;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public static class Extensions
   {
      static GregorianCalendar _gc = new GregorianCalendar();

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="time">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      public static int GetWeekOfMonth(this DateTime time)
      {
         var first = new DateTime(time.Year, time.Month, 1);
         return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="time">
      ///   Description not available.
      /// </param>
      /// <returns>
      ///   Description not available.
      /// </returns>
      ///****************************************************************************
      static int GetWeekOfYear(this DateTime time)
      {
         return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
      }

   }
}