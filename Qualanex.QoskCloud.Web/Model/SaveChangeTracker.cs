﻿
namespace Qualanex.QoskCloud.Web.Model
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Linq.Expressions;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   using Qualanex.Qosk.Library.Model.DBModel;

   public class SaveChangeTracker
   {
      readonly Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud _context;

      private List<ChangeTrackerEntity> _lstChangeTrackHistory;

      readonly string _identityRefId = string.Empty;

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///
      /// </summary>
      /// <param name="_DBContext"></param>
      /// <param name="_IdentityRefId"></param>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public SaveChangeTracker(Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud _DBContext, string _IdentityRefId)
      {
         this._context = _DBContext;
         this._identityRefId = _IdentityRefId;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="item"></param>
      /// <param name="changedPropertyNames"></param>
      /// <returns></returns>
      ///****************************************************************************
      public List<ChangeTrackerEntity> Update<T>(T item, params string[] changedPropertyNames) where T : class, new()
      {
         var param = Expression.Parameter(typeof(T), typeof(T).Name);
         var prop = typeof(T).GetProperties().ToList();

         foreach (var propertyName in prop)
         {
            // If we can't find the property, this line will throw an exception,
            try
            {
               if (this._context.Entry(item).Property(propertyName.Name).IsModified)
               {
                  try
                  {
                     if (this._lstChangeTrackHistory == null) this._lstChangeTrackHistory = new List<ChangeTrackerEntity>();

                     this._lstChangeTrackHistory.Add(new ChangeTrackerEntity
                                                      {
                                                         PrimaryColumnId = this._identityRefId,
                                                         TableName = param.Name,
                                                         ColumnName = propertyName.Name,
                                                         OldValue = Convert.ToString(this._context.Entry(item).Property(propertyName.Name).OriginalValue),
                                                         NewValue = Convert.ToString(this._context.Entry(item).Property(propertyName.Name).CurrentValue),
                                                         Version = Convert.ToInt64(this._context.Entry(item).Property(Constants.Model_SaveChangeTracker_Version).CurrentValue),
                                                         ModifiedBy = Convert.ToString(this._context.Entry(item).Property(Constants.Model_SaveChangeTracker_ModifiedBy).CurrentValue),
                                                         CurrentDateTime = DateTime.UtcNow,
                                                         OperationType = Constants.Model_SaveChangeTracker_Update,
                                                      });
                  }
                  catch (Exception ex)
                  {
                     Logger.Error(ex.Message);
                  }
               }
            }
            catch (Exception)
            {

            }

         }
         return this._lstChangeTrackHistory;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="cloudModelEntities"></param>
      /// <param name="objlstChange"></param>
      ///****************************************************************************
      public void AddChangeTrackingToDB(List<ChangeTrackerEntity> objlstChange)
      {
         if (objlstChange != null)
         {
            foreach (var objContent in objlstChange)
            {
               //cloudModelEntities.ChangeTracker.Add(new ChangeTracker
               //{
               //   PrimayColumnId = objContent.PrimaryColumnId,
               //   TableName = objContent.TableName,
               //   ColumnName = objContent.ColumnName,
               //   OldValue = objContent.OldValue,
               //   NewValue = objContent.NewValue,
               //   Version = objContent.Version,
               //   ModifyBy = objContent.ModifiedBy,
               //   ModifyDateTime = objContent.CurrentDateTime,
               //   OperationType = objContent.OperationType
               //});
            }
         }
      }

   }
}