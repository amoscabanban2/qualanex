﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="QoskDataAccess.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web
{
   using System;
   using System.Collections.Generic;
   using System.Data.Entity.Validation;
   using System.Linq;

   using Qualanex.Qosk.Library.Common.CodeCorrectness;
   using Qualanex.Qosk.Library.Model.DBModel;
   using Qualanex.Qosk.Library.Model.QoskCloud;

   using Qualanex.QoskCloud.Entity;
   using Qualanex.QoskCloud.Utility;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public class QoskDataAccess
   {
      ///****************************************************************************
      /// <summary>
      ///   RegisterQosk method used to Add Qosk to database.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response RegisterQosk(QoskRequest objQoskRequest)
      {
         ParameterValidation.Begin().IsNotNull(() => objQoskRequest);

         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //Qualanex.QoskCloud.DBModel.Qosk qosk = new Qualanex.QoskCloud.DBModel.Qosk();
               //qosk.QoskID = objQoskRequest.QoskID;
               //qosk.MachineName = objQoskRequest.MachineName;
               //qosk.Active = objQoskRequest.Active;
               //qosk.ProfileCode = objQoskRequest.ProfileCode;
               //qosk.CustomerSupportNumber = objQoskRequest.CustomerSupportNumber;
               //qosk.CreatedBy = objQoskRequest.CreatedBy;
               //qosk.CreatedDate = DateTime.UtcNow;
               //qosk.Password = objQoskRequest.Password;
               //qosk.State = objQoskRequest.State;
               //cloudModelEntities.Qosk.Add(qosk);

               //if (cloudEntities.SaveChanges() > 0)
               //{
               //   objclsResponse.Status = Status.OK;
               //}
               //else
               //{
               //   objclsResponse.Status = Status.Fail;
               //}
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   UpdateQosk methods used to Update Qosk.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static Response UpdateQosk(QoskRequest objQoskRequest)
      {
         var objclsResponse = new Response();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var qosk = cloudEntities.Qosk.Single(p => p.QoskID == objQoskRequest.QoskID);
               //qosk.MachineName = objQoskRequest.MachineName;
               //qosk.Active = objQoskRequest.Active;
               //qosk.ProfileCode = objQoskRequest.ProfileCode;
               //qosk.CustomerSupportNumber = objQoskRequest.CustomerSupportNumber;
               //qosk.ModifiedBy = objQoskRequest.ModifiedBy;
               //qosk.ModifiedDate = DateTime.UtcNow;

               //int response = cloudEntities.SaveChanges();
               //if (response > 0)
               //{
               //   objclsResponse.Status = Status.OK;
               //}
               //else
               //{
               //   objclsResponse.Status = Status.Fail;
               //}
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return objclsResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///   GetQoskDetails method used to Get Details.
      /// </summary>
      /// <param name="objQoskRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static List<QoskRequest> GetQoskDetails(QoskRequest objQoskRequest)
      {
         var lstQoskRequest = new List<QoskRequest>();

         try
         {
            if (objQoskRequest.Active || objQoskRequest.ProfileCode > 0 || !string.IsNullOrEmpty(objQoskRequest.QoskID) || !string.IsNullOrEmpty(objQoskRequest.MachineName) || !string.IsNullOrEmpty(objQoskRequest.State))
            {
               using (var cloudEntities = new QoskCloud())
               {
                  //var result = (from p in cloudEntities.Qosk
                  //              join prf in cloudModelEntities.Profiles on p.ProfileCode equals prf.ProfileCode
                  //              select new QoskRequest
                  //              {
                  //                 QoskID = p.QoskID,
                  //                 MachineName = p.MachineName,
                  //                 Active = p.Active ?? false,
                  //                 ProfileCode = p.ProfileCode,
                  //                 CustomerSupportNumber = p.CustomerSupportNumber,
                  //                 Password = p.Password,
                  //                 State = p.State,
                  //                 PharmacyName = prf.Name
                  //              });
                  //if ((objQoskRequest.ProfileCode > 0))
                  //{
                  //   result = result.Where(p => p.ProfileCode == objQoskRequest.ProfileCode);
                  //}
                  //if (!string.IsNullOrEmpty(objQoskRequest.QoskID))
                  //{
                  //   result = result.Where(p => p.QoskID.Equals(objQoskRequest.QoskID));
                  //}
                  //if (!string.IsNullOrEmpty(objQoskRequest.MachineName))
                  //{
                  //   result = result.Where(p => p.MachineName.Equals(objQoskRequest.MachineName));
                  //}
                  //if (!string.IsNullOrEmpty(objQoskRequest.State) && !objQoskRequest.State.Contains(Constants.ReportController_Select))
                  //{
                  //   result = result.Where(p => p.State.Equals(objQoskRequest.State));
                  //}
                  //result = result.Where(p => p.Active == objQoskRequest.Active);

                  //lstQoskRequest = result.ToList();
               }
            }
            else
            {
               using (var cloudEntities = new QoskCloud())
               {
                  //lstQoskRequest = (from p in cloudEntities.Qosk
                  //                  select new QoskRequest
                  //                  {
                  //                     QoskID = p.QoskID,
                  //                     MachineName = p.MachineName,
                  //                     Active = p.Active ?? false,
                  //                     ProfileCode = p.ProfileCode,
                  //                     CustomerSupportNumber = p.CustomerSupportNumber,
                  //                     Password = p.Password,
                  //                     State = p.State,
                  //                  }).ToList();
               }
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return lstQoskRequest;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="QoskID"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static QoskRequest GetQoskDetailsByID(string QoskID)
      {
         var lstQoskRequest = new QoskRequest();

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //lstQoskRequest = (from p in cloudEntities.Qosk
               //                  join prf in cloudEntities.Profile on p.ProfileCode equals prf.ProfileCode
               //                  where p.QoskID.Equals(QoskID)
               //                  select new QoskRequest
               //                  {
               //                     QoskID = p.QoskID,
               //                     MachineName = p.MachineName,
               //                     Active = p.Active ?? false,
               //                     ProfileCode = p.ProfileCode,
               //                     CustomerSupportNumber = p.CustomerSupportNumber,
               //                     Password = p.Password,
               //                     State = p.State,
               //                     Profile = prf.Name
               //                  }).FirstOrDefault();
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return lstQoskRequest;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="terms"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static ICollection<ProfilesName> GetPharmacyProfilesUsingAutoComplete(string terms)
      {
         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //return (from P1 in cloudEntities.Profile.Where(p => (p.Type.Equals(Constants.Model_PolicyDataAccess_ChainPharmacy) || p.Type.Equals(Constants.Model_PolicyDataAccess_RetailPharmacy) || p.Type.Equals(Constants.Model_PolicyDataAccess_HospitalClinic)) && p.Name.Contains(terms))
               //                  select new ProfilesName
               //                  {
               //                     ProfileCode = P1.ProfileCode,
               //                     Name = P1.Name,
               //                  }).Distinct().Take(20).ToList();
               return null;
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="State"></param>
      /// <param name="_stateKey"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetQoskID(string State, string _stateKey)
      {
         var KioskID = string.Empty;

         try
         {
            using (var cloudEntities = new QoskCloud())
            {
               //var Lastkiosk = cloudEntities.Qosk.Where(p => p.State.Equals(State)).OrderByDescending(p => p.CreatedDate);

               //if (Lastkiosk.Count() > 0)
               //{
               //   KioskID = Lastkiosk.FirstOrDefault().QoskID;
               //}

               //if (!string.IsNullOrEmpty(KioskID))
               //{
               //   KioskID = KioskID.Remove(0, 2);
               //   int KioskN = Convert.ToInt32(KioskID);
               //   KioskN = KioskN + 1;
               //   KioskID = _stateKey + KioskN.ToString();
               //   return KioskID;
               //}
               //else if (string.IsNullOrEmpty(KioskID))
               //{
               //   KioskID = _stateKey + Constants.Model_QoskDataAccess_Number;
               //   return KioskID;
               //}
            }
         }
         catch (DbEntityValidationException dbEx)
         {
            Logger.DBError(dbEx);
            throw;
         }
         catch (Exception ex)
         {
            Logger.Error(ex.Message);
            throw;
         }

         return KioskID;
      }

   }
}