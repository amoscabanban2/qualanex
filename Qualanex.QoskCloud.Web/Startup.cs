﻿using Microsoft.Owin;
using Owin;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Web.Model;
using System.Collections.Generic;

[assembly: OwinStartupAttribute(typeof(Qualanex.QoskCloud.Web.Startup))]

namespace Qualanex.QoskCloud.Web
{
   public partial class Startup
   {
      public void Configuration(IAppBuilder app)
      {
         this.ConfigureAuth(app);
         this.InitialiseAppConfigurationSetting();
      }

      private void InitialiseAppConfigurationSetting()
      {
         AppSettingInformation.lstQoskCloudAppInfo = CommonDataAccessLayer.QoskCloudConfigurationSetting(Constants.Startup_QoskCloud);
      }

   }
}
