﻿/// <reference path="jquery.validate.js" />
$(document).ready(function () {
    CreateSearchPanel();
    CreateGridColumnPanel();
});

var ConvertDate = function (row, columnfield, value, defaulthtml, columnproperties) {

    if (value != '') {
        var dateString = value.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var date = month + "/" + day + "/" + year;

        return date;
    }
    else { return '' }
}


function CreateSearchPanel() {

    var Url1 = '../LotNumber/CreateSearchPanel';
    $.ajax({
        url: '../LotNumber/CreateSearchPanel',
        data: {},
        success: function (data) {
            $("#SearchPanelList").html(data)

            ///Checking if Product List is already been searched by , then start assigning the value

            var rpreviouspage = document.referrer;
            if (sessionStorage.getItem("LotNumbersSearchField") != null && rpreviouspage.indexOf('Edit') != -1) {

                var objSearchFieldValue = sessionStorage.getItem("LotNumbersSearchField");
                SetSearchFieldValue(JSON.parse(objSearchFieldValue));
                ///Binding the Grid based on Previous search
                //CreateGridPanel();
                GetColumnAndBindintoGrid();
            }

            LoadColumnListingInPopup();
        },
        error: function (response) {
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}

///function to start assigning search value to control
function SetSearchFieldValue(objSearchFieldValue) {

    for (var i = 0; i < objSearchFieldValue.length; i++) {
        var SearchFieldObject = objSearchFieldValue[i];
        var controlname = SearchFieldObject.Type + '-' + SearchFieldObject.Name;
        document.getElementsByName(controlname)[0].value = SearchFieldObject.Value1;
    }

}

function LoadColumnListingInPopup() {

    $.ajax({
        url: '../LotNumber/CreateColumnListingPopUp',
        data: {},
        success: function (data) {
            $("#ColumnListingPopUp").html(data)
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
};

function GetLeftPanelList() {
    //selectAllList();
    var optionValues = [];

    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });

    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: '../LotNumber/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "3" },
        //data: "{ 'SearchList': '" + $('#sel2').val() + "' }",
        dataType: "json"

    }).done(function (data) {
        CreateSearchPanel();



    }).fail(function (response) {
        if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
        }
    });

}

function LoadLotGrid() {

    var LstofControlViewModel = new Array();

    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");
        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");


            var value = $(this).val().trim();

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value.trim(),
                "Label": value.trim(),
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }

            //alert(JSON.stringify(search));

            LstofControlViewModel.push(search);
        }
    });


    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',

        url: '../LotNumber/BindLotGrid',
        data: JSON.stringify({ 'objdsad': LstofControlViewModel }), // Note it is important
        success: function (data) {
            $("#LotGrid").html(data)
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}

$(document).ready(function () {
  
    $(".show-pagesize-cmb").hide();
    //$('#total-records').hide();
    //$('#lblidcounter').hide();
    //$('#PagesizeCounter').hide();
    $('#PagesizeCounter').change(function () {
        // var pagesize = $('#PagesizeCounter').val();
        //$('#total-records').val(data.TotalRows);
        var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
        var paginginfo = datainfo.paginginformation;
        //  var pageinf = (1 + paginginfo.pagenum * paginginfo.pagesize + "-" + Math.min(datainfo.rowscount, (paginginfo.pagenum + 1) * pagesize) + ' of ' + datainfo.rowscount)
        //  console.log(datainfo.rowscount);
        $('#total-records').text(datainfo.rowscount);
        var pagesize = $('#PagesizeCounter').val();
        $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
    });

    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });


    $("#btdeletenok").click(function () {

        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });

        $.ajax({
            url: '../LotNumber/Delete',
            data: { LotNumberID: lotNumberId },
            success: function (data) {
                //alert(JSON.stringify(data));
                if (data == 'Deleted') {
                    //alert("Lot Number Deleted");
                    $("#alertmessageid").text('Lot Number Deleted');
                    $(".alert-popup-body-message").show();
                    //CreateGridPanel();
                    GetColumnAndBindintoGrid();
                }
                if (data == 'Fails') {
                    //alert("Failed to delete Lot Number");
                    $("#alertmessageid").text('Failed to delete Lot Number');
                    $(".alert-popup-body-message").show();
                    //  CreateGridPanel();
                }
                //else { CreateGridPanel(); }
                //$("#CustomGridListing").html(data)
            },
            error: function (response) {
                // alert(response);
                if (response.status != 0) {
                    //  alert(response.status + " " + response.statusText);
                }
            }
        });

    });

    $("#btnSubmit").click(function () {
        GetColumnAndBindintoGrid();
        //CreateGridPanel();

    });

    $("#btnok,.alert-close").click(function () {
        $(".alert-popup-body-message").hide();
        $(".alert-popup").animate({ "top": "40%" });
    })

});

function GetColumnAndBindintoGrid() {
    sessionStorage.clear();
    var LstofControlViewModel = new Array();
    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");

        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = "";

            var ElementId = $(this).attr("id");

            if (result == "checkbox") {
                value = document.getElementById(ElementId).checked;
            } else {

                value = $(this).val().trim();

            }

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value,
                "Label": value,
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }

            //alert(JSON.stringify(search));

            LstofControlViewModel.push(search);
        }
    });
  
    sessionStorage.setItem("LotNumbersSearchField", JSON.stringify(LstofControlViewModel));
    //sessionStorage.setItem("ProductSearchField", JSON.stringify(LstofControlViewModel));
    $.ajax({
        //sync: false,
        type: "POST",
        data: { 'lstobjdsad': LstofControlViewModel },
        //contentType: 'application/json; charset=utf-8',
        url: '../LotNumber/GetColumnsList',
        // Note it is important
        success: function (data) {
            CreateGridForServerSide((data));
            $("#exp_col").hide(1000);
            $("#expand").show();
            $("#collapse").hide();
        },
        error: function (response) {
            if (response.status != 0) {
            }
        }
    });

}

function CreateGridForServerSide(jsoncolumn) {
    var source = {

        datatype: "json",
        type: 'GET',
        //datafields: [{ name: 'ShipCountry' }],
        url: '../LotNumber/BindJQXGridLayoutForServerSidePaging',
        root: 'Rows',
        contentType: "application/json; charset=utf-8",
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
        processdata: function (data) {
            var pagesize = $('#PagesizeCounter').val();
            data.pagesize = pagesize;
        },
        loadComplete: function () {
            var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records').text(datainfo.rowscount);
                //$('#total-records').show();
                //$('#lblidcounter').show();
                //$('#PagesizeCounter').show();
                $(".show-pagesize-cmb").show();
            }
            else {
                $(".show-pagesize-cmb").hide();
                $('#total-records').text(0);
                //$('#total-records').hide();
                //$('#lblidcounter').hide();
                $('#PagesizeCounter').val(100);
                //$('#PagesizeCounter').hide();
            }

            //copy the pager on top of grid
            //$('#pagerjqxgrid').clone(true).appendTo($("#copypager"));
            //$('#copypager').empty();
            //$('#pagerjqxgrid').children().clone(true).appendTo($("#copypager"));
        }

    };

    var dataadapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
            //alert(error);
        },

    });



    var editrow = -1;
    var deletrow = -1;
    var editlot = {

        "text": "Edit",
        "datafield": "Edit",
        "width": "50",
        pinned: true,
        sortable: false,
        filterable: false,
        menu: false,
        "cellsrenderer": function (row, column, value) {

            editrow = row;
            var dataRecord = $("#jqxgrid").jqxGrid('getrowdata', editrow);
            var lotId = dataRecord.lotNumberID;
            //alert(JSON.stringify(dataAdapter._source));
            // var lotId = dataAdapter._source.localdata[row].lotNumberID;

            // alert(JSON.stringify(dataAdapter));
            if (value.indexOf('#') != -1) {
                value = value.substring(0, value.indexOf('#'));
            }
            value = $.trim(value);
            var html = "<a href=../LotNumber/Edit?LotNumberID=" + $.trim(lotId) + " class=\"btnEditimage\"></a>";
            return html;
            return html;
        }



    };
    var deletelot = {
        "text": "Delete",
        "datafield": "Delete",
        "width": "50",
        pinned: true,
        sortable: false,
        filterable: false,
        menu: false,
        "cellsrenderer": function (row, column, value) {
            deletrow = row;
            var dataRecord = $("#jqxgrid").jqxGrid('getrowdata', deletrow);
            //var LotNumberID = dataAdapter._source.localdata[row].lotNumberID;

            var LotNumberID = dataRecord.lotNumberID;

            // alert(JSON.stringify(dataAdapter));
            if (value.indexOf('#') != -1) {
                value = value.substring(0, value.indexOf('#'));
            }
            value = $.trim(value);
            //alert(LotNumberID);
            if (LotNumberID == '')
            { LotNumberID = 0; }
            //var html = "<a href=../Product/DeleteProduct?prodId="+productID+ " class=\"delete-btn\" onclick=\"return confirm('Are you sure you want to delete this record?')\" ) ></a>";
            var html = "<a   class=\"delete-btn\" onclick=\"deleteselectedLot(" + $.trim(LotNumberID) + ");\" ></a>";
            return html;
        }
    };
    //columns.push(deletelot);
    //columns.push(search);
    $(jsoncolumn).each(function (i, v) {
        if (v.datafield == 'ExpirationDate') {
            v.cellsrenderer = ConvertDate;
        }
    });
    jsoncolumn.push(editlot);
    jsoncolumn.push(deletelot);


    $("#jqxgrid").jqxGrid({
        width: '100%',
        source: dataadapter,
        pageable: true,
        sortable: true,
        filterable: true,
        pageable: true,
        columnsreorder: true,
        virtualmode: true,
        showdefaultloadelement: false,
        enabletooltips: true,
        columnsresize: true,
        height: '100%',
        pagermode: 'simple',
        rendergridrows: function (obj) {
            return obj.data;
        },
        ready: function () {
            var pagesize = $('#PagesizeCounter').val();
            $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });


        },
        columns: jsoncolumn,


    });




}

function LoadProductGrid() {

    var LstofControlViewModel = new Array();


    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        if (result != "hidden") {
            var result = $(this).attr("name");
            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = $(this).val().trim();

            var search = {
                "Type": result.split('-')[0],
                "Name": result.split('-')[1],
                "Value1": value.trim(),
                "Label": value.trim(),
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }
            LstofControlViewModel.push(search);
        }
    });
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',

        url: '../LotNumber/BindLotGrid',
        data: JSON.stringify({ 'objdsad': LstofControlViewModel }), // Note it is important
        success: function (data) {
            $("#LotGrid").html(data)
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}


function GetGridPanelList() {

    var optionValues = [];

    var options = $('#GridList2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });

    var srchrslt = optionValues;
    $.ajax({
        type: "POST",
        url: '../LotNumber/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "4" },
        dataType: "json"

    }).done(function (data) {
        //CreateGridPanel();
        GetColumnAndBindintoGrid();
        CreateGridColumnPanel();



    }).fail(function (response) {
        if (response.status != 0) {
            //alert(response.status + " " + response.statusText);
        }
    });

}

function CreateGridColumnPanel() {
    var Url1 = '../LotNumber/BindColumnListing';
    $.ajax({
        url: Url1,
        data: {},
        success: function (data) {
            $("#CustomGridListing").html(data)
        },
        error: function (response) {
            // alert(response);
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}



function CreateGridPanel() {


    var CurrentServerPageNo = $('#txtlotCurrentPageNo').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }

    var LstofControlViewModel = new Array();


    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");

        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");
            //alert(result);
            //alert(result.split('-')[0]);
            //alert(result.split('-')[1]);
            //alert($(this).val());

            var value = $(this).val().trim();

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value.trim(),
                "Label": value.trim(),
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }

            //alert(JSON.stringify(search));

            LstofControlViewModel.push(search);
        }
    });

    ///Keeping all Search Based Field in Session Storage
    sessionStorage.clear();
    sessionStorage.setItem("LotNumbersSearchField", JSON.stringify(LstofControlViewModel));

    //$("#ulCustomeFields input[type=text]").each(function () {
    //    var result = $(this).attr("name");
    //    var AssotableName = $(this).attr("datamessage");
    //    var _datatype = $(this).attr("datatype");
    //    var _OperatorType = $(this).attr("OperatorType");
    //    //alert(result);
    //    //alert(result.split('-')[0]);
    //    //alert(result.split('-')[1]);
    //    //alert($(this).val());

    //    var value = $(this).val().trim();

    //    var search = {
    //        "Type": result.split('-')[0],
    //        "Name": result.split('-')[1],
    //        "Value1": value.trim(),
    //        "Label": value.trim(),
    //        "AssociateTableName": AssotableName,
    //        "DataType": _datatype,
    //        "OperatorType": _OperatorType,
    //    }

    //    //alert(JSON.stringify(search));

    //    LstofControlViewModel.push(search);


    //    //  alert($(this).val());
    //});

    //$("#ulCustomeFields input[type=checkbox]").each(function () {
    //    var result = $(this).attr("name");
    //    var AssotableName = $(this).attr("datamessage");
    //    var _datatype = $(this).attr("datatype");
    //    var _OperatorType = $(this).attr("OperatorType");
    //    //alert(result);
    //    //alert(result.split('-')[0]);
    //    //alert(result.split('-')[1]);
    //    //alert($(this).val());

    //    var value = $(this).val().trim();

    //    var search = {
    //        "Type": result.split('-')[0],
    //        "Name": result.split('-')[1],
    //        "Value1": value.trim(),
    //        "Label": value.trim(),
    //        "AssociateTableName": AssotableName,
    //        "DataType": _datatype,
    //        "OperatorType": _OperatorType,
    //    }

    //    //alert(JSON.stringify(search));

    //    LstofControlViewModel.push(search);


    //    //  alert($(this).val());
    //});


    var Url1 = '../Product/BindJQXGridLayout';
    $.ajax({

        type: "POST",
        contentType: 'application/json; charset=utf-8',
        url: '../LotNumber/BindJQXGridLayout',
        data: JSON.stringify({ 'objdsad': LstofControlViewModel, 'CurrentServerPageNo': CurrentServerPageNo }), // Note it is important
        success: function (data) {
            $('#jqxgrid').jqxGrid('clear');
            GenerateGrid(data);

        },
        error: function (response) {
            // alert(response);
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
    $('.grid-btn-prev').hide();
    $('.grid-btn-next').hide();
}

//function GenerateGrid(json) {
//    debugger;
//    $("#jqxgrid").jqxGrid('updatebounddata');
//    //alert(JSON.stringify(json));
//    //var obj = JSON.parse(json);
//    var columns = json.columns;
//    alert(JSON.stringify(columns));
//    var rows = json.rows;

//    var search = {

//        "text": "Edit",
//        "datafield": "Edit",
//        "width": "100",
//        pinned: true,
//        sortable: false,
//        filterable: false,
//        menu: false,
//        "cellsrenderer": function (row, column, value) {
//            //alert(JSON.stringify(dataAdapter._source));
//            var lotId = dataAdapter._source.localdata[row].lotNumberID;

//            // alert(JSON.stringify(dataAdapter));
//            if (value.indexOf('#') != -1) {
//                value = value.substring(0, value.indexOf('#'));
//            }
//            value = $.trim(value);
//            var html = "<a href=../LotNumber/Edit?LotNumberID=" + $.trim(lotId) + " class=\"btnEditimage\"></a>";
//            return html;
//            return html;
//        }



//    };
//    var deletelot = {
//        "text": "Delete",
//        "datafield": "Delete",
//        "width": "100",
//        pinned: true,
//        sortable: false,
//        filterable: false,
//        menu: false,
//        "cellsrenderer": function (row, column, value) {
//            var LotNumberID = dataAdapter._source.localdata[row].lotNumberID;



//            // alert(JSON.stringify(dataAdapter));
//            if (value.indexOf('#') != -1) {
//                value = value.substring(0, value.indexOf('#'));
//            }
//            value = $.trim(value);
//            //alert(LotNumberID);
//            if (LotNumberID == '')
//            { LotNumberID = 0; }
//            //var html = "<a href=../Product/DeleteProduct?prodId="+productID+ " class=\"delete-btn\" onclick=\"return confirm('Are you sure you want to delete this record?')\" ) ></a>";
//            var html = "<a   class=\"delete-btn\" onclick=\"deleteselectedLot(" + $.trim(LotNumberID) + ");\" ></a>";
//            return html;
//        }
//    };
//    columns.push(deletelot);
//    columns.push(search);


//    // columns.push(search2);

//    // prepare the data
//    var source =
//    {
//        datatype: "json",
//        //datafields: [
//        //    { name: 'name', type: 'string' },
//        //    { name: 'type', type: 'string' },
//        //    { name: 'calories', type: 'int' },
//        //    { name: 'totalfat', type: 'string' },
//        //    { name: 'protein', type: 'string' }
//        //],
//        id: 'id',
//        localdata: rows
//    };
//    var dataAdapter = new $.jqx.dataAdapter(source,
//           {
//               loadComplete: function () {
//                   var length = dataAdapter.records.length;

//                   $("#hdnTotalRecords").val(parseInt(length));

//               }
//           }
//           );
//    var pagerrenderer = function () {
//        var element = $("<div style='margin-top: 5px; width: 100%; height: 100%;'></div>");
//        var paginginfo = $("#jqxgrid").jqxGrid('getpaginginformation');
//        for (var i = 0; i < paginginfo.pagescount; i++) {
//            // add anchor tag with the page number for each page.  href='#" + i + "'
//            var j = i + 1;
//            var anchor = $("<a style='padding: 5px;' >" + j + "</a>");
//            anchor.appendTo(element);
//            anchor.click(function (event) {
//                // go to a page.
//                var pagenum = parseInt($(event.target).text());
//                pagenum = pagenum - 1;
//                $("#jqxgrid").jqxGrid('gotopage', pagenum);
//            });
//        }
//        return element;
//    }
//    $("#jqxgrid").jqxGrid(
//    {
//        width: '100%',
//        autoheight: true,
//        sortable: true,
//        filterable: true,
//        pageable: true,
//        columnsreorder: true,
//        source: dataAdapter,
//        rtl: true,
//        columnsresize: true,
//        pagerrenderer: pagerrenderer,
//        columns: columns,
//        ready: function () {
//            $('#jqxgrid').jqxGrid({ pagesizeoptions: ['50'] });

//        }
//    });
//    $("#jqxgrid").jqxGrid({ columns: columns });
//    $("#jqxgrid").jqxGrid("render");


//    var Pagesizeee = parseInt($("#hdnPageNo").val());
//    var totalRecord = $("#hdnTotalRecords").val();
//    if (totalRecord > 50) {
//        Pagesizeee = 50;
//        $("#hdnPageNo").val(Pagesizeee);

//    }
//    else {
//        Pagesizeee = totalRecord;
//        $("#hdnPageNo").val(Pagesizeee);
//    }
//    // If totalRecord
//    if (totalRecord >= 500) {
//        $('.grid-btn-prev').show();
//        $('.grid-btn-next').show();
//    }
//    else {
//        $('.grid-btn-prev').hide();
//        $('.grid-btn-next').hide();
//    }
//    if (totalRecord > 0) {
//        var pagetext = "1-" + Pagesizeee + " of " + totalRecord;
//        $(".grid-details").text(pagetext);
//        $(".grid-details").show();
//    }
//    else { $(".grid-details").hide(); }

//    //$(".grid-details").show();

//};

var lotNumberId = 0;
function deleteselectedLot(LotNumberID) {

    lotNumberId = LotNumberID;
    $("#alertdeletemessageid").text("Do you wish to delete this lot number ?");
    $(".alert-popup-body-message-confirm").show();
}



//$(document).ready(function () {
//    $('.grid-btn-prev').hide();
//    $('.grid-btn-next').hide();
//    $(".grid-details").hide();
//    $(document).ajaxStart(function () {
//        $("#wait").css("display", "block");
//    });
//    $(document).ajaxComplete(function () {
//        $("#wait").css("display", "none");
//    });

//});