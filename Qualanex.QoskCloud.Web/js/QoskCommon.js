//*****************************************************************************
//*
//* QoskCommon.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Common javascript functionality used throughout Qosk.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   // ...
   ///////////////////////////////////////////////////////////////////////////////
   window.KEY_TAB = 9;
   window.KEY_ENTER = 13;
   window.KEY_SPECIALCHAR = 28; //Note that the keyboard input is CTRL + \

   // Entry Control String(s)
   ///////////////////////////////////////////////////////////////////////////////
   window.ECS_INTEGER = /[0-9]/;
   window.ECS_NUMERIC = /[0-9]|\.|\-/;
   window.ECS_ALPHA = /[a-z\u00C0-\u00FF]/i;
   window.ECS_ALPHANAME = /[a-z\u00C0-\u00FF ,.'-]/i;
   window.ECS_ALPHAEMAIL = /[a-z\u00C0-\u00FF_.+-@]/i;
   window.ECS_ALPHANUMERIC = /[0-9a-z\u00C0-\u00FF]/i;
   window.ECS_ALPHALOTNO = /[0-9a-z\u00C0-\u00FF]|\.|\-/i;
   window.ECS_ALPHANUMERICWTHSPCE = /[a-zA-Z0-9\s]/i;
   window.ECS_ALPHANUMERICWTHDASH = /[a-zA-Z0-9\u002D]/i;
   window.ECS_RCVGBARCODE = /[0-9a-z\u00C0-\u00FF\u0021\u0040\u002D]/i;

   // Entry Format String(s)
   ///////////////////////////////////////////////////////////////////////////////
   window.EFS_ALPHAFIRST = /^[a-z\u00C0-\u00FF]([a-z\u00C0-\u00FF]*)$/i;
   window.EFS_ALPHAFIRSTNUMERIC = /^[a-z\u00C0-\u00FF]([a-z0-9\u00C0-\u00FF]*)$/i;
   window.EFS_USERNAME = /^[a-z\u00C0-\u00FF]([a-z0-9\u00C0-\u00FF]){3,}$/i;
   window.EFS_NAME = /^[a-z\u00C0-\u00FF]([a-z0-9\u00C0-\u00FF ,.'-]*)$/i;
   window.EFS_USPHONE = /^(\+0?1\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
   window.EFS_EMAIL = /^([a-z0-9_.+-])+\@(([a-z0-9-])+\.)+([a-z0-9]{2,4})+$/i;
   window.EFS_ZIPCODE = /^\d{5}(?:[-\s]\d{4})?$/;
   window.ECS_LOTNO = /[0-9A-Za-z]|\.|\/|\\|\-/;
   window.ECS_PHONE = /[0-9]/;
   window.ECS_YEAR = /[0-9]/;

   // ...
   ///////////////////////////////////////////////////////////////////////////////
   window.SEG_GENERAL = 0;
   window.SEG_WARNING = 1;
   window.SEG_ERROR = 2;

   window.previousFocus = $();
   window.validateKey = validateKey;
   window.nextFocus = nextFocus;
   window.formBusy = false;
   var app = "";

   window.onGridRefreshMethods = [];
   window.addGridRefreshMethod = function (func)
   {
      exclusivePush(onGridRefreshMethods, func);
   }

   //#############################################################################
   // Global functions
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Determine the browser currently being used.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   True if the specific browser is being used, otherwise False.
   //*
   //*****************************************************************************
   function isFirefox() { return (typeof InstallTrigger !== "undefined"); }
   window.isFirefox = isFirefox;
   function isIE() { return !!document.documentMode; }
   window.isIE = isIE;
   function isEdge() { return !isIE() && !!window.StyleMedia; }
   window.isEdge = isEdge;
   function isChrome() { return !!window.chrome && !!window.chrome.webstore; }
   window.isChrome = isChrome;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   regex - Description not available.
   //*   evt   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function validateKey(regex, evt)
   {
      var theEvent = evt || window.event;
      var key = String.fromCharCode(theEvent.keyCode || theEvent.which);

      if (!regex.test(key))
      {
         theEvent.returnValue = false;

         if (theEvent.preventDefault)
         {
            theEvent.preventDefault();
         }
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   function validateField(regex, value)
   {
      return regex.test(value);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set the application title in the page header.
   //*
   //* Parameters:
   //*   value - Application title.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.setHeaderTitle = function (value)
   {
      $("#applicationTitle").text(value);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function nextFocus(currentObj, setFocus) { return gotoFocus(currentObj, "nextfocus", setFocus); }
   function prevFocus(currentObj, setFocus) { return gotoFocus(currentObj, "prevfocus", setFocus); }
   ///////////////////////////////////////////////////////////////////////////////
   function gotoFocus(currentObj, direction, setFocus)
   {
      setFocus = (typeof setFocus === "undefined") ? true : setFocus;

      if (typeof $(currentObj).attr("id") !== "undefined")
      {
         var targetObj = "#" + $(currentObj).attr("id");

         // focus on itself if the direction attribute is undefined
         if ($(currentObj).attr(direction) === undefined)
         {
            return targetObj;
         }

         $.each($(currentObj).attr(direction).split(/\s+/), function (index, value)
         {
            targetObj = "#" + value;

            if ($(targetObj).is(":visible"))
            {
               return false;
            }

            targetObj = "#" + $(currentObj).attr("id");
         });

         if (setFocus)
         {
            $(targetObj).focus();
            $(targetObj).select();
         }

         return targetObj;
      }

      return undefined;
   }


   //*****************************************************************************
   //*
   //* Summary:
   //*   Executes functions held in an array.
   //*
   //* Parameters:
   //*   methodArrayName: the name of the array as a string.
   //*   data: data to be passed to each function in the array.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.executeMethods = function (methodArrayName, data)
   {
      if (window[methodArrayName] !== undefined)
      {
         window[methodArrayName].forEach(function (f)
         {
            f(data);
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.executeMethod = function (methodArrayName, data, data2)
   {
      if (window[methodArrayName] !== undefined)
      {
         return window[methodArrayName][0](data, data2);
      }
   }
   //*****************************************************************************
   //*
   //* Summary:
   //*    Removes a function from an array
   //*
   //* Parameters:
   //*   method: the function to be removed
   //*   methodArrayName: The name of the array as a string
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.removeMethod = function (method, methodArrayName)
   {
      if (window[methodArrayName] !== undefined)
      {
         window[methodArrayName].forEach(function (f, i, array)
         {
            if (method.toString() === f.toString())
            {
               array.splice(i, 1);
               return;
            }
         });
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Replaces Segments in an html block with the segments from an ajax return.
   //*
   //* Parameters:
   //*   $target: the html block where segments are to be replaced
   //*   $newHtml: the updated segments from an ajax return
   //*   group: specify the group of segments to replace "policy", "notification", "sidebar", etc
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var replaceSegments = function ($target, $newHtml, group)
   {
      if (!group || group === "")
      {
         $target.replaceWith($newHtml);
         return;
      }

      $target.find("[data-additional-segment=true]").parents(".QoskSegment").remove();
      $target.find("[data-additional-segment=true]").remove();
      $target.find("[data-segment-group='" + group + "']").each(function ()
      {
         $(this).parents(".QoskSegment").replaceWith($newHtml.find("#" + this.id).parents(".QoskSegment"));
      });

      $newHtml.find("[data-segment-group='" + group + "'][data-additional-segment=true]").appendTo($target);
      $target.find(".panel-collapse:not(.toCollapse)").each(function ()
      {
         //console.log(this.id + " loading expand");
         executeMethods(this.id + "ExpandMethods");
      });

      $target.find(".toCollapse").slideCollapse();
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.replaceSegments = replaceSegments;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Generic ajax form submission.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var ajaxFormSubmit = function ()
   {
      var $form = $(this);
      $("#Sidebar .error").html("");

      if ($(".viewDropDown").length)
      {
         $(".viewHidden")[0].value = $(".viewDropDown")[0].value;
         $(".viewHidden")[1].value = $(".viewDropDown")[0].value;
      }
      if ($form.attr("action").includes("updateSearch") && ($("#SubmitSearch").length && ($("#SubmitSearch").attr("data-disable") === "true" || !checkCalibrationExpiration())))
      {
         return false;
      }

      var data = $form.serialize() + getAntiForgeryToken();
      var targetId = "";
      var targetArray = window[$form.attr("data-ql-target-array")];

      if (targetArray && targetArray.length > 0)
      {
         targetId = targetArray[0].target;
      }
      else
      {
         targetId = $form.attr("data-ql-target").substr(1);
      }

      data += "&tab=" + targetId;
      var options = {
         async: true,
         url: $form.attr("action"),
         type: $form.attr("method"),
         data: data,
         error: function (err)
         {
            showError(err.statusText);
            formBusy = false;
         },
         success: function (data)
         {
            if (data === "")
            {
               LogOut(true);
            }

            if ($(".ndcMin").length)
            {
               $(".ndcMin")[0].value = $(".ndcField")[0].value;
            }

            if ($(".brandMin").length)
            {
               $(".brandMin")[0].value = $(".brandField")[0].value;
            }

            if ($(".lotMin").length)
            {
               $(".lotMin")[0].value = $(".lotField")[0].value;
            }

            if ($(".mfgrMin").length)
            {
               $(".mfgrMin")[0].value = $(".mfgrField")[0].value;
            }

            if ($(".viewDropDown").length)
            {
               $(".viewHidden")[0].value = $(".viewDropDown")[0].value;
               $(".viewHidden")[1].value = $(".viewDropDown")[0].value;
            }

            var $target = $($form.attr("data-ql-target"));
            var group = $form.attr("data-ql-group");

            if (targetArray && targetArray.length > 0)
            {
               $target = $("#" + targetArray[0].target);
               group = targetArray[0].group;
            }

            var $finalize = $(".ql-finalize");
            var $newHtml = $(data);
            var $finalizeNew = $newHtml.find(".ql-finalize-return");

            if ($finalizeNew.length && $finalize.length)
            {
               $finalizeNew.removeClass("ql-finalize-return").addClass("ql-finalize");
               $finalize.replaceWith($finalizeNew);
               $newHtml.find(".ql-finalize-return").replaceWith("");
            }

            if ($form.attr("data-ql-tab"))
            {
               window.enableActionbarControl("#CMD_FILTER", $(".ql-tab a[href='" + "#tabs-" + $form.attr("data-ql-tab") + "'] .formExDivFilter table").length > 0);
               $("#tab-container").easytabs("select", "#tabs-" + $form.attr("data-ql-tab"));
            }

            replaceSegments($target, $newHtml, group);

            $("#" + targetId + "-loading").hide();

            window.onSearchResultsLoad = function ()
            {
               $(".ndcField").focus();
               $(".ndcField").select();
               window.onSearchResultsLoad = undefined;
            }
            setTimeout(onSearchResultsLoad, 550);

            formBusy = false;
         }
      };

      $("#" + targetId + "-loading").show();

      if (options.url)
      {
         if (options.url.toUpperCase().indexOf("/PRODUCT/UPA/UPDATESEARCH") != -1)
         {
            $("#wait").show();
         }
      }

      ga("send", "event", "Search", $(this).attr("action"), $(this).attr("name") || this.id);
      $.ajax(options);
      return false;
   };

   //replaces submit function on any forms with data-ql-ajax='true ' property with above ajax function
   $("body").delegate("form[data-ql-ajax='true']", "submit", ajaxFormSubmit);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Pulls antiforgerytoken from form found in layout to submit along with
   //*   ajax requests.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   Serialized string to be appended to ajax data field
   //*
   //*****************************************************************************
   function getAntiForgeryToken()
   {
      var qs = window.location.href.slice(window.location.href.indexOf('?') + 1);
      if (qs.length)
      {
         qs = "&" + qs.split("#")[0];
      }
      var $form = $(document.getElementById("__AjaxAntiForgeryForm"));
      return "&" + $form.serialize() + qs;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.getAntiForgeryToken = getAntiForgeryToken;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Redirect the user to the logout page.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function LogOut(returnUrl)
   {
      var location = "/User/Logout";

      window.location = location;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.LogOut = LogOut;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Ajax submit handler from a selected div.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var selectionAjax = function ($div, tokenData, ajaxType)
   {
      if ($div !== null && $div !== undefined)
      {
         tokenData = $div.attr("data-ql-data");
         ajaxType = $div.attr("data-ql-method");
      }

      //for Audit application, pass in the item GUID associated with the product
      tokenData += (app === "WAREHS_AUDIT" ? "&auditItemGuid=" + $("#ItemGUID").val() : "") + getAntiForgeryToken();

      reloadOnSelection.forEach(function (element, index, array)
      {
         var options = {
            async: true,
            url: element.action,
            type: ajaxType,
            data: tokenData + "&tab=" + element.target,
            error: function (err)
            {
               showError(err.statusText);
               $("#" + element.target + "-loading").hide();
            },
            success: function (data)
            {
               $("#" + element.target + "-loading").hide();
               if (data === "")
               {
                  LogOut(true);
               }
               var $target = $("#" + element.target);
               var $newHtml = $(data);

               replaceSegments($target, $newHtml, element.group);

               $newHtml.find(".toCollapse").slideCollapse();
            }
         };
         try
         {
            $("#" + element.target + "-loading").show();
            $.ajax(options);
         } catch (e)
         {
            throw e;
         }
      });
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.selectionAjax = selectionAjax;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Easily find query parameter values.
   //*
   //* Parameters:
   //*   name: the paramater to retrieve.
   //*   url: the url from which to retrieve the query paramter, if empty, the current page's url will be used
   //*
   //* Returns:
   //*   the paramter value, may be null or empty
   //*
   //*****************************************************************************
   function getParameterByName(name, url)
   {
      if (!url)
      {
         url = window.location.href;
      }

      name = name.replace(/[\[\]]/g, "\\$&");

      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);

      if (!results)
      {
         return null;
      }

      if (!results[2])
      {
         return "";
      }

      return decodeURIComponent(results[2].replace(/\+/g, " "));
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.getParameterByName = getParameterByName;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Input field masking.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(function ()
   {
      $("[mask]").each(function (e)
      {
         $(this).mask($(this).attr("mask"));
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Goes through all selected elements to update the currently selected
   //*   one.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function changeSelection(element, selectElement)
   {
      if (selectElement === undefined)
      {
         selectElement = ".selectable";
      }

      var $row = $(element).closest(selectElement);

      if ($row.attr("data-unique-selection"))
      {
         $("[data-unique-selection='" + $row.attr("data-unique-selection") + "']").removeClass("selected");
         var tables = $.fn.dataTable.tables();

         $.each(tables, function (i, table)
         {
            var nodes = $(table).dataTable().fnGetNodes();
            $(nodes).filter("[data-unique-selection='" + $row.attr("data-unique-selection") + "']").removeClass("selected");
         });
      }

      $row.addClass("selected");
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.changeSelection = changeSelection;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Keeps tab area sized so as to prevent it going beyond the footer area.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var resizeTabs = function ()
   {
      try
      {
         restoreSlick();

         if ($("#tab-container").length)
         {
            var oldHeight = $(".ql-body").height();

            $(".ql-body").height(Math.floor($(window).height() - $(".ql-body.active").offset().top - 48));

            if (oldHeight === $(".ql-body").height())
            {
               $(".ql-body").height(Math.floor($(window).height() - $(".ql-body.active").offset().top - 48));
            }

            if ($(".ql-body.active .dataTables_scrollBody").length > 0 &&
               !$(".ql-body.active table").hasClass("relationControl"))
            {
               var activeTop = $(".ql-body.active").offset().top;
               var activeHeight = $(".ql-body.active").height();
               var gridTop = $(".ql-body.active .dataTables_scrollBody").offset().top;
               var gridHeight = $(".ql-body.active .dataTables_scrollBody").height();
               var olHeightGrid = $(".ql-body.active .dataTables_scrollBody").height();

               var scrollBody = Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50;
               if (scrollBody > 0 && scrollBody > 55)
               {
                  console.log(Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50);
                  $(".ql-body.active .dataTables_scrollBody")
                     .height(Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50);
               }
               //Math.floor($(window).height() - $(".ql-body.active .dataTables_scrollBody").offset().top - (($(window).height() - $(".ql-body.active .dataTables_scrollBody").offset().top) > 200 ? 150 : 20)));

               if (oldHeight === $(".ql-body.active .dataTables_scrollBody").height())
               {
                  $(".ql-body.active .dataTables_scrollBody").height("auto");
                  $(".ql-body.active .dataTables_scrollBody")
                     .css("min-height", Math.floor($(".ql-body.active").height() - 150));
               }

               if (scrollBody > 0 && scrollBody > 55)
               {
                  $(".ql-body.active .dataTables_scrollBody").css("max-height",
                     Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50);
                  $(".ql-body.active .dataTables_scrollBody").height("auto");
               }
               //(Math.floor($(".ql-body").height() - 150)));            

               if (activeHeight <= (gridHeight + (gridTop - activeTop))) //(activeHeight / 2) < (gridHeight + (gridTop - activeTop)))
               {
                  if (scrollBody > 0 && scrollBody > 55)
                  {
                     console.log(Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50);
                     $(".ql-body.active .dataTables_scrollBody")
                        .height(Math.floor(activeHeight - Math.floor(gridTop - activeTop)) - 50);
                  }
                  //(activeHeight - (gridTop - activeTop)) - 50);
               }
               //else
               //{
               //   $(".ql-body.active .dataTables_scrollBody").height("auto");
               //}

               if (!$(".ql-body.active table").hasClass("relationControl"))
               {
                  $(".ql-body.active .dataTables_scrollHeadInner").css({ "width": "100%", "padding-right": "0px" });
                  $(".ql-body.active .dataTable ").css("width", "100%");
               }
            }
            if ($(".QoskVMargin .ql-body").length)
            {
               $(".QoskVMargin .ql-body").height($(".QoskContent").height() - 12);
            }

            $("body").hide().show(0);
         }
      }
      catch (err)
      {
         ; // do nothing
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.resizeTabs = resizeTabs;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Slick scroller elements lose their functionality on resize or when elements
   //*   are hidden, this will restore functionality and scroll.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var restoreSlick = function ()
   {
      $(".slick-initialized:visible").each(function ()
      {
         try
         {
            $(this).slick("slickGoTo", $(this).slick("slickCurrentSlide"));
         }
         catch (ex)
         {

         }
      });
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.restoreSlick = restoreSlick;

   //*****************************************************************************
   //*
   //* Summary:
   //*   ResizeTabs every time window size changes.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(window).resize(function ()
   {
      resizeTabs();
   });


   //#############################################################################
   // Global event handlers
   //#############################################################################


   //*****************************************************************************
   //*
   //* Summary:
   //*   Removes entry of barcode scanner control character when not required.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ":input:not(.scannerRequired)", function (event)
   {
      if (event.key.charCodeAt(0) === KEY_SPECIALCHAR ||/*firefox does not recognize char 28 so the following check is done*/(KEY_SPECIALCHAR === 28 && event.key === "\\" && event.ctrlKey))
      {
         $(this).attr("data-barcode", true);
         console.log("Barcode Scanner Control Character Ignored");
         event.preventDefault();
      }
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   Enforces existence of barcode scanner control character when required.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".scannerRequired", function (event)
   {
      const scanTime = Date.parse($(this).attr("data-char-scanned"));
      if (!isNaN(scanTime) && scanTime + (2 * 1000) > new Date().getTime())
      {
         $(this).attr("data-char-scanned", new Date());
         return; //within 2 second window of last keypress following valid barcode scanning start, carry on.
      }
      else if (event.key.charCodeAt(0) === KEY_SPECIALCHAR ||/*firefox does not recognize char 28 so the following check is done*/ (event.key === "\\" && event.ctrlKey))
      {
         $(this).attr("data-char-scanned", new Date());
      }
      else if (event.key.charCodeAt(0) === KEY_ENTER)
      {
         return;
      }
      event.preventDefault();
   });
   $(document).on("paste", ".scannerRequired", function (event) { event.preventDefault() });
   $(document).on("drag", ".scannerRequired", function (event) { event.preventDefault() });
   $(document).on("drop", ".scannerRequired", function (event) { event.preventDefault() });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      console.log("QoskCommon::ready()");

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      window.setHeaderTitle(document.title);
      app = $(".QoskBody").attr("AppCode");

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      $("input").attr("autocomplete", "off");
      formBusy = false;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   regex - Description not available.
   //*   evt   - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("focusout", ":input", function ()
   {
      // Save the previously clicked value for later
      window.previousFocus = $(this);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Performs up to 3 ajax requests when element is clicked.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ql-div-click", function (e)
   {
      var $div = $(this);

      if ($(e.target).hasClass("ql-no-click"))
      {
         return null;
      }

      changeSelection(this);

      selectionAjax($div);

      ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);
      return false;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Performs up to 3 ajax requests when element is clicked.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ql-ajax", function (e)
   {
      $div = $(this);
      tokenData = $div.attr("data-ql-data");
      ajaxType = $div.attr("data-ql-method");
      tokenData += getAntiForgeryToken();
      var options = {
         async: true,
         url: $div.attr("name"),
         type: ajaxType,
         data: tokenData,
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            console.log(data);
         }
      };
      try
      {
         $.ajax(options);
      } catch (e)
      {
         throw e;
      }
   })

   //*****************************************************************************
   //*
   //* Summary:
   //*   Allows clicking on images to set radio buttons in IE.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "label", function (e)
   {
      if (navigator.userAgent.match(/Trident\/7\./))
      {
         $(this).find("input[type=radio]").change();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Performs a single ajax request that can update multiple elements
   //*   simultaneously.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".ql-multi", function (e)
   {
      var $div = $(this);
      var updateOptions = getParameterByName("options", $div.attr("data-ql-data")).split(", ");

      updateOptions.forEach(function (element, index, array)
      {
         element = element.toLowerCase();
         if ($("#" + element + "-loading").length)
         {
            $("#" + element + "-loading").show();
         }
      });

      var options = {
         async: true,
         url: $div.attr("data-ql-action"),
         type: $div.attr("data-ql-method"),
         data: $div.attr("data-ql-data") + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            if (data === "")
            {
               LogOut(true);
            }

            if ($(".resultViewImg.listView").length)
            {
               $(".resultViewImg.listView").click();
            }

            var $newHtml = $(data);

            updateOptions.forEach(function (element, index, array)
            {
               var $target = $("#" + element);

               $target.replaceWith($newHtml.find("#" + element));

               if ($("#" + element + "-loading").length)
               {
                  $("#" + element + "-loading").hide();
               }

               if ($div.attr("data-ql-tab"))
               {
                  window.enableActionbarControl("#CMD_FILTER", $(".ql-tab a[href='" + "#tabs-" + $div.attr("data-ql-tab") + "'] .formExDivFilter table").length > 0);
                  $("#tab-container").easytabs("select", "#tabs-" + $div.attr("data-ql-tab"));
               }

               resizeTabs();
            });

         }
      };
      try
      {
         $.ajax(options);
         ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);

         return false;
      }
      catch (e)
      {
         throw e;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click event on tab - BEFORE
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#tab-container").bind("easytabs:before", function (e)
   {
      //prevent tab switching while infinite scroll is loading
      if (window.isInfiniteScrollActive())
      {
         return false;
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   ResizeTabs when switching between tabs.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#tab-container").bind("easytabs:after", function (e, $clicked, $targetPanel)
   {
      ga("send", "event", "Navigation", "TabChanged", $targetPanel[0].id);
      resizeTabs();
   });

   // Start page on a tab if there is a url parameter
   var tab = getParameterByName("tab");
   if (tab !== null && tab !== undefined)
   {
      $("#tab-container").easytabs("select", "#tabs-" + tab);
      window.enableActionbarControl("#CMD_FILTER", $(".ql-tabs a[href='" + "#tabs-" + tab + "'] .formExDivFilter table").length > 0);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Resize tabs one second after page load.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      window.setTimeout(resizeTabs, 1000);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Allows show/hide events to trigger actions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $.each(["show", "hide"], function (i, ev)
   {
      var el = $.fn[ev];

      $.fn[ev] = function ()
      {
         this.trigger(ev);
         return el.apply(this, arguments);
      };
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Collapses current segment while expanding the next one.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".collapse-arrow", function (e)
   {
      var doBaseProc = true;
      var $target = $($(this).attr("data-target"));

      // When the application defines an event handler, call that first
      if (window.OnSegmentCollapse !== undefined)
      {
         doBaseProc = window.OnSegmentCollapse($target);
      }

      // Do we continue and execute the base logic?
      if (doBaseProc)
      {
         if ($target.hasClass("collapsed"))
         {
            $target.slideExpand();
            ga("send", "event", "Navigation", "Expand", $(this).attr("name") || this.id);
         }
         else
         {
            $target.slideCollapse();
            ga("send", "event", "Navigation", "Collapse", $(this).attr("name") || this.id);
         }

      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   img - Description not available.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var toggleImage = function (img)
   {
      if ($(img).attr("src") === $(img).attr("data-active-src"))
      {
         $(img).attr("src", $(img).attr("data-inactive-src"));
      }
      else
      {
         $(img).attr("src", $(img).attr("data-active-src"));
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.toggleImage = toggleImage;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Adds an element to an array preventing duplicate elements from being added.
   //*
   //* Parameters:
   //*   arr     - the array to which the element will be added.
   //*   element - the element being added.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var exclusivePush = function (arr, element)
   {
      var push = true;

      if (typeof element === "object")
      {
         arr.forEach(function (e, index, array)
         {
            if (JSON.stringify(e) === JSON.stringify(element) || (typeof element === "function" && e.toString() === element.toString()))
            {
               push = false;
            }
         });
      }

      if (!arr.includes(element) && push)
      {
         arr.push(element);
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.exclusivePush = exclusivePush;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Performs exclusive push as above, but will create the array if it is not defined.
   //*
   //* Parameters:
   //*   arrName - the string name of the array
   //*   element - the item to be added
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var createAndExclusivePush = function (arrName, element)
   {
      if (window[arrName] === undefined)
      {
         window[arrName] = [];
      }

      exclusivePush(window[arrName], element);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.createAndExclusivePush = createAndExclusivePush;


   //*****************************************************************************
   //*
   //* Summary:
   //*   Set the machine name cookie on page load
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data) { setCookie("MachineName", data, 7) });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   jquery extension methods
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $.fn.extend(
      {
         //$(..).disable() will add the disabled attribute to an element
         disable: function ()
         {
            $(this).attr("disabled", true);
         },
         //$(..).disable() will set the disabled attribute to false on an element
         enable: function ()
         {
            $(this).attr("disabled", false);
         },
         //Smoothly expands a div
         slideExpand: function (after)
         {
            return this.each(function ()
            {
               var $segment = $(this);
               var txtboxFocus = $segment.attr("data-textbox-focus");

               if ($segment.attr("data-ql-animated") !== "true" && $segment.hasClass("collapsed"))
               {
                  $segment.attr("data-ql-animated", "true");
                  $segment.parent().find(".collapse-arrow").attr("src", "/Areas/Product/Images/QoskGraphics/Qosk_Arrow_Up.png");

                  var slideOptions = {
                     duration: "fast",
                     always: function ()
                     {
                        var $set = $(this).parent().parent().find("tr > div > div");

                        $(this).replaceWith($set.contents());
                        $segment.attr("data-ql-animated", "false");
                        $segment.removeClass("collapsed");
                        restoreSlick();

                        executeMethods($segment[0].id + "ExpandMethods");

                        if (after !== null && after !== undefined)
                        {
                           after();
                        }
                        if (txtboxFocus !== "")
                        {
                           $(txtboxFocus).focus();
                        }
                     }
                  };

                  $segment.parent().find("tr > div").slideDown(slideOptions);
               }
            });
         },

         //Smoothly collapses a div
         slideCollapse: function (after)
         {
            return this.each(function ()
            {
               var $segment = $(this);

               if ($segment.attr("data-ql-animated") !== "true" && !$segment.hasClass("collapsed"))
               {
                  $segment.attr("data-ql-animated", "true");
                  $segment.parent().find(".collapse-arrow").attr("src", "/Areas/Product/Images/QoskGraphics/Qosk_Arrow_Down.png");
                  $segment.addClass("collapsed");

                  if ($segment.parent().find("tr > div").length === 0)
                  {
                     $segment.wrapInner('<div style="display: block;" />')
                  }
                  var slideOptions = {
                     duration: "fast",
                     always: function ()
                     {
                        $segment.attr("data-ql-animated", "false");

                        executeMethods($segment[0].id + "CollapseMethods");

                        if (after !== null && after !== undefined)
                        {
                           after();
                        }
                     }
                  };

                  $segment.wrapInner('<div style="display: block;" />').parent().find("tr > div").slideUp(slideOptions);
               }
            });
         }
      });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $("body").on("click", function (e)
   {
      var obj = $(e.target);
      var row = obj.closest("tr");

      // Does the row we clicked on a Submenu?
      if (row.hasClass("menuSubMenu"))
      {
         // expand/collapse menu (i.e. show/hide sub-menu)
         ///////////////////////////////////////////////////////////////////////////////
         var menuBlock = row.closest(".menuParent").find(".menuBlock").first();
         menuBlock.css("display", menuBlock.css("display") === "none" ? "block" : "none");

         // Flip sub-menu icon
         ///////////////////////////////////////////////////////////////////////////////
         var menuImg = row.find(".menuGrp");
         var isExpnd = menuBlock.css("display") === "none";

         menuImg.attr("src", menuImg.attr("src").replace(isExpnd ? "(1)" : "(2)", isExpnd ? "(2)" : "(1)"));
      }
      // Is it the menu image?
      else if (obj.hasClass("imgMenu"))
      {
         $("#QoskMenu").show("slide");
         $("#menuContent").scrollTop(0);
      }
      // Is it the menu filler?
      else if (obj.hasClass("menuFiller"))
      {
         $("#QoskMenu").hide("slide");
      }
      // is it a menu item?
      else if (row.hasClass("menuItem"))
      {
         $("#QoskMenu").hide("slide");
      }
      // Check to see if it's an element outside our menu
      else if (obj.parents(".menuClose").length || (!obj.hasClass("QoskMenu") && !obj.parents(".QoskMenu").length))
      {
         $("#QoskMenu").hide("slide");
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(".actionMenu").click(function (e)
   {
      $(".menuFrame").slideToggle(200);
      e.stopPropagation();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).click(function ()
   {
      if ($(".menuFrame").is(":visible"))
      {
         $(".menuFrame", this).slideUp();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Toggle the form between expanded and collapsed views
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(".formCtrl").click(function (e)
   {
      showForm($(".formMax").css("display") === "none");

   });
   ///////////////////////////////////////////////////////////////////////////////
   function showForm(showMax)
   {
      var objCollapse = showMax ? $(".formMin") : $(".formMax");
      var objExpand = showMax ? $(".formMax") : $(".formMin");

      objCollapse.slideUp("fast", function ()
      {
         objExpand.slideDown("fast", resizeTabs);
      });

      $(".formCtrl").attr("src", "/Images/Form/Qosk_Form_" + (showMax ? "Collapse" : "Expand") + ".svg");
      $(".formCtrl").attr("title", "Show " + (showMax ? "less" : "more") + " detail");
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.hideDetail = function () { showForm(false); }
   window.showDetail = function () { showForm(true); }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(".formOptn").click(function (e)
   {
      $(".formOptn").each(function ()
      {
         var isDIV = $(this).attr("id") === e.target.id;
         $(this).attr("src", $(this).attr("src").replace(isDIV ? "(0)" : "(1)", isDIV ? "(1)" : "(0)"));
      });

      var divID = "optn_" + e.target.id;

      $(".formOption").each(function ()
      {
         $(this).css("display", $(this).attr("id") === divID ? "block" : "none");
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("keydown", ".editable", function (e)
   {
      var theEvent = e || window.event;
      var keyCode = theEvent.keyCode || theEvent.which;

      //console.log(".editable::keydown:" + keyCode);

      switch (keyCode)
      {
         case KEY_TAB:
            e.preventDefault();
            e.shiftKey
               ? prevFocus($(e.target))
               : nextFocus($(e.target));

            break;

         default:
            break;
      }

      return true;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".editable", function (e)
   {
      var result = true;

      var theEvent = e || window.event;
      var keyCode = theEvent.keyCode || theEvent.which;

      switch (keyCode)
      {
         case KEY_ENTER:
            console.log("enter");
            break;

         default:
            {
               // For each of the defined classes...
               $.each($(e.target).attr("class").split(/\s+/), function (index, value)
               {
                  // Find the class that specifies the filter type...
                  if (value.match("^ECS_"))
                  {
                     // And set the function return value to the result of the key validation, the exit the .each loop
                     result = validateKey(eval(value), e);
                     return false;
                  }
               });

               break;
            }
      }

      return result;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var showError = function (errorMsg)
   {
      $("#wait").hide();
      $("#jsErrorMsg").html(errorMsg);
      $(".alert-popup-javascript-error").show();
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.showError = showError;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Displays a message whenever JS errors occur.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.onerror = function (errorMsg, url, lineNumber)
   {
      showError(errorMsg);
      throw (this);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clears above error
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("#btnokJS").click(function ()
   {
      $(".alert-popup-javascript-error").hide();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var isNullOrWhitespace = function (input)
   {
      if (typeof input === "undefined" || input === null)
      {
         return true;
      }

      return input.replace(/\s/g, "").length < 1;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.isNullOrWhitespace = isNullOrWhitespace;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var dataTableFixHeaders = function (tableName, additionalOptions, removeClassTh, offTh)
   {
      var tables;
      var i = 0;

      if ($(tableName).hasClass("dataTable"))
      {
         // convert our html table to a data table structure
         // see datatables.net for documentation
         tables = $(tableName).DataTable();

         if (removeClassTh.length > 0)
         {
            $(tableName).find("thead tr th").removeClass(removeClassTh);
         }

         if (offTh.length > 0)
         {
            $(tableName).find("thead tr th").off(offTh);
         }

         return tables;
      }

      if (additionalOptions.length > 0)
      {
         tables = removeClassTh.length > 0
            ? $(tableName).DataTable(
               {
                  additionalOptions,
                  scrollX: true,
                  scrollCollapse: true,
                  fixedColumns: true,
                  height: "auto",
                  ordering: false
               })
            : $(tableName).DataTable(
               {
                  additionalOptions,
                  scrollX: true,
                  scrollCollapse: true,
                  fixedColumns: true,
                  height: "auto"
               });
      }
      else
      {
         tables = removeClassTh.length > 0 ?
            $(tableName).DataTable(
               {
                  scrollX: true,
                  scrollCollapse: true,
                  fixedColumns: true,
                  height: "auto",
                  ordering: false
               })
            : $(tableName).DataTable(
               {
                  scrollX: true,
                  scrollCollapse: true,
                  fixedColumns: true,
                  height: "auto"
               });
      }

      $("table.dataTable").css("width", "100%");
      $("div.dataTables_scrollHeadInner").css("width", "100%");

      if (removeClassTh.length > 0)
      {
         for (i = 0; i < removeClassTh.length; i++)
         {
            $(tableName + "  thead tr th").removeClass(removeClassTh[i]);
         }
      }

      if (offTh.length > 0)
      {
         for (i = 0; i < offTh.length; i++)
         {
            $(tableName + "  thead tr th").off(offTh[i]);
         }
      }

      resizeTabs();
      return tables;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.dtHeaders = dataTableFixHeaders;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var firstSelectionRow = function (row, events)
   {
      if (events.length > 0)
      {
         $(row).trigger(events);
      }
      else
      {
         $(row).addClass("selected");
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.selectFirstRow = firstSelectionRow;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var CompareTextboxes = function (txt1, txt2)
   {
      txt1 = new RegExp(txt1.replace(/[^a-zA-Z0-9]+/g, "").replace(/\s/g, "").toLowerCase(), 'i');
      txt2 = new RegExp(txt2.replace(/[^a-zA-Z0-9]+/g, "").replace(/\s/g, "").toLowerCase(), 'i');
      return txt1.test(txt2);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.CompareTextboxes = CompareTextboxes;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var GetIgnoreCaseSpaceTxt = function (txt)
   {
      txt = txt.replace(/[^a-z0-9]+/gi, '');
      return txt.length > 0 ? txt.toLowerCase() : "";
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.GetIgnoreCaseSpaceTxt = GetIgnoreCaseSpaceTxt;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Parses the specified field entered for special characters - scanned from barcode
   //*
   //* Parameters:
   //*   barcode - String type - actual item scanned in field
   //*   source - String type - textbox in which the barcode resides
   //*
   //* Returns:
   //*   Value scanned, if the barcode scanned it's indeed a button press and not an RA/WPT
   //*
   //*****************************************************************************
   function parseBarcode(barcode, source)
   {
      var barcodeChar = String.fromCharCode(KEY_SPECIALCHAR); //double check for barcode character; error out if found

      if (source === "SpecialChar")
      {
         if (barcode.indexOf(barcodeChar) > -1)
         {
            var barcodeRegEx = new RegExp(barcodeChar, "g");
            barcode = barcode.replace(barcodeRegEx, "");
         }
         return barcode;
      }
   }
   window.specialChar = parseBarcode;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Check if the passed in element is visible to the user, if not, scroll to it
   //*
   //* Parameters:
   //*   element - the element you want to check the visibility of
   //*
   //* Returns:
   //*
   //*****************************************************************************
   function isInViewPort(element)
   {
      var topOfElement = element.offset().top;
      var bottomOfElement = element.offset().top + element.outerHeight();
      var topOfScreen = $(window).scrollTop();
      var bottomOfScreen = $(window).scrollTop() + window.innerHeight;

      if (!(bottomOfScreen > topOfElement && topOfScreen < bottomOfElement))
      {
         $('#tabs-Sidebar').animate({ scrollTop: bottomOfElement }, "slow");
      }
   }
   window.isInViewPort = isInViewPort;
});
