﻿//*****************************************************************************
//*
//* QoskSegment.LotExp.js
//*    Copyright (c) 2016 - 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Javascript functionality for the search tab.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function()
{

   //event handler for paging through the results of an image search
    $(document)
        .on("click",
            ".ql-pag-click",
            function(e) {
                var $div = $(this);
                var options = {
                    async: true,
                    url: $div.attr("data-ql-action"),
                    type: $div.attr("data-ql-method"),
                    data: $div.attr("data-ql-data") + "&" + $("#ImageSearchForm").serialize() + getAntiForgeryToken(),
                    error: function (err) {
                    	showError(err.statusText);
                    },
                    success: function(data) {
                        var $target = $($div.attr("data-ql-target"));
                        var $newHtml = $(data);
                        $target.replaceWith($newHtml);
                        //$newHtml.effect("highlight");
                        $($div.attr("data-ql-target") + "-loading").hide();
                        //$($div.attr("data-ql-target") + "-tab").effect("highlight");
                    }
                };
                //$div.effect("highlight");
                $($div.attr("data-ql-target") + "-loading").show();
                try {
                    $.ajax(options);
                    ga('send', 'event', 'ImageSearch', 'PageClick', $(this).attr("data-ql-label"));
                    return false;
                } catch (e) {
                    throw e;
                }
            });
});