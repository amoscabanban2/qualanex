﻿$(function ()
{
   //*****************************************************************************
   //*
   //* RelationControl.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the RelationControl partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************

   $.fn.extend({

      showColumn: function (col)
      {
         $(this).find('td:nth-child(' + col + '), th:nth-child(' + col + ')').show();
      },
      hideColumn: function (col)
      {
         $(this).find('td:nth-child(' + col + '), th:nth-child(' + col + ')').hide();
      },

      enableRelationEdit: function ()
      {
         var editcol = this.attr("data-edit-col");
         this.removeClass("readOnly");
         this.addClass("edit");

         if (editcol)
         {
            this.showColumn(editcol);
         }

         this.find(".relationEditRow").show();
         this.find(".relationDisplayRow").hide();
         this.find(".relationAddRow").show();
      },

      disableRelationEdit: function ()
      {
         var editcol = $(this).attr("data-edit-col");

         if (editcol)
         {
            this.hideColumn(editcol);
         }

         this.addClass("readOnly");
         this.removeClass("edit");
         this.find(".relationAddRow").hide();
         this.find(".relationEditRow").hide();
         this.find(".relationDisplayRow").show();
      },

      addRelationRow: function ()
      {

         var $row = $(this).is("tr") ? $(this) : $(this).parents("tr:first");
         var updateMethod = $(this).parents(".relationControl:first").attr("data-add-method");
         relationMethod(updateMethod, $row, function (data)
         {
            var rowid = newGuid();
            var $newDisplayRow = $row.parents(".relationControl:first")
               .find(".relationDisplayRowTemplate").clone().attr("data-row-id", rowid)
               .removeClass("relationDisplayRowTemplate").addClass("relationDisplayRow");
            var $newEditRow = $row.parents(".relationControl:first").find(".relationEditRowTemplate")
               .clone().attr("data-row-id", rowid).removeClass("relationEditRowTemplate")
               .addClass("relationEditRow");

            updateRowFields($row, $newDisplayRow, $newEditRow);
            $newDisplayRow.insertBefore($row);
            $newEditRow.insertBefore($row);
            updateRowFromJson($newEditRow, data);

            if ($row.parents(".relationControl").hasClass("edit"))
            {
               $newEditRow.show();
               $newDisplayRow.hide();
            }
            else
            {
               $newEditRow.hide();
               $newDisplayRow.show();
            }

            $row.remove();
         });
      },

      reOrderingRelationColumns: function (cl1, cl2, after)
      {
         $('.relationControl  tr').each(function ()
         {
            var tr = $(this);
            var td1 = tr.find('td:eq(' + cl2 + ')');
            var td2 = tr.find('td:eq(' + cl1 + ')');
            if (after)
            {
               td1.detach().insertAfter(td2);
            }
            else
            {
               td1.detach().insertBefore(td2);
            }
         });
         $('.relationControl  thead tr').each(function ()
         {
            var tr = $(this);
            var td1 = tr.find('th:eq(' + cl2 + ')');
            var td2 = tr.find('th:eq(' + cl1 + ')');
            if (after)
            {
               td1.detach().insertAfter(td2);
            }
            else
            {
               td1.detach().insertBefore(td2);
            }
         });
      },

      updateRelationRow: function ()
      {
         var $row = $(this).is("tr") ? $(this) : $(this).parents("tr:first");
         var $displayRow = $("[data-row-id=" + $row.attr("data-row-id") + "].relationDisplayRow");
         var updateMethod = $row.parents(".relationControl").attr("data-update-method");
         relationMethod(updateMethod, $displayRow, function (data)
         {
            updateRowFromJson($row, data);
            $row.removeClass("updated");
         });
      },

      deleteRelationRow: function ()
      {
         var $row = $(this).is("tr") ? $(this) : $(this).parents("tr:first");
         var deleteMethod = $(this).parents(".relationControl:first").attr("data-delete-method");
         relationMethod(deleteMethod, $row, function ()
         {
            $row.remove();
         });
      },

      saveAllRelationAddEdits: function ()
      {
         this.find("tr.updated").each(function ()
         {
            if ($(this).hasClass("relationEditRow"))
            {
               $(this).updateRelationRow();
            }
            else if ($(this).hasClass("relationAddRow"))
            {
               $(this).addRelationRow();
            }
         });
      },
      getRowFields: function ()
      {
         var $row = this;

         if (this.hasClass("relationDisplayRow"))
         {
            $row = $(".relationEditRow[data-row-id=" + this.attr("data-row-id") + "]");
         }

         return $row.find("input, select");
      },
      serializeRowArray: function ()
      {
         //Important: Get the results as you normally would...
         var results = [{}];
         //Now, find all the checkboxes and append their "checked" state to the results.
         this.getRowFields().each(function ()
         {
            if (this.type === "checkbox")
            {
               results.push({ name: this.name, value: this.checked });
            }
            else
            {
               results.push({ name: this.name, value: this.value });
            }
         });

         return results;
      },
      serializeRow: function ()
      {
         return jQuery.param(this.serializeRowArray());
      }
   });

   function newGuid()
   {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c)
      {
         var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
         return v.toString(16);
      });
   }

   function updateRowFields($original, $display, $edit)
   {
      $original.getRowFields().each(function ()
      {
         switch (this.type)
         {
            case "checkbox":
               if ($edit)
               {
                  $edit.find("[name=" + this.name + "]")[0].checked = this.checked;
               }

               $display.find("[name=" + this.name + "]")[0].checked = this.checked;
               break;
            case "select-one":
               if ($edit)
               {
                  $edit.find("[name=" + this.name + "]").val($(this).val());
               }

               $display.find("[data-field-name=" + this.name + "]").html(this.selectedOptions[0].innerText.trim());
               break;
            default:
               if ($edit)
               {
                  $edit.find("[name=" + this.name + "]").val($(this).val());
               }

               $display.find("[data-field-name=" + this.name + "]").html($(this).val());
         }
      });
   }

   function relationMethod(method, $row, success)
   {

      if (method.trim() === "")
      {
         success();
      }

      var afterMethod = function (returnData)
      {
         if (returnData.error && returnData.error !== "")
         {
            showError(returnData.error);
         }
         else
         {
            success(returnData);
         }
      };

      if (window[method])
      {
         afterMethod(window[method]($row.serializeRow()));
      }
      else
      {
         var options = {
            async: true,
            url: method,
            type: "POST",
            data: $row.serializeRow() + getAntiForgeryToken(),
            error: function (err)
            {
               showError(err.statusText);
               return false;
            },
            success: function (returnData)
            {
               afterMethod(returnData);
            }
         };
         try
         {
            $.ajax(options);
            return true;
         }
         catch (e)
         {
            throw e;
         }
      }

      return true;
   }

   function updateRowFromJson($row, data)
   {
      var $edit = $row.parents(".relationControl:first").find(".relationEditRow[data-row-id=" + $row.attr("data-row-id") + "]");
      for (var field in data)
      {
         var $input = $edit.find("[name=" + field + "]");

         if ($input)
         {
            switch ($input[0].type)
            {
               case "checkbox":
                  $input[0].checked = data[field];
                  break;
               default:
                  if (data[field] && data[field].toString().includes("/Date("))
                  {
                     $input.val((new Date(parseInt(data[field].substr(6)))).toGMTString());
                  }
                  else
                  {
                     $input.val(data[field]);
                  }
            }
         }
      }

      var $display = $row.parents(".relationControl:first").find(".relationDisplayRow[data-row-id=" + $row.attr("data-row-id") + "]");
      updateRowFields($edit, $display);
   }

   $(document).on("click", ".relationDisplayRow", function (e)
   {
      if (!$(e.target).is("a"))
      {
         var $row;

         if ($(this).is("tr"))
         {
            $row = $(this);
         }
         else
         {
            $row = $(this).parents("tr:first");
         }

         var selectMethod = $(this).parents(".relationControl:first").attr("data-select-method");
         relationMethod(selectMethod, $row, function ()
         {
            changeSelection($row);
         });
      }
   });

   $(document).on("change", ".relationEditRow", function ()
   {
      $(this).addClass("updated");
   });

   $(document).on("change", ".relationAddRow", function ()
   {
      $(this).addClass("updated");

      if ($(this).hasClass("blank"))
      {
         $(this).removeClass("blank");
      }

      //Do not add a new ddl row for Waste stream in green bin processing
      if ($(this).closest("div").find("select").attr("name") !== "WasteStreamCode")
      {
         var $newAddRow = $(this).parents(".relationControl:first").find(".relationAddRowTemplate").clone().removeClass("relationAddRowTemplate").addClass("relationAddRow").show();
         $newAddRow.insertAfter($(this));
      }
   });

   $(document).on("click", "a.relationControlUpdate", function () { $(this).updateRelationRow() });

   $(document).on("click", "a.relationControlAdd", function () { $(this).addRelationRow(); });

   $(document).on("click", "a.relationControlDelete", function () { $(this).deleteRelationRow() });
});