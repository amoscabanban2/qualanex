﻿function SetPagesizeDynamic() {
    var Pagesizeee = parseInt($("#hdnPageNo").val());

    $('#jqxgrid').jqxGrid({ pagesizeoptions: [Pagesizeee] });
    $("#wait").css("display", "none");
    $("#jqxgrid").jqxGrid('render');
    if ($("#hdnTotalRecords").val() > 0) {
        var pagetext = "1-" + Pagesizeee + " of " + $("#hdnTotalRecords").val();
        $(".grid-details").text(pagetext);
        $(".grid-details").show();
    }
}


function ViewPolicySearchScreen(policyID) {
    var selectedType = $("#SelectedMWRType").val();
    var ManufactureName = $("#ManufactureName").val();

    $.ajax({
        url: '../Policy/ViewDeletePolicyProfile/',
        data: { 'policyID': policyID, 'MWRType': selectedType, 'MWRProfileCode': ManufactureName },

        success: function (data) {

        },
        error: function (response) {
            if (response.status != 0) {
            }
        }
    });

}
function PolicySearch(e) {

    $('#jqxgrid').jqxGrid('clear');
    if ($("#frmIndexPolicy").validationEngine('validate') == false) {
        if (e != undefined) {
            e.preventDefault();   
        }

        return false;
    }

    var _this = this;
    $(".grid-details").hide();
    if (typeof (Storage) !== "undefined") {
        sessionStorage.clear();
    }


    var CurrentServerPageNo = $('#txtCurrentPageNo').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }
    var policyType = $("#PolicyType").val();
    var selectedType = $("#SelectedMWRType").val();
    var ManufactureName = $("#ManufactureName").val();

    if (selectedType != '' && ManufactureName != '') {
        //Code for local storage
        if (typeof (Storage) !== "undefined") {
            sessionStorage.setItem('policytype', policyType)
            sessionStorage.setItem('selectedType', selectedType)
            sessionStorage.setItem('ManufactureName', ManufactureName)
        }
        //end

        //Serversidecode

        var datafields = [{ name: 'PolicyNameforGrid', type: 'string' },
               { name: 'PolicyTypeName', type: 'string' },
               { name: 'PharmacyName', type: 'string' },
                { name: 'PolicyID', type: 'int' }
        ];

        var columns = [
        { text: "Policy Type", datafield: "PolicyNameforGrid", cellsformat: 'd' },
        { text: "Policy Name", datafield: "PolicyTypeName" },
        { text: "Customer", datafield: "PharmacyName" }];

        var editrow = -1;
        var editlot = {
            "text": "Edit",
            "datafield": "Edit",
            "width": "50",
            pinned: true,
            sortable: false,
            filterable: false,
            menu: false,
            "cellsrenderer": function (row, columnfield, value, defaulthtml, columnproperties, data) {
                editrow = row;
                var policyID = data.PolicyID;
                if (value.indexOf('#') != -1) {
                    value = value.substring(0, value.indexOf('#'));
                }
                value = $.trim(value);
                //var html = "<a href=../Policy/RedirectToPolicyforEDIT?PolicyID=" + $.trim(policyID) + " class=\"btnEditimage\"></a>";
                var html = "<a  onclick= \"redirecttoEdit('" + policyID + "')\"  class=\"btnEditimage\"></a>";
                return html;
            }
        };
      
        var viewpolicyrow = -1
        var viewpolicy = {
            "text": "View",
            "datafield": "View",
            "width": "50",
            pinned: true,
            sortable: false,
            filterable: false,
            menu: false,

            "cellsrenderer": function (row, columnfield, value, defaulthtml, columnproperties, data) {
                viewpolicyrow = row;
                var policyID = data.PolicyID;
                if (value.indexOf('#') != -1) {
                    value = value.substring(0, value.indexOf('#'));
                }
                value = $.trim(value);
                var selectedType = $("#SelectedMWRType").val();
                var ManufactureName = $("#ManufactureName").val();
                var html = "<a href=../Policy/ViewEditPolicy?PolicyID=" + $.trim(policyID) + " class=\"view-btn\"></a>";

               // var html = "<a  onclick= \"redirecttoEdit('" + policyID + "')\"  class=\"view-btn\"></a>";
                return html;
            }
        };


        columns.push(editlot);
        columns.push(viewpolicy);
        var url = '../Policy/GetPolicyDetails';
        var data = { ManufactureName: ManufactureName.toString(), Type: selectedType, CurrentServerPageNo: CurrentServerPageNo, PolicyType: policyType };

        BindJQXGrid("#jqxgrid", url, data, columns, datafields, false, function () {
            var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records').text(datainfo.rowscount);
                $('#total-records').show();
                $('#lblidcounter').show();
                $('#PagesizeCounter').show();
                $('.show-pagesize-cmb').show();
            }
            else {
                $('#total-records').text(0);
                $('#total-records').hide();
                $('#lblidcounter').hide();
                $('#PagesizeCounter').val(100);
                $('#PagesizeCounter').hide();
                $('.show-pagesize-cmb').hide();

            }

        },function () {
            setTimeout(function () {
                var state = sessionStorage.getItem("GridState");
                if (state) {
                    try {
                        state = JSON.parse(state);

                        $("#jqxgrid").jqxGrid('loadstate', state);

                        var index = state.selectedrowindexes;
                        if (!isNaN(index)) {
                            $("#jqxgrid").jqxGrid('ensurerowvisible', index);
                        }

                        sessionStorage.removeItem("GridState");
                    } catch (e) { }
                }
            }, 1000);
        });

    }
    else {

    }

}


var updatePageState = function (pagenum) {
    var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
    var pagenum = datainfo.paginginformation.pagenum;
    var pagesize = datainfo.paginginformation.pagesize;
    var startrow = pagenum * pagesize;
    // select the rows on the page.
    $("#jqxgrid").jqxGrid('beginupdate');
    var checkedItemsCount = 0;
    for (var i = startrow; i < startrow + pagesize; i++) {
        var boundindex = $("#jqxgrid").jqxGrid('getrowboundindex', i);
        var value = $("#jqxgrid").jqxGrid('getcellvalue', boundindex, 'available');
        if (value) checkedItemsCount++;
        if (value) {
            $("#jqxgrid").jqxGrid('selectrow', boundindex);
        }
        else {
            $("#jqxgrid").jqxGrid('unselectrow', boundindex);
        }
    }

    $("#jqxgrid").jqxGrid('endupdate');
    if (checkedItemsCount == pagesize) {
        columnCheckBox.jqxCheckBox({ checked: true });
    }
    else if (checkedItemsCount == 0) {
        columnCheckBox.jqxCheckBox({ checked: false });
    }
    else {
        columnCheckBox.jqxCheckBox({ checked: null });
    }
}

function GetProfileNameLIst() {
    var selectedType = $("#SelectedMWRType").val();
    if (selectedType != "Please select") {
        $('#ManufactureName, #SelectedMWRType').css({
            "border": "",
            "background": ""
        });
        //$('#ManufactureName').removeAttr("disabled", "disabled");
        $.ajax({

            url: '../Policy/BindManufactureRepackagerWholesaler',
            async: false,
            data: { Ptype: selectedType.toString() },
            datatype: "json",
            success: function (data) {
                $("#ManufactureName").empty();
                $("#ManufactureName").append("<option  value=''>Please select</option>");
                $.each(data, function (index, optionData) {
                    if (selectedType == 'ManufacturerGroup' || selectedType == 'WholesalerGroup') {
                        $("#ManufactureName").append("<option value='" + optionData.ProfileGroupID + "'>" + optionData.GroupName + "</option>");
                    }
                    else { $("#ManufactureName").append("<option value='" + optionData.ProfileCode + "'>" + optionData.Name + "</option>"); }
                });
            }, error: function (response) {
                $("#ManufactureName").empty();
                if (response.status != 0) {
                }
            }
        });
    }
    else { $("#ManufactureName").empty(); }
}
var policyglobID = 0;
function deleteselectedPolicy(policyID) {

    policyglobID = policyID;
    $("#alertdeletemessageid").text("Do you wish to delete this policy ?");
    $(".alert-popup-body-message-confirm").show();
}
function redirecttoEdit(id) {
    //var html = "<a href=../Policy/ViewEditPolicy?PolicyID=" + $.trim(policyID) + " class=\"view-btn\"></a>";
    var state = $("#jqxgrid").jqxGrid('savestate');
    sessionStorage.setItem("GridState", JSON.stringify(state));
    //alert(state.selectedrowindexes);
    window.location = "../Policy/RedirectToPolicyforEDIT?PolicyID=" + id;
}
$(document).ready(function () {
    BindValidations();

    $(".show-pagesize-cmb").hide();
    $('#PagesizeCounter').change(function () {
        var pagesize = $('#PagesizeCounter').val();
        $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
    });
  



    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });

    $("#btdeletenok").click(function (e) {

        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
        $.ajax({
            url: '../Policy/DeleteSelectedPolicy',
            data: { policyID: policyglobID },
            success: function (data) {
                if (data.Result == 'OK') {
                    $("#alertmessageid").text(Policy.DeleteSuccess);
                    $(".alert-popup-body-message").show();
                    PolicySearch(e);
                }
                else if (data.Result == 'Deleted') {
                    $("#alertmessageid").text(Policy.Delete);
                    $(".alert-popup-body-message").show();
                }
                else {

                    $("#alertmessageid").text(Policy.DeleteFailed);
                    $(".alert-popup-body-message").show();

                }
            },
            error: function (response) {
                if (response.status != 0) {
                }
            }
        });
    });



    $("#hdnPageNo").val(50);

    $(document).keypress(function (e) {
        if (e.which == 13) {
            PolicySearch(e);
        }
    })

    $("#btnNo").click(function () {
        window.location = '../Policy/Index';
    });

    $("#btdeletenCancel").click(function () {
        UpdatePolicy(true);
        $(".alert-popup").animate({ "top": "40%" });
        $(".alert-popup-body-message-confirm").hide();
    })

    $("#btnok,.alert-close").click(function () {
        $(".alert-popup").animate({ "top": "40%" });
        $(".alert-popup-body-message").hide();
    })

    if (typeof (Storage) !== "undefined") {



        var referrer = document.referrer;

        $("#PolicyType").val(sessionStorage.getItem('policytype'));
        $("#SelectedMWRType").val(sessionStorage.getItem('selectedType'));
        GetProfileNameLIst();
        $("#ManufactureName").val(sessionStorage.getItem('ManufactureName'));
        PolicySearch();

    }
    else {

    }

    $("#SelectedMWRType").change(function () {
        GetProfileNameLIst();
    });



    $("#ManufactureName").change(function () {
        $('#ManufactureName').css({
            "border": "",
            "background": ""
        });
    });

    $('#PolicyType').val(0);
    $("#frmIndexPolicy").validationEngine();

    $('.lot-icon').unbind("click");
});

