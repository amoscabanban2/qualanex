﻿$(document).ready(function () {
    CreateSearchPanel();
    CreateGridColumnPanel();

    
});




function CreateSearchPanel() {

    var Url1 = '../LotNumber/CreateSearchPanel';
    $.ajax({
        url: '../LotNumber/CreateSearchPanel',
        data: {},
        success: function (data) {
            $("#SearchPanelList").html(data)

            ///Checking if Product List is already been searched by , then start assigning the value

            var rpreviouspage = document.referrer;
           if (sessionStorage.getItem("LotNumbersSearchField") != null) {

                var objSearchFieldValue = sessionStorage.getItem("LotNumbersSearchField");
                SetSearchFieldValue(JSON.parse(objSearchFieldValue));
                ///Binding the Grid based on Previous search
                //CreateGridPanel();
                GetColumnAndBindintoGrid();
           }

            LoadColumnListingInPopup();
        },
        error: function (response) {
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}

///function to start assigning search value to control
function SetSearchFieldValue(objSearchFieldValue) {

    for (var i = 0; i < objSearchFieldValue.length; i++) {
        var SearchFieldObject = objSearchFieldValue[i];
        var controlname = SearchFieldObject.Type + '-' + SearchFieldObject.Name;
        document.getElementsByName(controlname)[0].value = SearchFieldObject.Value1;
    }

}

function LoadColumnListingInPopup() {

    $.ajax({
        url: '../LotNumber/CreateColumnListingPopUp',
        data: {},
        success: function (data) {
            $("#ColumnListingPopUp").html(data);
            BindCustomSearch();
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
};

function GetLeftPanelList() {
    //selectAllList();
    var optionValues = [];

    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });

    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: '../LotNumber/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "3" },
        //data: "{ 'SearchList': '" + $('#sel2').val() + "' }",
        dataType: "json"

    }).done(function (data) {
        CreateSearchPanel();



    }).fail(function (response) {
        if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
        }
    });

}

function LoadLotGrid() {

    var LstofControlViewModel = new Array();

    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");
        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");


            var value = $(this).val().trim();

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value.trim(),
                "Label": value.trim(),
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }

            //alert(JSON.stringify(search));

            LstofControlViewModel.push(search);
        }
    });


    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',

        url: '../LotNumber/BindLotGrid',
        data: JSON.stringify({ 'objdsad': LstofControlViewModel }), // Note it is important
        success: function (data) {
            $("#LotGrid").html(data)
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}

$(document).ready(function () {
  
    $("#jqxgrid").bind("pagechanged", function (event) {
        clearCookiesfn();
        var args = event.args;
        pagersizeno = args.pagenum;
        pagesize = args.pagesize;
        $.cookie("pagersizeno", pagersizeno);
        $.cookie("pagesize", pagesize);
        //alert(pagersizeno);

    });
    $(".show-pagesize-cmb").hide();
    $('#PagesizeCounter').change(function () {
        var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
        var paginginfo = datainfo.paginginformation;
    
        $('#total-records').text(datainfo.rowscount);
        var pagesize = $('#PagesizeCounter').val();
        $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
    });

    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });


    

    $("#btnSubmit").click(function () {
        //clearCookiesfn();
        alert("Lot");
        GetColumnAndBindintoGrid();
        //CreateGridPanel();

    });

    $("#btnok,.alert-close").click(function () {
        $(".alert-popup-body-message").hide();
        $(".alert-popup").animate({ "top": "40%" });
    })

});

function GetColumnAndBindintoGrid() {
    //sessionStorage.clear();
    var LstofControlViewModel = new Array();
    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");

        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = "";

            var ElementId = $(this).attr("id");

            if (result == "checkbox") {
                value = document.getElementById(ElementId).checked;
            } else {

                value = $(this).val().trim();

            }

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value,
                "Label": value,
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }

            //alert(JSON.stringify(search));

            LstofControlViewModel.push(search);
        }
    });
  
    sessionStorage.setItem("LotNumbersSearchField", JSON.stringify(LstofControlViewModel));
    //sessionStorage.setItem("ProductSearchField", JSON.stringify(LstofControlViewModel));
    $.ajax({
        //sync: false,
        type: "POST",
        data: { 'lstobjdsad': LstofControlViewModel },
        //contentType: 'application/json; charset=utf-8',
        url: '../LotNumber/GetColumnsList',
        cache:false,
        // Note it is important
        success: function (data) {
            CreateGridForServerSide((data));
            $("#exp_col").hide(1000);
            $("#expand").show();
            $("#collapse").hide();
        },
        error: function (response) {
            if (response.status != 0) {
            }
        }
    });

}

function CreateGridForServerSide(jsoncolumn) {

        var pagesize = $('#PagesizeCounter').val();;
        var pagenmbr = $.cookie("pagersizeno");
        
        if (parseInt(pagenmbr) > 0) {
            pagenmbr = parseInt(pagenmbr);

        }
        else { pagenmbr = 0; }
        var pagesz = $.cookie("pagesize");
        if (parseInt(pagesz) > 0) {
            pagesize = parseInt(pagesz);
            $('#PagesizeCounter').val(pagesize);
        }
        else {
           // pagesize = $('#PagesizeCounter').val(100);
        }


    var source = {

        datatype: "json",
        type: 'GET',
        pagenum: pagenmbr,
        //datafields: [{ name: 'ShipCountry' }],
        url: '../LotNumber/BindJQXGridLayoutForServerSidePaging',
        root: 'Rows',
        contentType: "application/json; charset=utf-8",
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
       
        loadComplete: function () {
            //var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
            //var paginginfo = datainfo.paginginformation;
            //if (datainfo.rowscount > 0) {
            //    $('#total-records').text(datainfo.rowscount);
            //    $(".show-pagesize-cmb").show();
            //}
            //else {
            //    $(".show-pagesize-cmb").hide();
            //    $('#total-records').text(0);
               
            //    $('#PagesizeCounter').val(100);
               
            //}
                        var totalrecord = source.totalrecords;

                        // var paginginfo = datainfo.paginginformation;
                        if (totalrecord > 0) {
                            $('#total-records').text(totalrecord);
                            $(".show-pagesize-cmb").show();
                        }
                        else {
                            $(".show-pagesize-cmb").hide();
                            $('#total-records').text(0);
                            $('#PagesizeCounter').val(100);

                        }
        },
        processdata: function (data) {
        var pagesize = $('#PagesizeCounter').val();
        data.pagesize = pagesize;
    },

    };

    var dataadapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
            //alert(error);
        },

    });



    var editrow = -1;
    var deletrow = -1;
    var editlot = {

        "text": "Edit",
        "datafield": "Edit",
        "width": "50",
        pinned: true,
        sortable: false,
        filterable: false,
        menu: false,
        "cellsrenderer": function (row, column, value) {

            editrow = row;
            var dataRecord = $("#jqxgrid").jqxGrid('getrowdata', editrow);
            var lotId = dataRecord.lotNumberID;
            //alert(JSON.stringify(dataAdapter._source));
            // var lotId = dataAdapter._source.localdata[row].lotNumberID;

            // alert(JSON.stringify(dataAdapter));
            if (value.indexOf('#') != -1) {
                value = value.substring(0, value.indexOf('#'));
            }
            value = $.trim(value);
            var html = "<a href=../LotNumber/Edit?LotNumberID=" + $.trim(lotId) + " class=\"btnEditimage\"></a>";
            return html;
            return html;
        }



    };
    
    jsoncolumn.push(editlot);
   
    $(jsoncolumn).each(function (i, v) {

        if (v.datafield == 'ExpirationDate') {
            v.cellsrenderer = ConvertDate;
        }
        if (v.datafield == 'ProfileCode') {
            v.datafield = 'Name';
        }

        if (v.datafield == 'DosageCode') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                return '<div title="' + data.DosagesDescription + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + value + '</div>';
            };
        }

        if (v.datafield == 'RXorOTC') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                var newVal;
                if (value == true) {
                    newVal = 'Rx';
                }
                else {
                    newVal = 'OTC';
                }

                return '<div title="' + newVal + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + newVal + '</div>';
            };
        }

        if (v.datafield == 'UnitDose') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                var newVal;
                if (value == true) {
                    newVal = 'Yes';
                }
                else {
                    newVal = 'No';
                }

                return '<div title="' + newVal + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + newVal + '</div>';
            };
        }

        if (v.datafield == 'ARCOSReportable') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                var newVal;
                if (value == true) {
                    newVal = 'Yes';
                }
                else {
                    newVal = 'No';
                }

                return '<div title="' + newVal + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + newVal + '</div>';
            };
        }

        if (v.datafield == 'BrandorGeneric') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                var newVal;
                if (value == true) {
                    newVal = 'Brand';
                }
                else {
                    newVal = 'Generic';
                }

                return '<div title="' + newVal + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + newVal + '</div>';
            };
        }
        if (v.datafield == 'PhysicianSample') {
            v.cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {
                var newVal;
                if (value == true) {
                    newVal = 'Yes';
                }
                else {
                    newVal = 'No';
                }

                return '<div title="' + newVal + '" style="overflow: hidden; text-overflow: ellipsis; padding-bottom: 2px; text-align: left; margin-right: 2px; margin-left: 4px; margin-top: 4px;">' + newVal + '</div>';
            };
        }
    });

    $("#jqxgrid").jqxGrid({
        width: '100%',
        source: dataadapter,
        pageable: true,
        sortable: true,
        filterable: true,
        pageable: true,
        columnsreorder: true,
        virtualmode: true,
        showdefaultloadelement: false,
        enabletooltips: true,
        columnsresize: true,
        pagesize: pagesize,
        height: '100%',
        pagermode: 'simple',
        rendergridrows: function (obj) {
            return obj.data;
        },
        ready: function () {
            var pagesize = $('#PagesizeCounter').val();
            //$('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
            pagesize: pagesize
        },
        columns: jsoncolumn,


    });




}


function LoadProductGrid() {

    var LstofControlViewModel = new Array();


    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        if (result != "hidden") {
            var result = $(this).attr("name");
            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = $(this).val().trim();

            var search = {
                "Type": result.split('-')[0],
                "Name": result.split('-')[1],
                "Value1": value.trim(),
                "Label": value.trim(),
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }
            LstofControlViewModel.push(search);
        }
    });
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',

        url: '../LotNumber/BindLotGrid',
        data: JSON.stringify({ 'objdsad': LstofControlViewModel }), // Note it is important
        success: function (data) {
            $("#LotGrid").html(data)
        },
        error: function (response) {

            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}


function GetGridPanelList() {

    var optionValues = [];

    var options = $('#GridList2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });

    var srchrslt = optionValues;
    $.ajax({
        type: "POST",
        url: '../LotNumber/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "4" },
        dataType: "json"

    }).done(function (data) {
        //CreateGridPanel();
        GetColumnAndBindintoGrid();
        CreateGridColumnPanel();



    }).fail(function (response) {
        if (response.status != 0) {
            //alert(response.status + " " + response.statusText);
        }
    });

}

function CreateGridColumnPanel() {
    var Url1 = '../LotNumber/BindColumnListing';
    $.ajax({
        url: Url1,
        data: {},
        success: function (data) {
            $("#CustomGridListing").html(data);
            BindCustomGrid();
        },
        error: function (response) {
            // alert(response);
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}



function clearCookiesfn()
{
    $.removeCookie("pagersizeno");
    $.removeCookie("pagesize");
}

