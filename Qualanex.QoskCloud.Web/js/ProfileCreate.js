﻿var isLeaveProfilewithoutSave = true;
var urlName = null;
$(".alert-popup-body").hide();

function SetAccountDetails() {

    $("#AccountgrpType").hide();
    $("#btnUpdateAccount").hide();

}

$(window).scroll(function () {
    var noofRecords = ($("#hdnTotalRecords").val());
    if (parseInt($("#hdnPageNo").val()) < noofRecords) {
        if (parseInt($("#hdnPageNo").val()) < noofRecords) {
            if ($(window).scrollTop() == $(document).outerHeight() - $(window).outerHeight()) {
                //$("#wait").css("display", "block");
                //$("#hdnPageNo").val(parseInt($("#hdnPageNo").val()) + 50);
                //SetPagesizeDynamic();
            }
        }

    }
});
$(document).ready(function () {
    BindValuesInData("div.main-container");
    BindValidations();
    $("#frmCreateProfile").validationEngine();

    //Code for validate
    $('#btnProfileCreate').click(function (e) {
        if (!$("#frmCreateProfile").validationEngine('validate')) {
            e.preventDefault();
            return false;
        }
        else {
            return true;
        }
    });

    $(".show-pagesize-cmb").hide();
    $('#tab-container').easytabs();
    $("#jqxWidget").removeClass("margin_grid");
    if ($('#ProfileSuccessMessage').text() == "Profile has been created successfully") {
        $("#alertmessageid").text("Profile has been created successfully ");
        $(".alert-popup-body-message").show();
    }
    if ($('#ProfileSuccessMessage').text() == "Save Fails") {
        $("#alertmessageid").text("Profile has not been created");
        $(".alert-popup-body-message").show();
    }
    //$(".alert-popup-body-message").hide();
    $("#btnok,.alert-close").click(function () {
        $(".alert-popup-body-message").hide();
        $(".alert-popup").animate({ "top": "40%" });
    })

    $('.wrapper').click(function () {
        $('#ProfileSuccessMessage').text("");

    });

    $(".custom-checkbox input[type='checkbox']").after("<i></i>");
    //$('.lot-icon').click(function (e) {
    //    urlName = $(this).attr('href');
    //    e.stopPropagation();
    //    e.preventDefault();
    //    if (isLeaveProfilewithoutSave == true) {

    //        $(".alert-popup-body").show();
    //        $(".alert-popup").animate({ "top": "40%" });
    //    }
    //    else {
    //        window.location.href = urlName;
    //    }
    //});
    //$(".alert-close, #btnAlertStay").click(function () {
    //    $(".alert-popup-body").hide();
    //    $(".alert-popup").animate({ "top": "40%" });
    //})

    //$('#btnAlertSave').click(function () {
    //    // CreareProfile();
    //    $("#frmCreateProfile").submit();
    //    $(".alert-popup-body").hide();
    //    isLeaveProfilewithoutSave = false;
    //    return true;

    //});
    //$('#btnAlertLeave').click(function () {
    //    isLeaveProfilewithoutSave = false;
    //    window.location.href = urlName;
    //    return true;
    //});

    function CreareProfile() {
        var rprofileCode = $('#Name').val();
        if (rprofileCode > 0) {
            $('#RollupProfileCode').val(rprofileCode);
        }
        else {
            $('#RollupProfileCode').val(0);
        }
        return true;
    }

    $('.profileSave').click(function () {
        // CreareProfile();
        isLeaveProfilewithoutSave = false;

    });

    $('#DEAExpirationDate').datepicker({
        //changeMonth: true,
        //changeYear: true,
        //dateFormat: 'MM dd, yy',
        //minDate: 1,
    });
    $('#VendorLoadDate').datepicker({
        //changeMonth: true,
        //changeYear: true,
        //datefrmCreateProfileat: 'MM dd, yy',
        //minDate: 1,
    });
    function NumericOnly(obj) {
        if (obj.value.length > 0) {

            obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
        }
    }
    $(function () {
        $('#staticParent').on('keydown', '#VendorID', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
    })
    function NumericWithDecimalOnly(obj) {
        if (obj.value.length > 0) {
            obj.value = obj.value.replace(/[^0-9\.]/g, '');
            //obj.value = obj.value.replace(/[((\d+)(\.\d{2}))$]/g, '');
        }
    }

   
    
    

});


