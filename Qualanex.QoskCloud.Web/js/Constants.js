
var DateFormat = 'mm/dd/yy';
var DateFormatGrid = 'MM/dd/yy';
var GridDateTimeFormat = 'M/d/yyyy h:mm:ss tt';
var checkNDCUPCWithDashesRegex = [/^\d{4}-\d{4}-\d{2}$/, /^\d{5}-\d{3}-\d{2}$/, /^\d{5}-\d{4}-\d{2}$/];

var NDCUPCWithDashes = "NDC/UPC With Dashes";
var NDC = "NDC";
var UPC = "UPC ";

var PoilcyType =
    {
        AllProducts : 1,
        ProductsSpecifictoNDCUPC :2,
        ProductsSpecifictoNDCUPCandLotNumber:3
    }
var Compare = {
    Deleted: 'Deleted',
    OK: 'OK',
    Fails: 'Fails'

};

var UserRoles = {
    ADMN: 0,
    CUSR: 1,
    RUSR: 2,
    SUSR: 3
};

var profilenameecum = {
    Manufacturer: 1,
    ManufacturerGroup:2,
    Wholesaler: 3,
    WholesalerGroup: 4,
    Repackager: 5
}

var ReportNames = {

   ProductDetails: 'Product Details',
   WasteReport: 'Waste Report',
   PharmacySummary: 'Pharmacy Summary',
   NonCreditableSummary: 'Non Creditable Summary',
   ManufacturerSummary: 'Manufacturer Summary',
   ReturnToStock: 'Return To Stock'
}


