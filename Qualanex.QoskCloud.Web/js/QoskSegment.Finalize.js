﻿$(function ()
{
   $(document).on("click", "#printLabelButton", function (e)
   {
      var $div = $(this);
      $("#printLabelButton").prop('disabled', true);

      var options = {
         async: true,
         url: $div.attr("data-ql-action"),
         type: "POST",
         data: $("#policyForm").serialize() + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
         },
         success: function (data)
         {
            var $target = $($div.attr("data-ql-target"));
            var $newHtml = $(data);
            $("#printLabelButton").hide();
            $("#rePrintLabelButton").show();
            $target.replaceWith($newHtml);
         }
      };
      try
      {
         $.ajax(options);
      }
      catch (e)
      {
         throw e;
      }

      ga('send', 'event', 'Policy', 'PrintClick');
      return false;
   });

   var preparePrint = function ()
   {
      // select printer to print on
      // for simplicity sake just use the first LabelWriter printer
      var printers = dymo.label.framework.getPrinters();

      if (printers.length === 0)
      {
         $("#printLabelButton").hide();
         $("#printError").html("No DYMO printers are installed. Install DYMO printers.");
      }

      var printerName = "";

      for (var i = 0; i < printers.length; ++i)
      {
         var printer = printers[i];

         if (printer.printerType === "LabelWriterPrinter")
         {
            printerName = printer.name;
            break;
         }
      }

      if (printerName === "")
      {
         $("#printLabelButton").hide();
         $("#printError").html("No LabelWriter printers found. Install LabelWriter printer");
      }
      window.printerName = printerName;
   }
   window.preparePrint = preparePrint;
});