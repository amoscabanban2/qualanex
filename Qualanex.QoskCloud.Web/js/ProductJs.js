﻿$(document).ready(function () {
    $('#ProductList-Grid').find('a').each(function () {
        var cre = $(this).text();
        // alert(cre);

        if (cre == "Next") {
            // $(this).hide();
            var txtDesc = $("#txtDescription").val();
            $(this).attr("href", '../Product/Index?status=Next&txtDescription=' + txtDesc);
        }

        if (cre == "Prev") {
            // $(this).hide();

        }


    })
});

$(document).ready(function () {
    CreateSearchPanel();
    CreateGridColumnPanel();    
});


function CreateSearchPanel() {
    var Url1 = '../Product/CreateSearchPanel';
    $.ajax({
        url: Url1,
        data: {},
        success: function (data) {
            $("#SearchPanelList").html(data)
            ///Checking if Product List is already been searched by , then start assigning the value
            var rpreviouspage = document.referrer;
     
            if (sessionStorage.getItem("ProductSearchField") != null ) {

                var objSearchFieldValue = sessionStorage.getItem("ProductSearchField");
                SetSearchFieldValue(JSON.parse(objSearchFieldValue));
                ///Binding the Grid based on Previous search
                GetColumnAndBindintoGrid();
           }

            LoadColumnListingInPopup();
        },
        error: function (response) {
           // alert(response);
            if (response.status != 0) {
               // alert(response.status + " " + response.statusText);
            }
        }
    });
}
function SetSearchFieldValue(objSearchFieldValue) {

    for (var i = 0; i < objSearchFieldValue.length; i++) {
        var SearchFieldObject = objSearchFieldValue[i];

        var controlname = SearchFieldObject.Type + '-' + SearchFieldObject.Name;
        document.getElementsByName(controlname)[0].value = SearchFieldObject.Value1;
    }

}
function LoadColumnListingInPopup() {

    $.ajax({
        url: '../Product/CreateColumnListingPopUp',
        data: {},
        success: function (data) {
            $("#ColumnListingPopUp").html(data);
            BindCustomSearch();
        },
        error: function (response) {
          //  alert(response);
            if (response.status != 0) {
               // alert(response.status + " " + response.statusText);
            }
        }
    });
};


function CreateGridColumnPanel() {
    var Url1 = '../Product/BindColumnListing';
    $.ajax({
        url: Url1,
        data: {},
        success: function (data) {
            $("#CustomGridListing").html(data);
            BindCustomGrid();
        },
        error: function (response) {
           // alert(response);
            if (response.status != 0) {
              //  alert(response.status + " " + response.statusText);
            }
        }
    });
}





   



$(document).ready(function () {
    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });



    $("#btdeletenok").click(function () {

        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
        $.ajax({
            url: '../Product/DeleteProduct',
            data: { prodId: userId },
            success: function (data) {
                if (data.Result == 'OK') {
                    $("#alertmessageid").text(Product.DeleteSuccess);
                    $(".alert-popup-body-message").show();
                    //alert("Product Deleted");
                    GetColumnAndBindintoGrid();
                    //CreateGridPanel();
                }
                else if (data.Result == 'Deleted')
                {
                    $("#alertmessageid").text(Product.Delete);
                    $(".alert-popup-body-message").show();
                    GetColumnAndBindintoGrid();
                }
                else  {

                    $("#alertmessageid").text(Product.DeleteFailed);
                    $(".alert-popup-body-message").show();
                   
                }
               
            },
            error: function (response) {
                // alert(response);
                if (response.status != 0) {
                    //  alert(response.status + " " + response.statusText);
                }
            }
        });
    });

    
    

});



var userId = 0;
///function used when User go for Delete option, Then need to show confirmation message
function deleteselectedProduct(productID) {

    userId = productID;
    //if (userId > 0) {
    $("#alertdeletemessageid").text("Do you wish to delete this product ?");
    $(".alert-popup-body-message-confirm").show();
    //}

}


function SimpleSearch() {
   
    
    var ndc = $("#txtNDCUPC").val();
    var lot = $("#txtLotNumber").val();
    var des = $("#txtDescription").val();
    var recordsno = $("#SelectedRecords").val();
    $.ajax({
        type: "POST",

        url: '@Url.Action("Index", "Product")',
        data: { txtNDCUPC: ndc, txtLotNumber: lot, txtDescription: des, records: recordsno },
        //dataType: "json"

    }).done(function (data) {

        //$('#ProductList-Grid').html('');

    }).fail(function (response) {
       // alert(response);
    });
}


function Bindpopuplist() {

    $.ajax({
        type: "POST",

        url: '@Url.Action("GetColoumnDetails", "Product")',
        data: { Product: "product" },
        dataType: "json"

    }).done(function (data) {

        var dt = data.CustomSearch.split(',');
        var sel = $('#sel1');

        var dfltsrc = data.CustomGrid.split(',');
        var sel2 = $('#sel2');

        for (i = 0; i < dt.length; i++) {
            sel.append('<option draggable="true" ondragstart="drag(event)" id="drag_' + i + '">' + dt[i] + '</option>');
        }
        for (i = 0; i < dfltsrc.length; i++) {
            sel2.append('<option draggable="true" ondragstart="drag(event)" id="dragd_' + i + '">' + dfltsrc[i] + '</option>');
        }

    }).fail(function (response) {
        if (response.status != 0) {
           // alert(response.status + " " + response.statusText);
        }
    });
}


function BindpopupGridlist() {


    $.ajax({
        type: "POST",

        url: '@Url.Action("GetColoumnDetails", "Product")',
        data: { Product: "product" },
        dataType: "json"

    }).done(function (data) {

        var dt = data.CustomGrid.split(',');
        var sel = $('#customgrdid');

        for (i = 0; i < dt.length; i++) {
            sel.append('<option draggable="true" ondragstart="drag(event)" id="drag_' + i + '">' + dt[i] + '</option>');
        }


    }).fail(function (response) {
        if (response.status != 0) {
           // alert(response.status + " " + response.statusText);
        }
    });
}

function selectAllList() {

    var aSelect = $('#sel2');
   
    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
       // alert(option.value);
    });


    var aSelectLen = aSelect.length;
    for (i = 0; i < aSelectLen; i++) {
        aSelect.options[i].attr('selected', 'selected');
    }
}

function GetLeftPanelList() {
    //selectAllList();
    var optionValues = [];

    //$('#sel2').each(function () {
    //    alert($(this).val());
    //    optionValues.push($(this).val());
    //});
    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });



    // $('#result').html(optionValues);

    //var srchrslt = $('#sel2').val();
    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: '../Product/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "1" },
        //data: "{ 'SearchList': '" + $('#sel2').val() + "' }",
        dataType: "json"

    }).done(function (data) {
        CreateSearchPanel();
     


    }).fail(function (response) {
        if (response.status != 0) {
           // alert(response.status + " " + response.statusText);
        }
    });

}

function GetGridPanelList() {

    //selectAllList();
    var optionValues = [];

    //$('#sel2').each(function () {
    //    alert($(this).val());
    //    optionValues.push($(this).val());
    //});
    var options = $('#GridList2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });



    // $('#result').html(optionValues);

    //var srchrslt = $('#sel2').val();
    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: '../Product/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "2" },
        //data: "{ 'SearchList': '" + $('#sel2').val() + "' }",
        dataType: "json"

    }).done(function (data) {
        //LoadProductGrid();
        //alert(data);
        GetColumnAndBindintoGrid();
        //CreateGridPanel();  
        CreateGridColumnPanel();
        


    }).fail(function (response) {
        if (response.status != 0) {
          //  alert(response.status + " " + response.statusText);
        }
    });

}

function GetColumnAndBindintoGrid() {
    
    var LstofControlViewModel = new Array();
    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");

        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = "";

            var ElementId = $(this).attr("id");

            if (result == "checkbox") {
                value = document.getElementById(ElementId).checked;
            } else {

                value = $(this).val().trim();

            }

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value,
                "Label": value,
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }
            //alert(JSON.stringify(search));
            LstofControlViewModel.push(search);
        }
    });
    sessionStorage.setItem("ProductSearchField", JSON.stringify(LstofControlViewModel));
    $.ajax({
        //sync: false,
        type: "POST",
        data: { 'lstobjdsad': LstofControlViewModel },
        //contentType: 'application/json; charset=utf-8',
        url: '../Product/GetColumnsList',
        // Note it is important
        success: function (data) {
            CreateGridForServerSide((data));
           
            $("#exp_col").hide(1000);
            $("#expand").show();
            $("#collapse").hide();
        },
        error: function (response) {
            if (response.status != 0) {
            }
        }
    });

}