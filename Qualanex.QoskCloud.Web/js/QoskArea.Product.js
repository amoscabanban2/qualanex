﻿//*****************************************************************************
//*
//* QoskSegment.Product.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Javascript functionality for product area.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   // The following arrays are used to define which elements/tabs should be reloaded in specific cases
   window.reloadOnSearch = [];
   window.reloadOnClear = [];
   window.reloadOnSelection = [];

   //this trigger replaces the field separator in 2D barcodes with a pipe
   //since browser text fields do not retain the field separator 
   $(document).on("keypress", ".ndcField", function (event)
   {
      if (event.keyCode === 29 || event.which === 93)
      {
         $(this).val($(this).val() + "|");
      }
   });

   //mirror the values on the full and minimized forms
   $(document).on("change", ".ndcField", function (e)
   {
      $(".ndcMin")[0].value = $(".ndcField")[0].value;
   });

   //mirror the values on the full and minimized forms
   $(document).on("change", ".brandField", function (e)
   {
      if ($(".brandMin").length)
      {
         $(".brandMin")[0].value = $(".brandField")[0].value;
      }
   });

   //before the image on the carousel changes, store the zoomed image url in a temporary attribute
   //to disable zooming while dragging
   $(document).on("beforeChange", ".slick-product-slider", function ()
   {
      console.log("before change");

      $(".slick-product-image").each(function ()
      {
         $(this).attr("data-featherlight-temp", $(this).attr("data-featherlight"));
         $(this).attr("data-featherlight", "");
      });
   });

   //re-enable zoom after dragging has finished
   $(document).on("afterChange", ".slick-product-slider", function ()
   {
      console.log("after change");

      $(".slick-product-image").each(function ()
      {
         $(this).attr("data-featherlight", $(this).attr("data-featherlight-temp"));
      });
   });


   //mirror the values on the full and minimized forms
   $(document).on("keyup", ".mfgrMin", function (e)
   {
      $(".mfgrField")[0].value = $(".mfgrMin")[0].value;
   });

   //mirror the values on the full and minimized forms
   $(document).on("keyup", ".mfgrField", function (e)
   {
      if ($(".mfgrMin").length)
      {
         $(".mfgrMin")[0].value = $(".mfgrField")[0].value;
      }
   });

   //mirror the values on the full and minimized forms
   $(document).on("change", ".ndcMin", function (e)
   {
      $(".ndcField")[0].value = $(".ndcMin")[0].value;
   });

   //mirror the values on the full and minimized forms
   $(document).on("change", ".brandMin", function (e)
   {
      $(".brandField")[0].value = $(".brandMin")[0].value;
   });

   //when pressing the submit button, submit the form
   //unless data-disable attribute is true
   $(document).on("click", "#SubmitSearch", function ()
   {
      if ($(this).attr("data-disable") === "true")
      {
         return;
      }

      if ($("#productDetailForm").is(":visible"))
      {
         $("#productDetailForm").submit();
      }
      else if ($("#productDetailMinForm").is(":visible"))
      {
         $("#productDetailMinForm").submit();
      }
   });

   //toggle between list and image view when the action bar icons are clicked
   $(document).on("click", ".resultViewImg", function ()
   {
      $(".resultViewImg").each(function ()
      {
         $(this).attr("src", $(this).attr("data-inactive-src"));
      });

      var $view = $(this);
      $(".viewHidden")[0].value = $view.attr("data-value");
      $(".viewHidden")[1].value = $view.attr("data-value");
      $view.attr("src", $view.attr("data-active-src"));
   });

   //Performs a single ajax request that can update multiple elements simultaneously
   $(document).on("click", ".ql-product-multi", function (e)
   {
      var $div = $(this);
      $("#wait").show();

      //$div.effect("highlight");
      reloadOnClear.forEach(function (element, index, array)
      {
         element = element.toLowerCase();
         if ($("#" + element + "-loading").length)
         {
            $("#" + element + "-loading").show();
         }
      });

      var options = {
         async: true,
         url: $div.attr("data-ql-action"),
         type: $div.attr("data-ql-method"),
         data: $div.attr("data-ql-data") + getAntiForgeryToken(),
         error: function (err)
         {
            showError(err.statusText);
            $("#wait").hide();
         },
         success: function (data)
         {
            if (data === "")
            {
               LogOut(true);
            }

            if ($(".resultViewImg.listView").length)
            {
               $(".resultViewImg.listView").click();
            }

            var $newHtml = $(data);

            reloadOnClear.forEach(function (element, index, array)
            {
               var $target = $("#" + element);
               $target.replaceWith($newHtml.find("#" + element));

               if ($("#" + element + "-loading").length)
               {
                  $("#" + element + "-loading").hide();
               }

               //if ($("#" + element + "-tab").length) {
               //    $("#" + element + "-tab").effect("highlight");
               //}
               if ($div.attr("data-ql-tab"))
               {
                  $("#tab-container").easytabs("select", "#tabs-" + $div.attr("data-ql-tab"));
               }

               resizeTabs();
            });

            $("#wait").hide();
         }
      };

      try
      {
         $.ajax(options);
         ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);
         return false;
      }
      catch (e)
      {
         throw e;
      }
   });

   //If the URL query string includes a parameter named min with any value,
   //the form will minimize on page load
   var min = getParameterByName('min');

   if (min !== null)
   {
      $("#productDetailMinimal").show();
      $("#productDetailFull").hide();
   }
});