﻿

$(document).ready(function () {

    CreateSearchPanel();
    CreateGridColumnPanel();
});

function CreateSearchPanel() {

    var Url = '~/Report/CreateSearchPanel';
    $.ajax({
        url: Url,
        data: {},
        success: function (data) {
            $("#SearchPanelList").html(data)
            ///Checking if Product List is already been searched by , then start assigning the value
            var rpreviouspage = document.referrer;

            if (sessionStorage.getItem("ProductSearchField") != null) {

                var objSearchFieldValue = sessionStorage.getItem("ProductSearchField");
                SetSearchFieldValue(JSON.parse(objSearchFieldValue));
                ///Binding the Grid based on Previous search
                GetColumnAndBindintoGrid();
            }

            LoadColumnListingInPopup();
        },
        error: function (response) {
            // alert(response);
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
}

function CreateGridColumnPanel() {
    var Url1 = '../Report/BindColumnListing';
    $.ajax({
        url: Url1,
        data: {},
        success: function (data) {
            $("#CustomGridListing").html(data);
            BindCustomGrid();
        },
        error: function (response) {
            // alert(response);
            if (response.status != 0) {
                //  alert(response.status + " " + response.statusText);
            }
        }
    });
}

function LoadColumnListingInPopup() {

    $.ajax({
        url: '../Report/CreateColumnListingPopUp',
        data: {},
        success: function (data) {
            $("#ColumnListingPopUp").html(data);
            BindCustomSearch();
        },
        error: function (response) {
            //  alert(response);
            if (response.status != 0) {
                // alert(response.status + " " + response.statusText);
            }
        }
    });
};

function GetLeftPanelList() {
    //selectAllList();
    var optionValues = [];

    //$('#sel2').each(function () {
    //    alert($(this).val());
    //    optionValues.push($(this).val());
    //});
    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });



    // $('#result').html(optionValues);

    //var srchrslt = $('#sel2').val();
    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: '../Product/SaveCustomSearchDetailsByUserName',
        data: { SearchList: srchrslt.toString(), LayoutId: "1" },
        //data: "{ 'SearchList': '" + $('#sel2').val() + "' }",
        dataType: "json"

    }).done(function (data) {
        CreateSearchPanel();



    }).fail(function (response) {
        if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
        }
    });

}

function GetColumnAndBindintoGrid2() {

    var LstofControlViewModel = new Array();
    $("#ulCustomeFields :input").each(function () {
        var result = $(this).attr("type");
        var result2 = $(this).attr("name");

        if (result != "hidden" && result2 != undefined) {

            var AssotableName = $(this).attr("datamessage");
            var _datatype = $(this).attr("datatype");
            var _OperatorType = $(this).attr("OperatorType");

            var value = "";

            var ElementId = $(this).attr("id");

            if (result == "checkbox") {
                value = document.getElementById(ElementId).checked;
            } else {

                value = $(this).val().trim();

            }

            var search = {
                "Type": result2.split('-')[0],
                "Name": result2.split('-')[1],
                "Value1": value,
                "Label": value,
                "AssociateTableName": AssotableName,
                "DataType": _datatype,
                "OperatorType": _OperatorType,
            }
            LstofControlViewModel.push(search);
        }
    });

    sessionStorage.setItem("ProductSearchField", JSON.stringify(LstofControlViewModel));
    alert(LstofControlViewModel);


    //$.ajax({
    //    //sync: false,
    //    type: "POST",
    //    data: { 'lstobjdsad': LstofControlViewModel },
    //    //contentType: 'application/json; charset=utf-8',
    //    url: '../Product/GetColumnsList',
    //    // Note it is important
    //    success: function (data) {

    //        CreateGridForServerSide((data));

    //        $("#exp_col").hide(1000);
    //        $("#expand").show();
    //        $("#collapse").hide();
    //    },
    //    error: function (response) {
    //        if (response.status != 0) {
    //        }
    //    }
    //});

}