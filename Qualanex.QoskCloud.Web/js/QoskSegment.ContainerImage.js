﻿$(function()
{

   var segmentId = "containerImage";
   var segmentSelector = "#" + segmentId;
   var errorSpanSelector = segmentSelector + " .tdMessage .error";
   var warningSpanSelector = segmentSelector + " .tdMessage .warning";
   var presecriptionSelected = false;
   window.webcamsLoaded = false;
   var intervalHandle;

   //on error loading webcams or capturing,
   //display the error to the user
   var webcamError = function(message)
   {
      $("#webcamError").html(message);
      $("#imageCapture").attr("disabled", true);
      $("#imageReload").attr("disabled", false);
      //OverconstrainedError indicates that the deviceIds in the cookie
      //could not be found, this is resolved by simply reloading the cameras
      if (message.includes("OverconstrainedError"))
      {
         console.log(message);
         $("#imageReload").click();
      }
   }

   //this method is called when the segment is expanded, this is handled in QoskCommon.js
   var containerImageExpandMethod = function()
   {
      //wait until expansion animation completes, or cameras will fail
      setTimeout(function()
      {
         if ($("#sideCamera:visible").length || $("#topCamera:visible").length)
         {
            //when the webcam video elements are hidden, the stream is stopped,
            //once visible again, calling changeCamera will restart the stream
            changeCamera($("#cboSideCamera")[0]);
            //wait for the first camera to complete before restarting the second
            setTimeout(function()
            {
               changeCamera($("#cboTopCamera")[0]);
               setTimeout(validateCameraSetup, 200);
            }, 1000);
         }
      }, 200);

   }
   createAndExclusivePush("containerImageExpandMethods", containerImageExpandMethod);

   //When cameras are loaded, enable/disable the appropriate buttons
   var reenableCapture = function()
   {
      webcamsLoaded = true;
      $("#imageRetake").prop("disabled", true);
      setTimeout(function()
      {
         $("#imageReload").prop("disabled", false);
      }, 500);
      $("#imageCapture").prop("disabled", false);
      if (!presecriptionSelected)
      {
         $("#cboSideCamera").prop("disabled", false);
      }
      $("#cboTopCamera").prop("disabled", false);
   }

   //when images are captured, or cameras need to be reloaded,
   //set the flag and unload the webcams
   function doUnloadWebcams()
   {
      webcamsLoaded = false;
      unloadWebcams();
   }

   //set the flag and clear any error message
   function doLoadWebcams()
   {
      if ($(".webcamCapture").length)
      {
         $("#webcamError").html("");
         loadWebcams(webcamError, reenableCapture);
      }
   }

   window.LoadUpaCameras = doLoadWebcams;

   //An explicit call to set the camera drop downs from the cookie value
   function setCameraFromCookie(dropdown)
   {
      var isValidValue, valueChanged = false;
      var value = $.cookie(segmentId + dropdown);
      if (value !== undefined)
      {

         $("#" + dropdown + " option").each(function()
         {
            var cval = $(this).val();
            if ($(this).val() === value)
            {
               isValidValue = true;
               if ($(this).val() !== value)
               {
                  $("#" + dropdown).val(value);
                  $("#" + dropdown).change();
                  valueChanged = true;
               }
            }
         });
         if (!isValidValue)
         {
            $.removeCookie("ContainerImage.cboTopCamera");
         }
      }

      return valueChanged;
   }

   //Called after camera selections are created or changed to verify that two 
   //different streams are being used
   function validateCameraSetup()
   {
         $(errorSpanSelector).each(function ()
         {
            if (this.id !== "webcamError")
            {
               $(this).html("");
            }
         });

         if (isTooFewCameras())
         {
            $("#imageContainerError").html($("#tooFewCamerasErrorText").html());
            return failValidation(segmentId);
         }

         if (isSameCamera())
         {
            $("#imageContainerError").html($("#sameCameraErrorText").html());
            return failValidation(segmentId);
         }

         if ($("#couldNotSaveErrorText").is(":visible"))
         {
            return failValidation(segmentId);
         }
         return true;
   }

   window.validateCameraSetup = validateCameraSetup;

   //verify that the top and side cameras are not using the same stream
   function isSameCamera()
   {
      if (!presecriptionSelected && $("#cboSideCamera").val() === $("#cboTopCamera").val() && $("#cboTopCamera").val() !== null)
      {
         return true;
      }
      $("#imageContainerError").html("");
      return false;
   }

   //verify the correct number of cameras are present
   function isTooFewCameras()
   {
      if ($("#cboTopCamera option").length === 2)
      {
         return true;
      }
      $("#imageContainerError").html("");
      return false;
   }

   //check if images are captured succesfully 
   function isNoCapture()
   {
      if ((!presecriptionSelected && $(".SideImageUriHidden").val() === "") || $(".TopImageUriHidden").val() === "")
      {
         return true;
      }
      $("#imageContainerError").html("");
      return false;
   }

   //validation method called when preparing bin designation or policy disposition
   var validateContainerImage = function()
   {
      $(errorSpanSelector).each(function()
      {
         $(this).html("");
      });

      if (isNoCapture())
      {
         $("#imageContainerError").html($("#noCaptureErrorText").html());
         return failValidation(segmentId);
      }

      $(segmentSelector).attr("data-segment-complete", "true");
      removeSegmentIncompleteMessage(segmentId);

      if ($("#couldNotSaveErrorText").is(":visible"))
      {
         return failValidation(segmentId);
      }
      return true;
   }

   window.validateContainerImage = validateContainerImage;

   //method to unload webcams when collapsed
   var containerImageCollapse = function()
   {
      doUnloadWebcams();
   }
   //Not being pushed so that cameras remain loaded for the weightQuantityCondition segment
   //createAndExclusivePush("containerImageCollapseMethods", containerImageCollapse);

   $(document)
       .on("change",
           "input[type=radio].ContainerTypeRadio",
           function(e)
           {
              if (this.value.indexOf("Prescription") >= 0)
              {
                 presecriptionSelected = true;
                 $("#cboSideCamera").prop("disabled", true);
                 deleteImage($(".SideImageUriHidden").val());
                 $("#sideReview").hide();
                 $("#sideCamera").hide();
                 $("#sidePrescriptionImage").show();
                 $(".SideImageUriHidden").removeAttr("data-featherlight");
              } else
              {
                 presecriptionSelected = false;
                 $("#sidePrescriptionImage").hide();
                 if ($("#imageCapture").length && !$("#imageCapture").attr("disabled"))
                 {
                    $("#sideCamera").show();
                    $("#cboSideCamera").prop("disabled", false);
                    $("#imageCapture").prop("disabled", true);
                    $("#imageReload").prop("disabled", true);
                    doUnloadWebcams();
                    doLoadWebcams();
                    setTimeout(validateCameraSetup, 200);

                 }
              }
           });

   $(document).on("click", "#imageCapture", function ()
   {
      if (!validateCameraSetup())
      {
         return;
      }

      $("#imageContinue").prop("disabled", true);
      $("#imageCapture").prop("disabled", true);
      $("#imageReload").prop("disabled", true);
      $("#cboTopCamera").prop("disabled", true);
      $("#imageRetake").prop("disabled", false);
      var sideImgData;

      if (!presecriptionSelected)
      {
         $("#cboSideCamera").prop("disabled", true);
         sideImgData = capture($('#sideCamera')[0], function (sideUri, tempLink)
         {
            if (sideUri === "error" || sideUri === undefined)
            {
               $("#couldNotSaveErrorText").show();
            }
            else
            {
               $("#sideReview").attr("data-featherlight", tempLink);
               $(".SideImageUriHidden").val(sideUri);
            }
         });
      }

      var topImgData = capture($('#topCamera')[0], function (topUri, tempLink)
      {
         if (topUri === "error" || topUri === undefined)
         {
            $("#couldNotSaveErrorText").show();
         }
         else
         {
            $(".TopImageUriHidden").val(topUri);
            $("#topReview").attr("data-featherlight", tempLink);
         }
      });

      if ((presecriptionSelected || sideImgData !== undefined) && topImgData !== undefined)
      {
         doUnloadWebcams();
         if (!presecriptionSelected)
         {
            $("#sideCamera").hide();
            $("#sideReview").show();
            $(".SideImageUriHidden").val("loading");
            $("#sideReview").attr("src", sideImgData);
         }
         $("#topCamera").hide();
         $("#topReview").show();
         $(".TopImageUriHidden").val("loading");;
         $("#topReview").attr("src", topImgData);
         $(".webcamCapture").removeClass("webcamCapture");
         if ($("#weightQuantityCondition video").length)
         {

            $.cookie("cboContentCamera", $.cookie("cboTopCamera"));
            $("#weightQuantityCondition video").addClass("webcamCapture");
            $("#weightQuantityCondition .selectCamera").addClass("webcamSelect");
            loadWebcams(undefined, function() { window.webcamsLoaded = true; });
         }
      }

   });

   //Reset the container image segment so new images can be captured
   $(document).on("click", "#imageRetake", function ()
   {
      doUnloadWebcams();
      $("#couldNotSaveErrorText").hide();
      $(".webcamCapture").removeClass("webcamCapture");
      $(".webcamSelect").removeClass("webcamSelect");
      $("#containerImage video").addClass("webcamCapture");
      $("#containerImage .selectCamera").addClass("webcamSelect");

      if ($("#contentCamera").length)
      {
         $(".webcamCapture").each(function() { this.srcObject = null; });
      }

      $("#imageRetake").prop("disabled", true);
      //delete the previously captured images
      deleteImage($(".SideImageUriHidden").val());
      deleteImage($(".TopImageUriHidden").val());
      $(".SideImageUriHidden").removeAttr("data-featherlight");
      $(".TopImageUriHidden").removeAttr("data-featherlight");

      //don't load the side camera if a prescription vial is present for HIPAA protection
      if (!presecriptionSelected)
      {
         $(".SideImageUriHidden").val("");
         $("#sideReview").hide();
         $("#sideCamera").show();
      }

      $(".TopImageUriHidden").val("");
      $("#topReview").hide();
      $("#topCamera").show();
      doLoadWebcams();
      validateCameraSetup();
   });
   //handle image reload click if there is a problem loading the cameras
   $(document).on("click", "#imageReload", function()
   {
      $("#imageCapture").prop("disabled", true);
      $("#imageReload").prop("disabled", true);
      doUnloadWebcams();
      doLoadWebcams();
      setTimeout(validateCameraSetup, 200);
   });

   //Stop the continue button from being clicked until the camera has reinitialized 
   function cameraReloadWatcher()
   {
      intervalHandle = setInterval(function ()
      {
         if ($("#imageContinue").prop("disabled") && $("#cboTopCamera").val() !== null && $("#cboSideCamera").val() !== null)
         {
            $("#imageContinue").prop("disabled", false);
         }
      }, 200);
   }
   createAndExclusivePush("containerImageExpandMethods", cameraReloadWatcher);

   function cameraWatcherDestroyer()
   {
      if (intervalHandle)
      {
         clearInterval(intervalHandle);
      }
   }
   createAndExclusivePush("containerImageCollapseMethods", cameraWatcherDestroyer);
});