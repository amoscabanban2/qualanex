﻿$(function()
{
   //*****************************************************************************
   //*
   //* MessageBox.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the Messagebox partial class.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************

   $.fn.extend({

      //Show pop up
      showMessageBox: function(defaultTitle)
      {
         var heigrid = $(window).height();
         var popctrgrid = (heigrid - 435) / 2;
         $(".custom-messageBox").show();
         $(".custom-search-messageBox").show();
         $(".custom-search-messageBox").css({ "top": +popctrgrid + "px" });
         $(".gapping-inline").click(function()
         {
            $(".model-popup").show();
            $(".model-box").animate({ "top": "20%" });
         });
         $(this).addTitles(defaultTitle);
         $(this).showContent();

      },
      // cancel button function
      closeMessageBox: function()
      {
         $(".close-button-messageBox").css("display", "inline-block");
         $("#CustomMessageBoxVerification").css("display", "none");
         $(".custom-search-messageBox").hide();
         $(".model-popup,.custom-messageBox").hide();
         $('.custom-search-messageBox').html('');
         $('.custom-search-messageBox').html("<img class='close-button-messageBox' src='/images/SVG/Escape.svg' /><div id='CustomMessageBoxEntity' style='max-height:450px'></div>");
      },
      // first tab selecting content
      showContent: function()
      {
         if ($(".custom-messageBox li.active").length > 0)
         {
            $("#" + $(".custom-messageBox li.active").closest("li").attr("id")).trigger("click");
            $(this).activeTitle();
         }
         if (!$(".custom-search-messageBox li").hasClass("ql-tabs-popup"))
         {
            $(".CustomMessageBoxTitle").css("margin-top", "0px");
         }
      },
      // click on tabs
      activeTabs: function()
      {
         $(".ql-tabs-popup").removeClass("active");
         $(this).addClass("active");
         $(this).activeTitle();
      },
      // add content of each tab
      addToContent: function(data)
      {
         $("#CustomMessageBoxEntity").html(data);
      },
      // create error message
      showErrorMessage: function(errorData, BtnObjects)
      {
         if ($("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError").length === 0)
         {
            $("#" + $("div.custom-messageBox").attr("id") + " #CustomMessageBoxEntity")
               .after("<div id=" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError" + ">" +
            "<div id='" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Text" + "' style='float:left;margin-top:10px;margin-left:10px;font-size:20px'></div>" +
            "<div id='" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Button" + "' style='float:right;margin-top:10px;margin-right:10px'></div></div>");
         }
         if ($("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Text").length === 0)
         {
            $("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError").html(
            "<div id='" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Text" + "' style='float:left;margin-top:10px;margin-left:10px;font-size:20px'>" + errorData +
            "</div><div id='" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Button" + "' style='float:right;margin-top:10px;margin-right:10px'></div>"
         );
         }
         else
         {
            $("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Text").html(errorData);
         }

         $("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError")
            .css({
               "border-top": "2px solid #bbb",
               "color": "red",
               "overflow": "hidden",
               "height": "auto"
            });
         if (BtnObjects !== undefined)
         {
            $(this).addErrorButton(BtnObjects);
         }
      },
      // hide error message
      hideErrorMessage: function()
      {
         if ($("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError").length > 0)
         {
            $("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError").html('');
            $("#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError").css("border-top", "");
         }
      },
      addMessageButton: function(buttonObjects, messageObjects)
      {
         var divs = "<div>";
         if (messageObjects !== undefined)
         {
            for (var i = 0; i < messageObjects.length; i++)
            {
               divs += "<p id='" + messageObjects[i].id + "'";
               divs += (messageObjects[i].style === undefined) ? "" : "style='" + messageObjects[i].style + "'";
               divs += (messageObjects[i].class === undefined) ? "" : "class='" + messageObjects[i].class + "'";
               divs += ">";
               divs += messageObjects[i].name + " </p>";
            }
            divs += "</div>";
         }
         else
         {
            divs = "";
         }
         if (buttonObjects.length > 0)
         {
            divs += "<div style='text-align:center'>";
            for (i = 0; i < buttonObjects.length; i++)
            {
               divs += "<input type='button' id='" + buttonObjects[i].id + "' value='" + buttonObjects[i].name + "' ";
               divs += (buttonObjects[i].class === undefined) ? "" : "class='" + buttonObjects[i].class + "'";
               divs += (buttonObjects[i].function === undefined) ? "" : buttonObjects[i].function;
               divs += (buttonObjects[i].style === undefined) ? "" : "style='" + buttonObjects[i].style + "'";
               divs += "/>";
            }
            divs += "</div>";
         }
         else if (divs === "<div>" || divs === "")
         {
            return false;
         }
         $(this).addToContent(divs);
      },
      addTabs: function(tabObjects)
      {
         if (tabObjects.length > 0)
         {
            var divs = "<ul class='etabs row' style='margin-top:-39px;margin-left:10px;'>";
            for (var i = 0; i < tabObjects.length; i++)
            {
               divs += "<li id='" + tabObjects[i].id + "'";
               divs += (tabObjects[i].active) ? "class='ql-tabs-popup active'" : "class='ql-tabs-popup'";
               divs += ">";
               divs += tabObjects[i].name + "</li>";
            }
            divs += "</ul>";
            $(".close-button-messageBox").after(divs);
         }
      },
      addTitles: function(titleObjects)
      {
         if (titleObjects !== undefined)
         {
            if (titleObjects.length === 0)
            {
               return false;
            }
            var div = "";
            for (var i = 0; i < titleObjects.length; i++)
            {
               div += "<div class='CustomMessageBoxTitle'";
               div += titleObjects[i].id === undefined ? ">" : "data-field-name='" + titleObjects[i].id + "'>";
               div += "<h3 style='color:#fff'>" + titleObjects[i].title + "</h3>";
               div += "</div>";
            }
            if (!$(".custom-search-messageBox div").hasClass("CustomMessageBoxTitle"))
            {
               $("#CustomMessageBoxEntity").before(div);
            }
            else
            {
               var $target = $(".CustomMessageBoxTitle");
               var $newHtml = div;
               $target.replaceWith($newHtml);
            }
         }
      },
      activeTitle: function()
      {
         if ($(".CustomMessageBoxTitle").length > 1)
         {
            $(".CustomMessageBoxTitle").hide();
            $(".CustomMessageBoxTitle[data-field-name='" + $("li.ql-tabs-popup.active").attr("id") + "']").show();
         }
      },
      addErrorButton: function(btnObject)
      {
         if ($("div#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Button").length > 0)
         {
            var btn = "";
            for (var i = 0; i < btnObject.length; i++)
            {
               btn += "<input type='button' id='" + btnObject[i].id +
                  "' class='ql-policy-continue buttonEx' style='width: 85px; height: 35px;' value='"
                  + btnObject[i].name + "'";
               btn += (btnObject[i].function === undefined || btnObject[i].function === "") ? "/>" : btnObject[i].function + "/>";
            }
            $("div#" + $("div.custom-messageBox").attr("id") + "_CustomMessageBoxError_Button").html(btn);
         }
      },
      messageBoxStatus: function()
      {
         return $(".custom-messageBox").css("display") === "block" ?
            true
            : false;
      },
      setErrorMessage: function(message, titles, buttonName)
      {
         messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>" + message + "</h3>"
         }];
         btnObjects = [{
            "id": "btn_Continue",
            "name": buttonName,
            "class": "btnErrorMessage",
            "style": "margin:0 auto;text-align:center",
            "function": "onclick='$(this).closeMessageBox();'"
         }];

         titleobjects = [{ "title": titles }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
      },
      setVerificationMessageBox: function(messsage, titles, buttons, cancleButton, focusButton)
      {
         if (cancleButton)
         {
            $(".close-button-messageBox").css("display", "none");
         }
         $("#CustomMessageBoxEntity").css("display", "none");
         $("#CustomMessageBoxVerification").css("display", "inline-block");
         $(this).addVerificationMessagesButtons(buttons, focusButton === undefined ? "" : focusButton, " <h3 style='margin:0 auto;text-align:center;width:100%'>" + messsage + "</h3>");
         $(this).showMessageBox([{ "title": titles }]);
      },
      addVerificationMessagesButtons: function(buttonObjects, focusButton, messageObjects, notes)
      {
         if ($(".custom-search-messageBox div#CustomMessageBoxVerification").length === 0)
         {
            var newHtml = "<div id='CustomMessageBoxVerification' style='overflow:auto;max-height:450px;width:100%'><input type='hidden' id='VerificationHiddenNotes' />";
            newHtml += "<div class='messages'></div><div class='textboxes QoskLogin' style='background-image:none;width:30%;margin:0px auto'>";
            newHtml += "<table style=''><tr><td class='txEx' style='padding-bottom: 4px;'>";
            newHtml += "<input class='inputEx' data-val='true' data-val-length='The field User Name must be a string with a maximum length of 20.' placeholder='Username' data-val-length-max='20' data-val-required='User Name is required' maxlength='20' id='UserName' minlength='4' tabindex='1'/></td><td rowspan='2' style='padding-left: 7px;' tabindex='3'>";
            newHtml += "</td></tr><tr><td class='txEx' style='padding-bottom: 4px;'><input class='inputEx' placeholder='Password'";
            newHtml += "tabindex='2' type='text' style='-webkit-text-security: disc' autocomplete='off' id='Password' nextfocus='" + focusButton + "' /></td></tr><tr><td colspan='2' class='tdEx QoskError'";
            newHtml += "id='alertMessageID'>&nbsp;</td></tr></table></div><div class='buttons' style='width:100%;margin:0px auto;text-align:center'></div></div>";
            $(".custom-search-messageBox").append(newHtml);
         }
         else
         {
            $("div#CustomMessageBoxVerification input#Password").attr("nextfocus", focusButton);
            $("#CustomMessageBoxVerification .textboxes input#UserName").val("");
            $("#CustomMessageBoxVerification .textboxes input#Password").val("");
         }
         $("#VerificationHiddenNotes").val(notes);
         $("#CustomMessageBoxVerification .messages").html(messageObjects);

         if (buttonObjects.length > 0)
         {
            divsButtons = "";// $("#CustomMessageBoxVerification .buttons").html();
            for (i = 0; i < buttonObjects.length; i++)
            {
               divsButtons += "<input type='button' id='" + buttonObjects[i].id + "' value='" + buttonObjects[i].name + "' ";
               divsButtons += (buttonObjects[i].class === undefined) ? "" : "class='" + buttonObjects[i].class + "'";
               divsButtons += (buttonObjects[i].function === undefined) ? "" : buttonObjects[i].function;
               divsButtons += (buttonObjects[i].style === undefined) ? "style='margin-right:5px;margin-left:5px;'" : "style='margin-right:5px;margin-left:5px;" + buttonObjects[i].style + "'";
               divsButtons += "/>";
            }
            $("#CustomMessageBoxVerification .buttons").append(divsButtons);
         }         
      },
      checkForValidation(notes)
      {
         if ($("#CustomMessageBoxVerification .textboxes input#UserName").val() === "")
         {
            $(this).showErrorMessage("Please enter the username.");
            $("#CustomMessageBoxVerification .textboxes input#UserName").focus();
            return [{ Success: false, UserName: "", Error: "" }];
         }
         else if ($("#CustomMessageBoxVerification .textboxes input#Password").val() === "")
         {
            $(this).showErrorMessage("Please enter the password.");
            $("#CustomMessageBoxVerification .textboxes input#Password").focus();
            return [{ Success: false, UserName: "", Error: "" }];
         }
         $(this).showErrorMessage("");
         var jqXhr = $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            async: false,
            data: "userName=" + $("#CustomMessageBoxVerification .textboxes input#UserName").val()
               + "&password=" + $("#CustomMessageBoxVerification .textboxes input#Password").val()
               + "&notes=" + notes
               + getAntiForgeryToken(),
            url: "/MessageBox/CheckUsernameAndPassword"
         });
         if (jqXhr.responseJSON.Error.length > 0 && !jqXhr.responseJSON.Success)
         {
            $(this).showErrorMessage(jqXhr.responseJSON.Error);
            $("#CustomMessageBoxVerification .textboxes input").val("");
            $("#CustomMessageBoxVerification .textboxes input#UserName").focus();
         }

         return jqXhr.responseJSON;
      }
   });

   $(document).on("keypress", "#CustomMessageBoxVerification .textboxes input#Password", function(e)
   {
      if (e.keyCode === 13)
      {
         if ($(this).attr("nextfocus").length > 0)
         {
            $("#CustomMessageBoxVerification #" + $(this).attr("nextfocus")).click();
         }
      }
   });

   $(document).on("click",
    "li.ql-tabs-popup",
    function() { $(this).activeTabs(); $(this).hideErrorMessage(); });

   $(document).on("click",
      ".close-button-messageBox, .cancel, #btnSubmit",
      function()
      {
         $(this).closeMessageBox();
      });
   $(document).on("click",
      "#btnValidationSubmit",
      function()
      {
         alert($(this).checkForValidation("Test").Success);
      });
});