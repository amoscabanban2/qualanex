//*****************************************************************************
//*
//* QoskForm.js
//*    Copyright (c) 2017 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   var editMode;
   var formChanged;

   //#############################################################################
   // Global functions
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get/Set the edit mode of the form.
   //*
   //* Parameters:
   //*   value - True if the form is editable, otherwise false.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.getEditMode = function ()
   {
      return editMode;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.setEditMode = function (mode)
   {
      console.log("window.setEditMode(" + mode + ")");

      editMode = mode;

      // Set each of the input field(s)
      $.merge($(".formMax input"), $(".formMax textarea")).each(function ()
      {
         if ($(this).hasClass("editable"))
         {
            $(this).prop("readonly", !editMode);
         }
      });

      // Set each of the select field(s)
      $(".formMax select").each(function ()
      {
         if ($(this).hasClass("editable"))
         {
            $(this).prop("disabled", !editMode);
         }
      });

      // Clear/Set associated edit flag(s)
      setFormChanged(false);

      // Clear/Set associated Actionbar control(s)
      window.enableActionbarControl("#CMD_NEW", !editMode);
      window.enableActionbarControl("#CMD_EDIT", !editMode);
      window.enableActionbarControl("#CMD_SAVE", editMode);
      window.enableActionbarControl("#CMD_CANCEL", editMode);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   value - True if the form has changed, otherwise false.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   window.getFormChanged = function ()
   {
      return formChanged;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.setFormChanged = function (value)
   {
      formChanged = value;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Sets or determines the lock status of a control.
   //*
   //* Parameters:
   //*   ctrlID - Element ID.
   //*   lock   - [optional] Lock status to set.
   //*
   //* Returns:
   //*   True when the 'lock' parameter is omitted and the control is locked;
   //*   otherwise, false;
   //*
   //*****************************************************************************
   var lockCtrl = function (ctrlID, lock)
   {
      var ctrlObj = window.$("#" + ctrlID);

      if (ctrlObj !== "undefined")
      {
         if (lock !== undefined)
         {
            if (lock)
            {
               ctrlObj.addClass("locked");
            }
            else
            {
               ctrlObj.removeClass("locked");
            }
         }
         else
         {
            return ctrlObj.hasClass("locked");
         }
      }

      return false;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.lockCtrl = lockCtrl;

   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(".formOptn").click(function (e)
   {
      $(".formOptn").each(function ()
      {
         var isDiv = $(this).attr("id") === e.target.id;
         $(this).attr("src", $(this).attr("src").replace(isDiv ? "(0)" : "(1)", isDiv ? "(1)" : "(0)"));
      });

      var divID = "optn_" + e.target.id;

      $(".formOption").each(function ()
      {
         $(this).css("display", $(this).attr("id") === divID ? "block" : "none");
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(".editable").on("input", function ()
   {
      console.log("change");

      if (getEditMode())
      {
         setFormChanged(true);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(".editable").on("change", function ()
   {
      console.log("change");

      if (getEditMode())
      {
         setFormChanged(true);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(".scannable").on("keypress", function ()
   {
      console.log(".scannable::keypress");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $("select").on("change", function ()
   {
      console.log("change");

      if (getEditMode())
      {
         setFormChanged(true);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $("input").focusin(function (e)
   {
      if (getEditMode())
      {
         $.each($(e.target).attr("class").split(/\s+/), function (index, value)
         {
            if (value.match("^ECS_PHONE"))
            {
               $(e.target).val($(e.target).val().replace(/[^0-9]/g, ""));
               $(e.target).attr("maxlength", "10");
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $("input").focusout(function (e)
   {
      if (getEditMode())
      {
         $.each($(e.target).attr("class").split(/\s+/), function (index, value)
         {
            if (value.match("^ECS_PHONE") && /[0-9]{10}/.test($(e.target).val()))
            {
               $(e.target).removeAttr("maxlength");
               $(e.target).val("(" + $(e.target).val().substring(0, 3) + ") " + $(e.target).val().substring(3, 6) + "-" + $(e.target).val().substring(6, 10));
            }
         });
      }
   });

});
