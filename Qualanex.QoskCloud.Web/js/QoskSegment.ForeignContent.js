﻿//*****************************************************************************
//*
//* QoskSegment.ForeignContent.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function()
{

   const segmentId = "foreignContent";

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function validateForeignContent()
   {
      console.log("foreign content validation is running");
      return window.failValidation(segmentId);

      return true;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.validateForeignContent = validateForeignContent;


});
