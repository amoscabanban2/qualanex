﻿
    $(document).ready(function () {

        $("#SelectedPolicyType").change(function () {

            var name = $("#SelectedPolicyType").val();
            if (name == 'Custom') {
                $(".cus-policy-name").show();
                $("#select-profile-group-type").css("display", "block");
                $("#radio-list-manufacture-wholesaler-repackager").css("display", "none");
                $("#manufacture-profile-group").css("display", "block");
                $("#wholesaler-profile-group").css("display", "block");
                //prddivbutton
            }
            else if (name == 'Published') {

                $(".cus-policy-name").hide();
                $("#select-profile-group-type").css("display", "none");
                $("#pharmacy-list-grd").css("display", "none");
                $("#radio-list-manufacture-wholesaler-repackager").css("display", "block");
                $("#manufacture-profile-group").css("display", "none");
                $("#wholesaler-profile-group").css("display", "none");
                $("#prddivbutton").css("visibility", "hidden");
                $("#product-grd-manufacturebased").css("display", "none");
                $("#getBlotbyproductid_button").css("visibility", "hidden");
                $("#lot-grd-productebased").css("visibility", "hidden");
            }
            else {
                $(".cus-policy-name").hide();
                $("#select-profile-group-type").css("display", "none");
                $("#pharmacy-list-grd").css("display", "none");
                $("#radio-list-manufacture-wholesaler-repackager").css("display", "none");
                $("#manufacture-profile-group").css("display", "none");
                $("#wholesaler-profile-group").css("display", "none");
                $("#prddivbutton").css("visibility", "hidden");
                $("#product-grd-manufacturebased").css("display", "none");
                $("#getBlotbyproductid_button").css("visibility", "hidden");
                $("#lot-grd-productebased").css("visibility", "hidden");
            }
        });

        $("#cus-pol").click(function () {

            $(".cus-policy-name").show();
            $("#select-profile-group-type").css("visibility", "visible");
        });
        $("#pub-pol").click(function () {
            $(".cus-policy-name").hide();
            $("#select-profile-group-type").css("visibility", "hidden");
            $("#pharmacy-list-grd").css("display", "none");
            $("#radio-list-manufacture-wholesaler-repackager").css("display", "none");
            $("#product-grd-manufacturebased").css("display", "none");
            $("#getBlotbyproductid_button").css("visibility", "hidden");
            $("#lot-grd-productebased").css("visibility", "hidden");
        });
        $("#profile-type").click(function () {
            $(".cus-profile-type").show();
            $(".cus-group-type").hide();

        });
        $("#group-type").click(function () {
            $(".cus-profile-type").hide();
            $(".cus-group-type").show();
        });
        $('.cus-group-type option, .cus-profile-type option').click(function () {
            $(".cus-sel-table").show();
        });
        $("#manufacture-profile").click(function () {
            $(".cus-manufa").show();
            $(".cus-manufa2").hide();
            $(".cus-manufa3").hide();
            $(".cus-manufa4").hide();
            $(".cus-manufa5").hide();
        });
        $("#manufacture-profile-group").click(function () {
            $(".cus-manufa").hide();
            $(".cus-manufa2").show();
            $(".cus-manufa3").hide();
            $(".cus-manufa4").hide();
            $(".cus-manufa5").hide();
        });
        $("#wholesaler-profile").click(function () {
            $(".cus-manufa").hide();
            $(".cus-manufa2").hide();
            $(".cus-manufa3").show();
            $(".cus-manufa4").hide();
            $(".cus-manufa5").hide();
        });
        $("#wholesaler-profile-group").click(function () {
            $(".cus-manufa").hide();
            $(".cus-manufa2").hide();
            $(".cus-manufa3").hide();
            $(".cus-manufa4").show();
            $(".cus-manufa5").hide();
        });
        $("#repackager-profile").click(function () {
            $(".cus-manufa").hide();
            $(".cus-manufa2").hide();
            $(".cus-manufa3").hide();
            $(".cus-manufa4").hide();
            $(".cus-manufa5").show();
        });
        $('.cus-manufa option, .cus-manufa2 option, .cus-manufa3 option, .cus-manufa4 option, .cus-manufa5 option').click(function () {
            $(".cus-pro-table").show();
        });
        $(".lot-btn-table input[type='button']").click(function () {
            $(".cus-prolot-table").show();
        });
        $(".selecctall").click(function () {

            var myCheckBox = $($(this).parent().parent().parent()).find("input[type='checkbox']")
            if ($(this).attr('class') != 'selecctall') {
                $(this).parent().parent().parent().children().eq(0).children().eq(0).children().eq(0).prop("checked", false);
            } else {
                myCheckBox.prop("checked", this.checked);
            }
        });
        $(".selecctallgrd").click(function () {

            var myCheckBox = $($(this).parent().parent().parent().parent()).find("input[type='checkbox']")
            if ($(this).attr('class') != 'selecctallgrd') {
                $(this).parent().parent().parent().parent().children().eq(0).children().eq(0).children().eq(0).prop("checked", false);
            } else {
                myCheckBox.prop("checked", this.checked);
            }
        });

        $("#profile-type, #group-type, #manufacture-profile").click(function () {
            var listsel = $(".chk-container").height();
            if (listsel > 80) {
                $(".chk-container").css({ "height": "80px", "overflow-y": "scroll" });
            }
        });
        $(".collpse-policy2").click(function () {
            $(".collpse2").slideToggle();
            $(".collpse1").slideUp();
        });
        $(".collpse-policy1").click(function () {
            $(".collpse1").slideToggle();
            $(".collpse2").slideUp();
        });
        $('#tab-container').easytabs();
        $('#tab-container1').easytabs();


        $('#getprofiledetails_button').click(function () {
          
            var SelectionType;
            var checkedprofile = $('.checkbox1:checked').map(function () {
                SelectionType = "ProfileType"
                return this.value;
            }).get();

            if (checkedprofile.length < 1) {
                checkedprofile = $('.checkboxgroup-type:checked').map(function () {
                    SelectionType = "GroupType"
                    return this.value;
                }).get();
            }


            if (checkedprofile.length > 0 || checkedprofiledetails.length>0) {
                $("#radio-list-manufacture-wholesaler-repackager").css("display", "block");
                $("#prddivbutton").css("visibility", "visible");
            }
            else { return false; }

            $.ajax({
                url: '@Url.Action("PharmacyDetailsBasedonProfileAndGroupType")',
                data: { checkedprofile: checkedprofile.toString(), SelectionType: SelectionType },
                success: function (data) {
                    $("#partialPharmacyListGrd").html(data)

                    $("#pharmacy-list-grd").css("display", "block");
                },
                error: function (response) {
                    alert(response);
                    if (response.status != 0) {
                        alert(response.status + " " + response.statusText);
                    }
                }
            });

        });


        $('#getprofiledetails_button1').click(function () {


            var checkedprofile = $('.checkbox1:checked').map(function () {
                return this.value;
            }).get();



            if (checkedprofile.length > 0) {
                $("#pharmacy-list-grd").css("display", "block");
                //$("#pharmacy-list-grd").css("display", "block");
                $("#radio-list-manufacture-wholesaler-repackager").css("display", "block");
                $("#prddivbutton").css("visibility", "visible");
                //prddivbutton
            }
            else { $("#pharmacy-list-grd").css("display", "none"); $("#radio-list-manufacture-wholesaler-repackager").css("display", "none"); $("#prddivbutton").css("visibility", "hidden"); return false; }
            var url = '@Url.Content("~/Policy/BindProfileDetailsBasedOnSelectedProfile")';
            $.getJSON(url, { checkedValues: checkedprofile.toString() }, function (data) {

                //code to bind table
                $.each(data, function (i, item) {

                    var html = "<tr><td><input type=\"checkbox\" class=\"profiledetails-class\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" />";
                    + "</td>";
                    html += "<td>" + item.Name + "</td>";
                    html += "<td>" + item.Address1 + "</td>";
                    html += "<td>" + item.City + "</td>";
                    html += "<td>" + item.State + "</td>";
                    // and html += other fields...
                    $("#grd1 tr:last").after(html);
                    //  $("#grd1").css(style = "overflow-y:scroll; height:100px");
                    // the above line is like that because you use <tbody>
                    // in table definition.
                });
            });

        });


        $('input[type="radio"]').click(function () {
            if ($(this).attr("id") == "manufacture-profile") {
                var url = '@Url.Content("~/Policy/BindManufactureRepackagerWholesaler")';
                $.getJSON(url, { Ptype: 'Manufacturer' }, function (data) {

                    var html = "<ul class=\"chk-container\">";
                    $.each(data, function (i, item) {
                        html += "<ul><li><label><input type=\"checkbox\" class=\"manfacturecheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" /><i></i>" + item.Name + "</label></li>"
                    });
                    html += "</ul>";
                    $("#manufacure-check-list").html(html);

                });

            }
            if ($(this).attr("id") == "wholesaler-profile") {
                var url = '@Url.Content("~/Policy/BindManufactureRepackagerWholesaler")';
                $.getJSON(url, { Ptype: 'Wholesaler' }, function (data) {

                    var html = "<ul class=\"chk-container\">";
                    $.each(data, function (i, item) {
                        html += "<ul><li><label><input type=\"checkbox\" class=\"wholesalercheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" /><i></i>" + item.Name + "</label></li>"
                    });
                    html += "</ul>";
                    $("#wholesaler-check-list").html(html);

                });

            }
            if ($(this).attr("id") == "repackager-profile") {
                var url = '@Url.Content("~/Policy/BindManufactureRepackagerWholesaler")';
                $.getJSON(url, { Ptype: 'Repackager' }, function (data) {

                    var html = "<ul class=\"chk-container\">";
                    $.each(data, function (i, item) {
                        html += "<ul><li><label><input type=\"checkbox\" class=\"repackagercheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" /><i></i>" + item.Name + "</label></li>"
                    });
                    html += "</ul>";
                    $("#repackager-check-list").html(html);

                });
            }
            if ($(this).attr("id") == "wholesaler-profile-group") {
                var url = '@Url.Content("~/Policy/BindManufactureRepackagerWholesaler")';
                $.getJSON(url, { Ptype: 'WholesalerGroup' }, function (data) {

                    var html = "<ul class=\"chk-container\">";
                    $.each(data, function (i, item) {
                        html += "<ul><li><label><input type=\"checkbox\" class=\"wholesalergroupcheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" /><i></i>" + item.Name + "</label></li>"
                    });
                    html += "</ul>";
                    $("#wholesaler-group-check-list").html(html);

                });
            }
            if ($(this).attr("id") == "manufacture-profile-group") {
                var url = '@Url.Content("~/Policy/BindManufactureRepackagerWholesaler")';
                $.getJSON(url, { Ptype: 'ManufactureGroup' }, function (data) {

                    var html = "<ul class=\"chk-container\">";
                    $.each(data, function (i, item) {
                        html += "<ul><li><label><input type=\"checkbox\" class=\"manufacturegroupcheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProfileCode + "\" /><i></i>" + item.Name + "</label></li>"
                    });
                    html += "</ul>";
                    $("#manufacture-group-check-list").html(html);

                });
            }

        });

        //});
        $('#getproductdetails_button1').click(function () {

            var checkedprofile = $('.manfacturecheckclass:checked').map(function () {
                return this.value;
            }).get();

            var checkedgroupprofile = $('.manfacturegroupcheckclass:checked').map(function () {
                return this.value;
            }).get();

            var checkedprofiledetails = $('.profiledetails-class:checked').map(function () {
                return this.value;
            }).get();

            var checkedProductWithLot = $('.productcheckclass:checked').map(function () {
                return this.value;
            }).get();

            if ((checkedprofile.length > 0 || checkedgroupprofile.length > 0) && checkedprofiledetails.length > 0) {

                //$("#radio-list-manufacture-wholesaler-repackager").css("visibility", "visible");

            }
            else {
                $("#product-grd-manufacturebased").css("display", "none");
                $("#getBlotbyproductid_button").css("visibility", "hidden");
                alert("Please select manufacture profile and profile details from grid");
                //$("#radio-list-manufacture-wholesaler-repackager").css("visibility", "hidden");
                return false;
            }
            @*url: '@Url.Action("GetProductWithLotDetails", "Policy")',*@
            @*var url = '@Url.Content("~/Policy/BindProductDetailsBasedonSelectedmanufacturer")';*@
            $("#product-grd-manufacturebased").css("visibility", "visible");
            var url = '@Url.Content("~/Policy/GetProductWithLotDetails")';
            $("#product-grd-manufacturebased").css("visibility", "visible");
            $.getJSON(url, { checkedValues: checkedprofile.toString(), checkedProductWithLot: checkedProductWithLot.toString() }, function (data) {

                $("#product-grd-manufacturebased").css("visibility", "visible");
                //code to bind table
                $.each(data, function (i, item) {
                    $("#product-grd-manufacturebased").css("visibility", "visible");
                    $("#getBlotbyproductid_button").css("visibility", "visible");
                    var html = "<tr><td><input type=\"checkbox\" class=\"productcheckclass\" id=\"cb_" + i + "\" value=\"" + item.ProductID + "\" />";
                    + "</td>";
                    html += "<td>" + item.NDCUPCWithDashes + "</td>";
                    html += "<td>" + item.Description + "</td>";
                    html += "<td>" + item.Strength + "</td>";
                    html += "<td>" + item.DosageCode + "</td>";
                    html += "<td>" + item.ControlNumber + "</td>";
                    // and html += other fields...
                    $("#productgrd tr:last").after(html);
                    // the above line is like that because you use <tbody>
                    // in table definition.
                });
            });

        });

        $('#getBlotbyproductid_button').click(function () {

            var checkedprofile = $('.productcheckclass:checked').map(function () {
                return this.value;
            }).get();

            if (checkedprofile.length > 0) {

                //$("#radio-list-manufacture-wholesaler-repackager").css("visibility", "visible");

            }
            else {
                $("#lot-grd-productebased").css("visibility", "hidden");
                alert("Please select manufacture profile and profile details from grid");
                //$("#radio-list-manufacture-wholesaler-repackager").css("visibility", "hidden");
                return false;
            }

            var url = '@Url.Content("~/Policy/BindLotBasedOnSelectedProduct")';
            $.getJSON(url, { checkedProductIDValues: checkedprofile.toString() }, function (data) {

                //code to bind table
                $.each(data, function (i, item) {
                    $("#lot-grd-productebased").css("visibility", "visible");
                    var html = "<tr><td><input type=\"checkbox\" id=\"cb_" + i + "\" value=\"" + item.ProductID + "\" />";
                    + "</td>";
                    html += "<td>" + item.NDCUPCWithDashes + "</td>";
                    html += "<td>" + item.LotNumber + "</td>";
                    html += "<td>" + item.ManufactureRepackagerName + "</td>";
                    // and html += other fields...
                    $("#lotbasedonproductgrd tr:last").after(html);
                    // the above line is like that because you use <tbody>
                    // in table definition.
                });
            });

        });


        $('#btnPolicyInsert').click(function () {
          
            var checkedProductWithLot;
            var checkedSelectAllProduct;
            var check = $("#cbSelectAll").prop("checked");
            if (check)
            {
                checkedSelectAllProduct = $('#cbSelectAlllot:checked').map(function () { return this.value; }).get();
                checkedProductWithLot = $('.check-box:checked').map(function () {
                    return this.value;
                }).get();
            }
            else
            {
                checkedSelectAllProduct = $('#cbSelectAlllot:checked').map(function () { return this.value; }).get();
                checkedProductWithLot = $('.lotchecked-class:checked').map(function () {
                    return this.value;
                }).get();

                //if (checkedSelectAllProduct != '' && checkedProductWithLot != '')
                //{
                //    checkedProductWithLot = checkedSelectAllProduct + "," + checkedProductWithLot
                //}
                //else if (checkedSelectAllProduct != '' && checkedProductWithLot != '')
                //{
                //    checkedProductWithLot = checkedSelectAllProduct + "," + checkedProductWithLot
                //}

            }

            var checkedprofile = $('.manfacturecheckclass:checked').map(function () {
                return this.value;
            }).get();

            var wholesalercheckedprofile = $('.wholesalercheckclass:checked').map(function () {
                return this.value;
            }).get();

            var repckagercheckedprofile = $('.repackagercheckclass:checked').map(function () {
                return this.value;
            }).get();

            var checkedprofiledetails = $('.profiledetails-class:checked').map(function () {
                return this.value;
            }).get();

            if (checkedprofiledetails.length > 0) {

            }
            else {
                checkedprofiledetails = $('.checkboxgroup-type:checked').map(function () {
                    return this.value;
                }).get();
            }



            var WMRProfileCode;
            var ProfileType;
            if (checkedprofile != '')
            {
                WMRProfileCode = checkedprofile;
                ProfileType = 'Manufacturer';
            }
            if (wholesalercheckedprofile != '') {
                WMRProfileCode = wholesalercheckedprofile;
                ProfileType = 'Wholesaler';
            }

            if (repckagercheckedprofile != '') {
                WMRProfileCode = repckagercheckedprofile;
                ProfileType = 'Repackager';
            }
            var requestData =
              {
                  //Returnable Product Rules
                  PolicyName: $('#PolicyName').val(),
                  NumberOfMonthsAfterExpirationReturnable: $('#NumberOfMonthsAfterExpirationReturnable').val(),
                  NumberOfMonthsBeforeExpirationReturnableSealed: $('#NumberOfMonthsBeforeExpirationReturnableSealed').val(),
                  NumberOfMonthsBeforeExpirationReturnableOpened: $('#NumberOfMonthsBeforeExpirationReturnableOpened').val(),
                  ExpiresOnFirstOfCurrentMonth: $('#ExpiresOnFirstOfCurrentMonth').val(),
                  OverstockApprovalRequired: $("input[name=OverstockApprovalRequired ]:checked").val(),
                  ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
                  BrokenSealReturnable: $("input[name=BrokenSealReturnable]:checked").val(),
                  PrescriptionVialsReturnable: $("input[name=PrescriptionVialsReturnable]:checked").val(),
                  RepackagedIntoUnitDoseReturnable: $("input[name=RepackagedIntoUnitDoseReturnable]:checked").val(),
                  MissingLotNumberorExpDateNotReturnable: $("input[name=MissingLotNumberorExpDateNotReturnable]:checked").val(),
                  PartialsReturnable: $("input[name=PartialsReturnable]:checked").val(),
                  SelectedPolicyType: $('#SelectedPolicyType').val(),

                  //Pricing Rules
                  HandlingFee: $('#HandlingFee').val(),
                  UseDebitPricing: $('#UseDebitPricing').val(),
                  HandlingFeeExceptionForStatePartialPolicyLaw: $('#HandlingFeeExceptionForStatePartialPolicyLaw').val(),
                  AllowQuantityOverPackageSize: $("input[name=AllowQuantityOverPackageSize]:checked").val(),
                  DoNotUseFallThroughPrice: $("input[name=DoNotUseFallThroughPrice]:checked").val(),
                  UseWholeSalerDirectPriceWhenNoIndirect: $("input[name=UseWholeSalerDirectPriceWhenNoIndirect]:checked").val(),
                  AuthorizedForCreditConsideration: $('#AuthorizedForCreditConsideration').val(),
                  //Event Rules

                  PriorReturnAuthorizationRequired: $("input[name=PriorReturnAuthorizationRequired]:checked").val(),
                  DoNotUseFallThroughPrice: $("input[name=OverstockApprovalRequired]:checked").val(),
                  ReturnAuthorizationCompletionPercent: $('#ReturnAuthorizationCompletionPercent').val()

              };

            $.ajax({
                type: "POST",
                url: '@Url.Action("SavePolicyDetails","Policy")',
                data: { 'requestData': requestData, 'checkedprofiledetails': checkedprofiledetails.toString(), 'checkedprofile': checkedprofile.toString(), 'checkedProductWithLot': checkedProductWithLot.toString(), 'wholesalercheckedprofile': wholesalercheckedprofile.toString(), 'checkedSelectAllProduct': checkedSelectAllProduct.toString(), 'WMProfileType': ProfileType, 'WMRProfileCode': WMRProfileCode.toString() },
                datatype: "json"

            }).done(function (data) {
                //Successfully pass to server and get response
                if (data.result = "OK") { alert("Success"); }
            }).fail(function (response) {

            });

        });


        $('#btnPolicyUpdate').click(function () {

            var requestData =
              {
                  //Returnable Product Rules
                  PolicyID:$('#PolicyID').val(),
                  PolicyName: $('#PolicyName').val(),
                  NumberOfMonthsAfterExpirationReturnable: $('#NumberOfMonthsAfterExpirationReturnable').val(),
                  NumberOfMonthsBeforeExpirationReturnableSealed: $('#NumberOfMonthsBeforeExpirationReturnableSealed').val(),
                  NumberOfMonthsBeforeExpirationReturnableOpened: $('#NumberOfMonthsBeforeExpirationReturnableOpened').val(),
                  ExpiresOnFirstOfCurrentMonth: $('#ExpiresOnFirstOfCurrentMonth').val(),
                  OverstockApprovalRequired: $("input[name=OverstockApprovalRequired ]:checked").val(),
                  ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
                  BrokenSealReturnable: $("input[name=BrokenSealReturnable]:checked").val(),
                  PrescriptionVialsReturnable: $("input[name=PrescriptionVialsReturnable]:checked").val(),
                  RepackagedIntoUnitDoseReturnable: $("input[name=RepackagedIntoUnitDoseReturnable]:checked").val(),
                  MissingLotNumberorExpDateNotReturnable: $("input[name=MissingLotNumberorExpDateNotReturnable]:checked").val(),
                  PartialsReturnable: $("input[name=PartialsReturnable]:checked").val(),
                  SelectedPolicyType: $('#SelectedPolicyType').val(),

                  //Pricing Rules
                  HandlingFee: $('#HandlingFee').val(),
                  UseDebitPricing: $('#UseDebitPricing').val(),
                  HandlingFeeExceptionForStatePartialPolicyLaw: $('#HandlingFeeExceptionForStatePartialPolicyLaw').val(),
                  AllowQuantityOverPackageSize: $("input[name=AllowQuantityOverPackageSize]:checked").val(),
                  DoNotUseFallThroughPrice: $("input[name=DoNotUseFallThroughPrice]:checked").val(),
                  UseWholeSalerDirectPriceWhenNoIndirect: $("input[name=UseWholeSalerDirectPriceWhenNoIndirect]:checked").val(),
                  AuthorizedForCreditConsideration: $('#AuthorizedForCreditConsideration').val(),
                  //Event Rules

                  PriorReturnAuthorizationRequired: $("input[name=PriorReturnAuthorizationRequired]:checked").val(),
                  DoNotUseFallThroughPrice: $("input[name=OverstockApprovalRequired]:checked").val(),
                  ReturnAuthorizationCompletionPercent: $('#ReturnAuthorizationCompletionPercent').val()

              };

            $.ajax({
                type: "POST",
                url: '@Url.Action("UpdatePolicyDetails", "Policy")',
                data: { 'requestData': requestData },
                datatype: "json"

            }).done(function (data) {
                //Successfully pass to server and get response
                if (data.result = "OK") { alert("Update Success"); }
            }).fail(function (response) {

            });

        });


        $('#getproductdetails_button').click(function () {


            var checkedManufactureprofile = $('.manfacturecheckclass:checked').map(function () {
                return this.value;
            }).get();

            $.ajax({
                url: '@Url.Action("ProductsLotDetailsBasedOnSelectedManufacture")',
                data: { checkedManufactureprofile: checkedManufactureprofile.toString() },
                success: function (data) {
                    $("#partialProductListGrd").html(data)
                    $("#product-grd-manufacturebased").css("display", "block");
                },
                error: function (response) {
                    alert(response);
                    if (response.status != 0) {
                        alert(response.status + " " + response.statusText);
                    }
                }
            });

        });

        $('#getproductdetails_button2').click(function ()
        {

            var checkedProductWithLot = $('.lotchecked-class:checked').map(function () {
                return this.value;
            }).get();

            var checkedprofile = $('.manfacturecheckclass:checked').map(function () {
                return this.value;
            }).get();

            var checkedprofiledetails = $('.profiledetails-class:checked').map(function () {
                return this.value;
            }).get();

            $.ajax({
                type: "POST",
                url: '@Url.Action("SavePolicyDetails", "Policy")',
                data: { 'checkedprofiledetails': checkedprofiledetails.toString(), 'checkedprofile': checkedprofile.toString(), 'checkedProductWithLot': checkedProductWithLot.toString() },
                datatype: "json"

            }).done(function (data) {
                //Successfully pass to server and get response
                if (data.result = "OK") { }
            }).fail(function (response) {

            });
        });

        $(".next-click").click(function () {
            $(this).parent().parent().next().slideDown();
            $(this).parent().parent().hide();
            $(this).parent().parent().parent().prev().children().removeClass('active');
            $(this).parent().parent().parent().prev().children().children().removeClass('active');
            var aa = $(this).parent().parent().attr('id');
            var a1 = "a[href=#" + aa + "]";
            $(this).parent().parent().parent().prev().children().find(a1).parent().next().addClass('active').show();
            $(this).parent().parent().parent().prev().children().find(a1).parent().next().children().addClass('active').removeClass('disable-click');
        });


    });

function GoStepTwo() {

    //$("#qu-tab1").hide();
    //$(".tb1 li:nth-child(2)").show();
    ////$(".tb1 li:nth-child(1)").css("display", "none");
    //    $("#qu-tab2").slideDown();
    //    $(".tb1 li:first-child").removeClass('active');
    //    $(".tb1 li:first-child a").removeClass('active');
    //    $(".tb1 li:nth-child(2)").addClass('active');
    //    $(".tb1 li:nth-child(2) a").addClass('active');
}
