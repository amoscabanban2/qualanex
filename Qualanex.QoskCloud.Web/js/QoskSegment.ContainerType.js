﻿$(function ()
{
   var setContainerType = function (e, selection)
   {
      changeSelection(selection, "div");

      if (selection.value !== "")
      {
         $("#containerType").attr("data-segment-complete", "true");
         removeSegmentIncompleteMessage("containerType");
      }

      var $segments = $(".panel-collapse");
      var nextSegment = $segments[$segments.index($("#containerType")) + 1].id;
      $("#lotDate").find("input, label").enable();

      if (nextSegment === "lotDate" && $(".IsRecallPossible").val() === "False")
      {
         if (["Prescription|Open", "None|Loose", "UnitDose|Open"].includes($(".selected .ContainerTypeRadio").val()) || isForeignContainer())
         {
            $("#btnNoLot").click();
            $("#btnNoExp").click();

            //this is so that the segments will be disabled when we force the NA lot
            //we witnessed users entering lots for Prescription Vials by resetting the lot fields
            $("#lotDate").find("input, label").disable();
         }
         else
         {
            //allows us to reset the lot/exp date fields when we switch the container type
            var lotNr = $(".txtLotNo").val();
            var expMnth = $(".cboExpMonth").val();
            var expYr = $(".txtExpYear").val();

            $("#btnReset").click();
            $(".txtLotNo").val(lotNr);
            $(".cboExpMonth").val(expMnth);
            $(".txtExpYear").val(expYr);

            //these triggers take care of normal workflow in the lot expiration segment (header titles, continue button, etc.)
            if (lotNr !== "")
            {
               $(".txtLotNo").change();
            }

            if (expMnth !== "")
            {
               $(".cboExpMonth").change();
            }

            if (expYr !== "")
            {
               $(".txtExpYear").change();
            }
         }

         if (window[$("#lotDate").attr("data-segment-validation")]())
         {
            $("#lotDate").slideCollapse();
            nextSegment = $segments[$segments.index($("#lotDate")) + 1].id;
            changeNextSegment($("#containerType"), nextSegment);
         }
         else
         {
            $("#lotDate .QnexCaption.error").hide();
            changeNextSegment($("#containerType"), "lotDate");
         }
      }

      if (selection.value.includes("Sealed") || selection.value.includes("Case"))
      {
         //disable the sealed/case quantities for warehouse induction - OK for DSS
         if ($("#binDesignation").length)
         {
            $("#ID_SEALED").disable();
            $("#ID_CASECOUNT").disable();
         }

         $("#CNTRCDTN_FOREIGN").parent().hide();
      }
      else
      {
         $("#CNTRCDTN_FOREIGN").parent().show();
      }

      //for an original container, show the Prescription Label condition button
      if (selection.value.includes("Original") && !selection.value.includes("Case"))
      {
         $("#prescriptionLabel").show();
      }
      else
      {
         $("#prescriptionLabel").hide();
      }

      //set the inner NDC in the Container Type segment header if we're processing an inner product
      var innerNdc = $(".scroll-container-type").attr("inner-ndc");
      $("#ContainerTypeSelection").html("- " + $(selection).attr("data-ql-label") + (innerNdc !== undefined ? "<span style=\"color: red\"> (INNER NDC " + innerNdc + ")</span>" : ""));
      showQuantity(selection.value);

      //input event in QoskSegment.QuantityWeightCondition.js takes care of the case size and quantity
      //however, we first have to run showQuantity() in order for the ID_CASECOUNT field to be filled properly
      if (selection.value.includes("Case") && $("#ID_CASESIZE").val() !== "")
      {
         $("#ID_CASESIZE").trigger("input");
      }

      //determines if we have a predetermined container type (whether it's an inner or a case quantity from 2D barcodes)
      //disable container type segment if it was auto-populated
      var predeterminedContrType = $(".scroll-container-type").attr("contr-type") !== undefined && $(".scroll-container-type").attr("contr-type") === "true";

      if (predeterminedContrType)
      {
         $(".QoskSegment #containerType .scroll-container-type :input").attr("disabled", true);
         var slickIndex = parseInt($(selection).closest(".ContainerTypeRadio").attr("data-index"));

         //without setTimeout, the scroll doesn't get triggered
         setTimeout(function ()
         {
            $("#containerType .scroll-container-type").slick("slickGoTo", slickIndex);
         }, 150);
      }

      $(".ContainerTypeHidden").val(selection.value);
      submitPolicy(e, selection);
      ga('send', 'event', 'Policy', 'ContainerTypeSelected', $(selection).attr("data-ql-label"));
   }
   window.setContainerType = setContainerType;

   $(document).on("change", "input[type=radio].ContainerTypeRadio", function (e)
   {
      setContainerType(e, this);
   });

   $(document).on("click", ".scroll-container-type .selected label", function (e)
   {
      //for some reason, this gets fired even if the segment is disabled (unlike the change event above)
      //during warehouse induction, form will only be disabled when we process an item with a pre-determined container type
      if ($(".scroll-container-type :input").attr("disabled") !== undefined && $(".scroll-container-type :input").attr("disabled") === "disabled")
      {
         return false;
      }

      setContainerType(e, $(this).find("input[type=radio]")[0]);
   });
});