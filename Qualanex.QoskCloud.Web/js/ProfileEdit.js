﻿function SetAccountDetails() {
    $("#AccountgrpType").hide();
    $("#btnUpdateAccount").hide();
}

$(window).scroll(function () {
    var noofRecords = ($("#hdnTotalRecords").val());
    if (parseInt($("#hdnPageNo").val()) < noofRecords) {
        if (parseInt($("#hdnPageNo").val()) < noofRecords) {
            if ($(window).scrollTop() == $(document).outerHeight() - $(window).outerHeight()) {
               
            }
        }

    }
});

function NumericOnly(obj) {
    if (obj.value.length > 0) {

        obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}

$(document).ready(function () {
    BindEditUpdateMode();
    BindValidations();

    BindValuesInData("div.main-container");
    $(".show-pagesize-cmb").hide();
    $('#tab-container').easytabs();
    closePopup();
    //$("#btnok,.alert-close").click(function () {
    //    $(".alert-popup-body-message").hide();
    //    $(".alert-popup").animate({ "top": "40%" });
    //});

    $(".custom-checkbox input[type='checkbox']").after("<i></i>");
    $(".alert-popup-body").hide();

    $('#DEAExpirationDate').datepicker({
        minDate: 0
    });
    $('#VendorLoadDate').datepicker({
        minDate: 0
    });
    $("#hdnPageNo").val(0);
    if ($("#hdnTotalRecords").val() > 0) {
        var Pagesizeee = parseInt($("#hdnPageNo").val());
        var pagetext = "1-" + Pagesizeee + " of " + $("#hdnTotalRecords").val();
        $(".grid-details").text(pagetext);
        $(".grid-details").show();
    }

    $('.grid-btn-prev').hide();
    $('.grid-btn-next').hide();
    $(".grid-details").hide();

    $("#ProfileDesdiv").find("input,select,textarea").attr("disabled", "disabled");
    $("#ProfileContactdiv").find("input,select,textarea").attr("disabled", "disabled");
    $("#ProfileOther1div").find("input,select,textarea").attr("disabled", "disabled");
    $("#ProfileOther2div").find("input,select,textarea").attr("disabled", "disabled");

    // validate signup form on keyup and submit
    $("#frmProfileDesdiv").validationEngine();
    $("#frmProfileContactdiv").validationEngine();
    $("#frmProfileOther1div").validationEngine();

    $('#ProfileDesUpdate').click(function (e) {
       
        if (!$("#frmProfileDesdiv").validationEngine('validate')) {
            e.preventDefault();
            return false;
        }

        var _this = this;

        var requestData =
              {
                  ProfileCode: $('#ProfileCode').val(),
                  Name: $('#ProfieName').val(),
                  Type: $('#Type').val(),
                  DEANumber: $('#DEANumber').val(),
                  DEAExpirationDate: $('#DEAExpirationDate').val(),
                  EmailAddress: $('#EmailAddress').val(),
                  Address1: $('#Address1').val(),
                  Address2: $('#Address2').val(),
                  Address3: $('#Address3').val(),
                  ProfileGroupID: $('#ProfileGroupID').val(),
                  RegionCode: $('#RegionCode').val(),
                  Version: $('#Version').val()
              };

        if (!IsChangedValue('#' + $(_this).attr('frmdiv'))) {
            // do if there is no change

            return false;
        }

        var url = '../Profile/UpdateProfileFirstSection';
        $.ajax({
            type: "POST",
            url: url,
            data: requestData,
            dataType: "json"
        }).done(function (data) {

            if (data.VersionChanged) {
                //var returnResult = data.Updatedresult;
                //alert(JSON.stringify(data.Updatedresult))
                $(document).data('returnResult', data.Updatedresult);

                $("#alertmessageid").html(Product.VersionChange);
                $('#Version').val(data.Updatedresult.objResponse.Version);

                $("#btnok").bind("click", function () {

                    var NewResultobject = $(document).data('returnResult');
                    var NewResult = NewResultobject.objResponse;
                    lockingFeature(NewResult);
                    $("#btnok").unbind("click");
                    closePopup();
                })

            }
            else if (data.Result == 'Deleted') {
                $("#alertmessageid").html(Product.Delete);

            }
            else {
                $('#Version').val(data.UpdatedVersion);
                $("#alertmessageid").html(data.Result.toString());

                $('.highlitebordercss').removeClass("highlitebordercss");

            }
            $(".alert-popup-body-message").show();



            //Successfully pass to server and get response                   
            //$("#Version").val(data.UpdatedVersion);
            //$("#alertmessageid").text(data.Result);
            //$(".alert-popup-body-message").show();

            HideUpdate(_this);

        }).fail(function (response) {

            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        });
    });

    $('#ProfileContactUpdateId').click(function (e) {
       if (!$("#frmProfileContactdiv").validationEngine('validate')) {
            e.preventDefault();
            return false;
        }

        var _this = this;

        var LstofControlViewModel = new Array();
        LstofControlViewModel.push('ProfileCode');
        LstofControlViewModel.push('City');
        LstofControlViewModel.push('State');
        LstofControlViewModel.push('ZipCode');
        LstofControlViewModel.push('ContactPhoneNumber');


        var requestData =
              {
                  ProfileCode: $('#ProfileCode').val(),
                  City: $('#City').val(),
                  State: $('#State').val(),
                  ZipCode: $('#ZipCode').val(),
                  ContactFirstName: $('#ContactFirstName').val(),
                  ContactLastName: $('#ContactLastName').val(),
                  ContactPhoneNumber: $('#ContactPhoneNumber').val(),
                  ContactFaxNumber: $('#ContactFaxNumber').val(),
                  ContactEmailAddress: $('#ContactEmailAddress').val(),
                  PhoneNumber: $('#PhoneNumber').val(),
                  Version: $('#Version').val()
              };

        if (!IsChangedValue('#' + $(_this).attr('frmdiv'))) {
            // do if there is no change

            return false;
        }
        var url = '../Profile/UpdateProfileSecondSection';


        $.ajax({
            type: "POST",
            url: url,
            data: requestData,
            dataType: "json",

            beforeSubmit: function () {
                //$('#contact').validate({  });

                return $('#frmEditCreateProfile').valid();
            },
        }).done(function (data) {
            //Successfully pass to server and get response
            if (data.VersionChanged) {
                //var returnResult = data.Updatedresult;
                //alert(JSON.stringify(data.Updatedresult))
                $(document).data('returnResult', data.Updatedresult);

                $("#alertmessageid").text(Product.VersionChange);
                $('#Version').val(data.Updatedresult.objResponse.Version);

                $("#btnok").bind("click", function () {

                    var NewResultobject = $(document).data('returnResult');
                    var NewResult = NewResultobject.objResponse;

                    lockingFeature(NewResult);
                    $("#btnok").unbind("click");
                    closePopup();
                })

            }
            else if (data.Result == 'Deleted') {
                $("#alertmessageid").text(Product.Delete);

            }
            else {
                $('#Version').val(data.UpdatedVersion);
                $("#alertmessageid").text(data.Result.toString());
                $('.highlitebordercss').removeClass("highlitebordercss");
            }
            $(".alert-popup-body-message").show();
            HideUpdate(_this);

        }).fail(function (response) {

            var dictionary = response.responseText;
            data = $.parseJSON(dictionary);
            var result = '';
            $.each(data, function (i, item) {
                jQuery.each(LstofControlViewModel, function (index, value) {
                    if (item.key === value)
                        result = result + item.errors;
                });
            });

            $("#alertmessageid").text(result);
            $(".alert-popup-body-message").show();

        });
    });


    $('#ProfileOther1UpdateId').click(function (e) {
       if (!$("#frmProfileOther1div").validationEngine('validate')) {
            e.preventDefault();
            return false;
        }
        var _this = this;
        var requestData =
          {
              ProfileCode: $('#ProfileCode').val(),
              RollupProfileCode: $('#RollupProfileCode').val(),
              TollFreePhoneNumber: $('#TollFreePhoneNumber').val(),
              Notes: $('#Notes').val(),
              RepresentativeNumber: $('#RepresentativeNumber').val(),
              WholesalerAccountNumber: $('#WholesalerAccountNumber').val(),
              FaxNumber: $('#FaxNumber').val(),
              FaxNumber: $('#FaxNumber').val(),
              DirectAccountsOnly: $('#DirectAccountsOnly').val(),
              DisplayOrder: $('#DisplayOrder').val(),
              LotNumberExpirationDateValidationMethod: $('#LotNumberExpirationDateValidationMethod').val(),
              Version: $('#Version').val()
          };

        if (!IsChangedValue('#' + $(_this).attr('frmdiv'))) {
            // do if there is no change

            return false;
        }

        var url = '../Profile/UpdateProfileThirdSection';
        $.ajax({
            type: "POST",
            url: url,
            data: requestData,
            dataType: "json"
        }).done(function (data) {
            //Successfully pass to server and get response
            if (data.VersionChanged) {
                //var returnResult = data.Updatedresult;
                //alert(JSON.stringify(data.Updatedresult))
                $(document).data('returnResult', data.Updatedresult);

                $("#alertmessageid").text(Product.VersionChange);
                $('#Version').val(data.Updatedresult.objResponse.Version);

                $("#btnok").bind("click", function () {

                    var NewResultobject = $(document).data('returnResult');
                    var NewResult = NewResultobject.objResponse;

                    lockingFeature(NewResult);

                    //if ($("#measureTypeid").val() != NewResult.UnitOfMeasure) {
                    //    $("#measureTypeid").val(NewResult.UnitOfMeasure).css(highlitebordercss);
                    //}


                    $("#btnok").unbind("click");
                    closePopup();
                })

            }
            else if (data.Result == 'Deleted') {
                $("#alertmessageid").text(Product.Delete);

            }
            else {
                $('#Version').val(data.UpdatedVersion);
                $("#alertmessageid").text(data.Result.toString());
                $('.highlitebordercss').removeClass("highlitebordercss");
            }
            $(".alert-popup-body-message").show();
            HideUpdate(_this);

        }).fail(function (response) {
            //alert("Fail");
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        });
    });
  

});



