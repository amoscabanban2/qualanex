﻿//*****************************************************************************
//*
//* QoskSegment.Notifications.js
//*    Copyright (c) 2016 - 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Javascript functionality for notifications tab.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************
$(function()
{
   //initialize the productnotifications datatable
   var initializeRtsTable = function()
   {

      window.dtHeaders("#Notifications #productNotifications", "order: [7, 'asc']", [], []);
   }
   window.initializeRtsTable = initializeRtsTable;


   //filters the product notifications based on the filters selected on the segment header
   var filterReturnToStock = function()
   {
      //destroy the datatable if it's been drawn already
      try
      {
         $("#productNotifications").DataTable().destroy();
      }
      catch (e) { }
      initializeRtsTable();
      window.dtHeaders("#Notifications #productNotifications", "order: [7, 'asc']", [], []);
      //build the regex based on the selected filters
      var regex = "(Actionable";
      if (!$("#ReturnedToggle").attr("src").includes("Inactive")) //inactive includes active, so we search for the inverse
      {
         regex += "|Returned";
      }
      if (!$("#DispensedToggle").attr("src").includes("Inactive"))
      {
         regex += "|Dispensed";
      }
      if (!$("#RemovedToggle").attr("src").includes("Inactive"))
      {
         regex += "|DuplicateRemoved";
      }
      if (!$("#TransferedToggle").attr("src").includes("Inactive"))
      {
         regex += "|StoreTransfer";
      }
      if (!$("#PendingToggle").attr("src").includes("Inactive"))
      {
         regex += "|Pending";
      }
      regex += ")";
      $("#productNotifications").DataTable().column(0).search(regex, true).draw();
   }
   window.filterReturnToStock = filterReturnToStock;


   //initialize the datatable for recall notifications
   var initializeRecallTable = function()
   {
      
      window.dtHeaders("#recallBody #recalls", "order: [0, 'desc']", [], []);
   }
   window.initializeRecallTable = initializeRecallTable;

   //filter recalls based on the selections in the segment header
   var filterRecalls = function()
   {
      try
      {
         $("#recalls").DataTable().destroy();
      }
      catch (e) { }
      initializeRecallTable();
      window.dtHeaders("#recallBody #recalls", "order: [0, 'desc']", [], []);
      var regex = "(ActiveRecall";
      if (!$("#AcknowledgedToggle").attr("src").includes("Inactive"))
      {
         regex += "|Acknowledged";
      }
      if (!$("#InactiveToggle").attr("src").includes("Inactive"))
      {
         regex += "|Pending";
      }
      regex += ")";
      $("#recalls").DataTable().column(0).search(regex, true).draw();
   }
   window.filterRecalls = filterRecalls;

   //handle selecting a recall lot dropdown value
   $(document).on("change", ".ql-dropdown-lot", function(e)
   {

      var $div = $(this).parents(".ql-div-noclick:first");;
      changeSelection(this);
      $div.attr("data-ql-data", $div.attr("data-ql-data") + "&" + $(this).val());
      selectionAjax($div);

      ga('send', 'event', 'Selection', 'DropDownSelect', $(this).attr("name") || this.id);
      return false;

   });

   //toggle between active and inactive filter controls
   $(document).on("click", 
      ".segmentControls img",
      function()
      {
         toggleImage(this);
      });

   //handle acknowledgement of a recall
   $(document).on("click", ".acknowledgeCheck", function () {
      $(this).removeClass("acknowledgeCheck");
      $(this).removeClass("Pending");
      $(this).addClass("Acknowledged");
      $(this).parents("tr:first").removeClass("unacknowledged");
      $(this).parents("tr:first").addClass("selectable");
      $(this).parents("tr:first").find(".recallState").html("Acknowledged");
      $(this).attr("src", "/Areas/Product/Images/NotificationActions/BoxChecked.svg");
   });

   $(document).on("click", ".segmentControls.rtsFilter img", filterReturnToStock);
   $(document).on("click", ".segmentControls.recallFilter img", filterRecalls);


   //remove the detail row from a recall
   function removeExpandedRow($row)
   {
      if ($row === null)
      {
         $row = $(this);
      }
      $row.find("div:first").slideUp("fast",
         function()
         {
            $row.remove();
         });

   }

   //expand or colapse a group of notifications
   $(document).on("click",
      ".notificationGroup",
      function()
      {
         var $expandedRow = $(".notificationExpansionRow[data-groupguid=" + $(this).attr("data-groupguid") + "]:visible");
         if ($expandedRow.length)
         {
            removeExpandedRow($expandedRow);
            return;
         }
         var $rows = $($(this).parents(".dataTable").DataTable().rows().nodes());
         var i = $rows.index(this);
         $rows.eq(i).after($(this).find(".notificationExpansionRow").parent().html());
         $(".notificationExpansionRow[data-groupguid=" + $(this).attr("data-groupguid") + "]:visible div:first").slideDown();
      });

   //select a notification on click
   $(document).on("click", ".notificationExpansionRow .selectable",
      function()
      {
         if ($(this).hasClass("selected") && !$(this).parents(".notificationExpansionRow:first").hasClass("updatePending"))
         {
            $(this).removeClass("selected");
            return;
         }
         changeSelection(this);
      });

   //perform selected notification status updates on commit button click
   $(document).on("click",
      ".notificationExpansionRow .commitBtn",
      function()
      {
         var $notificationRow = $(this).parents(".notificationExpansionRow");
         if ($notificationRow.hasClass("updatePending"))
         {
            return false;
         } else
         {
            $notificationRow.addClass("updatePending");
         }
         var returnIds = [], dispenseIds = [], transferIds = [], removeIds = [], undoIds = [];
         if ($(this).attr("data-value") === "ReturnAltered")
         {
            returnIds.push(parseInt($(this).attr("data-unique-selection")));
         } else
         {
            $(this).parents(".notificationExpansionRow:first").find(".selected").each(function()
            {
               switch ($(this).attr("data-value"))
               {
                  case "Returned":
                     returnIds.push(parseInt($(this).attr("data-unique-selection")));
                     break;
                  case "Dispensed":
                     dispenseIds.push(parseInt($(this).attr("data-unique-selection")));
                     break;
                  case "StoreTransfer":
                     transferIds.push(parseInt($(this).attr("data-unique-selection")));
                     break;
                  case "DuplicateRemoved":
                     removeIds.push(parseInt($(this).attr("data-unique-selection")));
                     break;
                  case "Pending":
                     undoIds.push(parseInt($(this).attr("data-unique-selection")));
                     break;
               }
            });
         }
         if (returnIds.length || dispenseIds.length || transferIds.length || removeIds.length || undoIds.length)
         {
            var options = {
               async: true,
               url: "UpdateNotificationStatus",
               type: "POST",
               data: "&productid=" + $(this).parents(".notificationExpansionRow:first").attr("data-productid") + "&returnIds=" + returnIds
                  + "&dispenseIds=" + dispenseIds + "&transferIds=" + transferIds + "&removeIds=" + removeIds + "&undoIds=" + undoIds + getAntiForgeryToken(),
               error: function(err)
               {
                  showError(err.statusText);
               },
               success: function(pageData)
               {
                  var updateIds = undoIds.concat(dispenseIds).concat(returnIds).concat(removeIds).concat(transferIds);
                  updateIds.forEach(function(notificationId)
                  {
                     //Replace the detail row with the new one from the server
                     $("[data-unique-selection=" + notificationId + "]").parent().replaceWith($(pageData).find("[data-unique-selection=" + notificationId + "]:first").parent());

                     //Replace the status from the group row with the new one from the server
                     $("[data-unique-selection=" + notificationId + "]").parents(".notificationGroup").find("td:first")
                        .replaceWith($(pageData).find("[data-unique-selection=" + notificationId + "]")
                           .parents(".notificationGroup").find("td:first"));
                  });
                  $notificationRow.removeClass("updatePending");
               }
            };
            if (dispenseIds.length || transferIds.length || removeIds.length || undoIds.length || returnIds.length)
            {
               try
               {
                  if (returnIds.length)
                  {
                     selectionAjax(null,
                        "&productid=" +
                        $(this).parents(".notificationExpansionRow:first").attr("data-productid") +
                        "&notificationIds=" +
                        returnIds,
                        "POST");
                  }
                  $.ajax(options);

               } catch (e)
               {
                  throw e;
               }
            }
            ga('send', 'event', 'Policy', 'PrintClick');
            return false;
         }
      });

});