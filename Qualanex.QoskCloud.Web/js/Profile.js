﻿var grpProfileTypeType = true;
$(document).ready(function () {
    //BindValuesInData("div.main-container");
    $(document).ajaxStart(function () {       
        $("#wait").show();
    });
    $(document).ajaxComplete(function () {
        $("#wait").hide();
    });

    $('#AccountProfileCode option:first-child').attr('value', '');
    $('#accountProfileGroupType option:first-child').attr('value', '');

    $("#frmEditAccountProfile ").validationEngine();

    $(".model-popup").children().click(function (e) {
        return true;
    });

    $("#grpType").hide();
    $("#AccountgrpType").hide();
    $("#pharamacy-profile").click(function () {
        $("#grpType").hide();
        $('#txtAccountNumber').val("");
        $('#btnAddAccount').show();
        $('#btnUpdateAccount').hide();
        grpProfileTypeType = true;
        $("#accountPhramacyType").show();
        $("#accountProfileCode").show();
        $("#AccountgrpType").hide();
    });
    $("#pharamcy-group").click(function () {
        $("#grpType").show();
        $('#txtAccountNumber').val("");
        $('#btnAddAccount').show();
        $('#btnUpdateAccount').hide();
        grpProfileTypeType = false;
        $("#accountPhramacyType").hide();
        $("#AccountgrpType").show();
        $("#accountProfileCode").hide();
    });

    $("#btnAddAccount").click(function (e) {        

        if ($("#frmEditAccountProfile ").validationEngine('validate') == false) {
            e.preventDefault();
            return false;
        }

        var _this = this;

        var selectgroupID = $('#ddlAccountGroupName').val();
        if (selectgroupID == null || selectgroupID == "")
            selectgroupID = 0;
        var selectProfileCode = $('#ddlAccountProfileName').val();
        if (selectProfileCode == null || selectProfileCode == "" || selectProfileCode == "-1")
            selectProfileCode = 0;
        var isvalidate = true;
        var type = '';
        var isgroupProfilevalidate = false;
        var pType = $('#accountProfileGroupType').val();
        var gTypr = $('#grpProfileGroupType').val();
        if (pType != null && pType != "") {
            type = pType;
            if (selectProfileCode > 0) {
                isgroupProfilevalidate = true;
            }

        }
        else if (gTypr != null && gTypr != "") {
            type = gTypr;
            if (selectgroupID > 0) {
                isgroupProfilevalidate = true;
            }
        }

        //if ($('#AccountProfileCode').val() == "" || $('#ddAccountType').val() == "" || $('#txtAccountNumber').val() == "" || type == "" || isgroupProfilevalidate == false) {
        //    isvalidate = false;
        //}

        if (isvalidate) {
            $.ajax({
                url: '../Profile/SaveUpdateAccountNumber',
                data: {
                    manufactureCode: $('#AccountProfileCode').val(),
                    accountType: $('#ddAccountType').val(),
                    profileCode: selectProfileCode,
                    groupId: selectgroupID,
                    accountNumber: $('#txtAccountNumber').val(),
                    isupdate: false,
                    type: type

                },
                success: function (data) {
                    if (data) {

                        $("#alertmessageid").text("Save successfully");
                        $(".alert-popup-body-message").show();
                        $("#frmEditAccountProfile").get(0).reset();
                        $("#pharamacy-profile").trigger("click");
                        $('#btnAddAccount').show();
                        $('#btnUpdateAccount').hide();
                    }
                    else {
                        $("#ProfileSuccessMessage").text("Save Not Successfully");
                    }

                }, error: function (response) {

                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });
        }
        else {

            $("#alertmessageid").text("* fields are required");
            $(".alert-popup-body-message").show();
        }

    });
    $("#btnUpdateAccount").click(function (e) {

        if (!$("#frmEditAccountProfile ").validationEngine('validate')) {
            e.preventDefault();
            return false;
        }
        var _this = this;

        var selectgroupID = $('#ddlAccountGroupName').val();
        if (selectgroupID == null || selectgroupID == "")
            selectgroupID = 0;
        var selectProfileCode = $('#ddlAccountProfileName').val();
        if (selectProfileCode == null || selectProfileCode == "" || selectProfileCode == "-1")
            selectProfileCode = 0;
        var isvalidate = true;

        var type = '';
        var isgroupProfilevalidate = false;
        var pType = $('#accountProfileGroupType').val();
        var gTypr = $('#grpProfileGroupType').val();
        if (pType != null && pType != "") {
            type = pType;
            if (selectProfileCode > 0) {
                isgroupProfilevalidate = true;
            }

        }
        else if (gTypr != null && gTypr != "") {
            type = gTypr;
            if (selectgroupID > 0) {
                isgroupProfilevalidate = true;
            }
        }





        //if ($('#AccountProfileCode').val() == "" || $('#ddAccountType').val() == "" || $('#txtAccountNumber').val() == "" || type == "" || isgroupProfilevalidate == false) {
        //    isvalidate = false;
        //}
        if (isvalidate) {
            $.ajax({
                url: '../Profile/SaveUpdateAccountNumber',
                data: {
                    manufactureCode: $('#AccountProfileCode').val(),
                    accountType: $('#ddAccountType').val(),
                    profileCode: selectProfileCode,
                    groupId: selectgroupID,
                    accountNumber: $('#txtAccountNumber').val(),
                    isupdate: true,
                    type: type
                },
                success: function (data) {
                    if (data) {
                        $("#alertmessageid").text(" Update successfully");
                        $(".alert-popup-body-message").show();
                        $("#frmEditAccountProfile").get(0).reset();
                        //$("#ProfileSuccessMessage").text("Update Successfully");
                        $("#pharamacy-profile").trigger("click");
                        $('#btnAddAccount').show();
                        $('#btnUpdateAccount').hide();
                    }
                    else {
                        $("#ProfileSuccessMessage").text("Update Not Successfully");
                        //alert("Update Not Successfully");
                    }

                }, error: function (response) {

                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });
        }
        else {
            $("#alertmessageid").text("* fields are required");
            $(".alert-popup-body-message").show();

        }
    });
    $("#btnAcountSearch").click(function () {

        
        if (!$('#AccountProfileCode').validationEngine('validate')) {
            

            $(document).unbind("ajaxStart ajaxStop");  // unbind the default ajax start and ajax stop
            CreateGridPanel();
            // bind the default ajax start and ajax stop
            $(document).ajaxStart(function () {
                $("#wait").show();
            });
            $(document).ajaxComplete(function () {
                $("#wait").hide();
            });
            
        }

        return false;
    });

    $("#btnAccountCancel").click(function () {
        $("#jqxgrid").hide();
        $(".show-pagesize-cmb").hide();
    });

    $("#accountProfileGroupType").change(function () {
   
        var selectedType = $('#accountProfileGroupType').val();
        $("#ddlAccountProfileName").empty();
        if (selectedType != "Select Type" && selectedType != "") {
            $.ajax({

                url: '../Profile/Profiles',
                data: { ProfileType: selectedType },
                datatype: "json",
                success: function (data) {
                    $("#accountProfileCode").show();
                    $("#ddlAccountProfileName").empty();
                    $("#lblAccountCustomer").html("");
                    $("#lblAccountCustomer").html(selectedType + " <span>*</span>");
                    $("#ddlAccountProfileName").append("<option  value=''>Select Customer Name</option>");
                    $.each(data, function (index, optionData) {
                        $("#ddlAccountProfileName").append("<option value='" + optionData.ProfileCode + "'>" + optionData.Name + "</option>");
                    });
                    $("#ddlAccountProfileName").prop("disabled", false);

                }, error: function (response) {
                    $("#ddlAccountProfileName").empty();

                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });
        }
        else {
            $("#ddlAccountProfileName").empty();
            $("#ddlAccountProfileName").append("<option  value=''>Select Customer Name</option>");
            $("#ddlAccountProfileName").prop("disabled", true);
        }


    });

    $("#grpProfileGroupType").change(function () {
        var selectedType = $('#grpProfileGroupType').val();
        $('#txtAccountNumber').val("");
        $('#btnAddAccount').show();
        $('#btnUpdateAccount').hide();
        if (selectedType != "Select Group Type" && selectedType != "") {
            $.ajax({
                url: '../Profile/GetGroupTypes',
                data: { groupType: $('#grpProfileGroupType').val() },
                datatype: "json",
                success: function (data) {
                    $("#ddlAccountGroupName").show();
                    $("#ddlAccountGroupName").empty();
                    $("#ddlAccountGroupName").append("<option  value=''>Select Group Name</option>");
                    $.each(data, function (index, optionData) {
                        $("#ddlAccountGroupName").append("<option value='" + optionData.ProfileCode + "'>" + optionData.Name + "</option>");
                    });
                    $("#ddlAccountGroupName").prop("disabled", false);
                }, error: function (response) {
                    $("#ddlAccountGroupName").empty();
                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });
        }
        else {
            $("#ddlAccountGroupName").empty();
            $("#ddlAccountGroupName").append("<option  value=''>Select Group Name</option>");
            $("#ddlAccountGroupName").prop("disabled", true);
        }


    });

    $("#ddlAccountProfileName").change(function () {
        var selectedType = $('#ddlAccountProfileName').val();
        $('#txtAccountNumber').val("");
        $('#btnAddAccount').show();
        $('#btnUpdateAccount').hide();
        if (selectedType != "Select Customer Name") {

            $.ajax({
                url: '../Profile/GetAccountNumber',
                data: {
                    manufactureCode: $('#AccountProfileCode').val(),
                    accountType: $('#ddAccountType').val(),
                    profileCode: $('#ddlAccountProfileName').val(),
                    groupId: 0

                },
                success: function (data) {

                    if (data != null && data != "") {
                        $('#txtAccountNumber').val(data);
                        $('#btnAddAccount').hide();
                        $('#btnUpdateAccount').show();
                    }
                    else {
                        $('#btnAddAccount').show();
                        $('#btnUpdateAccount').hide();
                    }

                }, error: function (response) {
                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });


        }


    });

    $("#ddlAccountGroupName").change(function () {
        var selectedType = $('#AccountGroupCode').val();
        $('#txtAccountNumber').val("");
        $('#btnAddAccount').show();
        $('#btnUpdateAccount').hide();
        if (selectedType != "Select Group Name") {

            $.ajax({
                url: '../Profile/GetAccountNumber',
                data: {
                    manufactureCode: $('#AccountProfileCode').val(),
                    accountType: $('#ddAccountType').val(),
                    profileCode: 0,
                    groupId: $('#ddlAccountGroupName').val()

                },
                success: function (data) {

                    if (data != null && data != "") {
                        $('#txtAccountNumber').val(data);
                        $('#btnAddAccount').hide();
                        $('#btnUpdateAccount').show();
                    }
                    else {
                        $('#btnAddAccount').show();
                        $('#btnUpdateAccount').hide();
                    }

                }, error: function (response) {

                    // alert(response);
                    if (response.status != 0) {
                        //   alert(response.status + " " + response.statusText);
                    }
                }
            });
        }


    });

    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });

    $("#btnProfileDelete").click(function () {
        var profileCode = $("#ProfileCode").val();
        deleteProfile(profileCode);
        $("#btdeletenok").unbind("click");

        $("#btdeletenok").click(function () {
            $.ajax({
                url: '../Profile/Delete',
                data: {
                    id: deleteprofileCode
                },
                global: false,
                success: function (data) {

                    if (data.Result == Compare.Deleted || data.Result == Compare.OK) {

                        $("#alertmessageid").text(Profile.DeleteSuccess);
                        $(".alert-popup-body-message").show();
                        $(".alert-close").hide();
                        $("#btnok").bind("click", function () {

                            window.location = '../Profile/Index';
                            $("#btnok").unbind("click");
                        });
                    }
                    if (data.Result == Compare.Fails) {

                        $("#alertmessageid").text(Profile.DeleteFailed);
                        $(".alert-popup-body-message").show();
                    }
                    $(".alert-popup-body-message-confirm").hide();
                },
                error: function (response) {
                    if (response.status != 0) {
                        //alert(response.status + " " + response.statusText);
                    }
                }
                 
            });


        });        
        return false;
    });

});

var deleteprofileCode = "";
function deleteProfile(profileCode) {
    deleteprofileCode = profileCode;
    $("#alertdeletemessageid").text(Profile.DeleteConfirmation);
    $(".alert-popup-body-message-confirm").show();
}

function CreateGridPanel() {
    var selectgroupID = $('#ddlAccountGroupName').val();
    if (selectgroupID == null || selectgroupID == "")
        selectgroupID = 0;
    var selectProfileCode = $('#ddlAccountProfileName').val();
    if (selectProfileCode == null || selectProfileCode == "" || selectProfileCode == "-1")
        selectProfileCode = 0;

    var type = '';
    var pType = $('#accountProfileGroupType').val();
    var gTypr = $('#grpProfileGroupType').val();
    if (pType != null && pType != "") {
        type = pType;
    }
    else if (gTypr != null && gTypr != "") {
        type = gTypr;
    }

    var source = {

        datatype: "json",
        type: 'GET',
        //datafields: jsoncolumn,//[{ name: 'Expiration Date', type: "date", format: 'dd.MM.yyyy' }],
        url: '../Profile/BindAccountGrid',
        data: {
            manufactureCode: $('#AccountProfileCode').val(),
            accountType: $('#ddAccountType').val(),
            profileCode: selectProfileCode,
            groupId: selectgroupID,
            accountNumber: $('#txtAccountNumber').val(),
            type: type

        },
        root: 'Rows',
        //contentType: "application/json; charset=utf-8",
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;

        },
        loadComplete: function () {
            var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records').text(datainfo.rowscount);
                $(".show-pagesize-cmb").show();
                $("#jqxgrid").show();
            }
            else {
                $(".show-pagesize-cmb").hide();
                $('#total-records').text(0);
                $('#PagesizeCounter').val(100);
                $("#jqxgrid").hide();
            }

        }

    };

    var dataadapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
        },

    });


    // prepare the data
    var columns = [
            { text: 'Pharmacy Name', editable: false, datafield: 'PharmacyName' },
            { text: 'Profile Type', editable: false, datafield: 'Type' },
            { text: 'City', editable: false, datafield: 'City' },
            { text: 'State', editable: false, datafield: 'State' },
            { text: 'Account Type', editable: false, datafield: 'AccountType' },
            { text: 'Account Number', editable: false, datafield: 'AccountNumber' }
    ]
    // initialize jqxGrid
    $("#jqxgrid").jqxGrid({
        width: '100%',
        source: dataadapter,        
        sortable: true,
        filterable: true,
        autoheight: true,
        pageable: true,
        columnsreorder: true,
        virtualmode: true,
        showdefaultloadelement: false,
        enabletooltips: true,
        columnsresize: true,        
        pagermode: 'simple',
        rendergridrows: function (obj) {
            return obj.data;
        },
        ready: function () {
            var pagesize = $('#PagesizeCounter').val();
            $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
        },
        columns: columns,


    });
}