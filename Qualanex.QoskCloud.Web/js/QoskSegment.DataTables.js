﻿$(function()
{
   //*****************************************************************************
   //*
   //* Summary:
   //*   When the grid gets refreshed, reorder the date row attributes for the Summary tab
   //*
   //* Parameters:
   //*   target - the entire grid that needs to be reordered accordingly
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var reshuffleDateRows = function (target)
   {
      var newDateRow = -1;
      var dates = "";
      var itemDate = "";

      $(target).each(function ()
      {
         itemDate = $(this).find("td[exchange='Date']").attr("exchange-value");
         $(this).find("td[exchange='Date']").removeAttr("style").removeClass("dateRow_0").removeClass("dateRow_1").removeClass("dateRow_2");

         if (dates !== itemDate)
         {
            if (dates !== "")
            {
               $(this).closest("tr").addClass("firstRow");
            }

            newDateRow = (newDateRow + 1) % 3;
            dates = itemDate;
            $(this).find("td[exchange='Date']").removeAttr("style");
            $(this).find("td[exchange='Date'] span").removeAttr("style");
         }
         else
         {
            itemDate = "";
            $(this).closest("tr").removeClass("firstRow");
         }

         // reshuffle the date group IDs based on the table entry
         $(this).attr("dateRow", newDateRow);
         $(this).find("td[exchange='Date']").addClass("dateRow_" + newDateRow);

         if (itemDate === "")
         {
            $(this).find("td[exchange='Date']").css({ "border-color": "transparent" });
            $(this).find("td[exchange='Date'] span").attr("style", "display:none");
         }
      });
   }
   window.reshuffleDateRows = reshuffleDateRows;

   //*****************************************************************************
   //*
   //* Summary:
   //*   keyup event on all Data Table searches (DataTables.net)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keyup", ".dataTables_filter input[type=search]", function()
   {
      //grab the refactored grid we're filtering on via the search input box
      var $filterGrid = $(".ql-body.active .QnexGrid");

      //this is to account for any non-refactored grids
      if ($filterGrid.html() === undefined)
      {
         $filterGrid = $(".ql-body.active .QnexContent");
      }

      // upon first key entry, apply class
      // remove class if empty
      if ($(this).val() !== undefined && $(this).val() !== "")
      {
         // dates are hidden in a span tag
         $filterGrid.find("tr td span").each(function()
         {
            $(this).addClass("searchFilter");
         });
      }
      else
      {
         $filterGrid.find("tr td span").each(function ()
         {
            $(this).removeClass("searchFilter");
         });
      }

      $filterGrid.find(".dataTables_scrollBody").height("auto");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Keypress event on all Data Table searches (DataTables.net)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".dataTables_filter input[type=search]", function (e)
   {
      if (getEditMode())
      {
         e.preventDefault();
      }
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   keydown event on all Data Table searches (DataTables.net)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keydown", ".dataTables_filter input[type=search]", function (e)
   {
      if (getEditMode())
      {
         e.preventDefault();
      }
   });
});