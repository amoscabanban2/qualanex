﻿
function PhoneNumberMask(e)
{
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
}

function NumericOnly(obj) {
    if (obj.value.length > 0) {
        
        obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}
function NumericWithDashOnly(obj) {
    if (obj.value.length > 0) {

        obj.value = obj.value.replace(/[^[0-9]+(-[0-9]+)+$]/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}


function NumericWithDecimalOnly(obj) {
    if (obj.value.length > 0) {
        obj.value = obj.value.replace(/[^0-9\.]/g, '');
        //obj.value = obj.value.replace(/[((\d+)(\.\d{2}))$]/g, '');
    }
}
function CollapseSearch() {
    $("#collapse").hide();
    $("#expand").show();    
    $("#exp_col").hide(1000);
}
function ExpendSearch() {
    $("#collapse").show();
    $("#expand").hide();
    $("#exp_col").show(1000);
}

function BindEditUpdateMode() {
    
    $('.edit').show();
    if ($('.update').prev().hasClass("cancel")) {
        $('.update').hide();
        $('.cancel').hide();
    }

    
    $('.edit').click(function () {
        var _this = this;
        $(_this).hide();
        $(_this).parent().find('.update').show();
        $(_this).parent().find('.cancel').show();

        BindValuesInData('#' + $(_this).attr('frmdiv'));
        $('#' + $(_this).attr('frmdiv')).find("input,select,textarea").removeAttr('disabled');

        //setTimeout(function () {
        //    $('#' + $(_this).attr('frmdiv')).parent().validationEngine();
        //}, 100);
        
        
    });

  

    $('.cancel').click(function () {
        
        $(this).hide();
        $(this).parent().find('.update').hide();
        $(this).parent().find('.edit').show();
        this.form.reset();
        $('#' + $(this).attr('frmdiv')).find("input,select,textarea").attr("disabled", "disabled");
        //$('#' + $(this).attr('frmdiv')).parent().validationEngine('detach');
        $('#' + $(this).attr('frmdiv')).parent().validationEngine('hide');
    });
}

function HideUpdate(elem)
{
    $(elem).hide();
    $(elem).parent().find('.cancel').hide();
    $(elem).parent().find('.edit').show();
    $('#' + $(elem).attr('frmdiv')).find("input,select,textarea,a").attr("disabled", "disabled");
    //$('#' + $(this).attr('frmdiv')).parent().validationEngine('detach');
}


function BindValuesInData(box) {
    $(box).find("input,select,textarea").each(function () {
        if ($(this).prop("tagName") == "INPUT") {
            if ($(this).attr('type') == "checkbox") {
                $(box).data($(this).attr('id'), $(this).prop('checked'));
                
            }
            else if ($(this).attr('type') == "radio") {
                $(box).data($(this).attr('id'), $(this).prop('checked'));
            }
            else if ($(this).attr('type') == "text") {
                $(box).data($(this).attr('id'), $(this).val());
            }
            //else if ($(this).attr('type') == "hidden") {
            //    $(box).data($(this).attr('id'), $(this).val());
            //}
        }
        if ($(this).prop("tagName") == "SELECT") {
            $(box).data($(this).attr('id'), $(this).val());

        }
        if ($(this).prop("tagName") == "TEXTAREA") {
            $(box).data($(this).attr('id'), $(this).val());
        }
    });
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}

function BindEmptyValuesInData(box) {
    $(box).find("input,select,textarea").each(function () {
        if ($(this).prop("tagName") == "INPUT") {
            if ($(this).attr('type') == "checkbox") {
                $(box).data($(this).attr('id'), false);
            }
            else if ($(this).attr('type') == "radio") {
                $(box).data($(this).attr('id'), false);
            }
            else if ($(this).attr('type') == "text") {
                $(box).data($(this).attr('id'), "");
            }
            else if ($(this).attr('type') == "hidden") {
                $(box).data($(this).attr('id'), "");
            }
        }
        if ($(this).prop("tagName") == "SELECT") {
            $(box).data($(this).attr('id'), "");

        }
        if ($(this).prop("tagName") == "TEXTAREA") {
            $(box).data($(this).attr('id'), "");
        }
    });
}

function IsChangedValue(box) {

    var retval = false;
    $(box).find("input,select,textarea").each(function () {

        
        if ($(this).prop("tagName") == "INPUT") {
            if ($(this).attr('type') == "checkbox") {
                if ($(this).prop('checked') != $(box).data($(this).attr('id'))) {                    
                    retval = true;
                }
            }
            else if ($(this).prop('checked') == "radio") {
                if ($(this).prop('checked') != $(box).data($(this).attr('id'))) {
                    retval = true;
                }
            }
            else if ($(this).attr('type') == "text") {
                if ($(this).val() != $(box).data($(this).attr('id'))) {
                    retval = true;
                }
            }
            //else if ($(this).attr('type') == "hidden") {                
            //    if ($(this).val() != $(box).data($(this).attr('id'))) {
            //        alert($(this).val())
            //        alert($(box).data($(this).attr('id')))
            //        retval = true;
            //    }
            //}
        }
        if ($(this).prop("tagName") == "SELECT") {
            var val = $(this).val() == null ? "" : $(this).val();
            var match = $(box).data($(this).attr('id')) == null ? "" : $(box).data($(this).attr('id'));
            if (val != match) {
                retval = true;
            }

        }
        if ($(this).prop("tagName") == "TEXTAREA") {
            if ($(this).val() != $(box).data($(this).attr('id'))) {
                retval = true;
            }
        }
    });
    return retval;
}

function UndoValues(box) {
    $(box).find("input,select,textarea").each(function () {
        if ($(this).prop("tagName") == "INPUT") {
            if ($(this).attr('type') == "checkbox") {
                $(this).prop('checked', $(box).data($(this).attr('id')))

            }
            else if ($(this).attr('type') == "radio") {
                $(this).prop('checked', $(box).data($(this).attr('id')))
            }
            else if ($(this).attr('type') == "text") {
                $(this).val($(box).data($(this).attr('id')))
            }
            else if ($(this).attr('type') == "hidden") {
                $(this).val($(box).data($(this).attr('id')))
            }
        }
        if ($(this).prop("tagName") == "SELECT") {
            
            if ($(box).data($(this).attr('id')) == "" || $(box).data($(this).attr('id')) == null) {                
            }
            else {
                $(this).val($(box).data($(this).attr('id')))
            }
        }
        if ($(this).prop("tagName") == "TEXTAREA") {
            $(this).val($(box).data($(this).attr('id')))
        }
    });

    //$('.error-msg').css('display', 'none');
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
}

function SendAjax(url, data) {
    
    var result;
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        async: false,
        dataType: 'json',
        success: function (data) {
            result = data;
        },
        complete: function () {
        },
        error: function (req, status, error) {
            //alert(status);                                        
        }
    });
    return result;
}

function RestricZero(field, rules, i, options) {
   
    var msg = undefined;
    if (field.val() != "" && field.val() == "0") {
            msg = "* Value can  not be zero ";
           
    }
    return msg;
}

function SendAjaxGet(url, data) {
    var result;
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        async: false,
        dataType: 'json',
        success: function (data) {

            result = data;
        },
        complete: function () {
        },
        error: function (req, status, error) {

            //alert(status);                                        
        }
    });
    return result;
}

function SendAjaxGetAsync(url, data,success,error) {
    var result;
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        async: true,
        dataType: 'json',
        success: function (data) {            
            if (success != null && success != undefined) {
                success(data);
            }
        },
        complete: function () {
        },
        error: function (req, status, error) {
            if (error != null && error != undefined)
            {
                error(req, status, error);
            }
            
        }
    });
    return result;
}


//convert jason date to date ==================================Created on 15 Apr 2015
function convertJsonDate(date, format) {
    try {
        if (date == null
           || date == undefined
           || date == ""
           || date == NaN) {
            return "";
        }
        else {
            var dte = eval("new " + date.replace(/\//g, '') + ";");
            //return dte.format(format);
            return $.datepicker.formatDate(format, dte)
        }
    }
    catch (err) {
        return "";
    }
}


function BindValidations() {
    bindPhoneMasking();
    bindOnlyNumbers();
}

function bindOnlyNumbers() {
    $(".Numbers").each(function () {
        $(this).keypress(function (e) { return onlyNumbers(e) });
    });
}

//only numbers
function onlyNumbers(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function setMaskingContact(elem, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 8 && charCode != 46) {
        var text = $(elem).val();
        $(elem).val(setMaskingContactByString(text));

    }
    return true;
}

function setMaskingContactByString(str) {
    var textarray = new Array();

    textarray = str.replace(/\(/g, "").replace(/\)/g, "").replace(/\-/g, "").split('');

    var text = '';
    var j = 10;
    if (textarray.length > 11) {
        j = 10;
    }
    else {
        j = textarray.length;
    }
    for (var i = 0; i < j; i++) {
        if (i == 3 && textarray[i] != '-') {
            text = text + '-';
        }
        if (i == 6 && textarray[i] != '-') {
            text = text + '-';
        }

        text = text + textarray[i];
    }
    return text;
}

//function RestrictforHtmlTag(str) {

//    var reg = /<(.|\n)*?>/g;

//    if (reg.test($('#YourTextAreaID').val()) == true) {

//        var ErrorText = 'do not allow HTMLTAGS';

//        alert('ErrorText');

//    }
//}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function bindPhoneMasking() {  
    $('.phone').each(function () {
        var $this = $(this);
        $(this).attr("MaxLength", "20");        
        $(this).keypress(function (e) {
            if ($(this).val().length == 10) {
                var charCode = (e.which) ? e.which : event.keyCode;
                if (charCode == 88 || charCode == 120) {
                    return true;
                }
                return onlyNumbers(e);
            }
            else {
                return onlyNumbers(e);
            }
        });
        $(this).change(function (e) {
            var phone, originalval;
            originalval = $(this).val().replace(/-/gi, '');

            if (originalval.length < 10) {
                //alert("Please enter valid phone number.")
            }
            else if (originalval.length == 10) {
                phone = originalval.substring(0, 10);
                $this.val(setMaskingContactByString(phone));
            }
        });
    });
}
function BindDateMasking() {
    $('.date').each(function () {
        $(this).attr("MaxLength", "10");
        $(this).attr("placeholder", DateFormat);
        $(this).keypress(function (e) { return onlyNumbers(e) });
        $(this).keyup(function (e) { setMaskingDate(this, e); });
        $(this).blur(function (e) {
            try {
                var date = $(this).datepicker('getDate');
                $(this).val($.datepicker.formatDate(DateFormat, date));
            }
            catch (e) {
                $(this).val('');
            }
        });
    });
}

function StartDateEndDate(start,end)
{
    $(start).datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(end).datepicker("option", "minDate", dt);
        }
    });

    var date = $(start).datepicker('getDate');
    $(start).val($.datepicker.formatDate(DateFormat, date));

    $(end).datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(start).datepicker("option", "maxDate", dt);
        }
    });

    date = $(end).datepicker('getDate');
    $(end).val($.datepicker.formatDate(DateFormat, date));
}


function CheckStringEmpty(str) {
    if (str == '' || str == null || str == undefined) {
        return false;
    }
    return true;
}

function setStringEmpty(str) {
    if (str == '' || str == null || str == undefined) {
        return '';
    }
    return str;
}

function GetUserData() {
    return SendAjaxGet('../Common/LogedINUserDetails', '');
}
function RedirectToUrl(url) {
    window.location = url;
}
//var highlitebordercss = { border: "3px solid yellow" }
function lockingFeature(NewResult)
{
    
    
    $.each(NewResult, function (k, v) {
       
        try {
            if ($("#" + k + ", [name='" + k + "']").prop("tagName") == "INPUT") {

                if ($('[name="' + k + '"]').attr('type') == "checkbox") {
                    if ($('[name="' + k + '"]:checked').val() != toTitleCase(v.toString())) {
                        $('[name="' + k + '"][value="' + toTitleCase(v.toString()) + '"]').attr('checked', 'checked').parent().addClass("highlitebordercss");
                        $('[name="' + k + '"][value="' + toTitleCase(v.toString()) + '"]').prop('defaultChecked', true);
                    }
                }
                else if ($('[name="' + k + '"]').attr('type') == "radio") {
                 
                    if ($('[name="' + k + '"]:checked').val() != toTitleCase(v.toString())) {
                        $('[name="' + k + '"][value="' + toTitleCase(v.toString()) + '"]').attr('checked', 'checked').parent().addClass("highlitebordercss");
                        $('[name="' + k + '"][value="' + toTitleCase(v.toString()) + '"]').prop('defaultChecked', true);
                    }
                }
                else if ($("#" + k).attr('type') == "text") {

                    if (setStringEmpty($("#" + k).val()) != setStringEmpty(v)) {

                        if (k.toLowerCase().indexOf("date") >= 0) {
                            if (setStringEmpty($("#" + k).val()) != ConvertExpirationDate(v))
                            {
                                $("#" + k).val(ConvertExpirationDate(v)).addClass("highlitebordercss");
                                $("#" + k).prop('defaultValue', ConvertExpirationDate(v))
                            }                            
                        }
                        else {
                            $("#" + k).val(v).addClass("highlitebordercss");
                            $("#" + k).prop('defaultValue', v);
                        }
                    }
                }
            }
            if ($("#" + k).prop("tagName") == "SELECT") {

                if (typeof v == 'boolean') {
                    if ($("#" + k).val() != toTitleCase(v.toString())) {
                        $("#" + k).val(toTitleCase(v.toString())).addClass("highlitebordercss");
                        $("#" + k).prop('defaultSelected', toTitleCase(v.toString()));
                    }
                }
                else {
                    if ($("#" + k).val() != v) {
                        $("#" + k).val(v).addClass("highlitebordercss");
                        $("#" + k).prop('defaultSelected', v);
                    }
                }

            }
            if ($("#" + k).prop("tagName") == "TEXTAREA") {
                if (setStringEmpty($("#" + k).val()) != setStringEmpty(v))
                {
                    $("#" + k).val(toTitleCase(v.toString())).addClass("highlitebordercss");
                    $("#" + k).prop('defaultValue', toTitleCase(v.toString()));
                    //if ($("#" + k).val() != v) {
                    //    $("#" + k).val(v).addClass("highlitebordercss");
                    //}
                }
            }


        }
        catch (e) {

        }
    });
}

function closePopup() {

    $("#btnok,.alert-close").click(function () {
        $(".alert-popup-body-message").hide();
        $(".alert-popup").animate({ "top": "40%" });
    })

}
function ConvertExpirationDate(value) {
    if (value != '') {
        var dateString = value.substr(6);
        var currentTime = new Date(parseInt(dateString));
        //var month = currentTime.getMonth() + 1;
        //var day = currentTime.getDate();
        //var year = currentTime.getFullYear();
        //var date = month + "/" + day + "/" + year;
        //$.datepicker.formatDate(DateFormat, date)
        return $.datepicker.formatDate(DateFormat, currentTime);
    }
    else { return '' }
}

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}
function SearchNDCUPCType(id, span) {
    var result = 0;
    if ($(id).val().length > 0) {
        result = SendAjax('../Product/GetSearchType', {
            searchvalue: $(id).val()
        });
        if (result == 0) {
            $(span).text("(NDC With Dashes)");
        }
        else if (result == 1) {
            $(span).text("(NDC Number)");
        }
        else if (result == 2) {
            $(span).text("(UPC Number)");
        }
        else if (result == -1) {
            $(span).text("(Invalid)");
        }
        
    }
    else {
        $(span).text('');
    }
    return result;
}


$(document).ready(function () {

    $('.update').click(function () {
        BindValuesInData("div.main-container");
    });

    $("#collapse").show();
    $("#expand").hide();
    $("#collapse").click(function () {
        $("#collapse").hide();
        $("#expand").show();
        
        $("#exp_col").hide(1000);
    });
    $("#expand").click(function () {
        $("#collapse").show();
        $("#expand").hide();
        $("#exp_col").show(1000);
    });


    $('.lot-icon').click(function (e) {
        sessionStorage.clear();
        urlName = $(this).attr('href');
        e.stopPropagation();
        e.preventDefault();
        //if ($('#successerrormsgforproductdescription').html() == "Saved Successfully") {
        //    isLeavewitoutSave = false;
        //}
        if (IsChangedValue("div.main-container")) {
            $(".alert-popup-body").show();
            $(".alert-popup").animate({ "top": "40%" });
           return false;
        }
        else { window.location.href = urlName; }

        return true;
    });

    //Use for Save changes poup functionality
   

    $(".alert-close, #btnAlertStay,.close-btn").click(function () {
        $(".alert-popup-body").hide();
        $(".alert-popup").animate({ "top": "40%" });

        $(".model-popup").hide();
    })


    $('#btnAlertSave').click(function () {
    
        $(".update").each(function () {
            if ($(this).is(":visible")) {
                $(this).click();
                $(".alert-popup-body").hide();
                $(".alert-popup").animate({ "top": "40%" });

            }
        })
        return true;
    });
   


    $('#btnAlertLeave').click(function () {
        isLeavewitoutSave = true;
        window.location.href = urlName;
        return true;
    });




    $(window).resize(function () {

        if ($(window).width() <= 800) {
            $("#collapse").hide();
            $("#expand").hide();
        }
        else {
            $("#collapse").show();
        }
    });

    $(window).scroll(function () {
        if ($(window).width() > 800) {
            var scroll = $(window).scrollTop();
            if (scroll >= 150) {
                $("#exp_col").hide(1000);
            }
            else if (scroll <= 150) {
                $("#collapse").hide();
                $("#expand").show();
                $("#exp_col").show(1000);

            }
        }
    });
});


function BindJQXGrid(dv, url, data, columns, datafields, filterable, callbackloadComplete, callbackready, callbackinitrowdetails,rowdetails)
{


    var source = {
        datatype: "json",
        type: 'GET',
        async: true,
        datafields: datafields,
        url: url,
        data: data,
        root: 'Rows',
        contentType: "application/json; charset=utf-8",
        // update the grid and send a request to the server.
        filter: function () {
            $(dv).jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $(dv).jqxGrid('updatebounddata', 'sort');
        },
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
        loadComplete: function () {
            if (callbackloadComplete != null && callbackloadComplete != undefined) {
            callbackloadComplete(source);
            }
        },
    };

    var dataadapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
        },

    });
    
    $(dv).jqxGrid({
        width: '100%',
        pagesize: 100,
        source: dataadapter,
        pageable: true,
        sortable: true,
        filterable: filterable,
        autoloadstate: false,
        autosavestate: false,       
        columnsreorder: true,
        virtualmode: true,
        
        showdefaultloadelement: false,
        enabletooltips: true,
        columnsresize: true,        
        height: '100%',
        pagermode: 'simple',
        rendergridrows: function (obj) {
            return obj.data;
        },
        ready: function () {
            if (callbackready != null && callbackready != undefined) {
                callbackready();
            }
        },

        rowdetails: rowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 4px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        initrowdetails: function (index, parentElement, gridElement, record) {            
            if (callbackinitrowdetails != null && callbackinitrowdetails != undefined) {
                callbackinitrowdetails(index, parentElement, gridElement, record);
            }
        },

        columns: columns,
    });
}

function Populatedropdown(id, data, text, value, selectedvalue) {
    $(id).empty();
    $(id).get(0).options[0] = new Option("Select Option", "");
    $.each(data, function (index, item) {
        $(id).get(0).options[$(id).get(0).options.length] = new Option(item[text], item[value]);
    });
    $(id).val(selectedvalue);
}

function PopulatedropdownAjax(id, url, data, text, value, selectedvalue) {
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        dataType: "json",
        success: function (result) {
            Populatedropdown(id, result, text, value, selectedvalue)
        }
    });
}