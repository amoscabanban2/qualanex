﻿$(function()
{

   $(document).on("change",
       "input[type=radio].DispositionRadio",
       function(e)
       {
          //if (!validateAll()) {
          //    return false; //TODO: uncomment once disposition requires validation
          //}

          $("#DispositionSelection").html(" - " + $(this).attr("data-ql-label"));
          $(".DispositionHidden").val($(this).val());
          changeSelection(this, "div");
          var $form = $("#policyForm");
          var disabledInputs = $("#Policy :input[disabled]");
          disabledInputs.attr("disabled", false);
          var options = {
             async: true,
             url: $form.attr("data-ql-finalize"),
             type: $form.attr("method"),
             data: $form.serialize() + getAntiForgeryToken(),
             error: function(err)
             {
                showError(err.statusText);
             },
             success: function(data)
             {
                if ($("#finalize").length)
                {
                   $("#policyStatus").slideCollapse();
                   $("#finalize").slideExpand();
                   var $target = $("#finalize-result");
                   var $newHtml = $(data);
                   $target.replaceWith($newHtml);
                   $('#tabs-Policy').animate({ scrollTop: 0 }, 200);
                   setTimeout(function()
                   {
                      $('#tabs-Policy').animate({
                         scrollTop: $("#finalize").offset().top - $("#tabs-Policy").offset().top - 43
                      },
                          200);
                   },
                       300);
                }
             }
          };
          disabledInputs.attr("disabled", true);
          $("#finalize-body").hide();
          $("#finalize-loading").show();
          $("[form='policyForm'],.ql-policy-continue").parents(".panel-body").find("input, textarea, button, select").attr("disabled", true);
          $.ajax(options);
          ga('send', 'event', 'Policy', 'DispositionSelected', $(this).attr("data-ql-label"));
          return false;
       });

});