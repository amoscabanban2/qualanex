﻿//*****************************************************************************
//*
//* QoskSegment.BinDesignation.js
//*    Copyright (c) 2017 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Javascript functionality supporting Bin Designation and Warehouse Induciton
//*
//* Notes:
//*    Because the bin designation segment is the primary difference between warehouse 
//*    and pharma induction, it contains the functional differences required throughout
//*    the warehouse induction process.
//*
//*****************************************************************************
$(function ()
{
   window.sortBinType = "";
   var stationId = "";
   var location = "";
   var gaylordClosed = false;

   //Add methods to run when the user successfully scans the designated sort container
   //Execution is in putContainer()
   window.onDesignationMethods = [];
   window.addDesignationMethod = function (func)
   {
      exclusivePush(onDesignationMethods, func);
   }

   //Add methods to run when bin designation loads with a sort bin
   //Execution is in onDesignation()
   window.onDesignationLoadMethods = [];
   window.addDesignationLoadMethod = function (func)
   {
      exclusivePush(onDesignationLoadMethods, func);
   }

   //flag used to indicate that an item is tied to the inbound container
   //and is ready to be put in an outbound container
   var readyForPut = true;

   //array used to cycle between interchangeable lrg/sml gaylords
   var c0Gaylords = ["LRG_GAYLRD", "SML_GAYLRD"];

   //array used to check if item is sorted to any gaylord
   var gaylords = ["ORG_GAYLRD", "LRG_GAYLRD", "SML_GAYLRD"];

   //prevents search from continuing without an inbound container
   //added to sidebar.beforeClose methods in setupWarehouseInduction
   var disableSubmitSearch = function ($container)
   {
      if ($container.find("[data-container-field='containerId']").val() === sidebar.getInboundContainer().containerId)
      {
         $("#SubmitSearch").attr("data-disable", true);
      }

      return true;
   }

   //enables search when inbound container is attached
   //added to sidebar.onBinOpen and sidebar.onToteOpen in setupWarehouseInduction
   var enableSubmitSearch = function ($container)
   {
      if (sidebar.getInboundContainer().containerId !== "")
      {
         $("#SubmitSearch").attr("data-disable", false);
         $(".ndcField").select();
      }

      return true;
   }

   //Disable the designation functionality on designation error (no picture touching, no bin/gaylord scanning, etc.)
   var disableBinDesignation = function (errorMsg = "")
   {
      $("#designationError").html(errorMsg);
      $(".txtSortBin").attr("placeholder", "").disable();
      $(".sortBinSubmit").disable();
      $("#Sidebar [name=containerClose]").disable();
   }

   //helper method to determine and retrieve the exact gaylord required for the 
   //current item and RA. 
   function getGaylord(containerType, containerId)
   {
      var controlNumber = 0;

      switch ($(".CtrlNumberHidden").val())
      {
         case "0":
            controlNumber = 0;
            break;
         case "2":
            controlNumber = 2;
            break;
         case "3":
         case "4":
         case "5":
            controlNumber = 345;
            break;
      }

      var profileCode = 0;

      //if ra information for this container is not available, load it synchronously
      //in most situations it will have been loaded asynchronously earlier in processing,
      //but this is kept here as a backup
      if ($(".ItemGuid").val() !== "" && (window.raInformation === undefined || window.raInformation.Profile === undefined))
      {
         var request = $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "itemGuid=" + $(".ItemGuid").val() + getAntiForgeryToken(),
            url: "/product/upa/GetRaForItem",
            async: false,
            success: function (result)
            {
               window.raInformation = result;
            },
            error: function ()
            {
               window.raInformation = undefined;
            }
         });

         window.raInformation = request.responseJSON;
      }

      if (window.raInformation !== undefined && window.raInformation.Profile !== undefined && window.raInformation.Profile.SpecialHandling === true)
      {
         profileCode = window.raInformation.Profile.ProfileCode;
      }

      if ($("#PolicyForm_IsForeignContainer").val() === "True")
      {
         profileCode = 0;
      }

      //get the waste profile ID for the item processed
      //theoretically, only non-hazardous should hit this
      var wasteProfileId = null;

      if ($("#PolicyForm_ItemWasteStreamProfileId").val() !== "")
      {
         wasteProfileId = $("#PolicyForm_ItemWasteStreamProfileId").val();
      }

      return sidebar.getGaylord(controlNumber, profileCode, null, wasteProfileId, containerType, containerId);
   }

   //helper method determines what process to use to find the required container
   function getContainer(containerType, containerId)
   {
      if (gaylords.includes(containerType))
      {
         //gather the required properties for gaylords
         return getGaylord(containerType, containerId);
      }
      else
      {
         //call the sidebar directly
         return sidebar.getContainerByType(containerType, "", containerId);
      }
   }


   //this method is added to onDesignationLoadMethods in _SegmentBinDesignation
   //it contains induction specific logic and is not included with implementations of 
   //bin designation not using _SegmentBinDesignation
   window.disablePolicyForm = function ()
   {
      //ReadyForDisposition is set to true when all required forms are filled,
      //the load will include a sort bin
      if ($(".ReadyForDispositionHidden").val() === "True")
      {
         //prevent editing the policy form
         $("[form='policyForm'],.ql-policy-continue").parents(".panel-body").find("input, textarea, button, select").attr("disabled", true);
         var inbound = sidebar.getInboundContainer();

         //if a guid is already set, this is an item that is being re-inducted
         var reinductItem = $(".ItemGuid").val() !== "";

         //if this is a reinduction, or the item is being taken from a tote,
         //make sure there is a record tying the item to the current inbound container
         if (inbound.typeGroup === "Tote" || reinductItem)
         {
            readyForPut = false;
            var item = $(".itemIdHidden").val();

            //check if the item is already in the inbound container
            sidebar.isItemIn(item, inbound.containerId, inbound.type, inbound.category, function (isItemInData)
            {
               // check for any exceptions; notify the user that the item must be re-processed
               if (!isItemInData.success)
               {
                  disableBinDesignation(isItemInData.error);
                  return;
               }

               //if not, add it
               if (!isItemInData.isItemIn || reinductItem)
               {
                  // if for some reason, the ItemGuid is still empty at this point (which we cannot have empty due to the seralization in SaveItem) then fill it with ItemIdHidden
                  if (!$(".ItemGuid").val())
                  {
                     $(".ItemGuid").val(item);
                  }

                  //save the item if it's a new induction, save any updates for a reinduciton
                  $.post("SaveItem", $("#policyForm").serialize() + "&auditItem=" + reinductItem + getAntiForgeryToken(), function (dataBool)
                  {
                     if (dataBool === "False")
                     {
                        disableBinDesignation("Controller call failed on Item save. Please clear your screen and re-process Item GUID " + item + ".");
                        return;
                     }

                     //put the item in the inbound container if a record doesn't already exist
                     if (!isItemInData.isItemIn)
                     {
                        inbound.put(item, function (data)
                        {
                           //if, for some reason, the item initially does not get placed into a tote, then disable all buttons that would
                           //allow the user to finish the bin designation process
                           if (!data.success)
                           {
                              disableBinDesignation("Controller call failed on Tote Item save. Please clear your screen and re-process Item GUID " + item + ".");
                              return;
                           }

                           //get the outbound container
                           var containerType = $("#sortBinType").val();
                           var container = getGaylord(containerType);

                           //if the required container is not already attached, get a reference to it and make it visible
                           if (container !== undefined && container !== null && ($(".txtSortBin").val() === "" || $(".txtSortBin").val() !== container.id) && (gaylords.includes(containerType)))
                           {
                              container.show();
                           }
                        });
                     }

                     readyForPut = true;
                     return;
                  });
               }
               else
               {
                  readyForPut = true;
                  return;
               }
            });
         }
      }
   }

   //Puts the item into the designated container
   //called from checkOpenContainer
   var putContainer = function (container)
   {
      var item = $(".itemIdHidden").val();
      //if the item is not ready (not in an inbound bin yet)
      //wait 200 milliseconds and check again
      if (!readyForPut)
      {
         setTimeout(200, function ()
         {
            putContainer(container);
         });

         return;
      }

      var grnBinOverride = $(".GreenBinOverrideReasonHidden").val();

      //if green bin reasons have been added after designation is returned,
      //the item needs to be updated with the new override reasons
      if (grnBinOverride !== "Override" && grnBinOverride !== "" && grnBinOverride !== "0")
      {
         //post to save greenbin override reasons
         $.post("SaveItem", $("#policyForm").serialize() + "&auditItem=" + false + getAntiForgeryToken());
      }

      container.put(item, function (data2)
      {
         if (data2.success)
         {
            executeMethods("onDesignationMethods", data2);
            $(".actionTool [value=Clear]").click();
            $(".ndcField").select();
         }
         else
         {
            $("#designationError").html(data2.error);
            $(".txtSortBin").select();
            $(".sortBinSubmit").enable();
         }
      });
   }

   //shows error message when scanned container does not match
   //the required gaylord
   function buildGaylordError(container)
   {
      if (!gaylords.includes(container.type))
      {
         return "";
      }

      var message = "The gaylord must be Ctrl ";

      switch (container.ctrlNo)
      {
         case 345:
            message += "III-V";
            break;
         case 2:
            message += "II";
            break;
         default:
            message += "0";
            break;
      }

      if (container.profileCode > 0)
      {
         message += ` for ${raInformation.Profile.Name}`;
      }

      if (container.recallId > 0)
      {
         message += ` for recall ${container.recallId}`;
      }

      return message;
   }

   //*****************************************************************************************
   //*
   //* Summary:
   //*   Get Machine Location of the station
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************************
   function getMachineLocation()
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "machineName=" + stationId + getAntiForgeryToken(),
         async: false,
         url: "GetMachineLocation",
         success: function (data)
         {
         }
      });
      return request.responseJSON;
   }

   //Function called when the user scans or submits an outbound container
   //backup type is used when multiple container types are interchangeable 
   //such as small and large gaylords
   var checkOpenContainer = function (backupType)
   {
      var grnBinOverride = $(".GreenBinOverrideReasonHidden").val();

      //if an item is overriden to green bin but no reasons have been selected
      if ($("#sortBinType").val() === "GRN_BIN" && window.sortBinType !== "GRN_BIN" && (grnBinOverride === "Override" || grnBinOverride === "" || grnBinOverride === "0"))
      {
         $("#designationError").html("Green Bin reasons are required");
         $(".sortBinSubmit").enable();
         return;
      }

      //if a backupType is defined, use it in place of the server designated sort bin
      var type = $("#sortBinType").val();

      if (backupType !== undefined && backupType !== null && typeof backupType === "string" && backupType !== "")
      {
         type = backupType;
      }

      var containerId = $(".txtSortBin").val();
      var container = getContainer(type, containerId);

      //if the scanned container is not attached, try to open it
      if (container !== undefined && container.containerId === "")
      {
         container.open(containerId, function (data)
         {
            if (data.success)
            {
               //check that the newly opened container matches the required container
               if (container.containerId === "")
               {
                  container = getContainer(type, containerId);

                  if (container.containerId === "")
                  {
                     sidebar.containers.find(c => c.containerId === containerId).close();
                     var message = buildGaylordError(container);
                     $("#designationError").html(message);
                     $(".sortBinSubmit").enable();
                     return;
                  }
               }

               putContainer(container);
            }
            else
            {
               //if the scanned container doesn't open successfully and it is a c0Gaylord,
               //try opening the other c0Gaylord (small or large)
               if (c0Gaylords.includes(container.type) && type !== backupType)
               {
                  container.control.find(".containerId").val("");
                  checkOpenContainer(c0Gaylords.filter(x => x !== type)[0]);
               }
               else
               {
                  container.control.find(".containerId").val("");
                  $(".txtSortBin").select();

                  if (data.error.split("(")[1] === "LRG_GAYLRD)" || data.error.split("(")[1] === "SML_GAYLRD")
                  {
                     data.error = data.error.split("(")[0] + "(LRG_GAYLRD) or (SML_GAYLRD)";
                  }

                  $("#designationError").html(data.error + "</br>" + buildGaylordError(getContainer(type)));
                  $(".sortBinSubmit").enable();
               }
            }
         });
      }
      else if (container === undefined)
      {
         $("#designationError").html(" The requested container could not be found");
         $(".sortBinSubmit").enable();
         $(".txtSortBin").select();
      }
      else
      {
         putContainer(container);
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   If a user has an OB container type open for designation, and the user is trying to put the item into another container of the same type, then close the previous container first
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function closeMatchingContrType(contrType, contrId)
   {
      $(this).closeMessageBox();
      contrId = contrType === "SML_GAYLRD" ? "" : contrId.toUpperCase();
      var container = getContainer(contrType, contrId);
      container.close(null, function (data)
      {
         if (data.success)
         {
            checkOpenContainer();
         }
         else
         {
            $("#designationError").html(data.error);
            $(".sortBinSubmit").enable();
            return;
         }
      });
   }
   window.closeMatchingContrType = closeMatchingContrType;

   //checks if item is foreign to the RA it was included in and 
   //makes necessary updates
   window.isForeignContainer = function ()
   {
      //if the profile was loaded on the tote, use that profile value
      var raProfile = "0";

      if ($("#ToteProfileCode").length &&
         $("#ToteProfileCode").val() !== undefined &&
         $("#ToteProfileCode").val() !== "" &&
         $("#ToteProfileCode").val() !== "0")
      {
         raProfile = $("#ToteProfileCode").val();
      }
      //if the window.raInfomation is loaded and the item matches, use that profile value
      else if ($("#ItemGUID").length && $("#ItemGUID").val() !== "") 
      {
         if (window.raInformation !== undefined && typeof window.raInformation.itemGuid !== 'undefined' && window.raInformation.itemGuid.toLowerCase() == $("#ItemGUID").val().toLowerCase())
         {
            raProfile = window.raInformation.Profile.ProfileCode;
         }
      }

      //if the profile does not match the item's manufacturer, it is a foreign container
      // ReSharper disable once CoercedEqualsUsing return from ajax is int, policyForm is string, coerce to avoid issues
      if (raProfile !== undefined && raProfile != "" && raProfile != "0" &&
         $(".productProfileCodeHidden").val() !== "" && $(".productProfileCodeHidden").val() !== "0" &&
         $(".productProfileCodeHidden").val() != raProfile)
      {
         $(".IsForeignContainer").val("True");
         return true;
      }

      $(".IsForeignContainer").val("False");
      return false;
   }

   //function handling bin designation loading with a designated sort container
   //called from _BinResult.cshtml
   var onDesignation = function ()
   {
      executeMethods("onDesignationLoadMethods");
      window.sortBinType = $("#sortBinType").val();
      var containerType = $("#sortBinType").val();
      var containerId = "";
      var item = $(".itemIdHidden").val();

      if (!$(".ItemGuid").val())
      {
         $(".ItemGuid").val(item);
      }

      //if the sort bin is a gaylord and one is attached, populate the containerId
      //otherwise show an empty gaylord segment so it can be requested
      if (c0Gaylords.includes(containerType))
      {
         var otherGaylord = c0Gaylords.filter(x => x !== containerType)[0];
         var container = getContainer(containerType);
         var otherContainer = getContainer(otherGaylord);

         if (container !== undefined && container !== null && container.containerId !== "")
         {
            containerId = container.containerId;
         }
         else if (otherContainer !== undefined && otherContainer !== null && otherContainer.containerId !== "")
         {
            containerId = otherContainer.containerId;
         }

         $(".txtSortBin").val(containerId);

         if (containerId === "")
         {
            getContainer("SML_GAYLRD").show();
         }
      }
      else if (containerType === "ORG_GAYLRD")
      {
         var orangeContr = getContainer(containerType);

         if (orangeContr !== undefined && orangeContr !== null && orangeContr.containerId !== "")
         {
            containerId = orangeContr.containerId;
            $(".txtSortBin").val(containerId);
         }
         if (containerId === "")
         {
            getContainer("ORG_GAYLRD").show();
         }
      }

      //put focus in scan field
      $(".txtSortBin").select();
      //scroll to the top of the bin designation segment
      $(".ql-body.active").animate({ scrollTop: $("#binDesignation").offset().top - $(".ql-body.active").find("div:first").offset().top - 20 });
   }
   window.onDesignation = onDesignation;

   //handle user submission of a container matching the sort designation
   var submitSortBin = function ()
   {
      if (!readyForPut)
      {
         setTimeout(200, function ()
         {
            submitSortBin();
         });

         return;
      }

      //prevent double-clicking of the designation picture, so that we don't create a second item put instance
      if ($(".sortBinSubmit").attr("disabled") !== undefined && $(".sortBinSubmit").attr("disabled") !== null)
      {
         return;
      }

      $(".sortBinSubmit").disable(); //disable the designation image button

      var type = $("#sortBinType").val();
      var containerId = $(".txtSortBin").val();
      var otherGaylord = null;

      if (containerId.trim() === "")
      {
         $("#designationError").html("Sort Bin ID cannot be empty");
         $(".sortBinSubmit").enable();
         $(".txtSortBin").select();
         return;
      }

      var container = getContainer(type, "", containerId.toUpperCase()); //close the current container of the designation type if a different container id is input
      var destinationContrType = sidebar.getContrType(containerId.toUpperCase()); //get the container type of the scanned container barcode

      //Attempt to make gaylords behave like other container types.  For small gaylord desigations check for already  
      //open large gaylords and close if necessary.
      if (type === "SML_GAYLRD")
      {
         otherGaylord = getContainer("LRG_GAYLRD", "");

         if (otherGaylord.containerId !== containerId && otherGaylord.attribute !== "I" && otherGaylord.isOpen())
         {
            otherGaylord.close();
         }
      }

      //user is attempting to open different outbound container when this container type is already open
      if (container.containerId !== containerId && container.attribute !== "I" && destinationContrType !== undefined && container.type === destinationContrType && container.isOpen())
      {
         var newContr = containerId.toUpperCase();
         var oldContr = container.containerId.toUpperCase();

         var messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " +
            "A " + container.title + " {" + oldContr + "} is already attached to this work station. Would you like sort the item to the new " +
            container.title + " {" + newContr + "}?</h3><script> $('.close-button-messageBox').hide()</script>"
         }];

         var btnObjects = [{
            "id": "btnSameContrType", "name": "Yes", "function": "onclick='window.closeMatchingContrType(\"" + type + "\", \"" + oldContr + "\");'", "class": "btnErrorMessage", "style": "margin:0 7px;text-align:center;width:70px"
         }, {
            "id": "btnCancel", "name": "No", "function": "onclick='$(this).closeMessageBox(); $(\".txtSortBin\").val(\"\").focus()'", "class": "btnErrorMessage", "style": "margin:0 7px;text-align:center;width:70px"
         }];

         var titleobjects = [{ "title": "Bin Designation Container Type" }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);
         $("#designationError").html("");
         $(".sortBinSubmit").enable();
         return false;
      }
      else
      {
         $("#sortBinType").val(container.type);
         checkOpenContainer();
      }
   }

   //handle return keypress in scan field
   $(document).on("keypress", ".txtSortBin", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;

      if (key === 13)
      {
         submitSortBin();
      }
   });

   //handle click on container image to submit
   $(document).on("click", ".sortBinSubmit", function ()
   {
      if ($(this).attr("disabled") === undefined)
      {
         submitSortBin();
      }
   });

   //induction tote item count must be displayed when the tote is closed
   //this is added to sidebar.onToteClose in setupWarehouseInduction
   function displayToteCount(count)
   {
      var container = sidebar.getInboundContainer();
      var toteCount = count !== undefined ? count : container.count;

      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " +
         toteCount +
         " Items Processed</h3>" +
         "<script>$('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue_tote", "name": "Continue", "function": "onclick='$(this).closeMessageBox();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Tote Final Count" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
      $(".actionTool [name=Clear]").click();
   }
   window.displayToteCount = displayToteCount;

   $(document).on("click", "#btn_continue_tote", function ()
   {
      $(".requiredContainerId").select();
   });

   //shows message if a user's station is not registered in the database
   var showLocationWarning = function ()
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>PC Registration Not Found. Please Contact Administrator.</h3>" +
            "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue",
         "function": "onclick='$(this).closeMessageBox(); setTimeout(function (){ $(\".requiredContainerId\").focus().select(); }, 150)'",
         "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Application Cannot Process" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //shows message if a user's station that's on the floor got into a cage applet (C2, C345)
   var showAppWarning = function ()
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>This application cannot be used on the floor.<br/>A manager is required to continue.</h3>" +
         "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue",
         "function": "onclick='$(this).closeMessageBox(); setTimeout(function (){ $(\".requiredContainerId\").focus().select(); }, 150)'",
         "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Application Cannot Process" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //shows message if a user is attempting to open a tote that doesn't correspond to the application
   var showToteWarning = function (msg)
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>" + msg + "</h3>" +
         "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue",
         "function": "onclick='$(this).closeMessageBox(); setTimeout(function (){ $(\".requiredContainerId\").focus().select(); }, 150)'",
         "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Application Cannot Process" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //shows message if an item's control number does not match 
   //the application's control level
   var showCtrlWarning = function ()
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>Control " + $(".CtrlNumberHidden").val() + " items cannot be processed with this application.<br/>A manager is required to continue.</h3>" +
         "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue", "function": "onclick='$(this).closeMessageBox(); $(\".actionTool [name=Clear]\").click();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Application Cannot Process" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //generic warning popup
   window.showInductionWarning = function (message)
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>" + message + "</h3>" +
         "<script> $('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue", "function": "onclick='$(this).closeMessageBox(); $(\".actionTool [name=Clear]\").click();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{
         "title": "Cannot Re-induct Item"
      }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //prevents search from continuing if a floor user is in a cage-specific application
   var checkAppLocation = function ()
   {
      //do not try to open container in sidebar if the PC is not registered
      if (location === "" || location === undefined)
      {
         showLocationWarning();
         $(".QoskBody").attr("disableApp", "true");
         $("#SubmitSearch").attr("data-disable", true);
         return "PC Registration Not Found. Please Contact Administrator.";
      }

      var appCtrl = getParameterByName("ctrl");

      if (appCtrl !== null && appCtrl !== undefined && appCtrl !== "0" && location === "0")
      {
         showAppWarning();
         $(".QoskBody").attr("disableApp", "true");
         $("#SubmitSearch").attr("data-disable", true);
         return "This application cannot be used on the floor";
      }

      return "";
   }

   var checkTotes = function (d)
   {
      var appCtrl = getParameterByName("ctrl");
      var tp = d !== undefined && d.length > 0 ? d.substring(0, 2).toUpperCase() : "";
      var stringBuilder = "";

      if (tp === "VT" && appCtrl !== "2")
      {
         stringBuilder = "CII totes cannot be processed with this application." + (location === "0" ? "<br/>Sort this tote immediately to the Vault." : "");
         showToteWarning(stringBuilder);
         $("#SubmitSearch").attr("data-disable", true);
      }
      else if (tp === "RT" && appCtrl === "2")
      {
         stringBuilder = "Induction totes cannot be processed with this application.";
         showToteWarning(stringBuilder);
         $("#SubmitSearch").attr("data-disable", true);
      }

      return stringBuilder;
   }

   //scan a black bin for C2 RAs and attempt to open the black bin
   var openBlackBin = function (containerId)
   {
      var type = "BLK_BIN";
      var container = getContainer(type, containerId);

      //if the scanned container is not attached, try to open it
      if (container !== undefined && container.containerId === "")
      {
         container.open(containerId, function (data)
         {
            if (data.success)
            {
               $(this).closeMessageBox();

               //check that the newly opened container matches the required container
               if (container.containerId === "")
               {
                  container = getContainer(type, containerId);

                  if (container.containerId === "")
                  {
                     sidebar.containers.find(c => c.containerId === containerId).close();
                     var message = buildGaylordError(container);
                     $("#CIIMatchingError").html(message);
                     return;
                  }
               }

               setTimeout(function()
               {
                  $(".ndcField").focus();
               }, 200);
            }
            else
            {
               container.control.find(".containerId").val("");
               $(".blackBinIdRequired").select();

               if (data.error.split("(")[1] === "LRG_GAYLRD)" || data.error.split("(")[1] === "SML_GAYLRD")
               {
                  data.error = data.error.split("(")[0] + "(LRG_GAYLRD) or (SML_GAYLRD)";
               }

               $("#CIIMatchingError").html(data.error + "</br>" + buildGaylordError(getContainer(type)));
            }
         });
      }
      else if (container === undefined)
      {
         $("#CIIMatchingError").html("The requested container could not be found");
         $(".blackBinIdRequired").select();
      }
   }

   //prevents search from continuing if an RA control number does not match the application control number
   var checkRaRel = function (d, raCtrl)
   {
      var appCtrl = getParameterByName("ctrl");
      var stringBuilder = "";

      if (raCtrl !== undefined && appCtrl !== undefined && raCtrl !== appCtrl)
      {
         stringBuilder = "The RA Control Number (" + raCtrl + ") does not match the Application Control Number (" + appCtrl + ")." +
            (raCtrl !== "0" && location === "0" ? "<br/>Sort this tote immediately to the " + (raCtrl === "2" ? "Vault." : "Cage.") : "");
         showToteWarning(stringBuilder);
         $("#SubmitSearch").attr("data-disable", true);
      }

      return stringBuilder;
   }

   //if user continues inducting a foreign container 
   $(document).on("click", ".foreignContainerToteSubmit", function ()
   {
      if ($(".foreignContainerToteId").val() === "")
      {
         $(".foreignContainerToteId").focus();
      }

      if ($(".foreignContainerToteId").val().trim() === sidebar.getInboundContainer().containerId)
      {
         //if there aren't recalls on this item, set lot and expiration to N/A
         if ($(".IsRecallPossible").val() === "False")
         {
            $("#btnNoLot").click();
            $("#btnNoExp").click();
         }

         //simulate the container type selection for a foreign container inner product
         if ($(".scroll-container-type").attr("contr-type") !== undefined && $(".scroll-container-type").attr("contr-type") === "true")
         {
            $("input[type=radio][value='" + $(".scroll-container-type").attr("contr") + "']").click();
         }

         $(this).closeMessageBox();
      }
      else
      {
         var ibContr = $(".QoskBody").attr("AppCode") !== "WAREHS_AUDIT" ? "tote" : "bin";
         $(".foreignContainerError").html("Scanned " + ibContr + " does not match");
         $(".foreignContainerToteId").focus().select();
      }
   });

   $(document).on("click", ".closeButtonClears", function ()
   {
      $(".actionTool [name=Clear]").click();
   });

   $(document).on("keypress", ".foreignContainerToteId", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;

      if (key === 13)
      {
         $(".foreignContainerToteSubmit").click();
      }
   });

   //warning message for when inducting a foreign container
   var showForeignContainerWarning = function ()
   {
      var ibContr = $(".QoskBody").attr("AppCode") !== "WAREHS_AUDIT" ? "Tote" : "Bin";
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> To induct as a foreign container, scan the current " + ibContr.toLowerCase() + " to verify" +
         "<br/><br/><input class=\"foreignContainerToteId scannerRequired text-box single-line\" style=\"height: 22px;\" type=\"text\" autocomplete=\"off\">" +
         "   <input type=\"button\" id=\"btn_continue\" value=\"Open\" class=\"btnErrorMessage foreignContainerToteSubmit\" style=\"margin:0 auto;text-align:center\"></h3>" +
         "<br/>" +
         "<span class='foreignContainerError' ></span>" +
         "<script>" +
         "setTimeout(function(){$('.foreignContainerToteId').select();},10);" +
         "$(\".close-button-messageBox\").addClass(\"closeButtonClears\");" +
         "</script>"
      }];
      var titleobjects = [{ "title": "Foreign Container in " + ibContr }];
      $(this).addMessageButton([], messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //warning message for when opening a special handling tote
   //note that toteAction is to determin whether this warning came up on a tote open or close
   //if it's on a close, then we need to run the function to display the tote count (at the end of processing)
   var showSpecialHandlingWarning = function (toteAction, toteOrItem = "Tote")
   {
      var toteCount = sidebar.getInboundContainer().count;
      var mfgr = $("#MfrName").val() !== undefined ? $("#MfrName").val() : $(".mfgrField")[0].value;

      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>" + mfgr + " requires special handling instructions. If you have not been trained to process this manufacturer, please see your line lead.</h3>" +
         "<script>$('.close-button-messageBox').hide()</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Acknowledge", "function": "onclick='$(this).closeMessageBox();" + (toteAction === "open" ? "" : " window.displayToteCount(" + toteCount + ");") + "'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Special Handling " + toteOrItem }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //prompt the user to scan a black bin whenever a C2 tote is opened
   var getC2BlackBin = function ()
   {
      //if we have a black bin open, no need to open another one
      if (getContainer("BLK_BIN").isOpen())
      {
         return false;
      }

      var messageObject = [{
         "id": "lbl_messages",
         "name":
               "<h3 style='margin:0 auto;text-align:center;width:100%'>This C2 RA requires a Black Bin. Please scan a Black Bin to continue." +
               "<br/><br/><input class='blackBinIdRequired scannerRequired text-box single-line' style='height: 22px;' type='text' autocomplete='off'>" +
               "   <input type='button' id='btn_continue' value='Open' class='btnErrorMessage blackBinSubmit' style='margin:0 auto;text-align:center'></h3>" +
               "<br/><div class='tdMessage' style='padding-left:33px'><table><tr>" +
               "<td style='margin:0 auto;width:100%;color:maroon;font: 16px eras_medium,\"Eras Medium ITC\",sans-serif;' " +
               "id='CIIMatchingError'></td></tr></table></div>" + 
               "<script>setTimeout(function(){$('.blackBinIdRequired').select();},10);" +
               "$('.close-button-messageBox').hide()</script>"
      }];
      var titleobjects = [{ "title": "C2 Black Bin Required"}];
      $(this).addMessageButton([], messageObject);
      $(this).showMessageBox(titleobjects);
   }

   //show appropriate elements when greenbin override is selected
   $(document).on("click", ".greenBinOverride", function ()
   {
      $(".originalSort").hide();
      $(".greenBinSort").show();
      $("#sortBinType").val("GRN_BIN");
      $(".txtSortBin").select();
   });

   //show appropriate elements when greenbin override is cancelled
   $(document).on("click", ".cancelOverride", function ()
   {
      $(".originalSort").show();
      $(".greenBinSort").hide();
      $(".GreenBinOverrideReasonHidden").val("0");
      $("#sortBinType").val(window.sortBinType);
      $(".txtSortBin").select();
   });

   //toggle a green bin reason 
   $(document).on("click", ".greenBinReason.unchecked", function ()
   {
      var reason = $(".GreenBinOverrideReasonHidden");
      window.toggleImage($(this).find("img"));
      reason.val(reason.val() + $(this).attr("data-reason"));
      $(this).addClass("checked");
      $(this).removeClass("unchecked");
   });

   //toggle a green bin reason
   $(document).on("click", ".greenBinReason.checked", function ()
   {
      var reason = $(".GreenBinOverrideReasonHidden");
      window.toggleImage($(this).find("img"));
      reason.val(reason.val().replace($(this).attr("data-reason"), ""));
      $(this).addClass("unchecked");
      $(this).removeClass("checked");
   });

   //scan a black bin for C2 RAs and attempt to open the black bin
   $(document).on("click", ".blackBinSubmit", function ()
   {
      openBlackBin($(".blackBinIdRequired").val());
   });
   $(document).on("keydown", ".blackBinIdRequired", function (e)
   {
      if (e.keyCode === 32) //don't allow spaces
      {
         return false;
      }

      if (e.keyCode === 13)
      {
         openBlackBin($(".blackBinIdRequired").val());
      }
   });

   //display the ra number and manufacturer on the inbound tote
   var showRaInformation = function ()
   {
      //when we open/close bins/gaylords for bin designation in processing, we still run the GetRAInformation below, which leads to the special handling warning always showing up
      //bit ugly, but this is one way to suppress the warning if the function is executed due to an outbound container being opened/closed as opposed to a tote open/close
      //the else statement is for when we attempt to close a gaylord at any time in the process, but before a designation occurs (before an item is entered in the product search, during processing, etc)
      var suppressWarning = false;

      //this implies we have the bin designation segment that has returned a designation
      if ($(".txtSortBin").length)
      {
         suppressWarning = true;

         if (gaylords.includes($("#sortBinType").val()))
         {
            getContainer($("#sortBinType").val()).show();
         }
      }
      else
      {
         suppressWarning = gaylordClosed;
      }

      var tote = sidebar.getInboundContainer();

      //this should be true at processing only since it's the only place where a tote is an inbound container
      if (tote.typeGroup !== "Tote")
      {
         return;
      }

      if (tote.isOpen())
      {
         console.log("GetRaInformation: Calling from JS");
         $.post("GetRaInformation", "&containerId=" + tote.containerId + "&instanceId=" + tote.instanceId + getAntiForgeryToken(), function (result)
         {
            console.log("GetRaInformation: Returned info to JS");
            window.raInformation = result;
            tote.control.find(".counter").after("<tr><td>RA: </td><td>" +
               result.ReturnAuthorization.RtrnAuthNumber +
               "<input id=\"RaCtrlCode\" value=\"" +
               result.ReturnAuthorization.RtrnAuthCtrlCode +
               "\" hidden/>  </td></tr>" +
               "<tr><td>MFG: </td><td>" +
               result.Profile.Name +
               "<input id=\"MfrName\" value=\"" +
               result.Profile.Name +
               "\" hidden/>" +
               "<input id=\"RaDebitMemo\" value=\"" +
               result.DebitMemo.DebitMemoNumber +
               "\" hidden/>" +
               "<input id=\"ToteProfileCode\" value=\"" +
               result.DebitMemo.MfrProfileCode +
               "\" hidden/>" +
               "<input id=\"SpecialHandling\" value=\"" +
               result.Profile.SpecialHandling +
               "\" hidden/></td></tr>");

            if ($(".QoskBody").attr("disableApp") === "false" && $("#RaCtrlCode").val() !== undefined)
            {
               //Check the control number of the RA against the application control number
               if (!suppressWarning)
               {
                  checkRaRel(tote.containerId, $("#RaCtrlCode").val().toLowerCase().replace("ctrl", ""));
               }

               //default data-disable to false if it's undefined - done moreso on inital page load when there is a tote already opened, as opposed to opening a tote once the page is loaded
               if ($("#SubmitSearch").attr("data-disable") === undefined)
               {
                  $("#SubmitSearch").attr("data-disable", false);
               }

               //Show special handling warning, so that user is required to acknowledge it
               if ($("#SpecialHandling").val().toLowerCase() === "true" && $("#SpecialHandling").val() !== undefined && $("#SubmitSearch").attr("data-disable").toLowerCase() === "false" && !suppressWarning)
               {
                  showSpecialHandlingWarning("open");
               }

               //for a C2 Tote, we need a black bin attached from the beginning
               if ($("#RaCtrlCode").val().toLowerCase().replace("ctrl", "") === "2" && getParameterByName("ctrl") === "2" && !suppressWarning)
               {
                  getC2BlackBin();
               }

               gaylordClosed = false;
            }
         });
      }
   }

   //set focus on the ndc input field after a message box is closed
   $(document).on("click", "#btn_continue,.close-button-messageBox", function ()
   {
      $(".ndcField").focus();
   });

   window.clearInduction = function ()
   {
      $(".actionTool [name=Clear]").click();
   }

   //populate the requested gaylord id after it is created
   var gaylordRequestMethod = function (data)
   {
      var shortList = sidebar.containers.filter(c => c.type === data.container.find("[data-container-field='type']").val());
      var container = shortList.find(c => !c.isOpen());
      container.open(data.containerId);
      if (($("#sortBinType").val() === container.type || (gaylords.includes($("#sortBinType").val()) && gaylords.includes(container.type))))
      {
         $(".txtSortBin").val(data.containerId);
         $(".txtSortBin").select();
      }
   }

   $(document).on("ready", function ()
   {
      sidebar.onRequest(gaylordRequestMethod);
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
      {
         stationId = data;
         location = location === "" ? getMachineLocation().Location : location;
      });
   });

   //loads rainformation when bin designation is loaded
   //called form _BinResult.cshtml
   window.setupRaForBinDesignation = function ()
   {
      if ($("#ItemGUID").length && $("#ItemGUID").val() !== "" && $(".ItemGuid").val() === "")
      {
         $(".ItemGuid").val($("#ItemGUID").val());
      };

      var itemGuid = $("#ItemGUID").val();

      //if an item is selected from a binventory, load the RA it is tied to unless it is already loaded
      if ($("#ToteProfileCode").val() === undefined && $("#ItemGUID").length && $("#ItemGUID").val() !== ""
         && (window.raInformation === undefined || window.raInformation.itemGuid.toLowerCase() !== itemGuid.toLowerCase()))
      {
         var request = $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: "itemGuid=" + itemGuid + getAntiForgeryToken(),
            url: "/Product/UPA/GetRaForItem",
            async: false
         });

         window.raInformation = request.responseJSON;
         window.raInformation.itemGuid = itemGuid;
      }
   }

   //Performs warehouse induction specific functions when the policy tab is loaded
   //called in _SegmentBinDesignation.cshtml
   var setupWarehouseInduction = function ()
   {
      //add required sidebar event methods
      sidebar.beforeOpen(function (contrId)
      {
         var status = checkAppLocation();

         if (status === "")
         {
            status = checkTotes(contrId);
         }

         return status;
      });
      sidebar.beforeClose(function ($div)
      {
         //this is done more-so when we close an outbound bin
         //isComplete is used to check if the Tote has been processed
         if ($div.find("[data-container-field=attribute]").val() === "O")
         {
            return true;
         }

         var toteList = sidebar.isComplete();

         if (toteList !== undefined && toteList.length === 0)
         {
            return disableSubmitSearch($div);
         }

         var guidMsg = "";
         var listLen = toteList.length - 1 > 4 ? 5 : toteList.length; //limit list top the first 5 entries so that the popup msg doesn't run off-screen

         for (var i = 0; i < listLen; i++)
         {
            guidMsg += "<h3 style='margin:0 auto;text-align:center;width:100%'>" + toteList[i].ItemGUID + "</h3>";
         }

         //show all item GUIDs that are stuck in "limbo" i.e. in totes but not bins/gaylords
         var messageObject = [{
            "id": "lbl_messages",
            "name": "<h3 style='margin:0 auto;text-align:center;width:100%'> " +
            "Please process all " + toteList.length + " item(s) before closing this tote. The following item(s) have not been designated for an outbound container"
            + (toteList.length - 1 > 4 ? " (only the first 5 items are shown):" : ":")
            + guidMsg + "  </h3>" +
            "<script> $('.close-button-messageBox').hide()</script>"
         }];
         var btnObjects = [{
            "id": "btnItemList", "name": "Continue", "function": "onclick='$(this).closeMessageBox(); $(\".ndcField\").select();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
         }];
         var titleobjects = [{ "title": "Items Not Processed" }];
         $(this).addMessageButton(btnObjects, messageObject);
         $(this).showMessageBox(titleobjects);

         return false;
      });
      sidebar.onBinOpen(enableSubmitSearch);
      sidebar.onToteOpen(function ()
      {
         //disableApp would be true if checkAppLocation function comes back with an error (i.e. floor machine accessing a cage applet)
         if ($(".QoskBody").attr("disableApp") === "false")
         {
            enableSubmitSearch();
         }
      });

      sidebar.onTotePause(clearInduction);
      sidebar.onGaylordClose(function(status)
      {
         gaylordClosed = true;
      });
      var container = window.sidebar.getInboundContainer();

      //disable search if no inbound container is attached
      if (container.containerId === "")
      {
         $("#SubmitSearch").attr("data-disable", true);
      }

      //prevent an item guid tied to another tote to be processed with the currently-attached tote
      if ($(".InboundContainerIdHidden").val() !== "" && $(".InboundInstanceIdHidden").val() !== ""
         && container.typeGroup === "Tote"
         && $(".InboundContainerIdHidden").val() !== container.containerId
         && $(".InboundInstanceIdHidden").val() !== container.instanceId)
      {
         showInductionWarning("Item is from a different induction tote and cannot be re-inducted");
         return;
      }

      sidebar.onToteClose(function ()
      {
         //Show special handling warning, so that user is required to acknowledge it
         //otherwise, display the tote count
         if ($("#SpecialHandling").val() !== undefined && $("#SpecialHandling").val().toLowerCase() === "true")
         {
            showSpecialHandlingWarning("close");
         }
         else
         {
            displayToteCount();
         }
      });

      var initMethodLength = window.initMethods.length;
      sidebar.onInit(showRaInformation);

      if (window.initMethods.length > initMethodLength)
      {
         if (checkAppLocation() === "")
         {
            checkTotes(container.containerId);
            showRaInformation();
         }
      }

      $(".InboundContainerIdHidden").val(container.containerId);
      $(".InboundInstanceIdHidden").val(container.instanceId);
      var appCtrl = getParameterByName("ctrl");

      if ($(".CtrlNumberHidden").val() !== "" && (($(".CtrlNumberHidden").val() === "2" && appCtrl !== "2")
         || (appCtrl !== null && !appCtrl.includes($(".CtrlNumberHidden").val()) && !isForeignContainer())))
      {
         showCtrlWarning();
      }
      else if (isForeignContainer() && ($("#ToteProfileCode").val() !== undefined || window.raInformation !== undefined))
      {
         showForeignContainerWarning();
      }
      else if ($(".QoskBody").attr("AppCode") === "WAREHS_AUDIT" && $(".SpecialHandlingHidden").val().toLowerCase() === "true")
      {
         showSpecialHandlingWarning("open", "Item");
      }
   }
   window.setupWarehouseInduction = setupWarehouseInduction;
});