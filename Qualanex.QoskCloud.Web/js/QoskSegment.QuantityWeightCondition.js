﻿//*****************************************************************************
//*
//* QoskSegment.WtQty.js
//*    Copyright (c) 2016 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Local definitions
   //-----------------------------------------------------------------------------
   const segmentId = "weightQuantityCondition";
   const segmentSelector = "#" + segmentId;
   const calibrationThreshold = 5;
   const weightTolerance = .10; //tolerance percent
   const appCodeDss = "PHARMA_DECISION";
   window.overrideCalibrationWarning = false;
   var consecutivePrdWtViol = false;
   var isCameraOff = false;
   var failedSave = false;

   // Segment violations - Weight related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_WEIGHT = "The content weight cannot be greater than the product weight, please update the weights.";
   ERR_NOCONTRWT = "Container weight for this product cannot be found. Calculation cannot occur. Please weight its contents manually.";
   ERR_INVALIDPRODUCTWT = "The product weight is invalid. Please capture or re-enter the weight.";
   ERR_INVALIDCONTENTWT = "The content weight is invalid. Please capture or re-enter the weight.";
   ERR_MISSINGPRODUCTWT = "The product weight is required.";
   ERR_MISSINGCONTENTWT = "The content weight is required.";
   ERR_ZEROPRODUCTWT = "Product weight must be greater than zero.";
   ERR_FULLCONTRPRODUCTWT = "Product weight falls outside the expected range for the product.";
   ERR_CONTRPRODUCTWT = "Container weight cannot exceed the product weight.";
   ERR_INDCONTPRODUCTWT = "Individual count weight cannot exceed the product weight.";

   // Segment violations - Quantity related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_INVALIDPACKAGE = "The package count is invalid. Please re-enter the value.";
   ERR_INVALIDINDIVIDUAL = "The individual count is invalid. Please re-enter the value.";
   ERR_INVALIDQTY = "The quantity is invalid. Please re-enter the value.";
   ERR_INVALIDCASE = "The case count is invalid. Please re-enter the value.";
   ERR_INVALIDCASESIZE = "The case size is invalid. Please re-enter the value.";
   ERR_MISSINGPACKAGE = "The package count is required.";
   ERR_MISSINGINDIVIDUAL = "The individual count is required.";
   ERR_MISSINGQTY = "The quantity is required.";
   ERR_MISSINGCASE = "The case count is required.";
   ERR_MISSINGCASESIZE = "The case size is required.";
   ERR_NOINDIVIDUALWT = "The individual count weight for this product cannot be found. Calculation cannot occur. Manual count is required.";
   ERR_ZEROQTY = "Quantity count entered is invalid. Quantity cannot be a negative number. Please re-enter the value.";

   // Segment violations - Hardware related
   ///////////////////////////////////////////////////////////////////////////////
   ERR_NOSCALE = "There was an error connecting to the scale, please enter weight values manually.";
   ERR_NOACK = "There was an error communicating with the scale, please enter weight values manually.";
   ERR_CALIBRATE = "The scale needs to be calibrated prior to use. <a onclick='launchCalibration();'> Click here to begin.</a>";
   ERR_NOCONTENTIMAGE = "Please capture content image.";
   ERR_FAILEDTOSAVEIMAGE = "The captured image could not be saved, please recapture.";

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var showWtQtyViolation = function (serverity, violation)
   {
      var messageObj = $(segmentSelector + " .tdMessage .tdCaption");

      // Clear any old messages and/or violations
      ///////////////////////////////////////////////////////////////////////////////
      messageObj.removeClass("error warning general");
      messageObj.text("");

      // Display the message and/or violation
      ///////////////////////////////////////////////////////////////////////////////
      switch (serverity)
      {
         case SEG_GENERAL:
            messageObj.addClass("general");
            break;

         case SEG_WARNING:
            messageObj.addClass("warning");
            break;

         case SEG_ERROR:
            messageObj.addClass("error");
            break;
      }

      messageObj.html(violation);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.showWtQtyViolation = showWtQtyViolation;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function captureContentImage(retrying)
   {
      if (!webcamsLoaded)
      {
         if (!retrying)
         {
            setTimeout(function () { captureContentImage(true); }, 500);
         }

         return;
      }
      if ($("#contentCamera").length)
      {
         deleteImage($("ContentImageUriHidden").val());

         var contentImage = capture($("#contentCamera")[0], function (contentUri, tempLink)
         {
            console.log(contentUri);

            if (contentUri === "error" || contentUri === undefined)
            {
               showWtQtyViolation(SEG_ERROR, ERR_FAILEDTOSAVEIMAGE);
               failedSave = true;
            }
            else
            {
               $(".ContentImageUriHidden").val(contentUri);
               $("#contentReview").attr("data-featherlight", tempLink);
               failedSave = false;
            }
         });

         $("#contentCamera").hide();
         $("#contentReview").show();
         $("#contentReview").attr("src", contentImage);
      }
   }

   $(document).on("click", ".ctrl2RecountSubmit", function ()
   {
      var qty = parseFloat($(".ctrl2RecountQty").val());

      if (!isNaN(qty) && qty >= 0)
      {
         $("#ID_INDIVIDUAL:visible, #ID_OPENED:visible").val(qty);
         $(".QuantityHidden").val(qty);
         $(this).closeMessageBox();
         $("#weightContinue").click();
      }
      else
      {
         $(".foreignContainerError").html("Quantity is required and cannot be negative");
      }
   });

   $(document).on("keypress", ".ctrl2RecountQty", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;

      if (key === 13)
      {
         $(".ctrl2RecountSubmit").click();
      }
   });

   var showCtrl2Recount = function ()
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The quantity must be recounted for this product. Enter the recounted value to continue." +
         "<br/><br/><input class=\"ctrl2RecountQty text-box single-line\" style=\"height: 22px;\" type=\"number\" autocomplete=\"off\">" +
         "   <input type=\"button\" id=\"btn_continue\" value=\"Submit\" class=\"btnErrorMessage ctrl2RecountSubmit\" style=\"margin:0 auto;text-align:center\"></h3>" +
         "<br/>" +
         "<span class='foreignContainerError' ></span>" +


         "<script>" +
         "setTimeout(function(){$('.ctrl2RecountQty').select();},10);" +
         "</script>"
      }];
      var titleobjects = [{ "title": "Quantity Recount Required" }];
      //$(this).addMessageButton([], messageObject);
      //$(this).showMessageBox(titleobjects);
      $(".segQuantity .segLabelRecount").text("Manual Recount");
      $("#ID_INDIVIDUAL").disable();
      $(".segWtQtyRecount").show();
      $(".segWtQtyInstructions").html("The quantity must be recounted for this product. Enter the recounted value to continue.");
      $("#ID_RECOUNT").focus();
   }
   window.showCtrl2Recount = showCtrl2Recount;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var showQuantity = function (containerType)
   {
      // Setup the controls with the specified Package Size, Units/Package and UOM
      ///////////////////////////////////////////////////////////////////////////////
      var packageUnit = parseInt($("#policyForm .UnitsPerPackageHidden").val());
      var packageSize = $("#policyForm .PackageSizeHidden").val();
      var uomPackage = "GM";
      var uomContent = $("#policyForm .UnitOfMeasureHidden").val();
      var uomIndividualWt = parseFloat($("#policyForm .IndividualCountWeightHidden").val());

      $(".segWeight .segWtQtyHeader").text("Weight: " + uomPackage);
      $(".segWeight .segLabelSm").text("XX.XXX " + uomPackage);

      //display individual count weight if the product processed has it
      //INTERNAL ONLY - DO NOT APPLY THIS TO DECISION SUPPORT
      if (!isNaN(uomIndividualWt) && $(".QoskBody").attr("AppCode") !== appCodeDss)
      {
         $(".weightContent .segLabelSm").text(uomIndividualWt + " " + uomPackage);
      }

      $("#ID_PRODUCTWT, #ID_CONTENTWT").prop("placeholder", "Weight (" + uomPackage + ")");

      $(".segQuantity .segWtQtyHeader").text("Quantity: " + uomContent);
      $(".segQuantity .segLabelSm").text(packageUnit + " Pkg x " + Math.round((packageSize / packageUnit), 3) + " " + uomContent);
      $("#ID_ITMCOUNT, #ID_OPENED, #ID_INDIVIDUAL").prop("placeholder", uomContent);
      $("#ID_INDIVIDUAL").removeAttr("disabled");
      $(".segQuantity .segLabelRecount").text("");
      consecutivePrdWtViol = false;

      //=============================================================================
      // ...
      //-----------------------------------------------------------------------------
      $(".weightProduct").show();

      $(".segWtQtySealed").hide();
      $(".segWtQtyOpened").hide();
      $(".segWtQtyLoose").hide();
      $(".segWtQtyCase").hide();
      $(".segWtQtyRecount").hide();

      var sealedOpenCase = containerType.split("|")[1];

      switch (sealedOpenCase)
      {
         case "Sealed":
            $(".weightContent").hide();
            $(".segWtQtySealed").show();

            $("#ID_SEALED").val("1");
            $("#contentCamera").hide();
            $("#contentReview").hide();
            $("contentReview").removeAttr("data-featherlight");
            isCameraOff = true;
            break;

         case "Open":
            if ($(".ContainerTypeHidden").val().includes("UnitDose") || ($(".ContainerTypeHidden").val().includes("Prescription") && $(".DosageImageCodeHidden").val() === "L"))
            {
               $(".weightContent").hide();
               $(".segWtQtyLoose").show();
            }
            else if (packageUnit > 1)
            {
               $(".weightContent").hide();
               $(".segWtQtyOpened").show();
            }
            else
            {
               $(".weightContent").show();
               $(".segWtQtyLoose").show();
            }

            if ($(".DosageImageCodeHidden").val() === "C" || $(".DosageImageCodeHidden").val() === "L")
            {
               $("#contentCamera").hide();
               isCameraOff = true;
            }
            else
            {
               $("#contentCamera").show();
               isCameraOff = false;
            }

            $("#contentReview").hide();
            $("contentReview").removeAttr("data-featherlight");

            break;

         case "Loose":
            $(".weightContent").show();
            $(".segWtQtyLoose").show();
            $("#contentCamera").show();
            $("#contentReview").hide();
            $("contentReview").removeAttr("data-featherlight");
            isCameraOff = false;
            break;

         case "Case":
            $(".weightContent").hide();
            $(".segWtQtyCase").show();

            $("#ID_CASECOUNT").val("1");
            $("#contentCamera").hide();
            $("#contentReview").hide();
            $("contentReview").removeAttr("data-featherlight");
            isCameraOff = true;
            break;
      }

      //hide content weight field for all non-C2 foreign containers and non-C2 force manual count products
      if (isForeignContainer() && $(".CtrlNumberHidden").val() !== "2")
      {
         $(".weightContent").hide();

         //open, non-control products - set the quantity to half of the package size
         //manual count for open, control products
         if ($(".CtrlNumberHidden").val() === "0")
         {
            if (sealedOpenCase === "Open" && packageUnit > 1)
            {
               //simulate the three quantity boxes being entered
               var qty = Math.round((packageSize / 2), 3);
               var pckg = qty / (packageSize / packageUnit);
               var itmcnt = qty % pckg;

               $("#ID_PKGCOUNT").val(pckg).disable();
               $("#ID_ITMCOUNT").val(itmcnt).disable();
               $("#ID_OPENED").val(qty).disable();
            }
            else if (sealedOpenCase === "Open" || sealedOpenCase === "Loose")
            {
               $("#ID_INDIVIDUAL").val(Math.round((packageSize / 2), 3)).disable();
            }
         }
      }
      else if ($(".ForceQuantityCountHidden").val() === "True" && $(".CtrlNumberHidden").val() !== "2")
      {
         $(".weightContent").hide();
      }

      //=============================================================================
      // ...
      //-----------------------------------------------------------------------------
      var staticImg = "Coming Soon.png";

      switch (containerType)
      {
         case "Original|Open":
            staticImg = "ContainerType_Original(Opened).png";
            break;

         case "Original|Sealed":
            staticImg = "ContainerType_Original(Sealed).png";
            break;

         case "Prescription|Open":
            staticImg = "ContainerType_Prescription.png";
            break;

         case "Repackager|Open":
            staticImg = "ContainerType_Repackager(Opened).png";
            break;

         case "Repackager|Sealed":
            staticImg = "ContainerType_Repackager(Sealed).png";
            break;

         case "UnitDose|Open":
            staticImg = "ContainerType_UnitDose.png";
            break;

         case "None|Loose":
            staticImg = "ContainerType_Loose.png";
            break;

         case "Original|Case":
            staticImg = "ContainerType_Case.png";
            break;
      }

      $("#ID_STATICPREVIEW").attr("src", ("/Images/ContainerType/") + staticImg);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.showQuantity = showQuantity;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function validateWeightQuantityCondition()
   {
      showWtQtyViolation(null, "");

      //=============================================================================
      // ...
      //-----------------------------------------------------------------------------
      var productWt = parseFloat($("#ID_PRODUCTWT").val());
      var contentWt = parseFloat($("#ID_CONTENTWT").val());

      //if ($(".segWeight").is(":visible"))
      //{
      //   console.log("validateWeight");

      //   // Check product weight for value
      //   ///////////////////////////////////////////////////////////////////////////////
      //   if (!weightCheck("#ID_PRODUCTWT", productWt, contentWt))
      //   {
      //      return window.failValidation(segmentId);
      //   }

      //   // If the content weight is required, then check for a value
      //   ///////////////////////////////////////////////////////////////////////////////
      //   if ($(".weightContent").is(":visible"))
      //   {
      //      if (!weightCheck("#ID_CONTENTWT", productWt, contentWt))
      //      {
      //         return window.failValidation(segmentId);
      //      }
      //   }
      //}

      if ($(".segWeight").is(":visible"))
      {
         console.log("validateWeight");

         // Check product weight for value
         ///////////////////////////////////////////////////////////////////////////////
         if (isNaN(productWt))
         {
            showWtQtyViolation(SEG_ERROR, ERR_MISSINGPRODUCTWT);
            $("#ID_PRODUCTWT").focus().select();
            return window.failValidation(segmentId);
         }

         if (productWt <= 0)
         {
            showWtQtyViolation(SEG_ERROR, ERR_INVALIDPRODUCTWT);
            $("#ID_PRODUCTWT").focus().select();
            return window.failValidation(segmentId);
         }

         // If the content weight is required, then check for a value
         ///////////////////////////////////////////////////////////////////////////////
         if ($(".weightContent").is(":visible"))
         {
            if (isNaN(contentWt))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGCONTENTWT);
               $("#ID_CONTENTWT").focus().select();
               return window.failValidation(segmentId);
            }

            if (contentWt < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_INVALIDCONTENTWT);
               $("#ID_CONTENTWT").focus().select();
               return window.failValidation(segmentId);
            }

            if (contentWt > productWt)
            {
               showWtQtyViolation(SEG_ERROR, ERR_WEIGHT);
               $("#ID_CONTENTWT").focus().select();
               return window.failValidation(segmentId);
            }
         }
      }

      //=============================================================================
      // ...
      //-----------------------------------------------------------------------------
      var countSealed = parseFloat($("#ID_SEALED").val());

      var countPkg = parseFloat($("#ID_PKGCOUNT").val());
      var countItm = parseFloat($("#ID_ITMCOUNT").val());
      var countOpn = parseFloat($("#ID_OPENED").val());

      var countQty = parseFloat($("#ID_INDIVIDUAL").val());

      var countCase = parseFloat($("#ID_CASECOUNT").val());
      var countSize = parseFloat($("#ID_CASESIZE").val());

      var countRecount = parseFloat($("#ID_RECOUNT").val());
      var hiddenC2count = parseFloat($(".QuantityHidden").val());

      var packageUnit = parseInt($("#policyForm .UnitsPerPackageHidden").val());
      var packageSize = $("#policyForm .PackageSizeHidden").val();
      var quantity = 0;

      if ($(".segQuantity").is(":visible"))
      {
         console.log("validateQuantity");

         // ...
         ///////////////////////////////////////////////////////////////////////////////
         if ($(".segWtQtyRecount").is(":visible"))
         {
            if (isNaN(countRecount))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGQTY);
               $("#ID_RECOUNT").focus().select();
               return window.failValidation(segmentId);
            }

            if (countRecount < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_ZEROQTY);
               $("#ID_RECOUNT").focus().select();
               return window.failValidation(segmentId);
            }

            quantity = countRecount;
         }

         // ...
         ///////////////////////////////////////////////////////////////////////////////
         else if ($(".segWtQtySealed").is(":visible"))
         {
            if (isNaN(countSealed))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGPACKAGE);
               $("#ID_SEALED").focus().select();
               return window.failValidation(segmentId);
            }

            if (countSealed < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_ZEROQTY);
               $("#ID_SEALED").focus().select();
               return window.failValidation(segmentId);
            }

            quantity = countSealed;
         }
         // ...
         ///////////////////////////////////////////////////////////////////////////////
         else if ($(".segWtQtyOpened").is(":visible"))
         {
            if (isNaN(countPkg))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGPACKAGE);
               $("#ID_PKGCOUNT").focus().select();
               return failValidation(segmentId);
            }

            if (isNaN(countItm))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGINDIVIDUAL);
               $("#ID_ITMCOUNT").focus().select();
               return failValidation(segmentId);
            }

            if (countPkg < 0 || countItm < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_ZEROQTY);
               $(countItm < 0 ? "#ID_ITMCOUNT" : "#ID_PKGCOUNT").focus().select();
               return window.failValidation(segmentId);
            }

            quantity = countOpn;
         }
         // ...
         ///////////////////////////////////////////////////////////////////////////////
         else if ($(".segWtQtyLoose").is(":visible"))
         {
            if (isNaN(countQty))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGQTY);
               $("#ID_INDIVIDUAL").focus().select();
               return failValidation(segmentId);
            }

            if (countQty < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_ZEROQTY);
               $("#ID_INDIVIDUAL").focus().select();
               return window.failValidation(segmentId);
            }

            //bypass the control 2 recount for a quantity mismatch for decision support and foreign containers
            if (!isForeignContainer() && hiddenC2count !== countQty && $(".CtrlNumberHidden").val() === "2" && $("#policyForm .UnitOfMeasureHidden").val() === "EA" && $(".QoskBody").attr("AppCode") !== appCodeDss)
            {
               showCtrl2Recount();
               return false;
            }

            quantity = countQty;
         }
         // ...
         ///////////////////////////////////////////////////////////////////////////////
         else if ($(".segWtQtyCase").is(":visible"))
         {
            if (isNaN(countCase))
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGCASE);
               $("#ID_CASECOUNT").focus().select();
               return window.failValidation(segmentId);
            }

            if (isNaN(countSize) || countSize < 1)
            {
               showWtQtyViolation(SEG_ERROR, ERR_MISSINGCASESIZE);
               $("#ID_CASESIZE").focus().select();
               return window.failValidation(segmentId);
            }

            if (countCase < 0 || countSize < 0)
            {
               showWtQtyViolation(SEG_ERROR, ERR_ZEROQTY);
               $(countCase < 0 ? "#ID_CASECOUNT" : "#ID_CASESIZE").focus().select();
               return window.failValidation(segmentId);
            }

            quantity = countCase;
         }
      }

      if (isCameraOff === false && $("#contentCamera").is(":visible"))
      {
         showWtQtyViolation(SEG_ERROR, ERR_NOCONTENTIMAGE);
         $("#ID_PRODUCTCOMMENT").focus();
         return window.failValidation(segmentId);
      }

      if (failedSave === true)
      {
         showWtQtyViolation(SEG_ERROR, ERR_FAILEDTOSAVEIMAGE);
         $("#ID_PRODUCTCOMMENT").focus();
         return window.failValidation(segmentId);
      }

      $(segmentSelector).attr("data-segment-complete", "true");

      // Persist weight related information
      console.log("set#policyForm.ContentWeightHidden: '" + (isNaN(contentWt) ? "" : contentWt) + "'");
      console.log("set#policyForm.ProductWeightHidden: '" + (isNaN(productWt) ? "" : productWt) + "'");

      $("#policyForm .ContentWeightHidden").val(isNaN(contentWt) ? "" : contentWt);
      $("#policyForm .ProductWeightHidden").val(isNaN(productWt) ? "" : productWt);

      // Persist quantity related information
      console.log("set#policyForm.QuantityHidden: '" + quantity + "'");
      console.log("set#policyForm.CaseSizeHidden: '" + (isNaN(countSize) ? "" : countSize) + "'");

      $("#policyForm .QuantityHidden").val(quantity);
      $("#policyForm .CaseSizeHidden").val(isNaN(countSize) ? "" : countSize);

      console.log("set#policyForm.ContainerConditionHidden: " + $(".ContainerConditionHidden").val());

      return true;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.validateWeightQuantityCondition = validateWeightQuantityCondition;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Page load functions.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var checkScaleCondition = function ()
   {
      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleConnected", {}, function (result)
      {
         console.log("ScaleConnected:" + result);

         if ($.parseJSON(result))
         {
            setScaleBusy(false);
            window.showWtQtyViolation(null, "");
         }
         else
         {
            window.showWtQtyViolation(SEG_WARNING, ERR_NOSCALE);
         }
      });
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.checkScaleCondition = checkScaleCondition;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var setQuantityType = function ()
   {
      if (!$(segmentSelector).length || !$(".ContainerTypeHidden").length)
      {
         return;
      }

      showQuantity($(".ContainerTypeHidden").val());
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.createAndExclusivePush("containerTypeCollapseMethods", setQuantityType);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var restartCamera = function ()
   {
      setTimeout(function ()
      {
         if ($("#contentCamera:visible").length)
         {
            window.changeCamera($("#cboContentCamera")[0]);
         }
      }, 200);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.createAndExclusivePush(segmentId + "ExpandMethods", restartCamera);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "input:checkbox[name=condition]", function ()
   {
      var elementId = $(this).attr("id");

      //=============================================================================
      // NOTE:
      //    You might be looking at this and wondering why we are switching from
      //    jQuery to JavaScript DOM...  Good question!  It appears JQuery has an
      //    issue setting a checked value for a hidden element.  Because we need
      //    this ability, we use the DOM instead.
      //-----------------------------------------------------------------------------

      if ($('#' + elementId).is(":checked") === true)
      {
         $(".ContainerConditionHidden").val($(".ContainerConditionHidden").val() + "|" + $('#' + elementId).attr('value'));
      }
      else
      {
         $(".ContainerConditionHidden").val($(".ContainerConditionHidden").val().replace("|" + $('#' + elementId).attr('value'), ""));
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SCALETARE", function ()
   {
      setScaleBusy(true);

      $.getJSON("https://localhost:8001/Qualanex/http/ScaleTare", {}, function (result)
      {
         console.log("ScaleTare:" + result);

         setScaleBusy(false);

         if ($.parseJSON(result))
         {
            $("#ID_PRODUCTWT").val("");
            $("#ID_CONTENTWT").val("");
            $("#ID_PRODUCTWT").focus();
         }
         else
         {
            window.showWtQtyViolation(SEG_WARNING, ERR_NOACK);
         }
      });

      ga("send", "event", "SegmentWtQty", "tare", "tareButton");
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var captureScaleWeight = function (retry)
   {
      var oldFocus = "#" + previousFocus.prop("id");

      setScaleBusy(true);
      overrideCalibrationWarning = false;
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleCapture", {}, function (result)
      {
         console.log("ScaleCapture:" + result);

         setScaleBusy(false);

         if (result === "")
         {
            $.getJSON("https://localhost:8001/Qualanex/http/LastError", {}, function (errorCode)
            {
               setScaleBusy(false);

               if (errorCode === 6)
               {
                  showWtQtyViolation(SEG_WARNING, ERR_CALIBRATE);
               }
               else if (errorCode === 2 && retry < 3)
               {
                  setTimeout(function () { captureScaleWeight(retry + 1) }, 200);
               }
               else
               {
                  showWtQtyViolation(SEG_WARNING, ERR_NOACK);
               }
            });

            return;
         }
         var arrayObj = "#ID_PRODUCTWT #ID_CONTENTWT";

         if (($(".ContainerTypeHidden").val().includes("Open") || $(".ContainerTypeHidden").val().includes("Loose") || ($(".ContainerTypeHidden").val().includes("Prescription") && $(".DosageImageCodeHidden").val() !== "L")) && $("#ID_PRODUCTWT").val() !== "")
         {
            setTimeout(captureContentImage, 500);
         }

         $.each(arrayObj.split(/\s+/), function (index, value)
         {
            if ($(value).val() === "" || value === oldFocus)
            {
               $(value).val(result);
               $(value).change();

               if ($("#ID_PRODUCTWT").val() && $("#ID_CONTENTWT").val())
               {
                  window.nextFocus("#ID_CONTENTWT");
               }
               else
               {
                  window.nextFocus(value);
               }

               if (!$(".weightContent").is(":visible")) //Stop populating Content Weight if it's not visible
               {
                  $("#ID_CONTENTWT").val("");
               }
               return false;
            }
         });

      });

   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#CMD_SCALECAPTURE", function ()
   {
      captureScaleWeight(0);
      ga("send", "event", "SegmentWtQty", "capture", "captureButton");
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   window.launchCalibration = function ()
   {
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleCalibrate", {}, function (result)
      {
         console.log("ScaleCalibrate:" + result);
         window.showWtQtyViolation(null, "");
      });

      ga("send", "event", "SegmentWtQty", "calibrate", "calibrate link");
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", "#ID_PKGCOUNT, #ID_ITMCOUNT", function ()
   {
      var unitsPerPackage = parseFloat($(".UnitsPerPackageHidden").val());
      var packageSize = parseFloat($("#policyForm .PackageSizeHidden").val()) / unitsPerPackage;

      var packageCount = parseInt($("#ID_PKGCOUNT").val(), 10);
      var itemCount = parseFloat($("#ID_ITMCOUNT").val());

      console.log("input#policyForm .PackageSizeHidden: '" + packageSize + "'");
      console.log("input#ID_PKGCOUNT: '" + packageCount + "'");
      console.log("input#ID_ITMCOUNT: '" + itemCount + "'");

      $("#ID_OPENED").val(((isNaN(packageCount) ? 0 : packageCount) * (isNaN(packageSize) ? 0 : packageSize)) + (isNaN(itemCount) ? 0 : itemCount));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("input", "#ID_CASECOUNT, #ID_CASESIZE", function ()
   {
      var caseCount = parseInt($("#ID_CASECOUNT").prop("value"), 10);
      var caseSize = parseInt($("#ID_CASESIZE").prop("value"), 10);

      $("#ID_CASEQTY").val((isNaN(caseCount) ? 0 : caseCount) * (isNaN(caseSize) ? 0 : caseSize));
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#ID_CONTENTWT", function ()
   {
      showWtQtyViolation(null, "");
      var contentWt = parseFloat($("#ID_CONTENTWT").val());
      var productWt = parseFloat($("#ID_PRODUCTWT").val());

      //do not calculate the quantity based off the content weight for foreign containers
      //...or if the content weight isn't visible
      if (isForeignContainer() || !$(".weightContent").is(":visible"))
      {
         return;
      }

      if ($(".segWtQtyLoose").is(":visible") && $(".ForceQuantityCountHidden").val() === "False")
      {
         var individualWt = parseFloat($("#policyForm .IndividualCountWeightHidden").val());

         console.log("change#ID_CONTENTWT:" + contentWt);
         console.log("change#policyForm .IndividualCountWeightHidden: '" + individualWt + "'");

         if (!weightCheck("#" + $(this).attr("id"), contentWt))
         {
            return window.failValidation(segmentId);
         }

         //if (isNaN(individualWt) && $(".CtrlNumberHidden").val() === "2" && $(".DosageImageCodeHidden").val() !== "P")
         //{
         //   showWtQtyViolation(SEG_WARNING, ERR_MISSINGINDIVIDUAL + " " + ERR_NOINDIVIDUALWT);
         //}

         if (!isNaN(contentWt) && !isNaN(individualWt) && $(".IsWeightCountingHidden").val() === "True" && ($(".CtrlNumberHidden").val() !== "2" || $(".DosageImageCodeHidden").val() === "L"))
         {
            $("#ID_INDIVIDUAL").val(Math.round(contentWt / individualWt));
            $("#ID_INDIVIDUAL").trigger("input");
         }
         else if (!isNaN(contentWt) && !isNaN(individualWt) && $(".CtrlNumberHidden").val() === "2")
         {
            $(".QuantityHidden").val(Math.round(contentWt / individualWt));
         }
      }

      if (!weightCheck("#" + $(this).attr("id"), contentWt))
      {
         return window.failValidation(segmentId);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   For a weight violation, reset the necessary fields
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function resetWeightFields(id)
   {
      var foreignContainer = isForeignContainer();

      if (id === "#ID_PRODUCTWT")
      {
         if ($(".segWtQtyLoose").is(":visible") && !foreignContainer)
         {
            $("#ID_CONTENTWT").val("");
            $("#ID_INDIVIDUAL").val("");
         }

         if ($(".segWtQtyOpened").is(":visible") && !foreignContainer)
         {
            $("#ID_PKGCOUNT").val("");
            $("#ID_ITMCOUNT").val("0").trigger("input");;
         }

         $(id).val("");

         setTimeout(function ()
         {
            $("#ID_PRODUCTWT").focus();
         }, 150);
      }
      else if (id === "#ID_CONTENTWT")
      {
         if ($(".weightContent").is(":visible"))
         {
            $(id).val("");
            $("#ID_INDIVIDUAL").val("");

            setTimeout(function ()
            {
               $("#ID_CONTENTWT").focus();
            }, 150);
         }
      }
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function enterAsTab(id, e)
   {
      if (e.keyCode == 13)
      {
         var nextFocusAttribute = document.getElementById(id).getAttribute("nextfocus");
         var nextFocusArray = nextFocusAttribute.split(" ");

         for (var i = 0; i < nextFocusArray.length; i++)
         {
            var target = "#" + nextFocusArray[i];

            if ($(target).is(":visible"))
            {
               document.getElementById(nextFocusArray[i]).focus();
               break;
            }
         }
      }
   }
   window.enterAsTab = enterAsTab;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function weightCheck(id, weight)
   {
      var containerWt = parseFloat($("#policyForm .ContainerWeightHidden").val());
      var fullContainerWt = parseFloat($("#policyForm .FullContainerWeightHidden").val());
      var indCountWt = parseFloat($("#policyForm .IndividualCountWeightHidden").val());
      var containerType = $(".ContainerTypeHidden").val();
      var lowTolerance = weight;
      var highTolerance = weight;
      var error = false;
      var checkWeightTolerance = false;

      //allow a tolerance level for product weight captured (warehouse only); send to Audit if exceeds the weight range based on the tolerance
      if (!isForeignContainer() && $(".CtrlNumberHidden").val() !== "2" && $(".QoskBody").attr("AppCode") !== appCodeDss)
      {
         switch (containerType)
         {
            case "Original|Open":

               if (!isNaN(containerWt))
               {
                  checkWeightTolerance = true;
                  lowTolerance = (1 - weightTolerance) * (containerWt);
               }

               if (!isNaN(fullContainerWt))
               {
                  checkWeightTolerance = true;
                  highTolerance = (1 + weightTolerance) * (fullContainerWt);
               }

               break;

            case "Original|Sealed":

               if (!isNaN(fullContainerWt))
               {
                  checkWeightTolerance = true;
                  lowTolerance = (1 - weightTolerance) * fullContainerWt;
                  highTolerance = (1 + weightTolerance) * fullContainerWt;
               }

               break;
         }
      }

      if (id === "#ID_PRODUCTWT")
      {
         if (isNaN(weight))
         {
            showWtQtyViolation(SEG_WARNING, ERR_MISSINGPRODUCTWT);
            error = true;
         }
         else if (weight <= 0)
         {
            showWtQtyViolation(SEG_WARNING, ERR_INVALIDPRODUCTWT + " " + ERR_ZEROPRODUCTWT);
            error = true;
         }
         else if (checkWeightTolerance && (weight < lowTolerance || weight > highTolerance))
         {
            showWtQtyViolation(SEG_WARNING, ERR_FULLCONTRPRODUCTWT);
         }
         //else if (productWt < indCountWt)
         //{
         //   showWtQtyViolation(SEG_WARNING, ERR_INVALIDPRODUCTWT + " " + ERR_INDCONTPRODUCTWT);
         //   error = true;
         //}
         //else if (productWt < containerWt)
         //{
         //   showWtQtyViolation(SEG_WARNING, ERR_INVALIDPRODUCTWT + " " + ERR_CONTRPRODUCTWT);
         //   error = true;
         //}
         //else if (contentWt > productWt)
         //{
         //   showWtQtyViolation(SEG_WARNING, ERR_WEIGHT);
         //   error = true;
         //}
         //else if (isNaN(containerWt) && containerType === "Open" && $(".DosageImageCodeHidden").val() !== "P")
         //{
         //   showWtQtyViolation(SEG_WARNING, ERR_NOCONTRWT);
         //}
      }
      else if (id === "#ID_CONTENTWT")
      {
         if ($(".weightContent").is(":visible"))
         {
            if (isNaN($("#ID_PRODUCTWT").val()) || $("#ID_PRODUCTWT").val() === 0 || $("#ID_PRODUCTWT").val() === "")
            {
               showWtQtyViolation(SEG_WARNING, ERR_MISSINGPRODUCTWT);
               id = "#ID_PRODUCTWT"; //product weight violation, re-capture productWt
               error = true;
            }
            else if (isNaN(weight))
            {
               showWtQtyViolation(SEG_WARNING, ERR_MISSINGCONTENTWT);
               error = true;
            }
            else if (weight < 0)
            {
               showWtQtyViolation(SEG_WARNING, ERR_INVALIDCONTENTWT);
               error = true;
            }
            //else if (contentWt > productWt)
            //{
            //   showWtQtyViolation(SEG_WARNING, ERR_WEIGHT);
            //   id = "#ID_PRODUCTWT"; //product weight violation, re-capture productWt
            //   error = true;
            //}
         }
      }

      if (error)
      {
         resetWeightFields(id);
         return false;
      }

      return true;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#ID_RECOUNT", function ()
   {
      if ($(".segWtQtyRecount").is(":visible"))
      {
         var recountWt = parseFloat($("#ID_RECOUNT").val());

         console.log("change#ID_RECOUNT:" + recountWt);

         if (!isNaN(recountWt) && $(".CtrlNumberHidden").val() === "2" && recountWt > 0)
         {
            $("#weightContinue").click();
         }
         else
         {
            showWtQtyViolation(SEG_ERROR, ERR_INVALIDQTY);
            return window.failValidation(segmentId);
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#ID_PRODUCTWT", function ()
   {
      showWtQtyViolation(null, "");
      $("#ID_CONTENTWT").enable();
      var productWt = parseFloat($("#ID_PRODUCTWT").val());
      var contentWt = parseFloat($("#ID_CONTENTWT").val());

      if (!weightCheck("#" + $(this).attr("id"), productWt))
      {
         return window.failValidation(segmentId);
      }

      window.selectContentImage();
      if ($(".segWtQtyLoose").is(":visible"))
      {
         var containerWt = parseFloat($("#policyForm .ContainerWeightHidden").val());
         var dosageImageCode = $("#policyForm .DosageImageCodeHidden").val();
         var unitsPerPackage = parseFloat($(".UnitsPerPackageHidden").val());
         var ctrlNumber = $(".CtrlNumberHidden").val();

         console.log("change#ID_PRODUCTWT:" + productWt);
         console.log("change#policyForm .ContainerWeightHidden: '" + containerWt + "'");
         console.log("change#policyForm .DosageImageCodeHidden: '" + dosageImageCode + "'");

         //only do auto-calculation for original containers (no prescription vials, repackaging, etc)
         if (!isNaN(productWt) && !isNaN(containerWt) && !isNaN(unitsPerPackage) && dosageImageCode === "L" && unitsPerPackage === 1 && $(".ContainerTypeHidden").val() === "Original|Open")
         {
            $("#ID_CONTENTWT").val((productWt - containerWt).toFixed(3));
            $("#ID_CONTENTWT").change();
         }

         if (isForeignContainer())
         {
            if (ctrlNumber === "0")
            {
               $("#ID_CONTENTWT").val(parseFloat(productWt / 2).toFixed(3));
               $("#ID_CONTENTWT").change();
            }
            else if (ctrlNumber === "2")
            {
               if (!weightCheck("#" + $(this).attr("id"), productWt))
               {
                  return window.failValidation(segmentId);
               }

               return;
            }
            else if ($(".segWtQtyInstructions").html().trim() === "")
            {
               $(".weightContent").hide();
               $(".segWtQtyInstructions").html("Manual count is required");
            }

            $("#ID_CONTENTWT").disable();
         }
      }

      if (!weightCheck("#" + $(this).attr("id"), productWt))
      {
         return window.failValidation(segmentId);
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", "#ID_PRODUCTCOMMENT", function ()
   {
      $(".ProductCommentHidden").val($(this).val());
   });
   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   /*$(document).on("blur", "#ID_INDIVIDUAL", function ()
   {
      var individualQty = parseFloat($("#ID_INDIVIDUAL").val());

      document.getElementById((!isNaN(individualQty) && individualQty === 0.0) ? "CNTRCDTN_EMPTY" : document.getElementById("ContainerCondition").getAttribute("lastValue")).checked = true;
   });*/

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setScaleBusy(isBusy)
   {
      $("#CMD_SCALETARE").prop("disabled", isBusy);
      $("#CMD_SCALECAPTURE").prop("disabled", isBusy);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var tareScale = function ()
   {
      $.getJSON("https://localhost:8001/Qualanex/http/ScaleTare", {}, function (data) { console.log("tared: " + data); });
   }

   createAndExclusivePush("containerTypeCollapseMethods", tareScale);

   window.selectContentImage = function ()
   {
      var $contentImage = $(".slick-product-slider [data-image-type='CNT']:first");
      if ($contentImage.length && !$(".slick-current [data-image-type='CNT']").length)
      {
         console.log("scrolling image");
         var index = $contentImage.parent().attr("data-slick-index");
         setTimeout(function ()
         {
            $(".slick-product-slider:visible").slick("slickGoTo", index);
         }, 1000);
      }
   }
   //createAndExclusivePush("containerImageCollapseMethods", selectContentImage);
   createAndExclusivePush("weightQuantityConditionExpandMethods", selectContentImage);


   var captureWeight = function ()
   {

      window.selectContentImage();
      if (!$("#ID_PRODUCTWT").length || ($("#ID_PRODUCTWT").val() !== "" && $("#ID_PRODUCTWT").val() !== 0))
      {
         return;
      }

      if (!$("#CMD_SCALECAPTURE").attr("disabled"))
      {
         $("#CMD_SCALECAPTURE").click();
      }
      else
      {
         $("#ID_PRODUCTWT").select();
      }
   }
   createAndExclusivePush(segmentId + "ExpandMethods", captureWeight);


   window.showCalibrationWarning = function ()
   {
      var messageObject = [{
         "id": "lbl_messages",
         "name": "The scale calibration will expire soon. Press \"Continue\" to begin calibration" +
         "<script>" +
         "$(\".close-button-messageBox\").addClass(\"closeButtonOverridesCalibration\");" +
         "</script>"
      }];
      var btnObjects = [{
         "id": "btn_continue", "name": "Continue", "function": "onclick='$(this).closeMessageBox();'", "class": "btnErrorMessage launchCalibration", "style": "margin:0 auto;text-align:center"
      }];
      var titleobjects = [{ "title": "Scale Calibration Expiring Soon" }];
      $(this).addMessageButton(btnObjects, messageObject);
      $(this).showMessageBox(titleobjects);
   }

   $(document).on("click", ".launchCalibration", function ()
   {
      launchCalibration();
      $(".actionTool .ql-product-multi").click();
   });

   $(document).on("click", ".closeButtonOverridesCalibration", function ()
   {
      overrideCalibrationWarning = true;
      $("#SubmitSearch").click();
   });

   window.checkCalibrationExpiration = function ()
   {
      if (!$("#weightQuantityCondition").length || overrideCalibrationWarning)
      {
         return true;
      }

      try
      {
         var request = $.ajax({
            cache: false,
            type: "GET",
            datatype: "JSON",
            async: false,
            url: "https://localhost:8001/Qualanex/http/ScaleCalibrationMeter?scaleClass=Induction",
            timeout: 300
         });

         var result = request.responseJSON;
         console.log("CalibrationMeter:" + result);
         var times = result.split(":");

         if (parseFloat(times[0]) === 0 && parseFloat(times[1]) < calibrationThreshold)
         {
            request = $.ajax({
               cache: false,
               type: "GET",
               datatype: "JSON",
               async: false,
               url: "https://localhost:8001/Qualanex/http/ScaleConnected"
            });
            var connected = request.responseJSON;

            if (connected)
            {
               showCalibrationWarning();
               return false;
            }
         }
      }
      catch (e)
      {
         return true;
      }

      return true;
   }
});
