//*****************************************************************************
//*
//* QoskActionbar.js
//*    Copyright (c) 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   //#############################################################################
   // Global functions
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Sets the enable/disable state of an Actionbar control.
   //*
   //* Parameters:
   //*   controlId - ID specific to the Actionbar control.
   //*   enable    - Control state to set.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var enableActionbarControl = function (controlId, enable)
   {
      var from = enable ? "(0)" : "(1)";
      var to = enable ? "(1)" : "(0)";

      if ($(controlId).length > 0)
      {
         $(controlId).attr("src", window.$(controlId).attr("src").replace(from, to));
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.enableActionbarControl = enableActionbarControl;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Determine if a specified control is enabled.
   //*
   //* Parameters:
   //*   controlId - ID specific to the Actionbar control.
   //*
   //* Returns:
   //*   True if the specified control on the Actionbar is enabled, otherwise
   //*   False.
   //*
   //*****************************************************************************
   var isEnabledActionbar = function (controlId)
   {
      return ($(controlId).attr("src").indexOf("(1)") > 0);
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.isEnabledActionbar = isEnabledActionbar;

   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Event handler for Actionbar control click.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".actionIcon", function (e)
   {
      //prevent action bar items being clicked while Infinite Scroll is in effect
      if (window.isInfiniteScrollActive())
      {
         return false;
      }

      console.log("QoskMgmt::click.actionIcon" + $(this).prop("id"));

      // If the control is enabled...
      ///////////////////////////////////////////////////////////////////////////////
      if (window.isEnabledActionbar($(this)))
      {
         // Handle the event
         switch ($(this).prop("id"))
         {
            // ...
            //=============================================================================
            case "CMD_REFRESH":
               window.OnQnexRefresh();
               break;

            // ...
            //=============================================================================
            case "CMD_NEW":
               window.OnQnexNew();
               break;

            // ...
            //=============================================================================
            case "CMD_EDIT":
               window.OnQnexEdit();
               break;

            // ...
            //=============================================================================
            case "CMD_SAVE":
               window.OnQnexSave();
               break;

            // ...
            //=============================================================================
            case "CMD_CANCEL":
               window.OnQnexCancel();
               break;

            // ...
            //=============================================================================
            case "CMD_PRINT":
               window.OnQnexPrint();
               break;

            // ...
            //=============================================================================
            case "CMD_RETIRED":
               window.OnQnexRetired();
               break;

            // ...
            //=============================================================================
            case "CMD_FILTER":
               if ($(".ql-body.active div.dataTables_scrollBody").length > 0 && $(".ql-body.active div.formExDivFilter").length > 0)
               {
                  if (!$(".ql-body.active div.formExDivFilter input").hasClass("inputError"))
                  {
                     $(".filterFrame td.tdMessage").html("");
                  }
                  $(".filterFrame").slideToggle(200);
                  e.stopPropagation();

                  $(".filterFrame .formExDiv").html($(".ql-body.active div.formExDivFilter").html());
                  window.OnQnexFilter();
               }

               break;
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Filter.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".filterFrame .SubmitCriteria", function (e)
   {
      $(".filterFrame .filterContent .inputFilter").each(function ()
      {
         switch ($(this).prop("tagName"))
         {
            // ...
            //=============================================================================
            case "INPUT":
               {
                  $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").val($(this).val());
                  $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").attr("value", $(this).val());

                  if ($(this).hasClass("hasDatepicker") && $(this).val().length > 0)
                  {
                     if (!$(this).val().match(/^\d{1,2}\/\d{1,2}\/(?:\d{2}|\d{4})$/) || $(this).val().match(/^\d{1,2}\/\d{1,2}\/(?:\d{2}|\d{4})$/) === null)
                     {
                        $(this).addClass("inputError");
                        $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
                        break;
                     }

                     var dateArray = $(this).val().split("/");
                     var month = parseInt(dateArray[0], 10);
                     var day = parseInt(dateArray[1], 10);
                     var year = parseInt(dateArray[2], 10);

                     if ((year >= 100 && (year < 1990 || year > 2100)) || month === 0 || month > 12)
                     {
                        $(this).addClass("inputError");
                        $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
                        break;
                     }

                     var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                     if ((year > 1000 && (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))) || (year < 100 && year % 4 === 0))
                     {
                        monthDays[1] = 29;
                     }

                     if (day > 0 && day <= monthDays[month - 1])
                     {
                        $(this).removeClass("inputError");
                        $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").removeClass("inputError");
                     }
                     else
                     {
                        $(this).addClass("inputError");
                        $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
                     }
                  }
                  break;
               }

            // ...
            //=============================================================================
            case "SELECT":
               {
                  $(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "']").val($(this).val());
                  $(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "']").attr("value", $(this).val());
                  $(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "'] option").removeAttr("selected");
                  $(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "'] option[value='" + $(this).val() + "']").attr("selected", true);
                  break;
               }
         }
      });

      if ($(".filterFrame .filterContent .inputFilter").hasClass("inputError"))
      {
         $(".filterFrame td.tdMessage").html("Invalid input");
         return false;
      }
      else
      {
         $(".filterFrame td.tdMessage").html("");
      }

      var $div = $(this).closest("tbody").find("div.formExDiv form");
      var $url = $($div).attr("action");

      if ($url !== "")
      {
         $("#wait").show();

         if ($("div.filterFrame").is(":visible"))
         {
            $("#CMD_FILTER").click();
         }

         $.ajax({
            cache: false,
            type: "POST",
            datatype: "JSON",
            data: $(this).closest("tbody").find("div.formExDiv form").serialize() + getAntiForgeryToken(),
            url: $url,
            async: true,
            success: function (data)
            {
               var $filterDefaultValue = $(".ql-body.active .formExDivFilter").html();
               var $target = $($($filterDefaultValue).attr("data-div-id"));
               var $newHtml = $(data);
               $newHtml.find(".formExDivFilter").html($filterDefaultValue);
               replaceSegments($target, $newHtml);
               window.dtHeaders("#" + $newHtml.find(".filterableGrid").attr("id"), $($filterDefaultValue).attr("data-grid-options"), ["sorting"], []);
               executeMethods("onGridRefreshMethods", data);

               setTimeout(function ()
               {
                  $("#wait").hide();
               }, 400);
            }
         });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Clear Filter.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".filterFrame .ClearCriteria", function (e)
   {
      $(".filterFrame .filterContent input,.filterFrame .filterContent select").not("[type='button']").each(function ()
      {
         switch ($(this).prop("tagName"))
         {
            // ...
            //=============================================================================
            case "INPUT":
               {
                  $(this).val($(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").attr("data-default-value"));
                  $(this).removeAttr("style").removeClass("inputError");
                  break;
               }

            // ...
            //=============================================================================
            case "SELECT":
               {
                  $(this).val($(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "']").attr("data-default-value"));
                  $(this).attr("value", $(".ql-body.active div.formExDivFilter select[name='" + $(this).attr("name") + "']").attr("data-default-value"));
                  break;
               }
         }
      });
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Cancel Filter.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".filterFrame .CancelCriteria", function (e)
   {
      $(".filterFrame").slideUp();
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Click event - hide the filter if the user clicks off the search filter frame area
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).click(function (e)
   {
      if ($(e.target).closest(".filterFrame").length === 0 && $(".filterFrame").is(":visible") &&
         (!$(e.target).hasClass("ui-icon-circle-triangle-e") &&
            ($(".filterFrame").offset().top > e.clientY || e.clientY > ($(".filterFrame").offset().top + $(".filterFrame").height())
               || $(".filterFrame").offset().left > e.clientX || e.clientX > ($(".filterFrame").offset().left + $(".filterFrame").width()))))
      {
         $(".filterFrame", this).slideUp();
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Focusout event for date field in search filter
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".inputFilter", function (e)
   {
      if ($(this).hasClass("hasDatepicker") && $(this).val().length > 0)
      {
         if (!$(this).val().match(/^\d{1,2}\/\d{1,2}\/(?:\d{2}|\d{4})$/) || $(this).val().match(/^\d{1,2}\/\d{1,2}\/(?:\d{2}|\d{4})$/) === null)
         {
            $(this).css("background-color", "rgb(243, 104, 104)").addClass("inputError");
            $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
            return;
         }

         var dateArray = $(this).val().split("/");
         var month = parseInt(dateArray[0], 10);
         var day = parseInt(dateArray[1], 10);
         var year = parseInt(dateArray[2], 10);

         if ((year >= 100 && (year < 1990 || year > 2100)) || month === 0 || month > 12)
         {
            $(this).css("background-color", "rgb(243, 104, 104)").addClass("inputError");
            $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
            return;
         }

         var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

         if ((year > 1000 && (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))) || (year < 100 && year % 4 === 0))
         {
            monthDays[1] = 29;
         }

         if (day > 0 && day <= monthDays[month - 1])
         {
            $(this).removeAttr("style").removeClass("inputError");
            $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").removeClass("inputError");
         }
         else
         {
            $(this).css("background-color", "rgb(243, 104, 104)").addClass("inputError");
            $(".ql-body.active div.formExDivFilter input[name='" + $(this).attr("name") + "']").addClass("inputError");
         }
      }
   });

});
