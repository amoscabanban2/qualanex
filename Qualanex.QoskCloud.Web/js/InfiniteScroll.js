﻿//*****************************************************************************
//*
//* InfiniteScroll.js
//*    Copyright (c) 2018 - 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Implements "infinite scroll" functionality into a datatable UI element as 
//*    a more natural alternative to paging
//*
//* Notes:
//*    Intended to be used with DataTables.net version 1.10
//*    Requires that a criteria/filter form be implemented as a child of 
//*    an element with a class of "formExDivFilter", additionally the form 
//*    must contain inputs corresponding to the fields in IInfiniteScroll.cs
//*    Implemenation details follow the code
//*
//*****************************************************************************
$(function ()
{

   window.onGridStyleMethods = [];
   window.addGridStyleMethod = function (func)
   {
      exclusivePush(onGridStyleMethods, func);
   }

   window.extendInfiniteProductScroll = function ()
   {
      var $input = $(this).closest("td").find(".dataTables_filter input[type=search]");
      var $myTableObj = $(this);

      //do not apply infinite scroll if 1. the item is in edit mode, 2. if there is input in the search box (if applicable, not all data table grids utilize the search field), 
      //or 3. if the search filter has an error in at least one criteria field
      if (window.getEditMode() || ($input.val() !== undefined && $input.val() !== "") ||
         ($(".ql-body.active div.dataTables_scrollBody").length > 0
            && $(".ql-body.active div.formExDivFilter").length > 0
            && $(".filterFrame .filterContent .inputFilter").hasClass("inputError")))
      {
         return false;
      }

      //The form must contain inputs correspending to those in IInfiniteScroll.cs
      var $form = $(this).parents(".ql-body:first").find(".formExDivFilter form");
      var $scrollBody = $(this).find(".infiniteScroll");

      //Load more only if the table exists, it has been scrolled away from the top, it is not awaiting more results, 
      //it has not reached the end of the resultset and it is below the designated height from the bottom of the table
      if ($scrollBody.length && $scrollBody.attr("data-scroll-loading") !== "true" && $scrollBody.attr("data-scroll-end") !== "true"
         && $scrollBody.position().top !== 0 && $scrollBody.height() + $scrollBody.position().top - $scrollBody.parents(".dataTables_scrollBody:first").height()
         <= parseInt($form.find("[name*='LoadAtHeight']").val()))
      {
         //set the loading flag on the table so there won't be any duplicate loads
         $scrollBody.attr("data-scroll-loading", true);
         //set the previous count in the form so the query knows where to start
         $form.find("[name*='PreviousCount']").val($scrollBody.find(".infiniteScrollRow").length);
         var options = {
            async: true,
            url: $form.attr("action"), //the target is defined as the action in the criteria form
            type: "POST",
            data: $form.serialize() + getAntiForgeryToken(), //all data should be contained within the criteria form
            error: function (err)
            {
               $scrollBody.attr("data-scroll-loading", false); //reset the loading flag so it can try again 🤷‍
            },
            success: function (data)
            {

               $scrollBody.attr("data-scroll-loading", false); //reset the loading flag
               $scrollBody.DataTable().rows(".loading-row").remove(); //remove the loading row that is added immediately after the ajax request is sent
               var scrollLocation = $scrollBody.parents(".dataTables_scrollBody:first").scrollTop(); //save the current scroll position because datatables resets it when it redraws

               //This will accept any arbitrary html as long as it contains no more than one table with .infiniteScrollRow classed row elements
               $(data).find(".infiniteScrollRow").each(function ()
               {
                  //the bulk load in datatables only works for json, so we transfer each row one by one
                  $scrollBody.DataTable().row.add(this);
               });

               if ($(data).find(".infiniteScrollRow").length) //there are results
               {
                  //update the scroll location, it probably changed since the request was first sent
                  scrollLocation = $scrollBody.parents(".dataTables_scrollBody:first").scrollTop();

               }
               else //there are no more results
               {
                  //add a row letting the user know there are no more results
                  $myTableObj.find(".infiniteScroll").DataTable().row.add(buildRow($scrollBody, "end of results", "end-row"));
                  //set the flag on the table to prevent extra load attempts
                  $scrollBody.attr("data-scroll-end", true);
               }

               $scrollBody.DataTable().draw(); //redraw the table so the user sees the new rows
               $scrollBody.parents(".dataTables_scrollBody:first").scrollTop(scrollLocation - 2); //set the scroll location a little bit higher so that it's not bottomed out, provides a little bounce at the end of results
               executeMethods("onGridStyleMethods", data); //refresh grouping row styles
            }
         };

         //Add a row letting the user know more results are coming
         $(this).find(".infiniteScroll").DataTable().row.add(buildRow($scrollBody, "loading...", "loading-row"));

         var scrollLocation = $scrollBody.parents(".dataTables_scrollBody:first").scrollTop();//get the scroll position

         $(this).find(".infiniteScroll").DataTable().draw(); //redraw the table
         $scrollBody.parents(".dataTables_scrollBody:first").scrollTop(scrollLocation);// match the previous scroll position
         $.ajax(options);

         $form.find("[name*='PreviousCount']").val(0); //reset the form's previous count so that new submissions start from the top of the result set

      }
   }

   //builds a new row for user output, includes the order and number of hidden columns in the rest of the table
   function buildRow($scrollBody, message, className)
   {
      //keeping track of if the message has been included in case the first column(s) is hidden
      var messagePrinted = false;
      var row = "<tr class='" + className + "'>";
      $scrollBody.find(".infiniteScrollRow:first > td").each(function ()
      {
         if ($(this).attr("hidden"))
         {
            row += "<td hidden></td>";
         }
         else if (!messagePrinted)
         {
            row += "<td>" + message + "</td>";
            messagePrinted = true;
         }
         else
         {
            row += "<td></td>";
         }
      });

      row += "</tr>";
      console.log(row);
      return $(row);
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Determine if infinite scroll is currently in effect; prevent any other functionality while infinite scroll is loading
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   True if infinite scroll is active, otherwise False.
   //*
   //*****************************************************************************
   var isInfiniteScrollActive = function ()
   {
      return $(".ql-body.active .infiniteScroll .loading-row").length > 0;
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.isInfiniteScrollActive = isInfiniteScrollActive;
});

/*

to implement infinite scroll, the table (<table class="infiniteScroll">) that will be given datatable styling must have the .infiniteScroll class
Each row (<tr class="infiniteScrollRow">) within that table that is part of the result set must have the .infiniteScrollRow class

The query must be implemented with the ORDERBY FETCH OFFSET (OrderBy(..).Skip(..).Take(..) in LINQ) syntax using the IInfiniteScroll defined properties
For example in SQL syntax:

   var sqlQuery = "SELECT...
                  "... FROM...
                  "... WHERE...
                  "... 
                  "...
                  "...
                  ORDER by  t.StgeDate desc 
                  OFFSET @previousCount ROWS 
                  FETCH NEXT @returnCount ROWS ONLY";


   var lst = cloudEntities.Database.SqlQuery<WrhsStagingModel>(sqlQuery,
                                    new SqlParameter("@previousCount", model.PreviousCount),
                                    new SqlParameter("@returnCount", model.PreviousCount == 0 ? model.InitialLoadCount : model.ScrollLoadCount)
                                    ).ToList();
In LINQ syntax:
      result = (from wasteStream in entities.ProductWasteStream
               where wasteStream.ProductID == productID    
               select wasteStream)
               .OrderBy(x => x.SeverityOrder)
               .Skip(model.PreviousCount)
               .Take(model.PreviousCount == 0 ? model.InitialLoadCount : model.ScrollLoadCount)
               .ToList();

*/