﻿
var isLeavewitoutSave = true;
var urlName;

function ScrollBarPhoto() {

   var containerwidth = 0;
   var containerwidthmargin = ($("#containerimage li").length) * 25 + 50;
   $("#containerimage li").each(function() {
      containerwidth += $(this).outerWidth();
   });
   var containerwidthtotal = containerwidth + containerwidthmargin;
   $("#containerimage").css({ "width": +containerwidthtotal + "px" });

   var contentwidth = 0;
   var contentwidthmargin = ($("#contentimage li").length) * 25 + 50;
   $("#contentimage li").each(function() {
      contentwidth += $(this).outerWidth();
   });
   var contentwidthtotal = contentwidth + contentwidthmargin;
   $("#contentimage").css({ "width": +contentwidthtotal + "px" });
}

function CheckDecimal(str) {
   str = alltrim(str);

   return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(str);
}

function setLocalStorage(ddlvalues) {
   if (typeof (Storage) !== "undefined") {
      localStorage.setItem("Dosagelist", ddlvalues);
   }
}

function getLocalStorage() {
   if (typeof (Storage) !== "undefined") {
      var dosagedetails = localStorage.getItem("Dosagelist");

   }
}

function readURL(input) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {

         $('#imgProduct').attr('src', e.target.result);
      }
      return reader.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readURLforContent(input) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {

         $('#imgContentProductimage').attr('src', e.target.result);
      }
      return reader.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readContainerURL(input, imageID) {
   if (input.files && input.files[0]) {

      var readernew = new FileReader();

      readernew.onload = function(e) {

         $('#ContainerImageID_' + imageID).attr('src', e.target.result);
      }
      return readernew.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readContentURL(input, contentimageID) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {
         $('#ContentImageID_' + contentimageID).attr('src', e.target.result);
         // $('#imgContentProductimage').attr('src', e.target.result);
      }
      return reader.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function check_file() {

   str = document.getElementById('file').value.toUpperCase();
   suffix1 = ".JPG";
   suffix2 = ".JPEG";
   suffix3 = ".BMP";
   suffix4 = ".PNG";
   suffix5 = ".TIF";
   if (!(str.indexOf(suffix1, str.length - suffix1.length) !== -1 ||
      str.indexOf(suffix2, str.length - suffix2.length) !== -1 || str.indexOf(suffix3, str.length - suffix3.length) !== -1 ||
      str.indexOf(suffix4, str.length - suffix5.length) !== -1 || str.indexOf(suffix5, str.length - suffix5.length) !== -1)) {
      //  alert('File type not allowed,\nAllowed file: *.jpg,*.jpeg,*.bmp,*.png,*.tif');
      alert('sorry you can upload only *.jpg,*.jpeg,*.bmp,*.png,*.tif file');
      //            $('#file').attr('value', '');
      var control = $("#file");
      control.replaceWith(control = control.clone(true));
   }
}

function validateImage() {
   var isValid = true;
   $(".Imageuploadclass").each(function(i, obj) {

      if ($('#' + obj.id).val() == '') {
         isValid = false;
         $('#' + obj.id).css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });

      } else {
         $('#' + obj.id).css({
            "border": "",
            "background": ""
         });
      }

   });

   if (isValid == false) {
      return false;
   } else
   ('Thank you for submitting');
}

function validateImageonUpdate() {
   var isValid = true;


   if ($('#httppostedContentfile').val() == '') {

      isValid = false;
      $('#' + obj.id).css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });

   } else {
      $('#httppostedContentfile').css({
         "border": "",
         "background": ""
      });
   }

   if (isValid == false) {
      return false;
   }
}

function clientValidation() {

   var isValid = true;
   $(".formAddValidation").each(function(i, obj) {

      if ($('#' + obj.id).val() == '') {
         isValid = false;
         $('#' + obj.id).css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });

      } else {
         $('#' + obj.id).css({
            "border": "",
            "background": ""
         });
      }

   });

   if (isValid == false) {
      return false;
   } else
   ('Thank you for submitting');

   var SelectedddlValue = $("#Name").val();
   if (SelectedddlValue > 0 || SelectedddlValue == '') {
      if ($("#Name").val() == '' || $("#Name").val() == 0) {
         isValid = false;
         $("#Name").css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $("#Name").css({
            "border": "",
            "background": ""
         });
         return true;
      }
   }
}

function SelectManufacture() {
   var SelectedddlValue = $("#Name").val();
   if (SelectedddlValue > 0 || SelectedddlValue == '') {
      if ($("#Name").val() == '' || $("#Name").val() == 0) {
         isValid = false;
         $("#Name").css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $("#Name").css({
            "border": "",
            "background": ""
         });
         return true;
      }
   }
}

function EditContainerImage(ProductImageDetailID, ProductID, NDC, Description, Image, ContainerType, ContentImage) {
   //alert(ProductID + "," + NDC + "," + Description + "," + Image + "," + ContainerType);

   $("#PackagingOrContainerType").val(ContainerType);
   $("#ProductID").val(ProductID);
   $("#imageNDCID").val(NDC);
   $("#DescriptionforImageTab").val(Description);
   $("#HiddenProductImageDetailID").val(ProductImageDetailID);
   if (ContainerType == "Original") {
      $("#imgProduct").attr("src", "../images/ProductImage/Original/" + Image);

   } else {
      $("#imgProduct").attr("src", "../images/ProductImage/Repackager/" + Image);
   }
   $("#imgContentProductimage").attr("src", "../images/ProductImage/Content/" + ContentImage);
}

function AddImageOnclick() {

   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file1 == undefined) {
      if (file1 == undefined) {
         $('.browse-btn-container').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      }
   } else {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }
   if ($("#NDCUPCWithDashesforImageTab").val() == '' || $("#PackagingOrContainerType").val() == '') {
      if ($("#NDCUPCWithDashesforImageTab").val() == '') {
         $('#NDCUPCWithDashesforImageTab').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#NDCUPCWithDashesforImageTab').css({
            "border": "",
            "background": ""
         });
      }
      if ($("#PackagingOrContainerType").val() == '') {
         $('#PackagingOrContainerType').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#PackagingOrContainerType').css({
            "border": "",
            "background": ""
         });
      }
   }
   //if ($("#Name").val() == '') {
   //    alert("Please select manufacturer name from left menu dropdown");
   //    return false;
   //}

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("ProfileCode", $("#hiddenprofilecodeforsaveimage").val());
   $.ajax({
      type: "POST",
      url: '@Url.Action("AddProductImageUsingAjax", "Product")',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         $("#successerrormsg").text(response);
         $("#NDCUPCWithDashesforImageTab").val('')
         $("#DescriptionforImageTab").removeAttr('disabled', 'disabled');
         $("#DescriptionforImageTab").val('')
         $("#PackagingOrContainerType").val('')
         $("#imgProduct").attr('src', '../images/Defaultimages.jpg');
         $("#imgContentProductimage").attr('src', '../images/Defaultimages.jpg');
         alert(response);
         //location.reload();
         //if (response.result = "OK") {

         //    $("#successerrormsg").text("Save Successfully");
         //}
         //// alert("success");
         ////$('#GeneralSection').html(response.responseText);
      },
      error: function(error) {
         $("#successerrormsg").text("Save fails");
         // alert("error");
         //$('#GeneralSection').html(error.responseText);
      }
   });
}

function AddNewProductPhoto() {

   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file1 == undefined) {
      $('.browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;

   } else {
      $('.browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }
   if ($("#labelNDCUPCWithDashesforImageTab").text() == '' || $("#labelPackagingOrContainerType").val() == '') {
      if ($("#labelNDCUPCWithDashesforImageTab").text() == '') {
         $('#labelNDCUPCWithDashesforImageTab').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#labelNDCUPCWithDashesforImageTab').css({
            "border": "",
            "background": ""
         });
      }
      if ($("#labelPackagingOrContainerType").val() == '') {
         $('#labelPackagingOrContainerType').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#labelPackagingOrContainerType').css({
            "border": "",
            "background": ""
         });
      }
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDCUPCWithDashes", $("#labelNDCUPCWithDashesforImageTab").text());
   formData.append("Description", $("#labelDescriptionforImageTab").text());
   formData.append("ContainerType", $("#labelPackagingOrContainerType").val());
   formData.append("ProductID", $("#labelHiddenProductID").val());
   formData.append("ProfileCode", $("#labelHiddenProfileCode").val());
   $.ajax({
      type: "POST",
      url: '@Url.Action("AddNewProductPhotoUsingAjax", "Product")',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         //if (response.result = "OK") {

         $("#successerrormsg").text(response);
         alert(response);
         //$("#imgProduct").attr('src', '../images/Defaultimages.jpg');
         //$("#imgContentProductimage").attr('src', '../images/Defaultimages.jpg');
         location.reload(true);
         //}
         // alert("success");
         //$('#GeneralSection').html(response.responseText);
      },
      error: function(error) {
         //$("#successerrormsg").text(error);
         // alert("error");
         //$('#GeneralSection').html(error.responseText);
      }
   });
}

function UpdateImageOnclick() {

   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file == undefined || file1 == undefined) {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   // alert($("#imageNDCID").val());
   formData.append("NDC", $("#imageNDCID").val());
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", $("#HiddenProductImageDetailID").val());
   formData.append("ProductID", $("#ProductID").val());

   $.ajax({
      type: "POST",
      url: '@Url.Action("EditProductImageUsingAjax", "Product")',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         location.reload();

         $("#successerrormsg").text("Update Successfully");
         //alert("success");
         //$('#GeneralSection').html(response.responseText);
      },
      error: function(error) {
         $("#successerrormsg").text("Update fails");
         //alert("error");
         //$('#GeneralSection').html(error.responseText);
      }
   });
}

function UpdateIndividualImageclick(ProductImageDetailID, imageID) {

   var formData = new FormData();

   var file1 = document.getElementById("productcontainerimgfile_" + imageID).files[0];
   var file = document.getElementById("productcontentimgfile_" + imageID).files[0];

   formData.append("Contentfile", file);
   formData.append("productcontainerimgfile", file1);
   formData.append("ContainerType", $("#labelPackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", ProductImageDetailID);
   formData.append("ProductID", $("#labelHiddenProductID").val());

   $.ajax({
      type: "POST",
      url: '@Url.Action("UpdateIndividualContentImage", "Product")',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {

         //location.reload();
         $("#successerrormsg").text(response);
         alert(response);
      },
      error: function(error) {
         $("#successerrormsg").text(error);
      }
   });
}

function DeleteProductPhoto(ProductImageDetailID, imageID) {

   var checkedoriginalValue = $('#DeleteOriginalRepackager_' + imageID + ":checked").val();
   var checkedcontentValue = $('#DeleteContent_' + imageID + ":checked").val();
   if (checkedoriginalValue.length > 0 && !checkedcontentValue.length > 0) {
      alert("you can not delete only container image");
      return false;
   }
   if (checkedoriginalValue.length > 0 || checkedcontentValue.length > 0) {
      var rs = confirm('Are you sure you want to delete this record?');
      if (rs) {
         //var checkedoriginalValue = $('#DeleteOriginalRepackager_' + imageID).find('input[type="checkbox"]:checked').each(function () {
         //    //alert("item clicked, id = " + $(this).attr("data-id"));
         //    values.push($(this).attr("data-id"));
         //});
         //var checkedcontentValue = $('#DeleteContent_' + imageID).find('input[type="checkbox"]:checked').each(function () {
         //    //alert("item clicked, id = " + $(this).attr("data-id"));
         //    values.push($(this).attr("data-id"));
         //});


         $.ajax({
            type: "POST",
            url: '@Url.Action("DeleteProductPhoto", "Product")',
            data: { 'ProductImageDetailID': ProductImageDetailID, 'OriginalRpk': checkedoriginalValue, 'Content': checkedcontentValue },
            //dataType: 'json',
            //async: false,
            //contentType: false,
            //processData: false,
            success: function(response) {
               //location.reload();
               $("#successerrormsg").text(response);
               alert("Image deleted");
               location.reload(true);
            },
            error: function(error) {
               $("#successerrormsg").text(error);
            }
         });
      }
   } else {
      alert("Please select checkbox");
      return false;
   }
}

function AddProductSuccess(json) {
   console.log(json);

}

function UpdateImageOnclick() {
   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file == undefined || file1 == undefined) {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;
   } else {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   //alert($("#imageNDCID").val());
   formData.append("NDC", $("#imageNDCID").val());
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", $("#HiddenProductImageDetailID").val());
   formData.append("ProductID", $("#ProductID").val());

   $.ajax({
      type: "POST",
      url: '@Url.Action("EditProductImageUsingAjax", "Product")',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         location.reload();

         $("#successerrormsg").text("Update Successfully");

      },
      error: function(error) {
         $("#successerrormsg").text("Update fails");

      }
   });
}

$(document).ready(function() {
   //$(".alert-popup-body").hide();
   //$('.lot-icon').click(function (e) {
   //    urlName = $(this).attr('href');
   //    e.stopPropagation();
   //    e.preventDefault();
   //    if ($('#successerrormsgforproductdescription').text() == "Save Successfully") {
   //        isLeavewitoutSave = false;
   //    }
   //    if (isLeavewitoutSave == true) {

   //        $(".alert-popup-body").show();
   //        $(".alert-popup").animate({ "top": "40%" });
   //        return false;
   //    }
   //    else {
   //        window.location.href = urlName;
   //    }
   //    return true;
   //});


   //$(".alert-close, #btnAlertStay").click(function () {
   //    $(".alert-popup-body").hide();
   //    $(".alert-popup").animate({ "top": "40%" });
   //})

   //$('#btnAlertSave').click(function () {
   //    $("#myform").submit();
   //    if ($('#successerrormsgforproductdescription').text() == "Save Successfully") {
   //        isLeavewitoutSave = false;
   //    }
   //    $(".alert-popup-body").hide();
   //    $(".alert-popup").animate({ "top": "40%" });
   //    return true;

   //});
   //$('#btnAlertLeave').click(function () {
   //    isLeavewitoutSave = true;
   //    window.location.href = urlName;
   //    return true;
   //});

   $('#NDCUPCWithDashesforImageTab').blur(function(e) {
      if ($('#NDCUPCWithDashesforImageTab').val() != "") {
         var url = '@Url.Content("../Product/CheckExistanceNDCUPCWithDashes/")';
         $.post(url,
         {
            NDCUPCWithDashes: $('#NDCUPCWithDashesforImageTab').val()

         }, function(result) {
            if (result.data == "OK") {
               // alert(result.data2);
               //$('#UserName').css({
               //    "border": ""

               //});
               isUserValidate = true;
               $("#hiddenprofilecodeforsaveimage").val(result.data3);
               $("#DescriptionforImageTab").val(result.data2);

               $("#DescriptionforImageTab").attr("disabled", "disabled");
               $('#successerrormsg').html("");
            } else {
               //$('#UserName').css({
               //    "border": "1px solid red"

               //});
               $("#DescriptionforImageTab").removeAttr("disabled", "disabled");
               $('#successerrormsg').html("Please enter correct NDC With Dashes");
               $('#NDCUPCWithDashesforImageTab').val('');
               isUserValidate = false;
            }
         });
         return false;

      }
   });

   $('#NDCUPCWithDashes').blur(function(e) {

      if ($('#NDCUPCWithDashes').val() != "") {
         var url = '@Url.Content("../Product/CheckExistanceNDCUPCWithDashes/")';
         $.post(url,
         {
            NDCUPCWithDashes: $('#NDCUPCWithDashes').val()

         }, function(result) {
            if (result.data == "OK") {
               $("#successerrormsgforproductdescription").text("NDC With Dashes already exist in database");
               $('#NDCUPCWithDashes').val('');
               isUserValidate = true;
            } else {

               isUserValidate = false;
            }
         });
      }
   });
   $('#NDC').blur(function(e) {

      if ($('#NDC').val() != "") {
         var url = '@Url.Content("../Product/CheckExistanceNDCUPCWithDashes/")';
         $.post(url,
         {
            NDCUPCWithDashes: $('#NDC').val(),
         }, function(result) {
            if (result.data == "OK") {
               $("#successerrormsgforproductdescription").text("NDC already exist in database");
               $('#NDC').val('');
               isUserValidate = true;
            } else {

               isUserValidate = false;
            }
         });
         //return false;

      }
   });

   $("#CaseSize2").keypress(function() {
      if ($("#CaseSize1").val() == 0 || $("#CaseSize1").val() == '') {

         alert("Fill CaseSize1 field");
         $('#CaseSize1').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      }
   });

   $("#CaseSize3").keypress(function() {

      if ($("#CaseSize1").val() == 0 || $("#CaseSize1").val() == '') {
         alert("Fill CaseSize1 field");
         $('#CaseSize1').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      }

      if ($("#CaseSize2").val() == 0 || $("#CaseSize2").val() == '') {
         alert("Fill CaseSize2 field");
         $('#CaseSize2').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      }

   });

   $("#CaseSize2").change(function() {
      if ($("#CaseSize1").val() > 0) {
         if ($("#CaseSize2").val() == $("#CaseSize1").val()) {
            alert("value should be greater or less than from case size1");
            $("#CaseSize2").val('');
            $('#CaseSize2,#CaseSize1').css({
               "border": "1px solid red",
               "background": "#FFCECE"
            });
            return false;

         }
      } else {
         alert("First Enter CaseSize 1 field");
         $('#CaseSize1').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
      }
   });

   $("#CaseSize3").change(function() {
      if ($("#CaseSize2").val() > 0) {
         if ($("#CaseSize1").val() == $("#CaseSize2").val() || $("#CaseSize2").val() == $("#CaseSize3").val() || $("#CaseSize1").val() == $("#CaseSize3").val()) {

            alert("value should be greater or less than from case size2");
            $("#CaseSize3").val('');
            $('#CaseSize3,#CaseSize2').css({
               "border": "1px solid red",
               "background": "#FFCECE"
            });
            return false;
         } else {
            $('#CaseSize3,#CaseSize2,#CaseSize1').css({
               "border": "",
               "background": ""
            });
         }
      } else {
         alert("First Enter CaseSize 2 field");
         $('#CaseSize2').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
      }
   });

   //$(".other-user-disable").find("input,button,select,textarea").attr("disabled", "disabled");

   $("#createlotNumberWithDiverstedLink").click(function() {

      $("#divestedShowList").hide();
      $("#createlotNumberWithDivested").show();

   });

   $("#DiverseManufacturerProfileCode").change(function() {
      var profileId = $("#DiverseManufacturerProfileCode").val();
      $(".model-popup ").show();
      $(".model-box").animate({ "top": "20%" });

   });

   $(".imglarge").mouseover(function() {
      $(this).siblings("#pop-up").show();
      //$("#pop-up").show();
   });
   $(".imglarge").mouseout(function() {
      $(this).siblings("#pop-up").hide();
      //$("#pop-up").hide();
   });

   $(".content-img-large").mouseover(function() {
      $(this).siblings("#content-pop-up").show();
      //$("#pop-up").show();
   });
   $(".content-img-large").mouseout(function() {
      $(this).siblings("#content-pop-up").hide();
      //$("#pop-up").hide();
   });

   $('#ProductEndDatedtp').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   $('#LotNumbersData_ExpirationDate').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });
   $("#LotNumbersData_ExpirationDate").keypress(function() {

      $('#LotNumbersData_ExpirationDate').datepicker({
         //changeMonth: true,
         //changeYear: true,
         //dateFormat: 'MM dd, yy',
         //minDate: 1,
        
      });

   });

   $('#tab-container').easytabs();

   $('#defaultTable').dragtable();

   $('#ProductStartDatedtp').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   var packageresult = $("#PackageSize").val();
   if (packageresult == 0) {
      $("#PackageSize").val('');
      $("#UnitsPerPackage").val('');
      //$("#ProductID").val('');
   }
   $("#productdesupdateId").hide();
   $("#additionaldesupdatedid").hide();
   $("#physiciandesupdateid").hide();
   var qrStr = window.location.search;


   if (qrStr != undefined && qrStr != "") {
      qrStr = qrStr.split("?")[1].split("=")[1];
      if (qrStr == 'Add' || qrStr == 'Add&prodId') {

         $("#productdiv").find("input,button,select,textarea").removeAttr('disabled');
         $("#physiciandiv").find("input,button,select,textarea").removeAttr("disabled", "disabled");
         $("#additionaldiv").find("input,button,textarea").removeAttr("disabled", "disabled");
      } else {
         $("#productdiv").find("input,button,select,textarea").attr("disabled", "disabled");
         $("#physiciandiv").find("input,button,select,textarea").attr("disabled", "disabled");
         $("#additionaldiv").find("input,button,select,textarea").attr("disabled", "disabled");
      }
   }

   $("#productdesId").click(function() {
      $("#productdesupdateId").show();
      $("#productdesId").hide();
      //$("#productdesId").attr("value", 'Update');
      $("#productdiv").find("input,button,select,textarea").removeAttr('disabled');
   });

   $("#physiciandesid").click(function() {

      $("#physiciandesupdateid").show();
      $("#physiciandesid").hide();
      //$("#physiciandesid").attr("value", 'Update');
      $("#physiciandiv").find("input,button,select,textarea").removeAttr('disabled');

   });
   $("#additionaldesid").click(function() {
      $("#additionaldesupdatedid").show();
      $("#additionaldesid").hide();
      //$("#additionaldesid").attr("value", 'Update');
      $("#additionaldiv").find("input,button,textarea").removeAttr('disabled');
   });


   $('#productdesupdateId').click(function(e) {

      var isValid = true;
      $(".physiciandesupdate").each(function(i, obj) {

         //if ($('#' + obj.id).val() == '' || $('#' + obj.id).val() == 0) {
         if ($('#' + obj.id).val() == '') {
            isValid = false;
            $('#' + obj.id).css({
               "border": "1px solid red",
               "background": "#FFCECE"
            });

         } else {
            $('#' + obj.id).css({
               "border": "",
               "background": ""
            });
         }
      });

      if (isValid == false) {
         e.preventDefault();
         return false;
      } else
      ('Thank you for submitting');


      $("#productdesId").show();
      $("#productdesupdateId").hide();
      var requestData =
      {
         ProductID: $('#ProductID').val(),
         ProfileCode: $('#ProfileCode').val(),
         Strength: $('#Strength').val(),
         Description: $('#Description').val(),
         NDC: $('#NDC').val(),
         PackageSize: $('#PackageSize').val(),
         NDCUPCWithDashes: $('#NDCUPCWithDashes').val(),
         UnitsPerPackage: $('#UnitsPerPackage').val(),
         UnitOfMeasure: $('#measureTypeid').val(),
         UPC: $('#UPC').val(),
         RXorOTC: $('#RXorOTC').val(), //$("input[name=RXorOTC]:checked").val(),
         DosageCode: $('#DosageCode').val(),
         ControlNumber: $('#controlTypeid').val(),
         ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
         SectionNumber: 1,
         PillType: $('#PillType').val(),
      };
      var url = '@Url.Content("~/Product/UpdateProductDescriptionSection/")';
      $.ajax({
         type: "POST",
         url: url,

         //  data: "{'AJAXProduct':'" + $('#ProductID').val() + "','AJAXProfile':'" + $('#ProfileCode').val() + "','Strength':'" + $('#Strength').val() + "','AjaxDescription':'" + $('#Description').val() + "','AjaxNDC':'" + $('#NDC').val() + "','PackageSize':'" + $('#PackageSize').val() + "','AjaxNDCUPCWithDashes':'" + $('#NDCUPCWithDashes').val() + "','UnitsPerPackage':'" + $('#UnitsPerPackage').val() + "','UnitOfMeasure':'" + $('#measureTypeid').val() + "','UPC':'" + $('#UPC').val() + "','DosageCode':'" + $('#DosageCode').val() + "','ControlNumber':'" + $('#controlTypeid').val() + "','ARCOSReportable':'" + $("input[name=ARCOSReportable]:checked").val() + "'}",
         data: requestData,
         //contentType: "application/json; charset=utf-8",
         dataType: "json"

      }).done(function(data) {

         if (data.VersionChanged) {
            $("#alertmessageid").text('Policy has already been modified by other user');
            $(".alert-popup-body-message").show();
            $("#productdiv").find("input,button,select,textarea").attr("disabled", "disabled");
         } else if (data.Result == 'Deleted') {
            $("#alertmessageid").text('Policy has already been deleted by other user');
            $(".alert-popup-body-message").show();
            $("#productdiv").find("input,button,select,textarea").attr("disabled", "disabled");
         } else {
            $("#alertmessageid").text(data);
            $(".alert-popup-body-message").show();
            $("#productdiv").find("input,button,select,textarea").attr("disabled", "disabled");
         }
         //Successfully pass to server and get response
         //if (data.result = "OK") {
         //$("#successerrormsgforproductdescription").text(data);
         //alert(" Product Update successfully.");
         //$("#productdiv").find("input,button,select,textarea").attr("disabled", "disabled");
         //}
      }).fail(function(response) {
         if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
         }
      });
   });

   $('#physiciandesupdateid').click(function(e) {
      $("#physiciandesid").show();
      $("#physiciandesupdateid").hide();
      var requestData =
      {
         ProductID: $('#ProductID').val(),
         PhysicianSample: $("input[name=PhysicianSample]:checked").val(),
         PhysicianSampleProductNumber: $('#PhysicianSampleProductNumber').val(),
         TamperResistantSealExists: $("input[name=TamperResistantSealExists]:checked").val(),
         DEAWatchList: $("input[name=DEAWatchList]:checked").val(),
         Withdrawn: $("input[name=Withdrawn]:checked").val(),
         CaseSize1: $('#CaseSize1').val(),
         CaseSize2: $('#CaseSize2').val(),
         CaseSize3: $('#CaseSize3').val(),
         IndividualCountWeight: $('#IndividualCountWeight').val(),
         ContainerWeight: $('#ContainerWeight').val(),
         FullContainerWeightWithContents: $('#FullContainerWeightWithContents').val(),
         UnitDose: $("input[name=UnitDose]:checked").val()
      };
      var url = '@Url.Content("~/Product/UpdatePhysicianDescriptionSection/")';
      $.ajax({
         type: "POST",
         url: url,
         data: requestData,
         dataType: "json"
      }).done(function(data) {
         //if (data.result = "OK") {
         //    $("#successerrormsgforproductdescription").text("Update successfully");
         //    alert("Update successfully.");
         //    $("#physiciandiv").find("input,button,textarea").attr("disabled", "disabled");
         //}
         $("#successerrormsgforproductdescription").text(data);
         $("#physiciandiv").find("input,button,textarea").attr("disabled", "disabled");
      }).fail(function(response) {
         if (response.status != 0) {
         }
      });
   });

   $('#additionaldesupdatedid').click(function(e) {

      var requestData = {
         ProductID: $('#ProductID').val(),
         MFGProductNumber: $('#MFGProductNumber').val(),
         RollupProductID: $('#RollupProductID').val(),
         ForceQuantityCountNotes: $('#txtAreaforcecountnotes').val(),
         EDINumber: $('#EDINumber').val(),
         WholesalerNumber: $('#WholesalerNumber').val(),
         Refrigerated: $("input[name=Refrigerated]:checked").val(),
         DivestedByLot: $("input[name=DivestedByLot]:checked").val(),
         SpecialHandlingInstructions: $('#txtAreaspecial').val(),
         ExcludeFromExternalInterface: $("input[name=ExcludeFromExternalInterface]:checked").val(),
         ExcludeFromDropDown: $("input[name=ExcludeFromDropDown]:checked").val(),
         AlwaysSortToQuarantine: $("input[name=AlwaysSortToQuarantine]:checked").val(),
         ForceQuantityCount: $("input[name=ForceQuantityCount]:checked").val()
      };

      // data: JSON.stringify(requestData),
      $("#additionaldesid").show();
      $("#additionaldesupdatedid").hide();
      var url = '@Url.Content("~/Product/UpdateAdditionalDescriptionSection/")';
      $.ajax({
         type: "POST",
         url: url,
         //long ProductID, string BrandorGeneric, string TherapeuticClass, string ShortSupply, string ForceQuantityCountNotes, string SpecialHandlingInstructions, bool TamperResistantSealExists, bool DEAWatchList, bool ARCOSReportable, bool Withdrawn, bool ExcludeFromExternalInterface, bool ExcludeFromDropDown, bool AlwaysSortToQuarantine, bool ForceQuantityCount, bool IsRepackager, int DiverseManufacturerProfileCode

         //data: "{'ProductID':'" + $('#ProductID').val() + "','DiverseManufacturerProfileCode':'" + $('#DiverseManufacturerProfileCode').val() + "','TherapeuticClass':'" + $('#TherapeuticClass').val() + "','ShortSupply':'" + $('#ShortSupply').val() + "','ForceQuantityCountNotes':'" + $('#txtAreaforcecountnotes').val() + "','SpecialHandlingInstructions':'" + $('#txtAreaspecial').val() + "','TamperResistantSealExists':'" + $("input[name=TamperResistantSealExists]:checked").val() + "','DEAWatchList':'" + $("input[name=DEAWatchList]:checked").val() + "','ARCOSReportable':'" + $("input[name=ARCOSReportable]:checked").val() + "','Withdrawn':'" + $("input[name=Withdrawn]:checked").val() + "','ExcludeFromExternalInterface':'" + $("input[name=ExcludeFromExternalInterface]:checked").val() + "','ExcludeFromDropDown':'" + $("input[name=ExcludeFromDropDown]:checked").val() + "','AlwaysSortToQuarantine':'" + $("input[name=AlwaysSortToQuarantine]:checked").val() + "','ForceQuantityCount':'" + $("input[name=ForceQuantityCount]:checked").val() + "','IsRepackager':'" + $("input[name=IsRepackager]:checked").val() + "'}",
         data: requestData,
         //contentType: "application/json; charset=utf-8",
         dataType: "json"
      }).done(function(data) {
         //if (data.result = "OK") {
         //    $("#successerrormsgforproductdescription").text("Update successfully");
         //    alert("Update successfully.");
         //    $("#additionaldiv").find("input,button,textarea").attr("disabled", "disabled");
         //}
         $("#successerrormsgforproductdescription").text(data);
         $("#additionaldiv").find("input,button,textarea").attr("disabled", "disabled");
      }).fail(function(response) {
         if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
         }
      });
   });

   $('.main-container').click(function() {
      $('#successerrormsgforproductdescription').text("");
      $('#successerrormsg').text("");

   });

});
