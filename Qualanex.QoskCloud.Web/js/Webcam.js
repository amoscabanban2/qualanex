﻿//*****************************************************************************
//
// Webcam.js
//    Copyright (c) 2016 - 2018, Qualanex LLC.
//    All rights reserved.
//
// Summary:
//    This provides a relatively simple interface to access one or more webcams
//    through the browser's api. 
//
//
//
// Notes:
//    <video> elements with a class of "webcamCapture" will be  used as the preview
//    for the webcams
//    <select> elements with a class of "webcamSelect" and an attribute of "data-target"
//    containing the id of the above <video> element will be populated with a list of 
//    available webcams that the user can choose between.
//    A window level function window.validateCameraSetup must be defined. It does not need
//    to return anything to webcam.js, but provides an opportunity for calling code to validate
//    the settings 
//
//    WebRTC provides a shim to insulate javascript code from changes in the browser API
//    and differences between browsers. This is stored locally in ~\js\webrtc-adapter\adapter.js
//    and should be updated on a regular basis from the source at https://webrtc.github.io/adapter/adapter-latest.js
//    adapter.js should be included as a script reference wherever webcam.js is used
//    Visit https://webrtc.org for more information, samples are available at https://github.com/webrtc
//
//    Modern browsers require a valid HTTPS environment to allow camera access, and
//    users are required to explicitly allow access to the cameras at least once
//
//    The majority of the complexity in our webcam implementation comes from viewing two 
//    cameras simultaneously. Additional simultaneous cameras exponentially increase the
//    complexity.
//
//    The following are the functions used to provide webcam functionality in calling scripts:

//    loadWebcams(..) initializes the camera, including acquiring user permission 
//    if needed.
//    
//    unLoadWebcams() deactivates the camera elements
//
//    capture(..) captures a still image and uploads it to azure
//
//    deleteImage(..) deletes a captured image from azure
//*****************************************************************************

$(function ()
{
   var streamArray = [];
   var webcams = [];
   var mediaStream = null;
   var webcamList;
   var currentCam = null;
   var headers = {};

   var mediaRecorder;
   var recordedBlobs;

   //when cameras are initialized, success and error methods are stored as 
   //instance variables since it is an async process and the api does not allow
   //setting callback methods dynamically
   var errorMethod;
   var successMethod;

   //webcamLock is used as a semaphore to prevent calls to webcam methods
   //from interrupting operations already in progress
   var webcamLock = false;
   window.webcamLock = webcamLock;

   // writeError(string) - Provides a way to display errors to the user
   // currently this is handled by calling the errorMethod defined in loadWebcams()
   var writeError = function (string)
   {
      errorMethod(string);
   };

   // initializeVideoStream() - Callback function when getUserMedia() returns successfully with a mediaStream object,
   // set the mediaStream on the video tag. initializeVideoStream() checks that no other operations are in progress before
   //calling initializeVideoStreamWithLock()

   function initializeVideoStream(stream, cam)
   {
      //because this is async and multiple cameras are loading simultaneously, wait for operations to finish before continuing
      if (webcamLock)
      {
         setTimeout(function ()
         {
            initializeVideoStream(stream, cam);
         }, 800);
      }
      else
      {
         initializeVideoStreamWithLock(stream, cam);
      }

   }
   var initializeVideoStreamWithLock = function (stream, cam)
   {
      webcamLock = true;
      mediaStream = stream;
      var video = $(".webcamCapture")[cam];

      if ($(video).length)
      {
         //the api returns the stream from a webcam, apply it to the video element
         video.srcObject = mediaStream;
         //start playback
         if (video.paused)
         {
            video.play();
         }
      }
      else
      {
         //if there's no video element to play this track, remove it from the stream so it
         //will be available for later use
         stream.removeTrack(stream.getTracks()[0]);
      }

      webcamLock = false;
   };

   // getUserMediaError() - Callback function when getUserMedia() returns error
   // 1. Show the error message with the error.name

   var getUserMediaError = function (e)
   {
      if (e.name.indexOf('NotFoundError') >= 0)
      {
         writeError('Webcam not found.');
      }

      if (e.name.indexOf('NotAllowedError') >= 0)
      {
         writeError('You must allow the use of cameras in the browser and press Reload to continue.');
      }
      else
      {
         writeError('The following error occurred: "' + e.name + '" Please check your webcam device(s) and press Reload to try again.');
      }

      throw (e);
   };

   // 1. Capture a video frame from the video tag and upload to the server and pass the resulting image url to a callback method
   // passed in video parameter must be a DOM reference to a loaded video element
   // The image bitstream is returned and can be used as the src of an img element
   var capture = function (video, callback)
   {
      if (video.srcObject === null)
      {
         callback("");
         return;
      }

      //create a temporary canvas to capture the still, matching the dimensions of the video stream
      var canvas = document.createElement("canvas");
      var videoWidth = video.videoWidth;
      var videoHeight = video.videoHeight;

      if (canvas.width !== videoWidth || canvas.height !== videoHeight)
      {
         canvas.width = videoWidth;
         canvas.height = videoHeight;
      }

      var ctx = canvas.getContext('2d');
      ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
      var token = $('input[name="__RequestVerificationToken"]').val();
      var imgData = canvas.toDataURL('image/jpeg');

      if (imgData === "data:,")
      {
         showError("Cameras must finish loading before capturing images");
         successMethod();
         return;
      }

      $.ajax({
         url: '/Webcam/Capture',  //Server script to process data
         type: 'POST',
         headers: headers,

         //Ajax events
         success: function (data)
         {
            if (data.imageUri === undefined)
            {
               callback("error");
            }

            callback(data.imageUri, data.tempLink);
         },
         error: function (err)
         {
            console.log(err);
            showError(err.statusText);
            throw (err);
         },
         // Form data
         data: { __RequestVerificationToken: token, file: imgData }
      });
      canvas = null;
      return imgData;
   };
   window.capture = capture;

   //deleteImage allows an uploaded image to be deleted from the webcam-uploads container
   //of the qoskimages storage account. This is to be used when the image is being retaken.
   var deleteImage = function (uri)
   {
      if (isNullOrWhitespace(uri))
      {
         return false;
      }

      var token = $('input[name="__RequestVerificationToken"]').val();
      $.ajax({
         url: '/Webcam/DeleteImage',  //Server script to process data
         type: 'POST',
         headers: headers,
         //Ajax events
         error: function (err)
         {
            console.log(err);
         },
         // Form data,
         data: { uri: uri, __RequestVerificationToken: token }
      });
   };
   window.deleteImage = deleteImage;

   //calls the browser api to start the stream for a specific device
   //deviceId is a reference to a specific webcam that may have been taken
   //from a select list or a cookie
   //after is a function called with the result of the api call
   var startWebcam = function (deviceId, after)
   {
      navigator.mediaDevices.getUserMedia({
         video: {
            width: 1280,
            height: 720,
            deviceId: { exact: deviceId }
         },
         audio: false //we don't need audio for stills, don't waste the ram
      }).then(after).catch(getUserMediaError);
   }


   //nextWebCam is recursively called to initialize each video
   //element with a webcam media stream
   var nextWebCam = function ()
   {
      //prevent interrupting webcam calls
      webcamLock = true;
      //currentCam is the index used to iterate through webcamList and streamArray
      //webcamList is an array of camera deviceIds
      if (currentCam !== null)
      {
         currentCam++;

         if (currentCam >= webcamList.length)
         {
            currentCam = 0;
         }
      }
      else
      {
         currentCam = 0;
      }

      if (streamArray[currentCam] != undefined)
      {
         initializeVideoStream(streamArray[currentCam], currentCam);
         webcamLock = false;
         return;
      }

      if (webcamList === null)
      {
         return;
      }
      //the callback from startWebcam adds the stream object to streamArray with an index matched to the its deviceId in webcamList
      startWebcam(webcamList[currentCam], function (stream)
      {
         streamArray[currentCam] = stream;
         initializeVideoStream(stream, currentCam);
         //when loading webcams, but the Nth stream in the Nth video element
         $($(".webcamCapture")[currentCam]).attr("data-deviceId", webcamList[currentCam]);

         //if there are more streams to load, call nextWebCam again with a delay to avoid interrupts
         if (currentCam < webcamList.length - 1)
         {
            setTimeout(nextWebCam, 500);
         }
         else
         { //all streams have been loaded
            //populate the select lists with options for the connected cameras
            $(".webcamSelect").each(function ()
            {
               var select = this;
               var useCookie = false;

               webcams.forEach(function (wc, i)
               {
                  //check if the value in the cookie matches an existing camera, the browser occasionally generates new ids for each device
                  if ($.cookie(select.id) === wc.deviceId)
                  {
                     useCookie = true;
                  }

                  $("<option data-option='webcam'>").val(wc.deviceId).text((i + 1) + ": " + wc.label).appendTo(select);
               });

               //if there is a device matching the id from the cookie, set that as the selected value
               if (useCookie)
               {
                  $(select).val($.cookie(select.id));
               }
               else
               {
                  //delete the cookie, that device id no longer exists
                  $.removeCookie(select.id);

                  //try to 'smartly' determine which camera feed goes to where based on the feed label. If the camera names are not what's expected  
                  //use 'dumb' matching, the nth video stream will be the nth display object 
                  var i = 0;
                  outerswitch:
                  switch ($(select).attr("data-target"))
                  {
                     case "sideCamera":
                        while (i < webcams.length)
                        {
                           if (webcams[i].label === "Camera3 (1bcf:e001)")
                           {
                              $(select).val(webcams[i].deviceId);
                              break outerswitch;
                           }
                           i++;
                        }
                        $(select).val($("#" + $(select).attr("data-target")).attr("data-deviceId"));
                        break;
                     case "topCamera":
                        while (i < webcams.length)
                        {
                           if (webcams[i].label === "HD Cam (1bcf:e001)")
                           {
                              $(select).val(webcams[i].deviceId);
                              break outerswitch;
                           }
                           i++;
                        }
                        $(select).val($("#" + $(select).attr("data-target")).attr("data-deviceId"));
                        break;
                     default:
                        {
                           $(select).val($("#" + $(select).attr("data-target")).attr("data-deviceId"));
                           break;
                        }
                  }
               }

               //update the video element to show the selected camera 
               changeCamera(select);
            });

            webcamLock = false;
         }
      });
   };

   // enumerateMediaDevices() - function to start enumerateDevices() and define the callback functions

   //loadWebcams initializes devices, sets error and success methods to handle the eventual load outcome
   //of the cameras
   //error accepts a function that is used to pass any errors back to the calling code through initialization and capture
   //success accepts a function that is called after a successful camera load
   var loadWebcams = function (error, success)
   {
      errorMethod = error;
      successMethod = success;

      if (webcamLock)
      {
         console.log("Requested operation cannot be performed: A webcam operation is in progress");
         return;
      }

      webcamLock = true;

      //if webcams are not already in the process of loading, call the api to start the process
      //mediaDevices.enumerateDevices probes every device on the machine and devicecsCallback filters
      //the list to only video input objects (ie webcams) 
      if (!($(".webcamCapture")[0].srcObject))
      {
         /*eslint-disable*/
         navigator.mediaDevices.enumerateDevices().then(devicesCallback).catch(getUserMediaError);
         /*eslint-enable*/
      }
   };
   window.loadWebcams = loadWebcams;

   //When cameras are no longer needed they should be unloaded to conserve memory
   //
   var unloadWebcams = function ()
   {
      webcamLock = true;
      //stop each video stream and clear it from the object so the memory can be reclaimed
      $(".webcamCapture").each(function ()
      {
         if (this.srcObject)
         {
            this.srcObject.getVideoTracks()[0].stop();
            this.srcObject = null;
         }
      });

      //clear out the select lists
      $("[data-option='webcam']").remove();
      //reset the instance variables
      streamArray = [];
      webcams = [];
      mediaStream = null;
      webcamList = null;
      currentCam = null;

      mediaRecorder = null;
      recordedBlobs = null;
      webcamLock = false;
   }
   window.unloadWebcams = unloadWebcams;

   //This is the callback from the api call that initializes the webcams when successful
   //It should be noted that the 'dumb' camera assignment in startWebCam is affected by the order enumerateDevices 
   //returns the webcams.  Switching which USB port the cameras are plugged into can change the order of the cameras 
   //in this list.  A quick and dirty way to 'fix' any new camera models without changing code.  
   var devicesCallback = function (devices)
   {
      // Identify all webcams
      webcamList = [];
      //enumerate the available video streaming devices
      for (var i = 0; i < devices.length; i++)
      {
         if (devices[i].kind === 'videoinput')
         {
            webcamList[webcamList.length] = devices[i].deviceId;
            webcams[webcams.length] = devices[i];
         }
      }

      if (webcamList.length > 0)
      {
         nextWebCam();
      }
      else
      {
         writeError('Webcams not found.');
      }

      if (successMethod != null)
      {
         successMethod();
      }
   };

   //changeCamera updates the video stream to match the current selection of the passed in select list DOM reference
   //changeCamera checks the changeCameraLock semaphore, if not locked calls changeCameraWithLock
   //window.validateCameraSetup is called at the end of a successful selection change
   var changeCameraLock = false;
   function changeCamera(select)
   {
      if (webcamLock || changeCameraLock)
      {
         setTimeout(function ()
         {
            changeCamera(select);
         }, 800);
      }
      else
      {
         changeCameraWithLock(select);
      }

   }
   window.changeCamera = changeCamera;
   function changeCameraWithLock(select)
   {
      webcamLock = true;
      changeCameraLock = true;
      //update the cookie so that next time the cameras are loaded, the selection is retained
      //These cookies only last as long as the browser session
      $.cookie(select.id, $(select).val());
      var video = document.getElementById($(select).attr("data-target"));

      //Added null check to prevent null ref error if user clears form too quickly
      if (video && video.srcObject !== 'null' && video.srcObject !== 'undefined')
      {
         mediaStream = video.srcObject;

         if (mediaStream)
         {
            var videoTracks = mediaStream.getVideoTracks();
            videoTracks[0].stop();
            mediaStream = undefined;
         }

         if (typeof video.srcObject !== 'undefined')
         {
            video.srcObject = null;
         }

         video.removeAttribute("src");

         startWebcam($(select).val(), function (stream)
         {

            video.srcObject = stream;

            if (video.paused)
            {
               video.play();
            }

            webcamLock = false;
         });
      }
      window.validateCameraSetup();
      changeCameraLock = false;
   }

   //event trigger to update the video stream when a select list value is changed
   $(document).on("change", ".webcamSelect", function ()
   {
      changeCamera(this);
   });


   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////////////  Video below, this is not functional proof of concept code
   //////////// this increases memory use up to 3 gigabytes that are released until
   //////////// the browser is closed
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


   function startRecording(stream)
   {
      recordedBlobs = [];
      var options = { mimeType: 'video/webm;codecs=vp9' };

      if (!MediaRecorder.isTypeSupported(options.mimeType))
      {
         console.log(options.mimeType + ' is not Supported');
         options = { mimeType: 'video/webm;codecs=vp8' };

         if (!MediaRecorder.isTypeSupported(options.mimeType))
         {
            console.log(options.mimeType + ' is not Supported');
            options = { mimeType: 'video/webm' };

            if (!MediaRecorder.isTypeSupported(options.mimeType))
            {
               console.log(options.mimeType + ' is not Supported');
               options = { mimeType: '' };
            }
         }
      }
      try
      {
         mediaRecorder = new MediaRecorder(stream, options);
      }
      catch (e)
      {
         console.error('Exception while creating MediaRecorder: ' + e);
         writeError('Exception while creating MediaRecorder: ' + e + '. mimeType: ' + options.mimeType);
         return;
      }

      console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
      mediaRecorder.ondataavailable = handleDataAvailable;
      mediaRecorder.start(10); // collect 10ms of data
      console.log('MediaRecorder started', mediaRecorder);
   }

   window.startRecording = startRecording;

   function stopRecording()
   {
      mediaRecorder.stop();
      mediaRecorder = "";
   }

   window.stopRecording = stopRecording;

   function play(video)
   {
      var superBuffer = new Blob(recordedBlobs, { type: 'video/webm' });
      video.src = window.URL.createObjectURL(superBuffer);
      video.controls = true;
   }

   window.play = play;

   function saveVideo()
   {
      var blob = new Blob(recordedBlobs, { type: 'video/webm' });
      var url = window.URL.createObjectURL(blob);
      var a = document.createElement('a');
      a.style.display = 'none';
      a.href = url;
      a.download = 'test.webm';
      document.body.appendChild(a);
      a.click();

      setTimeout(function ()
      {
         document.body.removeChild(a);
         window.URL.revokeObjectURL(url);
      }, 100);
   }

   window.saveVideo = saveVideo;

   var uploadVideo = function (callback)
   {
      var blob = new Blob(recordedBlobs, { type: 'video/webm' });
      var token = $('input[name="__RequestVerificationToken"]').val();
      var data = new FormData();
      data.append('file', blob);
      data.append('__RequestVerificationToken', token);
      var request = new XMLHttpRequest();

      request.onreadystatechange = function ()
      {
         if (request.readyState === 4 && request.status === 200)
         {
            console.log(request.responseText);
            callback(JSON.parse(request.response).videoUri);
         }
      };

      request.open('POST', '/Webcam/UploadVideo');
      request.send(data);
   }

   window.uploadVideo = uploadVideo;

   function handleDataAvailable(event)
   {
      if (event.data && event.data.size > 0)
      {
         recordedBlobs.push(event.data);
      }
   }

   $(document).on("click", ".videoRecord", function ()
   {
      var stream = document.querySelector("#" + $(this).attr("data-target")).srcObject;
      startRecording(stream);
      $(this).removeClass("videoRecord");
      $(this).addClass("videoStop");
      $(this).html("Stop Recording");
   });

   $(document).on("click", ".videoStop", function ()
   {
      stopRecording();
      var btnPlay = document.querySelector(".videoPlay");
      var btnUpload = document.querySelector(".videoUpload");
      var btnSave = document.querySelector(".videoSave");

      if (btnPlay)
      {
         btnPlay.disabled = false;
      }

      if (btnUpload)
      {
         btnUpload.disabled = false;
      }

      if (btnSave)
      {
         btnSave.disabled = false;
      }

      $(this).removeClass("videoStop");
      $(this).addClass("videoRecord");
      $(this).html("Start Recording");
   });
});