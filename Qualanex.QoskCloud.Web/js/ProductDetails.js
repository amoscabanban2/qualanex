﻿var isLeavewitoutSave = true;
var urlName;

function GenerateOriginalGrid(jsonOriginaldata) {

   if (jsonOriginaldata != null) {

      var rows = jsonOriginaldata.OriginalManufactureLotNumbers.LotNumbersList;

      var selectGRPProfile = {
         "text": "<label><input type=\"checkbox\" class=\"selecctall\" id=\"SelectAllProfileGRP\"><i></i></label>",
         "datafield": "Select",
         "width": "100",
         "cellsrenderer": function(row, column, value) {
            var isDiverselotSelected = dataAdapter._source.localdata[row].IsDiverseLotNumber;
            var lotID = dataAdapter._source.localdata[row].LotID;

            // alert(JSON.stringify(dataAdapter));
            if (value.indexOf('#') != -1) {
               value = value.substring(0, value.indexOf('#'));
            }
            value = $.trim(value);
            if (isDiverselotSelected == true) {
               var html = "<label><input type=\"checkbox\" class=\"check-item1\" checked=\"checked\" id=\"GRPPRF-Profile\" data-id=" + lotID + " ><i></i></label>";
               return html;
            } else {
               var html = "<label><input type=\"checkbox\" class=\"check-item1\" id=\"GRPPRF-Profile\" data-id=" + lotID + "><i></i></label>";
               return html;
            }
         }
      }

      // prepare the data
      var columns = [
         { text: 'Description', editable: false, datafield: 'Description' },
         { text: 'NDC', editable: false, datafield: 'NDC' },
         { text: 'Lot Number', editable: false, datafield: 'LotNumber' }
      ]
      columns.push(selectGRPProfile);
      var source =
      {
         datatype: "json",
         id: 'id',
         localdata: rows
      };
      var dataAdapter = new $.jqx.dataAdapter(source);
      $("#jqxOriginalgrid").jqxGrid(
      {
         width: '100%',
         sortable: true,
         filterable: true,
         pageable: true,
         columnsreorder: true,
         source: dataAdapter,
         //theme: theme,
         columnsresize: true,
         columns: columns

      });
   }
}

function GenerateOwnOwnershipGrid(jsonOwnOwnershipdata) {

   if (jsonOwnOwnershipdata != null && jsonOwnOwnershipdata.DiverseManufactureLotNumbers.ProfileCode != -1) {

      var rows = jsonOwnOwnershipdata.DiverseManufactureLotNumbers.LotNumbersList;
      // prepare the data
      var columns = [
         { text: 'Profile Name', editable: false, datafield: 'ProfileName' },
         { text: 'NDC', editable: false, datafield: 'NDC' },
         { text: 'Lot Number', editable: false, datafield: 'LotNumber' },
         { text: 'Description', editable: false, datafield: 'Description' }
      ]

      var source =
      {
         datatype: "json",
         id: 'id',
         localdata: rows
      };
      var dataAdapter = new $.jqx.dataAdapter(source);
      $("#jqxLotNumberOwneshipgrid").jqxGrid(
      {
         width: '100%',
         sortable: true,
         filterable: true,
         pageable: true,
         columnsreorder: true,
         source: dataAdapter,
         //theme: theme,
         columnsresize: true,
         columns: columns

      });
   }
   // initialize the popup window and buttons.
}

function ScrollBarPhoto() {

   var containerwidth = 0;
   var containerwidthmargin = ($("#containerimage li").length) * 25 + 50;
   $("#containerimage li").each(function() {
      containerwidth += $(this).outerWidth();
   });
   var containerwidthtotal = containerwidth + containerwidthmargin;
   $("#containerimage").css({ "width": +containerwidthtotal + "px" });

   var contentwidth = 0;
   var contentwidthmargin = ($("#contentimage li").length) * 25 + 50;
   $("#contentimage li").each(function() {
      contentwidth += $(this).outerWidth();
   });
   var contentwidthtotal = contentwidth + contentwidthmargin;
   $("#contentimage").css({ "width": +contentwidthtotal + "px" });
}

function CheckDecimal(str) {
   str = alltrim(str);

   return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(str);
}

function setLocalStorage(ddlvalues) {
   if (typeof (Storage) !== "undefined") {
      localStorage.setItem("Dosagelist", ddlvalues);
   }
}

function getLocalStorage() {
   if (typeof (Storage) !== "undefined") {
      var dosagedetails = localStorage.getItem("Dosagelist");
   }
}

function readURL(input) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {
         $('#imgProduct').attr('src', e.target.result);
      }

      return reader.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readURLforContent(input) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {
         $('#imgContentProductimage').attr('src', e.target.result);
      }

      return reader.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readContainerURL(input, imageID) {
   if (input.files && input.files[0]) {

      var readernew = new FileReader();

      readernew.onload = function(e) {

         $('#ContainerImageID_' + imageID).attr('src', e.target.result);
      }

      return readernew.readAsDataURL(input.files[0]);
      //reader.readAsDataURL(input.files[0]);
   }
}

function readContentURL(input, contentimageID) {

   if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function(e) {
         $('#ContentImageID_' + contentimageID).attr('src', e.target.result);
      }

      return reader.readAsDataURL(input.files[0]);
   }
}

function check_file() {

   str = document.getElementById('file').value.toUpperCase();
   suffix1 = ".JPG";
   suffix2 = ".JPEG";
   suffix3 = ".BMP";
   suffix4 = ".PNG";
   suffix5 = ".TIF";

   if (!(str.indexOf(suffix1, str.length - suffix1.length) !== -1 ||
      str.indexOf(suffix2, str.length - suffix2.length) !== -1 || str.indexOf(suffix3, str.length - suffix3.length) !== -1 ||
      str.indexOf(suffix4, str.length - suffix5.length) !== -1 || str.indexOf(suffix5, str.length - suffix5.length) !== -1)) {
      //  alert('File type not allowed,\nAllowed file: *.jpg,*.jpeg,*.bmp,*.png,*.tif');
      //alert('sorry you can upload only *.jpg,*.jpeg,*.bmp,*.png,*.tif file');
      $("#alertmessageid").html('sorry you can upload only *.jpg,*.jpeg,*.bmp,*.png,*.tif file');
      $(".alert-popup-body-message").show();

      //            $('#file').attr('value', '');
      var control = $("#file");
      control.replaceWith(control = control.clone(true));
   }
}

function validateImage() {
   var isValid = true;
   $(".Imageuploadclass").each(function(i, obj) {

      if ($('#' + obj.id).val() == '') {
         isValid = false;
         $('#' + obj.id).css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });

      } else {
         $('#' + obj.id).css({
            "border": "",
            "background": ""
         });
      }

   });

   if (isValid == false) {
      return false;
   } else
   ('Thank you for submitting');
}

function validateImageonUpdate() {
   var isValid = true;

   if ($('#httppostedContentfile').val() == '') {

      isValid = false;
      $('#' + obj.id).css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });

   } else {
      $('#httppostedContentfile').css({
         "border": "",
         "background": ""
      });
   }

   if (isValid == false) {
      return false;
   }
}

function clientValidation() {

   var isValid = true;
   $(".formAddValidation").each(function(i, obj) {

      if ($('#' + obj.id).val() == '') {
         isValid = false;
         $('#' + obj.id).css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
      } else {
         $('#' + obj.id).css({
            "border": "",
            "background": ""
         });
      }
   });

   if (isValid == false) {
      return false;
   } else
   ('Thank you for submitting');

   var SelectedddlValue = $("#Name").val();
   if (SelectedddlValue > 0 || SelectedddlValue == '') {
      if ($("#Name").val() == '' || $("#Name").val() == 0) {
         isValid = false;
         $("#Name").css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $("#Name").css({
            "border": "",
            "background": ""
         });
         return true;
      }
   }
}

function EditContainerImage(ProductImageDetailID, ProductID, NDC, Description, Image, ContainerType, ContentImage) {
   //alert(ProductID + "," + NDC + "," + Description + "," + Image + "," + ContainerType);

   $("#PackagingOrContainerType").val(ContainerType);
   $("#ProductID").val(ProductID);
   $("#imageNDCID").val(NDC);
   $("#DescriptionforImageTab").val(Description);
   $("#HiddenProductImageDetailID").val(ProductImageDetailID);
   if (ContainerType == "Original") {
      $("#imgProduct").attr("src", "../images/ProductImage/Original/" + Image);

   } else {
      $("#imgProduct").attr("src", "../images/ProductImage/Repackager/" + Image);
   }
   $("#imgContentProductimage").attr("src", "../images/ProductImage/Content/" + ContentImage);
}

function NumericOnly(obj) {
   if (obj.value.length > 0) {

      obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
   }
}

function NumericWithDashOnly(obj) {
   if (obj.value.length > 0) {

      obj.value = obj.value.replace(/[^[0-9]+(-[0-9]+)+$]/g, ''); // This would validate the inputs for allowing only numeric chars
   }
}

function NumericWithDecimalOnly(obj) {
   if (obj.value.length > 0) {
      obj.value = obj.value.replace(/[^0-9\.]/g, '');
      //obj.value = obj.value.replace(/[((\d+)(\.\d{2}))$]/g, '');
   }
}

function AddImageOnclick() {

   if ($('form').validationEngine('validate') == false) {
      return false;
   }
   //return result;
   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file1 == undefined) {
      if (file1 == undefined) {
         $('.browse-btn-container').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      }

   } else {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("ProfileCode", $("#hiddenprofilecodeforsaveimage").val());
   $.ajax({
      type: "POST",
      url: '../Product/AddProductImageUsingAjax',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         //$("#successerrormsg").text(response);
         $("#NDCUPCWithDashesforImageTab").val('')
         $("#DescriptionforImageTab").removeAttr('disabled', 'disabled');
         $("#DescriptionforImageTab").val('')
         $("#PackagingOrContainerType").val('')
         $("#imgProduct").attr('src', '../images/Defaultimages.jpg');
         $("#imgContentProductimage").attr('src', '../images/Defaultimages.jpg');

         $("#alertmessageid").html(response);
         $(".alert-popup-body-message").show();

      },
      error: function(error) {

      }
   });
}

function AddNewProductPhoto(e) {

   if (!$("#frmProductphotoedit").validationEngine('validate')) {
      e.preventDefault();
      return false;
   }
   var _this = this;

   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file1 == undefined) {
      $('.browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;

   } else {
      $('.browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }
   if ($("#labelNDCUPCWithDashesforImageTab").text() == '' || $("#labelPackagingOrContainerType").val() == '') {
      if ($("#labelNDCUPCWithDashesforImageTab").text() == '') {
         $('#labelNDCUPCWithDashesforImageTab').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#labelNDCUPCWithDashesforImageTab').css({
            "border": "",
            "background": ""
         });
      }
      if ($("#labelPackagingOrContainerType").val() == '') {
         $('#labelPackagingOrContainerType').css({
            "border": "1px solid red",
            "background": "#FFCECE"
         });
         return false;
      } else {
         $('#labelPackagingOrContainerType').css({
            "border": "",
            "background": ""
         });
      }
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDCUPCWithDashes", $("#labelNDCUPCWithDashesforImageTab").text());
   formData.append("Description", $("#labelDescriptionforImageTab").text());
   formData.append("ContainerType", $("#labelPackagingOrContainerType").val());
   formData.append("ProductID", $("#labelHiddenProductID").val());
   formData.append("ProfileCode", $("#labelHiddenProfileCode").val());
   $.ajax({
      type: "POST",
      url: '../Product/AddNewProductPhotoUsingAjax',
      data: formData,
      dataType: 'json',
      contentType: false,
      processData: false,
      success: function(response) {

         $("#alertmessageid").html(response);
         $(".alert-popup-body-message").show();
         $("#btnok").bind("click", function() {

            location.reload(true);
            $("#btnok").unbind("click");
         });


      },
      error: function(error) {

      }
   });
}

function UpdateImageOnclick() {

   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file == undefined || file1 == undefined) {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDC", $("#imageNDCID").val());
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", $("#HiddenProductImageDetailID").val());
   formData.append("ProductID", $("#ProductID").val());

   $.ajax({
      type: "POST",
      url: '../Product/EditProductImageUsingAjax',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         location.reload();

         $("#successerrormsg").html("Update Successfully");
      },
      error: function(error) {
         $("#successerrormsg").html("Update fails");
      }
   });
}

function UpdateIndividualImageclick(ProductImageDetailID, imageID) {

   var formData = new FormData();

   var file1 = document.getElementById("productcontainerimgfile_" + imageID).files[0];
   var file = document.getElementById("productcontentimgfile_" + imageID).files[0];

   formData.append("Contentfile", file);
   formData.append("productcontainerimgfile", file1);
   formData.append("ContainerType", $("#labelPackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", ProductImageDetailID);
   formData.append("ProductID", $("#labelHiddenProductID").val());
   formData.append("Version", $("#Version").val());

   $.ajax({
      type: "POST",
      url: '../Product/UpdateIndividualContentImage',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(data) {
         if (data.VersionChanged) {
            $("#alertmessageid").html(Product.VersionChange);

         } else if (data.Result == 'Deleted') {
            $("#alertmessageid").html(Product.Delete);

         } else {
            //$('#Version').val(data.UpdateVersion);
            $('#Version').val(data.UpdateVersion);
            $("#alertmessageid").html(data.Result.toString());
            //$("#Version").val();
         }
         $(".alert-popup-body-message").show();

      },
      error: function(error) {
      }
   });
}

var ProductImgDetailID = 0;
var checkedoriginalValue = '';
var checkedcontentValue = '';
var contentimagefilename = '';
var containerimagefilename = '';
///function used when User go for Delete option, Then need to show confirmation message
function DeleteProductPhoto(ProductImageDetailID, imageID) {

   var contentfilename = $("#ContentImageID_" + imageID).attr('src');
   var containerfilename = $("#ContainerImageID_" + imageID).attr('src');
   var fileNameIndex = contentfilename.lastIndexOf("/") + 1;
   contentimagefilename = contentfilename.substr(fileNameIndex);

   var containerfilenameIndex = containerfilename.lastIndexOf("/") + 1;
   containerimagefilename = containerfilename.substr(containerfilenameIndex);
   //console.log(filename);
   //console.log(containerimagefilename);

   checkedoriginalValue = $('#DeleteOriginalRepackager_' + imageID + ":checked").val();
   checkedcontentValue = $('#DeleteContent_' + imageID + ":checked").val();
   if (checkedoriginalValue != undefined) {
      if ((checkedoriginalValue.length > 0) && (checkedcontentValue == undefined)) {

         $("#alertmessageid").html("You can't delete only container image");
         $(".alert-popup-body-message").show();
         return false;
      }
   }
   if (checkedoriginalValue != undefined || checkedcontentValue != undefined) {
      ProductImgDetailID = ProductImageDetailID;
      $("#alertdeletemessageid").html("Do you wish to delete this product image ?");
      $(".alert-popup-body-message-confirm").show();
   } else {


      $("#alertmessageid").html("Please select checkbox");
      $(".alert-popup-body-message").show();

      return false;
   }
}

function AddProductSuccess(json) {
   console.log(json);

}

function UpdateImageOnclick() {
   var formData = new FormData();
   var file = document.getElementById("httppostedContentfile").files[0];
   var file1 = document.getElementById("productimgfile").files[0];
   if (file == undefined || file1 == undefined) {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "1px solid red",
         "background": "#FFCECE"
      });
      return false;
   } else {
      $('.browse-btn-content, .browse-btn-container').css({
         "border": "",
         "background": ""
      });
   }

   formData.append("Contentfile", file);
   formData.append("productimgfile", file1);
   formData.append("NDC", $("#imageNDCID").val());
   formData.append("NDCUPCWithDashes", $("#NDCUPCWithDashesforImageTab").val());
   formData.append("Description", $("#Description").val());
   formData.append("ContainerType", $("#PackagingOrContainerType").val());
   formData.append("HiddenProductImageDetailID", $("#HiddenProductImageDetailID").val());
   formData.append("ProductID", $("#ProductID").val());

   $.ajax({
      type: "POST",
      url: '../Product/EditProductImageUsingAjax',
      data: formData,
      dataType: 'json',
      async: false,
      contentType: false,
      processData: false,
      success: function(response) {
         location.reload();

         $("#successerrormsg").html("Update Successfully");

      },
      error: function(error) {
         $("#successerrormsg").html("Update fails");

      }
   });
}

function NavigateToChangeDetails() {

   var NDCUPCWithDashes = $("#NDCUPCWithDashes").val();
   var ProductID = $("#ProductID").val();
   var Description = $("#Description").val();
   $.ajax({
      type: "Post",
      url: '../Product/ChangeTrackerDetails',
      data: { 'prodID': ProductID, 'NDCUPCWithDashes': NDCUPCWithDashes, 'Description': Description },
      dataType: 'json',

      success: function(response) {
         location.href = ""

      },
      error: function(error) {

      }
   });

}

function validateNDCUPCWithDashes(field, rules, i, options) {
   if (field.val() != "") {
      var res = true;
      $(checkNDCUPCWithDashesRegex).each(function(k, v) {
         var pattern = new RegExp(v);
         if (pattern.test(field.val()) == false) {
            res = false;
         } else {
            res = true;
            return false;
         }
      })

      if (res == false) {
         return "* Please enter NDC With Dashes in correct format!!!";
      }
   }
}

function checkNDCUPCWithDashesForProductPhoto(field, rules, i, options) {

   var result = SearchTypeProductPhoto();
   var msg = undefined;
   // setTimeout(function () {
   var url = '../Product/CheckExistanceNDCUPCWithDashes/';

   if (field.val() != "" && (field.val().indexOf("-") != -1)) {
      var res = true;
      $(checkNDCUPCWithDashesRegex).each(function(k, v) {
         var pattern = new RegExp(v);
         if (pattern.test(field.val()) == false) {
            res = false;
         } else {
            res = true;
            return false;
         }
      })
   }
   if (res == false) {
      return "* Please enter NDC With Dashes in correct format!!!";
   }

   var result = SendAjax(url, {
      NDCUPCWithDashes: field.val(),
      PrdID: parseInt($('#ProductID').val()),
      inputType: (result)
   });
   if (result.data == "OK") {
      $("#hiddenprofilecodeforsaveimage").val(result.data3);
      $("#DescriptionforImageTab").val(result.data2);

      $("#DescriptionforImageTab").attr("disabled", "disabled");
      $('#successerrormsg').html("");


      isUserValidate = true;
   } else {
      //$('#successerrormsg').html("Please enter correct NDC With Dashes");
      isUserValidate = false;
   }

   return msg;
}

function checkNDCUPCWithDashes(field, rules, i, options) {

   var msg = undefined;
   var url = '../Product/CheckExistanceNDCUPCWithDashes/';

   if (field.val() != "") {
      var res = true;
      $(checkNDCUPCWithDashesRegex).each(function(k, v) {
         var pattern = new RegExp(v);
         if (pattern.test(field.val()) == false) {
            res = false;
         } else {
            res = true;
            return false;
         }
      })

      if (res == false) {
         return "* Please enter NDC With Dashes in correct format!!!";
      }

      var result = SendAjax(url, {
         NDCUPCWithDashes: field.val(),
         PrdID: parseInt($('#ProductID').val())
      });
      if (result.data == "OK") {
         msg = "* NDC With Dashes already exist in database";
         isUserValidate = false;
      } else {
         isUserValidate = true;
      }
   }

   return msg;
}

function checkNDCExist(field, rules, i, options) {

   var msg = undefined;

   if (field.val() != "" && field.val() == "0") {
      msg = "* Value can  not be zero ";
      isUserValidate = false;
      return msg;
   }

   var url = '../Product/CheckExistanceNDCUPCWithDashes/';
   if (field.val() != "") {
      var result = SendAjax(url, {
         NDCUPCWithDashes: field.val(),
         PrdID: parseInt($('#ProductID').val())
      });
      if (result.data == "OK") {
         msg = "* NDC already exist in database";
         isUserValidate = false;
      } else {
         isUserValidate = true;
      }
   }
   return msg;
}

function OpenDivestPartial() {
   if ($("input[name=DivestedByLot]:checked").val() == 'True') {
      $('#ahidelotlink').hide();
      $('#dvhidebacksearch').hide();
      LoadDivestedLotPopupINEditPage($('#ProductID').val(), $('#ProfileCode').val());
   } else {
      $("#alertmessageid").html(Product.SelectDivest);
      $(".alert-popup-body-message").show();
      return false;
   }
}

function loadEditPartialView(WNID) {
   $("#LoadWholesalerPartialView").load('/Product/GetWholesalerDetailsByWholesalerID?WNID=' + WNID, function() {
      $('#frmWholesalernumber').validationEngine();
   });
}

//function loadPartialView()
//{
//    $("#LoadWholesalerPartialView").load('/Product/LoadPartialView', function () {
//        $('#frmWholesalernumber').validationEngine();

//    });
    
//}
//function AddWNumberspecificToNDC(otype)
//{
//    if (!$("#frmWholesalernumber").validationEngine('validate')) {
//        return false;
//    }
    
//    var objAddWholesalerNumber =
//     {
//         ProfileCode: $('#WholesalerProfileCode').val(),
//         NDCUPCWithDashes: $('#AddWNNDCUPCWithDashes').val(),
//         Description: $('#WNDescription').val(),
//         Number: $('#Number').val(),
//         OperationType: otype,
//         WholesalerNumberID: $("#WholesalerNumberID").val(),
//         WholesalerVersion: $("#WholesalerVersion").val(),
//         NDCUPCInputName: $('#inputfieldName').text(),
//         NDCUPCInputvalue:$('#NDCUPCInputvalue').val()
//     };
 
//    var resultstatus = SendAjaxGet('../Product/CheckExistanceofWholesalerNumber', objAddWholesalerNumber)
   
//   if (resultstatus.data == 0) {
//       var result = SendAjax('../Product/InsertWholesalerNumber', objAddWholesalerNumber)
//       if (result.data == 0 && otype == 'Save')
//       {
//           $("#alertmessageid").html(Product.SaveSuccess);
//           $(".alert-popup-body-message").show();
//           $('#AddWNNDCUPCWithDashes').val('');
//           $('#Number').val('');
//           $('#WholesalerProfileCode').val('');
//           $('#inputfieldName').text('');
//       }
//       else if (result.data == 1 && otype == 'Save')
//       {
//           $("#alertmessageid").html(Product.SaveFails);
//           $(".alert-popup-body-message").show();
//       }
//       else if (otype == 'Update') {

//           if (result.VersionChanged) {
//               $(document).data('returnResult', result.Updatedresult);
//               $("#alertmessageid").html(Product.VersionChange);
//               $('#WholesalerVersion').val(result.Updatedresult.objResponse.Version)
//               $("#btnok").bind("click", function () {

//                   var NewResultobject = $(document).data('returnResult');
//                   var NewResult = NewResultobject.objResponse;
//                   $("#myform")[0].reset();


//                   lockingFeature(NewResult);

//                   if ($("#WholesalerProfileCode").val() != NewResult.ProfileCode) {
//                       $("#WholesalerProfileCode").val(NewResult.ProfileCode).addClass("highlitebordercss");
//                   }
         

//                   $("#btnok").unbind("click");
//                   closePopup();
//               })
//           }
//           else if (result.Result == 'Deleted') {
//               $("#alertmessageid").html(Product.Delete);

//           }
//           else {
//               $('#WholesalerVersion').val(result.UpdateVersion);
//               $("#alertmessageid").html(result.Result.toString());
//               $('.highlitebordercss').removeClass("highlitebordercss");
//               //searchWholesalerNDC();
//           }

//         $(".alert-popup-body-message").show();
//       }
       
//   }
//   else {
//       $("#alertmessageid").html(Product.WholesalerNumberExist);
//       $(".alert-popup-body-message").show();
//   }
//   AddWNumberspecificToNDC
//}

function SearchTypeProductPhoto() {
   var result = SearchNDCUPCType('#NDCUPCWithDashesforImageTab', '#inputfieldNamesearchImgforproductphoto');
   $('#hdnsearchTypeValue').val(result);
   return result;
}

function SearchType() {

   if ($('#AddWNNDCUPCWithDashes').val().length > 0) {
      var result = SearchNDCUPCType('#AddWNNDCUPCWithDashes', '#inputfieldName');

      $('#NDCUPCInputvalue').val(result);
   } else {
      $('#NDCUPCInputvalue').val('');
      $('#inputfieldName').text('');


   }
}

function OnSuccess(data) {
   alert("Success" + data);
}

function OnFailure(xhr, status) {
   alert('Error: ' + xhr.statusText);
}

function trimHashFromPath(url) {
   return url.split("&message")[0];
}

$(document).ready(function() {
   var edithref = $(location).attr('href');

   BindEditUpdateMode();
   BindValidations();
   $('form').validationEngine();

   $('#btnPrdWholesalerreset').click(function() {
      $('#WNNDCUPCWithDashes').val('');
      $('#WNDescription').val('');
      //$('#WNProfileCode').val(0);
      $("#WNProfileCode").val($("#WNProfileCode option:first").val());
      $('#WNNumber').val('');
   });

   $("#btnPrdInsert").click(function() {
      var result = true;
      if ($('form').validationEngine('validate') == false) {
         result = false;
      }
      return result;
   });

   $("#btnPrdReset").click(function() {
      $(this).resetValidation();
      $('form').validationEngine('hide');
   });

   $("#btdeletenCancel,.alert-close").click(function() {
      $(".alert-popup-body-message-confirm").hide();
      $(".alert-popup").animate({ "top": "40%" });
   });

   $("#btdeletenok").click(function() {

      $(".alert-popup-body-message-confirm").hide();
      $(".alert-popup").animate({ "top": "40%" });
      $.ajax({
         type: "POST",
         url: '../Product/DeleteProductPhoto',
         data: { 'ProductImageDetailID': ProductImgDetailID, 'OriginalRpk': checkedoriginalValue, 'Content': checkedcontentValue, 'OriginalRpkImag': containerimagefilename, 'ContentImg': contentimagefilename },

         success: function(response) {

            $("#alertmessageid").html(response);
            $(".alert-popup-body-message").show();

            location.reload(true);
         },
         error: function(error) {

         }
      });
   });

   $(".alert-popup-body").hide();
   if ($('#successerrormsgforproductdescription').html() == "Saved Successfully") {

      $("#alertmessageid").html(Product.SaveSuccess);
      $(".alert-popup-body-message").show();
      window.location = window.location.href.replace("&message=success", '');

   } else {
      $(".alert-popup-body-message").hide();
   }
   $(".close-btn").click(function() {
      $(".model-popup").hide();
   });

   $('#btnHideDivestedPopup').click(function() {
      $('#dvdivestedLotNumbers').hide();
      $('#dvdivestpartial').hide();
      $('#btnHideDivestedPopup').hide();
      $('#dveditsecton').show();
      $('#ahidelotlink').show();
      $('#dvhidebacksearch').show();
   });

   $('#save').click(function() {
      var prdID = $("#divestedprdid").val();
      var updatestatus = $('#updatestatus').val();
      var values = new Array();
      $('#jqxOriginalgrid').find('input[type="checkbox"]:checked').each(function() {
         //alert("item clicked, id = " + $(this).attr("data-id"));
         values.push($(this).attr("data-id"));
      });
      var diveLotID = values.join(", ")
      var diverseProfileCode = $('#diverseLotNumbersDetails_DivestedProfileCode').val();
      if (diverseProfileCode == '' || values.length == 0) {
         $("#alertmessageid").html("Please select lot number and manufacture name for change ownership");
         $(".alert-popup-body-message").show();
         //alert("Please select lot number and manufacture name for change ownership");
         return false;
      } else {
         $.ajax({
            url: '../LotNumber/SaveDiverseLotNumber',
            data: { ProfileCode: diverseProfileCode, LotIDS: diveLotID, ProductID: prdID, updatestatus: updatestatus },
            success: function(data) {
               if (data == "OK") {
                  $.ajax({
                     url: '../LotNumber/DiverseLotNumberList',
                     data: { productID: $('#ProductID').val() },
                     success: function(data) {
                        GenerateOriginalGrid(data);
                        GenerateOwnOwnershipGrid(data);
                     },
                     error: function(response) {
                        alert(response);
                        if (response.status != 0) {
                           alert(response.status + " " + response.statusText);
                        }
                     }

                  });
                  $(".model-popup").show();
                  $(".model-box").animate({ "top": "20%" });
               }

            },
            error: function(response) {
               alert(response);
               if (response.status != 0) {
                  alert(response.status + " " + response.statusText);
               }
            }
         });
      }
      return false;
   });

   //$('input[type=radio][name=DivestedByLot]').change(function () {

   //    if ($("input[name=DivestedByLot]:checked").val() == 'True') {

   //        $(".model-popup").show();
   //        $(".model-box").animate({ "top": "20%" });
   //        $.ajax({
   //            type: "Get",
   //            async: "false",
   //            contentType: 'application/json; charset=utf-8',
   //            url: '../LotNumber/DiverseLotNumberList',
   //            data: { productID: $('#ProductID').val() },
   //            success: function (data) {
   //                if (data.DiverseManufactureLotNumbers.LotNumbersList != null) {
   //                    GenerateOwnOwnershipGrid(data);
   //                }
   //                if (data.OriginalManufactureLotNumbers.LotNumbersList != null) {

   //                    GenerateOriginalGrid(data);
   //                }
   //            },
   //            error: function (response) {
   //                alert(response);
   //                if (response.status != 0) {
   //                    alert(response.status + " " + response.statusText);
   //                }
   //            }

   //        });


   //    }
   //    if ($("input[name=DivestedByLot]:checked").val() == 'False') {
   //        $(".model-popup").hide();
   //        $(".model-box").animate({ "top": "20%" });

   //    }

   //});


   if (edithref.indexOf('mode=Add') != -1) {

      setTimeout(function() {
         $('#tab-container').find("input,select,textarea").prop("disabled", true);
         $("#ProfileCode").prop("disabled", false);

      }, 200);
      $("#ProfileCode").change(function() {
         $("#NDCUPCWithDashes").prop("disabled", false);
         $("#NDC").prop("disabled", false);
         $("#UPC").prop("disabled", false);
         $("#NDCUPCWithDashes").focus();
      });


      $("#NDCUPCWithDashes").blur(function(e) {
         if ($(this).val() != "") {
            var newVal = $("#NDCUPCWithDashes").val().split('-').join("");
            $("#NDC").val(newVal);
            $("#NDC").prop("readonly", true);
            $("#UPC").focus();
         } else {
            $("#NDC").val("");
         }
      });
      $("#NDC").blur(function(e) {
         //if (e.relatedTarget.id != "UPC" || e.relatedTarget.id != "NDCUPCWithDashes") {
         if ($("#NDC").val() == "" && $("#UPC").val() == "") {
            //$("#NDC").focus();
         } else {
            $("input,select,textarea").prop("disabled", false);
         }
         //}
      });

      $("#UPC").blur(function(e) {
         //if (e.relatedTarget.id != "NDC" || e.relatedTarget.id != "NDCUPCWithDashes") {
         if ($("#NDC").val() == "" && $("#UPC").val() == "") {
            $("#NDCUPCWithDashes").focus();
         } else {
            $("input,select,textarea").prop("disabled", false);
         }
         //}
      });
   }

   $("#CaseSize2").keypress(function() {
      if ($("#CaseSize1").val() == 0 || $("#CaseSize1").val() == '') {

         $("#alertmessageid").html("Fill CaseSize1 field");
         $(".alert-popup-body-message").show();
         //alert("Fill CaseSize1 field");
         //$('#CaseSize1').css({
         //    "border": "1px solid red",
         //    "background": "#FFCECE"
         //});
         return false;
      }
   });

   $("#CaseSize3").keypress(function() {

      if ($("#CaseSize1").val() == 0 || $("#CaseSize1").val() == '') {
         $("#alertmessageid").html("Fill CaseSize1 field");
         $(".alert-popup-body-message").show();
         //alert("Fill CaseSize1 field");
         //$('#CaseSize1').css({
         //    "border": "1px solid red",
         //    "background": "#FFCECE"
         //});
         return false;
      }

      if ($("#CaseSize2").val() == 0 || $("#CaseSize2").val() == '') {
         $("#alertmessageid").html("Fill CaseSize2 field");
         $(".alert-popup-body-message").show();
         //alert("Fill CaseSize2 field");
         //$('#CaseSize2').css({
         //    "border": "1px solid red",
         //    "background": "#FFCECE"
         //});
         return false;
      }

   });

   $("#CaseSize2").change(function() {
      if ($("#CaseSize1").val() > 0) {
         if ($("#CaseSize2").val() == $("#CaseSize1").val()) {
            $("#alertmessageid").html("Value should be greater or less than from case size1");
            $(".alert-popup-body-message").show();
            //alert("value should be greater or less than from case size1");
            $("#CaseSize2").val('');
            //$('#CaseSize2,#CaseSize1').css({
            //    "border": "1px solid red",
            //    "background": "#FFCECE"
            //});
            return false;

         }
      } else {
         $("#alertmessageid").html("First Enter CaseSize 1 field");
         $(".alert-popup-body-message").show();
         //alert("First Enter CaseSize 1 field");
         //$('#CaseSize1').css({
         //    "border": "1px solid red",
         //    "background": "#FFCECE"
         //});
      }
   });

   $("#CaseSize3").change(function() {
      if ($("#CaseSize2").val() > 0) {
         if ($("#CaseSize1").val() == $("#CaseSize2").val() || $("#CaseSize2").val() == $("#CaseSize3").val() || $("#CaseSize1").val() == $("#CaseSize3").val()) {
            $("#alertmessageid").html("Value should be greater or less than from case size2");
            $(".alert-popup-body-message").show();
            //alert("value should be greater or less than from case size2");
            $("#CaseSize3").val('');
            //$('#CaseSize3,#CaseSize2').css({
            //    "border": "1px solid red",
            //    "background": "#FFCECE"
            //});
            return false;
         } else {
            $('#CaseSize3,#CaseSize2,#CaseSize1').css({
               "border": "",
               "background": ""
            });
         }
      } else {
         //alert("First Enter CaseSize 2 field");
         $("#alertmessageid").html("First Enter CaseSize 2 field");
         $(".alert-popup-body-message").show();
         //$('#CaseSize2').css({
         //    "border": "1px solid red",
         //    "background": "#FFCECE"
         //});
      }
   });

   $("#createlotNumberWithDiverstedLink").click(function() {

      $("#divestedShowList").hide();
      $("#createlotNumberWithDivested").show();

   });

   $("#DiverseManufacturerProfileCode").change(function() {
      var profileId = $("#DiverseManufacturerProfileCode").val();
      $(".model-popup ").show();
      $(".model-box").animate({ "top": "20%" });

   });

   $(".imglarge").mouseover(function() {
      $(this).siblings("#pop-up").show();
      //$("#pop-up").show();
   });

   $(".imglarge").mouseout(function() {
      $(this).siblings("#pop-up").hide();
      //$("#pop-up").hide();
   });

   $(".content-img-large").mouseover(function() {
      $(this).siblings("#content-pop-up").show();
      //$("#pop-up").show();
   });
   $(".content-img-large").mouseout(function() {
      $(this).siblings("#content-pop-up").hide();
      //$("#pop-up").hide();
   });

   $('#ProductEndDatedtp').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   $('#LotNumbersData_ExpirationDate').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   $("#LotNumbersData_ExpirationDate").keypress(function() {

      $('#LotNumbersData_ExpirationDate').datepicker({
         //changeMonth: true,
         //changeYear: true,
         //dateFormat: 'MM dd, yy',
         //minDate: 1,
        
      });
   });

   $('.hidedeletebtn').click(function() { $('.del_btn').hide(); });
   $('.showdeletebtn').click(function() { $('.del_btn').show(); });

   $('#tab-container').easytabs();

   $('#defaultTable').dragtable();

   $('#ProductStartDatedtp').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   var packageresult = $("#PackageSize").val();
   if (packageresult == 0) {
      $("#PackageSize").val('');
      $("#UnitsPerPackage").val('');
      //$("#ProductID").val('');
   }

   var qrStr = window.location.search;


   if (qrStr != undefined && qrStr != "") {
      qrStr = qrStr.split("?")[1].split("=")[1];
      if (qrStr == 'Add' || qrStr == 'Add&prodId') {

         $("#productdiv").find("input,button,select,textarea").removeAttr('disabled');
         $("#physiciandiv").find("input,button,select,textarea").removeAttr("disabled", "disabled");
         $("#additionaldiv").find("input,button,textarea,a").removeAttr("disabled", "disabled");
      } else {
         $("#productdiv").find("input,button,select,textarea,a").attr("disabled", "disabled");
         $("#physiciandiv").find("input,button,select,textarea,a").attr("disabled", "disabled");
         $("#additionaldiv").find("input,button,select,textarea,a").attr("disabled", "disabled");
      }
   }

   $('#productdesupdateId').click(function(e) {

      if (!$("#frmProductDescription").validationEngine('validate')) {
         e.preventDefault();
         return false;
      }
      var _this = this;

      var isValid = true;

      var requestData =
      {
         ProductID: $('#ProductID').val(),
         ProfileCode: $('#ProfileCode').val(),
         Strength: $('#Strength').val(),
         Description: $('#Description').val(),
         NDC: $('#NDC').val(),
         PackageSize: $('#PackageSize').val(),
         NDCUPCWithDashes: $('#NDCUPCWithDashes').val(),
         UnitsPerPackage: $('#UnitsPerPackage').val(),
         UnitOfMeasure: $('#measureTypeid').val(),
         UPC: $('#UPC').val(),
         RXorOTC: $('#RXorOTC').val(), //$("input[name=RXorOTC]:checked").val(),
         DosageCode: $('#DosageCode').val(),
         ControlNumber: $('#controlTypeid').val(),
         ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
         Version: $('#Version').val(),
         SectionNumber: 1,
         PillType: $('#PillType').val()
      };
      var url = '../Product/UpdateProductDescriptionSection/';
      $.ajax({
         type: "POST",
         url: url,

         //  data: "{'AJAXProduct':'" + $('#ProductID').val() + "','AJAXProfile':'" + $('#ProfileCode').val() + "','Strength':'" + $('#Strength').val() + "','AjaxDescription':'" + $('#Description').val() + "','AjaxNDC':'" + $('#NDC').val() + "','PackageSize':'" + $('#PackageSize').val() + "','AjaxNDCUPCWithDashes':'" + $('#NDCUPCWithDashes').val() + "','UnitsPerPackage':'" + $('#UnitsPerPackage').val() + "','UnitOfMeasure':'" + $('#measureTypeid').val() + "','UPC':'" + $('#UPC').val() + "','DosageCode':'" + $('#DosageCode').val() + "','ControlNumber':'" + $('#controlTypeid').val() + "','ARCOSReportable':'" + $("input[name=ARCOSReportable]:checked").val() + "'}",
         data: { objPrddescription: requestData },
         //contentType: "application/json; charset=utf-8",
         dataType: "json"

      }).done(function(data) {

         if (data.VersionChanged) {
            $(document).data('returnResult', data.Updatedresult);
            $("#alertmessageid").html(Product.VersionChange);
            $('#Version').val(data.Updatedresult.objResponse.Version);
            $("#btnok").bind("click", function() {

               var NewResultobject = $(document).data('returnResult');
               var NewResult = NewResultobject.objResponse;
               //$('#frmProductDescription').reset();
               $("#frmProductDescription")[0].reset();
               $("#frmAdditionalInformation")[0].reset();
               $("#frmAdditionalProductNumbers")[0].reset();


               lockingFeature(NewResult);

               if ($("#measureTypeid").val() != NewResult.UnitOfMeasure) {
                  $("#measureTypeid").val(NewResult.UnitOfMeasure).addClass("highlitebordercss");
               }
               if ($("#controlTypeid").val() != NewResult.ControlNumber) {
                  $("#controlTypeid").val(NewResult.ControlNumber).addClass("highlitebordercss");
               }

               $("#btnok").unbind("click");
               closePopup();
            });
         } else if (data.Result == 'Deleted') {
            $("#alertmessageid").html(Product.Delete);
         } else {
            $('#Version').val(data.UpdateVersion);
            $("#alertmessageid").html(data.Result.toString());
            $('.highlitebordercss').removeClass("highlitebordercss");
            $("#btnok").bind("click", function() {
               $("#btnok").unbind("click");
               closePopup();
            })
         }

         $(".alert-popup-body-message").show();


         HideUpdate(_this)

      }).fail(function(response) {
         if (response.status != 0) {
            // alert(response.status + " " + response.statusText);
         }
      });
   });

   $('#physiciandesupdateid').click(function(e) {
      if (!$("#frmAdditionalInformation").validationEngine('validate')) {
         e.preventDefault();
         return false;
      }
      var _this = this;


      var requestData =
      {
         ProductID: $('#ProductID').val(),
         PhysicianSample: $("input[name=PhysicianSample]:checked").val(),
         PhysicianSampleProductNumber: $('#PhysicianSampleProductNumber').val(),
         TamperResistantSealExists: $("input[name=TamperResistantSealExists]:checked").val(),
         DEAWatchList: $("input[name=DEAWatchList]:checked").val(),
         Withdrawn: $("input[name=Withdrawn]:checked").val(),
         CaseSize1: $('#CaseSize1').val(),
         CaseSize2: $('#CaseSize2').val(),
         CaseSize3: $('#CaseSize3').val(),
         IndividualCountWeight: $('#IndividualCountWeight').val(),
         ContainerWeight: $('#ContainerWeight').val(),
         FullContainerWeightWithContents: $('#FullContainerWeightWithContents').val(),
         UnitDose: $("input[name=UnitDose]:checked").val(),
         Version: $('#Version').val(),
         SectionNumber: 2
      };
      var url = '../Product/UpdateProductDescriptionSection/';
      $.ajax({
         type: "POST",
         url: url,
         data: requestData,
         dataType: "json"
      }).done(function(data) {
         if (data.VersionChanged) {
            //var returnResult = data.Updatedresult;
            //alert(JSON.stringify(data.Updatedresult))
            $(document).data('returnResult', data.Updatedresult);

            $("#alertmessageid").html(Product.VersionChange);
            $('#Version').val(data.Updatedresult.objResponse.Version);

            $("#btnok").bind("click", function() {

               var NewResultobject = $(document).data('returnResult');
               var NewResult = NewResultobject.objResponse;

               lockingFeature(NewResult);

               if ($("#measureTypeid").val() != NewResult.UnitOfMeasure) {
                  $("#measureTypeid").val(NewResult.UnitOfMeasure).css(highlitebordercss);
               }


               $("#btnok").unbind("click");
               closePopup();
            })

         } else if (data.Result == 'Deleted') {
            $("#alertmessageid").html(Product.Delete);

         } else {
            $('#Version').val(data.UpdateVersion);
            $("#alertmessageid").html(data.Result.toString());
            $('.highlitebordercss').removeClass("highlitebordercss");
         }
         $(".alert-popup-body-message").show();
         HideUpdate(_this);
      }).fail(function(response) {
         if (response.status != 0) {
         }
      });
   });

   $('#additionaldesupdatedid').click(function(e) {

      if (!$("#frmAdditionalProductNumbers").validationEngine('validate')) {
         e.preventDefault();
         return false;
      }
      var _this = this;

      var requestData = {
         ProductID: $('#ProductID').val(),
         MFGProductNumber: $('#MFGProductNumber').val(),
         RollupProductID: $('#RollupProductID').val(),
         ForceQuantityCountNotes: $('#ForceQuantityCountNotes').val(),
         EDINumber: $('#EDINumber').val(),
         WholesalerNumber: $('#WholesalerNumber').val(),
         Refrigerated: $("input[name=Refrigerated]:checked").val(),
         DivestedByLot: $("input[name=DivestedByLot]:checked").val(),
         SpecialHandlingInstructions: $('#SpecialHandlingInstructions').val(),
         ExcludeFromExternalInterface: $("input[name=ExcludeFromExternalInterface]:checked").val(),
         ExcludeFromDropDown: $("input[name=ExcludeFromDropDown]:checked").val(),
         AlwaysSortToQuarantine: $("input[name=AlwaysSortToQuarantine]:checked").val(),
         ForceQuantityCount: $("input[name=ForceQuantityCount]:checked").val(),
         Version: $('#Version').val(),
         SectionNumber: 3
      };

      // data: JSON.stringify(requestData),
      $("#additionaldesid").show();
      $("#additionaldesupdatedid").hide();
      $("#additionaldescancelid").hide();
      var url = '../Product/UpdateProductDescriptionSection/';
      $.ajax({
         type: "POST",
         url: url,
         data: requestData,
         dataType: "json"
      }).done(function(data) {
         if (data.VersionChanged) {
            //var returnResult = data.Updatedresult;
            //alert(JSON.stringify(data.Updatedresult))
            $(document).data('returnResult', data.Updatedresult);

            $("#alertmessageid").html(Product.VersionChange);
            $('#Version').val(data.Updatedresult.objResponse.Version);

            $("#btnok").bind("click", function() {

               var NewResultobject = $(document).data('returnResult');
               var NewResult = NewResultobject.objResponse;

               lockingFeature(NewResult);

               if ($("#measureTypeid").val() != NewResult.UnitOfMeasure) {
                  $("#measureTypeid").val(NewResult.UnitOfMeasure).css(highlitebordercss);
               }


               $("#btnok").unbind("click");
               closePopup();
            })

         } else if (data.Result == 'Deleted') {
            $("#alertmessageid").html(Product.Delete);

         } else {
            $('#Version').val(data.UpdateVersion);
            $("#alertmessageid").html(data.Result.toString());
            $('.highlitebordercss').removeClass("highlitebordercss");
         }
         $(".alert-popup-body-message").show();
         HideUpdate(_this);

      }).fail(function(response) {
         if (response.status != 0) {

         }
      });
   });

   $('.remove-message-test').click(function() {

      $('#successerrormsgforproductdescription').html("");
      $('#successerrormsg').html("");

   });

   $("#DosageCode").change(function() {

      if ($(this).val() == "TAB" || $(this).val() == "CAP") {
         $('#pilltypeid').show();
      } else {
         $('#pilltypeid').hide();
      }
   });

   $("#PackagingOrContainerTypeSearch").change(function() {

      if ($("#PackagingOrContainerTypeSearch").val() == "Original") {

         $(".image-display-Repackager-none").hide();
         $(".image-display-Original-none").show();

      }
      if ($("#PackagingOrContainerTypeSearch").val() == "Repackager") {

         $(".image-display-Repackager-none").show();
         $(".image-display-Original-none").hide();

      }
      if ($("#PackagingOrContainerTypeSearch").val() == "ALL") {
         $(".image-display-Repackager-none").show();
         $(".image-display-Original-none").show();

      }

   });

   $("#refreshId").click(function() {

      var result = $(document).data('returnResult');

      //var mydata = $("returnResult").data();
   });

   $("#btnProductdelete").click(function() {
      var productID = $("#ProductID").val();
      deleteselectedProduct(productID);
      $("#btdeletenok").unbind("click");
      $("#btdeletenok").click(function() {
         $(".alert-popup-body-message-confirm").hide();
         $(".alert-popup").animate({ "top": "40%" });
         $.ajax({
            url: '../Product/DeleteProduct',
            data: { prodId: userId },
            cache: false,
            success: function(data) {

               if (data.Result == 'Deleted' || data.Result == 'OK') {

                  $("#alertmessageid").text('Product Deleted');
                  $(".alert-popup-body-message").show();
                  $(".alert-close").hide();
                  $("#btnok").bind("click", function() {

                     window.location = '../Product/Index';
                     $("#btnok").unbind("click");
                  });


               }
               if (data.Result == 'Fails') {
                  if (data.Message == '' || data.Message == null || data.Message == undefined) {
                     $("#alertmessageid").text('Failed to delete Product');
                  } else {
                     $("#alertmessageid").text(data.Message);
                  }

                  $(".alert-popup-body-message").show();
                  $("#btnok").bind("click", function() {

                     $(".alert-popup-body-message").hide();
                     $("#btnok").unbind("click");
                  });

               }

            },
            error: function(response) {
               // alert(response);
               if (response.status != 0) {
                  //  alert(response.status + " " + response.statusText);
               }
            }
         });
         return false;
      });
      return false;
   });


   BindValuesInData("div.main-container");
   //$(".alert-popup-body-message").hide();
   closePopup();

   $(document).ajaxStart(function() {
      $("#wait").css("display", "block");
   });
   $(document).ajaxComplete(function() {
      $("#wait").css("display", "none");
   });
   $(".model-popup").children().click(function(e) {
      return true;
   });
   $(".model-popup1").children().click(function(e) {
      return true;
   });
   $('#LotNumbersData_ExpirationDate').datepicker({
      //changeMonth: true,
      //changeYear: true,
      //dateFormat: 'MM dd, yy',
      //minDate: 1,
    
   });

   //onEdit

   if (edithref.indexOf('mode=edit') != -1) {
      // CreateGridForServerSide();
      BindWholeselerGrid(parseInt($('#ProductID').val()));
      $("#dvwhiteborderforwholesaler").hide();
   }
   $('#dvdivestpartial').hide();

});
// documnet read end here

var userId = 0;
///function used when User go for Delete option, Then need to show confirmation message
function deleteselectedProduct(productID) {

   userId = productID;
   //if (userId > 0) {
   $("#alertdeletemessageid").text("Do you wish to delete this product ?");
   $(".alert-popup-body-message-confirm").show();
   //}
}

function resetFormData() {
   $("#myform").reset();
}
