﻿var action = 0;

$(function()
{
   var messageObject = [];
   var btnObjects = [];
   var titleobjects = [];
    //Add methods to run when load the binventory grid 
   //Execution is in putContainerGhost()
   window.onInventoryMethods = [];
   window.addInventoryMethod = function (func)
   {
      exclusivePush(onInventoryMethods, func);
   }

   //Add methods to run when clicking on grid and want to have specific behavior
   //Execution is in clicking grid
   window.onClickGrid_ControlMethods = [];
   window.addClickGrid_ControlMethod = function (func)
   {
      exclusivePush(onClickGrid_ControlMethods, func);
   }

   //Add methods to run when have specific behavior on unselect functionality(for example want to clear detail form)
   //Execution is in clicking grid
   window.onUnselectMethods = [];
   window.addUnselectMethod = function (func)
   {
      exclusivePush(onUnselectMethods, func);
   }

   //Add methods to run when have specific behavior on select functionality(for example want to go in another tab)
   //Execution is in clicking grid
   window.onselectMethods = [];
   window.addSelectMethod = function (func)
   {
      exclusivePush(onselectMethods, func);
   }

   window.onMissortMethods = [];
   window.addMissortMethod = function (func)
   {
      exclusivePush(onMissortMethods, func);
   }   

   //added to reconciling functionality when sorting to virtual gaylord using sidebar.putVirtual()
   var putContainerGhost = function(itemGuid, type,category)
   {
      sidebar.putVirtual(type, itemGuid,category,
        function(data)
        {
           if (data.success)
           {
              executeMethods("onInventoryMethods", data);
              $(".inputEx").val("");
           } else
           {
              $.getJSON("https://localhost:8008/Qualanex/http/Speak?textString=" + data.error, {
              }, function(data)
              {
              });
              action = 0;
           }
        });
   }

   // reconciling function when click on reconcile button all unsorted items sorting to ghost location( Virtual Gaylord)
   // reconciling requires secondary verification
   // when reconciling button text is close it means you are finish with reconciling or sorting and you can close the tote
   var onReconciling = function()
   {
      if (sidebar.containers.find(c=>c.attribute === 'I' && c.isOpen()) === undefined)
      {
         return false;
      }
      if ($(this).val() === "Close")
      {
         // closing the tote 
         var container = sidebar.containers.find(c=>c.attribute === 'I' && c.isOpen());
         var $div = $(".containerId.text-box[value=" + container.containerId + "]").closest("div.QoskSegment");
         $div.find("tr input.containerClose").click();
      }
      else
      {
         if ($(".searchGrid_WrhsSrtDetails-div-click").hasClass("Requested"))
         {
            $(this).setErrorMessage("You have Requested Gaylord.", "Error Message", "Continue");
            return false;
         }
         btnObjects = [{
            "id": "btnValidationSubmitCancelSort",
            "name": "Cancel",
            "function": "onclick='$(this).closeMessageBox();'",
            "class": "btnErrorMessage"
         },
           {
              "id": "btnValidationSubmitSort",
              "name": "Submit",
              "function": "",
              "class": "btnErrorMessage"
           }];
         $(this).setVerificationMessageBox("Secondary verification is required to sort these missing items. Please verify that items are missing.",
            "Secondary Verification",
            btnObjects,
            false,
            "btnValidationSubmitSort");
      }
   }
   window.onReconciling = onReconciling;

   // click event on reconciling button
   $(document).on("click", "#Reconcile_Close", onReconciling);

   // click on submit button when you are reconciling and submiting secondary verification
   $(document).on("click", "#btnValidationSubmitSort", function()
   {
      if ($(this).checkForValidation("E-Signature for Reconcile - Miss Sorted").Success)
      {
         if ($(".searchGrid_WrhsSrtDetails-div-click").hasClass("selected"))
         {
            $(".searchGrid_WrhsSrtDetails-div-click.selected").click();
         }

         $(this).closeMessageBox();
         $(".searchGrid_WrhsSrtDetails-div-click").not(".done")
        .closest("tr").each(function()
        {
           var type = $(this).find("td[exchange='" + $("#InventoryType").val() + "']").attr("exchange-value");
           var itemGuid = $(this).find("td[exchange='" + $("#InventoryKey").val() + "']").attr("exchange-value");
           var category = $(this).find("td[exchange='" + $("#InventoryCategory").val() + "']").attr("exchange-value");
           putContainerGhost(itemGuid, type,category);
        });
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Binventory Grid selection via item GUID scan.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var binventoryRowClick = function (e)
   {
      if (onClickGrid_ControlMethods.length > 0)
      {
         if (!executeMethod("onClickGrid_ControlMethods", e))
         {
            return false;
         }
      }

      if (e.hasClass("selected"))
      {
         if (onUnselectMethods.length > 0)
         {
            if (!executeMethod("onUnselectMethods", e))
            {
               return false;
            }
         }

         e.removeClass("selected");

      }
      else
      {
         e.closest("tr").addClass("MgmtEntity");

         if (onselectMethods.length > 0)
         {
            if (!executeMethod("onselectMethods", e))
            {
               return false;
            }
         }

         $("#searchGrid_WrhsSrtDetails tr.selected").removeClass("selected");
         $("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr").click();
         binventoryRowClick($("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr"));
      }
   }
   window.binventoryRowClick = binventoryRowClick;

   //*****************************************************************************
   //*
   //* Summary:
   //*   click on Binventory Grid. NOTE THAT WE HAVE REMOVED THE CLICK EVENT SO THAT WE DO NOT ALLOW USERS TO SELECT A BINVENTORY ITEM VIA CLICKING - DMB, 01.09.2019
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   //$(document).on("click", ".searchGrid_WrhsSrtDetails-div-click", function(e)
   //{
   //   if (onClickGrid_ControlMethods.length > 0)
   //   {
   //      if (!executeMethod("onClickGrid_ControlMethods", $(this)))
   //      {
   //         return false;
   //      }
   //   }

   //   if ($(this).hasClass("selected"))
   //   {
   //      if (onUnselectMethods.length > 0)
   //      {
   //         if (!executeMethod("onUnselectMethods", $(this)))
   //         {
   //            return false;
   //         }
   //      }

   //      $(this).removeClass("selected");

   //   }
   //   else
   //   {
   //      $(this).closest("tr").addClass("MgmtEntity");
   //      if (onselectMethods.length > 0)
   //      {
   //         if (!executeMethod("onselectMethods", $(this)))
   //         {
   //            return false;
   //         }
   //      }
   //      $("#searchGrid_WrhsSrtDetails tr.selected").removeClass("selected");
   //      $("#searchGrid_WrhsSrtDetails tr.MgmtEntity").closest("tr").click();
   //   }
   //});

   //*****************************************************************************
   //*
   //* Summary:
   //*   keypress event on key inputs.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".inputEx", function(e)
   {
      if ($(this).attr("id") === $("#InventoryKey").val())
      {
         // when press enter on key input ( like ItemGUID )
         if (e.keyCode === 13)
         {
            var keyValue = $(this).val();

            //do not check binventory if no GUID is entered
            if (keyValue.length === 0)
            {
               return false;
            }

            var $row = "";
            var $sidebarIb = sidebar.containers.find(c => c.attribute === "I" && c.isOpen());

            if ($sidebarIb !== undefined)
            {
               $("#searchGrid_WrhsSrtDetails td[exchange='" + $("#InventoryKey").val() + "']").each(function ()
               {
                  if ($(this).attr("exchange-value").toLowerCase() === keyValue.toLowerCase())
                  {
                     $row = $(this).closest("tr");
                     return false;
                  }
               });

               if ($row !== "")
               {
                  if (!$row.hasClass("selected"))
                  {
                     //replace the click trigger with the function call
                     //$row.trigger("click");
                     binventoryRowClick($row);
                  }
               }
               else
               {
                  // find where the scanned guid belongs
                  // add to list if found AND its container type matches the inbound container type
                  $("#processing-loading").show();
                  $.ajax({
                     cache: false,
                     type: "POST",
                     datatype: "JSON",
                     data: "itemGUID=" + keyValue + "&ibcontrId=" + $sidebarIb.containerId + "&ibcontrType=" + $sidebarIb.type + getAntiForgeryToken(),
                     url: "/Product/UPA/GetMissortedItemContr",
                     async: true,
                     success: function (data)
                     {
                        $("#processing-loading").hide();
                        if (data.success)
                        {
                           window.onAuditInventoryLoad = function()
                           {
                              $("#" + $("#InventoryKey").val()).val(keyValue);
                              $("#" + $("#InventoryKey").val()).focus();

                              //trigger the guid being typed in again
                              var ex = jQuery.Event("keypress");
                              ex.keyCode = 13;
                              ex.key = "Enter";
                              $("#" + $("#InventoryKey").val()).trigger(ex);
                              window.onAuditInventoryLoad = undefined;
                           }

                           //Audit does not have a refresh button; use this method, which calls Audit binventory
                           if (onMissortMethods.length > 0)
                           {
                              executeMethod("onMissortMethods", data);
                           }
                           else
                           {
                              $("#CMD_REFRESH").trigger("click");
                           }

                           messageObject = [{
                              "id": "lbl_messages",
                              "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>This item was found in another container of the same type as the inbound container. The records have been updated to reflect that. Processing can continue.</h3>"
                           }];
                           btnObject = [{
                              "id": "btn_missort_continue",
                              "name": "Continue",
                              "class": "btnErrorMessage",
                              "style": "margin:0 auto;text-align:center",
                              "function": "onclick='$(this).closeMessageBox();'"
                           }];

                           titleobjects = [{
                              "title": "Missorted Item"
                           }];
                           $(this).addMessageButton(btnObject, messageObject);
                           $(this).showMessageBox(titleobjects);
                           return false;
                        }
                        else if (data.error)
                        {
                           messageObject = [{
                              "id": "lbl_messages",
                              "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>The item could not be found in a container. Please see your supervisor.</h3>"
                           }];
                           btnObject = [{
                              "id": "btn_Continue",
                              "name": "Continue",
                              "class": "btnErrorMessage",
                              "style": "margin:0 auto;text-align:center",
                              "function": "onclick='$(this).closeMessageBox(); $(\"#\" + $(\"#InventoryKey\").val()).val(\"\"); $(\"#\" + $(\"#InventoryKey\").val()).focus();'"
                           }];

                           titleobjects = [{
                              "title": "Error Message"
                           }];
                           $(this).addMessageButton(btnObject, messageObject);
                           $(this).showMessageBox(titleobjects);

                           return false;
                        }
                        else if (!data.success && !data.error)
                        {
                           messageObject = [{
                              "id": "lbl_messages",
                              "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>This item has been missorted. Please place item into a(n) " + data.containerText + ".</h3>"
                           }];
                           btnObject = [{
                              "id": "btn_Continue",
                              "name": "Continue",
                              "class": "btnErrorMessage",
                              "style": "margin:0 auto;text-align:center",
                              "function": "onclick='$(this).closeMessageBox(); $(\"#\" + $(\"#InventoryKey\").val()).focus();'"
                           }];

                           titleobjects = [{
                              "title": "Error Message"
                           }];
                           $(this).addMessageButton(btnObject, messageObject);
                           $(this).showMessageBox(titleobjects);

                           return false;
                        }
                     }
                  });

                  $("#searchGrid_WrhsSrtDetails tr.selected").click();
               }
            }
         }
      }
   });
});