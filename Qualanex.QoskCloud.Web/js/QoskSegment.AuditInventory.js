﻿$(function ()
{
   //*****************************************************************************************
   //*
   //* QoskSegment.AuditInventory.js
   //*    Copyright (c) 2017 - 2018, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Provides functionality for the Induction Audit Inventory tab.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************************

   var stationId = "";
   var location = "";

   //*****************************************************************************
   //*
   //* Summary:
   //*   get the saved cookie based on the parameter
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getCookie(cname)
   {
      var name = cname + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++)
      {
         var c = ca[i];

         while (c.charAt(0) === ' ')
         {
            c = c.substring(1);
         }

         if (c.indexOf(name) === 0)
         {
            return c.substring(name.length, c.length);
         }
      }

      return "";
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Get the location of the machine based on the station id
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getMachineLocation()
   {
      var request = $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "machineName=" + stationId + getAntiForgeryToken(),
         async: false,
         url: "GetMachineLocation",
         success: function (data)
         {
         }
      });

      return request.responseJSON;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Items associated with bin Id.
   //*
   //* Parameters:
   //*   binId.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function getBinventoryTabDetails(binId)
   {
      $.ajax({
         cache: false,
         type: "POST",
         datatype: "JSON",
         data: "binId=" + binId + getAntiForgeryToken(),
         url: "GetBinventoryDetails",
         async: true,
         success: function (data)
         {
            var $target = $("#AuditInventory");
            var $newHtml = $(data);

            if ($newHtml.find(".searchGrid_WrhsSrtDetails-div-click").length > 0)
            {
               binItemId = binId.toUpperCase();
            }
            else
            {
               binItemId = "";
               $(".formMax .inputEx").val("");
            }

            $newHtml.find(".searchGrid_WrhsSrtDetails-div-click td[exchange='Sorted']").closest("td").each(function ()
            {
               if (parseInt($(this).attr("exchange-value")) === 1)
               {
                  $(this).closest("tr").addClass("done");
                  $(this).closest("tr").css("text-decoration", "line-through");
               }
            });

            var processedCount = $newHtml.find(".searchGrid_WrhsSrtDetails-div-click.done").length;
            var processingCount = $newHtml.find(".searchGrid_WrhsSrtDetails-div-click").length;

            if (processedCount === processingCount)
            {
               $newHtml.find("input#Reconcile_Close").val("Close");
            }

            $newHtml.find(".formMax").removeClass("formMax");
            $("#tab-container").easytabs("select", "#tabs-Binventory");
            $target.replaceWith($newHtml);

            setTimeout(function ()
            {
               $("#ItemGUID").select();
            }, 250);

            $("#searchGrid_WrhsSrtDetails_filter").css("display", "none");
            $("#searchGrid_WrhsSrtDetails_filter .dataTables_scroll table.dataTable thead tr th").removeClass("sorting");
            //$(document).off("click", ".searchGrid_WrhsSrtDetails-div-click");
            $("#SubmitSearch").attr("data-disable", true);
            window.showProcessedCounts();

            //DoNotAudit attribute is set if a logged in user goes into audit, and there's a bin already attached to the station
            //The bin also has at least one item processed by the same auditor, so we cannot let them audit their own items
            if ($("#searchGrid_WrhsSrtDetails").attr("donotaudit") !== undefined && $("#searchGrid_WrhsSrtDetails").attr("donotaudit") === "true")
            {
               var messageObject = [{
                  "id": "lbl_messages",
                  "name": "<h3 style='margin:0 auto;text-align:center;width:100%'>Container cannot be Audited as it includes items processed by the auditor</h3>" +
                     "<script>$('.close-button-messageBox').hide()</script>"
               }];
               var btnObjects = [{
                  "id": "btn_continue", "name": "OK", "function": "onclick='$(this).closeMessageBox();'", "class": "btnErrorMessage", "style": "margin:0 auto;text-align:center"
               }];
               var titleobjects = [{ "title": "Audit Binventory Error" }];

               $(this).addMessageButton(btnObjects, messageObject);
               $(this).showMessageBox(titleobjects);
               $("#ItemGUID").disable();
            }
         }
      });
   }

   var auditSelectMethod = function ($row)
   {
      if ($(".searchGrid_WrhsSrtDetails-div-click.selected").length === 0 && !$row.hasClass("done"))
      {
         $("#tab-container").easytabs("select", "#tabs-Policy");
         $("#ItemGUID").val($row.find("[exchange='ItemGUID']").attr("exchange-value"));
         $row.addClass("selected");
         $(".ndcField").focus();
         $("#SubmitSearch").attr("data-disable", false);
      }
   }

   $(document).on("keypress", "#ItemGUID", function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;

      if (key === 13)
      {
         //$("a[name='Clear']").click();
         var $row = $("[exchange='ItemGUID'][exchange-value='" + $(this).val().toLowerCase() + "']").parent();

         if ($row.length)
         {
            auditSelectMethod($row);
         }
         else
         {
            //$("#ItemGUID").val("");
            $("#SubmitSearch").attr("data-disable", true);
         }
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Pop-up shows up when a mis-sorted item was placed in the correct container
   //*   After pop-up is closed, focus on the NDC field (see QoskSegment.Binventory.js)
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btn_missort_continue", function ()
   {
      $(".ndcField").focus();
   });

   var checkIfDetailNeeded = function ()
   {
      var inbound = sidebar.getInboundContainer().containerId;

      if (($("#AuditInventory #ItemGUID").length === 0 && inbound !== "") || ($("#AuditInventory #ItemGUID").length && inbound === ""))
      {
         getBinventoryTabDetails(inbound);
      }
   }

   window.showProcessedCounts = function ()
   {
      var processedCount = $(".searchGrid_WrhsSrtDetails-div-click.done").length;
      var processingCount = $(".searchGrid_WrhsSrtDetails-div-click").length;

      if (processingCount > 0)
      {
         $("#processedCount").html(processedCount);
         $("#processingCount").html(processingCount);
         $(".defaultImage").hide();
         $(".processingCounts").show();
      }
      else
      {

         $(".defaultImage").show();
         $(".processingCounts").hide();
      }
   }

   var clearWhenInboundClosed = function (bin)
   {
      if ($(bin).find("input[name='Attribute']").val() === "I")
      {
         clearInduction();
      }
   }


   $(document).on("click", ".actionTool [name=Clear]", function ()
   {
      $(".searchGrid_WrhsSrtDetails-div-click.selected").removeClass("selected");
      $("#ItemGUID").val("");
      $("#ItemGUID").select();
      $("#SubmitSearch").attr("data-disable", true);
      $("#tab-container").easytabs("select", "#tabs-Binventory");
   });


   $(document).on("ready", function ()
   {
      $.getJSON("https://localhost:8007/Qualanex/http/MachineName", {}, function (data)
      {
         stationId = data;
      });
      stationId = stationId === "" ? getCookie("MachineName") : stationId;

      $("#SubmitSearch").attr("data-disable", true);
      window.addSelectMethod(auditSelectMethod);
      window.addMissortMethod(function ()
      {
         getBinventoryTabDetails(sidebar.getInboundContainer().containerId);
      });

      sidebar.onInit(checkIfDetailNeeded);

      sidebar.onBinClose(clearWhenInboundClosed);
      sidebar.onBinPause(clearWhenInboundClosed);

      //window.createAndExclusivePush("containerTypeCollapseMethods", function()
      //{
      //   console.log("setting audit guid");
      //   $(".ItemGuid").val($("#ItemGUID").val());
      //   $.ajax({
      //      cache: false,
      //      type: "POST",
      //      datatype: "JSON",
      //      data: "itemGuid=" + $("#ItemGUID").val() + getAntiForgeryToken(),
      //      url: "GetRaForItem",
      //      async: true,
      //      success: function(result)
      //      {
      //         window.raInformation = result;
      //      },
      //      error: function()
      //      {
      //         window.raInformation = undefined;
      //      }
      //   });
      //});

      window.addDesignationMethod(function ()
      {
         getBinventoryTabDetails(sidebar.getInboundContainer().containerId);
         $("#SubmitSearch").attr("data-disable", true);
      });
      window.addUnselectMethod(function ()
      {
         clearInduction();
      });
      sidebar.beforeOpen(function ()
      {
         location = location === "" ? getMachineLocation().Location : location;

         //do not try to open container in sidebar if the PC is not registered
         if (location === "" || location === undefined)
         {
            return "PC Registration Not Found. Please Contact Administrator.";
         }

         console.log("getMachineLocation(): " + location);
         return "";
      });
      sidebar.beforeClose(function (d)
      {
         return $(d).find("input[name='Attribute']").val() === "I"
            ? ($(".searchGrid_WrhsSrtDetails-div-click").length === $(".searchGrid_WrhsSrtDetails-div-click.done").length)
            : true;
      });

      checkIfDetailNeeded();
   });

   $(document).on("click", ".formCtrl", function ()
   {
      var processedCount = $(".searchGrid_WrhsSrtDetails-div-click.done").length;
      var processingCount = $(".searchGrid_WrhsSrtDetails-div-click").length;

      if (processingCount > 0)
      {
         var minProcessed = document.querySelector("#productDetailMinimal").querySelector("#processedCount");
         var minProcessing = document.querySelector("#productDetailMinimal").querySelector("#processingCount");

         if (minProcessed !== null && minProcessing !== null)
         {
            minProcessed.innerHTML = processedCount;
            minProcessing.innerHTML = processingCount;
         }
      }
   });
});