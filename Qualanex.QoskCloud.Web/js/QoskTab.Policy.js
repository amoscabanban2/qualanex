﻿//*****************************************************************************
//*
//* QoskSegment.LotExp.js
//*    Copyright (c) 2016 - 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Javascript functionality for the policy tab.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function()
{


   var incompleteMessageId = "IncompleteMessage";


   //disables all continue buttons 
   function disablePolicyContinue()
   {
      $("#lotContinue").attr("disabled", true);

   }
   window.disablePolicyContinue = disablePolicyContinue;

   //sets the next segment attribute on the segment to a specified segmentId
   var changeNextSegment = function($currentSegment, nextSegmentId)
   {
      $currentSegment.find("[data-ql-next]").each(function()
      {
         $(this).attr("data-ql-next", "#" + nextSegmentId);
      });
   }
   window.changeNextSegment = changeNextSegment;

   //automatically set the next segments based on the order of segments
   var setNextSegments = function()
   {
      var $prevPanel;
      $(".panel-collapse").each(function()
      {
         if ($prevPanel)
         {
            changeNextSegment($prevPanel, this.id);
         }
         $prevPanel = $(this);
      });
   }
   window.setNextSegments = setNextSegments;

   //enables the continue button
   function enablePolicyContinue()
   {
      $("#lotContinue").attr("disabled", false);

   }
   window.enablePolicyContinue = enablePolicyContinue;

   //adds a message to the policyresult segment alerting the user that the specified segment is not complete
   var addSegmentIncompleteMessage = function(segmentId)
   {
      if ($("#" + segmentId + incompleteMessageId).length)
      {
         return;
      }
      var $segment = $("#" + segmentId);
      $("#incompleteMessages").append("<span id='" +
          segmentId +
          incompleteMessageId +
          "' data-target-segment='" +
          segmentId +
          "' style='cursor:pointer;'>The " +
          $segment.attr("name") +
          " segment is not complete<br/></span>");
   }
   window.addSegmentIncompleteMessage = addSegmentIncompleteMessage;

   //removes the incomplete message for the given segment
   var removeSegmentIncompleteMessage = function(segmentId)
   {
      var $message = $("#" + segmentId + incompleteMessageId);
      if ($message.length)
      {
         $message.remove();
      }
   }
   window.removeSegmentIncompleteMessage = removeSegmentIncompleteMessage;

   //common helper method to set a segment as incomplete and display the incomplete message
   //returns false
   var failValidation = function(segmentId)
   {
      addSegmentIncompleteMessage(segmentId);
      $("#" + segmentId).attr("data-segment-complete", "false");
      return false;
   }
   window.failValidation = failValidation;

   //iterates through all segments with a data-sement-complete attribute and verifies their state
   var updateIncompleteMessages = function()
   {
      $(".ReadyForDispositionHidden").val("False");
      var dispositionFail = false;

      $("#Policy [data-segment-complete]").each(function()
      {
         if (["true", "NA"].includes($(this).attr("data-segment-complete")))
         {
            removeSegmentIncompleteMessage(this.id);
         }
         else
         {
            dispositionFail = true;
            addSegmentIncompleteMessage(this.id);
         }
      });

      //once all segments have been validated, then set the ReadyForDisposition flag to true
      if (!dispositionFail)
      {
         $(".ReadyForDispositionHidden").val("True");
      }
   }
   window.updateIncompleteMessages = updateIncompleteMessages;


   //*****************************************************************************
   //*
   //* Summary:
   //*   submits the policy information and updates the policy status. This is used
   //*   both for bin designation and policy result
   //*
   //* Parameters:
   //*   e - holds the event if submitPolicy is used as an event handler
   //*   control - the DOM element triggering the submit
   //*
   //* Returns:
   //*   None
   //*
   //*****************************************************************************
   var submitPolicy = function(e, control)
   {
      //perform validation for the segment if a button was clicked to continue
      var validateMethod = $(control).parents(".panel-collapse").attr("data-segment-validation");
      if (validateMethod && !window[validateMethod]()) //don't submit if validation fails
      {
         return;
      }

      //update the status of all segments
      updateIncompleteMessages();
      $(".policy-status").hide();
      var $btn = $(this);
      if ($(control).length)
      {
         $btn = $(control);
      }

      //find the next segment that is not complete to expand
      var $segments = $("#Policy [data-segment-complete]");
      var currentIndex = $segments.index($btn.parents(".panel-collapse:first"));
      var $next = $segments.filter((i, s) =>(i > currentIndex && $(s).attr("data-segment-complete") !== "true")).first();

      var $form = $("#policyForm");
      $form.validate();
      if ($form.valid())
      {
         var app = $(".QoskBody").attr("AppCode"); //get the application in which this is called

         //if this is not the last segment, collapse it
         if ($next.length)
         {
            $btn.parents(".panel-collapse:first").slideCollapse();
         }
         //disable all inputs while performing the submission
         var disableInputs = $("#Policy :input[disabled]");
         disableInputs.attr("disabled", false);

         //submit the form each and every time for non-Induction applets
         //for Induction applications only, send the form if we're ready for designation and there isn't currently another call to the server running
         //yes it's a bit ugly, but we want to isolate the induction process so that the window.formBusy isn't affecting other applications
         if (app !== "WAREHS_RETURN")
         {
            //indicate the result is updating
            $(".policy-status-loading").show();
            $form.submit();
         }
         else if ($(".ReadyForDispositionHidden").val() === "True" && !window.formBusy)
         {
            window.formBusy = true;
            $(".policy-status-loading").show(); //indicate the result is updating
            $form.submit();
         }
         
         //re-enable them once ready
         disableInputs.attr("disabled", true);
      }
      if ($next.length)
      {
         //expand the next tab
         $next.slideExpand();
         //scroll to the top of the next tab
         $('#tabs-Policy').animate({ scrollTop: 0 }, 200);
         setTimeout(function()
         {
            $('#tabs-Policy').animate({ scrollTop: $next.offset().top - $("#Policy").offset().top - 43 },
                200);

            if ($next[0].id === "lotDate")
            {
               if ($(".txtLotNo").length)
               {
                  $(".txtLotNo").focus();
                  $(".txtLotNo")[0].select();
               } else if ($(".cboExpMonth").length)
               {
                  $(".cboExpMonth").focus();
               }
            }
         },
             300);
      };
   }
   window.submitPolicy = submitPolicy;

   //validates all segments, updating the readyForDisposition value as needed
   var validateAll = function()
   {
      var originalReadyForDisposition = $(".ReadyForDispositionHidden").val();
      $(".ReadyForDispositionHidden").val("True");
      $("[data-segment-validation]").each(function()
      {
         var validateMethod = $(this).attr("data-segment-validation");
         if (validateMethod && !window[validateMethod]())
         {
            $(".ReadyForDispositionHidden").val("False");
         }
      });
      if (originalReadyForDisposition !== $(".ReadyForDispositionHidden").val())
      {

         var disabledInputs = $("#Policy :input[disabled]");
         disabledInputs.attr("disabled", false);
         $("#policyForm").submit();
         disabledInputs.attr("disabled", true);
         return false;
      }
      return true;
   }
   window.validateAll = validateAll;

   //click handler for continue buttons
   $(document)
       .on("click",
           ".ql-policy-continue",
           function(e)
           {
              submitPolicy(e, this);
              ga('send', 'event', 'Policy', 'Continue', $(this).attr("name") || this.id);

           });

   //click handler for continuing to a specified tab
   $(document).on("click",
       "[data-target-segment]",
       function()
       {
          var $next = $("#" + $(this).attr("data-target-segment"));
          $next.slideExpand();
          $('#tabs-Policy').animate({ scrollTop: 0 }, 200);
          setTimeout(function()
          {
             $('#tabs-Policy').animate({ scrollTop: $next.offset().top - $("#tabs-Policy").offset().top - 43 }, 200);
             if ($next[0].id === "lotDate")
             {
                if ($(".txtLotNo").length)
                {
                   $(".txtLotNo").focus();
                   $(".txtLotNo")[0].select();
                } else if ($(".cboExpMonth").length)
                {
                   $(".cboExpMonth").focus();
                }
             }
          },
              300);
       });

});