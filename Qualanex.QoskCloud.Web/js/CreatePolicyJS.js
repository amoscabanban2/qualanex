﻿var selectedNDCNumber = [];
var selectedNDCLOTNumber = [];


$(document).ready(function () {



    function resetreturnableproduct() {
        $('#qu-tab11').find('input:text').val('');
    }



    BindValuesInData("div.main-container");

    $(".show-pagesize-cmb").hide();
    $(".show-pagesize-cmb-ndclot-view").hide();
    $("#hdnpolicyPageNo").val(50);
    $('.grid-btn-prev-pharmacy').hide();
    $('.grid-btn-next-pharmacy').hide();
    $("#policytpeidforshoandhidepublished").hide();
    $("#policytpeidforshoandhidecustom").hide();
    $("#policyProfileTypeid").hide();
    $("#policygroupTypeid").hide();
    $("#policyprftypgrptypeID").hide();

    $('#PagesizeCounter').change(function () {
        var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
        var paginginfo = datainfo.paginginformation;
        $('#total-records').text(datainfo.rowscount);
        var pagesize = $('#PagesizeCounter').val();
        $('#jqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
    });
    $('#PagesizeCounterNDCLOT').change(function () {

        var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
        var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
        var checkedProductWithLot = "";
        var checkedProductNDC = "";
        if ((selectedddlPolicyTypeText = "Products Specific to NDC/UPC")) {

            var datainfo = $("#NDCjqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            $('#total-records-ndclot').text(datainfo.rowscount);
            var pagesize = $('#PagesizeCounterNDCLOT').val();

            $('#NDCjqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
        }
        if ((selectedddlPolicyTypeText = "Products Specific to NDC/UPC and Lot Number")) {
            var datainfo = $("#NDCLOTjqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            $('#total-records-ndclot').text(datainfo.rowscount);
            var pagesize = $('#PagesizeCounterNDCLOT').val();

            $('#NDCLOTjqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
        }

    });

    $("#ProfileListSec").hide();



    $("#btnok,.alert-close").click(function () {
        if (globalvalue == true) {
            window.location.reload(true);
            $(".alert-popup-body-message").hide();
            $(".alert-popup").animate({ "top": "40%" });
        }
        else {
            $(".alert-popup-body-message").hide();
            $(".alert-popup").animate({ "top": "40%" });
        }
    })

    $("#PolicyStartDate").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#PolicyEndDate").datepicker("option", "minDate", dt);
        }
    });
    $("#PolicyEndDate").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#PolicyStartDate").datepicker("option", "maxDate", dt);
        }
    });


    $("#ddlProfileTypeForCustomEnum").change(function () {

        $('#NDCjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedview").hide();


        $('#NDCLOTjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedlotNumber").hide();

        var selectedptype = document.getElementById("ddlProfileTypeForCustomEnum");
        var selectedText = selectedptype.options[selectedptype.selectedIndex].text;


        if (selectedText != 'Please select') {
            $("#ProfileListSec").show();
            var options = {};
            options.url = "../Policy/BindManufactureRepackagerWholesaler";
            options.type = "POST";
            options.data = JSON.stringify({ Ptype: selectedText });
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (MWR) {

                $("#MWRddllist").empty();
                $("#MWRddllist").append("<option selected=\"selected\"> Please select </option>");
                if (selectedText == "ManufacturerGroup" || selectedText == "WholesalerGroup") {
                    $.each(MWR, function (i, item) {
                        $("#MWRddllist").append("<option value=\"" + item.ProfileGroupID + "\">" + item.GroupName + "</option>");
                    });

                }
                else {

                    $.each(MWR, function (i, item) {
                        $("#MWRddllist").append("<option value=\"" + item.ProfileCode + "\">" + item.Name + "</option>");
                    });
                }
                $("#MWRddllist").prop("disabled", false);

            };
            options.error = function () { alert("Error retrieving states! (0x01)"); };
            $.ajax(options);
        }
        else {
            $("#MWRddllist").empty();
            $("#ProfileListSec").hide();
        }
    });

    $("#SelectedMWRTypepublished").change(function () {
        $('#NDCjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedview").hide();


        $('#NDCLOTjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedlotNumber").hide();

        var selectedptype = document.getElementById("SelectedMWRTypepublished");
        var selectedText = selectedptype.options[selectedptype.selectedIndex].text;
        if (selectedText != 'Please select') {
            $("#ProfileListSec").show();

            var options = {};
            options.url = "../Policy/BindManufactureRepackagerWholesaler";
            options.type = "POST";
            options.data = JSON.stringify({ Ptype: selectedText });
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (MWR) {

                $("#MWRddllist").empty();
                $("#MWRddllist").append("<option selected=\"selected\"> Please select </option>");
                $.each(MWR, function (i, item) {
                    $("#MWRddllist").append("<option value=\"" + item.ProfileCode + "\">" + item.Name + "</option>");
                });
                $("#MWRddllist").prop("disabled", false);

            };
            options.error = function () { alert("Error retrieving states! (0x02)"); };
            $.ajax(options);
        }
        else {
            $("#ProfileListSec").hide();
            $("#MWRddllist").empty();
        }
    });

    $("#ddlPolicyTypeEnum").change(function () {
        $('#NDCjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedview").hide();

        $('#NDCLOTjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedlotNumber").hide();

        $("#ddlProfileTypeForCustomEnum").val(0);
        $("#SelectedMWRTypepublished").val(0);
        $("#MWRddllist").val(0);

        if ($(this).val() == 1 || $(this).val() == 2) {
            $(".show-pagesize-cmb-ndclot-view").hide();
        }
    });

    $("#jqxgrid").bind('rowselect', function (event) {

        $("#selectedpharmacyProfileCode").val("");
        $("#selectedpharmacyProfileName").val("");
        var row = event.args.rowindex;

        var datarow = $("#jqxgrid").jqxGrid('getrowdata', row);
        var prfCode = datarow.ProfileCode;

        $("#selectedpharmacyProfileCode").val(prfCode);
        $("#selectedpharmacyProfileName").val(datarow.Name);
        $("#selectedpharmacyProfileName").show();

    });

    $('#product-grd-manufacturebasedview').on('click', '.NDCcheckbox', function () {

        if (this.checked) {
            var url = window.location.href;
            if (url.indexOf('ViewEditPolicy') > -1) {
                var checkedProductWithLot = "";
                var checkedProductNDC = "";
                var SelectionType = $("#ProfileGroupSelectionType").val();
                var selectedText = $("#SelectedPolicyType").val();
                var pharmacyProfileCode = "";
                if (selectedText == 'Customer Specific') {
                    if (SelectionType == "GroupType") {
                        pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                    }
                    if (SelectionType == "ProfileType") {
                        pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                    }
                }

                var selectedddlPolicyTypeText = $("#PolicyTypeText").val();
                if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC") {
                    checkedProductNDC = this.value
                    if (checkedProductNDC.length > 0) { } else {
                        $("#alertmessageid").text("Please select NDC/UPC");
                        $(".alert-popup-body-message").show();
                    }
                }
                var MWrProfileCode = $("#SelectedMWWRProfileCode").val().toString();
                if (selectedText == "Customer Specific") {
                    if (pharmacyProfileCode.length > 0) {
                    }
                    else {
                        this.checked = false;

                        $("#alertmessageid").text("Please Select pharmacy details");
                        $(".alert-popup-body-message").show();

                        return false;
                    }
                }
                var checkboxid = this.value;
                $.ajax({
                    type: "Get",

                    url: "../Policy/ISExistPolicy",
                    data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                    datatype: "json"

                }).done(function (data) {

                    $("#alertmessageid").text(data);
                    $(".alert-popup-body-message").show();
                    $("input[value=" + checkboxid + "]").removeAttr('checked');

                }).fail(function (response) {

                });
            }
            else {
                var name = document.getElementById("ddlPolicyNameEnum");
                var selectedText = name.options[name.selectedIndex].text;

                var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
                var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
                var checkedProductWithLot = "";
                var checkedProductNDC = "";
                if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC")
                { checkedProductNDC = this.value }


                var MWrProfileCode = $("#MWRddllist").val();
                var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();

                var SelectionType = $('input[name=select-type]:checked').val();
                if (SelectionType == "GroupType") {
                    var pharmacyProfileCode = $("#grptype").val();
                }
                if (SelectionType == "ProfileType") {
                    var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
                }
                if (selectedText == "Customer Specific") {
                    if (pharmacyProfileCode.length > 0)
                    { }
                    else
                    {
                        this.checked = false;
                        $("#alertmessageid").text("Please Select pharmacy details");
                        $(".alert-popup-body-message").show();

                        return false;
                    }
                }
                var checkboxid = this.value;
                $.ajax({
                    type: "Get",

                    url: "../Policy/ISExistPolicy",
                    data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                    datatype: "json"

                }).done(function (data) {

                    $("input[value=" + checkboxid + "]").removeAttr('checked');


                    $("#alertmessageid").text(data);
                    $(".alert-popup-body-message").show();


                    return false;

                }).fail(function (response) {

                });
            }
        }
    });

    $('#product-grd-manufacturebasedlotNumber').on('click', '.lotchecked-class', function () {

        if (this.checked) {
            var url = window.location.href;
            if (url.indexOf('ViewEditPolicy') > -1) {
                var checkedProductWithLot = "";
                var checkedProductNDC = "";
                var SelectionType = $("#ProfileGroupSelectionType").val();
                if (SelectionType == "GroupType") {
                    var pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                }
                if (SelectionType == "ProfileType") {
                    var pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                }
                var selectedText = $("#SelectedPolicyType").val();
                var selectedddlPolicyTypeText = $("#PolicyTypeText").val();
                if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC and Lot Number")
                { checkedProductWithLot = this.value }
                var MWrProfileCode = $("#SelectedMWWRProfileCode").val().toString();
                if (selectedText == "Customer Specific") {
                    if (pharmacyProfileCode.length > 0)
                    { }
                    else
                    {

                        $("#alertmessageid").text("Please Select pharmacy details");
                        $(".alert-popup-body-message").show();
                        return false;
                    }
                }
                var checkboxid = this.value;
                $.ajax({
                    type: "Get",
                    url: "../Policy/ISExistPolicy",
                    data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                    datatype: "json"

                }).done(function (data) {

                    $("input[value=" + checkboxid + "]").removeAttr('checked');
                    $("#alertmessageid").text(data);
                    $(".alert-popup-body-message").show();
                }).fail(function (response) {

                });
            }
            else {
                var name = document.getElementById("ddlPolicyNameEnum");
                var selectedText = name.options[name.selectedIndex].text;

                var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
                var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
                var checkedProductWithLot = "";
                var checkedProductNDC = "";
                if (selectedddlPolicyTypeText == "Products Specific to NDC/UPC and Lot Number")
                { checkedProductWithLot = this.value }


                var MWrProfileCode = $("#MWRddllist").val();
                var SelectionType = $('input[name=select-type]:checked').val();
                if (SelectionType == "GroupType") {
                    var pharmacyProfileCode = $("#grptype").val();
                }
                if (SelectionType == "ProfileType") {
                    var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
                }

                if (selectedText == "Customer Specific") {
                    if (pharmacyProfileCode.length > 0)
                    { }
                    else
                    {
                        $("#alertmessageid").text("Please Select pharmacy details");
                        $(".alert-popup-body-message").show();
                        this.checked = false;
                        return false;
                    }
                }
                var checkboxid = this.value;
                $.ajax({
                    type: "Get",
                    url: "../Policy/ISExistPolicy",
                    data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                    datatype: "json"

                }).done(function (data) {

                    $("input[value=" + checkboxid + "]").removeAttr('checked');
                    $("#alertmessageid").text(data);
                    $(".alert-popup-body-message").show();
                }).fail(function (response) {

                });
            }
        }
    });


    $("#NDCjqxgrid").bind('cellendedit', function (event) {
        if (event.args.value) {
            $("#jqxgrid").jqxGrid('selectrow', event.args.rowindex);
        }
        else {
            $("#jqxgrid").jqxGrid('unselectrow', event.args.rowindex);
        }
    });

    $('#NumberOfMonthsAfterExpirationReturnable').blur(function (e) {
        if (!($('#NumberOfMonthsAfterExpirationReturnable').val() <= 255 && $('#NumberOfMonthsAfterExpirationReturnable').val() >= 0)) {
            $("#alertmessageid").text("Value should be less than 256");
            $(".alert-popup-body-message").show();
            $('#NumberOfMonthsAfterExpirationReturnable').val('');
        }
    });

    $('#NumberOfMonthsBeforeExpirationReturnableSealed').blur(function (e) {
        if (!($('#NumberOfMonthsBeforeExpirationReturnableSealed').val() <= 255 && $('#NumberOfMonthsBeforeExpirationReturnableSealed').val() >= 0)) {
            $("#alertmessageid").text("Value should be less than 256");
            $(".alert-popup-body-message").show();
            $('#NumberOfMonthsBeforeExpirationReturnableSealed').val('');
        }
    });

    $('#NumberOfMonthsBeforeExpirationReturnableOpened').blur(function (e) {
        if (!($('#NumberOfMonthsBeforeExpirationReturnableOpened').val() <= 255 && $('#NumberOfMonthsBeforeExpirationReturnableOpened').val() >= 0)) {
            $("#alertmessageid").text("Value should be less than 256");
            $(".alert-popup-body-message").show();
            $('#NumberOfMonthsBeforeExpirationReturnableOpened').val('');
        }
    });

    $("#MWRddllist").change(function () {

        var MWRTYPEforPublishedCustome = '';
        var MWRProfileCode = $("#MWRddllist").val();
        var selectedptype = document.getElementById("MWRddllist");
        var selectedText = selectedptype.options[selectedptype.selectedIndex].text;

        var selectedpolicyname = document.getElementById("ddlPolicyNameEnum");
        var selectedpolicyNameText = selectedpolicyname.options[selectedpolicyname.selectedIndex].text;
        if (selectedpolicyNameText == "Customer Specific") {
            var SelectedMWRTypepublishedID = document.getElementById("ddlProfileTypeForCustomEnum");
            MWRTYPEforPublishedCustome = SelectedMWRTypepublishedID.options[SelectedMWRTypepublishedID.selectedIndex].text;
        }
        else {
            var SelectedMWRTypepublishedID = document.getElementById("SelectedMWRTypepublished");
            MWRTYPEforPublishedCustome = SelectedMWRTypepublishedID.options[SelectedMWRTypepublishedID.selectedIndex].text;
        }

        var SelectedMWRTypepublishedID = document.getElementById("SelectedMWRTypepublished");
        var MWRTYPEforPublished = SelectedMWRTypepublishedID.options[SelectedMWRTypepublishedID.selectedIndex].text;


        var selectedddlPolicyTypeEnum = document.getElementById("ddlPolicyTypeEnum");
        var selecteddddlPolicyTypeEnumText = selectedddlPolicyTypeEnum.options[selectedddlPolicyTypeEnum.selectedIndex].text;



        if (selectedText != "Please select") {

            if (selecteddddlPolicyTypeEnumText == 'Products Specific to NDC/UPC' && MWRTYPEforPublishedCustome != "Wholesaler" && MWRTYPEforPublishedCustome != "WholesalerGroup") {
                GetNDCUPC();
            }
            if (selecteddddlPolicyTypeEnumText == 'Products Specific to NDC/UPC and Lot Number' && MWRTYPEforPublishedCustome != "Wholesaler" && MWRTYPEforPublishedCustome != "WholesalerGroup") {
                GetNDCUPCLOT();
            }
            if (selecteddddlPolicyTypeEnumText == 'All Products') {
            }

        }
        else { $("#PolicyType").prop("disabled", true); }
    });

    $("div").on('click', '.next-click', function () {
        $(this).parent().parent().next().slideDown();
        $(this).parent().parent().hide();
        $(this).parents('div.tab-container').find("li.active").removeClass('active');
        $(this).parents('div.tab-container').find("a.active").removeClass('active');
        $(this).parents('div.tab-container').find("div.active").removeClass('active');
        var aa = $(this).parent().parent().attr('id');

        var a1 = "a[href=#" + aa + "]";
        $(this).parents('div.tab-container').find(a1).parent().next().addClass('active').show();
        $(this).parents('div.tab-container').find(a1).parent().next().children().addClass('active').removeClass('disable-click');
    });

    $('#tab-container').easytabs();
    $('#tab-container1').easytabs();

    $("#ddlPolicyNameEnum").change(function () {

        var name = document.getElementById("ddlPolicyNameEnum");
        var selectedText = name.options[name.selectedIndex].text;
        if (selectedText == 'Published Policy') {
            $("#policytpeidforshoandhidepublished").show();
            $("#policytpeidforshoandhidecustom").hide();
            $("#policyprftypgrptypeID").hide();

            $("#policyProfileTypeid").hide();
            $("#policygroupTypeid").hide();

            $('#NDCjqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebasedview").hide();


            $('#NDCLOTjqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebasedlotNumber").hide();

            $('#jqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebased").hide();

            $('#selectedpharmacyProfileName').val("");
            $('#selectedpharmacyProfileCode').val("");
            $('#selectedpharmacyProfileName').hide();
            $('#MWRddllist').val(0);

            $(".show-pagesize-cmb").hide();
            $(".show-pagesize-cmb-ndclot-view").hide();

        }
        else if (selectedText == 'Customer Specific') {
            $("#policytpeidforshoandhidepublished").hide();
            $("#policytpeidforshoandhidecustom").show();
            $("#policyprftypgrptypeID").show();
            $('#NDCjqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebasedview").hide();

            $('#NDCLOTjqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebasedlotNumber").hide();

            $('#jqxgrid').jqxGrid('clear');
            $("#product-grd-manufacturebased").hide();
            $("#MWRddllist").val(0);
        }
        else if (selectedText == 'Please select') {
            window.location.reload(true);
            $("#policytpeidforshoandhidepublished").hide();
            $("#policytpeidforshoandhidecustom").hide();
            $("#policyprftypgrptypeID").hide();

            $('#selectedpharmacyProfileName').val("");
            $('#selectedpharmacyProfileCode').val("");
            $('#selectedpharmacyProfileName').hide();
        }

    });




    $('#product-grd-manufacturebased').on('click', '.check-item1', function () {
            $('.check-item1').not(this).prop('checked', false);
    });



    $("#profile-type").click(function () {
        $("#selectedpharmacyProfileCode").val("");
        $("#selectedpharmacyProfileName").val("");
        $("#selectedpharmacyProfileName").hide();

        $('#NDCjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedview").hide();


        $('#NDCLOTjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedlotNumber").hide();

        $('#jqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebased").hide();

        $("#policyProfileTypeid").show();
        $("#policygroupTypeid").hide();
        bindProfileGroupData("Profile")
    });

    $("#group-type").click(function () {

        $("#selectedpharmacyProfileCode").val("");
        $("#selectedpharmacyProfileName").val("");
        $("#selectedpharmacyProfileName").hide();

        $('#NDCjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedview").hide();


        $('#NDCLOTjqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebasedlotNumber").hide();

        $('#jqxgrid').jqxGrid('clear');
        $("#product-grd-manufacturebased").hide();

        $("#policyProfileTypeid").hide();
        $("#policygroupTypeid").show();
        bindProfileGroupData("Group");
    });

    $('#prftype').change(function () {

        HideRefreshNDCAndNDCLOTGrid();

        GetPharmacyDetails();
        var selectedcheckedgroupProfile = $('.multiselect-container dropdown-menu active checkbox').map(function () {
            return this.value;
        }).get();


        $("#MWRddllist").val(0);

    });

    $('#grptype').change(function () {

        HideRefreshNDCAndNDCLOTGrid();

        GetPharmacyDetails();
        $("#MWRddllist").val(0);
        $("#ddlPolicyTypeEnum").val(0);
    });





    $(window).scroll(function () {


        if (parseInt($("#hdnpolicyPageNo").val()) < 500) {
            var noofRecords = ($("#hdnpharmacyTotalRecords").val());
            if (parseInt($("#hdnpolicyPageNo").val()) < noofRecords) {

                if ($(window).scrollTop() == $(document).outerHeight() - $(window).outerHeight()) {
                    $("#wait").css("display", "block");
                    $("#hdnpolicyPageNo").val(parseInt($("#hdnpolicyPageNo").val()) + 50);
                    SetPagesizeDynamic();
                }
            }

        }
    });


    $('#btnPolicyUpdate').click(function () {

        if ($('#PolicyStartDate').val() == '') {
            $("#alertmessageid").text("Please Select Policy Start Date");
            $(".alert-popup-body-message").show();

            return false;
        }
        if ($('#PolicyEndDate').val() == '') {
            $("#alertmessageid").text("Please Select Policy End Date");
            $(".alert-popup-body-message").show();

            return false;
        }
        if ($('#PolicyStartDate').val().length > 0 && $('#PolicyEndDate').val().length > 0) {

        }

        var requestData =
          {
              //Returnable Product Rules
              PolicyID: $('#PolicyID').val(),
              PolicyName: $('#PolicyName').val(),
              Form41Allowed: $("input[name=Form41Allowed]:checked").val(),
              InnerPack: $('#InnerPack').val(),
              NumberOfMonthsAfterExpirationReturnable: $('#NumberOfMonthsAfterExpirationReturnable').val(),
              NumberOfMonthsBeforeExpirationReturnableSealed: $('#NumberOfMonthsBeforeExpirationReturnableSealed').val(),
              NumberOfMonthsBeforeExpirationReturnableOpened: $('#NumberOfMonthsBeforeExpirationReturnableOpened').val(),
              ExpiresOnFirstOfCurrentMonth: $('#ExpiresOnFirstOfCurrentMonth').val(),
              OverstockApprovalRequired: $("input[name=OverstockApprovalRequired ]:checked").val(),
              ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
              BrokenSealReturnable: $("input[name=BrokenSealReturnable]:checked").val(),
              PrescriptionVialsReturnable: $("input[name=PrescriptionVialsReturnable]:checked").val(),
              RepackagedIntoUnitDoseReturnable: $("input[name=RepackagedIntoUnitDoseReturnable]:checked").val(),
              MissingLotNumberorExpDateNotReturnable: $("input[name=MissingLotNumberorExpDateNotReturnable]:checked").val(),
              PartialsReturnable: $("input[name=PartialsReturnable]:checked").val(),
              SelectedPolicyType: $('#SelectedPolicyType').val(),

              //Pricing Rules
              HandlingFee: $('#HandlingFee').val(),
              UseDebitPricing: $('#UseDebitPricing').val(),
              HandlingFeeExceptionForStatePartialPolicyLaw: $('#HandlingFeeExceptionForStatePartialPolicyLaw').val(),
              AllowQuantityOverPackageSize: $("input[name=AllowQuantityOverPackageSize]:checked").val(),
              DoNotUseFallThroughPrice: $("input[name=DoNotUseFallThroughPrice]:checked").val(),
              UseWholeSalerDirectPriceWhenNoIndirect: $("input[name=UseWholeSalerDirectPriceWhenNoIndirect]:checked").val(),
              AuthorizedForCreditConsideration: $('#AuthorizedForCreditConsideration').val(),
              //Event Rules

              PriorReturnAuthorizationRequired: $("input[name=PriorReturnAuthorizationRequired]:checked").val(),
              DoNotUseFallThroughPrice: $("input[name=OverstockApprovalRequired]:checked").val(),
              ReturnAuthorizationCompletionPercent: $('#ReturnAuthorizationCompletionPercent').val(),
              PolicyStartDate: $('#PolicyStartDate').val(),
              PolicyEndDate: $('#PolicyEndDate').val()
          };

        $.ajax({
            type: "POST",
            url: "../Policy/UpdatePolicyDetails",
            data: { 'requestData': requestData },
            datatype: "json"

        }).done(function (data) {

            //Successfully pass to server and get response
            $("#alertmessageid").text(data);
            $(".alert-popup-body-message").show();
            globalvalue = true;

        }).fail(function (response) {

        });

    });

    $("#btnviewCreatedNDC").click(function () {
        if ($("#PolicyTypeText").val() == "Products Specific to NDC/UPC and Lot Number") {
            GenerateGrid1();
            $("#product-grd-manufacturebasedlotNumberview").show();
        }
        if ($("#PolicyTypeText").val() == "Products Specific to NDC/UPC") {
            GenerateGridForNDCLOTView();
            $("#product-grd-manufacturebasedviewviewpolicy").show();
        }


    });

    $("#btnAddNEW").click(function () {
        if ($("#PolicyTypeText").val() == "Products Specific to NDC/UPC and Lot Number") {
            CreateNDCLOTGridPanel('viewpolicy');
            $("#btnupdateNDC").show();
            $("#product-grd-manufacturebasedlotNumber").show();
            $("#product-grd-manufacturebased").hide();
            $("#partialProductLotListGrd").show();
            $("#product-grd-manufacturebasedview").hide();
            $("#partialProductListGrd").hide();


        }
        if ($("#PolicyTypeText").val() == "Products Specific to NDC/UPC") {
            CreateNDCGridPanel('viewpolicy');
            $("#btnupdateNDC").show();
            $("#product-grd-manufacturebasedview").show();
            $("#partialProductListGrd").show();
            $("#product-grd-manufacturebasedlotNumber").hide();
            $("#partialProductLotListGrd").hide();
        }

    });

    $("#btnupdateNDC").click(function () {

        var checkedProductWithLot = "";
        var checkedProductNDC = "";
        var checkedProductWithLot = selectedNDCLOTNumber;

        checkedProductNDC = selectedNDCNumber;
        if (checkedProductWithLot.length > 0 || checkedProductNDC.length > 0) {

        }
        else {

            $("#alertmessageid").text("Please select NDC/UPC/LOT");
            $(".alert-popup-body-message").show();
            return false;
        }

        var MWRPolicyType = $("#PolicyType").val();
        var MWRProfileCode = $("#SelectedMWWRProfileCode").val();

        var selectedPharmacy = "";
        var SelectionType = $("#ProfileGroupSelectionType").val();
        if (SelectionType == "GroupType") {
             selectedPharmacy = $("#SelectedPharmacyGroupProfileCode").val();
        }
        if (SelectionType == "ProfileType") {
            selectedPharmacy = $("#SelectedPharmacyGroupProfileCode").val();
        }

        var WMRProfileCode = "";
        var ProfileType = "";
        MWRProfileType = $("#MWWRProfileType").val();


        if (MWRProfileType == 1) {
            WMRProfileCode = MWRProfileCode;
            ProfileType = 1;
        }
        if (MWRProfileType == 3) {
            WMRProfileCode = MWRProfileCode;
            ProfileType = 3;
        }
        if (MWRProfileType == 5) {
            WMRProfileCode = MWRProfileCode;
            ProfileType = 5;
        }
        if (MWRProfileType == 4) {
            WMRProfileCode = MWRProfileCode;
            ProfileType = 4;
        }
        if (MWRProfileType == 2) {
            WMRProfileCode = MWRProfileCode;
            ProfileType = 2;
        }

        var requestData =
            {
                PolicyID: $("#PolicyID").val(),
                ProfileGroupSelectionType: SelectionType
            };
        var selecteddddlPolicyNameEnumText = $("#PolicyTypeName").val();

        $.ajax({
            type: "POST",

            url: "../Policy/SavePolicyDetailsNew",
            data: { 'requestData': requestData, 'pharmacylist': selectedPharmacy.toString(), 'checkedProductWithLot': checkedProductWithLot.toString(), 'checkedProductNDC': checkedProductNDC, 'WMProfileType': ProfileType, 'WMRProfileCode': WMRProfileCode.toString(), 'PolicyType': MWRPolicyType.toString() },
            datatype: "json"

        }).done(function (data) {

            $("#alertmessageid").text(data);
            $(".alert-popup-body-message").show();

        }).fail(function (response) {

        });
    });

    $('#btnEventNext').click(function () {

        if ($('#PolicyStartDate').val() == '') {
            $("#alertmessageid").text("Please Select Policy Start Date");
            $(".alert-popup-body-message").show();
            $("#btnEventNext").removeClass('next-click');
            return;
        }
        if ($('#PolicyEndDate').val() == '') {
            $("#alertmessageid").text("Please Select Policy End Date");
            $(".alert-popup-body-message").show();
            $("#btnEventNext").removeClass('next-click');
            return;
        }
        if ($('#PolicyStartDate').val().length > 0 && $('#PolicyEndDate').val().length > 0) {
            $("#btnEventNext").addClass('next-click');
        }

    });



    var policyType = $("#PolicyType").val();
    if (policyType == 2 || policyType == 3) {

        $("#btnviewCreatedNDC").show();
        $("#btnAddNEW").show();
    }

    $('#resetBtnCancel').on('click', function () {
        window.location.reload(true);
    });


});


function InsertWithValidation() {
    var resulr = FinalSaveValidate();
}

function SavePolicyDetails() {


    var checkedProductWithLot = "";
    var checkedProductNDC = "";

    var checkedProductWithLot = selectedNDCLOTNumber;
    checkedProductNDC = selectedNDCNumber;



    var MWRPolicyType = $('#ddlPolicyTypeEnum').val();

    var MWRProfileCode = $("#MWRddllist").val();


    var SelectionType = $('input[name=select-type]:checked').val();
    var selectedPharmacy = "";
    if (SelectionType == "GroupType") {
        selectedPharmacy = $("#grptype").val();
    }
    if (SelectionType == "ProfileType") {
        selectedPharmacy = $("#selectedpharmacyProfileCode").val();
    }
    var selecteddddlPolicyNameEnumText = $("#ddlPolicyNameEnum").val();

    var WMRProfileCode = 0;
    var ProfileType;
    var MWRProfileType;
    if (selecteddddlPolicyNameEnumText == 2) {

        MWRProfileType = $('#ddlProfileTypeForCustomEnum').val();

    }
    else {

        MWRProfileType = $('#SelectedMWRTypepublished').val();
    }

    if (MWRProfileType == 1) {
        WMRProfileCode = MWRProfileCode;
        ProfileType = 1;
    }
    if (MWRProfileType == 2) {
        WMRProfileCode = MWRProfileCode;
        ProfileType = 2;
    }
    if (MWRProfileType == 3) {
        WMRProfileCode = MWRProfileCode;
        ProfileType = 3;
    }
    if (MWRProfileType == 4) {
        WMRProfileCode = MWRProfileCode;
        ProfileType = 4;
    }
    if (MWRProfileType == 5) {
        WMRProfileCode = MWRProfileCode;
        ProfileType = 5;
    }



    var requestData =
      {
          PolicyType: $('#ddlPolicyTypeEnum').val(),
          Form41Allowed: $("input[name=Form41Allowed]:checked").val(),
          InnerPack: $('#InnerPack').val(),
          NumberOfMonthsAfterExpirationReturnable: $('#NumberOfMonthsAfterExpirationReturnable').val(),
          NumberOfMonthsBeforeExpirationReturnableSealed: $('#NumberOfMonthsBeforeExpirationReturnableSealed').val(),
          NumberOfMonthsBeforeExpirationReturnableOpened: $('#NumberOfMonthsBeforeExpirationReturnableOpened').val(),
          ExpiresOnFirstOfCurrentMonth: $('#ExpiresOnFirstOfCurrentMonth').val(),
          OverstockApprovalRequired: $("input[name=OverstockApprovalRequired ]:checked").val(),
          ARCOSReportable: $("input[name=ARCOSReportable]:checked").val(),
          BrokenSealReturnable: $("input[name=BrokenSealReturnable]:checked").val(),
          PrescriptionVialsReturnable: $("input[name=PrescriptionVialsReturnable]:checked").val(),
          RepackagedIntoUnitDoseReturnable: $("input[name=RepackagedIntoUnitDoseReturnable]:checked").val(),
          MissingLotNumberorExpDateNotReturnable: $("input[name=MissingLotNumberorExpDateNotReturnable]:checked").val(),
          PartialsReturnable: $("input[name=PartialsReturnable]:checked").val(),
          SelectedPolicyType: selecteddddlPolicyNameEnumText,//$('#SelectedPolicyType').val(),

          //Pricing Rules
          HandlingFee: $('#HandlingFee').val(),
          UseDebitPricing: $('#UseDebitPricing').val(),
          HandlingFeeExceptionForStatePartialPolicyLaw: $('#HandlingFeeExceptionForStatePartialPolicyLaw').val(),
          AllowQuantityOverPackageSize: $("input[name=AllowQuantityOverPackageSize]:checked").val(),
          DoNotUseFallThroughPrice: $("input[name=DoNotUseFallThroughPrice]:checked").val(),
          UseWholeSalerDirectPriceWhenNoIndirect: $("input[name=UseWholeSalerDirectPriceWhenNoIndirect]:checked").val(),
          AuthorizedForCreditConsideration: $('#AuthorizedForCreditConsideration').val(),
          //Event Rules

          PriorReturnAuthorizationRequired: $("input[name=PriorReturnAuthorizationRequired]:checked").val(),
          DoNotUseFallThroughPrice: $("input[name=OverstockApprovalRequired]:checked").val(),
          ReturnAuthorizationCompletionPercent: $('#ReturnAuthorizationCompletionPercent').val(),
          ProfileGroupSelectionType: SelectionType,
          PolicyStartDate: $('#PolicyStartDate').val(),
          PolicyEndDate: $('#PolicyEndDate').val(),
      };

    $.ajax({
        type: "POST",

        url: "../Policy/SavePolicyDetailsNew",
        data: { 'requestData': requestData, 'pharmacylist': selectedPharmacy.toString(), 'checkedProductWithLot': checkedProductWithLot.toString(), 'checkedProductNDC': checkedProductNDC, 'WMProfileType': ProfileType, 'WMRProfileCode': WMRProfileCode, 'PolicyType': MWRPolicyType },
        datatype: "json"

    }).done(function (data) {

        $("#alertmessageid").text(data);

        $(".alert-popup-body-message").show();
        globalvalue = true;

    }).fail(function (response) {

    });
}

var globalvalue = false;

function HideRefreshNDCAndNDCLOTGrid() {
    $('#NDCjqxgrid').jqxGrid('clear');
    $("#product-grd-manufacturebasedview").hide();


    $('#NDCLOTjqxgrid').jqxGrid('clear');
    $("#product-grd-manufacturebasedlotNumber").hide();
}

function GetPharmacyDetails() {

    $("#product-grd-manufacturebased").show();

    var CurrentServerPageNo = $('#txtCurrentPageNoforpharmacy').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }
        var SelectionType;
        var checkedprofile;
        var pgType = $('input[name=select-type]:checked').val();

        if (pgType == 'ProfileType') {
            SelectionType = "ProfileType"
            checkedprofile = $("#prftype").val();
        }
        if (pgType == 'GroupType') {
            SelectionType = "GroupType"
            checkedprofile = $("#grptype").val();
        }
        GenerateGrid(checkedprofile, SelectionType);

}

function GenerateGridForRestPagesforPharmacy(ButtonClicked) {

    $("#wait").css("display", "block");
    $("#hdnpolicyPageNo").val(50);
    var result = parseInt($('#txtCurrentPageNoforpharmacy').val());
    if (isNaN(result) == true) {
        result = 0;
    }
    if (ButtonClicked) {
        result = result + 1;
    }
    else {
        result = result - 1;
    }
    if (result < 0) { result = 0; }
    $('#txtCurrentPageNoforpharmacy').val(result);
    result = $('#txtCurrentPageNoforpharmacy').val();
    $("#wait").css("display", "none");
    //Call search methods
    GetPharmacyDetails();

    $("#wait").css("display", "none");
}
function SetPagesizeDynamic() {
    var Pagesizeee = parseInt($("#hdnpolicyPageNo").val());
    $('#jqxgrid').jqxGrid({ pagesizeoptions: [Pagesizeee] });
    $("#wait").css("display", "none");
    $("#jqxgrid").jqxGrid('reloadGrid');



}
function bindProfileGroupData(prfgrptype) {

    //$(document).ready(function () {
        $.ajax({
            type: 'POST',
            url: '../Policy/GetProfileAndGroupType',
            data: { profilegroup: prfgrptype },
            success: function (data) {
                if (prfgrptype == "Profile") {
                    $("#prftype").empty();
                    $("#prftype").append("<option selected=\"selected\"> Select Type </option>");
                    $.each(data, function (i, item) {

                        $("#prftype").append("<option>" + item + "</option>");
                    });

                }
                else if (prfgrptype == "Group") {
                    $("#grptype").empty();
                    $("#grptype").append("<option selected=\"selected\">Select Group</option>");
                    $.each(data, function (i, item) {

                        $("#grptype").append("<option value=\"" + item.ProfileGroupID + "\"  >" + item.GroupName + "</option>");
                    });

                }

            }
        });
    //});
}

function BindProfileTypeAndGroupTypeDropdown(prfgrptype) {

    var options = {};
    options.url = "../Policy/GetProfileAndGroupType";
    options.type = "POST";
    options.data = { 'profilegroup': prfgrptype };
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (MWR) {
        $("#prftype").empty();
        if (prfgrptype == "Profile") {
            $.each(MWR, function (i, item) {
                $("#prftype").append("<option>" + item.GroupName + "</option>");
            });

        }
        else if (prfgrptype == "Group") {
            {
                $.each(MWR, function (i, item) {
                    $("#grptype").append("<option> value=\"" + item.ProfileCode + "\">" + item.GroupName + "</option>");
                });
            }
            $("#MWRddllist").prop("disabled", false);

        };
        options.error = function () { alert("Error retrieving states! (0x03)"); };
        $.ajax(options);

    }
}

function SteponeValidate() {
    var selectedPharmacy = "";
    var checkedProductWithLot = "";
    var checkedProductNDC = "";
    var selectedText = $('#ddlPolicyNameEnum').val();
    if (selectedText == 0) {
        $("#alertmessageid").text(Policy.SelectPolicy);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }
    if (selectedText == 1) {

    }
    if (selectedText == 2) {
        var pgType = $('input[name=select-type]:checked').val();

        if (pgType == 'ProfileType') {

             selectedPharmacy = $("#selectedpharmacyProfileCode").val();
        }
        if (pgType == 'GroupType') {

             selectedPharmacy = $("#grptype").val();
        }

        if (pgType !== 'ProfileType' && pgType !== 'GroupType') {
            $("#alertmessageid").text(Policy.SelectProfilegroupType);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;
        }
        if (pgType == 'ProfileType') {
            var profiletypeseleted = document.getElementById("prftype");
            var profiletypeseletedText = profiletypeseleted.options[profiletypeseleted.selectedIndex].text;
            if (profiletypeseletedText == 'Select Type') {
                $("#alertmessageid").text(Policy.SelectPharmacyType);
                $(".alert-popup-body-message").show();
                $("#btnPolicyStepOne").removeClass('next-click');
                return false;
            }
        }
        if (pgType == 'GroupType') {
            var grouptypeseleted = document.getElementById("grptype");
            var grouptypeseletedText = grouptypeseleted.options[grouptypeseleted.selectedIndex].text;
            if (grouptypeseletedText == 'Select Group') {
                $("#alertmessageid").text(Policy.SelectPharmacyGroupType);
                $(".alert-popup-body-message").show();
                $("#btnPolicyStepOne").removeClass('next-click');
                return false;
            }
        }

        if (selectedPharmacy.length > 0) {

        }
        else {
            $("#alertmessageid").text(Policy.SelectPharmacy);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;

        }
    }

    var selectedddlPolicyTypeEnumnameText = $('#ddlPolicyTypeEnum').val();
    if (selectedddlPolicyTypeEnumnameText == 0) {
        $("#alertmessageid").text(Policy.SelectPolicyType);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }


    var MWRTYPEforPublishedCustome = '';
    if (selectedText == 2) {
        var MWRTYPEforPublishedCustome = $('#ddlProfileTypeForCustomEnum').val();

    }
    else {
        var MWRTYPEforPublishedCustome = $('#SelectedMWRTypepublished').val();
    }
    if (MWRTYPEforPublishedCustome == 0) {
        $("#alertmessageid").text(Policy.SelectPharmacyProfileType);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }

    var MWRddllistame = document.getElementById("MWRddllist");
    var MWRddllistameText = MWRddllistame.options[MWRddllistame.selectedIndex].text;

    var MWRProfileCode = '';
     MWRProfileCode = $("#MWRddllist").val();
     if (MWRProfileCode != null && MWRddllistameText != 'Please select' && MWRProfileCode.length > 0) { }
    else {
        $("#alertmessageid").text(Policy.SelectProfileName);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }



    if (selectedddlPolicyTypeEnumnameText == 2 && (MWRTYPEforPublishedCustome != 3 && MWRTYPEforPublishedCustome != 5)) {
        checkedProductNDC = selectedNDCNumber;
        if (checkedProductNDC.length > 0) { $("#btnPolicyStepOne").addClass('next-click'); }
        else {
            $("#alertmessageid").text(Policy.SelectNDCUPC);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;
        }
    }
    if (selectedddlPolicyTypeEnumnameText == 3 && (MWRTYPEforPublishedCustome != 3 && MWRTYPEforPublishedCustome != 5)) {
        var checkedProductWithLot = selectedNDCLOTNumber;
        if (checkedProductWithLot.length > 0) { $("#btnPolicyStepOne").addClass('next-click'); }
        else {
            $("#alertmessageid").text(Policy.SelectNDCUPCLOT);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;
        }
    }


    if ((selectedText == 1 || selectedText == 2) && (selectedddlPolicyTypeEnumnameText == 3 || selectedddlPolicyTypeEnumnameText == 2) && (MWRTYPEforPublishedCustome == 3 || MWRTYPEforPublishedCustome == 4)) {
        $("#alertmessageid").text(Policy.RestrictToCreatePolicyforWholesaler);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }

    if (selectedText == 1 && selectedddlPolicyTypeEnumnameText == 1) {
        var rslt = checkPublishedPolicyForAllProductforCustomerSpecific(null);
        if (rslt == null || rslt == '') {
            $("#btnPolicyStepOne").addClass('next-click');
        }
        else {
            $("#alertmessageid").text(rslt);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;
        }

    }
    if (selectedText == 2 && selectedddlPolicyTypeEnumnameText == 1) {
        var rslt = checkPublishedPolicyForAllProductforCustomerSpecific(null);
        if (rslt == null) {
            $("#btnPolicyStepOne").addClass('next-click');
        }
        else { $("#btnPolicyStepOne").removeClass('next-click'); return false; }
    }

    $("#btnPolicyStepOne").addClass('next-click');
    //return true;
}

function FinalSaveValidate() {
    var selectedPharmacy = "";
    var checkedProductWithLot = "";
    var checkedProductNDC = "";
    var selectedText = $('#ddlPolicyNameEnum').val();
    if (selectedText == 0) {
        $("#alertmessageid").text(Policy.SelectPolicy);
        $(".alert-popup-body-message").show();

        return false;
    }
    if (selectedText == 1) {

    }
    if (selectedText == 2) {
        var pgType = $('input[name=select-type]:checked').val();

        if (pgType == 'ProfileType') {

            selectedPharmacy = $("#selectedpharmacyProfileCode").val();
        }
        if (pgType == 'GroupType') {

            selectedPharmacy = $("#grptype").val();
        }


        if (pgType !== 'ProfileType' && pgType !== 'GroupType') {
            $("#alertmessageid").text(Policy.SelectProfilegroupType);
            $(".alert-popup-body-message").show();

            return false;
        }

        if (pgType == 'ProfileType') {
            var profiletypeseleted = document.getElementById("prftype");
            var profiletypeseletedText = profiletypeseleted.options[profiletypeseleted.selectedIndex].text;
            if (profiletypeseletedText == 'Select Type') {
                $("#alertmessageid").text(Policy.SelectPharmacyType);
                $(".alert-popup-body-message").show();

                return false;
            }
        }
        if (pgType == 'GroupType') {
            var grouptypeseleted = document.getElementById("grptype");
            var grouptypeseletedText = grouptypeseleted.options[grouptypeseleted.selectedIndex].text;
            if (grouptypeseletedText == 'Select Group') {
                $("#alertmessageid").text(Policy.SelectPharmacyGroupType);
                $(".alert-popup-body-message").show();

                return false;
            }
        }

        if (selectedPharmacy.length > 0) {

        }
        else {
            $("#alertmessageid").text(Policy.SelectPharmacy);
            $(".alert-popup-body-message").show();

            return false;

        }
    }

    var ddlPolicyTypeEnumname = document.getElementById("ddlPolicyTypeEnum");
    var selectedddlPolicyTypeEnumnameText = ddlPolicyTypeEnumname.options[ddlPolicyTypeEnumname.selectedIndex].text;
    if (selectedddlPolicyTypeEnumnameText == 'Please select') {
        $("#alertmessageid").text(Policy.SelectPolicyType);
        $(".alert-popup-body-message").show();

        return false;
    }


    var MWRTYPEforPublishedCustome = '';
    if (selectedText == 2) {

        var MWRTYPEforPublishedCustome = $('#ddlProfileTypeForCustomEnum').val();
    }
    else {

        var MWRTYPEforPublishedCustome = $('#SelectedMWRTypepublished').val();
    }
    if (MWRTYPEforPublishedCustome == 0) {
        $("#alertmessageid").text(Policy.SelectPharmacyProfileType);
        $(".alert-popup-body-message").show();

        return false;
    }

    var MWRddllistame = document.getElementById("MWRddllist");
    var MWRddllistameText = MWRddllistame.options[MWRddllistame.selectedIndex].text;

    var MWRProfileCode = '';
    MWRProfileCode = $("#MWRddllist").val();
    if (MWRProfileCode != null && MWRddllistameText != 'Please select' && MWRProfileCode.length > 0) { }
    else {
        $("#alertmessageid").text(Policy.SelectProfileName);
        $(".alert-popup-body-message").show();

        return false;
    }



    if (selectedddlPolicyTypeEnumnameText == 2 && (MWRTYPEforPublishedCustome != 3 && MWRTYPEforPublishedCustome != 5)) {

        checkedProductNDC = selectedNDCNumber;
        if (checkedProductNDC.length > 0) { $("#btnPolicyStepOne").addClass('next-click'); }
        else {
            $("#alertmessageid").text(Policy.SelectNDCUPC);
            $(".alert-popup-body-message").show();

            return false;
        }
    }
    if (selectedddlPolicyTypeEnumnameText == 3 && (MWRTYPEforPublishedCustome != 3 && MWRTYPEforPublishedCustome != 5)) {

        var checkedProductWithLot = selectedNDCLOTNumber;
        if (checkedProductWithLot.length > 0) { $("#btnPolicyStepOne").addClass('next-click'); }
        else {
            $("#alertmessageid").text(Policy.SelectNDCUPCLOT);
            $(".alert-popup-body-message").show();

            return false;
        }
    }


    if ((selectedText == 1 || selectedText == 2) && (selectedddlPolicyTypeEnumnameText == 3 || selectedddlPolicyTypeEnumnameText == 2) && (MWRTYPEforPublishedCustome == 3 || MWRTYPEforPublishedCustome == 4)) {
        $("#alertmessageid").text(Policy.RestrictToCreatePolicyforWholesaler);
        $(".alert-popup-body-message").show();
        $("#btnPolicyStepOne").removeClass('next-click');
        return false;
    }

    if (selectedText == 1 && selectedddlPolicyTypeEnumnameText == 1) {
        var rslt = checkPublishedPolicyForAllProductforCustomerSpecific(null);
        if (rslt == null || rslt == '') {

        }
        else {
            $("#alertmessageid").text(rslt);
            $(".alert-popup-body-message").show();
            $("#btnPolicyStepOne").removeClass('next-click');
            return false;
        }

    }
    if (selectedText == 2 && selectedddlPolicyTypeEnumnameText == 1) {
        var rslt = checkPublishedPolicyForAllProductforCustomerSpecific(null);
        if (rslt == null) {

        }
        else { return false; }
    }
    if ($('#PolicyStartDate').val() == '') {
        $("#alertmessageid").text(Policy.SelectPolicystartdate);
        $(".alert-popup-body-message").show();

        return false;
    }
    if ($('#PolicyEndDate').val() == '') {
        $("#alertmessageid").text(Policy.SelectPolicyenddate);
        $(".alert-popup-body-message").show();

        return false;
    }
    if ($('#PolicyStartDate').val().length > 0 && $('#PolicyEndDate').val().length > 0) {
        SavePolicyDetails();
    }

}

function GenerateGrid(checkedprofile, SelectionType) {

    var source = {

        datatype: "json",
        type: 'GET',
          url: "../Policy/PharmacyDetailsBasedonProfileAndGroupTypeforCreatePolicyScreen",
          data: { checkedprofile: checkedprofile.toString(), SelectionType: SelectionType },
        root: 'Rows',
        contentType: "application/json; charset=utf-8",
        // update the grid and send a request to the server.
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
        processdata: function (data) {
            var pagesize = $('#PagesizeCounter').val();
            data.pagesize = pagesize;

        },
        loadComplete: function () {
            var datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records').text(datainfo.rowscount);

                $(".show-pagesize-cmb").show();
            }
            else {
                $('#total-records').text(0);
                $('#PagesizeCounter').val(100);
                $(".show-pagesize-cmb").hide();
            }

        }

    };

    var dataadapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
        },

    });

    var pgType = $('input[name=select-type]:checked').val();
    var columnsnew;
    if (pgType == 'ProfileType') {
        columnsnew = [
              { text: 'Name', editable: false, datafield: 'Name' },
              { text: 'Address', editable: false, datafield: 'Address1' },
              { text: 'City', editable: false, datafield: 'City' },
              { text: 'State', editable: false, datafield: 'State' }


        ]
    }
    if (pgType == 'GroupType') {
        columnsnew = [

               { text: 'Name', editable: false, datafield: 'GroupProfileName' },
               { text: 'City', editable: false, datafield: 'City' },
               { text: 'State', editable: false, datafield: 'State' },
               { text: 'Region', editable: false, datafield: 'Region' }


        ]
    }
    selectrow = -1;
    if (pgType == 'ProfileType') {
        var selectGRPProfileMain = {
            "text": "Select",
            "datafield": "Select",
            "width": "100",
            "sortable": false,
            "cellsrenderer": function (row, column, value) {

                selectrow = row;

               var dataRecord = $("#jqxgrid").jqxGrid('getrowdata', selectrow);
               var GroupProfile = dataRecord.ProfileCode;

                var html = "<label><input type=\"checkbox\" class=\"check-item1\" value=\"" + GroupProfile + "\"  ><i></i></label>";
                return html;
            }
        }

    }
    columnsnew.push(selectGRPProfileMain);
    $("#jqxgrid").jqxGrid({
        width: '100%',
        source: dataadapter,
        pageable: true,
        sortable: true,
        filterable: true,
        pageable: true,
        columnsreorder: true,
        columnsresize: true,
        showdefaultloadelement: false,
        enabletooltips: true,
        columnsresize: true,
        height: '100%',
        pagermode: 'simple',
        rendergridrows: function (obj) {
            return obj.data;
        },
        ready: function () {
            $('#jqxgrid').jqxGrid({ pagesizeoptions: ['100'] });


        },
        columns: columnsnew,


    });


}
function NumericOnly(obj) {
    if (obj.value.length > 0) {

        obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}
function NumericWithDecimalOnly(obj) {
    if (obj.value.length > 0) {
        obj.value = obj.value.replace(/[^0-9\.]/g, '');
    }
}
function CreateNDCLOTGridPanel(createviewsearchtype) {


    var selectedText = "";
    var searchTypeID = "";
    var selectedProfileCode = "";
    if (createviewsearchtype != 'viewpolicy') {
        var selectedptype = document.getElementById("ddlProfileTypeForCustomEnum");
        selectedText = selectedptype.options[selectedptype.selectedIndex].text;
         searchTypeID = parseInt($("#ddlPolicyTypeEnum").val());
         selectedProfileCode = $("#MWRddllist").val().toString();
    }
    else {
         selectedText = $("#SelectedMWWRProfileType").val();
         selectedProfileCode = $("#SelectedMWWRProfileCode").val();
         searchTypeID = parseInt($("#PolicyType").val());
    }

    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }

    GenerateNDCLOTGrid(selectedProfileCode, searchType, selectedText);
}
function CreateNDCLOTGridPanelOLD(createviewsearchtype) {

    var CurrentServerPageNo = $('#txtNDCLOTCurrentPageNo').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }
    CurrentServerPageNo = 0;
    var selectedText = "";
    var searchTypeID = "";
    var selectedProfileCode = "";
    if (createviewsearchtype != 'viewpolicy') {
        var selectedptype = document.getElementById("ddlProfileTypeForCustomEnum");
        selectedText = selectedptype.options[selectedptype.selectedIndex].text;
        searchTypeID = parseInt($("#ddlPolicyTypeEnum").val());
        selectedProfileCode = $("#MWRddllist").val().toString();
    }
    else {
        selectedText = $("#SelectedMWWRProfileType").val();
        selectedProfileCode = $("#SelectedMWWRProfileCode").val().toString();
        searchTypeID = parseInt($("#PolicyType").val());
    }

    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }


    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufacture',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,
            CurrentServerPageNo: CurrentServerPageNo
        }, // Note it is important// Note it is important
        success: function (data) {
            GenerateNDCLOTGrid(data);

        },
        error: function (response) {
            alert(response);
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        }
    });
}
var ConvertDate = function (row, columnfield, value, defaulthtml, columnproperties) {

    if (value != '') {
        var dateString = value.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var date = month + "/" + day + "/" + year;

        return date;
    }
    else { return '' }
}
function GenerateNDCLOTGrid(selectedProfileCode, searchType, selectedText) {

    var source = {

        datatype: "json",
        type: 'GET',
        //datafields: jsoncolumn,//[{ name: 'Expiration Date', type: "date", format: 'dd.MM.yyyy' }],
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufacture',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,

        }, // Note it is important// Note it is important
        contentType: "application/json; charset=utf-8",
        filter: function () {
            $("#NDCLOTjqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#NDCLOTjqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        processdata: function (data) {
           // var pagesize = $('#PagesizeCounterNDCLOT').val();
            //data.pagesize = pagesize;

        },

        async: false,
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        }

    };
    var columnsmain = [
    { text: 'Name', datafield: 'Name' },
     { text: 'Description', datafield: 'Description' },
     { text: 'MFG Labeler Code', datafield: 'MFGLabelerCode' },
     { text: 'MFG Product Number', datafield: 'MFGProductNumber' },
     { text: 'NDC', datafield: 'NDC' },
     { text: 'NDC/UPC WithDashes', datafield: 'NDCUPCWithDashes' }

    ]
    var adapter = new $.jqx.dataAdapter(source);



    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);
        console.log(record);
        //var selecetdProfileCode = record.ProfileCode;
        var selecetdProductID = record.ProductID;
        var selectedPrfCode = record.ProfileCode;

        var selectrow = -1;
        var nestedSource =
         {

             datatype: "json",
             type: 'Get',
             url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureDetails',
             root: 'Rows',
             data: {
                 //checkedManufactureprofile: selectedProfileCode,
                 checkedManufactureprofile: selectedPrfCode,
                 searchType: searchType,
                 MWRType: selectedText,
                 ProductID: selecetdProductID
             },
             //contentType: "application/json; charset=utf-8",
             filter: function () {
                 grid.jqxGrid('updatebounddata', 'filter');
             },
             // update the grid and send a request to the server.
             sort: function () {
                 grid.jqxGrid('updatebounddata', 'sort');
             },
             async: false,
             //root: 'Rows',
             beforeprocessing: function (data) {
                 nestedSource.totalrecords = data.TotalRows;
             },
             loadComplete: function () {

             }


         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);

        var columnsnew = [
                          { text: 'Lot Number', datafield: 'LotNumber' },
                          { text: 'Expiration Date', datafield: 'ExpirationDate', cellrenderer: ConvertDate }

        ]
        $(columnsnew).each(function (i, v) {
            if (v.datafield == 'ExpirationDate') {
                v.cellsrenderer = ConvertDate;
            }
        });



        function removeByIndex(arr, index) {
            arr.splice(index, 1);
        }



        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter,
                width: '99%',
                height: 300,
                filterable: true,
                pageable: true,
                pagermode: 'simple',
                selectionmode: 'checkbox',
                sortable: true,
                columnsreorder: true,
                columnsresize: true,
                virtualmode: true,
                ready: function () {
                    grid.jqxGrid('showrowdetails', 0);


                    grid.bind('rowunselect', function (event) {

                        var datarow = grid.jqxGrid('getrowdata', event.args.rowindex);

                        var SelectedProfileCode = datarow.ProfileCode;
                        var LotNumberID = datarow.LotNumber;
                        var ProductID = datarow.ProductID;
                        var checkedproductlot = SelectedProfileCode + "_" + ProductID + "_" + LotNumberID;

                        var index = selectedNDCLOTNumber.indexOf(checkedproductlot);
                        if (index > -1) {
                            removeByIndex(selectedNDCLOTNumber, index);
                        }
                        //alert(selectedNDCLOTNumber)
                    });
                    grid.bind('rowselect', function (event) {
                        var datarow = grid.jqxGrid('getrowdata', event.args.rowindex);
                        console.log(datarow);
                        var SelectedProfileCode = datarow.ProfileCode;
                        var LotNumberID = datarow.LotNumber;
                        var ProductID = datarow.ProductID;
                        var checkedproductlot = SelectedProfileCode + "_" + ProductID + "_" + LotNumberID;
                        var index = selectedNDCLOTNumber.indexOf(checkedproductlot);
                        if (index == -1) {
                            selectedNDCLOTNumber.push(checkedproductlot);
                            NDCLOTPOlicyValidation(checkedproductlot, grid, event.args.rowindex);
                        }
                    });
                },

                rendergridrows: function (obj) {
                    console.log(obj);

                    return obj.data;
                },
                columns: columnsnew,

            });

        }

    }
    $("#NDCLOTjqxgrid").jqxGrid(
    {
        width: '100%',
        autoheight: true,
        filterable: true,
        pageable: true,
        sortable: true,
        columnsreorder: true,
        source: source,
        pagermode: 'simple',
        rowdetails: true,
        rowsheight: 35,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 4px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        ready: function () {

            $("#NDCLOTjqxgrid").jqxGrid('showrowdetails', 0);
        },

        virtualmode: true,
        rendergridrows: function (obj) {
            console.log(obj.data);
            return obj.data;
        },
        columns: columnsmain
    });
}
function GenerateNDCLOTGridForRestPages(ButtonClicked) {
    var result = parseInt($('#txtNDCLOTCurrentPageNo').val());
    if (isNaN(result) == true) {
        result = 0;
    }
    //alert(result);
    if (ButtonClicked) {
        result = result + 1;
    }
    else {
        result = result - 1;
    }
    if (result < 0) { result = 0; }
    //alert(result);
    $('#txtNDCLOTCurrentPageNo').val(result);
    result = $('#txtNDCLOTCurrentPageNo').val();

    //Call search methods
   // CreateNDCLOTGridPanel();

}
function CreateNDCGridPanel(createviewsearchtype) {


    var selectedText = "";
    var searchTypeID = "";
    var selectedProfileCode = "";
    if (createviewsearchtype != 'viewpolicy') {
        var selectedptype = document.getElementById("ddlProfileTypeForCustomEnum");
        selectedText = selectedptype.options[selectedptype.selectedIndex].text;
        searchTypeID = parseInt($("#ddlPolicyTypeEnum").val());
        selectedProfileCode = $("#MWRddllist").val().toString();
    }
    else {
        selectedText = $("#SelectedMWWRProfileType").val();
        selectedProfileCode = $("#SelectedMWWRProfileCode").val().toString();
        searchTypeID = parseInt($("#PolicyType").val());
    }


    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }
    //var selectedProfileCode = $("#MWRddllist").val().toString();

    GenerateNDCGrid(selectedProfileCode, searchType, selectedText);
    //$.ajax({
    //    type: "Get",
    //    contentType: 'application/json; charset=utf-8',
    //    url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufacture',
    //    data: {
    //        checkedManufactureprofile: selectedProfileCode,
    //        searchType: searchType,
    //        MWRType: selectedText,
    //        CurrentServerPageNo: CurrentServerPageNo
    //    }, // Note it is important
    //    success: function (data) {

    //        GenerateNDCGrid(data);

    //    },
    //    error: function (response) {
    //        alert(response);
    //        if (response.status != 0) {
    //            alert(response.status + " " + response.statusText);
    //        }
    //    }
    //});
}






function GenerateNDCGrid(selectedProfileCode, searchType, selectedText) {
    var source = {

        datatype: "json",
        type: 'GET',
        //datafields: jsoncolumn,//[{ name: 'Expiration Date', type: "date", format: 'dd.MM.yyyy' }],
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufacture',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,

        },
        //contentType: "application/json; charset=utf-8",
        filter: function () {
            $("#NDCjqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#NDCjqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        async: false,
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
        processdata: function (data) {
            var pagesize = $('#PagesizeCounterNDCLOT').val();
            data.pagesize = pagesize;

        },
        loadComplete: function () {
            var datainfo = $("#NDCjqxgrid").jqxGrid('getdatainformation');

            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records-ndclot').text(datainfo.rowscount);
                $(".show-pagesize-cmb-ndclot-view").show();
            }
            else {
                $(".show-pagesize-cmb-ndclot-view").hide();
                $('#total-records-ndclot').text(0);
                $('#PagesizeCounterNDCLOT').val(100);

            }

        }
    };
    var columnsmain = [
    { text: 'Name', datafield: 'Name' },
    { text: 'Type', datafield: 'Type' },
    { text: 'City', datafield: 'City' },
    { text: 'State', datafield: 'State' },
    { text: 'RegionCode', datafield: 'RegionCode' },
    { text: 'Address1', datafield: 'Address1' }

    ]
    var adapter = new $.jqx.dataAdapter(source);



    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);

        //// var testttt = json[index].groupDetail;
        //var testttt = record.groupDetail;
        ////console.log(testttt.groupDetails[index].GroupProfileCode);

        //var innergrid = JSON.stringify(record.groupDetail);
        ////  var innergrid = JSON.stringify(json[index].groupDetail);

        //var selectedgrpID = record.ProfileGroupID;

        var selectedPrfCode = record.ProfileCode;
        var editrow = -1;

        var nestedSource =
         {

             datatype: "json",
             type: 'Get',
             url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureDetails',
             root: 'Rows',
             data: {
                 //checkedManufactureprofile: selectedProfileCode,
                 checkedManufactureprofile: selectedPrfCode,
                 searchType: searchType,
                 MWRType: selectedText,
                 ProductID: 0
             },
             //contentType: "application/json; charset=utf-8",
             filter: function () {
                 grid.jqxGrid('updatebounddata', 'filter');
             },
             // update the grid and send a request to the server.
             sort: function () {
                 grid.jqxGrid('updatebounddata', 'sort');
             },
             async: false,
             //root: 'Rows',
             beforeprocessing: function (data) {
                 nestedSource.totalrecords = data.TotalRows;
             },
             loadComplete: function () {

             }


         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [
                        //{ text: 'Select', datafield: 'Select', columntype: 'checkbox', width: 40 },
                        { text: 'Description', datafield: 'Description' },
                        { text: 'NDC', datafield: 'NDC' },
                        { text: 'NDCUPCWithDashes', datafield: 'NDCUPCWithDashes' },
                        { text: 'MFGLabelerCode', datafield: 'MFGLabelerCode' },
                        { text: 'Strength', datafield: 'Strength' },


        ]


      //  columnsnew.push(selectProductProfile);

        function removeByIndex(arr, index) {
            arr.splice(index, 1);
        }

        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter,
                width: '99%',
                height: 300,
                filterable: true,
                pageable: true,
                pagermode: 'simple',
                selectionmode: 'checkbox',
                sortable: true,
                columnsreorder: true,
                columnsresize: true,
                virtualmode: true,
                ready: function () {
                    grid.jqxGrid('showrowdetails', 0);


                    grid.bind('rowunselect', function (event) {
                        var datarow = grid.jqxGrid('getrowdata', event.args.rowindex);

                        var ProductProfilecode = datarow.ProfileCode;
                        var ProductID = datarow.ProductID;
                        var checkedProductNDC = ProductProfilecode + "_" + ProductID
                        var index = selectedNDCNumber.indexOf(checkedProductNDC);
                        if (index > -1) {
                            removeByIndex(selectedNDCNumber, index);
                        }

                    });
                    grid.bind('rowselect', function (event) {

                        var datarow = grid.jqxGrid('getrowdata', event.args.rowindex);
                        var ProductProfilecode = datarow.ProfileCode;
                        var ProductID = datarow.ProductID;
                        var checkedProductNDC = ProductProfilecode + "_" + ProductID


                        var index = selectedNDCNumber.indexOf(checkedProductNDC);

                        if (index == -1) {
                            selectedNDCNumber.push(checkedProductNDC);
                            NDCPolicyValidation(checkedProductNDC, grid, event.args.rowindex);

                        }

                        //if (index > -1) {
                        //    selectedNDCNumber.push(checkedProductNDC);
                        //}

                    });
                },

                rendergridrows: function (obj) {
                    //console.log(obj);
                    //for (var i = 0; i < obj.data.length; i++) {
                    //    if (obj.data[i].Assigned != null && obj.data[i].Assigned) {
                    //        //var prfCode = obj.data[i].PProfileCode;
                    //        //var groupID = obj.data[i].ProfileGroupID;
                    //        //var Region = obj.data[i].Region;
                    //        //selectedgrpdata.push(groupID + "_" + prfCode + "_" + Region);

                    //        selectedrows.push(i);
                    //    }
                    //}

                    return obj.data;
                },
                columns: columnsnew,

            });

            //$.each(selectedrows, function (i, v) {
            //    grid.jqxGrid('selectrow', v);
            //});

            //selectedrows = new Array();

        }


    }

    // creage jqxgrid
    $("#NDCjqxgrid").jqxGrid(
    {
        width: '100%',
        autoheight: true,
        filterable: true,
        pageable: true,
        sortable: true,
        columnsreorder: true,
        source: source,
        pagermode: 'simple',
        rowdetails: true,
        rowsheight: 35,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 4px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        ready: function () {
            var pagesize = $('#PagesizeCounterNDCLOT').val();
            $('#NDCjqxgrid').jqxGrid({ pagesizeoptions: [pagesize] });
            $("#NDCjqxgrid").jqxGrid('showrowdetails', 0);
        },

        virtualmode: true,
        rendergridrows: function (obj) {
            return obj.data;
        },
        columns: columnsmain
    });


}

function GenerateNDCGridOLD(json) {
    $("#NDCjqxgrid").jqxGrid('updatebounddata');
    var data = JSON.stringify(json)
    var source =
    {
        root: "entry",
        datatype: 'json',
        localdata: data
    };
    var columnsmain = [
     { text: 'Name', datafield: 'Name' },
     { text: 'Type', datafield: 'Type' },
     { text: 'City', datafield: 'City' },
     { text: 'State', datafield: 'State' },
     { text: 'RegionCode', datafield: 'RegionCode' },
     { text: 'Address1', datafield: 'Address1' }

    ]
    var adapter = new $.jqx.dataAdapter(source);
    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);
        var innerID = json[index].products;

        //console.log(testttt.groupDetails[index].GroupProfileCode);
        var innergrid = JSON.stringify(json[index].products);
        var nestedSource =
         {

             datatype: 'json',
             root: 'productsDetails',
             localdata: innergrid

         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [
                        //{ text: 'Select', datafield: 'Select', columntype: 'checkbox', width: 40 },
                        { text: 'Description', datafield: 'Description' },
                        { text: 'NDC', datafield: 'NDC' },
                        { text: 'NDCUPCWithDashes', datafield: 'NDCUPCWithDashes' },
                        { text: 'MFGLabelerCode', datafield: 'MFGLabelerCode' },
                        { text: 'Strength', datafield: 'Strength' },


        ]
        var selectProductProfile = {
            "text": "Select",
            "datafield": "Select",
            "width": "100",
            "sortable": false,
            "cellsrenderer": function (row, column, value) {
                //var isGroupProfileSelected = nestedAdapter._source.localdata[row].Assigned;

                var ProductProfilecode = innerID.productsDetails[row].ProfileCode;
                //var checkedstatus = innerID.productsDetails[row].Assigned;
                var checkedstatus = false;
                var ProductID = innerID.productsDetails[row].ProductID;
                var productIDWithProfileCode = ProductProfilecode + "_" + ProductID;

                if (checkedstatus) {
                    var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  disabled=\"disabled\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " checked><i></i></label>";
                    return html;
                }
                else {
                    var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " ><i></i></label>";
                    return html;
                }

            }
        }
       columnsnew.push(selectProductProfile);

        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter, width: '97%', height: 300,
                filterable: true,
                //editable: true,
                pageable: true,
                columnsreorder: true,
                columnsresize: true,
                columns: columnsnew
            });
        }
    }
    $("#NDCjqxgrid").jqxGrid(
   {
       width: '100%',
       autoheight: true,
       filterable: true,
       pageable: true,
       columnsreorder: true,
       source: source,
       rowdetails: true,
       rowsheight: 35,
       initrowdetails: initrowdetails,
       rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
       ready: function () {
           $("#NDCjqxgrid").jqxGrid('showrowdetails', 1);
           $('#NDCjqxgrid').jqxGrid({ pagesizeoptions: ['50'] });
       },
       columnsresize: true,
       columns: columnsmain
   });
}

function GetNDCUPC() {
    //GenerateGridForNDCLOTView();
    CreateNDCGridPanel("");
    $("#product-grd-manufacturebasedview").show();
    $("#partialProductListGrd").show();
    $("#product-grd-manufacturebasedlotNumber").hide();
    $("#partialProductLotListGrd").hide();

    return false;
}
function GetNDCUPCLOT() {

    CreateNDCLOTGridPanel("");
    $("#product-grd-manufacturebasedlotNumber").show();
    //$("#product-grd-manufacturebased").hide();
    $("#partialProductLotListGrd").show();
    $("#product-grd-manufacturebasedview").hide();
    $("#partialProductListGrd").hide();
    //return false;
}

var globalstatus = '';
function checkPublishedPolicyForAllProductforCustomerSpecific(str) {
    var flag = false;

    var SelectionType = $('input[name=select-type]:checked').val();
    var name = document.getElementById("ddlPolicyNameEnum");
    var selectedText = name.options[name.selectedIndex].text;
    // var selectedText = $('#ddlPolicyNameEnum').val();

    var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
    var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
    //var selectedddlPolicyTypeText=$('#ddlPolicyTypeEnum').val();

    var checkedProductWithLot = "";
    var checkedProductNDC = "";
    if (selectedddlPolicyTypeText == "Products Specific to NDC/UPC")
    { checkedProductWithLot = this.value }

    var pharmacyProfileCode = "";
    var MWrProfileCode = $("#MWRddllist").val();
    if (SelectionType == "GroupType") {
         pharmacyProfileCode = $("#grptype").val();
    }
    if (SelectionType == "ProfileType") {
         pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
    }


    $.ajax({
        type: "Get",
        async: false,
        url: "../Policy/ISExistPolicy",
        data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductWithLot, 'ProductNDCWithLOt': checkedProductNDC, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
        datatype: "json"

    }).done(function (data) {
        flag = true;
        str = data;

        //alert(returnstr);

    }).fail(function (response) {

    });
   // alert(str);
    //alert(str+1);
    return str;
}

//Edit Case

function CreateNDCLOTGridPanelForView() {

    var CurrentServerPageNo = $('#txtNDCLOTCurrentPageNo').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }
    CurrentServerPageNo = 0;
    var selectedText = $("#SelectedMWWRProfileType").val();
    var polid = $("#PolicyID").val();

    var selectedProfileCode = $("#SelectedMWWRProfileCode").val().toString();
    var searchTypeID = parseInt($("#PolicyType").val());
    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }



    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureForView',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,
            CurrentServerPageNo: CurrentServerPageNo,
            Policyid: polid
        }, // Note it is important// Note it is important
        success: function (data) {
            GenerateNDCLOTGridView(data);
            //$('.grid-btn-prev').show();
            //$('.grid-btn-next').show();
        },
        error: function (response) {
            alert(response);
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        }
    });
}

function GenerateNDCLOTGridView(json) {

    $("#NDCLOTjqxgridview").jqxGrid('updatebounddata');
    var data = JSON.stringify(json)
    var source =
    {
        root: "entry",
        datatype: 'json',
        localdata: data
    };
    var columnsmain = [
     { text: 'Name', datafield: 'Name' },
     { text: 'Description', datafield: 'Description' },
     { text: 'MFG Labeler Code', datafield: 'MFGLabelerCode' },
     { text: 'MFG Product Number', datafield: 'MFGProductNumber' },
     { text: 'NDC', datafield: 'NDC' },
     { text: 'NDC/UPC WithDashes', datafield: 'NDCUPCWithDashes' }
    ]
    var adapter = new $.jqx.dataAdapter(source);
    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);
        var innerID = json[index].lotDetails;

        //console.log(testttt.groupDetails[index].GroupProfileCode);
        var innergrid = JSON.stringify(json[index].lotDetails);
        var nestedSource =
         {
             datafields: [
                    { name: 'LotNumber', type: 'string' },
                    { name: 'ExpirationDate', type: 'date' }
             ],
             datatype: 'json',
             root: 'lotNumberDetails',
             localdata: innergrid
         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [

                        { text: 'Lot Number', datafield: 'LotNumber' },
                        { text: 'ExpirationDate', datafield: 'ExpirationDate', cellrenderer: ConvertDate },
        ]

        $(columnsnew).each(function (i, v) {
            if (v.datafield == 'ExpirationDate') {
                v.cellsrenderer = ConvertDate;
            }
        });

        var selectProductLotProfile = {
            "text": "Select",
            "datafield": "Select",
            "width": "100",
            "sortable": false,
            "cellsrenderer": function (row, column, value) {
                //var isGroupProfileSelected = nestedAdapter._source.localdata[row].Assigned;
                var SelectedProfileCode = innerID.lotNumberDetails[row].ProfileCode;
                var LotNumberID = innerID.lotNumberDetails[row].LotID;
                var ProductID = innerID.lotNumberDetails[row].ProductID;
                var productlot = SelectedProfileCode + "_" + ProductID + "_" + innerID.lotNumberDetails[row].LotNumber;
                // alert(JSON.stringify(dataAdapter));
                //var checkedstatus = innerID.lotNumberDetails[row].Assigned
                var checkedstatus = false;
                if (checkedstatus) {
                    var html = "<label><input type=\"checkbox\" class=\"lotchecked-class\"  disabled=\"disabled\"  value=\"" + productlot + "\"  data-id=" + productlot + " checked><i></i></label>";
                    return html;
                }
                else {
                    var html = "<label><input type=\"checkbox\" class=\"lotchecked-class\" value=\"" + productlot + "\"   data-id=" + productlot + " ><i></i></label>";
                    return html;
                }

            }
        }
       // columnsnew.push(selectProductLotProfile);

        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter,
                width: '97%',
                height: 300,
                filterable: true,
                pageable: true,
                columnsreorder: true,
                columnsresize: true,
                columns: columnsnew
            });
        }

    }
    // creage jqxgrid

    $("#NDCLOTjqxgridview").jqxGrid(
    {
        width: '100%',
        autoheight: true,
        filterable: true,
        pageable: true,
        columnsreorder: true,
        source: source,
        rowdetails: true,
        rowsheight: 35,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        ready: function () {

            $("#NDCLOTjqxgridview").jqxGrid('showrowdetails', 1);
            $('#NDCLOTjqxgridview').jqxGrid({ pagesizeoptions: ['50'] });

            var datainfo = $("#NDCLOTjqxgridview").jqxGrid('getdatainformation');
            var paginginfo = datainfo.paginginformation;
            if (datainfo.rowscount > 0) {
                $('#total-records-ndclot').text(datainfo.rowscount);
                $(".show-pagesize-cmb-ndclot-view").show();
            }
            else {
                $(".show-pagesize-cmb-ndclot-view").hide();
                $('#total-records-ndclot').text(0);
                $('#PagesizeCounterNDCLOT').val(100);

            }
        },
        columnsresize: true,
        columns: columnsmain
    });

}

function CreateNDCGridPanelForView() {

    var CurrentServerPageNo = $('#txtNDCCurrentPageNo').val();
    if (CurrentServerPageNo == '') {
        CurrentServerPageNo = 0;
    }
    CurrentServerPageNo = 0;

    //var selectedptype = document.getElementById("ddlProfileTypeForCustomEnum");
    //var selectedText = selectedptype.options[selectedptype.selectedIndex].text;
    //var searchTypeID = parseInt($("#ddlPolicyTypeEnum").val());

    var selectedText = $("#SelectedMWWRProfileType").val();
    var polid = $("#PolicyID").val();

    var selectedProfileCode = $("#SelectedMWWRProfileCode").val().toString();
    var searchTypeID = parseInt($("#PolicyType").val());


    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }
    //var selectedProfileCode = $("#MWRddllist").val().toString();

    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureForView',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,
            CurrentServerPageNo: CurrentServerPageNo,
            Policyid: polid
        }, // Note it is important
        success: function (data) {

            GenerateNDCGridView(data);

        },
        error: function (response) {
            alert(response);
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        }
    });
}

function GenerateNDCGridView(json) {

    $("#NDCjqxgridforshowpolicy").jqxGrid('updatebounddata');
    var data = JSON.stringify(json)
    var source =
    {
        root: "entry",
        datatype: 'json',
        localdata: data
    };
    var columnsmain = [
     { text: 'Name', datafield: 'Name' },
     { text: 'Type', datafield: 'Type' },
     { text: 'City', datafield: 'City' },
     { text: 'State', datafield: 'State' },
     { text: 'RegionCode', datafield: 'RegionCode' },
     { text: 'Address1', datafield: 'Address1' }

    ]
    var adapter = new $.jqx.dataAdapter(source);
    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);
        var innerID = json[index].products;

        //console.log(testttt.groupDetails[index].GroupProfileCode);
        var innergrid = JSON.stringify(json[index].products);
        var nestedSource =
         {

             datatype: 'json',
             root: 'productsDetails',
             localdata: innergrid

         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [
                        //{ text: 'Select', datafield: 'Select', columntype: 'checkbox', width: 40 },
                        { text: 'Description', datafield: 'Description' },
                        { text: 'NDC', datafield: 'NDC' },
                        { text: 'NDCUPCWithDashes', datafield: 'NDCUPCWithDashes' },
                        { text: 'MFGLabelerCode', datafield: 'MFGLabelerCode' },
                        { text: 'Strength', datafield: 'Strength' },


        ]
        var selectProductProfile = {
            "text": "Select",
            "datafield": "Select",
            "width": "100",
            "sortable": false,
            "cellsrenderer": function (row, column, value) {
                //var isGroupProfileSelected = nestedAdapter._source.localdata[row].Assigned;

                var ProductProfilecode = innerID.productsDetails[row].ProfileCode;
                //var checkedstatus = innerID.productsDetails[row].Assigned;
                var checkedstatus = false;
                var ProductID = innerID.productsDetails[row].ProductID;
                var productIDWithProfileCode = ProductProfilecode + "_" + ProductID;

                if (checkedstatus) {
                    var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  disabled=\"disabled\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " checked><i></i></label>";
                    return html;
                }
                else {
                    var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " ><i></i></label>";
                    return html;
                }

            }
        }
       // columnsnew.push(selectProductProfile);

        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter, width: '97%', height: 300,
                filterable: true,
                //editable: true,
                pageable: true,
                columnsreorder: true,
                columnsresize: true,
                columns: columnsnew
            });
        }
    }
    $("#NDCjqxgridforshowpolicy").jqxGrid(
   {
       width: '100%',
       autoheight: true,
       filterable: true,
       pageable: true,
       columnsreorder: true,
       source: source,
       rowdetails: true,
       rowsheight: 35,
       initrowdetails: initrowdetails,
       rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
       ready: function () {
           $("#NDCjqxgridforshowpolicy").jqxGrid('showrowdetails', 1);
           $('#NDCjqxgridforshowpolicy').jqxGrid({ pagesizeoptions: ['50'] });
       },
       columnsresize: true,
       columns: columnsmain
   });
}
function GetNDCUPCLOTForView() {

    CreateNDCLOTGridPanelForView();
    $("#product-grd-manufacturebasedlotNumberview").show();
    //$("#product-grd-manufacturebased").hide();
    //$("#partialProductLotListGrd").show();
    //$("#product-grd-manufacturebasedview").hide();
    //$("#partialProductListGrd").hide();
    return false;
}
function GetNDCUPCForView() {

    CreateNDCGridPanelForView();
    $("#product-grd-manufacturebasedviewviewpolicy").show();
    //$("#partialProductListGrdviewpolicy").show();
    //$("#product-grd-manufacturebasedlotNumber").hide();
    //$("#partialProductLotListGrd").hide();

    return false;
}

var editrow = -1;

function GenerateGrid1() {

    var selectedText = $("#SelectedMWWRProfileType").val();
    var polid = $("#PolicyID").val();

    var selectedProfileCode = $("#SelectedMWWRProfileCode").val();
    var searchTypeID = parseInt($("#PolicyType").val());
    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }

    var source = {

        datatype: "json",
        type: 'Get',
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureForView',
        root: 'Rows',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,
            Policyid: polid
        }, // Note it is important// Note it is important        //contentType: "application/json; charset=utf-8",
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        async: false,
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        },
        processdata: function (data) {
            //    var pagesize = $('#PagesizeCounter').val();
        data.pagesize = 100;

    },

    };
    var columnsmain = [
      { text: 'Name', datafield: 'Name' },
      { text: 'Description', datafield: 'Description' },
      { text: 'MFG Labeler Code', datafield: 'MFGLabelerCode' },
      { text: 'MFG Product Number', datafield: 'MFGProductNumber' },
      { text: 'NDC', datafield: 'NDC' },
      { text: 'NDC/UPC WithDashes', datafield: 'NDCUPCWithDashes' }
    ]
    var adapter = new $.jqx.dataAdapter(source);
    var selectProductLotProfile = {
        "text": "Select",
        "datafield": "Select",
        "width": "100",
        "sortable": false,
        "cellsrenderer": function (row, column, value) {
            //var isGroupProfileSelected = nestedAdapter._source.localdata[row].Assigned;
            var SelectedProfileCode = innerID.lotNumberDetails[row].ProfileCode;
            var LotNumberID = innerID.lotNumberDetails[row].LotID;
            var ProductID = innerID.lotNumberDetails[row].ProductID;
            var productlot = SelectedProfileCode + "_" + ProductID + "_" + innerID.lotNumberDetails[row].LotNumber;
            // alert(JSON.stringify(dataAdapter));
            //var checkedstatus = innerID.lotNumberDetails[row].Assigned
            var checkedstatus = false;
            if (checkedstatus) {
                var html = "<label><input type=\"checkbox\" class=\"lotchecked-class\"  disabled=\"disabled\"  value=\"" + productlot + "\"  data-id=" + productlot + " checked><i></i></label>";
                return html;
            }
            else {
                var html = "<label><input type=\"checkbox\" class=\"lotchecked-class\" value=\"" + productlot + "\"   data-id=" + productlot + " ><i></i></label>";
                return html;
            }

        }
    }

    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);

        var selecetdProfileCode = record.ProfileCode;
        var selecetdProductID = record.ProductID;
        var selecetdLot = record.LotNumber;
        console.log(record);
        console.log(selecetdLot);
        console.log(selecetdProductID);
        console.log(selecetdProfileCode);
        //console.log(selecetdLot);
        var nestedSource =
         {
             datatype: "json",
             type: 'Get',
             url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedNDCForViewDetails1',
             root: 'Rows',
             //data: { 'checkedManufactureprofile': selecetdProfileCode, 'Policyid': polid },
             data: { 'productID': selecetdProductID, 'lotNumber': '2', 'checkedManufactureprofile': selecetdProfileCode, 'Policyid': polid },

             //contentType: "application/json; charset=utf-8",
             filter: function () {
                 grid.jqxGrid('updatebounddata', 'filter');
             },
             // update the grid and send a request to the server.
             sort: function () {
                 grid.jqxGrid('updatebounddata', 'sort');
             },
             async: false,
             //root: 'Rows',
             beforeprocessing: function (data) {
                 nestedSource.totalrecords = data.TotalRows;
             },
             processdata: function (data) {
             //    var pagesize = $('#PagesizeCounter').val();
                 data.pagesize = 100;

             },
             loadComplete: function () {

             }
         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [
                           { text: 'Lot Number', datafield: 'LotNumber' },
                          { text: 'Expiration Date', datafield: 'ExpirationDate', cellrenderer: ConvertDate }
        ]

        $(columnsnew).each(function (i, v) {
            if (v.datafield == 'ExpirationDate') {
                v.cellsrenderer = ConvertDate;
            }
        });
       function removeByIndex(arr, index) {
            arr.splice(index, 1);
        }
        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter,
                width: '100%',
                filterable: true,
                pageable: true,
                sortable: true,
                pagermode: 'simple',
                showdefaultloadelement: false,
                autoheight: true,
                columnsreorder: true,
                columnsresize: true,
                ready: function () {
                    grid.jqxGrid('showrowdetails', 0);



                },
                virtualmode: true,
                rendergridrows: function (obj) {
                    return obj.data;
                },
                columns: columnsnew,

            });
        }

    }

    // creage jqxgrid
    $("#NDCLOTjqxgridview").jqxGrid(
    {
        width: '100%',
        autoheight: true,
        filterable: true,
        pageable: true,
        sortable: true,
        columnsreorder: true,
        source: source,
        pagermode: 'simple',
        rowdetails: true,
        rowsheight: 35,
        showdefaultloadelement: false,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 4px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        ready: function () {
            $("#NDCLOTjqxgridview").jqxGrid('showrowdetails', 0);
            $('#NDCLOTjqxgridview').jqxGrid({ pagesizeoptions: ['100'] });
        },

        virtualmode: true,
        rendergridrows: function (obj) {
            return obj.data;
        },
        columns: columnsmain
    });


}

function GenerateGridForNDCLOTView() {


    var selectedText = $("#SelectedMWWRProfileType").val();
    var polid = $("#PolicyID").val();

    var selectedProfileCode = $("#SelectedMWWRProfileCode").val().toString();
    var searchTypeID = parseInt($("#PolicyType").val());
    var searchType = "";
    if (searchTypeID == 2) {
        searchType = "NDC";
    }
    else if (searchTypeID == 3) {
        searchType = "NDCLOT";
    }

    var source = {

        datatype: "json",
        type: 'Get',
        url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureForView',
        root: 'Rows',
        data: {
            checkedManufactureprofile: selectedProfileCode,
            searchType: searchType,
            MWRType: selectedText,
            Policyid: polid
        }, // Note it is important// Note it is important        //contentType: "application/json; charset=utf-8",
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'filter');
        },
        // update the grid and send a request to the server.
        sort: function () {
            $("#jqxgrid").jqxGrid('updatebounddata', 'sort');
        },
        async: false,
        //root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data.TotalRows;
        }

    };
    var columnsmain = [
     { text: 'Name', datafield: 'Name' },
     { text: 'Type', datafield: 'Type' },
     { text: 'City', datafield: 'City' },
     { text: 'State', datafield: 'State' },
     { text: 'RegionCode', datafield: 'RegionCode' },
     { text: 'Address1', datafield: 'Address1' }

    ]
    var adapter = new $.jqx.dataAdapter(source);
    var selectProductLotProfile = {
        "text": "Select",
        "datafield": "Select",
        "width": "100",
        "sortable": false,
        "cellsrenderer": function (row, column, value) {
            //var isGroupProfileSelected = nestedAdapter._source.localdata[row].Assigned;
            var SelectedProfileCode = innerID.lotNumberDetails[row].ProfileCode;
            var LotNumberID = innerID.lotNumberDetails[row].LotID;
            var ProductID = innerID.lotNumberDetails[row].ProductID;
            var productlot = SelectedProfileCode + "_" + ProductID + "_" + innerID.lotNumberDetails[row].LotNumber;
            // alert(JSON.stringify(dataAdapter));
            //var checkedstatus = innerID.lotNumberDetails[row].Assigned
            var checkedstatus = false;
            if (checkedstatus) {
                var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  disabled=\"disabled\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " checked><i></i></label>";
                return html;
            }
            else {
                var html = "<label><input type=\"checkbox\" class=\"NDCcheckbox\"  value=\"" + productIDWithProfileCode + "\"  data-id=" + productIDWithProfileCode + " ><i></i></label>";
                return html;
            }

        }
    }

    // create nested grid.
    var initrowdetails = function (index, parentElement, gridElement, record) {
        var id = record.uid.toString();
        var grid = $($(parentElement).children()[0]);

        var selecetdProfileCode = record.ProfileCode;
        var selecetdProductID = record.productID;
        var selecetdLot = record.LotNumber;
        var nestedSource =
         {
             datatype: "json",
             type: 'Get',
             url: '../Policy/NDCLOTProductsDetailsBasedOnSelectedManufactureForViewDetails',
             root: 'Rows',
             data: { 'checkedManufactureprofile': selecetdProfileCode, 'Policyid': polid },

             //contentType: "application/json; charset=utf-8",
             filter: function () {
                 grid.jqxGrid('updatebounddata', 'filter');
             },
             // update the grid and send a request to the server.
             sort: function () {
                 grid.jqxGrid('updatebounddata', 'sort');
             },
             async: false,
             //root: 'Rows',
             beforeprocessing: function (data) {
                 nestedSource.totalrecords = data.TotalRows;
             },
             loadComplete: function () {

                 var datainfo = $("#NDCjqxgridforshowpolicy").jqxGrid('getdatainformation');

                 var paginginfo = datainfo.paginginformation;
                 if (datainfo.rowscount > 0) {
                     $('#total-records-ndclot').text(datainfo.rowscount);
                     $(".show-pagesize-cmb-ndclot-view").show();
                 }
                 else {
                     $(".show-pagesize-cmb-ndclot-view").hide();
                     $('#total-records-ndclot').text(0);
                     $('#PagesizeCounterNDCLOT').val(100);
                 }
             }
         };
        var nestedAdapter = new $.jqx.dataAdapter(nestedSource);
        var columnsnew = [
                        { text: 'Description', datafield: 'Description' },
                        { text: 'NDC', datafield: 'NDC' },
                        { text: 'NDCUPCWithDashes', datafield: 'NDCUPCWithDashes' },
                        { text: 'MFGLabelerCode', datafield: 'MFGLabelerCode' },
                        { text: 'Strength', datafield: 'Strength' },

        ]
       // columnsnew.push(selectProductLotProfile);
        function removeByIndex(arr, index) {
            arr.splice(index, 1);
        }
        if (grid != null) {
            grid.jqxGrid({
                source: nestedAdapter,
                width: '100%',
                height: 300,
                filterable: true,
                pageable: true,
                sortable: true,
                pagermode: 'simple',
                columnsreorder: true,
                columnsresize: true,
                //ready: function () {
                //    grid.jqxGrid('showrowdetails', 0);
                //    grid.bind('rowselect', function (event) {
                //        var selectedcheckedgroupProfile = $('.grpdetails-slectedprofile-classnew:checked').map(function () {
                //            return this.value;
                //        }).get();
                //        var unselectedcheckedgroupProfile = $('.grpdetails-slectedprofile-classnew:unchecked').map(function () {
                //            return this.value;
                //        }).get();

                //        var row = event.args.rowindex;

                //        var datarow = grid.jqxGrid('getrowdata', row);
                //        var prfCode = datarow.PProfileCode;
                //        var groupID = datarow.ProfileGroupID;
                //        var unselecteddata = unselectedcheckedgroupProfile.toString().split(',');
                //        if (unselecteddata != null) {
                //            for (var i = 0; i < unselecteddata.length; i++) {
                //                var unselecetgrp = unselecteddata[i];
                //                var index = data.indexOf(unselecetgrp);
                //                if (index != -1) {
                //                    removeByIndex(data, index)

                //                }
                //            }
                //        }
                //        var selecteddata = selectedcheckedgroupProfile.toString().split(',');

                //        if (selecteddata != null) {
                //            for (var i = 0; i < selecteddata.length; i++) {
                //                var grp = selecteddata[i];
                //                var index = data.indexOf(grp);
                //                if (index == -1 && grp != "") {

                //                    data[icounter] = grp;
                //                    ++icounter;
                //                }
                //            }
                //        }
                //        console.log()

                //    });


                //},
                virtualmode: true,
                rendergridrows: function (obj) {
                    return obj.data;
                },
                columns: columnsnew,

            });
        }

    }

    // creage jqxgrid
    $("#NDCjqxgridforshowpolicy").jqxGrid(
    {
        width: '100%',
        autoheight: true,
        filterable: true,
        pageable: true,
        sortable: true,
        columnsreorder: true,
        source: source,
        pagermode: 'simple',
        rowdetails: true,
        rowsheight: 35,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 4px;'></div>", rowdetailsheight: 320, rowdetailshidden: true },
        ready: function () {

            $("#NDCjqxgridforshowpolicy").jqxGrid('showrowdetails', 0);
        },

        virtualmode: true,
        rendergridrows: function (obj) {
            return obj.data;
        },
        columns: columnsmain
    });


}

function NDCPolicyValidation(checkedProductNDCdet, grid, row) {


        var url = window.location.href;
        if (url.indexOf('ViewEditPolicy') > -1) {
            var checkedProductWithLot = "";
         var checkedProductNDC = "";

            var SelectionType = $("#ProfileGroupSelectionType").val();
            //SelectionType = 'ProfileType';
            var selectedText = $("#SelectedPolicyType").val();
            var pharmacyProfileCode = "";
            if (selectedText == 'Customer Specific') {
                if (SelectionType == "GroupType") {
                    pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                }
                if (SelectionType == "ProfileType") {
                    pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
                }
            }


            var selectedddlPolicyTypeText = $("#PolicyTypeText").val();
            if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC") {
                checkedProductNDC = checkedProductNDCdet
                if (checkedProductNDC.length > 0) { } else {
                    $("#alertmessageid").text("Please select NDC/UPC");
                    $(".alert-popup-body-message").show();
                }
            }
            var MWrProfileCode = $("#SelectedMWWRProfileCode").val().toString();
            //End



            if (selectedText == "Customer Specific") {
                if (pharmacyProfileCode.length > 0) {
                }
                else {
                    grid.jqxGrid('unselectrow', row);

                    $("#alertmessageid").text("Please Select pharmacy details");
                    $(".alert-popup-body-message").show();

                    return false;
                }
            }
            //var checkboxid = this.value;
            $.ajax({
                type: "Get",

                url: "../Policy/ISExistPolicy",
                data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                datatype: "json"

            }).done(function (data) {

                grid.jqxGrid('unselectrow', row);
                $("#alertmessageid").text(data);
                $(".alert-popup-body-message").show();
                //$("input[value=" + checkboxid + "]").removeAttr('checked');

            }).fail(function (response) {

            });
        }
        else {
            var name = document.getElementById("ddlPolicyNameEnum");
            var selectedText = name.options[name.selectedIndex].text;

            var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
            var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
            var checkedProductWithLot = "";
            var checkedProductNDC = "";
        if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC") {
                checkedProductNDC = checkedProductNDCdet
            }


            var MWrProfileCode = $("#MWRddllist").val();
            var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();

            var SelectionType = $('input[name=select-type]:checked').val();
            if (SelectionType == "GroupType") {
                var pharmacyProfileCode = $("#grptype").val();
            }
            if (SelectionType == "ProfileType") {
                var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
            }
            if (selectedText == "Customer Specific") {
                if (pharmacyProfileCode.length > 0)
                { }
                else
                {
                    grid.jqxGrid('unselectrow', row);
                    $("#alertmessageid").text("Please Select pharmacy details");
                    $(".alert-popup-body-message").show();

                    return false;
                }
            }
            //var checkboxid = this.value;
            $.ajax({
                type: "Get",

                url: "../Policy/ISExistPolicy",
                data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                datatype: "json"

            }).done(function (data) {

                //$("input[value=" + checkboxid + "]").removeAttr('checked');

                grid.jqxGrid('unselectrow', row);
                $("#alertmessageid").text(data);
                $(".alert-popup-body-message").show();


                return false;

            }).fail(function (response) {
                //$('#NDCjqxgrid').jqxGrid('unselectrow', row);
            });

            //this.checked = false;
            // return false;
        }
   // }

}

function NDCLOTPOlicyValidation(checkedProductNDCLOTdet, grid, row) {

        var url = window.location.href;
        if (url.indexOf('ViewEditPolicy') > -1) {
            var checkedProductWithLot = "";
            var checkedProductNDC = "";

            var SelectionType = $("#ProfileGroupSelectionType").val();
            //SelectionType = 'ProfileType';
            if (SelectionType == "GroupType") {
                var pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
            }
            if (SelectionType == "ProfileType") {
                var pharmacyProfileCode = $("#SelectedPharmacyGroupProfileCode").val();
            }
            var selectedText = $("#SelectedPolicyType").val();
            var selectedddlPolicyTypeText = $("#PolicyTypeText").val();
            if (selectedddlPolicyTypeText = "Products Specific to NDC/UPC and Lot Number")
            { checkedProductWithLot = checkedProductNDCLOTdet }
            var MWrProfileCode = $("#SelectedMWWRProfileCode").val().toString();
            //End



            if (selectedText == "Customer Specific") {
                if (pharmacyProfileCode.length > 0)
                { }
                else
                {
                    $("#alertmessageid").text("Please Select pharmacy details");
                    $(".alert-popup-body-message").show();
                    return false;
                }
            }
            //var checkboxid = this.value;
            $.ajax({
                type: "Get",
                url: "../Policy/ISExistPolicy",
                data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                datatype: "json"

            }).done(function (data) {

                grid.jqxGrid('unselectrow', row);
                $("#alertmessageid").text(data);
                $(".alert-popup-body-message").show();
            }).fail(function (response) {

            });
        }
        else {
            var name = document.getElementById("ddlPolicyNameEnum");
            var selectedText = name.options[name.selectedIndex].text;

            var ddlPolicyTypetext = document.getElementById("ddlPolicyTypeEnum");
            var selectedddlPolicyTypeText = ddlPolicyTypetext.options[ddlPolicyTypetext.selectedIndex].text;
            var checkedProductWithLot = "";
            var checkedProductNDC = "";
            if (selectedddlPolicyTypeText == "Products Specific to NDC/UPC and Lot Number")
            { checkedProductWithLot = checkedProductNDCLOTdet }


            var MWrProfileCode = $("#MWRddllist").val();
            //var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
            var SelectionType = $('input[name=select-type]:checked').val();
            if (SelectionType == "GroupType") {
                var pharmacyProfileCode = $("#grptype").val();
            }
            if (SelectionType == "ProfileType") {
                var pharmacyProfileCode = $("#selectedpharmacyProfileCode").val();
            }

            if (selectedText == "Customer Specific") {
                if (pharmacyProfileCode.length > 0)
                { }
                else
                {
                    $("#alertmessageid").text("Please Select pharmacy details");
                    $(".alert-popup-body-message").show();
                    grid.jqxGrid('unselectrow', row);
                    return false;
                }
            }
            //var checkboxid = this.value;
            $.ajax({
                type: "Get",
                url: "../Policy/ISExistPolicy",
                data: { 'WMRProfileCode': MWrProfileCode, 'Pharmacy': pharmacyProfileCode.toString(), 'ProductIDNDC': checkedProductNDC, 'ProductNDCWithLOt': checkedProductWithLot, 'PolicyNamePublishedCustom': selectedText, 'policyTYpe': selectedddlPolicyTypeText.toString(), 'SelectionType': SelectionType },
                datatype: "json"

            }).done(function (data) {

                grid.jqxGrid('unselectrow', row);
                $("#alertmessageid").text(data);
                $(".alert-popup-body-message").show();
            }).fail(function (response) {

            });
        }

}


