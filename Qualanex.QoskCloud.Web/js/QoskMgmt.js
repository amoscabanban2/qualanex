//*****************************************************************************
//*
//* QoskMgmt.js
//*    Copyright (c) 2017, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   var groupId;
   var table;

   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------


   //#############################################################################
   // Global functions
   //#############################################################################


   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      console.log("QoskMgmt::ready()");

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      $("input").attr("autocomplete", "off");

      // ...
      ///////////////////////////////////////////////////////////////////////////////
      window.dtHeaders(".QoskSummary", '"order": [[1, "asc"]]', [], []);

   });

   //#############################################################################
   //#   Summary event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(".QoskSummary").click(function (e)
   {
      console.log("QoskMgmt::QoskSummary.click()");

      ;
      ;
      ;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("click", ".MgmtEntity", function (e)
   {
      console.log("QoskMgmt::MgmtEntity.click()");
      if (getEditMode())
      {
         return false;
      }

      var entity = $(this).closest("tr");

      // Is the entity already selected?
      if (!entity.hasClass("selected"))
      {
         // switch the selection from the old entity to this one
         ///////////////////////////////////////////////////////////////////////////////
         $(".MgmtEntity.selected").removeClass("selected");
         entity.addClass("selected");
         var entityList = JSON.parse(entity.attr("MgmtEntity-data").split("=")[1]);
         //.replace("entity=", "")
         // For each field in the data (i.e. model), call the handler to update the
         // field on the form.
         ///////////////////////////////////////////////////////////////////////////////
         doDataExchange(false, entityList);
         //(entity.attr("MgmtEntity-data").match(/{(.*)}/))[1].split(/,/));

         // Finally, send the event to Google Analytics
         ///////////////////////////////////////////////////////////////////////////////
         ga("send", "event", "Selection", "click", entity.attr("MgmtEntity-id"));
      }
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle when a Account's detail is selected form the table.
   //*
   //* Parameters:
   //*   e - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   $(document).on("click", ".entity-div-click", function (e)
   {

      //userID = $(this).closest("tr").attr("data-row-id");         // For keep row selection when refresh request

      //if ($("#CMD_NEW").attr("src").indexOf("(0)") > 0 || $("#CMD_EDIT").attr("src").indexOf("(0)") > 0) {   // On Edit /Add mode no selection allow
      //   var title = "Do you want to save the change?";
      //   setMessagBox(title, "Continue", "", "", "Save", "Discard");

      //   $("#confirmAlert").show();

      //   // Error message "Discard" button action
      //   $("#btdiscard,.alert-close").click(function () {
      //      $("#confirmAlert").hide();

      //      if ($("#searchGrid tbody").find("tr:first").hasClass("selected") && editMode === false) {
      //         $("#searchGrid tbody").find("tr:first").remove();
      //      }

      //      actionBar_Initial();

      //      //$("#CMD_NEW").attr("src").replace("(0)", "(1)");
      //      //$("#CMD_EDIT").attr("src").replace("(0)", "(1)");

      //      showFormBody($("#MgmtSummary"), userID);
      //      userID = null;
      //      return;
      //   });

      //   // Error message "Save" button action
      //   $("#btsave").click(function () {
      //      $("#btsave").unbind("click");
      //      $("#confirmAlert").hide();
      //      $("#CMD_SAVE").click();

      //      return;
      //   });

      //   // Error message "Continue" button action
      //   $("#btcontinue").click(function () {
      //       setEditMode_UserName(false, editMode);
      //      $("#btcontinue").unbind("click");
      //      $("#confirmAlert").hide();

      //      return;
      //   });
      //   return true;
      //};

      //getEditMode();
      //if (!getEditMode())
      //{
      //   userID = $(this).closest("tr").attr("data-row-id");         // For keep row selection when refresh request
      //   showFormBody($("#MgmtSummary"), userID);
      //   ga("send", "event", "Selection", "click", $(this).attr("name") || this.id);
      //   actionBar_Initial();                                      //Disable Save & Cancel after Row click and Enable New & Edit
      //}

      //return false;
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   saveAndvalidate - Description not available.
   //*   data            - Description not available.
   //*
   //* Returns:
   //*   Description not available.
   //*
   //*****************************************************************************
   function doDataExchange(saveAndvalidate, data)
   {
      console.log("QoskMgmt::doDataExchange(" + saveAndvalidate + ", " + data + ")");

      $.each(data, function (index, value)
      {
         var entityData = value;

         window.UpdateData(saveAndvalidate, index, value);
      });
   }

});
