//*****************************************************************************
//*
//* QoskWorksheet.js
//*    Copyright (c) 2018, Qualanex LLC.
//*    All rights reserved.
//*
//* Summary:
//*    Summary not available.
//*
//* Notes:
//*    None.
//*
//*****************************************************************************

$(function ()
{
   //=============================================================================
   // Global definitions
   //-----------------------------------------------------------------------------

   ;
   ;
   ;

   //#############################################################################
   // Local functions
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   actionTabID = ???.
   //*   count       = ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var getActionTab = function (actionTabID)
   {
      return window.$("#tab-container a[href=#" + actionTabID + "]");
   };

   //#############################################################################
   // Global functions
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   actionTabID = ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var selectActionTab = function (actionTabID)
   {
      var actionTab = getActionTab(actionTabID);

      if (actionTab !== "undefined")
      {
         actionTab.click();
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.selectActionTab = selectActionTab;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   actionTabID = ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var getSelectedActionTab = function ()
   {
      try
      {
         return (window.$("#tab-container a.active").attr("href").substr(1));
      }
      catch (err)
      {
      }

      return "";
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.getSelectedActionTab = getSelectedActionTab;


   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   actionTabID = ???.
   //*   count       = ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var setActionTabCount = function (actionTabID, count)
   {
      var actionTab = getActionTab(actionTabID);

      if (actionTab !== "undefined")
      {
         var actionCount = actionTab.find(".TabCount:first");

         if (actionCount !== "undefined")
         {
            actionCount.text(count);
            actionCount.css("display", count > 0 ? "inline-block" : "none");
         }
      }
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.setActionTabCount = setActionTabCount;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   actionTabID = ???.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var isActionTabSelected = function (actionTabID)
   {
      return (window.$("#tab-container a.active").attr("href") === ("#" + actionTabID));
   }
   ///////////////////////////////////////////////////////////////////////////////
   window.isActionTabSelected = isActionTabSelected;

   //#############################################################################
   // Global event handlers
   //#############################################################################

   //*****************************************************************************
   //*
   //* Summary:
   //*   Summary not available.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).ready(function ()
   {
      console.log("QoskWorksheet::ready()");

      ;
      ;
      ;
   });

});
