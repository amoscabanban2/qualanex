$(function ()
{
   //*****************************************************************************
   //*
   //* QoskSegment.LotExp.js
   //*    Copyright (c) 2016 - 2017, Qualanex LLC.
   //*    All rights reserved.
   //*
   //* Summary:
   //*    Javascript functionality for lot and expiration segment.
   //*
   //* Notes:
   //*    None.
   //*
   //*****************************************************************************

   //=============================================================================
   // Local definitions
   //-----------------------------------------------------------------------------

   var expNa = false;
   var segmentId = "lotDate";
   var segmentSelector = "#" + segmentId;
   var errorSpanSelector = segmentSelector + " .QnexMessage .error";
   var warningSpanSelector = segmentSelector + " .QnexMessage .warning";
   var awaitingLotCheck = false;
   var awaitingContinue = false;
   var recallNaLot = false;
   const appCodeDss = "PHARMA_DECISION";

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Lot NA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnNoLot", function ()
   {
      if ($("#divLotNo").css("display") !== "none")
      {
         // Reset the current value to empty
         setLotNa();
         $("#divLotNoRO").show().siblings("div").hide();
         $("#tornLabel").show();
      }
      isMissingLotNumber();
      checkNaLotForRecall();
      ga('send', 'event', 'LotNumber', 'NA', 'NAButton');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Expiration to NA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnNoExp", function ()
   {
      if ($("#divExpDate").css("display") !== "none")
      {

         $("#divExpDateRO").show().siblings("div").hide();
         setExpNa();
         $("#tornLabel").show();
      }
      ga('send', 'event', 'ExpirationDate', 'NA', 'NAButton');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Expiration to NA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setExpNa()
   {
      expNa = true;
      $(".cboExpMonth").val("");
      $(".txtExpYear").val("");
      $(".ExpMonthHidden").val("");
      $(".ExpYearHidden").val("");
      updateExpSelection();
   }

   window.setExpNa = setExpNa;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Set Lot to NA.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function setLotNa()
   {
      if ($(".txtLotNo").length && !$(".cboExpMonth").attr("disabled") && $("#divLotNo").css("display") !== "none")
      {
         $(".txtLotNo").val("");
         $(".LotNumberHidden").val("");
         $(".txtLotNo").attr("placeholder", "Enter Lot No");
         $("#LotSelection").html(' - N/A');
         isMissingLotNumber();
         $("#lotNumberNotFoundWarning").hide();
      }
   }

   window.setLotNa = setLotNa;


   //*****************************************************************************
   //*
   //* Summary:
   //*   Validate and update segment header when expiration value is updated
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function updateExpSelection()
   {
      if ($(".cboExpMonth").length && $(".txtExpYear").length && $(".cboExpMonth")[0].value.trim() !== '' && $(".txtExpYear")[0].value.trim() !== '')
      {
         $("#ExpSelection").html(" - " + $(".cboExpMonth")[0].value + "/" + $(".txtExpYear")[0].value);
         $("#expDateNotFoundWarning").hide();
      } else
      {

         $("#ExpSelection").html(' - N/A');
      }

      $(".ExpMonthHidden").val($(".cboExpMonth").val());
      $(".ExpYearHidden").val($(".txtExpYear").val());
      isMissingExpirationDate();
      isDateOutOfRangeError();
      isDateOutOfRangeWarning();
   }

   window.updateExpSelection = updateExpSelection;

   //*****************************************************************************
   //*
   //* Summary:
   //*   Verify if lot number is present.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   true when lot number is present
   //*
   //*****************************************************************************
   function isMissingLotNumber()
   {
      if (!$(".txtLotNo").length || recallNaLot)
      {
         return false;
      }

      var lot = $(".txtLotNo").val();

      if ($("#LotSelection").html() !== " - N/A" && jQuery.trim(lot).length === 0)
      {
         return true;
      }

      $("#missingLotNumberError").hide();
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Verify if expiration date is present.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   true when expiration date is present
   //*
   //*****************************************************************************
   function isMissingExpirationDate()
   {
      if (!$(".txtExpYear").length)
      {
         return false;
      }
      var year = $(".txtExpYear").val();
      var month = jQuery.trim($(".cboExpMonth").val());
      if (!expNa && (year === "" || month === ""))
      {
         return true;
      }
      $("#missingExpirationDateError").hide();
      $("#expDateNotFoundWarning").hide();
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Verify if expiration is within the acceptable range.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   true expiration is within accepted range
   //*
   //*****************************************************************************
   function isDateOutOfRangeError()
   {
      if (!$(".txtExpYear").length)
      {
         return false;
      }
      var year = $(".txtExpYear").val();
      var currentYear = new Date().getFullYear();
      if (!expNa && (year > currentYear + 50 || year < currentYear - 50) && year !== "")
      {
         return true;
      }
      $("#dateOutOfRangeError").hide();
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   Verify if expiration is within the warning range.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   true expiration is within warning range
   //*
   //*****************************************************************************
   function isDateOutOfRangeWarning()
   {
      if (!$(".txtExpYear").length)
      {
         return false;
      }
      var year = $(".txtExpYear").val();
      var currentYear = new Date().getFullYear();
      if (!expNa && (year > currentYear + 25 || year < currentYear - 25) && year !== "")
      {
         $("#dateOutOfRangeWarning").attr("data-display-time", new Date().valueOf());
         $("#dateOutOfRangeWarning").show();
         return true;
      }
      $("#dateOutOfRangeWarning").hide();
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   If No Lot button is pressed, determine if the NA lot is part of a recall
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   NA lot found in RecallProductRel table
   //*
   //*****************************************************************************
   function checkNaLotForRecall()
   {
      // stop db call to look up lot exp date for Green Bin processing
      if ($("#GreenBinFormBody").length || $(".QoskBody").attr("AppCode") === appCodeDss)
      {
         $(".cboExpMonth").focus();
         return false;
      }

      awaitingLotCheck = true;
      var productId = $("#productIdLotDate").val();
      var debitMemo = window.raInformation.DebitMemo.DebitMemoNumber;
      var options = {
         async: true,
         url: "/LotNumber/GetNaLotRecallId",
         type: "POST",
         data: "productId=" + productId + "&debitMemoNumber=" + debitMemo + getAntiForgeryToken(),
         success: function (data)
         {
            awaitingLotCheck = false;
            
            if (data.length > 0)
            {
               $(".noLotNo").val(data);
               $(".LotNumberHidden").val(data);
               $("#LotSelection").html(" - " + $(".noLotNo").val());
               recallNaLot = true;

               setTimeout(function ()
               {
                  $("#btnNoExp").click();
               }, 150);
            }
         },
         error: function (err)
         {
            awaitingLotCheck = false;
            recallNaLot = false;
         }
      };

      $.ajax(options);
      ga('send', 'event', 'LotNumber', 'NumberEntered', 'LotText');
      return false;
   }


   //*****************************************************************************
   //*
   //* Summary:
   //*   validates the input values set in the lot and expiration segments.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   true when all values are set and acceptable
   //*
   //*****************************************************************************
   function validateLotExp()
   {
      console.log("validating");
      $(errorSpanSelector).each(function ()
      {
         $(this).hide();
      });

      if (isMissingLotNumber())
      {
         $("#lotNumberNotFoundWarning").hide();
         $("#missingLotNumberError").show();
         $(".txtLotNo").focus().select();
         return failValidation(segmentId);
      }

      if (isMissingExpirationDate())
      {
         $("#lotNumberNotFoundWarning").hide();
         $("#missingExpirationDateError").show();
         $(".txtLotNo").focus().select();
         return failValidation(segmentId);
      }

      if (isDateOutOfRangeError())
      {
         $("#dateOutOfRangeError").show();
         return failValidation(segmentId);
      }

      var newWarning = false;
      $(warningSpanSelector + ":visible").each(function ()
      {
         var displayDate = parseFloat($(this).attr("data-display-time")) + 250;
         var currentDate = new Date().valueOf();

         if (displayDate > currentDate)
         {
            newWarning = true;
         }
      });

      if (newWarning)
      {
         return false;
      }

      $(segmentSelector).attr("data-segment-complete", "true");
      removeSegmentIncompleteMessage(segmentId);
      return true;
   }
   window.validateLotExp = validateLotExp;


   //*****************************************************************************
   //*
   //* Summary:
   //*   When return key is pressed on the lot number field, if database returns
   //*   with a valid date, set flags to automatically continue with next segment
   //*   after server returns.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   var checkForLotAwait = function (e)
   {
      if (awaitingLotCheck)
      {
         awaitingContinue = true;
         e.stopPropagation();
      }
   }
   window.checkForLotAwait = checkForLotAwait;

   //*****************************************************************************
   //*
   //* Summary:
   //*   When lot number is modified, query the server to see if a date record 
   //*   is available
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   function checkLotForExpDate()
   {
      $(".txtLotNo").val($(".txtLotNo").val().toUpperCase());
      $(".LotNumberHidden").val($(".txtLotNo").val());
      isMissingLotNumber();
      $("#LotSelection").html(" - " + $(".txtLotNo").val());

      if ($(".txtLotNo").val().trim() === "")
      {
         $("#lotNumberNotFoundWarning").hide();
         return false;
      }

      // stop db call to look up lot exp date for Green Bin processing
      var $isGrnBin = $("#GreenBinFormBody");

      if ($isGrnBin.length)
      {
         $(".cboExpMonth").focus();
         return false;
      }

      awaitingLotCheck = true;
      var productId = $("#productIdLotDate").val();
      var lotNumber = $(".txtLotNo").val();
      var options = {
         async: true,
         url: "/LotNumber/GetLotExpiration",
         type: "POST",
         data: "productId=" + productId + "&lotNumber=" + lotNumber + getAntiForgeryToken(),
         success: function (data)
         {
            awaitingLotCheck = false;
            expNa = false;
            $("#lotNumberNotFoundWarning").hide();

            if ($(".txtExpYear").length)
            {
               if ($("#divExpDateRO").css("display") !== "none")
               {
                  $("#divExpDate").show().siblings("div").hide();
               }
            }
            if (data.ExpirationDate === null)
            {
               $("#lotNumberNotFoundWarning").attr("data-display-time", new Date().valueOf());

               if (!$(".txtExpYear").length || ($(".txtExpYear").val() === "" || $(".cboExpMonth").val() === ""))
               {
                  $("#expDateNotFoundWarning").show();
               }

               if (awaitingContinue)
               {
                  awaitingContinue = false;
                  $("#lotContinue").click();
               }

               return;
            }

            if ($(".txtExpYear").length)
            {
               var expiration = new Date(parseInt(data.ExpirationDate.substr(6)));
               expiration.setTime(expiration.getTime() + (6 * 60 * 60 * 1000));
               $(".cboExpMonth").val(expiration.getMonth() + 1);
               $(".txtExpYear").val(expiration.getFullYear());
               $(".ExpMonthHidden").val($(".cboExpMonth").val());
               $(".ExpYearHidden").val($(".txtExpYear").val());
               updateExpSelection();
            }

            $("#expDateNotFoundWarning").hide();

            if (awaitingContinue)
            {
               awaitingContinue = false;
               $("#lotContinue").click();
            }
         },
         error: function (err)
         {
            awaitingLotCheck = false;
            awaitingContinue = false;

            if ($(".IsContractedMfgHidden").val() === "False")
            {
               $("#lotNumberNotFoundWarning").attr("data-display-time", new Date().valueOf());

               if (!$(".txtExpYear").length || ($(".txtExpYear").val() === "" || $(".cboExpMonth").val() === ""))
               {
                  $("#expDateNotFoundWarning").show();
               }

               return;
            }
            else
            {
               $("#lotNumberNotFoundWarning").attr("data-display-time", new Date().valueOf());
               $("#lotNumberNotFoundWarning").show();
               $(".txtLotNo").select();
            }
         }
      };

      $.ajax(options);
      ga('send', 'event', 'LotNumber', 'NumberEntered', 'LotText');
      return false;
   }

   //*****************************************************************************
   //*
   //* Summary:
   //*   When year is changed, ensure the slick slider matches the selected value
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".txtExpYear", function ()
   {
      //if a 2 digit year value is entered, prepend the 20
      if ($(".txtExpYear").val().length === 2)
      {
         $(".txtExpYear").val("20" + $(".txtExpYear").val());
      }

      var $yearButton = $("#lotDate .yearKey[value='" + $(".txtExpYear").val() + "']");
      if ($yearButton.length)
      {
         //find the index of the year in the scroller
         var index = $yearButton.parent().attr("data-slick-index");
         //scroll to the inedex
         $(".scroll-year").slick("slickGoTo", index);
      }

      updateExpSelection();
      ga('send', 'event', 'ExpirationDate', 'YearSelect', 'txt');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Runs character validation on year keypress
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".txtExpYear", function (e)
   {
      validateKey(ECS_YEAR, e);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Enforces 20 character lot limit
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", ".txtLotNo", function (e)
   {
      validateKey(ECS_LOTNO, e);
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle return key being clicked within the segment
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("keypress", segmentSelector, function (e)
   {
      var theEvent = e || window.event;
      var key = theEvent.keyCode || theEvent.which;
      if (key === 13)
      {
         if ($(".txtLotNo").is(":focus"))
         {
            checkLotForExpDate();
            if ($(".txtExpYear").is(":focus"))
            {
               $(".txtExpYear").change();
            }
            setTimeout(function ()
            {
               $("#lotContinue").focus();
               $("#lotContinue").click();
            },
               200);
         } else if ($(".txtExpYear").is(":focus"))
         {
            isDateOutOfRangeWarning();
            $("#lotContinue").focus();
            $("#lotContinue").click();
         } else
         {
            $("#lotContinue").focus();
            $("#lotContinue").click();

         }
         //submitPolicy(e, "#lotContinue");
      }
   });

   $(document).on("change", ".txtLotNo", checkLotForExpDate);

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle reset click.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnReset", function ()
   {
      $(errorSpanSelector).each(function ()
      {
         $(this).hide();
      });
      $(warningSpanSelector).each(function ()
      {
         $(this).hide();
      });

      expNa = false;
      recallNaLot = false;

      var returnValidate = $(this).attr("data-field-value").toLowerCase() === "false"
         ? false
         : true;
      if (!$(".cboExpMonth").attr("disabled"))
      {
         var dates = $("#ExpSelectionInput").val();
         $(".cboExpMonth").val(!returnValidate
            ? ""
            : dates.length > 0
               ? dates.split("/")[0]
               : "");
         $(".txtExpYear").val(!returnValidate
            ? ""
            : dates.length > 0
               ? dates.split("/")[1]
               : "");
         $(".ExpMonthHidden").val($(".cboExpMonth").val());
         $(".ExpYearHidden").val($(".txtExpYear").val());

         if ($("#divExpDateRO").css("display") !== "none")
         {
            $("#divExpDate").show().siblings("div").hide();
         }

         $(".scroll-year").slick("slickGoTo", 28);

         $("#ExpSelection").html(!returnValidate
            ? ""
            : dates.length > 0
               ? "- " + dates
               : "");
      }

      if (!$(".txtLotNo").attr("disabled"))
      {
         $(".txtLotNo").val(!returnValidate
            ? ""
            : $("#LotSelectionInput").val().trim());

         $(".LotNumberHidden").val(!returnValidate
            ? ""
            : $("#LotSelectionInput").val().trim());

         if ($("#divLotNoRO").css("display") !== "none")
         {
            $("#divLotNo").show().siblings("div").hide();
            $("#divLotNoRO .noLotNo").val("");
         }

         $(".txtLotNo").focus();
         $("#LotSelection").html(!returnValidate
            ? ""
            : $("#LotSelectionInput").val().trim().length > 0
               ? "- " + $("#LotSelectionInput").val().trim()
               : "");
         $("#lotDate .QnexMessage table tbody tr td table tbody tr").css("display", "none");
      }

      //the click event is here so we de-select the Torn/Can't Read Label button, which clears it from the container condition hidden field
      if ($('#CNTRCDTN_TORN_LBL').is(":checked") === true)
      {
         $("#tornLabel").click();
      }

      //the click event is here so we de-select the Prescription button, which clears it from the container condition hidden field
      if ($('#CNTRCDTN_PRSCRPTN_LBL').is(":checked") === true)
      {
         $("#prescriptionLabel").click();
      }

      setTimeout(function ()
      {
         $("#tornLabel").hide();
      }, 200);

      ga('send', 'event', 'Lot', 'Reset', 'ResetButton');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle click on a year button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".yearKey", function ()
   {
      if ($("#divExpDate").css("display") !== "none" && !$(".cboExpMonth").attr("disabled"))
      {
         $(".txtExpYear").val($(this).val());
         updateExpSelection();
      }
      ga('send', 'event', 'ExpirationDate', 'YearSelect', $(this).val());
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   handle click on a month button.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", ".monthKey", function ()
   {
      if ($("#divExpDate").css("display") !== "none" && !$(".cboExpMonth").attr("disabled"))
      {
         $(".cboExpMonth").val($(this).attr("data-value"));
         updateExpSelection();
      }
      ga('send', 'event', 'ExpirationDate', 'MonthSelect', $(this).attr("data-ql-value"));
   });


   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the previous page of year selections
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnPrevYear", function ()
   {
      $(".scroll-year").slick("slickPrev");
      ga('send', 'event', 'ExpirationDate', 'YearScroll', 'Prev');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Scroll to the next page of year selections.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("click", "#btnNextYear", function ()
   {
      $(".scroll-year").slick("slickNext");
      ga('send', 'event', 'ExpirationDate', 'YearScroll', 'Next');
   });

   //*****************************************************************************
   //*
   //* Summary:
   //*   Handle month selection.
   //*
   //* Parameters:
   //*   None.
   //*
   //* Returns:
   //*   None.
   //*
   //*****************************************************************************
   $(document).on("change", ".cboExpMonth", function (e)
   {
      updateExpSelection();
      ga('send', 'event', 'ExpirationDate', 'MonthSelect', 'DropDown');
   });

   function lotSegmentLoad()
   {
      $(".txtLotNo").focus();
   }
   window.createAndExclusivePush("lotDateExpandMethods", lotSegmentLoad);
});