﻿var Product = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>",
    Delete: "This record has been deleted by another user",
    DeleteFailed: "Failed to delete Product",
    SaveSuccess: "Saved successfully",
    SelectDivest: "Please select 'Yes' option to proceed into divested screen",
    UpdateSuccess: "Updated successfully",
    UpdateFails: "Update failed",
    SaveFails: "Please input correct data",
    WholesalerNumberExist: "Wholesaler number already exist for this product",
    DeleteConfirmation:"Do you wish to delete this Wholesaler Number?",
    SelectDivestedLot: "Right panel can't be empty please move lot number from left to right panel",
    MoveLotRigtToLeft: "Please move lot from right to left in case of All Lot divested",
    SelectAllLotOption: "Please select all lots option from divested type.",
    SelectDivestedManufacturer: "Select manufacturer name",
    SelectLeftLot: "Please select lot number from left panel list",
    SelectRightLot: "Please select lot number from right panel list",
    CreatePublishedPolicy: "Please first create published policy for selected manufacturer and then divest the lot number",
    SelectOtherManufacture: "Original manufacturer profile and divested manufacturer profile can't be same . Please select different divested manufacturer profile from original manufacturer",
}
var Policy = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>",
    Delete: "This record has been deleted by another user",
    DeleteSuccess: "Policy Deleted",
    DeleteFailed: "Failed to delete Policy",
    DeleteConfirmation: "Do you wish to delete this Policy?",

    SelectPolicy: "Please select policy",
    SelectProfilegroupType: "Please select type like Profile Type and Group Type",
    SelectPharmacyType: "Please select Pharmacy Type",
    SelectPharmacyGroupType: "Please select Pharmacy Group Type",
    SelectPharmacy: "Please select pharmacy",
    SelectPolicyType: "Please select policy type",
    SelectPharmacyProfileType: "Please select profile type",
    SelectProfileName: "Please select profile name",
    SelectNDCUPC: "Please select NDC/UPC",
    SelectNDCUPCLOT: "Please select NDC/UPC and LOT Number",
    RestrictToCreatePolicyforWholesaler: "Can not create the policy for NDC/UPC/LOT in case of Wholesaler and WholesalerGroup",
    SelectPolicystartdate: "Please Select Policy Start Date",
    SelectPolicyenddate: "Please Select Policy End Date"


}
var LotNumber = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>", Delete: "This record has been deleted by another user",
    DeleteSuccess: "Lot Number Deleted",
    DeleteFailed: "Failed to delete Lot Number",
    LotSucess:"Lot number is created Successfully."
}
    
var User = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>", Delete: "This record has been deleted by another user",
    DeleteSuccess: "User Deleted.",
    DeleteFailed: "Failed to delete User.",
    UserCreated: "User created successfully.",
    UserCreationFail:"Failed to create user."

}

var Profile = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>", Delete: "This record has been deleted by another user",
    DeleteSuccess: "Profile Deleted",
    DeleteFailed: "Failed to delete Profile",
    DeleteConfirmation: "Do you wish to delete this profile?",
    
}

var Common =
    {
        SelectLeft: "Please select column from left panel list",
        SelectRight: "Please select column from right panel list",
    }

var Group = 
    {
        AlertDeleteMsg: "Do you wish to delete this Group?",
        DeleteSuccess: "Group Deleted."
    }

var Report = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>", Delete: "This record has been deleted by another user",
    DeleteSuccess: "Report Deleted",
    DeleteFailed: "Failed to delete Report",
    DeleteConfirmation: "Do you wish to delete this Report?",

}