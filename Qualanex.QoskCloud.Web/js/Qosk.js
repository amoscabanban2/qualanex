﻿$(document).ready(function () {
    $("#Profile").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '../Qosk/GetProfile',
                data: { terms: request.term },
                dataType: 'json',
                type: 'GET',
                //cache: false,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Text,
                            value: item.Value
                        }
                    }));
                }
            });
        },
        focus: function (event, ui) {
            $("#Profile").val(ui.item.label);

        },
        select: function (event, ui) {

            $('#Profile').val(ui.item.label);
            $('#hdnProfile').val(ui.item.value);
            return false;
        }
    })
});