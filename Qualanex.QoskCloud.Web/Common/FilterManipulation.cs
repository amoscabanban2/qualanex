﻿using Qualanex.QoskCloud.Entity;
using System;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Web
{
    public class FilterManipulation
    {
        List<List<ControlViewModel>> _objLstofFilterCriteria = null;

        public dynamic ProductsFilterObj=null ;
        public dynamic LotNumbersFilterObj=null ;

        public FilterManipulation(List<List<ControlViewModel>> objlistofFilterCriteria)
        {
            _objLstofFilterCriteria = objlistofFilterCriteria;
            FilterAssignmentClass objFilterAssignment = null;

            for (int iCount = 0; iCount < _objLstofFilterCriteria.Count; iCount++)
            {
                string _DBTable = _objLstofFilterCriteria[iCount][0].AssociateTableName;

                DBTable _objDBTable = (DBTable)Enum.Parse(typeof(DBTable), _DBTable);
                switch (_objDBTable)
                {
                    case DBTable.Products:
                        {
                            objFilterAssignment = new FilterAssignmentClass(_objLstofFilterCriteria[iCount]);
                            ProductsFilterObj = objFilterAssignment.ValueAssignment();
                            objFilterAssignment = null;                            
                        }
                        break;
                    case DBTable.LotNumbers:
                        {
                            objFilterAssignment = new FilterAssignmentClass(_objLstofFilterCriteria[iCount]);
                            LotNumbersFilterObj = objFilterAssignment.ValueAssignment();
                            objFilterAssignment = null;  
                        }
                        break;

                }
            }

        }
       
    }

    public enum DBTable
    {
        Products = 1,
        LotNumbers = 2,
        None = 3,
    }
}