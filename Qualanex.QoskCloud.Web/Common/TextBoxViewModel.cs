﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Common
{
    public class TextBoxViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_textbox; }
        }
        public string Value { get; set; }
    }
}