﻿using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Web
{
    public class FilterAssignmentClass
    {
        List<ControlViewModel> _objList = null;
        
        public FilterAssignmentClass(List<ControlViewModel> objList)
        {
            _objList = objList;
          
        }

        public dynamic ValueAssignment()
        {
            List<FilterClass> objFilter = new List<FilterClass>();
            for (int iCount = 0; iCount < _objList.Count; iCount++)
            {
                if (string.IsNullOrEmpty(_objList[iCount].Value1))
                    continue;

                if ((string.Compare(_objList[iCount].Value1, "0", true) == 0))
                    continue;

                objFilter.Add(new FilterClass
                {
                    Property = _objList[iCount].Name,
                    Value = (_objList[iCount].Value1),
                    FilterType = _objList[iCount].OperatorType,
                });



            }


            return objFilter;
        }
    }
}