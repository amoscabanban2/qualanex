﻿using Qualanex.QoskCloud.Utility;
using System;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Common
{
    /// <summary>
    /// Check a User IsAuthenticated
    /// </summary>
    public class QoskAuthorizeAttribute : AuthorizeAttribute
    {

        public QoskAuthorizeAttribute()
        {
            View = Constants.QoskAuthorizeAttribute_View;
            Master = String.Empty;
        }

        public String View { get; set; }
        public String Master { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            CheckIfUserIsAuthenticated(filterContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckIfUserIsAuthenticated(AuthorizationContext filterContext)
        {
            // If Result is null, we’re OK: the user is authenticated and authorized. 
            if (filterContext.Result == null)
                return;

            // If here, you’re getting an HTTP 401 status code. In particular,
            // filterContext.Result is of HttpUnauthorizedResult type. Check Ajax here. 
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {

                if (String.IsNullOrEmpty(View))
                    return;
                var result = new ViewResult { ViewName = View, MasterName = Master };
                filterContext.Result = result;
            }
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                // For an Ajax request, just end the request 
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.End();
            }
        }
    }
}