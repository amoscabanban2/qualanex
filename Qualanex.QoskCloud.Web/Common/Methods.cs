﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="Methods.cs">
///   Copyright (c) 2017 - 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Common
{
   using System;
   using System.Runtime.Caching;

   using Qualanex.Qosk.Library.Model.DBModel;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public static class Methods
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="dbBase"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string NextId(DBBase dbBase = DBBase.QOSK)
      {
         var dbModel = new DBModel("name=QoskCloud");
         return NextID.GetNextID(ref dbModel, "QDV1", dbBase); //TODO: configurable prefix
      }

      ///****************************************************************************
      /// <summary>
      ///   Sets memory cache with a fixed 30 minute duration.
      /// </summary>
      /// <param name="key"></param>
      /// <param name="obj"></param>
      ///****************************************************************************
      internal static void SetCache(string key, object obj)
      {
         if (obj != null)
         {
            MemoryCache.Default.Set(key, obj, DateTimeOffset.UtcNow.AddMinutes(30));
         }
      }
   }
}
