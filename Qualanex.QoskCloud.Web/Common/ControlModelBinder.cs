﻿using Qualanex.QoskCloud.Utility;
using System;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Common
{
    public class ControlModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var type = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + Constants.ControlModelBinder_Type);
            object model = null;
            switch (type.AttemptedValue)
            {
                case Constants.ControlModelBinder_textbox:
                    {
                        model = new TextBoxViewModel();
                        break;
                    }
                case Constants.ControlModelBinder_checkbox:
                    {
                        model = new CheckBoxViewModel();
                        break;
                    }
                case Constants.ControlModelBinder_ddl:
                    {
                        model = new DropDownListViewModel();
                        break;
                    }
                case Constants.ControlModelBinder_datepicker:
                    {
                        model = new DateTimeViewModel();
                        break;
                    }
                default:
                    {
                        throw new NotImplementedException();
                    }
            };

            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, model.GetType());
            return model;
        }
    }

}