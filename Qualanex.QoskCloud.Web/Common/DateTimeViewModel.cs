﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using System;

namespace Qualanex.QoskCloud.Web.Common
{
    public class DateTimeViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_datepicker; }
        }
        public DateTime? Value { get; set; }
    }
}