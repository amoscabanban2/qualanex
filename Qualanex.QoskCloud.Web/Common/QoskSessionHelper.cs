﻿namespace Qualanex.QoskCloud.Web
{
    public class QoskSessionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetSessionData(string key)
        {
            object values = null;
            if (System.Web.HttpContext.Current.Session[key] != null)
            {
                values = System.Web.HttpContext.Current.Session[key];
            }
            return values;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="values"></param>
        public static void SetSessionData(string key, object values)
        {
            if (!string.IsNullOrEmpty(key) && values != null)
            {
                if (System.Web.HttpContext.Current.Session[key] != null)
                {
                    System.Web.HttpContext.Current.Session[key] = values;
                }
                else
                {
                    System.Web.HttpContext.Current.Session.Add(key, values);
                }
            }
        }

        public static void RemoveSessionData(string key)
        {
            System.Web.HttpContext.Current.Session.Remove(key);
        }

        /// <summary>
        /// Return the referrer URL, this is a quickfix for policyform building in decision support to ensure we know if we're in 
        /// decision support on first entry
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public static string GetReferringPage()
        {
            return System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
        }

    }
}