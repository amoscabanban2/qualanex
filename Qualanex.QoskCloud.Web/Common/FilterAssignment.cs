﻿namespace Qualanex.QoskCloud.Web
{
    public class FilterClass
    {

        public string Property { get; set; }
        public object Value { get; set; }

        public string FilterType { get; set; }
    }
    
}