﻿namespace Qualanex.QoskCloud.Web
{
    public class Column
    {
        public string text { get; set; }
        public string datafield { get; set; }
        public string width { get; set; }

        public string cellsrenderer { get; set; }
        public string buttonclick { get; set; }
        public string columntype { get; set; }
        public string columnsheight { get; set; }
        public string cellsformat { get; set; }
        public string type { get; set; }
        public string format { get; set; }
        public string filtertype { get; set; }
    }    

}