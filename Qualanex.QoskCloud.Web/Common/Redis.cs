﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Configuration;

namespace Qualanex.QoskCloud.Web.Common
{
   public static class Redis
   {
      private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
      {
         string cacheConnection = ConfigurationManager.ConnectionStrings["CacheConnection"].ToString();
         return ConnectionMultiplexer.Connect(cacheConnection);
      });

      public static ConnectionMultiplexer Connection
      {
         get
         {
            return lazyConnection.Value;
         }
      }

      internal static bool SetCacheItem<T>(string key, T obj)
      {
         bool ok = false;
         if (obj != null)
         {
            IDatabase cache = lazyConnection.Value.GetDatabase();
            ok = cache.StringSet(key, JsonConvert.SerializeObject(obj), TimeSpan.FromMinutes(30));
         }
         return ok;
      }

      internal static object GetCacheItem<T>(string key)
      {
         object obj = null;
         IDatabase cache = lazyConnection.Value.GetDatabase();
         RedisValue val = cache.StringGet(key);
         if (val.HasValue)
            obj = JsonConvert.DeserializeObject<T>(val);
         return obj;
      }
   }
}