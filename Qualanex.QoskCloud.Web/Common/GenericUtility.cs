﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Runtime.Caching;
namespace Qualanex.QoskCloud.Web
{
    public class GenericUtility : ICacheService
    {
        public static T MapObject<T>(T source, T target)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            object[] values = new object[props.Count];
            for (int i = 0; i < values.Length; i++)
            {
                try
                {
                    PropertyInfo propertyInfo = target.GetType().GetProperty(props[i].Name);
                    propertyInfo.SetValue(target, Convert.ChangeType(props[i].GetValue(source), Nullable.GetUnderlyingType(propertyInfo.PropertyType)
                    ?? propertyInfo.PropertyType), null);
                }
                catch (Exception) { }
            }
            return target;
        }
        public static string DisplayName(Enum value)
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            MemberInfo member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return outString;
        }



        public T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            T item = MemoryCache.Default.Get(cacheKey) as T;
            if (item == null)
            {
                item = getItemCallback();
                MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(10));
            }
            return item;
        }


    }

}
interface ICacheService
{
    T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class;
}