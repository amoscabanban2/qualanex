﻿using System;
using System.Web.Mvc;
using Qualanex.QoskCloud.Entity;
using System.Linq;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Common
{
    [AttributeUsage(AttributeTargets.All,
                   AllowMultiple = false,
                   Inherited = true)]
    public sealed class ExceptionFilterAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// Called by ASP.NET MVC framework  when an Exception occurs accros the application.
        /// </summary>
        /// <param name="filterContext"></param>s
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext != null)
            {
                base.OnException(filterContext);
                string errorInfo = string.Empty;
                if (AppSettingInformation.lstQoskCloudAppInfo.Count > 0)
                    errorInfo = AppSettingInformation.lstQoskCloudAppInfo.Where(x => x.keyName.Equals(Constants.ExceptionFilterAttribute_EnableError)).FirstOrDefault().Value.ToString();
                if (errorInfo.Equals(Constants.ExceptionFilterAttribute_yes, StringComparison.OrdinalIgnoreCase))
                {
                    Logger.Error(filterContext.Exception.ToString());
                    filterContext.ExceptionHandled = true;
                }
            }

        }
    }
}