﻿using System.Collections.Generic;
using Qualanex.QoskCloud.Entity;

namespace Qualanex.QoskCloud.Web
{
    public class RootObject
    {
        public List<MLGroup> Grouprows { get; set; }
        public List<Column> columns { get; set; }
        public List<ProductsML> rows { get; set; }
        public List<ResponsePolicy> policyrows { get; set; }
        public List<MLPharamcyPolicy> Viewpolicyrows { get; set; }
        public List<ChangeTrackerEntity> changeTrack { get; set; }
        public List<ProfileRequest> pharmacyDetailsrow { get; set; }
    }
}