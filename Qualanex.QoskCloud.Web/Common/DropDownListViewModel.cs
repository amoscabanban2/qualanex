﻿using Qualanex.QoskCloud.Utility;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Web.Common
{
    public class DropDownListViewModel : TextBoxViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_ddl; }
        }
        public SelectList Values { get; set; }
    }
}