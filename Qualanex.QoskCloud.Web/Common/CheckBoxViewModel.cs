﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Common
{
    public class CheckBoxViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_checkbox; }
        }
        public bool Value { get; set; }
    }
}