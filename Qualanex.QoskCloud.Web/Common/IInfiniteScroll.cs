﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="IQoskApplet.cs">
///   Copyright (c) 2018, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Web.Common
{
   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public interface IInfiniteScroll
   {
      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The number of records expected for the initial query return.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      int InitialLoadCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The number of records to load each time the element is scrolled near
      ///   the bottom.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      int ScrollLoadCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Stores the current count of records when submitting for additional
      ///   records.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      int PreviousCount { get; set; }

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   The height from the bottom of the scroll field in pixels when further
      ///   records should be queried.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      int LoadAtHeight { get; set; }
   }
}
