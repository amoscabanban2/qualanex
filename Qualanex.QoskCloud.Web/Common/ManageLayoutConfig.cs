﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.Web.Common
{
   public class ManageLayoutConfig
   {
      /// <summary>
      /// GetColumnListingByLayout
      /// </summary>
      /// <param name="LayoutName"></param>
      /// <param name="UserID"></param>
      /// <param name="ForGridColumnListing"></param>
      /// <param name="isCriteria"></param>
      /// <returns></returns>
      public static ColumnListEntity GetColumnListingByLayout(string LayoutName, long UserID, bool ForGridColumnListing, bool isCriteria = false)
      {
         ReturnLoginUserEntity objReturnLoginUserEntity = (ReturnLoginUserEntity)QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);

         UserRegistrationRequest userReturnData = objReturnLoginUserEntity != null
                                                                        ? objReturnLoginUserEntity.UserReturnData
                                                                        : new UserRegistrationRequest();
         UserID = userReturnData.UserID ?? 0;

         ColumnListRequest objColumnListRequest = new ColumnListRequest
         {
            ForGridColumnListing = ForGridColumnListing,
            LayoutName = LayoutName,
            UserId = UserID,
            Criteria = isCriteria
         };

         CommonDataAccess objCommon = new CommonDataAccess();
         return objCommon.GetAllColumnListing(objColumnListRequest);
      }
      /// <summary>
      /// SaveSubscribedColumn
      /// </summary>
      /// <param name="objColumnListing"></param>
      /// <param name="UserID"></param>
      /// <param name="LayoutID"></param>
      /// <param name="tableId"></param>
      /// <returns></returns>
      public static bool SaveSubscribedColumn(ColumnListEntity objColumnListing, long UserID, int LayoutID, int tableId = 0)
      {
         CommonDataAccess objCommon = new CommonDataAccess();
         return objCommon.SaveSubscribedColumnListing(objColumnListing, UserID, LayoutID, tableId);
      }
   }
}