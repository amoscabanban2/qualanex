﻿using System;
using System.Web.Mvc;
using System.Text;
using System.Globalization;
using Qualanex.QoskCloud.Entity;
using System.Linq;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Web.Common
{
    /// <summary>
    /// Class to implement tracing functionality.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public sealed class TraceFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// <class>CSTraceFilterAttribute</class>
        /// <method>OnActionExecuting</method>
        /// </summary>       
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext != null)
            {
                //For Trace Log                 
                string traceInfo = string.Empty;
                if (AppSettingInformation.lstQoskCloudAppInfo.Count > 0)
                    traceInfo = AppSettingInformation.lstQoskCloudAppInfo.Where(x => x.keyName.Equals(Constants.TraceFilterAttribute_EnableTracing)).FirstOrDefault().Value.ToString();
                if (traceInfo.Equals(Constants.ExceptionFilterAttribute_yes, StringComparison.OrdinalIgnoreCase))
                {
                    StringBuilder sbTrace = new StringBuilder(1024);
                    sbTrace.AppendFormat(CultureInfo.InvariantCulture, "Controller : {0}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName);
                    sbTrace.AppendFormat(CultureInfo.InvariantCulture, string.Format(CultureInfo.InvariantCulture, ", Action : {0}", filterContext.ActionDescriptor.ActionName));
                    foreach (var ActionParameter in filterContext.ActionParameters)
                    {
                        if (ActionParameter.Value != null)
                        {
                            if (ActionParameter.Value.GetType().Name.Equals(Constants.TraceFilterAttribute_FormCollection))
                            {
                                var formcollectionValue = (FormCollection)ActionParameter.Value;
                                foreach (var key in formcollectionValue)
                                {
                                    sbTrace.Append("Parameter Name : " + key);
                                    sbTrace.AppendLine();
                                    sbTrace.Append("Parameter Value: " + formcollectionValue[key.ToString()]);
                                    sbTrace.AppendLine();
                                }
                            }
                            else
                            {
                                sbTrace.Append("Parameter Name : " + ActionParameter.Key);
                                sbTrace.Append("Parameter Value: " + ActionParameter.Value);
                                sbTrace.AppendLine();
                            }
                        }
                    }
                    sbTrace.AppendFormat(CultureInfo.InvariantCulture, string.Format(CultureInfo.InvariantCulture, ", Accept Verb Type: {0}", filterContext.HttpContext.Request.HttpMethod));
                    Logger.Info(sbTrace.ToString());
                }
            }
        }
    }
}
