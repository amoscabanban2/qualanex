﻿using Qualanex.QoskCloud.Utility;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace Qualanex.QoskCloud.Web.Common
{
    public class QualanexQoskGlobalFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string userId = HttpContext.Current.User.Identity.Name;
            if (userId != null)
            {
                var userdata = QoskSessionHelper.GetSessionData(Constants.ManageLayoutConfig_UserDataWithLink);
                if (userdata == null)
                {
                    context.Result = new RedirectToRouteResult(
                            new RouteValueDictionary{{ Constants.QualanexQoskGlobalFilterAttribute_controller, Constants.QualanexQoskGlobalFilterAttribute_User },
                                          { Constants.QualanexQoskGlobalFilterAttribute_Action, Constants.QualanexQoskGlobalFilterAttribute_Login }

                                         });
                }
            }
            base.OnActionExecuting(context);

        }
    }
}