﻿
namespace System.Web.Mvc
{
   public static class FileUploadHelper
   {
      public static MvcHtmlString FileUploadForm(this HtmlHelper htmlHelper, string ControllerAction, string preview = null)
      {
         string formTemplate = string.IsNullOrWhiteSpace(preview) ? @"<form action='{0}' method='post' enctype='multipart/form-data' class='dropzone'  id='dropzoneForm'>
                                        <div class='fallback'>
                                            <input name='file' type='file' multiple />
                                            <input type='submit' value='Upload' />
                                        </div>
                                    </form>"
: @"<form action='{0}' method='post' enctype='multipart/form-data' class='dropzone'  id='dropzoneForm' data-field-preview='{1}'>
                                        <div class='fallback'>
                                            <input name='file' type='file' multiple />
                                            <input type='submit' value='Upload' />
                                        </div>
                                    </form>";
         return new MvcHtmlString(string.IsNullOrWhiteSpace(preview) ? string.Format(formTemplate, ControllerAction) : string.Format(formTemplate, ControllerAction, preview));
      }
   }
}