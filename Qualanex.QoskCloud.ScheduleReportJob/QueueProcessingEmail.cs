﻿using System;
using Qualanex.QoskCloud.Utility;
using System.Text;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.ScheduleReportJob
{
   public class QueueProcessingEmail
   {
      string NetworkUserEmail { get; set; }
      string NetworkUserPassword { get; set; }
      string NetworkSMTP { get; set; }
      string ReportSubject { get; set; }
      string MessageBody { get; set; }
      ReportRequest objReportRequest { get; set; }

      /// <summary>
      /// Constructor initialiaze all the variables.
      /// </summary>
      /// <param name="reportRequest"></param>
      public QueueProcessingEmail(ReportRequest reportRequest)
      {
         objReportRequest = reportRequest;
      }

      /// <summary>
      /// Send mail to users.
      /// </summary>
      public void SendEmailToUser(TextWriter log)
      {
         log.WriteLine("Begin SendEmail");
         InitializeMailVariables(log);

         if (objReportRequest != null && !string.IsNullOrEmpty(NetworkUserEmail) &&
             !string.IsNullOrEmpty(NetworkUserPassword) && !string.IsNullOrEmpty(NetworkSMTP)
             && objReportRequest.UserEmailList != null
             && objReportRequest.UserEmailList.Count > 0)
         {
            if (CreateMessageBody(log))
            {
               log.WriteLine("Attempt to send");
               SendUserMail.SendEmail(null, string.Empty, objReportRequest.UserEmailList, ReportSubject, MessageBody,
                  NetworkUserEmail, NetworkUserPassword, NetworkSMTP, log);
            }
         }
         else
         {
            log.WriteLine("Sending Canceled due to missing information");
         }
      }

      /// <summary>
      /// Temporary Email Body.
      /// </summary>
      /// <returns></returns>
      public bool CreateMessageBody(TextWriter log)
      {
         log.WriteLine("Building message body");
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var userName = (from user in cloudEntities.User
                               where user.UserID == objReportRequest.UserID
                               select user.UserName).SingleOrDefault();
               StringBuilder strMessagebuilder = new StringBuilder();
               strMessagebuilder.Append(
                  "A scheduled report is ready for your review. This report will be available for 24 hours after the time of execution. Click on the following link in order to download this report.<br/>");
               strMessagebuilder.Append("<a href=\"" + objReportRequest.BlobFilePath + "\">View Report</a><br/>");
               strMessagebuilder.Append("<br/>");
               strMessagebuilder.Append("<b>Report Title: </b>" + objReportRequest.ReportName + "<br/>");
               strMessagebuilder.Append("<b>Schedule Name: </b>" + objReportRequest.ReportScheduleTitle + "<br/>");

               if (userName != null)
               {
                  strMessagebuilder.Append("<b>Creator: </b>" + userName + "<br/>");
               }

               strMessagebuilder.Append("<b>Format: </b>" + objReportRequest.FileType + "<br/>");
               strMessagebuilder.Append("<br/>");
               strMessagebuilder.Append("Thank you,<br/>");
               strMessagebuilder.Append("Qualanex<br/>");
               MessageBody = strMessagebuilder.ToString();
               log.WriteLine("Message body built:" + MessageBody);
               return true;
            }
         }
         catch
         {
            log.WriteLine("Message body failed");
            return false;
         }
      }

      /// <summary>
      /// Initialize class variables.
      /// </summary>
      /// <returns></returns>
      public void InitializeMailVariables(TextWriter log)
      {
         try
         {
            log.WriteLine("Initializing variables...");
            ScheduleReportDataAccess scheduleReportDataAccess = new ScheduleReportDataAccess();
            List<AppConfigurationSetting> lstAzureConfigDetails = scheduleReportDataAccess.QoskCloudConfigurationSetting(Utility.Constants.Settings_QoskCloud);
            log.WriteLine("Getting app configuration settings");

            if (lstAzureConfigDetails != null && lstAzureConfigDetails.Count > 0)
            {
               ReportSubject = "Qosk Report, " + objReportRequest.ReportScheduleTitle;
               NetworkUserEmail =
                  lstAzureConfigDetails.Where(
                        x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialUserName))
                     .FirstOrDefault()
                     .Value.ToString();
               NetworkUserPassword =
                  lstAzureConfigDetails.Where(
                        x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialPassword))
                     .FirstOrDefault()
                     .Value.ToString();
               NetworkSMTP =
                  lstAzureConfigDetails.Where(x => x.keyName.Equals(Utility.Constants.UserController_CorporateSMTP))
                     .FirstOrDefault()
                     .Value.ToString();
               log.WriteLine(NetworkUserPassword + " " + NetworkUserEmail + " " + NetworkSMTP);
            }
            else
            {
               log.WriteLine("Null or empty config details!");
            }
         }
         catch
         {
            log.WriteLine("Exception thrown during mail variable initialization");
            throw;
         }
      }
   }
}
