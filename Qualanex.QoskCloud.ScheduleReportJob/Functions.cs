﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.ScheduleReportJob
{
    public class Functions
    {

        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called ReportQueue      
        public static void CreateScheduleReport([QueueTrigger(Constants.Azure_Queue_ReportRequestQueue)] ReportRequest reportRequest, TextWriter log)
        {
            log.Write("Processing Queue Message {0}", reportRequest);
            try
            {
                QueueProcessing queueprocessing = new QueueProcessing();
                var notifyResult = queueprocessing.CreateReport(reportRequest, log);
                log.WriteLine("Processing Complete for Message {0}", reportRequest);
                SendNotification(notifyResult, log);
                
            }
            catch (System.Exception ex)
            {
                log.Write("Error: {0}", ex.ToString());                
                throw;
            }
        }

      public static void SendNotification(ReportRequest objReportRequest, TextWriter log)
      {
         if (objReportRequest != null)
         {
            log.Write("Processing Queue Message {0}", objReportRequest);
            try
            {
               var queueprocessing = new QueueProcessingEmail(objReportRequest);
               ScheduleReportDataAccess objScheduleReportDataAccess = new ScheduleReportDataAccess();
               objScheduleReportDataAccess.UpdateScheduleReportStatus(objReportRequest.ReportRunGuid,
                  ReportRunningStatus.NotificationSending.ToString(), Constants.ReportMessage_Notification_Sending, null,
                  System.DateTime.UtcNow);
               queueprocessing.SendEmailToUser(log);
               objScheduleReportDataAccess.UpdateScheduleReportStatus(objReportRequest.ReportRunGuid,
                  ReportRunningStatus.NotificationSent.ToString(), Constants.ReportMessage_Notification_Sent, null,
                  System.DateTime.UtcNow);
               log.WriteLine("Processing Complete for Message {0}", objReportRequest);

            }
            catch (System.Exception ex)
            {
               log.Write("Error: {0}", ex.ToString());
               throw;
            }
         }
         else
         {
            log.WriteLine("Email skipped due to report generation failure!");
         }
      }
   }
}
