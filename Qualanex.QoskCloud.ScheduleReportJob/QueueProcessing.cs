﻿using Qualanex.QoskCloud.Utility;
using System;
using Telerik.Reporting;
using Newtonsoft.Json;
using System.Reflection;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Diagnostics;
using System.Collections.Generic;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.ScheduleReportJob
{
   /// <summary>
   /// QueueProcessing : To process schedule report as per their parameters.
   /// </summary>
   public class QueueProcessing
   {
      string SchedulerReportsDirectory { get; set; }
      string BlobFilePath { get; set; }
      /// <summary>
      /// CreateReport : Receive ReportRequest as a parameter and process report.
      /// </summary>
      /// <param name="reportRequest"></param>
      public ReportRequest CreateReport(ReportRequest reportRequest, TextWriter log)
      {
         log.WriteLine("CreateReport Entered");
         ReportRequest result = null;

         if (reportRequest != null)
         {
            CreateReportFolder(log);
            ScheduleReportDataAccess objScheduleReportDataAccess = new ScheduleReportDataAccess();
            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.Running.ToString(), Constants.ReportMessage_Picked_By_Schedule_Job, null, DateTime.UtcNow);
            result = GenerateReportBasedOnRequest(reportRequest, log);
         }

         log.WriteLine("CreateReport Finished");
         return result;
      }

      /// <summary>
      /// GenerateRetailerReport : Generate Retailer Report
      /// </summary>
      /// <param name="reportRequest"></param>
      private ReportRequest GenerateReportBasedOnRequest(ReportRequest reportRequest, TextWriter log)
      {
         log.WriteLine("Generating report...");
         ScheduleReportDataAccess objScheduleReportDataAccess = new ScheduleReportDataAccess();

         try
         {
            string criteria = reportRequest.Criteria;
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            log.WriteLine("Telerik processor initialized");

            //Set any deviceInfo settings if necessary
            System.Collections.Hashtable deviceInfo = new System.Collections.Hashtable();
            Telerik.Reporting.TypeReportSource typeReportSource = new Telerik.Reporting.TypeReportSource();

            //ReportName is the Assembly Qualified Name of the report
            typeReportSource.TypeName = GetReportTypeName(reportRequest.ReportName, reportRequest.ReportModule, log);
            GetReportParameters(criteria, typeReportSource, log);
            string fileType = reportRequest.FileType.ToString();
            typeReportSource.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, reportRequest.UserID));
            typeReportSource.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, fileType));
            log.WriteLine("Attempting report render");
            Telerik.Reporting.Processing.RenderingResult result =
            reportProcessor.RenderReport(fileType, typeReportSource, deviceInfo);
            log.WriteLine("Render Successful");
            string fileName = reportRequest.ReportScheduleTitle + GetEpochTime(DateTime.UtcNow).ToString() + Constants.Single_Dot + result.Extension;
            log.WriteLine("filename: " + fileName);
            string reportDirectory = SchedulerReportsDirectory;
            string filePath = System.IO.Path.Combine(reportDirectory, fileName);

            using (System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
            {
               fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
            }

            log.WriteLine("Successfully wrote file: " + filePath);
            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.Generated.ToString(), Constants.ReportMessage_Report_Exported_Successfully);
            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.Uploading.ToString(), Constants.ReportMessage_Uploading_To_Blob);
            UploadFilesfromWebJobToBlob(Constants.AzureContainer_GeneratedReports, filePath, Constants.AzureAccount_KioskSync, reportDirectory, log);

            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.Uploaded.ToString(), Constants.ReportMessage_Uploaded_To_Blob);
            BlobFilePath = BlobResource.GetSASUrl(Constants.AzureAccount_KioskSync, Constants.AzureContainer_GeneratedReports, fileName, Constants.FileAccess_Methods_Read, Constants.Report_SASUrl_24HrTimeout);
            log.WriteLine("Blob file path get");
            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.AddedToNotificationQueue.ToString(), Constants.ReportMessage_Placed_In_NotificationQueue);
            reportRequest.BlobFilePath = BlobFilePath;
         }
         catch (Exception ex)
         {
            objScheduleReportDataAccess.UpdateScheduleReportStatus(reportRequest.ReportRunGuid, ReportRunningStatus.Error.ToString(), ex.Message);
            throw;
         }

         return reportRequest;
      }

      /// <summary>
      /// Add message in to notification queue.
      /// </summary>
      /// <param name="reportRequest"></param>
      public void AddToNotificationQueue(ReportRequest reportRequest, TextWriter log)
      {
         log.WriteLine("Adding message to notification queue");
         try
         {
            AddMessageInQueue(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_ReportNotificationQueue, reportRequest, log);

         }
         catch
         {
            throw;
         }
      }

      /// <summary>
      /// Get the parameter of the report passed in to this method.
      /// </summary>                
      /// <param name="criteria"></param>
      /// <param name="typeReportSource"></param>
      private void GetReportParameters(string criteria, TypeReportSource typeReportSource, TextWriter log)
      {
         log.WriteLine("Getting report parameters");
         try
         {
            Dictionary<string, object> parameterList = JsonConvert.DeserializeObject<Dictionary<string, object>>(criteria);
            foreach (KeyValuePair<string, object> parameter in parameterList)
            {
               typeReportSource.Parameters.Add(new Parameter(parameter.Key, parameter.Value ?? ""));
            }
         }
         catch
         {
            throw;
         }
      }

      /// <summary>
      /// Get the fully qualified name of the report.
      /// </summary>
      /// <param name="reportName"></param>
      /// <param name="reportModule"></param>
      /// <returns></returns>
      private string GetReportTypeName(string reportName, string reportModule, TextWriter log)
      {
         log.WriteLine("Getting report assembly");
         //string executablePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
         // var reportAssembly = Assembly.LoadFrom(executablePath + Constants.Single_Backward_Slash + Constants.Report_AssemblyName);
         //Assembly reportAssembly = Assembly.Load(Constants.Report_AssemblyName);
         //var reportType = reportAssembly.GetType(string.Format(Constants.Report_Class_Full_QualifiedName, reportModule, reportName));
         //return reportType.AssemblyQualifiedName;
         Assembly reportAssembly = Assembly.Load("Qualanex.QoskCloud.Web");
         var reportType = reportAssembly.GetType(string.Format("Qualanex.QoskCloud.Web.Areas.Report.{0}.{1}", reportModule, reportName == "Product Details" ? "Sample_Report_Retailer_Product_Detail" : reportName.Replace(" ", "")));
         return reportType.AssemblyQualifiedName;

      }

      /// <summary>
      /// CreateReportFolder : Create folder in which all scheduled reports will be placed.
      /// </summary>
      private void CreateReportFolder(TextWriter log)
      {
         try
         {
            log.WriteLine("Checking for existing report directory...");
            string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            SchedulerReportsDirectory = Path.Combine(ApplicationPath, Constants.DirectoryName_ScheduledReports);

            if (!Directory.Exists(SchedulerReportsDirectory))
            {
               Directory.CreateDirectory(SchedulerReportsDirectory);
               log.WriteLine("Directory Created: " + SchedulerReportsDirectory);
            }
            else
            {
               log.WriteLine("Directory Exists: " + SchedulerReportsDirectory);
            }
         }
         catch
         {
            throw;
         }
      }

      /// <summary>
      /// Upload schedule report file on azure blob storage.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="filePath"></param>
      /// <param name="accountName"></param>
      /// <param name="folder"></param>
      /// <returns></returns>
      private void UploadFilesfromWebJobToBlob(string container, string filePath, string accountName, string folder, TextWriter log)
      {
         log.WriteLine("Uploading to blob");

         try
         {
            var connectString = GetConnectionString(accountName);
            log.WriteLine("Connection string get");
            Microsoft.WindowsAzure.Storage.CloudStorageAccount account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);
            log.WriteLine("Account initialized");
            CloudBlobClient client = account.CreateCloudBlobClient();
            log.WriteLine("Client initialized");
            CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());
            log.WriteLine("Container initialized");
            sampleContainer.CreateIfNotExists();

            sampleContainer.SetPermissions(new BlobContainerPermissions()
            {
               PublicAccess = BlobContainerPublicAccessType.Container
            });

            CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(Path.GetFileName(filePath));
            log.WriteLine("File path get");

            using (Stream file = System.IO.File.OpenRead(filePath))
            {
               blob.UploadFromStream(file);
            }

            log.WriteLine("Successfully Uploaded!");

            if (File.Exists(filePath))
            {
               log.WriteLine("Deleting " + filePath);
               File.Delete(filePath);
            }
         }
         catch
         {
            log.WriteLine("Exception during blob upload");
            throw;
         }
      }

      /// <summary>
      /// Get Azure connection String.
      /// </summary>
      /// <param name="account"></param>
      /// <returns></returns>
      private string GetConnectionString(string account)
      {
         var connectString = string.Empty;
         connectString = ConfigurationManager.GetConnectionString(account);

         if (string.IsNullOrEmpty(connectString))
         {
            throw new ApplicationException(string.Format(Constants.Azure_ConnectionString_NotDefined_Format, account));
         }

         return connectString;
      }

      /// <summary>
      /// Get Epoch Time
      /// </summary>
      /// <param name="utcDate"></param>
      /// <returns></returns>
      private static long GetEpochTime(DateTime utcDate)
      {
         return (long)(utcDate - new DateTime(1970, 1, 1)).TotalSeconds;
      }

      /// <summary>
      /// Add message in to azure queue.
      /// </summary>
      /// <param name="account"></param>
      /// <param name="queueName"></param>
      /// <param name="message"></param>
      private void AddMessageInQueue(string account, string queueName, object message, TextWriter log)
      {
         try
         {
            var configVal = ConfigurationManager.GetConnectionString(account);
            log.WriteLine("Config value loaded");

            if (string.IsNullOrEmpty(configVal))
            {
               log.WriteLine("Config value was null or empty!");
               throw new ApplicationException(string.Format(Constants.Azure_ConnectionString_NotDefined_Format, account));
            }

            var storageAccount = CloudStorageAccount.Parse(configVal);
            log.WriteLine("Storage account parsed");
            var client = storageAccount.CreateCloudQueueClient();
            log.WriteLine("Client initialized");
            var queue = client.GetQueueReference(queueName);
            log.WriteLine("Queue initialized");

            if (!queue.Exists())
            {
               log.WriteLine("Creating Queue...");
               queue.CreateIfNotExists();
            }

            Trace.TraceInformation("Adding Message to Queue: " + queueName + " for Account: " + account);
            queue.AddMessage(new CloudQueueMessage(JsonConvert.SerializeObject(message)));
            log.WriteLine("Queue message added - " + JsonConvert.SerializeObject(message));
         }
         catch (Exception ex)
         {
            log.WriteLine("Exception thrown: " + ex.Message);
         }
      }
   }
}
