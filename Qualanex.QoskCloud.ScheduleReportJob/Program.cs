﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.ScheduleReportJob
{
   public class Program
   {
      // Please set the following connection strings in app.config for this WebJob to run:
      // AzureWebJobsDashboard and AzureWebJobsStorage
      static void Main()
      {
         System.Console.WriteLine("Main() function called in ScheduleReportJob");
         using (JobHost host = new JobHost())
         {
            host.RunAndBlock();
         }
         // To Test in local
         //Functions.CreateScheduleReport(new ReportRequest()
         //{
         //   ReportID = 13,
         //   ReportModule = "Retailer",
         //   ReportName = "Product Details",
         //   ReportScheduleTitle = "New Webjob Report",
         //   ReportRunGuid = System.Guid.Parse("75c1ec05-2f67-4135-9894-34ea29fc11c0"),
         //   ReportScheduleID = 179,
         //   FileType = FileType.PDF,
         //   Criteria =
         //      "{\"ExpDate\":null,\"LotNumber\":null,\"VendorName\":null,\"ReturnStatus\":null,\"StoreName\":null,\"StoreNumber\":null,\"PartialQty\":null,\"Strength\":null,\"NDCNumber\":null,\"ItemGuid\":null,\"QoskProcessDate\":null,\"ProductDescription\":null,\"ControlNumber\":null,\"PackageSize\":null,\"FullQty\":null,\"UnitPriceBefore\":null,\"UnitPriceAfter\":null,\"CreditableAmountBeforeMFGDiscount\":null,\"CreditableAmountAfter\":null,\"OutofpolicyDescription\":null,\"RecalledProduct\":null,\"DiscontinuedProduct\":null,\"RXorOTC\":null,\"DosageForm\":null,\"PackageForm\":null,\"PartialPercentage\":null,\"ExtendedDiscountPercent\":null,\"ReportDataTimeZone\":\"(UTC-06:00) Central Time (US & Canada)\",\"ReportScheduleTimeZone\":\"(UTC-06:00) Central Time (US & Canada)\",\"DataStartDate\":null,\"DataEndDate\":null}",
         //   BlobFilePath = null,
         //   UserEmailList = new List<string> { "dbazan@qualanex.com" },
         //   UserID = 6004
         //}, new StringWriter());
         //Functions.CreateScheduleReport(new ReportRequest()
         //{
         //   ReportID = 14,
         //   ReportModule = "Retailer",
         //   ReportName = "Waste Report",
         //   ReportScheduleTitle = "Webjob Waste Test Daniel PDF",
         //   ReportRunGuid = System.Guid.Parse("68b82ec9-2e95-4f02-8347-fbe17480daed"),
         //   ReportScheduleID = 208,
         //   FileType = FileType.PDF,
         //   Criteria =
         //      "{\"ItemGuid\":null,\"SealedOpenCase\":null,\"LotNumber\":null,\"Strength\":null,\"WasteCode\":null,\"WasteStreamProfile\":null,\"NDCNumber\":null,\"ProductDescription\":null,\"FullQty\":null,\"PartialQty\":null,\"StoreName\":null,\"StoreNumber\":null,\"PackageSize\":null,\"ExpDate\":null,\"ControlNumber\":null,\"DosageForm\":null,\"PackageForm\":null,\"ReportDataTimeZone\":\"(UTC-05:00) Eastern Time (US & Canada)\",\"ReportScheduleTimeZone\":\"(UTC-05:00) Eastern Time (US & Canada)\",\"DataStartDate\":\"2016-05-17T19:45:42.0175241Z\",\"DataEndDate\":\"2017-01-12T19:45:42.0175241Z\"}",
         //   BlobFilePath = null,
         //   UserEmailList = new List<string> { "dbazan@qualanex.com" },
         //   UserID = 5033
         //}, new StringWriter());

      }

   }
}
