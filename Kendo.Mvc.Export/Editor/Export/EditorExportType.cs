﻿namespace Kendo.Mvc.Export
{
	/// <summary>
	/// Specifies the editor export types.
	/// </summary>
	public enum EditorExportType
	{
		/// <summary>
		/// Export to DOCX
		/// </summary>
		Docx = 1,

		/// <summary>
		/// Export to RTF
		/// </summary>
		Rtf = 2,

		/// <summary>
		/// Export to PDF
		/// </summary>
		Pdf = 4,

		/// <summary>
		/// Exprot to HTML
		/// </summary>
		Html = 8,

		/// <summary>
		/// Export to TXT
		/// </summary>
		Txt = 16
	}
}
