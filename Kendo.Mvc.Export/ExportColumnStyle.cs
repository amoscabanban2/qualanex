﻿using Telerik.Documents.SpreadsheetStreaming;

namespace Kendo.Mvc.Export
{
    /// <summary>
    /// Column style export settings.
    /// </summary>
    public class ExportColumnStyle
    {
        private readonly string name;

        /// <summary>
        /// Column name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }

        private readonly IColumnExporter column;

        /// <summary>
        /// Current column instance
        /// </summary>
        public IColumnExporter Column
        {
            get
            {
                return column;
            }
        }

        private readonly int index;

        /// <summary>
        /// Current column index
        /// </summary>
        public int Index
        {
            get
            {
                return index;
            }
        }

        /// <summary>
        /// ExportColumnStyle constructor
        /// </summary>
        /// <param name="column">Column instance</param>
        /// <param name="index">Column index</param>
        /// <param name="name">Column name</param>
        public ExportColumnStyle(IColumnExporter column, int index, string name)
        {
            this.column = column;
            this.index = index;
            this.name = name;
        }
    }
}
