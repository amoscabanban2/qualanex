﻿using System;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using Qualanex.Qosk.ServiceEntities;
using Qualanex.QoskCloud.Utility;
using Microsoft.WindowsAzure.Storage;
using System.Collections.Generic;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.PartialSyncDataJob
{
   public class QueueProcessing
   {
      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="fileName"></param>
      /// <param name="syncRequest"></param>
      ///****************************************************************************
      public void ProcessPartialSync(string fileName, SyncRequest syncRequest)
      {
         syncRequest.Status = SyncStatus.Uploading;
         CreateAVROData.UpdatePartialSyncStatus(syncRequest);

         if (uploadFilesfromWebJobToBlob(Constants.AzureContainer_Changeset, fileName, Constants.AzureAccount_SysUpdates, syncRequest.QoskMachineID))
         {
            syncRequest.Status = SyncStatus.Uploaded;
            CreateAVROData.UpdatePartialSyncStatus(syncRequest);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="container"></param>
      /// <param name="filePath"></param>
      /// <param name="accountName"></param>
      /// <param name="folder"></param>
      /// <returns></returns>
      ///****************************************************************************
      private bool uploadFilesfromWebJobToBlob(string container, string filePath, string accountName, string folder)
      {
         var connectString = ConfigurationManager.GetConnectionString(accountName);
         var account = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(connectString);
         var client = account.CreateCloudBlobClient();
         var sampleContainer = client.GetContainerReference(container.ToLower());
         sampleContainer.CreateIfNotExists();

         sampleContainer.SetPermissions(new BlobContainerPermissions()
         {
            PublicAccess = BlobContainerPublicAccessType.Container
         });
         var blob = sampleContainer.GetBlockBlobReference(Path.Combine(folder.ToLower(), Path.GetFileName(filePath)));
         using (Stream file = System.IO.File.OpenRead(filePath))
         {
            blob.UploadFromStream(file);
         }
         if (Directory.Exists(filePath))
         {
            File.Delete(filePath);
         }
         return true;
      }


      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="container"></param>
      /// <param name="folderPath"></param>
      /// <param name="fileName"></param>
      /// <param name="accountName"></param>
      ///****************************************************************************
      public void DownloadFile(string container, string folderPath, string fileName, string accountName)
      {
         try
         {
            var connectString = ConfigurationManager.GetConnectionString(accountName);
            CloudStorageAccount account = CloudStorageAccount.Parse(connectString);
            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer sampleContainer = client.GetContainerReference(container);

            if (sampleContainer.Exists())
            {
               CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(fileName);
               if (blob.Exists())
               {
                  blob.FetchAttributes();
                  blob.SetProperties();
                  string downloadFileWithPath = folderPath + Constants.Double_Backward_Slash + fileName;
                  using (Stream outputFile = new FileStream(downloadFileWithPath, FileMode.Create))
                  {
                     blob.DownloadToStream(outputFile);

                  }
               }
            }
         }
         catch
         {
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="objProductImageDetails"></param>
      /// <param name="productImageFolderPath"></param>
      ///****************************************************************************
      public void DownloadProductImagesFromAzureStorage(List<ProductImage> objProductImageDetails, string productImageFolderPath)
      {
         if (objProductImageDetails != null)
         {
            foreach (var imageItem in objProductImageDetails)
            {
               if (!string.IsNullOrEmpty(imageItem.FileName))
                  DownloadFile(Constants.AzureContainer_ReturnMedia, productImageFolderPath, Constants.Original_Product_Images_Folder + imageItem.FileName, Constants.AzureAccount_KioskSync);
               // TODO: Fix commented code
               //if (!string.IsNullOrEmpty(imageItem.ContentFileName))
               //   DownloadFile(Constants.AzureContainer_ReturnMedia, productImageFolderPath, Constants.Content_Product_Images_Folder + imageItem.ContentFileName, Constants.AzureAccount_KioskSync);
            }
         }
      }

   }
}
