﻿using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using System;
using System.IO;

namespace Qualanex.QoskCloud.PartialSyncDataJob
{
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            System.Console.WriteLine("Main() function called in PartialSyncProcess");
            using (JobHost host = new JobHost())
            {
                host.RunAndBlock();
            }
            //Functions.WritePartialSyncDataToBlob(JsonConvert.SerializeObject(new Qualanex.Qosk.ServiceEntities.SyncRequest { KioskID = "FL806591", RequestedDate = new DateTime(2015, 8, 25) }), new StringWriter());

        }
    }
}
