﻿using System.IO;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using Qualanex.QoskCloud.Utility;
using Qualanex.Qosk.ServiceEntities;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.PartialSyncDataJob
{
   public class Functions
   {
      private static SyncRequest syncRequest;

      ///****************************************************************************
      // This function will be triggered based on the schedule you have set for this WebJob
      // This function will enqueue a message on an Azure Queue called queue
      ///****************************************************************************
      //[NoAutomaticTrigger]
      //public static void WriteSyncDataToBlob(TextWriter log, int value, [Queue("queue")] out string message)
      public static void WritePartialSyncDataToBlob([QueueTrigger(Constants.Azure_Queue_PartialSync)] string message, TextWriter log)
      {
         log.Write("Processing Queue Message {0}", message);
         try
         {
            syncRequest = JsonConvert.DeserializeObject<SyncRequest>(message);
            PartialDataSync(log, syncRequest);
            log.WriteLine("Processing Complete for Message {0}", message);
         }
         catch (System.Exception ex)
         {
            log.Write("Error: {0}", ex.ToString());
            throw;
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="log"></param>
      /// <param name="syncRequest"></param>
      ///****************************************************************************
      private static void PartialDataSync(TextWriter log, SyncRequest syncRequest)
      {
         string filePath = "";
         QueueProcessing queueprocessing = new QueueProcessing();
         List<ProductImage> objProductImageDetails = new List<ProductImage>();
         ProcessCreateAVROForPartialSync processCreateAVROForPartialSync = new ProcessCreateAVROForPartialSync(syncRequest);

         log.WriteLine("Creation of AVROFiles Start for partial sync.");
         Responses<Status> responseAVROFiles = processCreateAVROForPartialSync.CreateAVROFilesForPartial(ref objProductImageDetails);
         log.WriteLine("Creation of AVROFiles End With Status ={0}", responseAVROFiles.status);

         log.WriteLine("Download Product Images From AzureStorage.");
         queueprocessing.DownloadProductImagesFromAzureStorage(objProductImageDetails, processCreateAVROForPartialSync.ProductImageFolderPath);
         objProductImageDetails.Clear();
         log.WriteLine("Download Product Images From AzureStorage.");

         if (responseAVROFiles.status == Status.OK)
         {
            log.WriteLine("Creation of Zip Files Start.");
            Responses<string> responseZip = processCreateAVROForPartialSync.CreateZipFile();
            log.WriteLine("Creation of Zip Files Start With Status ={0}", responseZip.status);

            if (responseZip.status == Status.OK)
            {
               log.WriteLine("Upload Zip file to Azure Start.");
               filePath = responseZip.responses;

               queueprocessing.ProcessPartialSync(filePath, syncRequest);
            }
         }
      }

   }
}
