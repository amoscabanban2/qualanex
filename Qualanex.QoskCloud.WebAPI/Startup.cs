﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Qualanex.QoskCloud.WebAPI.Startup))]

namespace Qualanex.QoskCloud.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
