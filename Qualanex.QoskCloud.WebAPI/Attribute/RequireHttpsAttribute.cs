﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Qualanex.QoskCloud.WebAPI.Attribute
{
    public sealed class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                using (HttpResponseMessage httpResponseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden))
                {
                    httpResponseMessage.StatusCode = System.Net.HttpStatusCode.Forbidden;
                    httpResponseMessage.ReasonPhrase = Utility.Constants.Https_ResponseMessage_ReasonPhrase;
                    actionContext.Response = httpResponseMessage;
                }                   
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }
}