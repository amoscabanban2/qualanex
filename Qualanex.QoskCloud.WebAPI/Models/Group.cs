﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class Group
    { 
        public int ProfileGroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupType { get; set; }
        public int ProfileCode { get; set; }
        public string Created { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDateTime { get; set; }    
    }
}
