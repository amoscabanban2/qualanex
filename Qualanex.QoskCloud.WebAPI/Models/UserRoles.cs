﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class UserRoles
    {
        public string UserID { get; set; }
        public string RoleCode { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }    
    }
}
