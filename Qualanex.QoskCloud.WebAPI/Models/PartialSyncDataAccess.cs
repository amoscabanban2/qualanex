﻿using Qualanex.Qosk.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using Qualanex.QoskCloud.WebAPI.ResourceFile;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.WebAPI.Models
{
   public class PartialSyncDataAccess
   {

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="syncRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal string GetStatus(SyncRequest syncRequest)
      {
         //PartialSync partialSync = null;
         //try
         //{
         //   using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //   {
         //      var syncData = (from ps in cloudModelEntities.PartialSync
         //                      where ps.RequestId == new Guid(syncRequest.RequestId) && ps.QoskID == syncRequest.KioskID
         //                      select ps).FirstOrDefault();
         //      partialSync = syncData;
         //   }
         //}
         //catch (System.Exception)
         //{
         //   throw;
         //}

         return ""; //(partialSync == null) ? null : partialSync.Status.ToString();
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="syncRequest"></param>
      ///****************************************************************************
      internal void UpdateStatus(SyncRequest syncRequest)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //cloudModelEntities.PartialSync.Add(new PartialSync()
            //{
            //   QoskID = syncRequest.KioskID,
            //   RequestedTime = syncRequest.RequestTime,
            //   RequestId = new Guid(syncRequest.RequestId),
            //   Status = syncRequest.Status.ToString()
            //});
            //cloudModelEntities.SaveChanges();
         }
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="checkVersionRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal CheckVersionResponse CheckSysExeUpdateStatus(CheckVersionRequest checkVersionRequest)
      {
         CheckVersionResponse checkVersionResponse = null;
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //checkVersionResponse = (from sevd in cloudModelEntities.SystemExeVersionDetails
            //                        join setype in cloudModelEntities.SystemExeType on sevd.TypeID equals setype.TypeID
            //                        where setype.Type.Equals(checkVersionRequest.Type) && sevd.Version != (checkVersionRequest.Version)
            //                        select new CheckVersionResponse
            //                        {
            //                           Type = setype.Type,
            //                           Version = sevd.Version,
            //                           Path = sevd.Path,
            //                           ReleaseDate = sevd.ReleaseDate,
            //                           Active = sevd.Active == null ? false : true,
            //                           IsImmediate = sevd.IsImmediate
            //                        }).FirstOrDefault();
         }

         return checkVersionResponse;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="checkVersionRequest"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal bool UpdateImmediateStatus(CheckVersionRequest checkVersionRequest)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //SystemExeVersionDetails systemExeVersionDetails = (from SEVDetails in cloudModelEntities.SystemExeVersionDetails
            //                                                   join SEType in cloudModelEntities.SystemExeType on SEVDetails.TypeID equals
            //                                                      SEType.TypeID
            //                                                   where SEType.Type.Equals(checkVersionRequest.Type)
            //                                                   select SEVDetails).FirstOrDefault();
            //systemExeVersionDetails.IsImmediate = false;
            //int count = cloudModelEntities.SaveChanges();
            //if (count > 0)
            //{
            //   return true;
            //}
         }

         return false;
      }

      ///****************************************************************************
      /// <summary>
      /// Verify whether requested qoskid is exist in the database or not.
      /// </summary>
      /// <param name="qoskID"></param>
      /// <param name="RequestedDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal QoskVerificationResult CheckQoskStatus(string qoskID, DateTime RequestedDate)
      {
         QoskVerificationResult qoskRequest = null;
         try
         {
            var OTPExpirationDate = RequestedDate != null ? RequestedDate.AddDays(7) : DateTime.UtcNow.AddDays(7);

            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               if (!string.IsNullOrEmpty(qoskID))
               {
                  //string PlainOTP = GenerateRandomString();
                  //var Details = (from qoskDetail in cloudModelEntities.Qosk
                  //               join user in cloudModelEntities.User on qoskDetail.ProfileCode equals user.ProfileCode into usertDetails
                  //               from qoskUserDetail in usertDetails.DefaultIfEmpty()
                  //               where qoskDetail.QoskID.Equals(qoskID)
                  //               select new {qoskDetail, qoskUserDetail}).ToList();

                  //if (Details != null && Details.Count > 0)
                  //{
                  //   qoskRequest = new QoskVerificationResult();
                  //   var qDetail = Details[0].qoskDetail;
                  //   qoskRequest.IsValidQosk = true;
                  //   qoskRequest.QoskID = qoskID;
                  //   qoskRequest.RequestedDate = RequestedDate;
                  //   qoskRequest.QoskDetail = new Qualanex.Qosk.ServiceEntities.Qosk();
                  //   qoskRequest.QoskDetail.QoskID = qDetail.QoskID;
                  //   qoskRequest.QoskDetail.Active = qDetail.Active;
                  //   qoskRequest.QoskDetail.ProfileCode = qDetail.ProfileCode;
                  //   qoskRequest.QoskDetail.MachineName = qDetail.MachineName;
                  //   qoskRequest.QoskDetail.CustomerSupportNumber = qDetail.CustomerSupportNumber;
                  //   qoskRequest.QoskDetail.VerificationDate = DateTime.UtcNow;
                  //   qoskRequest.QoskDetail.IsVerified = qDetail.IsVerified;
                  //   qoskRequest.QoskDetail.PasswordExpirationDate = OTPExpirationDate;

                  //   qoskRequest.QoskUser = new List<Users>();
                  //   foreach (var item in Details)
                  //   {
                  //      if (item.qoskUserDetail != null && item.qoskUserDetail.IsDeleted == false && item.qoskUserDetail.Enabled == true)
                  //      {
                  //         Users user = new Users()
                  //         {
                  //            UserID = item.qoskUserDetail.UserID.ToString(),
                  //            UserName = item.qoskUserDetail.UserName,
                  //            FirstName = item.qoskUserDetail.FirstName,
                  //            LastName = item.qoskUserDetail.LastName,
                  //            Email = item.qoskUserDetail.Email,
                  //            ProfileCode = item.qoskUserDetail.ProfileCode,
                  //            Enabled = item.qoskUserDetail.Enabled,
                  //            IsDeleted = item.qoskUserDetail.IsDeleted
                  //         };
                  //         qoskRequest.QoskUser.Add(user);
                  //      }
                  //   }
                  //}

                  if (qoskRequest == null)
                  {
                     qoskRequest = new QoskVerificationResult() {Message = APIResources.QOSK_NOT_REGISTERED};
                  }
                  else if (qoskRequest.QoskDetail.IsVerified == true)
                  {
                     qoskRequest = new QoskVerificationResult() {Message = APIResources.QOSK_ALREADY_REGISTERED};
                  }
                  else if (qoskRequest.QoskUser == null)
                  {
                     qoskRequest = new QoskVerificationResult() {Message = APIResources.QOSK_DOESNOT_HAVE_USERS};
                  }
               }
               else
               {
                  qoskRequest = new QoskVerificationResult() {Message = APIResources.QOSK_ID_REQUIRED};
               }
            }
         }
         catch
         {
            return qoskRequest = new QoskVerificationResult() {Message = APIResources.SOMETHING_WENT_HAPPENED};
         }

         return qoskRequest;
      }

      ///****************************************************************************
      /// <summary>
      ///   Update qosk isverified and verification date field when user verified qosk machine.
      /// </summary>
      /// <param name="request"></param>
      /// <returns></returns>
      ///****************************************************************************
      internal bool UpdateQosk(QoskVerificationResult request)
      {
         try
         {
            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               if (request != null)
               {
                  var isVerified = false;
                  DateTime? verificationDate = null;
                  var verificationStatus = Utility.Constants.VerificationStatus_Fail;
                  var verificationMessage = request.Message;

                  if (request.IsValidQosk)
                  {
                     isVerified = true;
                     verificationDate = request.RequestedDate;
                     verificationStatus = Utility.Constants.VerificationStatus_Success;
                  }

                  //DBModel.Qosk qosk = (from qoskDetail in cloudModelEntities.Qosk
                  //                     where qoskDetail.QoskID == request.QoskID
                  //                     select qoskDetail).FirstOrDefault();
                  //if (qosk != null)
                  //{
                  //   qosk.IsVerified = isVerified;
                  //   qosk.VerificationDate = verificationDate;
                  //   qosk.VerificationStatus = verificationStatus;
                  //   qosk.VerificationMessage = verificationMessage;
                  //   cloudModelEntities.Entry(qosk).State = System.Data.Entity.EntityState.Modified;
                  //   int status = cloudModelEntities.SaveChanges();
                  //   if (status == 1)
                  //      return true;
                  //}
               }
            }
         }
         catch
         {
            return false;
         }

         return false;
      }

   }
}