﻿using System.Collections.Generic;

namespace Qualanex.QoskCloud.WebAPI.Models
{
    /// <summary>
    /// This class will contain all the 
    /// Qosk specfic tables data
    /// </summary>
    public class SystemUpdates
    {

        public List<Qosk> lstQosk { get; set; }
        public List<Profiles> lstProfiles { get; set; }
        public List<Group> lstGroup { get; set; }
        public List<GroupProfiles> lstGroupProfile { get; set; }
        public List<Role> lstRoles { get; set; }
        public List<UserGroups> lstUserGroup { get; set; }
        public List<UserProfiles> lstUserProfile { get; set; }
        public List<UserRoles> lstUserRoles { get; set; }
        public List<User> lstUsers { get; set; }

    }
}