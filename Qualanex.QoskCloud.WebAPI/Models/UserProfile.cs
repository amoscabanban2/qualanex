﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class UserProfiles
    {
        public long UserID { get; set; }
        public int ProfileCode { get; set; }
        public string RegionCode { get; set; }
        public int ProfileGroupID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
    }
}
