﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System;

namespace Qualanex.QoskCloud.WebAPI.Models
{
    // You can add profile data for the user by adding more properties to your Users class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public long UserID { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> UserType { get; set; }
        public string FaxNumber { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public Nullable<int> RowsPerPage { get; set; }
        public Nullable<int> ProfileCode { get; set; }
        public string BiometricSecondFingureScan { get; set; }
        public Nullable<bool> IsModified { get; set; }
        public Nullable<bool> IsTemporaryPassword { get; set; }
        public string BioFingerPrint { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
    /// <summary>
    ///  ApplicationRole
    /// </summary>
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ApplicationRole(string name, string description)
            : base(name)
        {
            this.Description = description;
        }
        public virtual string Description { get; set; }
    }
    /// <summary>
    ///  Application Db Context
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base(Utility.Constants.DBModel_ConnectionString_DefaultConnection)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable(Utility.Constants.DbModelBuilder_Entity_ApplicationUser);
            modelBuilder.Entity<IdentityUserRole>().ToTable(Utility.Constants.DbModelBuilder_Entity_IdentityUserRole);
            modelBuilder.Entity<IdentityUserLogin>().ToTable(Utility.Constants.DbModelBuilder_Entity_IdentityUserLogin);
            modelBuilder.Entity<IdentityUserClaim>().ToTable(Utility.Constants.DbModelBuilder_Entity_IdentityUserClaim).Property(p => p.Id).HasColumnName(Utility.Constants.DbModelBuilder_Column_UserClaimId);
            modelBuilder.Entity<IdentityRole>().ToTable(Utility.Constants.DbModelBuilder_Entity_Role).Property(p => p.Id).HasColumnName(Utility.Constants.DbModelBuilder_Column_RoleId);
        }

    }  
}