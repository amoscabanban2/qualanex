﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class Profiles
    {
        public int ProfileCode { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public byte DisplayOrder { get; set; }
       
        public byte[] Logo { get; set; }
        public string DEANumber { get; set; }
        public Nullable<System.DateTime> DEAExpirationDate { get; set; }
        public string EmailAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string RegionCode { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactFaxNumber { get; set; }
       
        public byte[] PolicySnapshot { get; set; }
        public bool DirectAccountsOnly { get; set; }
        public Nullable<int> RollupProfileCode { get; set; }
        public string TollFreePhoneNumber { get; set; }
        public string Notes { get; set; }
        public string ReturnProcedures { get; set; }
        public string RepresentativeNumber { get; set; }
        public string WholesalerAccountNumber { get; set; }
        public string FaxNumber { get; set; }
        public string LotNumberExpirationDateValidationMethod { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool AlertPendingReturnAuthorizations { get; set; }
        public bool AlertCompletedReturnAuthorizations { get; set; }
        public string Status { get; set; }
        public Nullable<long> VendorID { get; set; }
        public Nullable<System.DateTime> VendorLoadDate { get; set; }
        public string SalesOrg { get; set; }
        public Nullable<bool> QualanexComDisplay { get; set; }
        public string PDMACoordinatorEmailAddress { get; set; }
        public string PDMACoordinatorEmailAddressProcessCompleted { get; set; }
        public long Version { get; set; }

       
    }
}