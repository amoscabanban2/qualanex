﻿using System.Linq;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.Qosk.Library.Model.DBModel;
using System.Collections.Generic;
using AutoMapper;

namespace Qualanex.QoskCloud.WebAPI.Models
{
   public static class NotificationRepository
   {
      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="dbVersion"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetFullDataSyncMasterDB(int dbVersion)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //var updatedVersionExists = dataContext.VersionDetails.Any(v => v.VersionId > dbVersion);

            //if (updatedVersionExists)
            //{
            //   return BlobResource.GetSASUrl(Utility.Constants.AzureAccount_SysUpdates, Utility.Constants.AzureContainer_Full,
            //      Utility.Constants.FileName_Qosklatest + Utility.Constants.FileExtension_Zip);
            //}
            //else
            {
               return Utility.Constants.Qosk_Machine_Is_UpToDate;
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      /// Create the Json format of all the tables data
      /// and return on single object.
      /// </summary>
      /// , int p_DbVersion
      ///****************************************************************************
      [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
      public static SystemUpdates GetFullDataSyncQoskSpecific(string qoskId)
      {
         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //// For Qosk table
            //var qoskData = dataContext.Qosk.Where(q => q.QoskID == qoskId);

            //// For Profiles data.
            //var profiledata = ((from p in dataContext.Profile
            //                    join q in dataContext.Qosk
            //                       on p.ProfileCode equals q.ProfileCode
            //                    select p).Union
            //   (
            //      from p in dataContext.Profiles
            //      join q in dataContext.Qosk
            //         on p.RollupProfileCode equals q.ProfileCode
            //      select p
            //   ).Union
            //   (
            //      from p in dataContext.Profiles
            //      where new[]
            //      {
            //         Utility.Constants.ProfileType_Manufacturer, Utility.Constants.Wholesaler,
            //         Utility.Constants.Repackager
            //      }.Contains(p.Type)
            //      select p
            //   ));

            //// For Group table Data.
            //var groupdata = (from pd in profiledata
            //                 join g in dataContext.Group
            //                    on pd.ProfileCode equals g.ProfileCode
            //                 select g);


            //// For GroupProfile table data
            //var groupProfiledata = (from gp in dataContext.GroupProfiles
            //                        join pd in profiledata
            //                           on gp.ProfileCode equals pd.ProfileCode
            //                        select gp);

            //// For UserProfiles table data
            //var userProfiledata = (from up in dataContext.UserProfiles
            //                       join pd in profiledata
            //                          on up.ProfileCode equals pd.ProfileCode
            //                       select up);

            //// For UserGroups table data
            //var userGroupsdata = (from ug in dataContext.UserGroups
            //                      join upd in userProfiledata
            //                         on ug.ProfileGroupID equals upd.ProfileGroupID
            //                      select ug);

            //// For user data
            //var userdata = (from u in dataContext.User
            //                join pc in profiledata
            //                   on u.ProfileCode equals pc.ProfileCode
            //                select u);

            //// For User Role data.
            //var userRoleData = (from ud in dataContext.Role
            //                    where userdata.Contains(ud.User.FirstOrDefault())
            //                    select ud);

            SystemUpdates systemUpadte = new SystemUpdates
            {
            //   lstQosk = new List<Qosk>(),
            //   lstProfiles = new List<Profiles>(),
            //   lstGroup = new List<Group>(),
            //   lstGroupProfile = new List<GroupProfiles>(),
            //   lstUserProfile = new List<UserProfiles>(),
            //   lstUserGroup = new List<UserGroups>(),
            //   lstUsers = new List<User>(),
            //   lstUserRoles = new List<UserRoles>(),
            //   lstRoles = new List<Role>()
            };

            //if (qoskData != null && qoskData.Count() != 0)
            //{
            //   foreach (DBModel.Qosk item in qoskData.ToList())
            //   {
            //      systemUpadte.lstQosk.Add(GenericUtility.MapListObject(item, new Qosk()));
            //   }
            //}

            //if (profiledata != null && profiledata.Count() != 0)
            //{
            //   foreach (DBModel.Profiles item in profiledata.ToList())
            //   {
            //      systemUpadte.lstProfiles.Add((Profiles)GenericUtility.MapListObject(item, new Profiles()));
            //   }
            //}

            //if (groupdata != null && groupdata.Count() != 0)
            //{
            //   foreach (DBModel.Group item in groupdata.ToList())
            //   {
            //      systemUpadte.lstGroup.Add(GenericUtility.MapListObject(item, new Group()));
            //   }
            //}

            //if (groupProfiledata != null && groupProfiledata.Count() != 0)
            //{
            //   foreach (DBModel.GroupProfiles item in groupProfiledata.ToList())
            //   {
            //      systemUpadte.lstGroupProfile.Add((GroupProfiles)GenericUtility.MapListObject(item, new GroupProfiles()));
            //   }
            //}

            //if (userProfiledata != null && userProfiledata.Count() != 0)
            //{
            //   foreach (DBModel.UserProfiles item in userProfiledata.ToList())
            //   {
            //      systemUpadte.lstUserProfile.Add((UserProfiles)GenericUtility.MapListObject(item, new UserProfiles()));
            //   }
            //}

            //if (userGroupsdata != null && userGroupsdata.Count() != 0)
            //{
            //   foreach (DBModel.UserGroups item in userGroupsdata.ToList())
            //   {
            //      systemUpadte.lstUserGroup.Add((UserGroups)GenericUtility.MapListObject(item, new UserGroups()));
            //   }
            //}

            //if (userdata != null && userdata.Count() != 0)
            //{
            //   foreach (DBModel.User item in userdata.ToList())
            //   {
            //      systemUpadte.lstUsers.Add((User)GenericUtility.MapListObject(item, new User()));
            //   }
            //}

            //if (userRoleData != null && userRoleData.Count() != 0)
            //{
            //   foreach (DBModel.Role item in userRoleData.ToList())
            //   {
            //      Mapper.CreateMap<DBModel.Role, Role>().ForSourceMember(x => x.User, y => y.Ignore());
            //      systemUpadte.lstRoles.Add(Mapper.Map<Role>(item));
            //   }
            //}

            return systemUpadte;
         }
      }

   }
}