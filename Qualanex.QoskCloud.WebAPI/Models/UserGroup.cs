﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class UserGroups
    {
        public int ID { get; set; }
        public int ProfileGroupID { get; set; }
        public long UserID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDateTime { get; set; }
    }
}
