﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class Role
    {
        public string RoleCode { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
