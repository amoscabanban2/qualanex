﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class Qosk
    {
        public string QoskID { get; set; }
        public string MachineName { get; set; }
        public Nullable<bool> Active { get; set; }
        public int ProfileCode { get; set; }
        public string CustomerSupportNumber { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
