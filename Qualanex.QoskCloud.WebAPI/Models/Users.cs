﻿using System;
namespace Qualanex.QoskCloud.WebAPI.Models
{
    public class User
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> ProfileCode { get; set; }
        public string Email { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public Nullable<int> RowsPerPage { get; set; }
        public string BiometricFingureScan { get; set; }
        public string BiometricSecondFingureScan { get; set; }
        public Nullable<bool> IsModified { get; set; }
        public Nullable<bool> IsSignin { get; set; }
    }
}
