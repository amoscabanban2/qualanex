﻿using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using System.Net.Http.Headers;
namespace Qualanex.QoskCloud.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.EnableCors();

            config.Routes.MapHttpRoute(
                name: Utility.Constants.Https_Default_Route_Name,
                routeTemplate: Utility.Constants.Https_Default_Route_Template,
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue(Utility.Constants.Https_MediaType));
        }
    }
}
