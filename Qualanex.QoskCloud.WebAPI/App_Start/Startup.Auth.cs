﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Qualanex.QoskCloud.WebAPI.Providers;
using Qualanex.QoskCloud.WebAPI.Models;
namespace Qualanex.QoskCloud.WebAPI
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId =Utility.Constants.PublicClientId;
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString(Utility.Constants.TokenEndpointPath),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString(Utility.Constants.AuthorizeEndpointPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(Utility.Constants.AccessTokenExpireTimeSpan),
                AllowInsecureHttp = true  // TODO: Set to False for production! grf.
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
            
            // Enable CORS
            app.Use(async (context, next) =>
            {
                IOwinRequest req = context.Request;
                IOwinResponse res = context.Response;
                if (req.Path.StartsWithSegments(new PathString(Utility.Constants.TokenEndpointPath)))
                {
                    var origin = req.Headers.Get(Utility.Constants.Origin);
                    if (!string.IsNullOrEmpty(origin))
                    {
                        res.Headers.Set(Utility.Constants.IOwinResponse_Header_Access_Control_Allow_Origin, origin);
                    }
                    if (req.Method == Utility.Constants.IOwinRequest_Method_OPTIONS)
                    {
                        res.StatusCode = 200;
                        res.Headers.AppendCommaSeparatedValues(Utility.Constants.IOwinRequest_Key_Access_Control_Allow_Methods, Utility.Constants.Https_Method_Get, Utility.Constants.Https_Method_Post);
                        res.Headers.AppendCommaSeparatedValues(Utility.Constants.IOwinRequest_Key_Access_Control_Allow_Headers, Utility.Constants.Https_Values_Authorization, Utility.Constants.Https_Values_ContentType);
                        return;
                    }
                }
                await next();
            });
        }
    }
}
