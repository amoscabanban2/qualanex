﻿using System.Web.Http;
using Qualanex.QoskCloud.WebAPI.Models;
using System.Web.Http.Cors;

namespace Qualanex.QoskCloud.WebAPI.Controllers
{
    [Authorize]
    [EnableCors(origins: Utility.Constants.Default_Access, headers: Utility.Constants.Default_Access, methods: Utility.Constants.Default_Access)]
    [RoutePrefix(Utility.Constants.RoutePrefix_ApiNotification)]
    public class NotificationController : ApiController
    {
        /// <summary>
        /// This will return the latest db backup's  
        /// "Master DB" avro files path
        /// Check if any updated version is available then return path 
        /// Else return the "Qosk machine is up-to date"
        /// </summary>    
        [AcceptVerbs(Utility.Constants.Https_Method_Get)]
        [Route(Utility.Constants.RouteTemplate_FullDataSyncMasterDB)]       
        public string GetFullDataSyncMasterDB(int dbVersion)
        {
            // Is this used? grf? Need to add communication queue entry!
            return NotificationRepository.GetFullDataSyncMasterDB(dbVersion);
        }

        /// <summary>
        /// this will return the qosk specific tables data 
        /// in json format.
        /// </summary>
        [AcceptVerbs(Utility.Constants.Https_Method_Get)]
        [Route(Utility.Constants.RouteTemplate_FullDataSyncQoskSpecific)]
        public SystemUpdates GetFullDataSyncQoskSpecific(string qoskId)
        {
            // Is this used? grf? Need to add communication queue entry!
            return NotificationRepository.GetFullDataSyncQoskSpecific(qoskId);
        }
    }
}