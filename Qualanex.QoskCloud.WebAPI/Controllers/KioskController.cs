﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Qualanex.QoskCloud.WebAPI.Models;
using System.Diagnostics;
using System.Web.Http.Cors;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.Qosk.ServiceEntities;
using Qualanex.QoskCloud.Utility;
using System.Collections.Generic;
using System.Threading;

namespace Qualanex.QoskCloud.WebAPI.Controllers
{
   [Authorize]
   [EnableCors(origins: Constants.Default_Access, headers: Constants.Default_Access, methods: Constants.Default_Access)]
   [RoutePrefix(Constants.RoutePrefix_ApiKiosk)]
   public class KioskController : ApiController
   {
      [Route(Constants.RouteTemplate_FullSync)]
      [HttpPost]
      public async Task<IHttpActionResult> RequestFullSyncAsync(FullSyncRequest request)
      {
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("RequestFullSync"));
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_FullSync, request);
            return Ok();
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
      }


      [Route(Constants.RouteTemplate_PartialSync)]
      [HttpPost]
      public async Task<IHttpActionResult> ExecutePartialSyncAsync(SyncRequest request)
      {
         var response = new SyncResponse();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("ExecutePartialSync"));

            ProcessCreateAVROForPartialSync processCreateAVROForPartialSync = new ProcessCreateAVROForPartialSync(request);
            if (processCreateAVROForPartialSync.IsUpdateAvailable())
            {
               response.UpdatesAvailable = true;
               await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_PartialSync, request);
            }
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_CheckPartialSyncStatus)]
      [HttpPost]
      public async Task<IHttpActionResult> GetPartialSyncStatusAsync(SyncRequest request)
      {
         var response = new SyncResponse();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("GetPartialSyncStatus"));

            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            string partialSyncStatus = await Task.Run(() => partialSyncDataAccess.GetStatus(request));
            if (!string.IsNullOrEmpty(partialSyncStatus))
               response.Status = (SyncStatus)Enum.Parse(typeof(SyncStatus), partialSyncStatus);
            else
            {
               response.Status = SyncStatus.Error;
            }
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_UpdateStatus)]
      [HttpPost]
      public async Task<IHttpActionResult> PutSyncStatusAsync(SyncRequest request)
      {
         var response = new SyncResponse();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("PutSyncStatus"));

            request.Status = SyncStatus.Finished;
            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            await Task.Run(() => partialSyncDataAccess.UpdateStatus(request));
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok();
      }


      [Route(Constants.RouteTemplate_BatchReturns)]
      [HttpPost]
      public async Task<IHttpActionResult> SubmitBatchReturnsAsync(BatchReturnsRequest request)
      {
         var tasks = new List<Task>();
         try
         {
            Trace.WriteLine($"Batch returns submitted by Qosk:{request.QoskMachineID} for Return item {request.BatchId}");
            tasks.Add(Task.Factory.StartNew(() => QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync,
                Constants.Azure_Queue_Communications, request.GetBase("SubmitBatchReturns"))));

            tasks.Add(Task.Factory.StartNew(() => QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync,
                Constants.Azure_Queue_BatchReturns, request)));

            await Task.WhenAll(tasks.ToArray());
            Trace.WriteLine($"Batch returns queued for processing by Qosk:{request.QoskMachineID} for Return item {request.BatchId}");
         }
         catch (Exception ex)
         {
            // Aggregate Exception here..
            foreach (var task in tasks.Where(t => t.IsFaulted))
            {
               Trace.WriteLine($"Return item {request.BatchId} failed processing with exception: {task.Exception.ToString()}");
            }

            return InternalServerError(ex);
         }
         return Ok();
      }

      [Route(Constants.RouteTemplate_ResourceURI)]
      [HttpPost]
      public async Task<IHttpActionResult> GetSASUriAsync(ResourceRequest request)
      {
         var response = new ResourceResponse();
         try
         {
            // Validate parameters
            if (string.IsNullOrEmpty(request.RequestId) || string.IsNullOrEmpty(request.ResourceName))
               throw new ArgumentNullException("RequestId and ResourceName required");

            // All Resource requests ARE Case-sensitive!!!  Fix grf below wait()...
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("GetSASUri"));

            //e.g., blob://qoskkiosksync/returnmedia/filename.zip
            //e.g., queue://qoskkiosksync/batchreturn

            string storageType = string.Empty, accountName = string.Empty, containerName = string.Empty,
                    fileName = string.Empty, queueName = string.Empty;
            var path = request.ResourceName.Split(new string[] { Constants.Colon + Constants.Double_Forward_Slash, Constants.Single_Forward_Slash }, StringSplitOptions.None);
            var resName = request.ResourceName;

            if (((resName.StartsWith(Constants.ResourceName_Blob + Constants.Colon + Constants.Double_Forward_Slash)
                || resName.StartsWith(Constants.ResourceName_Queue + Constants.Colon + Constants.Double_Forward_Slash)
                || resName.StartsWith(Constants.ResourceName_Table + Constants.Colon + Constants.Double_Forward_Slash)) == false) || path.Count() < 3)
               throw new ArgumentNullException("Resource Name invalid, StorageType, AccountName and Container missing, should be blob://accountName/container/filename");

            Trace.WriteLine(string.Format("User:{0} requests access to:{1}", this.User.Identity.Name, request.ResourceName), "Info");

            // The account passed in should not be considered a physical account
            // For now, we will map the account to the config entry for the correct environment
            // Later, we may map to a qosk specific blob storage for horozontal scalability -grf.

            storageType = path[0];
            accountName = path[1];

            switch (storageType.ToLower())
            {
               case Constants.ResourceName_Blob:
                  containerName = path[2];
                  if (path.Count() > 3)
                     fileName = string.Join(Constants.Single_Forward_Slash, path.Skip(3).ToArray());
                  response.ResourceURI = BlobResource.GetSASUrl(accountName, containerName, fileName, request.ResourceAccess);
                  break;
               case Constants.ResourceName_Queue:
                  queueName = path[2];
                  response.ResourceURI = QueueResource.GetSASUrl(accountName, queueName, request.ResourceAccess);
                  break;
               default:
                  throw new ArgumentException("Unsupported Storage Type: " + storageType);
            }
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            using (HttpResponseMessage httpRequestMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message))
            {
               throw new HttpResponseException(httpRequestMessage);
            }
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_SysExeSync)]
      [HttpPost]
      public async Task<IHttpActionResult> GetSysExeAvalabilityAsync(CheckVersionRequest request)
      {
         var response = new CheckVersionResponse();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("GetSysExeAvalability"));

            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            response = await Task.Run(() => partialSyncDataAccess.CheckSysExeUpdateStatus(request));

         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_UpdateSysExeImmediateStatus)]
      [HttpPost]
      public async Task<IHttpActionResult> SetImmediateSysExeStatusAsync(CheckVersionRequest request)
      {
         var response = new UpdateResponse();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("SetImmediateSysExeStatus"));

            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            bool isUpdate = await Task.Run(() => partialSyncDataAccess.UpdateImmediateStatus(request));
            if (isUpdate)
            {
               response.IsUpdateSuccess = true; ;
            }
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_RequestOTP)]
      [HttpPost]
      public async Task<IHttpActionResult> RequestQoskPasswordAsync(SyncRequest request)
      {
         var response = new QoskVerificationResult();
         try
         {
            await QueueResource.AddItemAsync(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_Communications, request.GetBase("RequestQoskPassword"));

            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            response.QoskID = request.QoskMachineID;
            response = await Task.Run(() => partialSyncDataAccess.CheckQoskStatus(request.QoskMachineID, request.RequestTime));
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(response);
      }

      [Route(Constants.RouteTemplate_UpdateQoskVerificationResult)]
      [HttpPost]
      public async Task<IHttpActionResult> SetQoskVerificationResultAsync(QoskVerificationResult request)
      {
         bool result = false;
         try
         {
            // This is a hack until we remove the ServiceEntities reference! grf
            PartialSyncDataAccess partialSyncDataAccess = new PartialSyncDataAccess();
            result = await Task.Run(() => partialSyncDataAccess.UpdateQosk(request));
         }
         catch (Exception ex)
         {
            var message = string.Format("{0}", ex.ToString());
            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
         }
         return Ok(result);
      }

   }
}
