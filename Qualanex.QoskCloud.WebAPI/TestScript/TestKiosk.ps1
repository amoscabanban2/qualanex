﻿# The purpose of this powershell script is to test the webapi kiosk resource requests -grf
# To run - must run powershell under ADMIN, then Set-ExecutionPolicy Unrestricted
cls
set-psdebug -trace 0

# $server = "http://api.dvqosk.com"
$server = "http://localhost:30235" 

# Get Token for Authentication
echo " "
echo "Authenticating to $server"
$tokenReq = "grant_type=password&username=Admin&password=Test@123"
$token = Invoke-RestMethod -Uri $server/Token -Method Post -ContentType "application/x-www-form-urlencoded" -Body $tokenReq | ForEach{ $_.access_token }
$token

# Get Container SAS Uri
echo " "
echo "Requesting SAS Uri to Kiosksync/returnmedia"
$auth = @{ Authorization = "Bearer " + $token }
# The base header passed on every call
$header = "RequestId:""" + [System.GUID]::NewGuid().ToString() + 
    """,KioskId:""K1001"",ProgramVersion:""1.00"",LastUpdateDate:""2016-01-01T00:00:00Z"",RequestTime:""" + [System.DateTime]::UtcNow.ToString("s") + "Z"""
$blobReq = "{$header,ResourceName:""blob://qoskimages/images"",ResourceAccess:""Read""}" 
$resUri = Invoke-RestMethod -Uri $server/api/kiosk/ResourceURI -Method Post -ContentType "application/json" -Body $blobReq -Headers $auth | ForEach{ $_.ResourceURI }
$resUri

# List blobs in Container SAS Uri
echo " "
echo "Enumerating block blobs in Kiosksync/images"
$xml = [xml](New-Object System.Net.WebClient).DownloadString($resUri)
$xml.EnumerationResults.Blobs.Blob | Format-Table Name

# Get Blob SAS Uri (first)
$blob = $xml.EnumerationResults.Blobs.Blob | select -First 1 | ForEach{ $_.Name }
echo " "
echo "Requesting SAS Uri for file: $blob in qoskimages/images"
# The base header passed on every call
$header = "RequestId:""" + [System.GUID]::NewGuid().ToString() + 
    """,KioskId:""K1001"",ProgramVersion:""1.00"",LastUpdateDate:""2016-01-01T00:00:00Z"",RequestTime:""" + [System.DateTime]::UtcNow.ToString("s") + "Z"""
$blobReq = "{$header,ResourceName:""blob://qoskimages/images/" + $blob + """,ResourceAccess:""Read""}" 
$resUri = Invoke-RestMethod -Uri $server/api/kiosk/ResourceURI -Method Post -ContentType "application/json" -Body $blobReq -Headers $auth | ForEach{ $_.ResourceURI }
$resUri

# Get Blob - returns 404 if not exists
echo " "
echo "Downloading contents for file: $blob in qoskimages/images"
$xml = (New-Object System.Net.WebClient).DownloadString($resUri)
$xml

# Get Queue SAS Uri
echo " "
echo "Requesting SAS Uri to Kiosksync/communication"
$auth = @{ Authorization = "Bearer " + $token }
# The base header passed on every call
$header = "RequestId:""" + [System.GUID]::NewGuid().ToString() + 
    """,KioskId:""K1001"",ProgramVersion:""1.00"",LastUpdateDate:""2016-01-01T00:00:00Z"",RequestTime:""" + [System.DateTime]::UtcNow.ToString("s") + "Z"""
$blobReq = "{$header,ResourceName:""queue://qoskkiosksync/communication"",ResourceAccess:""Add"",Timeout:10}" 
$resUri = Invoke-RestMethod -Uri $server/api/kiosk/ResourceURI -Method Post -ContentType "application/json" -Body $blobReq -Headers $auth | ForEach{ $_.ResourceURI }
$resUri

# Request Write SAS Uri to Container Test if not exist
echo " "
echo "Requesting SAS Uri to blob container: Kiosksync/test, create if doesn't exist"
$auth = @{ Authorization = "Bearer " + $token }
# The base header passed on every call
$header = "RequestId:""" + [System.GUID]::NewGuid().ToString() + 
    """,KioskId:""K1001"",ProgramVersion:""1.00"",LastUpdateDate:""2016-01-01T00:00:00Z"",RequestTime:""" + [System.DateTime]::UtcNow.ToString("s") + "Z"""
$blobReq = "{$header,ResourceName:""blob://qoskkiosksync/test"",ResourceAccess:""Write"",Timeout:10}" 
$resUri = Invoke-RestMethod -Uri $server/api/kiosk/ResourceURI -Method Post -ContentType "application/json" -Body $blobReq -Headers $auth | ForEach{ $_.ResourceURI }
$resUri
