﻿using Newtonsoft.Json;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{
    public class FullDataSync
    {

        private BlobStorageHandler blobStorageHandler;
        private DatabaseHandler databaseHandler;


        /// <summary>
        /// Create Nightly Scripts for Full Synchronization
        /// </summary>
        /// <param name="log">Text writer logging object</param>
        /// <returns>Task completion status</returns>
        public async Task ScriptProcessFullSync()
        {
            try
            {
                var syncTableHelper = new SyncTableHelper();
                var tables = syncTableHelper.ReadTables();

                blobStorageHandler = new BlobStorageHandler(Constants.AzureAccount_SysUpdates, Constants.AzureContainer_Full);
                databaseHandler = new DatabaseHandler(ConfigurationManager.GetConnectionStringByName("QualanexQoskCloudEntities"));
                var fileHandler = new FileHandler();
                
                List<string> files = new List<string>();

                //Create a dictionary of table scripts to be uploaded to blob storage.
                foreach (var table in tables)
                {
                    files.Add(databaseHandler.CreateScriptToFile(fileHandler.TemporaryFilePath, table));
                }

                //Create a set of tasks to upload to blob storage.
                var tasks = files.Select(async t =>
                {
                    await blobStorageHandler.UploadTableAsync(t);
                }
                ).ToArray();

                try
                {
                    await Task.WhenAll(tasks);
                    {
                        if (Directory.Exists(fileHandler.TemporaryFilePath))
                        {
                            Directory.Delete(fileHandler.TemporaryFilePath, true);
                        }
                    }
                }
                catch (Exception x)
                {
                    foreach (var task in tasks.Where(t => t.IsFaulted))
                    {
                        Logger.Error(task.Exception);
                    }
                    Logger.Error(x);
                    throw;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
            finally
            {
                Logger.Info("ScriptProcessFullSync Complete.");
            }

        }

    }
}
