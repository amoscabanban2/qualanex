﻿using System.IO;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Qualanex.QoskCloud.Utility.Common;
using System.Collections.Generic;
using Qualanex.Qosk.ServiceEntities;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Diagnostics;
using System.Text;

namespace Qualanex.QoskCloud.SyncDataJob
{
    public class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called queue
        [NoAutomaticTrigger]
        public static void WriteSyncDataToBlob(TextWriter log, int value, [Queue(Constants.Azure_Queue_Queue)] out string message)
        {
            message = value.ToString();
            try
            {
                log.WriteLine("Function is invoked with value={0}", value);
                var fullDataSync = new FullDataSync();
                fullDataSync.ScriptProcessFullSync().Wait();
                log.WriteLine("Following message will be written on the Queue={0}", message);
            }
            catch (System.Exception ex)
            {
                log.WriteLine("Error = {0}", ex.Message);
            }
        }
    }
}
