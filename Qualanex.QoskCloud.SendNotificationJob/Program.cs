﻿using System.Collections.Generic;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.SendNotificationJob
{
    public class Program
    { 
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main(string[] args)
        {          
            System.Console.WriteLine("Main() function called in SendNotificationJob");
         using (JobHost host = new JobHost())
         {
            host.RunAndBlock();
         }
         //To Test in local
         //Functions.SentNotification(new ReportRequest() { BlobFilePath = "http://google.com", ReportID = 1, ReportName = "Sample_Report_Retailer_Box_Detail", ReportRunGuid = System.Guid.NewGuid(), ReportScheduleID = 3, ReportScheduleTitle = "My Schedule Report", UserEmailList = new List<string>() { "dlemery@qualanex.com" } }, new System.IO.StringWriter());
      }
    }
}
