﻿using System;
using Qualanex.QoskCloud.Utility;
using System.Text;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;
using System.Linq;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.SendNotificationJob
{
    public class QueueProcessingEmail
    {
        string NetworkUserEmail { get; set; }
        string NetworkUserPassword { get; set; }
        string NetworkSMTP { get; set; }
        string ReportSubject { get; set; }
        string MessageBody { get; set; }
        ReportRequest objReportRequest { get; set; }

        /// <summary>
        /// Constructor initialiaze all the variables.
        /// </summary>
        /// <param name="reportRequest"></param>
        public QueueProcessingEmail(ReportRequest reportRequest)
        {
            objReportRequest = reportRequest;
        }
        /// <summary>
        /// Send mail to users.
        /// </summary>
        public void SendEmailToUser()
        {
         Console.WriteLine("Begin SendEmail");
            InitializeMailVariables();
           if (objReportRequest != null && !string.IsNullOrEmpty(NetworkUserEmail) &&
               !string.IsNullOrEmpty(NetworkUserPassword) && !string.IsNullOrEmpty(NetworkSMTP)
               && objReportRequest.UserEmailList != null
               && objReportRequest.UserEmailList.Count > 0)
           {
              if (CreateMessageBody())
              {
               Console.WriteLine("Attempt to send");
                 SendUserMail.SendEmail(null, string.Empty, objReportRequest.UserEmailList, ReportSubject, MessageBody,
                    NetworkUserEmail, NetworkUserPassword, NetworkSMTP);
              }

           }
           else
           {
              Console.WriteLine("Sending Canceled due to missing information");
           }
        }

        /// <summary>
        /// Temporary Email Body.
        /// </summary>
        /// <returns></returns>
        public bool CreateMessageBody()
        {
         Console.WriteLine("Building message body");
            try
            {
                StringBuilder strMessagebuilder = new StringBuilder();
                strMessagebuilder.AppendLine("<b>Hi User,</b></br>");
                strMessagebuilder.AppendLine("</br>");
                strMessagebuilder.AppendLine("Your Report is generated</br>");
                strMessagebuilder.AppendLine("<b>Original Report Name: </b>" + objReportRequest.ReportName + "</br>");
                strMessagebuilder.AppendLine("<b>Schedule Report Title: </b>" + objReportRequest.ReportScheduleTitle + "</br>");
                strMessagebuilder.AppendLine("<b>Click on following link to view this report.<b></br>");
                strMessagebuilder.AppendLine("<a href=\"" + objReportRequest.BlobFilePath + "\">View Report</a></br>");
                strMessagebuilder.AppendLine("</br>");
                strMessagebuilder.AppendLine("Thanks</br>");
                strMessagebuilder.AppendLine("Qualanex</br>");
                MessageBody = strMessagebuilder.ToString();
            Console.WriteLine("Message body built:" + MessageBody);
                return true;
            }
            catch
            {
            Console.WriteLine("Message body failed");
                return false;
            }
        }

        /// <summary>
        /// Initialize class variables.
        /// </summary>
        /// <returns></returns>
        public void InitializeMailVariables()
        {
            try
            {
            Console.WriteLine("Initializing variables...");
                ScheduleReportDataAccess scheduleReportDataAccess = new ScheduleReportDataAccess();
                List<AppConfigurationSetting> lstAzureConfigDetails = scheduleReportDataAccess.QoskCloudConfigurationSetting(Utility.Constants.Settings_QoskCloud);
            Console.WriteLine("Getting app configuration settings");
               if (lstAzureConfigDetails != null && lstAzureConfigDetails.Count > 0)
               {
                  ReportSubject = "Qualanex Report Created.";
                  NetworkUserEmail =
                     lstAzureConfigDetails.Where(
                           x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialUserName))
                        .FirstOrDefault()
                        .Value.ToString();
                  NetworkUserPassword =
                     lstAzureConfigDetails.Where(
                           x => x.keyName.Equals(Utility.Constants.UserController_NetworkCredentialPassword))
                        .FirstOrDefault()
                        .Value.ToString();
                  NetworkSMTP =
                     lstAzureConfigDetails.Where(x => x.keyName.Equals(Utility.Constants.UserController_CorporateSMTP))
                        .FirstOrDefault()
                        .Value.ToString();
                  Console.WriteLine(NetworkUserPassword + " " + NetworkUserEmail + " " + NetworkSMTP);
               }
               else
               {
                  Console.WriteLine("Null or empty config details!");
               }
                
            }
            catch
            {
            Console.WriteLine("Exception thrown during mail variable initialization");
                throw;
            }
        }
        
    }
}
