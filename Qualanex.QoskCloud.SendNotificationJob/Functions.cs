﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.Web.Areas.Report.Models;

namespace Qualanex.QoskCloud.SendNotificationJob
{
    public class Functions
    {

        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called NotificationQueue      
        public static void SentNotification([QueueTrigger(Constants.Azure_Queue_ReportNotificationQueue)] ReportRequest objReportRequest, TextWriter log)
        {
            Console.Write("Processing Queue Message {0}", objReportRequest);
            try
            {
                QueueProcessingEmail queueprocessing = new QueueProcessingEmail(objReportRequest);
                ScheduleReportDataAccess objScheduleReportDataAccess = new ScheduleReportDataAccess();
                objScheduleReportDataAccess.UpdateScheduleReportStatus(objReportRequest.ReportRunGuid, ReportRunningStatus.NotificationSending.ToString(), Constants.ReportMessage_Notification_Sending, null, System.DateTime.UtcNow);
                queueprocessing.SendEmailToUser();
                objScheduleReportDataAccess.UpdateScheduleReportStatus(objReportRequest.ReportRunGuid, ReportRunningStatus.NotificationSent.ToString(), Constants.ReportMessage_Notification_Sent, null, System.DateTime.UtcNow);
                Console.WriteLine("Processing Complete for Message {0}", objReportRequest);

            }
            catch (System.Exception ex)
            {
                Console.Write("Error: {0}", ex.ToString());
                throw;
            }
        }
    }
}
