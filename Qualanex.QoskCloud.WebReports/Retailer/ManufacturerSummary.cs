using System;
using Qualanex.QoskCloud.WebReports.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.WebReports.Retailer
{
    /// <summary>
    /// Summary description for ManufacturerSummary.
    /// </summary>
    public partial class ManufacturerSummary : Telerik.Reporting.Report
    {
        public ManufacturerSummary()
        {
            InitializeComponent();
            this.NeedDataSource += ManufacturerSummary_NeedDataSource;
        }
        /// <summary>
        /// ManufacturerSummary_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManufacturerSummary_NeedDataSource(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
            long userID = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_ManufacturerSummaryListPanel, userID);
            if (subscribedColumns.Count > 0)
            {
                ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
                reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail);
                entityDataSource.Context = typeof(Qualanex.QoskCloud.WebReports.Models.ReportDataContext);
                entityDataSource.ContextMember = "Get_Manufacturer_Summary_Details";
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_MFGLabeler, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_MFGLabeler].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_ManufacturerName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_ManufacturerName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_RetailerVendorNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_RetailerVendorNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_DebitMemoInvoiceNo, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_DebitMemoInvoiceNo].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalProductValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_TotalProductValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_TotalQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_ProcessingFee, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_ProcessingFee].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalInvoiceAmount, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ManufacturerSummary_TotalInvoiceAmount].Value);

                entityDataSource.Name = "entityDataSource";

                this.DataSource = entityDataSource;
            }
        }
    }
}