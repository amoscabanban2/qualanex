using System;
using Qualanex.QoskCloud.WebReports.Models;
using Qualanex.QoskCloud.Entity;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.WebReports.Retailer
{
    /// <summary>
    /// Summary description for Pharmacy Summary.
    /// </summary>
    public partial class PharmacySummary : Telerik.Reporting.Report
    {
        public PharmacySummary()
        {
            InitializeComponent();
            this.NeedDataSource += PharmacySummary_NeedDataSource;
        }
        /// <summary>
        /// PharmacySummary_NeedDataSource: Used for calling GenerateReportDefinition method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PharmacySummary_NeedDataSource(object sender, System.EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Telerik.Reporting.EntityDataSource entityDataSource = new Telerik.Reporting.EntityDataSource();
            long userID = Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value);
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            List<ColumnDetails> subscribedColumns = commonDataAccess.GetOnlySubscribedColumns(Utility.Constants.Report_PharmacySummaryListPanel, userID);
            if (subscribedColumns.Count > 0)
            {
                ReportExpressionFuctions reportExpressionFuctions = new ReportExpressionFuctions();
                reportExpressionFuctions.GenerateReportDefinition(Convert.ToInt64(report.Parameters[Utility.Constants.Report_Parameter_UserID].Value), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value, subscribedColumns, this, this.panel1, this.detail);
                entityDataSource.Context = typeof(Qualanex.QoskCloud.WebReports.Models.ReportDataContext);
                entityDataSource.ContextMember = "Get_Pharmacy_Summary_Details";
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataStartDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataStartDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_DataEndDate, typeof(DateTime?), report.Parameters[Utility.Constants.Report_Parameter_DataEndDate].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_Timezone, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_ReportScheduleTimeZone].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_FileType, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_FileType].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_StoreNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_StoreNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_StoreName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_StoreName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_WholesalerCustomerNumber, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_WholesalerCustomerNumber].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_RegionName, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_RegionName].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableQty].Value);

                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableQty].Value);

                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_TotalProductValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_TotalProductValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_TotalQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_TotalQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_ProcessingFeeBilled, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_ProcessingFeeBilled].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_TotalInvoiceAmount, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_TotalInvoiceAmount].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_TotalDebitMemoQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_TotalDebitMemoQty].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockValue, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockValue].Value);
                entityDataSource.Parameters.Add(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockQty, typeof(string), report.Parameters[Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockQty].Value);

                entityDataSource.Name = "entityDataSource";

                this.DataSource = entityDataSource;
            }
        }

    }
}