namespace Qualanex.QoskCloud.WebReports.Retailer
{
    partial class ManufacturerSummary
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManufacturerSummary));
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter5 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter6 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter7 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter8 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter9 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter10 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter11 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter12 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter13 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter14 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter15 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter16 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter17 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter18 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.txtReportName = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.txtReportGeneratedTime = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtReportName});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(1.9868215517249155E-08D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(127)))), ((int)(((byte)(182)))));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // txtReportName
            // 
            this.txtReportName.CanGrow = false;
            this.txtReportName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.10007866472005844D));
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.txtReportName.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtReportName.Style.Color = System.Drawing.Color.White;
            this.txtReportName.Style.Font.Bold = true;
            this.txtReportName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtReportName.Value = "Manufacturer Summary";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20007880032062531D);
            this.detail.Name = "detail";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.6999213695526123D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox61,
            this.txtReportGeneratedTime});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(11.200000762939453D), Telerik.Reporting.Drawing.Unit.Inch(0.29992151260375977D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999994516372681D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox61.Value = "=PageNumber";
            // 
            // txtReportGeneratedTime
            // 
            this.txtReportGeneratedTime.Format = "{0}";
            this.txtReportGeneratedTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.29992151260375977D));
            this.txtReportGeneratedTime.Name = "txtReportGeneratedTime";
            this.txtReportGeneratedTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6999213695526123D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.txtReportGeneratedTime.Value = resources.GetString("txtReportGeneratedTime.Value");
            // 
            // ManufacturerSummary
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageFooterSection1});
            this.Name = "Manufacturer Summary";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.5D), Telerik.Reporting.Drawing.Unit.Inch(11D));
            reportParameter1.AllowNull = true;
            reportParameter1.Name = "ReportDataTimeZone";
            reportParameter1.Value = "";
            reportParameter2.AllowNull = true;
            reportParameter2.Name = "DataStartDate";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter2.Value = "";
            reportParameter3.AllowNull = true;
            reportParameter3.Name = "DataEndDate";
            reportParameter3.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter3.Value = "";
            reportParameter4.AllowNull = true;
            reportParameter4.Name = "ReportScheduleTimeZone";
            reportParameter4.Value = "";
            reportParameter5.AllowNull = true;
            reportParameter5.Name = "MFGLabeler";
            reportParameter6.AllowNull = true;
            reportParameter6.Name = "ManufacturerName";
            reportParameter7.AllowNull = true;
            reportParameter7.Name = "RetailerVendorNumber";
            reportParameter8.AllowNull = true;
            reportParameter8.Name = "FileType";
            reportParameter9.AllowNull = true;
            reportParameter9.Name = "DebitMemoInvoiceNo";
            reportParameter10.AllowNull = true;
            reportParameter10.Name = "CreditableValue";
            reportParameter11.AllowNull = true;
            reportParameter11.Name = "UserID";
            reportParameter11.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter12.AllowNull = true;
            reportParameter12.Name = "CreditableQty";
            reportParameter13.AllowNull = true;
            reportParameter13.Name = "RecallCreditableValue";
            reportParameter14.AllowNull = true;
            reportParameter14.Name = "RecallCreditableQty";
            reportParameter15.AllowNull = true;
            reportParameter15.Name = "TotalProductValue";
            reportParameter16.AllowNull = true;
            reportParameter16.Name = "TotalQty";
            reportParameter17.AllowNull = true;
            reportParameter17.Name = "ProcessingFee";
            reportParameter18.AllowNull = true;
            reportParameter18.Name = "TotalInvoiceAmount";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.ReportParameters.Add(reportParameter5);
            this.ReportParameters.Add(reportParameter6);
            this.ReportParameters.Add(reportParameter7);
            this.ReportParameters.Add(reportParameter8);
            this.ReportParameters.Add(reportParameter9);
            this.ReportParameters.Add(reportParameter10);
            this.ReportParameters.Add(reportParameter11);
            this.ReportParameters.Add(reportParameter12);
            this.ReportParameters.Add(reportParameter13);
            this.ReportParameters.Add(reportParameter14);
            this.ReportParameters.Add(reportParameter15);
            this.ReportParameters.Add(reportParameter16);
            this.ReportParameters.Add(reportParameter17);
            this.ReportParameters.Add(reportParameter18);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }        

        #endregion
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;        
        private Telerik.Reporting.TextBox textBox61;        
        private Telerik.Reporting.TextBox txtReportGeneratedTime;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox txtReportName;
        
    }
}