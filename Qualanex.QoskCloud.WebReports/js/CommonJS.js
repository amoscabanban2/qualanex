﻿function resolveUrl() {
    return "";//"/Qualanex-Reports";
}
function PhoneNumberMask(e)
{
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
}

function NumericOnly(obj) {
    if (obj.value.length > 0) {
        
        obj.value = obj.value.replace(/[^\d]+/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}
function NumericWithDashOnly(obj) {
    if (obj.value.length > 0) {
        obj.value = obj.value.replace(/[^[0-9]+(-[0-9]+)+$]/g, ''); // This would validate the inputs for allowing only numeric chars
    }
}


function NumericWithDecimalOnly(obj) {
    if (obj.value.length > 0) {
        obj.value = obj.value.replace(/[^0-9\.]/g, '');
         
    }
}


function ReturnInvalidEmails(emails) {
    var invalidEmailString = '';
    if (emails != null && typeof (emails) != 'undefined' && emails != '') {
        var allEmails = emails.split(',');
        if (allEmails.length > 0) {
            for (var i = 0; i < allEmails.length; i++) {
                var IsEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(allEmails[i]);
               
                if (!IsEmail) {
                    invalidEmailString += '['+ allEmails[i] + '],';
                }

            }
            invalidEmailString = invalidEmailString.substring(0, invalidEmailString.length - 1);
        }

    }
    return invalidEmailString;
}

function IsChangedValue(box) {

    var retval = false;
    $(box).find("input,select,textarea").each(function () {

        
        if ($(this).prop("tagName") == "INPUT") {
            if ($(this).attr('type') == "checkbox") {
                if ($(this).prop('checked') != $(box).data($(this).attr('id'))) {                    
                    retval = true;
                }
            }
            else if ($(this).prop('checked') == "radio") {
                if ($(this).prop('checked') != $(box).data($(this).attr('id'))) {
                    retval = true;
                }
            }
            else if ($(this).attr('type') == "text") {
                if ($(this).val() != $(box).data($(this).attr('id'))) {
                    retval = true;
                }
            }
          
        }
        if ($(this).prop("tagName") == "SELECT") {
            var val = $(this).val() == null ? "" : $(this).val();
            var match = $(box).data($(this).attr('id')) == null ? "" : $(box).data($(this).attr('id'));
            if (val != match) {
                retval = true;
            }

        }
        if ($(this).prop("tagName") == "TEXTAREA") {
            if ($(this).val() != $(box).data($(this).attr('id'))) {
                retval = true;
            }
        }
    });
    return retval;
}



function SendAjax(url, data) {
    
    var result;
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        async: false,
        dataType: 'json',
        success: function (data) {
            result = data;
        },
        complete: function () {
        },
        error: function (req, status, error) {
                                                 
        }
    });
    return result;
}

function DisplayDate(start) {
    $(start).datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(end).datepicker("option", "minDate", dt);
        }
    });

    var date = $(start).datepicker('getDate');
    $(start).val($.datepicker.formatDate(DateFormat, date));
}

function StartDateEndDate(start,end)
{      

    $(start).datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(end).datepicker("option", "minDate", dt);
        }
    });

  

    $(end).datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(start).datepicker("option", "maxDate", dt);
        }
    });

    startDate = start[0].value;
    endDate = end[0].value;
    if (startDate != "" && endDate != "") {
        var dt1 = new Date(startDate);
        var dt2 = new Date(endDate);
        dt1.setDate(dt1.getDate() + 1);
        dt2.setDate(dt2.getDate() - 1);
        console.log(dt1);
        console.log(dt2);
        $(end).datepicker("option", "minDate", dt1);
        $(start).datepicker("option", "maxDate", dt2);
    }

    var date = $(start).datepicker('getDate');
    $(start).val($.datepicker.formatDate(DateFormat, date));

    date = $(end).datepicker('getDate');
    $(end).val($.datepicker.formatDate(DateFormat, date));
}



$(document).ready(function () {

    $('.lot-icon').click(function (e) {
        sessionStorage.clear();
        urlName = $(this).attr('href');
        e.stopPropagation();
        e.preventDefault();       
        if (IsChangedValue("div.main-container")) {
            $(".alert-popup-body").show();
            $(".alert-popup").animate({ "top": "40%" });
            return false;
        }
        else { window.location.href = urlName; }

        return true;
    });

    //Use for Save changes poup functionality
    $(".alert-close, #btnAlertStay,.close-btn").click(function () {
        $(".alert-popup-body").hide();
        $(".alert-popup").animate({ "top": "40%" });

        $(".model-popup").hide();
    });
});