﻿
$(document).ready(function () {
    LoadColumnListingInPopup();
    CreateGridColumnPanel();
});
//Function to generate parameters dynamically
function CreateSearchPanel(reportScheduleId) {
    var report = _ReportName;
    if ($("#hdnEditNewClicked").val() != "" && reportScheduleId != "") {
        PendingChanges();
    }
    else {
        var reportScheduleId = reportScheduleId;
        if (reportScheduleId == "") {
            reportScheduleId = -1;
        }
        ShowLoading();
        var Url1 = resolveUrl() + '/Report/CreateSearchPanel';       
        $.ajax({
            url: Url1,
            cache: false,
            data: { ReportName: report, ReportScheduleId: reportScheduleId },
            async:true,
            success: function (data) {
                $("#SearchPanelList").html(data);
            },          
            complete: function () {
                if ($("#hdnEditNewClicked").val() == "") {                    
                    $("#SearchPanelList :input").attr("disabled", true);
                    $("#SearchPanelList :input").css("cursor", "not-allowed");
                    $("#divReportScheduler :input").prop('disabled', true);
                }
                else
                {
                    $("#divReportScheduler :input").css("background-color", "#ECDD9D");
                    $("#SearchPanelList :input").css("background-color", "#ECDD9D");
                    isRunEnable = false;
                    $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
                    DisableToolbarControl("#btn_toolbar_Exclamation");
                }
                HideLoading();
                LoadColumnListingInPopup();
            }

        });
    }
}
//Function for pending changes(Run when user edit or add new record and click on grid then this function show the prompt)
function PendingChanges() {
    $(".alert-popup-body-message-confirm").hide();
    $("#alertmessageid").text("You have pending changes. Please save or cancel.");
    $(".alert-popup-body-message").show();
    $("#btnok").click(function () {
        $(".alert-popup-body-message").hide();
    });
}
//Function to bind parameters details in popup
function LoadColumnListingInPopup() {
    var report = _ReportName;
    $.ajax({
        url: resolveUrl() + '/Report/CreateColumnListingPopUp',
        cache: false,
        data: { ReportName: report },
        success: function (data) {
            $("#ColumnListingPopUp").html(data);
            BindCustomSearch();
        },
        error: function (response) {
            if (response.status != 0) {
               
            }
        }, beforeSend: function () {
            ShowLoading();
        },
        complete: function () {        
            HideLoading();
        }
    });
};
function SetSearchFieldValue(objSearchFieldValue) {

    for (var i = 0; i < objSearchFieldValue.length; i++) {
        var SearchFieldObject = objSearchFieldValue[i];

        var controlname = SearchFieldObject.Type + '-' + SearchFieldObject.Name;
        document.getElementsByName(controlname)[0].value = SearchFieldObject.Value1;
    }

}
//Function to create grid column panel
function CreateGridColumnPanel() {
    var report = _ReportName;
    var Url1 =resolveUrl()+ '/Report/BindColumnListing';
    $.ajax({
        url: Url1,
        cache: false,
        data: { ReportName: report },
        success: function (data) {
            $("#CustomGridListing").html(data);
            BindCustomGrid();
        },
        error: function (response) {            
            if (response.status != 0) {
               
            }
        }
    });
}

$(document).ready(function () {
    $("#btdeletenCancel,.alert-close").click(function () {
        $(".alert-popup-body-message-confirm").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });

});
//Function for showing and hiding loader
function ShowLoading() {
    $('#divloaderBackground').show();
    $('#divLoader').show();

}
function HideLoading() {
    $('#divLoader').hide();
    $('#divloaderBackground').hide();
}

//selectAllList
function selectAllList() {

    var aSelect = $('#sel2');

    var options = $('#sel2 option');

    var values = $.map(options, function (option) {
        // alert(option.value);
    });


    var aSelectLen = aSelect.length;
    for (i = 0; i < aSelectLen; i++) {
        aSelect.options[i].attr('selected', 'selected');
    }
}

//GetLeftPanelList
function GetLeftPanelList() {
    var layoutID = _SearchPanelLayoutID;
    var optionValues = [];  
    var options = $('#sel2 option');
    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });       
    var srchrslt = optionValues;
    $.ajax({
        type: "POST",
        url: resolveUrl() + '/Report/SaveCustomSearchDetailsByUserName',
        cache: false,
        data: { SearchList: srchrslt.toString(), LayoutId: layoutID }, 
        dataType: "json"
        , beforeSend: function () {
            ShowLoading();
        }
    }).done(function (data) {       
        CreateSearchPanel($("#hdnReportScheduleId").val());
        HideLoading();

    }).fail(function (response) {
       
    });

}

//GetGridPanelList
function GetGridPanelList() {
    var layoutID = _ListPanellayoutID;
    var optionValues = [];
    var options = $('#GridList2 option');

    var values = $.map(options, function (option) {
        optionValues.push(option.value);
    });
    var srchrslt = optionValues;

    $.ajax({
        type: "POST",
        url: resolveUrl() + '/Report/SaveCustomSearchDetailsByUserName',
        cache: false,
        data: { SearchList: srchrslt.toString(), LayoutId: layoutID },
        dataType: "json"
        , beforeSend: function () {
            ShowLoading();
        }
    }
    ).done(function (data) {
        $(".custom-grid").hide();
        HideLoading();
    }).fail(function (response) {
        if (response.status != 0) {
           
        }
    });

}

