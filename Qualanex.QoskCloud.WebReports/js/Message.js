﻿var Report = {
    VersionChange: "This record has been modified by another user. Your changes will not be saved. Please click ok button to load the updated information. <br><br> <span style='color:red;font-size:12px;'> Note : All the fields modified by other user will be highlighted in yellow </span>", Delete: "This record has been deleted by another user",
    DeleteSuccess: "Deleted successfully!",
    DeleteFailed: "Failed to delete Report",
    DeleteConfirmation: "Do you wish to delete this report?",

}