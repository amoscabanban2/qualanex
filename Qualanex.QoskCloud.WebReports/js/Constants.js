
var DateFormat = 'mm/dd/yy';
var DateFormatGrid = 'MM/dd/yy';
var GridDateTimeFormat = 'M/d/yyyy h:mm:ss tt';

var Compare = {
    Deleted: 'Deleted',
    OK: 'OK',
    Fails: 'Fails'

};

var UserRoles = {
    ADMN: 0,
    CUSR: 1,
    RUSR: 2,
    SUSR: 3
};

var ReportNames = {

    ProductDetails: 'Sample_Report_Retailer_Product_Detail',
    WasteReport: 'Waste Report',
    PharmacySummary: 'Pharmacy Summary',
    NonCreditableSummary: 'Non Creditable Summary',
    ManufacturerSummary: 'Manufacturer Summary',
    ReturnToStock: 'Return To Stock'
}





