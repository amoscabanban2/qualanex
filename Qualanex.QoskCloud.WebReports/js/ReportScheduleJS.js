﻿
var Details = "";
var reportCriteriaDetail = "";
var PreviewGridHeight = "";

//These function are executing while loading the page
LoadHeight();
ChangeReportName();
ProductDetailsInformation();
CreateReportScheduleGridDesign();

//Function to load height dynamically.
function LoadHeight() {
    var winHeight = $(window).height();
    var gridHeight = winHeight - 505;
    $("#hdnGridHeight").val(gridHeight);
    AllGridHeight = gridHeight;

    headerHeight = $('.header-top').height();
    customBarHeight = $('.top-nav-box').height();
    headingHeight = $('.text-center').height();
    previewTabsHeight = $('.navbar-div').height();
    footerHeight = $('.report-footer').height();

    PreviewGridHeight = winHeight - (headerHeight + customBarHeight + 4 * headingHeight + previewTabsHeight + footerHeight);
    $(".data-grid-show").css("height", gridHeight + "px");
    $("#ifReport").css("height", PreviewGridHeight + "px");
    $("#dvJqxgrid_ReportPreviewCSV").css("height", PreviewGridHeight + "px");
    $("#dvJqxgrid_ReportPreviewXLS").css("height", PreviewGridHeight + "px");
    $("#dvJqxgrid_ReportImageVideo").css("height", PreviewGridHeight + "px");
}
$(window).resize(function () {
    var height = $(window).height() - 505;
    $('.data-grid-show').height(height);
    AllGridHeight = height;
    $('#ifReport').height(PreviewGridHeight);
    $('#dvJqxgrid_ReportPreviewCSV').height(PreviewGridHeight);
    $('#dvJqxgrid_ReportPreviewXLS').height(PreviewGridHeight);
    $('#dvJqxgrid_ReportImageVideo').height(PreviewGridHeight);
});
$(window).trigger('resize');


function ProductDetailsInformation() {

    $("#reportPreviewHeading").text($("#hdnReportName").val());
    if ($("#hdnReportName").val() == ReportNames.ProductDetails) {
        $("#reportPreviewHeading").text("Product Details");
    }
    if ($("#hdnReportName").val() == ReportNames.ProductDetails) {
        $("#reportScheduleHeading").text("Scheduled Reports (Product Details)");
    }
    if ($("#hdnReportName").val() == ReportNames.ProductDetails) {
        $("#reportScheduleHistoryHeading").text("Scheduled Reports History (Product Details)");
    }
    if ($("#hdnReportName").val() == ReportNames.ProductDetails) {
        $("#reportImageVideoHeading").text("Image/Video (Product Details)");
    }

    $("#reportScheduleHistoryHeading").hide();
    $("#reportPreviewHeading").hide();
    $("#reportImageVideoHeading").hide();
}

$('.lot-icon').unbind("click");

//Function for showing and hiding loader
function ShowLoading() {
    $('#divloaderBackground').show();
    $('#divLoader').show();
}
function HideLoading() {
    $('#divLoader').hide();
    $('#divloaderBackground').hide();
}
//Toolbar control functionality
$("#btn_toolbar_new").on('click', function () {
    unbindDetailGridRowSelect();
    $("#hdnEditNewClicked").val("Edit_New");
    localStorage['visited'] = "yes";
    $('#hdnReportScheduleId').val('');
    CreateSearchPanel($('#hdnReportScheduleId').val());
    var datarow = {
        ReportScheduleID: null,
        ReportScheduleTitle: "",
        Schedule: "",
        Criteria: ""
    };
    var commit = $("#dvJqxgrid_schedule").jqxGrid('addrow', null, datarow, 0);
    $("#dvJqxgrid_schedule").jqxGrid('selectrow', 0);
    $("#divReportScheduler :input").prop('disabled', false);
    $("#SearchPanelList :input").prop("disabled", false);
    DisableEnableToolbarAfterAddNewAndEdit();

    EditScheduleGrid(0);
    event.stopPropagation();
});
$("#btn_toolbar_edit").click(function () {
    unbindDetailGridRowSelect();
    $("#hdnEditNewClicked").val("Edit_New");
    DisableEnableToolbarAfterAddNewAndEdit();
    if (isExclamationEnable == true) {
        $("#hdnEditNewClicked").val("");
        CreateSearchPanel($('#hdnReportScheduleId').val());
        $("#hdnEditNewClicked").val("Edit_New");
        EditScheduleGrid($('#hdnReportScheduleId').val());
    }
    EnableScheduleAndCriteriaAfterEdit();
});
$("#btn_toolbar_cancel").click(function () {

    $("#alertdeletemessageid").text("You have pending changes. Do you want to cancel?");
    $("#confirmAlert").show();

    $("#btdeletenCancel,#btnok,.alert-close").click(function () {
        $("#confirmAlert").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });
    $("#btdeletenok").unbind('click');
    $("#btdeletenok").click(function () {
        $("#hdnEditNewClicked").val("");
        location.reload();

    });
});

$("#btn_toolbar_next").click(function () {
    //Work for Telerik report paging.

    if (isReportIframeReady() == true) {
        GoToNextPage();
        return;
    }
    isExclamationEnable = false;
    //Work for Detail grid selection.
    EnableToolbarControl("#btn_toolbar_previous");
    var rowIndexes = $('#dvJqxgrid_schedule').jqxGrid('getselectedrowindexes');
    var nexttIndex = rowIndexes[0];
    var datainformations = $('#dvJqxgrid_schedule').jqxGrid('getdatainformation');
    var rowscounts = datainformations.rowscount;
    if (nexttIndex == rowscounts - 2) {
        DisableToolbarControl("#btn_toolbar_next");
        EnableToolbarControl("#btn_toolbar_previous");
    }
    var datarow = $('#dvJqxgrid_schedule').jqxGrid('selectrow', nexttIndex + 1);
    $('#dvJqxgrid_schedule').jqxGrid('ensurerowvisible', nexttIndex + 1);
});

$("#btn_toolbar_previous").click(function () {

    //Work for Telerik report paging.
    if (isReportIframeReady() == true) {
        GoToPreviousPage();
        return;
    }
    isExclamationEnable = false;
    //Work for grid row selection.
    var rowIndexes = $('#dvJqxgrid_schedule').jqxGrid('getselectedrowindexes');
    var previousIndex = rowIndexes[0];
    $('#dvJqxgrid_schedule').jqxGrid('selectrow', previousIndex - 1);
    $('#dvJqxgrid_schedule').jqxGrid('ensurerowvisible', previousIndex - 1);
    EnableToolbarControl("#btn_toolbar_next");

    if (previousIndex == 1) {
        EnableToolbarControl("#btn_toolbar_next");
        DisableToolbarControl("#btn_toolbar_previous");
    }
});

$('#btn_toolbar_refresh').click(function () {

    if (localStorage['History'] == 'yes') {
        CreateReportStatusGridDesign();
        DisableEnableToolbarAfterHistory();
    }
    else {
        isExclamationEnable = false;
        CreateReportScheduleGridDesign();
    }


});
var zoomVal = 0;
$("#btn_toolbar_zoomIn").click(function () {
    if (navigator.userAgent.indexOf("Chrome") != -1) {
        zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom"));
    }
    else if (!!document.documentMode == true /*For IE > 10*/) {
        zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom")) / 100;
    }
    else if (zoomVal == 0) zoomVal = 1;

    if (zoomVal == 5) return;

    zoomVal = zoomVal + .5;
    if (navigator.userAgent.indexOf("Chrome") != -1 || (!!document.documentMode == true /*For IE > 10*/)) {
        $("#dvJqxgrid_schedule").css("zoom", zoomVal);
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1) {
        $("#dvJqxgrid_schedule").css("MozTransform", "scale(" + zoomVal + ")");
        $("#dvJqxgrid_schedule").css("MozTransformOrigin", "0 0");
    }

});

$("#btn_toolbar_zoomOut").click(function () {

    if (navigator.userAgent.indexOf("Chrome") != -1) {
        zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom"));
    }
    else if (!!document.documentMode == true /*For IE > 10*/) {
        zoomVal = parseFloat($("#dvJqxgrid_schedule").css("zoom")) / 100;
    }

    if (zoomVal <= 1) return;

    zoomVal = zoomVal - 0.5;

    if (navigator.userAgent.indexOf("Chrome") != -1 || (!!document.documentMode == true /*For IE > 10*/)) {

        $("#dvJqxgrid_schedule").css("zoom", zoomVal);
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1) {

        $("#dvJqxgrid_schedule").css("MozTransform", "scale(" + zoomVal + ")");
        $("#dvJqxgrid_schedule").css("MozTransformOrigin", "0 0");
    }
});

$("#btn_toolbar_delete").click(function () {
    var reportScheduleID = '';
    var rowIndexes = $('#dvJqxgrid_schedule').jqxGrid('getselectedrowindexes');
    var rowIds = new Array();
    for (var i = 0; i < rowIndexes.length; i++) {
        var currentId = $('#dvJqxgrid_schedule').jqxGrid('getrowid', rowIndexes[i]);
        var rowData = $("#dvJqxgrid_schedule").jqxGrid('getRowData', currentId);
        reportScheduleID = rowData.ReportScheduleID;
    };

    if (reportScheduleID !== null) {
        deleteReport(reportScheduleID);
    }

    var deleteReportId = "";
    function deleteReport(reportScheduleID) {
        deleteReportId = reportScheduleID;
        $("#alertdeletemessageid").text(Report.DeleteConfirmation);
        $("#confirmAlert").show();
    }
    $("#btdeletenCancel,#btnok,.alert-close").click(function () {
        $("#confirmAlert").hide();
        $(".alert-popup").animate({ "top": "40%" });
    });
    $("#btdeletenok").unbind('click');
    $("#btdeletenok").click(function () {
        $.ajax({
            url: resolveUrl() + "/Report/Delete",
            cache: false,
            data: {
                ReportScheduleId: reportScheduleID
            },
            success: function (data) {
                if (data.Status == true) {

                    $("#confirmAlert").hide();
                    $("#alertmessageid").text(data.Result);
                    $(".alert-popup-body-message").show();

                    $("#btnok").click(function () {
                        $(".alert-popup-body-message").hide();
                        location.reload();
                    });

                }
                else {
                    $("#alertmessageid").text(data.Result);
                    $(".alert-popup-body-message").show();
                }

            },
            error: function (response) {
                if (response.status != 0 && response.status != 401) {
                    alert(response.status + " " + response.statusText);
                }
            }
        });
    });
});

$("#btn_toolbar_download").click(function () {
    var ReportName = _ReportName;
    var NDC = $('#NDCNumber').val();
    var ReportScheduleTimeZone = $('#ReportScheduleTimeZone').val();
    var ReportDataTimeZone = $('#ReportDataTimeZone').val();
    var FileType = $('select#FileType option:selected').text();
    if (typeof (FileType) != "undefined" && FileType != "Select") {
        if (FileType == "PDF") {
            ExportReport();
        }
        else {
            //Just to call report action method
            //As ajax does not support to binary write response.
            if (report == ReportNames.ProductDetails) {
                reportCriteriaDetail = productDetailCriteria();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else if (report == ReportNames.WasteReport) {
                reportCriteriaDetail = wasteReportCriteria();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else if (report == ReportNames.PharmacySummary) {
                reportCriteriaDetail = PharmacySummaryDetails();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else if (report == ReportNames.NonCreditableSummary) {
                reportCriteriaDetail = NonCreditableSummaryDetails();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else if (report == ReportNames.ManufacturerSummary) {
                reportCriteriaDetail = ManufacturerSummaryDetails();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else if (report == ReportNames.ReturnToStock) {
                reportCriteriaDetail = ReturnToStockDetails();
                reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true + "&reportCriteriaDetail=" + reportCriteriaDetail);

            }
            else {
                $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?NDCNumber=" + NDC + "&ReportDataTimeZone=" + ReportDataTimeZone + "&ReportScheduleTimeZone=" + ReportScheduleTimeZone + "&FileType=" + FileType + "&Isdownload=" + true);
            }
        }
    }

});
//Anchor tabs (Detail,History and Preview) functionality
$("#anchorDetail").click(function (event) {

    $("#reportPageNumber").hide();
    $("#gridData").show();
    $("#reportScheduleHistoryHeading").hide();
    $("#reportPreviewHeading").hide();
    $("#reportScheduleHeading").show();
    $("#reportImageVideoHeading").hide();
    if ($("#hdnEditNewClicked").val() != "") {
        return;
    }
    $("#anchorImagesVideos").removeClass('active');
    $("#anchorPreview").removeClass('active');
    $("#anchorDetail").addClass('active');
    $("#anchorHistory").removeClass('active');

    $("#dvJqxgrid_ReportSummary").hide();
    $("#dvReportPreview").hide();
    $("#dvReportPreviewXLS").hide();
    $("#dvJqxgrid_ReportImageVideo").hide();
    $("#dvJqxgrid_schedule").show();
    DisableToolbarControl("#btn_toolbar_download");
    $("#btn_toolbar_Run").attr("src", "/images/RunStandardEnable.png");
    isRunEnable = true;

    $(".report-wrapper").show();

    if ($("#hdnRunOnlineReport").val() != "") {
        $("#hdnEditNewClicked").val("Edit_New");
        DisableEnableToolbarAfterAddNewAndEdit();
    }

    else {
        DisableEnableToolbarControlAfterDetail();
    }
    localStorage['History'] = "";
    event.stopPropagation();
});

$("#anchorHistory").click(function (event) {
    DisableToolbarControl("#btn_toolbar_grid");
    $("#reportPageNumber").hide();
    if ($("#hdnEditNewClicked").val() != "") {
        PendingChanges();
    }
    else {

        if ($("#hdnReportScheduleId").val() == '') {
            ScheduleNotSelect();
            return;
        }
        $("#gridData").show();
        $("#reportScheduleHeading").hide();
        $("#reportPreviewHeading").hide();
        $("#reportImageVideoHeading").hide();
        $("#reportScheduleHistoryHeading").show();

        $("#anchorImagesVideos").removeClass('active');
        $("#anchorPreview").removeClass('active');
        $("#anchorDetail").removeClass('active');
        $("#anchorHistory").addClass('active');

        $(".report-wrapper").show();

        $("#dvJqxgrid_schedule").hide();
        $("#dvReportPreview").hide();
        $("#dvReportPreviewXLS").hide();
        $("#dvJqxgrid_ReportImageVideo").hide();
        $("#dvJqxgrid_ReportSummary").show();
        CreateReportStatusGridDesign();
        localStorage['History'] = "yes";
        DisableEnableToolbarAfterHistory();
        DisableToolbarControl("#btn_toolbar_search");
        $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
        DisableToolbarControl("#btn_toolbar_Exclamation");
        isRunEnable = false;
    }
    event.stopPropagation();
});

var report = _ReportName;
$("#anchorPreview").click(function (event) {
    DisableToolbarControl("#btn_toolbar_grid");
    DisableToolbarControl("#btn_toolbar_search");
    if ($("#hdnEditNewClicked").val() != "") {
        PendingChanges();
    }
    else {
        if ($("#hdnReportScheduleId").val() == '') {
            ScheduleNotSelect();
            return;
        }
        $("#gridData").hide();
        $("#reportScheduleHistoryHeading").hide();
        $("#reportScheduleHeading").hide();
        $("#reportPreviewHeading").show();
        $("#reportImageVideoHeading").hide();
        $("#anchorPreview").addClass('active');
        $("#anchorDetail").removeClass('active');
        $("#anchorHistory").removeClass('active');
        $("#anchorImagesVideos").removeClass('active');
        $("#dvJqxgrid_ReportSummary").hide();
        $("#dvJqxgrid_schedule").hide();
        $("#dvJqxgrid_ReportImageVideo").hide();
        $("#dvReportPreview").show();
        $(".report-wrapper").hide();
        $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
        DisableToolbarControl("#btn_toolbar_Exclamation");
        EnableToolbarControl("#btn_toolbar_download");
        isRunEnable = false;
      
        var FileType = $('select#FileType option:selected').text();
        var Isdownload = false;
        GenerateReportAsPerFileType(FileType, Isdownload);
    }

    event.stopPropagation();
});
//Image/Video Click
$("#anchorImagesVideos").click(function () {
    $("#reportPageNumber").hide();
    if ($("#hdnEditNewClicked").val() != "") {
        PendingChanges();
    }
    else {
        DisableEnableToolbarAfterImageVideo();
        $("#gridData").hide();
        $("#reportScheduleHistoryHeading").hide();
        $("#reportScheduleHeading").hide();
        $("#reportPreviewHeading").hide();
        $("#reportImageVideoHeading").show();

        $("#anchorImagesVideos").addClass('active');
        $("#anchorPreview").removeClass('active');
        $("#anchorDetail").removeClass('active');
        $("#anchorHistory").removeClass('active');


        $("#dvJqxgrid_ReportSummary").hide();
        $("#dvJqxgrid_schedule").hide();
        $("#dvReportPreview").hide();
        $("#dvJqxgrid_ReportPreviewXLS").hide();
        $("#dvJqxgrid_ReportPreviewCSV").hide();
        $("#dvReportPreviewXLS").show();
        $("#dvJqxgrid_ReportImageVideo").show();

        $(".report-wrapper").hide();
        $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
        DisableToolbarControl("#btn_toolbar_Exclamation");
        isRunEnable = false;

        ShowLoading();

        var FileType = 'XLS';
        var Isdownload = false;
        var report = _ReportName;
        var reportCriteriaDetail = "";
        if (report == ReportNames.ProductDetails) {
            reportCriteriaDetail = productDetailCriteria();
        }
        else if (report == ReportNames.WasteReport) {
            reportCriteriaDetail = wasteReportCriteria();
        }
        else if (report == ReportNames.PharmacySummary) {
            reportCriteriaDetail = PharmacySummaryDetails();
        }
        else if (report == ReportNames.NonCreditableSummary) {
            reportCriteriaDetail = NonCreditableSummaryDetails();
        }
        else if (report == ReportNames.ManufacturerSummary) {
            reportCriteriaDetail = ManufacturerSummaryDetails();
        }
        else if (report == ReportNames.ReturnToStock) {
            reportCriteriaDetail = ReturnToStockDetails();
        }
        CreateReportImageVideoDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
    }

});
//Function to remove ampersand from json data
function RemoveAmpersandFromJson(reportCriteriaDetail) {
    reportCriteriaDetail = JSON.stringify(reportCriteriaDetail);
    reportCriteriaDetail = reportCriteriaDetail.replace(/&/g, "%26");
    return reportCriteriaDetail;
}

//Report Name: Product Detail
//Function to get product detail criteria
function productDetailCriteria() {
    var retailerProductDetail =
    {
        NDCNumber: $('[name=textbox-NDCNumber]').val(),
        StoreName: $('[name=textbox-StoreName]').val(),
        StoreNumber: $('[name=textbox-StoreNumber]').val(),
        VendorName: $('[name=textbox-VendorName]').val(),
        ProductDescription: $('[name=textbox-ProductDescription]').val(),
        LotNumber: $('[name=textbox-LotNumber]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
        ExpDate: $('[name=datepicker-ExpDate]').val(),
        FullQty: $('[name=textbox-FullQty]').val(),
        PartialQty: $('[name=textbox-PartialQty]').val(),
        UnitPriceBefore: $('[name=textbox-UnitPriceBefore]').val(),
        CreditableAmountBeforeMFGDiscount: $('[name=textbox-CreditableAmountBeforeMFGDiscount]').val(),
        Creditable: $('[name=textbox-Creditable]').val(),
        OutofpolicyDescription: $('[name=textbox-OutofpolicyDescription]').val(),
        DataStartDate: $('[name=datepicker-DataStartDate]').val(),
        DataEndDate: $('[name=datepicker-DataEndDate]').val(),
        Strength: $('[name=textbox-Strength]').val(),
        ItemGuid: $('[name=textbox-ItemGuid]').val(),
        QoskProcessDate: $('[name=datepicker-QoskProcessDate]').val(),
        ControlNumber: $('[name=textbox-ControlNumber]').val(),
        PackageSize: $('[name=textbox-PackageSize]').val(),
        UnitPriceAfter: $('[name=textbox-UnitPriceAfter]').val(),
        CreditableAmountAfter: $('[name=textbox-CreditableAmountAfter]').val(),
        RecalledProduct: $('[name=textbox-RecalledProduct]').val(),
        DiscontinuedProduct: $('[name=textbox-DiscontinuedProduct]').val(),
        RXorOTC: $('[name=textbox-RXorOTC]').val(),
        DosageForm: $('[name=textbox-DosageForm]').val(),
        PackageForm: $('[name=textbox-PackageForm]').val(),
        PartialPercentage: $('[name=textbox-PartialPercentage]').val(),
    };
    return retailerProductDetail;
}

//Report Name: Waste Report
//Function to get Waste Report criteria
function wasteReportCriteria() {
    var wasteReportDetail =
    {
        NDCNumber: $('[name=textbox-NDCNumber]').val(),
        StoreName: $('[name=textbox-StoreName]').val(),
        StoreNumber: $('[name=textbox-StoreNumber]').val(),
        ItemGuid: $('[name=textbox-ItemGuid]').val(),
        ProductDescription: $('[name=textbox-ProductDescription]').val(),
        LotNumber: $('[name=textbox-LotNumber]').val(),
        Strength: $('[name=textbox-Strength]').val(),
        FullQty: $('[name=textbox-FullQty]').val(),
        ControlNumber: $('[name=textbox-ControlNumber]').val(),
        DosageForm: $('[name=textbox-DosageForm]').val(),
        PackageForm: $('[name=textbox-PackageForm]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
        ExpDate: $('[name=datepicker-ExpDate]').val(),
        PackageSize: $('[name=textbox-PackageSize]').val(),
        WasteStreamProfile: $('[name=textbox-WasteStreamProfile]').val(),
        WasteCode: $('[name=textbox-WasteCode]').val(),
        SealedOpenCase: $('[name=textbox-SealedOpenCase]').val(),
        DataStartDate: $('[name=datepicker-DataStartDate]').val(),
        DataEndDate: $('[name=datepicker-DataEndDate]').val(),
        PartialQty: $('[name=textbox-PartialQty]').val(),

    };
    return wasteReportDetail;
}
//Report Name: Pharmacy Summary
//Function to get Pharmacy Summary criteria
function PharmacySummaryDetails() {
    var pharmacySummaryDetail =
    {
        StoreNumber: $('[name=textbox-StoreNumber]').val(),
        StoreName: $('[name=textbox-StoreName]').val(),
        WholesalerCustomerNumber: $('[name=textbox-WholesalerCustomerNumber]').val(),
        RegionName: $('[name=textbox-RegionName]').val(),
        ReturnCreditableValue: $('[name=textbox-ReturnCreditableValue]').val(),
        ReturnCreditableQty: $('[name=textbox-ReturnCreditableQty]').val(),
        RecallCreditableValue: $('[name=textbox-RecallCreditableValue]').val(),
        RecallCreditableQty: $('[name=textbox-RecallCreditableQty]').val(),
        NonCreditableValue: $('[name=textbox-NonCreditableValue]').val(),
        NonCreditableQty: $('[name=textbox-NonCreditableQty]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
        TotalProductValue: $('[name=textbox-TotalProductValue]').val(),
        TotalQty: $('[name=textbox-TotalQty]').val(),
        ProcessingFeeBilled: $('[name=textbox-ProcessingFeeBilled]').val(),
        TotalInvoiceAmount: $('[name=textbox-TotalInvoiceAmount]').val(),
        TotalDebitMemoQty: $('[name=textbox-TotalDebitMemoQty]').val(),
        ReturnToStockValue: $('[name=textbox-ReturnToStockValue]').val(),
        ReturnToStockQty: $('[name=textbox-ReturnToStockQty]').val(),
    };
    return pharmacySummaryDetail;
}
//Report Name: Non Creditable Summary
//Function to get Non Creditable Summary criteria
function NonCreditableSummaryDetails() {
    var nonCreditableSummaryDetail =
    {
        StoreNumber: $('[name=textbox-StoreNumber]').val(),
        StoreName: $('[name=textbox-StoreName]').val(),
        RegionName: $('[name=textbox-RegionName]').val(),
        NonCreditableValue: $('[name=textbox-NonCreditableValue]').val(),
        NonCreditableQty: $('[name=textbox-NonCreditableQty]').val(),
        ManufacturerName: $('[name=textbox-ManufacturerName]').val(),
        NonCreditableReasonCode: $('[name=textbox-NonCreditableReasonCode]').val(),
        NonCreditableReasonDescription: $('[name=textbox-NonCreditableReasonDescription]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
    };
    return nonCreditableSummaryDetail;
}
//Report Name: Manufacturer Summary
//Function to get Manufacturer Summary criteria
function ManufacturerSummaryDetails() {
    var manufacturerSummaryDetail =
    {
        MFGLabeler: $('[name=textbox-MFGLabeler]').val(),
        ManufacturerName: $('[name=textbox-ManufacturerName]').val(),
        RetailerVendorNumber: $('[name=textbox-RetailerVendorNumber]').val(),
        DebitMemoInvoiceNo: $('[name=textbox-DebitMemoInvoiceNo]').val(),
        CreditableValue: $('[name=textbox-CreditableValue]').val(),
        CreditableQty: $('[name=textbox-CreditableQty]').val(),
        RecallCreditableValue: $('[name=textbox-RecallCreditableValue]').val(),
        RecallCreditableQty: $('[name=textbox-RecallCreditableQty]').val(),
        TotalProductValue: $('[name=textbox-TotalProductValue]').val(),
        TotalQty: $('[name=textbox-TotalQty]').val(),
        ProcessingFee: $('[name=textbox-ProcessingFee]').val(),
        TotalInvoiceAmount: $('[name=textbox-TotalInvoiceAmount]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
    };
    return manufacturerSummaryDetail;
}
//Report Name: Return To Stock
//Function to get Return To Stock criteria
function ReturnToStockDetails() {
    var returnToStockDetail =
    {
        VendorName: $('[name=textbox-VendorName]').val(),
        StoreName: $('[name=textbox-StoreName]').val(),
        StoreNumber: $('[name=textbox-StoreNumber]').val(),
        NDCNumber: $('[name=textbox-NDCNumber]').val(),
        ProductDescription: $('[name=textbox-ProductDescription]').val(),
        Strength: $('[name=textbox-Strength]').val(),
        ControlNumber: $('[name=textbox-ControlNumber]').val(),
        RXorOTC: $('[name=textbox-RXorOTC]').val(),
        DosageForm: $('[name=textbox-DosageForm]').val(),
        PackageForm: $('[name=textbox-PackageForm]').val(),
        LotNumber: $('[name=textbox-LotNumber]').val(),
        ExpDate: $('[name=datepicker-ExpDate]').val(),
        QoskProcessDate: $('[name=datepicker-QoskProcessDate]').val(),
        DateEligibleForCredit: $('[name=datepicker-DateEligibleForCredit]').val(),
        OutofpolicyDescription: $('[name=textbox-OutofpolicyDescription]').val(),
        ReportDataTimeZone: $('#ReportDataTimeZone').val(),
        ReportScheduleTimeZone: $('#ReportScheduleTimeZone').val(),
    };
    return returnToStockDetail;
}
//Function for pending changes(Run when user edit or add new record and click on grid then this function show the prompt)
function PendingChanges() {
    $(".alert-popup-body-message-confirm").hide();
    $("#alertmessageid").text("You have pending changes. Please save or cancel.");
    $(".alert-popup-body-message").show();
    $("#btnok").click(function () {
        $(".alert-popup-body-message").hide();
    });
}
//Function for ScheduleNotSelect
function ScheduleNotSelect() {
    $(".alert-popup-body-message-confirm").hide();
    $("#alertmessageid").text("Please select a schedule.");
    $(".alert-popup-body-message").show();
    $("#btnok").click(function () {
        $(".alert-popup-body-message").hide();
    });
}
//Function for edit scheduled report information
function EditScheduleGrid(scheduleId) {
    //ignore message when we add new report.
    if ($("#hdnEditNewClicked").val() != "" && scheduleId != 0 && isExclamationEnable == false) {
        PendingChanges();
    }
    else {
        $.ajax({
            type: "Get",
            cache: false,
            contentType: 'application/json; charset=utf-8',
            url: resolveUrl() + '/Report/EditReportScheduler',
            data: { ReportScheduleId: scheduleId },
            success: function (data) {
                $("#divReportScheduler").empty().html(data);
            },
            error: function (response) {
                if (response.status != 0 && response.status != 401) {
                    alert(response.status + " " + response.statusText);
                }
            },
            complete: function () {
                DisableEnableToolbarControlAfterDetail();
                if (rowscounts == 0) {
                    $("#divReportScheduler :input").prop('disabled', true);
                    $("#SearchPanelList :input").prop("disabled", true);
                    isRunEnable = true;
                    $("#btn_toolbar_Run").attr("src", "/images/RunStandardEnable.png");
                    EnableToolbarControl("#btn_toolbar_Exclamation");
                }
                else if (scheduleId != -1 && scheduleId != 0 && isExclamationEnable == false) {
                    $("#divReportScheduler :input").prop('disabled', true);
                }
                else if (isExclamationEnable == true && $("#hdnEditNewClicked").val() != "") {
                    EnableScheduleAndCriteriaAfterEdit();
                }
                else if (scheduleId == 0) {
                    $("#divReportScheduler :input").css("background-color", "#ECDD9D");
                    $("#SearchPanelList :input").css("background-color", "#ECDD9D");
                    isRunEnable = false;
                    $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
                    DisableToolbarControl("#btn_toolbar_Exclamation");
                }
            }
        });
    }
}
//Function for disable toolbar control
window.DisableToolbarControl = function DisableToolbarControl(controlID) {
    if (controlID == "#btn_toolbar_cancel") {
        $(controlID).css('color', '#88B2C8');
    }
    $(controlID).css('pointer-events', 'none');
    $(controlID).removeClass("darktxt");
    $(controlID).addClass("lighttxt");
}
//Function for enable toolbar control
window.EnableToolbarControl = function EnableToolbarControl(controlID) {
    if (controlID == "#btn_toolbar_cancel") {
        $(controlID).css('color', 'red');
    }
    $(controlID).css('pointer-events', 'auto');
    $(controlID).removeClass("lighttxt");
    $(controlID).addClass("darktxt");
}
//function for disable enable toolbar control value after click
function GridRowCountCondition() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    EnableToolbarControl("#btn_toolbar_zoomIn");
    EnableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    EnableToolbarControl("#btn_toolbar_edit");
    EnableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
function GridPositionEqualRowCount() {
    EnableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    EnableToolbarControl("#btn_toolbar_zoomIn");
    EnableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    EnableToolbarControl("#btn_toolbar_edit");
    EnableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
function GridPositionNotEqualRowCount() {
    DisableToolbarControl("#btn_toolbar_previous");
    EnableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    EnableToolbarControl("#btn_toolbar_zoomIn");
    EnableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    EnableToolbarControl("#btn_toolbar_edit");
    EnableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
function GridPositionLessThanRowCount() {
    EnableToolbarControl("#btn_toolbar_previous");
    EnableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    EnableToolbarControl("#btn_toolbar_zoomIn");
    EnableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    EnableToolbarControl("#btn_toolbar_edit");
    EnableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
function EnableOnlyNewInToolbar() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
function DisableSaveCancelAndPreviousInToolbar() {
    DisableToolbarControl("#btn_toolbar_previous");
    EnableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    EnableToolbarControl("#btn_toolbar_zoomIn");
    EnableToolbarControl("#btn_toolbar_zoomOut");
    EnableToolbarControl("#btn_toolbar_new");
    EnableToolbarControl("#btn_toolbar_edit");
    EnableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
}
var rowscounts = "";
function DisableEnableToolbarControlAfterDetail() {
    var pos = "";
    var rowIndexes = $('#dvJqxgrid_schedule').jqxGrid('getselectedrowindexes');
    var dataCount = $('#dvJqxgrid_schedule').jqxGrid('getpaginginformation');
    var datainformations = $('#dvJqxgrid_schedule').jqxGrid('getdatainformation');
    rowscounts = datainformations.rowscount;
    var indexCount = rowIndexes.length;
    for (var i = 0; i < rowIndexes.length; i++) {
        pos = $('#dvJqxgrid_schedule').jqxGrid('getrowid', rowIndexes[i]);
    };
    if ($("#hdnEditNewClicked").val() == "Edit_New") {
        pos = 0;
    }
    var page = dataCount.pagescount;
    DisableToolbarControl("#btn_toolbar_download");
    EnableToolbarControl("#btn_toolbar_search");
    EnableToolbarControl("#btn_toolbar_grid");
    $("#btn_toolbar_Run").attr("src", "/images/RunStandardEnable.png");
    EnableToolbarControl("#btn_toolbar_Exclamation");

    if (page > 0) {
        if (rowscounts == 1) {
            if ($("#hdnEditNewClicked").val() == "Edit_New") {
                DisableEnableToolbarAfterAddNewAndEdit();
            }
            else {

                GridRowCountCondition();
            }
        }
        else if (pos == 0) {
            if (pos + 1 == rowscounts) {
                GridPositionEqualRowCount();
            }
            else if (pos == 0 && $("#hdnEditNewClicked").val() == "Edit_New") {
                DisableEnableToolbarAfterAddNewAndEdit();
            }
            else {
                GridPositionNotEqualRowCount();
            }
        }
        else if (pos + 1 == rowscounts) {
            GridPositionEqualRowCount();
        }
        else if (pos + 1 < rowscounts) {
            GridPositionLessThanRowCount();
        }

        else {
            DisableSaveCancelAndPreviousInToolbar();
        }
    }
    else {
        EnableOnlyNewInToolbar();
    }
}
function DisableEnableToolbarAfterAddNewAndEdit() {

    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    EnableToolbarControl("#btn_toolbar_save");
    EnableToolbarControl("#btn_toolbar_cancel");
    DisableToolbarControl("#btn_toolbar_download");
}
function EnableScheduleAndCriteriaAfterEdit() {
    $("#divReportScheduler :input").prop('disabled', false);
    $("#SearchPanelList :input").prop("disabled", false);
    $("#divReportScheduler :input").css("background-color", "#ECDD9D");
    $("#divReportScheduler :input[data-val-date]").css("cursor", "pointer");
    $("#divReportParameter :input").css("background-color", "#ECDD9D");
    $("#SearchPanelList :input").css("background-color", "#ECDD9D");
    $("#SearchPanelList :input").css("cursor", "text");

    isRunEnable = false;
    $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
    DisableToolbarControl("#btn_toolbar_Exclamation");

}
function DisableEnableToolbarAfterHistory() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
    DisableToolbarControl("#btn_toolbar_download");

}
function DisableEnableToolbarAfterPreview() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
    //DisableToolbarControl("#btn_toolbar_download");
}
function DisableEnableToolbarAfterAddNewAndEdit() {

    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    EnableToolbarControl("#btn_toolbar_save");
    EnableToolbarControl("#btn_toolbar_cancel");
    DisableToolbarControl("#btn_toolbar_download");
}
function EnableScheduleAndCriteriaAfterEdit() {
    $("#divReportScheduler :input").prop('disabled', false);
    $("#SearchPanelList :input").prop("disabled", false);
    $("#divReportScheduler :input").css("background-color", "#ECDD9D");
    $("#divReportScheduler :input[data-val-date]").css("cursor", "pointer");
    $("#divReportParameter :input").css("background-color", "#ECDD9D");
    $("#SearchPanelList :input").css("background-color", "#ECDD9D");
    $("#SearchPanelList :input").css("cursor", "text");

    isRunEnable = false;
    $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
    DisableToolbarControl("#btn_toolbar_Exclamation");

}
function DisableEnableToolbarAfterHistory() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    EnableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
    DisableToolbarControl("#btn_toolbar_download");

}
function DisableEnableToolbarAfterPreview() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
    //DisableToolbarControl("#btn_toolbar_download");
}
function DisableEnableToolbarAfterImageVideo() {
    DisableToolbarControl("#btn_toolbar_previous");
    DisableToolbarControl("#btn_toolbar_next");
    DisableToolbarControl("#btn_toolbar_refresh");
    DisableToolbarControl("#btn_toolbar_zoomIn");
    DisableToolbarControl("#btn_toolbar_zoomOut");
    DisableToolbarControl("#btn_toolbar_new");
    DisableToolbarControl("#btn_toolbar_edit");
    DisableToolbarControl("#btn_toolbar_delete");
    DisableToolbarControl("#btn_toolbar_save");
    DisableToolbarControl("#btn_toolbar_cancel");
    DisableToolbarControl("#btn_toolbar_download");
    DisableToolbarControl("#btn_toolbar_grid");
    DisableToolbarControl("#btn_toolbar_search");
}
//Function for load top row schedule details
function LoadTopRowScheduleDetails() {
    var datarow = $("#dvJqxgrid_schedule").jqxGrid('getrowdata', 0);
    if (datarow == undefined) {
        EditScheduleGrid(0);
        CreateSearchPanel("");
    }
    else {
        EditScheduleGrid(datarow["ReportScheduleID"]);
        $('#dvJqxgrid_schedule').jqxGrid('selectrow', 0);
        $("#hdnReportScheduleId").val(datarow["ReportScheduleID"]);
        CreateSearchPanel($("#hdnReportScheduleId").val());
    }
    $("#dvJqxgrid_schedule").bind('rowselect', function (event) {
        isExclamationEnable = false;
        var rowindex = event.args.rowindex;
        var datarow = $("#dvJqxgrid_schedule").jqxGrid('getrowdata', rowindex);

        if (datarow != undefined && datarow["ReportScheduleID"] != null) {
            $("#hdnReportScheduleId").val(datarow["ReportScheduleID"]);
            EditScheduleGrid(datarow["ReportScheduleID"]);
            CreateSearchPanel($("#hdnReportScheduleId").val());
        }
    });
    DisableEnableToolbarControlAfterDetail();
}
//Function for create report schedule grid design
function CreateReportScheduleGridDesign() {
    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + '/Report/BindReportSchedulerGrid',
        cache: false,
        data: { ReportId: $("#hdnReportid").val() },
        success: function (data) {
            $('#dvJqxgrid_schedule').jqxGrid('clear');
            $("#dvJqxgrid_schedule").jqxGrid("render");
            $("#dvJqxgrid_schedule").jqxGrid('updatebounddata');

            GenerateScheduleGridData(data);
        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }
    }).done(function () { LoadTopRowScheduleDetails(); });
}

function GenerateScheduleGridData(json) {

    var rows = json;
    // prepare the data
    var columns = [
           { text: 'Report Schedule ID', name: 'AAA', editable: false, datafield: 'ReportScheduleID', hidden: 'true' },
           { text: 'Report Title', editable: false, datafield: 'ReportScheduleTitle', width: '20%' },
           { text: 'Schedule', editable: false, datafield: 'Schedule', width: '40%' },
           { text: 'Criteria', editable: false, datafield: 'Criteria', width: '40%' }

    ]

    var source =
    {
        datatype: "json",
        id: 'id',
        localdata: rows,
        addrow: function (rowid, rowdata, position, commit) {
            // synchronize with the server - send insert command
            // call commit with parameter true if the synchronization with the server is successful
            //and with parameter false if the synchronization failed.
            // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
            commit(true);
        }
    };
    var dataAdapter = new $.jqx.dataAdapter(source,
         {
             loadComplete: function () {
                 var length = dataAdapter.records.length;
                 $("#hdnTotalRecords").val(parseInt(length));

             }
         });


    $("#dvJqxgrid_schedule").jqxGrid(
    {
        width: '100%',
        height: AllGridHeight,
        autowidth: false,
        sortable: true,
        filterable: true,
        pageable: false,
        columnsreorder: true,
        source: dataAdapter,
        columnsresize: true,
        columns: columns

    });

    $("#dvJqxgrid_schedule").jqxGrid({ columns: columns });
    $("#dvJqxgrid_schedule").jqxGrid("render");

}
// Function for create report status grid design
function CreateReportStatusGridDesign() {
    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + '/Report/BindReportRunGrid',
        cache: false,
        data: { ReportScheduleID: $("#hdnReportScheduleId").val() }, // Note it is important
        success: function (data) {
            $('#dvJqxgrid_ReportSummary').jqxGrid('clear');
            $("#dvJqxgrid_ReportSummary").jqxGrid("render");
            $("#dvJqxgrid_ReportSummary").jqxGrid('updatebounddata');
            GenerateReportStatusGridData(data);
        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }, beforeSend: function () {

            ShowLoading();
        },
        complete: function () {

            HideLoading();
        }
    });
    $(".grid-details").hide();
    $('.grid-btn-prev').hide();
    $('.grid-btn-next').hide();
}
function GenerateReportStatusGridData(json) {
    var rows = json;
    // prepare the data
    var columns = [
           { text: 'Report Name', editable: false, datafield: 'ReportTitle', width: '22%' },
           { text: 'Report Status', editable: false, datafield: 'Status', width: '12%' },
           { text: 'Status Description', editable: false, datafield: 'StatusDescription', width: '22%' },
           { text: 'Report Run Time', editable: false, datafield: 'RunTime', width: '12%' },
           { text: 'URL', editable: false, datafield: 'BlobFile', width: '32%' }
    ]

    var source =
    {
        datatype: "json",
        id: 'id',
        localdata: rows
    };
    var dataAdapter = new $.jqx.dataAdapter(source,
         {
             loadComplete: function () {
                 var length = dataAdapter.records.length;
                 $("#hdnTotalRecords").val(parseInt(length));

             }
         });
    var pagerrenderer = function () {
        var element = $("<div id='123kds' style='margin-top: 5px; width: 100%; height: 100%;'></div>");
        var paginginfo = $("#dvJqxgrid_ReportSummary").jqxGrid('getpaginginformation');
        for (var i = 0; i < paginginfo.pagescount; i++) {
            // add anchor tag with the page number for each page.  href='#" + i + "'
            var j = i + 1;
            var anchor = $("<a style='padding: 5px;' >" + j + "</a>");
            anchor.appendTo(element);

            anchor.click(function (event) {
                // go to a page.
                var pagenum = parseInt($(event.target).text());
                pagenum = pagenum - 1;
                $("#dvJqxgrid_ReportSummary").jqxGrid('gotopage', pagenum);
            });
        }
        return element;
    }

    $("#dvJqxgrid_ReportSummary").jqxGrid(
    {
        width: '100%',
        height: AllGridHeight,
        autowidth: false,
        sortable: true,
        filterable: true,
        pageable: false,
        columnsreorder: true,
        source: dataAdapter,
        //theme: theme,
        columnsresize: true,
        pagerrenderer: pagerrenderer,
        columns: columns,
        ready: function () {
            $('#dvJqxgrid_ReportSummary').jqxGrid({ pagesizeoptions: ['50'] });
        },

    });


    $("#dvJqxgrid_ReportSummary").jqxGrid({ columns: columns });
    $("#dvJqxgrid_ReportSummary").jqxGrid("render");
    // initialize the popup window and buttons.
}

//Preview section for XLS
function CreateReportPreviewGridDesign(report, FileType, Isdownload, reportCriteriaDetail) {
    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + '/Report/' + report,
        cache: false,
        data: { FileType: FileType, Isdownload: Isdownload, reportCriteriaDetail: reportCriteriaDetail }, // Note it is important
        success: function (data) {
            GenerateReportPreviewGridData(data.reportRetailerBoxDetails, data.gridColumns);
        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }, beforeSend: function () {

            ShowLoading();
        },
        complete: function () {

            HideLoading();
        }
    });
    $(".grid-details").hide();
    $('.grid-btn-prev').hide();
    $('.grid-btn-next').hide();
}

//Bind Grid for XLS 
function GenerateReportPreviewGridData(json, columns) {
    $("#dvJqxgrid_ReportPreviewXLS").css("height", $("#hdnGridHeight").val() + "px");
    var countColumns = columns.length;
    //alert(countColumns);
    $("#jqxWidgetPreview").css("width", countColumns * 200 + "px");
    var rows = json;
    var source =
    {
        datatype: "json",
        id: 'id',
        localdata: rows
    };
    var dataAdapter = new $.jqx.dataAdapter(source,
         {
             loadComplete: function () {
                 var length = dataAdapter.records.length;
                 $("#hdnTotalRecords").val(parseInt(length));

             }
         });

    $("#dvJqxgrid_ReportPreviewXLS").jqxGrid(
    {
        width: '100%',
        height: PreviewGridHeight,
        autowidth: false,
        sortable: true,
        filterable: true,
        pageable: false,
        columnsreorder: true,
        source: dataAdapter,
        columnsresize: true,
        columns: columns
    });

    $("#dvJqxgrid_ReportPreviewXLS").jqxGrid({ columns: columns });
    $("#dvJqxgrid_ReportPreviewXLS").jqxGrid("render");
    // initialize the popup window and buttons.
}

//Render CSV data
function GenerateCSVDataGrid(report, FileType, Isdownload, reportCriteriaDetail) {

    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + '/Report/' + report,
        cache: false,
        data: { FileType: FileType, Isdownload: Isdownload, reportCriteriaDetail: reportCriteriaDetail },
        success: function (data) {
            if (data != null && data.reportRetailerBoxDetails != null && data.gridColumns != null) {
                var table = "";
                var tr = "";
                if (data.reportRetailerBoxDetails.length != 0) {
                    for (var i = 0; i < data.reportRetailerBoxDetails.length; i++) {
                        tr += "<p>";
                        for (var col = 0; col < data.gridColumns.length; col++) {
                            var val = data.reportRetailerBoxDetails[i][data.gridColumns[col].datafield];
                            if (val == null) val = "";
                            tr += val + ",";
                        }
                        if (tr.length > 3) {
                            tr = tr.substr(0, tr.length - 1);
                        }
                        tr += "</p>";
                    }
                    table += tr;
                    $("#dvJqxgrid_ReportPreviewCSV").empty().append(table);
                }
                else {
                    table += "<p style=\"text-align: center\;color:red\"><b>No data to display</b></p>";
                    $("#dvJqxgrid_ReportPreviewCSV").empty().append(table);
                }
            }

        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }, beforeSend: function () {

            ShowLoading();
        },
        complete: function () {

            HideLoading();
        }
    });

}
// Functions to use 
function isReportIframeReady() {
    if ($("#ifReport").length > 0 && $("#ifReport").is(":visible")) {
        return true;
    }
    return false;
}

function ReportPageCount() {
    if (isReportIframeReady() == true) {
        return document.getElementById('ifReport').contentWindow.PageCount();
    }
    return 0;
}
function ReportCurrentPageNumber() {
    if (isReportIframeReady() == true) {
        return document.getElementById('ifReport').contentWindow.CurrentPageNumber();
    }
    return 0;
}

function GoToNextPage() {
    document.getElementById('ifReport').contentWindow.GoToNextPage();
}
function GoToPreviousPage() {
    document.getElementById('ifReport').contentWindow.GoToPreviousPage();
}
function ExportReport() {
    var FileType = $('select#FileType option:selected').text();

    if (typeof (FileType) != "undefined" && FileType != "Select") {

        document.getElementById('ifReport').contentWindow.ExportReport(FileType);
    }
}


function ChangeReportName() {
    var reportName = _ReportName;
    $("#hdnReportName").val(reportName);
}


window.ShowReportNumber = function ShowReportNumber(numbertext) {
    $("#reportPageNumber").show();
    $("#reportPageNumber").text(numbertext);
}
//If unauthorized error occurred in any Ajax call, we navigate user to login page.
$(document).ajaxError(function (xhr, response, text) {
    console.log(xhr);
    if (response.status == 401) { window.location.reload(); }
});




var imageWidth = 0;
var imageHeight = 0;
//Zoom in functionality for pop-up
$("#btn_Image_zoomIn").click(function () {
    imageWidth = parseFloat($("#displayImage .image-popup-text img").css("width"));
    imageHeight = parseFloat($("#displayImage .image-popup-text img").css("height"));

    if (imageWidth > 1200) return;

    imageWidth = imageWidth + 200;
    imageHeight = imageHeight + 100;
    $("#displayImage .image-popup-text img").css("width", imageWidth);
    $("#displayImage .image-popup-text img").css("height", imageHeight);



});
//Zoom out functionality for pop-up
$("#btn_Image_zoomOut").click(function () {

    imageWidth = parseFloat($("#displayImage .image-popup-text img").css("width"));
    imageHeight = parseFloat($("#displayImage .image-popup-text img").css("height"));

    if (imageWidth <= 634) return;

    imageWidth = imageWidth - 200;
    imageHeight = imageHeight - 100;
    $("#displayImage .image-popup-text img").css("width", imageWidth);
    $("#displayImage .image-popup-text img").css("height", imageHeight);

});
//Close event functionality for send email pop-up
$("#btnCancel,.alert-close, .email-close ").click(function () {
    $("#sendEmail").hide();
});
//Close event functionality for display Image and video pop-up
$(".alert-close").click(function () {
    $("#displayImage").hide();
    $('.video_box').removeAttr("controls", 'controls');
});
//Function for preview images videos data
function PreviewImagesVideosData(json, columns) {
    var rows = json;

    var source =
    {
        datatype: "json",
        id: 'id',
        localdata: rows
    };
    var dataAdapter = new $.jqx.dataAdapter(source,
         {
             loadComplete: function () {
                 var length = dataAdapter.records.length;
             }
         });

    var initrowdetails = function (index, parentElement, gridElement, record) {
        Details = record;
        $.ajax({
            url: resolveUrl() + '/Report/DisplayImageAndVideo',
            cache: false,
            type: 'Get',
            data: { ItemGuid: record.ItemGuid },
            success: function (result) {
                var isAllURI = false;
                var defaultImage = '<img style="margin:2px; margin-left: 10px;" width="100" height="100" src=' + resolveUrl() + '"/images/Defaultimages.jpg"/>';
                var defaultVideo = '<video style="margin:2px; margin-left: 10px;" width="100" height="70" poster=' + resolveUrl() + '"/images/no-video.gif">';


                if (result != null && typeof (result) != "undefined" && result.length > 0) {

                    var htmlForImage = '<table class="ImageTable" style="background-color:#eff0f1"><tr>';
                    var htmlVideo = '<table class="ImageTable"><tr>';
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].ContentType == "Images") {
                            htmlForImage += '<td valign="top"><input class="login-checkbox" type="checkbox"/><img onclick="window.ShowImage(this)" class="side"  style="margin:0px; margin-left: 10px;" width="100" height="100" title="' + result[i].ContentName + '" src="' + result[i].ContentSasURI + '" /></td>';
                            if (isUri(result[i].ContentSasURI)) {
                                isAllURI = true;
                            }

                        } else if (result[i].ContentType == "Videos") {
                            if (result[i].ContentName.indexOf(".mp4") != -1) {
                                htmlForImage += '<td valign="top"><input class="login-checkbox" type="checkbox"/><video preload="auto" onclick="window.ShowVideo(this)" class="side video_box customvideo"  style="margin:0px; margin-left: 10px;" width="200" height="100" title="' + result[i].ContentName + '" src="' + result[i].ContentSasURI + '"  type="video/mp4" /></td>';
                            }
                            else {
                                htmlForImage += '<td>' + defaultVideo + '</td>';
                            }

                        }
                        else {

                            htmlForImage += '<td>' + defaultImage + '</td>';
                        }
                    }

                }
                var htmlImageUpdated = "";

                if (isAllURI) {
                    btnSendEmail = '<input id="btnOpenSendEmailPopup" class="btn-primary" type="button" value="Email"/>';
                    htmlImageUpdated = btnSendEmail + htmlForImage;
                }
                else {
                    htmlImageUpdated = htmlForImage;
                }

                htmlForImage += '</tr></table>';

                $('#grid' + index).empty().append(htmlImageUpdated);

            }
            , beforeSend: function () {
                var defaultHtml = '<table class="ImageTable" style=""><tr>' +
                  '<td><img  style="margin:2px; margin-left: 10px;" width="100" height="100" /></td>' +
                  '</tr></table>';
                $('#grid' + index).empty().append(defaultHtml);
            }

        });


    }

    function isUri(source) {
        return source.indexOf("http") > -1;
    }
    var renderer = function (row, column, value) {
        return '<span style="margin-left: 4px; margin-top: 9px; float: left;">' + value + '</span>';
    }


    // creage jqxgrid
    $("#dvJqxgrid_ReportImageVideo").jqxGrid(
    {
        width: '100%',
        height: PreviewGridHeight,
        source: source,
        sortable: true,
        autowidth: false,
        rowdetails: true,
        columnsresize: true,
        rowsheight: 25, pageable: false,
        initrowdetails: initrowdetails,
        rowdetailstemplate: { rowdetails: "<div id='grid' style='width:100%;overflow:auto'></div>", rowdetailsheight: 120, rowdetailshidden: true },
        columns: columns
    });

    Imagedialog = $("#window").dialog({
        autoOpen: false,
        height: 550,
        width: 850,
        maxHeight: 1024,
        maxWidth: 600,
        resizable: false,
        title: 'Image',
        left: 0,
        modal: true,
        buttons: {
            Cancel: {
                text: "Close",
                click: function () {
                    Imagedialog.dialog("close");
                }
            }
        }
    });

    Videodialog = $("#videoPopup").dialog({
        autoOpen: false,
        height: 550,
        width: 850,
        maxHeight: 1024,
        maxWidth: 600,
        resizable: false,
        left: 0,
        modal: true,
        title: 'Video',
        buttons: {
            Cancel: {
                text: "Close",
                click: function () {
                    Videodialog.dialog("close");
                }
            }
        }
    });


    //Function for display image and video in popup
    var contentType = "";
    window.ShowImage = function ShowImage(ele) {
        $(".magnify").data("jfMagnify").scaleMe(1);
        $(".ui-dialog-titlebar-close").hide()
        $(".ui-dialog").css("border", "#125484 solid 2px");
        Imagedialog.dialog("open");
        console.log("created");
        contentType = ele.tagName;

        if (contentType == "IMG") {
            var clone = $(ele).clone();
            $(".ui-dialog-title").html("Image | " + clone.attr('title'));
            $('.element_to_magnify').html(clone);
            $('.element_to_magnify img').css({ 'width': '850px', 'height': '550px', 'margin': '0px' });
        }


        $('#window img').removeAttr('onclick');
        $('.video_box').attr("controls", 'controls');

    }
}
window.ShowVideo = function ShowVideo(ele) {
    $(".ui-dialog-titlebar-close").hide()
    Videodialog.dialog("open");
    var clone = $(ele).clone();
    $(".ui-dialog-title").html("Video | " + clone.attr('title'));

    $('#videoPopup').html(clone);
    $('#videoPopup video').css({ 'width': '700px', 'height': '450px', 'margin': '0px' });
    $('#videoPopup video').removeAttr('onclick').attr("controls", 'controls');

}


$(document).on('click', '#btnOpenSendEmailPopup', function (event) {
    var ImageHtml = '<table>';
    $(this).closest("div").find("table.ImageTable img").each(function (index) {
        var checked = $(this).prev("input[type=checkbox ]").prop('checked');
        if (checked) {
            ImageHtml += '<tr><td><a style="color:blue" href="' + this.src + '">' + this.title + '</a></td></tr>';
        }
    });
    ImageHtml += '</table>';

    var VideoHtml = '<table>';

    $(this).closest("div").find("table.ImageTable video").each(function (index) {
        var checked = $(this).prev("input[type=checkbox ]").prop('checked');
        if (checked) {
            VideoHtml += '<tr><td><a style="color:blue" href="' + this.src + '">' + this.title + '</a></td></tr>';
        }
    });
    VideoHtml += '</table>';

    $("#sendEmail #divImage").empty().html(ImageHtml);
    $("#sendEmail #divVideo").empty().html(VideoHtml);
    $("#txtEmail").val("");
    $("#txtEmail").focus();
    $("#sendEmail").show();
});

$("#btnSendEmail").click(function () {

    var emailBody = [];
    var objImages;
    var objVideos;
    $("#divImage").find("a").each(function (index) {

        objImages = { ContentType: "Images", ContentName: this.innerHTML, ContentSasURI: this.href };
        emailBody.push(objImages);

    });
    $("#divVideo").find("a").each(function (index) {

        objVideos = { ContentType: "Videos", ContentName: this.innerHTML, ContentSasURI: this.href };
        emailBody.push(objVideos);

    });

    var emails = $("#txtEmail").val().trim();
    if (emails == "") {
        $("#txtEmail").val("");
        AlertMessage("Please enter valid email id(s).", false);
        $("#txtEmail").focus();
        return;
    }
    var invalidEmails = ReturnInvalidEmails(emails);
    if (invalidEmails != "") {
        AlertMessage(invalidEmails + " not valid email(s).", false);
        $("#txtEmail").focus();
        return;
    }

    $.ajax({
        type: "Post",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + "/Report/SendEmail",
        cache: false,
        data: "{ 'EmailBody': '" + JSON.stringify(emailBody) + "', 'EmailList': '" + emails + "','Details':'" + JSON.stringify(Details) + "' }",
        //traditional: true,
        //async: false,
        success: function (data) {
            AlertMessage(data, true);
        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }, beforeSend: function () {
            ShowLoading();
        },
        complete: function () {
            HideLoading();
        }
    });
});
//Function for create report image video design
function CreateReportImageVideoDesign(report, FileType, Isdownload, reportCriteriaDetail) {

    $.ajax({
        type: "Get",
        contentType: 'application/json; charset=utf-8',
        url: resolveUrl() + '/Report/' + report,
        cache: false,
        data: { FileType: FileType, Isdownload: Isdownload, reportCriteriaDetail: reportCriteriaDetail }, // Note it is important
        success: function (data) {

            $('#dvJqxgrid_ReportImageVideo').jqxGrid('clear');
            $("#dvJqxgrid_ReportImageVideo").jqxGrid("render");
            $("#dvJqxgrid_ReportImageVideo").jqxGrid('updatebounddata');
            PreviewImagesVideosData(data.reportRetailerBoxDetails, data.gridColumns);
        },
        complete: function () {
            HideLoading();
        },
        error: function (response) {
            if (response.status != 0 && response.status != 401) {
                alert(response.status + " " + response.statusText);
            }
        }
    });
    $(".grid-details").hide();
    $('.grid-btn-prev').hide();
    $('.grid-btn-next').hide();
}

//Function for showing alert message
function AlertMessage(message, hideEmailBox) {
    $(".alert-popup-body-message-confirm").hide();
    $("#alertmessageid").text(message);
    $(".alert-popup-body-message").show();
    $("#btnok").click(function () {
        $(".alert-popup-body-message").hide();
        if (hideEmailBox && $(".email-popup-body-confirm").length > 0)
            $(".email-popup-body-confirm").hide();
    });
}
var scaleNum = 1;
$(document).ready(function () {
    SetMagnify();
});

//Function to set zoom level
function SetMagnify() {
    $(".magnify").jfMagnify();
    $('.plus').click(function () {
        scaleNum += .3;
        if (scaleNum >= 3) {
            scaleNum = 3;
        };
        $(".magnify").data("jfMagnify").scaleMe(scaleNum);
    });
    $('.minus').click(function () {
        scaleNum -= .3;
        if (scaleNum <= 1) {
            scaleNum = 1;
        };
        $(".magnify").data("jfMagnify").scaleMe(scaleNum);
    });
    $('.magnify_glass').animate({
        'top': '55%',
        'left': '60%'
    }, {
        duration: 3000,
        progress: function () {
            $(".magnify").data("jfMagnify").update();
        },
        easing: "easeOutElastic"
    });

}

function setScale(scale) { scaleNum = scale; $(".magnify").data("jfMagnify").scaleMe(scaleNum); }

//Run Standard Report
var isRunEnable = true;
$("#btn_toolbar_Run").on('click', function (event) {
    if (isRunEnable) {
        RunStandardReport();
    }
});


//Function to generate report without schedule.
function RunStandardReport() {
    var report = _ReportName;
    DisableToolbarControl("#btn_toolbar_grid");
    DisableToolbarControl("#btn_toolbar_search");    
   
    var FileType = $('select#FileType option:selected').text();
    var ReportDataTimeZone = $('select#ReportDataTimeZone option:selected').text();
    var ReportScheduleTimeZone = $('select#ReportScheduleTimeZone option:selected').text();
    var Isdownload = false;
    if (FileType != "Select" && ReportScheduleTimeZone != "Select" && ReportDataTimeZone != "Select" && FileType != "" && ReportScheduleTimeZone != "" && ReportDataTimeZone != "") {
        if ($("#hdnEditNewClicked").val() == "") {
            $("#hdnRunOnlineReport").val("");
        }
        else {

            $("#hdnRunOnlineReport").val("OnlineReport");
            $("#hdnEditNewClicked").val("");
        }
        $("#btn_toolbar_Run").attr("src", "/images/RunStandardDisable.png");
        DisableToolbarControl("#btn_toolbar_Exclamation");
        EnableToolbarControl("#btn_toolbar_download");
        isRunEnable = false;
        $("#gridData").hide();
        $("#reportScheduleHistoryHeading").hide();
        $("#reportScheduleHeading").hide();
        $("#reportPreviewHeading").show();
        $("#reportImageVideoHeading").hide();
        $("#anchorPreview").removeClass('active');
        $("#anchorDetail").removeClass('active');
        $("#anchorHistory").removeClass('active');
        $("#anchorImagesVideos").removeClass('active');
        $("#dvJqxgrid_ReportSummary").hide();
        $("#dvJqxgrid_schedule").hide();
        $("#dvJqxgrid_ReportImageVideo").hide();
        $("#dvReportPreview").show();
        $(".report-wrapper").hide();
        GenerateReportAsPerFileType(FileType, Isdownload);
       
    }
    else {
        AlertMessage("Please select Type and Timezone under Report Schedule for generating online report.", false);
    }

    event.stopPropagation();
}

function GenerateReportAsPerFileType(FileType, Isdownload)
{
    if (FileType == "PDF") {

        $("#dvReportPreviewXLS").hide();
        if (report == ReportNames.ProductDetails) {
            reportCriteriaDetail = productDetailCriteria();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
        else if (report == ReportNames.WasteReport) {
            reportCriteriaDetail = wasteReportCriteria();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
        else if (report == ReportNames.PharmacySummary) {
            reportCriteriaDetail = PharmacySummaryDetails();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
        else if (report == ReportNames.NonCreditableSummary) {
            reportCriteriaDetail = NonCreditableSummaryDetails();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
        else if (report == ReportNames.ManufacturerSummary) {
            reportCriteriaDetail = ManufacturerSummaryDetails();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
        else if (report == ReportNames.ReturnToStock) {
            reportCriteriaDetail = ReturnToStockDetails();
            reportCriteriaDetail = RemoveAmpersandFromJson(reportCriteriaDetail);
            $("#ifReport").attr("src", resolveUrl() + "/Report/" + report + "?FileType=" + FileType + "&Isdownload=" + false + "&reportCriteriaDetail=" + reportCriteriaDetail);
        }
    }
    else if (FileType == "XLS") {

        $("#dvReportPreview").hide();
        $("#dvReportPreviewXLS").show();
        $("#dvJqxgrid_ReportPreviewCSV").hide();
        $("#dvJqxgrid_ReportPreviewXLS").show();
        if (report == ReportNames.ProductDetails) {
            reportCriteriaDetail = productDetailCriteria();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.WasteReport) {
            reportCriteriaDetail = wasteReportCriteria();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.PharmacySummary) {
            reportCriteriaDetail = PharmacySummaryDetails();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.NonCreditableSummary) {
            reportCriteriaDetail = NonCreditableSummaryDetails();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.ManufacturerSummary) {
            reportCriteriaDetail = ManufacturerSummaryDetails();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.ReturnToStock) {
            reportCriteriaDetail = ReturnToStockDetails();
            CreateReportPreviewGridDesign(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
    }
    else if (FileType == "CSV") {

        $("#dvReportPreview").hide();
        $("#dvReportPreviewXLS").show();
        $("#dvJqxgrid_ReportPreviewXLS").hide();
        $("#dvJqxgrid_ReportPreviewCSV").show();
        if (report == ReportNames.ProductDetails) {
            reportCriteriaDetail = productDetailCriteria();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.WasteReport) {
            reportCriteriaDetail = wasteReportCriteria();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.PharmacySummary) {
            reportCriteriaDetail = PharmacySummaryDetails();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.NonCreditableSummary) {
            reportCriteriaDetail = NonCreditableSummaryDetails();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.ManufacturerSummary) {
            reportCriteriaDetail = ManufacturerSummaryDetails();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
        else if (report == ReportNames.ReturnToStock) {
            reportCriteriaDetail = ReturnToStockDetails();
            GenerateCSVDataGrid(report, FileType, Isdownload, JSON.stringify(reportCriteriaDetail));
        }
    }
    DisableEnableToolbarAfterPreview();
}

//Function for remove and disable control value
function RemoveAndDisableControlValue() {

    $("#SearchPanelList :input").prop("disabled", false);
    $('#divReportScheduler :input').val('');
    $('#SearchPanelList :input').val('');

    $("#divReportScheduler option[value='']").attr('selected', true)
    $("#StartDate").datepicker('setDate', null);
    $("#EndDate").datepicker('setDate', null);

    $('#FileType').attr('disabled', false);
    $('#ReportScheduleTimeZone').attr('disabled', false);
    $('#ReportDataTimeZone').attr('disabled', false);
    $("#SearchPanelList :input").css("cursor", "text");

    $("#FileType").css("background-color", "#ECDD9D");
    $("#ReportScheduleTimeZone").css("background-color", "#ECDD9D");
    $("#ReportDataTimeZone").css("background-color", "#ECDD9D");

}
var isExclamationEnable = false;
$("#btn_toolbar_Exclamation").on('click', function (event) {
    isExclamationEnable = true;
    if (isExclamationEnable == true) {
        RemoveAndDisableControlValue();
    }
    $("#divReportScheduler option[value='']").attr('selected', false)

});

//Function for unbind detail grid row select
function unbindDetailGridRowSelect() {
    $("#dvJqxgrid_schedule").unbind();
}
function disableCriteriaAndSchedule() {
    $("#SearchPanelList :input").attr("disabled", true);
    $("#divReportScheduler :input").prop('disabled', true);;
}