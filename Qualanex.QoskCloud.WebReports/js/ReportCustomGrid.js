function BindCustomSearch() {
    // custom grid list selected move
    $('#move_right').click(function () {
        var selectedOpts = $('.list1 option:selected');

        if (selectedOpts.length > 0) {
            $('.list2').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            return;
        }
        else {
            $("#alertmessageid").text(Common.SelectLeft);
            $(".alert-popup-body-message").show();
            return;
        }
    });

    $('#move_left').click(function (e) {

        var selectedOpts = $('.list2 option:selected');
        if (selectedOpts.length > 0) {
            if (selectedOpts.attr('data-moveable') == "true") {

                e.preventDefault();
                return;
            }
            else {
                $('.list1').append($(selectedOpts).clone());
                $(selectedOpts).remove();
                e.preventDefault();
                return;
            }
        }
        else {
            $("#alertmessageid").text(Common.SelectRight);
            $(".alert-popup-body-message").show();
            return;
        }

    });

    $('.CustomSearch').children().click(function () {
        $this = $(this);
        var $op2 = $('.list2 option:selected');

        if ($op2.length) {
            if ($this.val() == 'Up')
                $op2.first().prev().before($op2);
            else if ($this.val() == 'Down')
                $op2.last().next().after($op2);
        }
    });

    $(".close-btn, .cancel, #btnSubmit").click(function () {
        $(".model-popup,.custom-grid").hide();
        $(".model-box").animate({ "top": "-100%" });

    });


}
//BindCommon
function BindCommon() {
    $(".gapping-inline").click(function () {
        $(".model-popup").show();
        $(".model-box").animate({ "top": "20%" });
    });
    $("#btn_toolbar_search").click(function () {
        var hei = $(window).height();
        var popctr = (hei - 435) / 2

        $(".model-popup").show();
        $(".custom-search-box").animate({ "top": +popctr + "px" });
    });

    // drag and drop

    $("#btn_toolbar_grid").click(function () {

        var heigrid = $(window).height();
        var popctrgrid = (heigrid - 435) / 2

        $(".custom-grid").show();
        $(".custom-search-box").animate({ "top": +popctrgrid + "px" });
    });

    $(".close-btn, .cancel, #btnSubmit").click(function () {
        $(".model-popup,.custom-grid").hide();
        $(".model-box").animate({ "top": "-100%" });
    });

    // empty box color chenge search 

    setInterval(function () { list2bg() }, 100);

    setInterval(function () { fotheight() }, 100);

    // custom gid
    $(".custom-grid").click(function () {
        $(".model-box").animate({ "top": "-100%" });
        $(".custom-grid").hide();
    }).children().click(function (e) {
        return false;
    });
}
//BindCustomGrid
function BindCustomGrid() {

    BindCommon();

    // popup box	
    // custom grid list selected move

    $('#move_right1').click(function (evt) {

        var selectedOpts = $('.list3 option:selected');
        if (selectedOpts.length > 0) {
            $('.list4').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            evt.stopPropagation();
            evt.preventDefault();
        }
        else {

            $("#alertmessageid").text(Common.SelectLeft);
            $(".alert-popup-body-message").show();
            return;
        }

    });



    $('#move_left1').click(function (e) {

        var selectedOptss = $('.list4 option:selected');
        if (selectedOptss.length > 0) {
            if (selectedOptss.attr('data-moveable') == "true") {

                e.preventDefault();
                return;
            }
            else {
                $('.list3').append($(selectedOptss).clone());
                $(selectedOptss).remove();
                e.preventDefault();
                return;
            }
        }
        else {

            $("#alertmessageid").text(Common.SelectRight);
            $(".alert-popup-body-message").show();
            return;
        }
    });

    $('.CustomGrid').children().click(function () {

        $this = $(this);
        var $op1 = $('.list4 option:selected');
        if ($op1.length) {
            if ($this.val() == 'Up')
                $op1.first().prev().before($op1);
            else if ($this.val() == 'Down')
                $op1.last().next().after($op1);
        }

    });
    // custom gid
    $(".custom-grid").click(function () {
        $(".model-box").animate({ "top": "-100%" });
        $(".custom-grid").hide();
    }).children().click(function (e) {
        return false;
    });

}

// empty box color chenge grid 

setInterval(function () { list2gridbg() }, 100);
function list2bg() {
    var list2hei = $(".list2 option").height();
    if (list2hei == null) {
        $(".list2").css({ "background-color": "#ebebeb", "border": "1px solid #ebebeb" });
    }
    else {
        $(".list2").css({ "background-color": "#ecf2e0", "border": "1px solid #c3e49b" });
    }
}
function list2gridbg() {
    var list2gridhei = $(".list4 option").height();
    if (list2gridhei == null) {
        $(".list4").css({ "background-color": "#ebebeb", "border": "1px solid #ebebeb" });
    }
    else {
        $(".list4").css({ "background-color": "#ecf2e0", "border": "1px solid #c3e49b" });
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if (String(ev.target.parentElement.tagName).toLowerCase() == "select") {
        ev.target.parentElement.options.add(document.getElementById(data), ev.target.parentElement.options[ev.target.index])
    }
    else {
        ev.target.appendChild(document.getElementById(data));
    }
}

//Function for footer height
function fotheight() {
    var fthei = $("body").height();
    var ftdht = $(".wrapper").height();
    var lnht = ($(".main-container").height()) + 90;
    if (ftdht > fthei) {
        $(".left-navigation").css({ "height": +lnht + "px", "bottom": "auto", "padding-top": "0" });
    }
    else {
        $(".left-navigation").css({ "bottom": "0px", "padding-top": "143px", "height": "100%" });
    }
}