﻿using Qualanex.QoskCloud.WebReports.Models;

namespace Qualanex.QoskCloud.WebReports.ReportClasses
{
    /// <summary>
    /// ProductDetailsEmailEntity: Entity class for Product Details report.
    /// </summary>
    public class ProductDetailsEmailEntity : RetailerProductDetailsEntity
    {
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DEANumber { get; set; }
        public string ContainerTypeName { get; set; }
        public string SealedOpenCase { get; set; }

    }
}