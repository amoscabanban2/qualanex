﻿using Qualanex.QoskCloud.WebReports.Models;

namespace Qualanex.QoskCloud.WebReports.ReportClasses
{
    /// <summary>
    /// WasteReportEmailEntity: Entity class for Waste Report.
    /// </summary>
    public class WasteReportEmailEntity : WasteReportEntity
    {
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DEANumber { get; set; }
        public string ContainerTypeName { get; set; }
        public string QoskProcessDate { get; set; }
        public string VendorName { get; set; }

    }
}