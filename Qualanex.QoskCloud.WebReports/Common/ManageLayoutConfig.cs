﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.WebReports.Models;

namespace Qualanex.QoskCloud.WebReports.Common
{
    public class ManageLayoutConfig
    {
        /// <summary>
        /// GetColumnListingByLayout
        /// </summary>
        /// <param name="LayoutName"></param>
        /// <param name="UserID"></param>
        /// <param name="ForGridColumnListing"></param>
        /// <returns></returns>
        public ColumnListEntity GetColumnListingByLayout(string LayoutName, long UserID, bool ForGridColumnListing)
        {
            ColumnListRequest objColumnListRequest = new ColumnListRequest();
            objColumnListRequest.ForGridColumnListing = ForGridColumnListing;
            objColumnListRequest.LayoutName = LayoutName;
            objColumnListRequest.UserId = UserID;
            CommonDataAccess objCommon = new CommonDataAccess();
            return objCommon.GetAllColumnListing(objColumnListRequest);
        }
        /// <summary>
        /// SaveSubscribedColumn
        /// </summary>
        /// <param name="objColumnListing"></param>
        /// <param name="UserID"></param>
        /// <param name="LayoutID"></param>
        /// <returns></returns>
        public bool SaveSubscribedColumn(ColumnListEntity objColumnListing, long UserID, int LayoutID)
        {
            CommonDataAccess objCommon = new CommonDataAccess();
            return objCommon.SaveSubscribedColumnListing(objColumnListing, UserID, LayoutID);
        }
    }
}