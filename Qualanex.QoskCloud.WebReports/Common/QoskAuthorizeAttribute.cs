﻿using Qualanex.QoskCloud.Utility;
using System;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.WebReports.Common
{
    /// <summary>
    /// Check a User IsAuthenticated
    /// </summary>
    public class QoskAuthorizeAttribute : AuthorizeAttribute
    {

        public QoskAuthorizeAttribute()
        {
            View = Constants.QoskAuthorizeAttribute_View;
            Master = String.Empty;
        }

        public String View { get; set; }
        public String Master { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            CheckIfUserIsAuthenticated(filterContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckIfUserIsAuthenticated(AuthorizationContext filterContext)
        {
            // If Result is null, we’re OK: the user is authenticated and authorized. 
            if (filterContext.Result == null)
                return;

            // If here, you’re getting an HTTP 401 status code. In particular,
            // filterContext.Result is of HttpUnauthorizedResult type. Check Ajax here. 
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {

                if (String.IsNullOrEmpty(View))
                    return;
                var result = new ViewResult { ViewName = View, MasterName = Master };
                filterContext.Result = result;
            }
            else {
                string routeString = string.Empty;
                //Navigae the use to the Admin website, If Unauthorized.
                if (filterContext.RouteData.Values[Constants.RouteConfig_ReportUrl_OptionalParameter] != null && !string.IsNullOrEmpty(filterContext.RouteData.Values[Constants.RouteConfig_ReportUrl_OptionalParameter].ToString()))
                {
                    foreach (var routedata in filterContext.RouteData.Values)
                    {
                        routeString += routedata.Value + Constants.Single_Forward_Slash;
                    }
                    routeString = routeString.Substring(0, routeString.Length - 1) + filterContext.HttpContext.Request.Url.Query;
                }
                //Uncomment below code at the time of deployment and comment when debug the code locally.
                #region 1. Uncomment below code at the time of deployment and comment when debug the code locally.
                
                if (!string.IsNullOrWhiteSpace(routeString))
                {
                    filterContext.Result = new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Constants.AdminWebsiteUrl) + routeString.Trim());
                }
                else
                {
                    filterContext.Result = new RedirectResult(Utility.Common.ConfigurationManager.GetAppSettingValue(Constants.AdminWebsiteUrl));
                }
                
                #endregion

                //Uncomment below code to debug locally and comment when deployment.
                #region 2. Uncomment below code to debug locally and comment when deployment.
                //filterContext.Result = new RedirectResult("~/Account/Login");
                #endregion
            }
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                // For an Ajax request, just end the request 
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.End();

            }
        }
    }
}