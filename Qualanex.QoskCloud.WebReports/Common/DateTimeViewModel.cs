﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using System;

namespace Qualanex.QoskCloud.WebReports.Common
{
    /// <summary>
    /// DateTimeViewModel: This class used to create dynamic date picker control
    /// </summary>
    public class DateTimeViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_datepicker; }
        }
        public DateTime? Value { get; set; }
    }
}