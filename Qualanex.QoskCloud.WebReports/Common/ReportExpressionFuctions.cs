﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Telerik.Reporting.Drawing;

namespace Qualanex.QoskCloud.WebReports
{
    /// <summary>
    /// Class is represent functions those are available in expression in report designer.
    /// </summary>
    public class ReportExpressionFuctions : Telerik.Reporting.Report
    {
        /// <summary>
        /// Convert UTC datetime in specified time zone datetime.
        /// </summary>
        /// <param name="rpReportDataTimeZone"></param>
        /// <param name="UtcDate"></param>
        /// <returns></returns>   
        public static DateTime? ConvertUTCToTimeZone(string rpReportDataTimeZone, DateTime? UtcDate)
        {
            return SchedulerFunctions.ConvertUTCToTimeZoneDate(rpReportDataTimeZone, Convert.ToDateTime(UtcDate));
        }

        /// <summary>
        /// Convert date into UTC date from local time.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime ConvertDateIntoUTC(DateTime date)
        {
            return date.ToUniversalTime();
        }
      
      
        /// <summary>
        /// CreateTxtHeader is used to generate dynamic report
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private Telerik.Reporting.TextBox CreateTxtHeader(string FieldName, int i)
        {
            Telerik.Reporting.TextBox txtHead = new Telerik.Reporting.TextBox();
            txtHead.Style.BackgroundColor = Color.Empty;
            txtHead.Style.BorderStyle.Default = BorderType.Solid;
            txtHead.Style.Font.Bold = true;
            txtHead.CanGrow = true;
            txtHead.Value = FieldName;
            return txtHead;
        }
        /// <summary>
        /// CreateTxtDetail is used to generate dynamic report
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private Telerik.Reporting.TextBox CreateTxtDetail(string FieldName, int i)
        {
            Telerik.Reporting.TextBox txtDetail = new Telerik.Reporting.TextBox();
            txtDetail.Style.BackgroundColor = Color.Empty;
            txtDetail.Style.BorderStyle.Default = BorderType.Solid;
            txtDetail.Style.Font.Bold = false;
            txtDetail.CanGrow = true;
            txtDetail.Value = "=Fields." + FieldName;
            return txtDetail;
        }

        /// <summary>
        /// Generate report based on user specific settings
        /// </summary>
        /// <param name="userid"></param>
       public void GenerateReportDefinition(long userid, object filetype, List<ColumnDetails> subscribedColumns, Telerik.Reporting.Report report, Telerik.Reporting.Panel panel1, Telerik.Reporting.DetailSection detail)
        {
            Telerik.Reporting.Report report1 = new Telerik.Reporting.Report();
            int count = subscribedColumns.Count;

            Unit x = Unit.Inch(0);
            Unit y_detail = Unit.Inch(0);
            Unit y_header = Unit.Inch(0.4);
            double columnWidth = 2;

            double columnMaxHeight = 0;

            Telerik.Reporting.ReportItemBase[] headColumnList = new Telerik.Reporting.ReportItem[count + 1];
            Telerik.Reporting.ReportItemBase[] detailColumnList = new Telerik.Reporting.ReportItem[count];

            for (int column = 0; column < count; column++)
            {
                string columnName = subscribedColumns[column].ColumnName;
                string columnDisplayText = subscribedColumns[column].DisplayText;


                Telerik.Reporting.TextBox header = CreateTxtHeader(columnDisplayText, column);
                header.Location = new Telerik.Reporting.Drawing.PointU(x, y_header);
                header.Name = "textboxHeader" + column;
                Unit s = header.Height;

                Telerik.Reporting.TextBox textBox = CreateTxtDetail(columnName, column);
                textBox.Location = new Telerik.Reporting.Drawing.PointU(x, y_detail);
                textBox.Name = "textboxDetail" + column;

                headColumnList[column] = header;
                detailColumnList[column] = textBox;

                header.Width = Unit.Inch(columnWidth);
                header.Height = GetInchForHeightOverCharacter(header.Value.Trim().Length);
                columnMaxHeight = Math.Max(header.Height.Value, columnMaxHeight);

                textBox.Width = Unit.Inch(columnWidth);
                textBox.Height = Unit.Inch(0.2);
                x += Unit.Inch(columnWidth);
            }

            Telerik.Reporting.ReportHeaderSection reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            if (filetype != null && filetype.ToString() == "XLS")
            {
                panel1.Visible = false;
                reportHeaderSection1.Height = new Unit(0.2, UnitType.Inch);
                reportHeaderSection1.Style.BackgroundColor = Color.White;
                detail.Style.BackgroundColor = System.Drawing.Color.White;
            }
            else
            {
                reportHeaderSection1.Height = new Unit(0.6, UnitType.Inch);
                reportHeaderSection1.Style.BackgroundColor = Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(186)))), ((int)(((byte)(74)))));
            }
            headColumnList.ToList().ForEach(xe => { if (xe != null) ((Telerik.Reporting.TextBox)xe).Height = Unit.Inch(columnMaxHeight); });
            headColumnList[headColumnList.Count() - 1] = panel1;
            reportHeaderSection1.Items.AddRange(headColumnList);

            Telerik.Reporting.DetailSection detailSection1 = new Telerik.Reporting.DetailSection();
            detailSection1.Height = new Unit(0.2, UnitType.Inch);
            detailSection1.Items.AddRange(detailColumnList);
            detailSection1.ItemDataBound += Detail_ItemDataBound;

            report.Items.Add(reportHeaderSection1);
            report.Items.Add(detailSection1);

            report.Width = panel1.Width = x;
            x += Unit.Inch(2);
            report.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            report.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(x, Telerik.Reporting.Drawing.Unit.Mm(250));
        }
        /// <summary>
        /// GetInchOverCharacter
        /// </summary>
        /// <param name="CharacterSize"></param>
        /// <returns></returns>
        double GetInchOverCharacter(int CharacterSize)
        {
            return (0.05 * CharacterSize);
        }

        /// <summary>
        /// GetInchOverCharacter
        /// </summary>
        /// <param name="CharacterSize"></param>
        /// <returns></returns>
        Unit GetInchForHeightOverCharacter(int CharacterSize)
        {
            if (CharacterSize >= 22)
                return Unit.Inch(((double)CharacterSize / 22) * 0.32);
            else return Unit.Inch(0.2);
        }

        /// <summary>
        /// Detail ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Detail_ItemDataBound(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.DetailSection detail = (Telerik.Reporting.Processing.DetailSection)sender;
            if (Telerik.Reporting.Processing.ElementTreeHelper.GetChildElements(detail).ToList().Count() > 0)
            {
                List<Telerik.Reporting.Processing.LayoutElement> elementCollection = Telerik.Reporting.Processing.ElementTreeHelper.GetChildElements(detail).ToList();
                object v = elementCollection.Max(x => (((Telerik.Reporting.Processing.TextBox)x).Value ?? "").ToString().Trim().Length);
                Unit MaxHeight = GetInchForHeightOverCharacter((int)v);
                elementCollection.ForEach(x => { if (x != null) ((Telerik.Reporting.Processing.TextBox)x).Height = MaxHeight; });
            }

        }

    }
}
