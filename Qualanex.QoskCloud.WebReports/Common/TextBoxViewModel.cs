﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.WebReports.Common
{
    /// <summary>
    /// TextBoxViewModel: This class is used to create dynamic textbox.
    /// </summary>
    public class TextBoxViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_textbox; }
        }
        public string Value { get; set; }
    }
}