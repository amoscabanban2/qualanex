﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.WebReports.Common
{
    /// <summary>
    /// CheckBoxViewModel: This class is used to create dynamic checkbox.
    /// </summary>
    public class CheckBoxViewModel : ControlViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_checkbox; }
        }
        public bool Value { get; set; }
    }
}