﻿using Qualanex.QoskCloud.Utility;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.WebReports.Common
{
    /// <summary>
    /// DropDownListViewModel: This class is used to create dynamic dropdown list.
    /// </summary>
    public class DropDownListViewModel : TextBoxViewModel
    {
        public override string Type
        {
            get { return Constants.ControlModelBinder_ddl; }
        }
        public SelectList Values { get; set; }
    }
}