﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.WebReports.Common;
using Qualanex.QoskCloud.WebReports.Models;
using Qualanex.QoskCloud.WebReports.Models.AzureUtility;
using Qualanex.QoskCloud.WebReports.ReportClasses;
using Qualanex.QoskCloud.WebReports.Resources;
using Qualanex.QoskCloud.WebReports.Retailer;
using Qualanex.QoskCloud.WebReports.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qualanex.Qosk.Library.Common.Logger;
using Qualanex.Qosk.Library.Model.DBModel;
using Telerik.Reporting;
using Constants = Qualanex.QoskCloud.Utility.Constants;
using Logger = Qualanex.QoskCloud.Utility.Logger;

namespace Qualanex.QoskCloud.WebReports.Controllers
{
    [QoskAuthorizeAttribute]
    public class ReportController : Controller
    {
        // GET: Report
        public static string ReportName = string.Empty;
        /// <summary>
        /// Index: First action method which execute when navigated from Admin Website.
        /// </summary>
        /// <param name="ReportId"></param>
        /// <returns></returns>
        public ActionResult Index(string ReportId)
        {
           try
           {
              if (string.IsNullOrEmpty(ReportId))
              {
                 return View(Utility.Constants.Report_ReportPreview);
              }
              ViewBag.ReportId = ReportId;
              ReportDataAccess reportDataAccess = new ReportDataAccess();
              ReportName = reportDataAccess.GetReportName(Convert.ToInt64(ReportId));
              if (string.IsNullOrEmpty(ReportName))
              {
                 return View(Utility.Constants.Report_ReportPreview);
              }
              List<ReportMasterLayoutList> reportMasterLayoutList = reportDataAccess.GetPanelLayoutId(ReportName);
              int SearchPanellayoutID =
                 reportMasterLayoutList.FirstOrDefault(
                    x => x.SectionType == Constants.GetSearchPanelLayoutId_SearchPanel).ReportMasterLayoutListID;
              int ListPanellayoutID =
                 reportMasterLayoutList.FirstOrDefault(x => x.SectionType == Constants.GetListPanelLayoutId_ListPanel)
                    .ReportMasterLayoutListID;
              string ReportModule = reportDataAccess.GetReportModule(Convert.ToInt64(ReportId));
              ViewBag.ReportName = ReportName;
              ViewBag.ReportModule = ReportModule;
              ViewBag.SearchPanellayoutID = SearchPanellayoutID;
              ViewBag.ListPanellayoutID = ListPanellayoutID;
              return View();
           }
           catch (Exception ex)
           {
            Logger.Error("Error",ex);
            return View(Utility.Constants.Report_ReportPreview);
           }
        }
        /// <summary>
        /// Sample_Report_Retailer_Box_Detail
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        public ActionResult Sample_Report_Retailer_Box_Detail(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long NDC = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Sample_Report_Retailer_Box_Detail).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportRetailerBoxDetails reportData = new ReportRetailerBoxDetails();
                List<RetailerBoxDetailsEntity> listReportPreview = new List<RetailerBoxDetailsEntity>();
                NDCNumber = string.IsNullOrEmpty(NDCNumber) ? "0" : NDCNumber;
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetRetailer_Box_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerBoxDetail)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// DownloadReportOnBrowser
        /// </summary>
        /// <param name="source"></param>
        /// <param name="FileType"></param>
        [NonAction]
        public void DownloadReportOnBrowser(TypeReportSource source, string FileType = Utility.Constants.Report_Using_FileType_pdf)
        {
            try
            {
                var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                var result = reportProcessor.RenderReport(FileType, source, null);
                string FileName = result.DocumentName + Utility.Constants.Report_Extension_Dot + result.Extension;
                this.Response.Clear();
                this.Response.ContentType = result.MimeType;
                this.Response.Cache.SetCacheability(HttpCacheability.Private);
                this.Response.Expires = -1;
                this.Response.Buffer = true;
                //Uncomment to handle the file as attachment
                Response.AddHeader(Utility.Constants.ResponseHeader_Content_Disposition,
                               string.Format(Utility.Constants.ResponseHeader_FileName_Format,
                                               Utility.Constants.ResponseHeader_Attachment,
                                               FileName));
                this.Response.BinaryWrite(result.DocumentBytes);
                this.Response.End();
            }
            catch
            {

            }
        }
        /// <summary>
        /// Sample_Reports_Retailer_Box_Summary
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_RetailerBoxSummary)]
        public ActionResult Sample_Reports_Retailer_Box_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long boxNumber = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Sample_Reports_Retailer_Box_Summary).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportRetailerBoxSummary reportData = new ReportRetailerBoxSummary();
                List<RetailerBoxSummaryEntity> listReportPreview = new List<RetailerBoxSummaryEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetRetailer_Box_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerBoxSummary)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Retailer_MGF_Summary
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_RetailerMFGSummary)]
        public ActionResult Retailer_MGF_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long boxNumber = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Retailer_MFG_Summary).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportRetailerMFGSummary reportData = new ReportRetailerMFGSummary();
                List<RetailerMFGSummaryEntity> listReportPreview = new List<RetailerMFGSummaryEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetRetailer_MFG_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerMFGSummary)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Retailer_MFG_Summary_Credits
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_RetailerMFGCreditSummary)]
        public ActionResult Retailer_MFG_Summary_Credits(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long boxNumber = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Retailer_MFG_Summary_Credits).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportRetailerMFGSummaryCredit reportData = new ReportRetailerMFGSummaryCredit();
                List<RetailerMFGSummaryCreditsEntity> listReportPreview = new List<RetailerMFGSummaryCreditsEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetRetailer_MFG_Credits_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerMFGSummaryCredits)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Retailer_Return_To_Stock_Details
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_RetailerReturnToStock)]
        public ActionResult Retailer_Return_To_Stock_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long NDC = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Retailer_Return_To_Stock_Details).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportRetailerReturnToStockDetailsEntity reportData = new ReportRetailerReturnToStockDetailsEntity();
                List<RetailerReturnToStockDetailsEntity> listReportPreview = new List<RetailerReturnToStockDetailsEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetRetailer_Return_Stock_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_RetailerReturnToStockDetails)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }

        }
        /// <summary>
        /// Retailer_Return_To_Stock_Summary
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_RetailerReturnToStockSummary)]
        public ActionResult Retailer_Return_To_Stock_Summary()
        {
            TypeReportSource Source = new TypeReportSource();
            Source.TypeName = typeof(Retailer_Return_To_Stock_Summary).AssemblyQualifiedName;
            ViewBag.ReportSource = Source;
            return View(Utility.Constants.Report_ReportRender);
        }
        /// <summary>
        /// Sample_Report_Wholesaler_Box_Details
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_WholesalerBoxDetail)]
        public ActionResult Sample_Report_Wholesaler_Box_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long NDC = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Sample_Reports_Wholesaler_Box_Detail).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportWholesalerBoxDetails reportData = new ReportWholesalerBoxDetails();
                List<WholesalerBoxDetailsEntity> listReportPreview = new List<WholesalerBoxDetailsEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetWholesaler_Box_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerBoxDetails)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Sample_Report_Wholesaler_Box_Summary
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_WholesalerBoxSummary)]
        public ActionResult Sample_Report_Wholesaler_Box_Details_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long NDC = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Sample_Reports_Wholesaler_Box_Summary).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportWholesalerBoxSummary reportData = new ReportWholesalerBoxSummary();
                List<WholesalerBoxSummaryEntity> listReportPreview = new List<WholesalerBoxSummaryEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetWholesaler_Box_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerBoxSummary)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Wholesaler_MFG_Summary_Credits
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_WholesalerMFGCreditSummary)]
        public ActionResult Wholesaler_MFG_Summary_Credits(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long boxNumber = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Wholesaler_MFG_Summary_Credits).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportWholesalerMFGSummaryCredits reportData = new ReportWholesalerMFGSummaryCredits();
                List<WholesalerMFGSummaryCreditsEntity> listReportPreview = new List<WholesalerMFGSummaryCreditsEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetWholesaler_MFG_Credits_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerMFGSummaryCredits)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Wholesaler_MGF_Summary
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_WholesalerMFGSummary)]
        public ActionResult Wholesaler_MGF_Summary(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long boxNumber = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Wholesaler_MFG_Summary).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_BoxNumber, boxNumber));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportWholesalerMFGSummary reportData = new ReportWholesalerMFGSummary();
                List<WholesalerMFGSummaryEntity> listReportPreview = new List<WholesalerMFGSummaryEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetWholesaler_MFG_Summary(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerMFGSummary)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Wholesaler_Return_To_Stock_Details
        /// </summary>
        /// <param name="NDCNumber"></param>
        /// <param name="ReportDataTimeZone"></param>
        /// <param name="ReportScheduleTimeZone"></param>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_WholesalerReturnToStock)]
        public ActionResult Wholesaler_Return_To_Stock_Details(string NDCNumber, string ReportDataTimeZone, string ReportScheduleTimeZone, string FileType, bool Isdownload = false)
        {
            if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
            {
                long NDC = Convert.ToInt64(NDCNumber);
                TypeReportSource Source = new TypeReportSource();
                Source.TypeName = typeof(Wholesaler_Return_To_Stock_Details).AssemblyQualifiedName;
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, NDC));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, ReportDataTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, ReportScheduleTimeZone));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, Utility.Constants.Report_Parameter_DataStartDate));
                Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, Utility.Constants.Report_Parameter_DataEndDate));
                if (Isdownload)
                {
                    DownloadReportOnBrowser(Source, FileType);
                    return null;
                }
                else
                {
                    ViewBag.ReportSource = Source;
                }
                return View(Utility.Constants.Report_ReportRender);
            }
            else
            {
                ReportWholesalerReturnToStockDetails reportData = new ReportWholesalerReturnToStockDetails();
                List<WholesalerReturnToStockDetailsEntity> listReportPreview = new List<WholesalerReturnToStockDetailsEntity>();
                using (ReportDataContext reportDataContext = new ReportDataContext())
                {
                    listReportPreview = reportDataContext.GetWholesaler_Return_Stock_Details(Convert.ToInt64(NDCNumber), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataStartDate), Convert.ToDateTime(Utility.Constants.Report_Parameter_DataEndDate), ReportDataTimeZone);
                }
                List<GridColumns> gcList = null;
                using (System.IO.StreamReader r = new System.IO.StreamReader(Server.MapPath(Utility.Constants.Report_Json_File_Path_WholesalerReturnToStockDetails)))
                {
                    string json = r.ReadToEnd();
                    gcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridColumns>>(json);
                }
                reportData.reportRetailerBoxDetails = listReportPreview;
                reportData.gridColumns = gcList;
                return Json(reportData, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// CreateCommonData: Binding the dropdowns
        /// </summary>
        [NonAction]
        public void CreateCommonData()
        {
            ViewBag.FileTypeList = GetFileTypeList();
            ViewBag.HourList = GetHourList();
            ViewBag.EveryList = GetEveryList();
            ViewBag.ReportTimeZoneList = GetReportTimeZoneList();
            ViewBag.ScheduleTypeList = GetScheduleTypeList();
            ViewBag.MinuteList = GetMinuteList();
            ViewBag.MeridieumList = GetMeridieumList();
        }

        /// <summary>
        /// EditReportScheduler: Edit record from Report Scheduler Grid
        /// </summary>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditReportScheduler(int ReportScheduleId)
        {
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            ReportScheduleDetailViewModel retailerBoxDetailReportViewModel = new ReportScheduleDetailViewModel();
            retailerBoxDetailReportViewModel = reportDataAccess.GetReportScheduleInformation(ReportScheduleId);
            CreateCommonData();
            ViewBag.ReportScheduleId = ReportScheduleId;
            return PartialView(Utility.Constants.Report_ReportScheduler, retailerBoxDetailReportViewModel);
        }
        /// <summary>
        /// BindReportSchedulerGrid: Bind Report Scheduler Grid
        /// </summary>
        /// <param name="ReportId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult BindReportSchedulerGrid(int ReportId)
        {
            List<ReportScheduleInformation> listReportSummary = new List<ReportScheduleInformation>();
            if (ReportId != 0)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                ReportDataAccess reportDataAccess = new ReportDataAccess();
                listReportSummary = reportDataAccess.GetReportSchedulerDetails(UserId, ReportId);
                return Json(listReportSummary, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(listReportSummary, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// BindReportRunGrid: Bind report status grid
        /// </summary>
        /// <param name="ReportScheduleID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult BindReportRunGrid(long ReportScheduleID)
        {
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            List<ReportSummary> listReportSummary = new List<ReportSummary>();
            listReportSummary = reportDataAccess.GetReportRunDetails(UserId, ReportScheduleID);
            return Json(listReportSummary, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// GetFileTypeList: Bind dropdown for file type from ReportFormData.xml
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<SelectListItem> GetFileTypeList()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
            System.Xml.XmlNodeList fileTypeList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_FileType);
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (System.Xml.XmlNode fileType in fileTypeList)
            {
                SelectListItem obj = new SelectListItem()
                {
                    Value = fileType.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
                    Text = fileType.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
                };
                lstSelectListItem.Add(obj);
            }
            return lstSelectListItem;
        }
        /// <summary>
        /// GetEveryList: Bind "Every" spinnner control on form
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private SelectList GetEveryList()
        {
            Dictionary<string, string> dicList = new Dictionary<string, string>();
            for (int count = 1; count < 100; count++)
            {
                dicList.Add(count.ToString(), count.ToString());
            }
            SelectList everyList = new SelectList((IEnumerable)dicList, Utility.Constants.Report_EveryList_Key, Utility.Constants.Report_EveryList_Value);
            return everyList;
        }
        /// <summary>
        /// GetHourList: Bind dropdown for Hour from ReportFormData.xml
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<SelectListItem> GetHourList()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
            System.Xml.XmlNodeList hourList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Hour);
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (System.Xml.XmlNode hour in hourList)
            {
                SelectListItem obj = new SelectListItem()
                {
                    Value = hour.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
                    Text = hour.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
                };
                lstSelectListItem.Add(obj);
            }
            return lstSelectListItem;
        }
        /// <summary>
        /// GetMinuteList: Bind dropdown for Minute from ReportFormData.xml
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<SelectListItem> GetMinuteList()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
            System.Xml.XmlNodeList minuteList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Minute);
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (System.Xml.XmlNode minute in minuteList)
            {
                SelectListItem obj = new SelectListItem()
                {
                    Value = minute.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
                    Text = minute.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
                };
                lstSelectListItem.Add(obj);
            }
            return lstSelectListItem;
        }
        /// <summary>
        /// GetScheduleTypeList: Bind dropdown for Schedule type from ReportFormData.xml
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<SelectListItem> GetScheduleTypeList()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
            System.Xml.XmlNodeList scheduleType = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_ScheduleType);
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (System.Xml.XmlNode schedule in scheduleType)
            {
                SelectListItem obj = new SelectListItem()
                {
                    Value = schedule.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
                    Text = schedule.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
                };
                lstSelectListItem.Add(obj);
            }
            return lstSelectListItem;
        }
        /// <summary>
        /// GetMeridieumList: Bind dropdown for Meridieum from ReportFormData.xml
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<SelectListItem> GetMeridieumList()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(System.Web.HttpContext.Current.Server.MapPath(Utility.Constants.Report_ReportFormDataXmlPath));
            System.Xml.XmlNodeList meridieumList = xmldoc.GetElementsByTagName(Utility.Constants.Report_ReportFormDataXmlNode_Meridieum);
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (System.Xml.XmlNode meridieum in meridieumList)
            {
                SelectListItem obj = new SelectListItem()
                {
                    Value = meridieum.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_ID].Value,
                    Text = meridieum.Attributes[Utility.Constants.Report_ReportFormDataXmlNode_Name].Value,
                };
                lstSelectListItem.Add(obj);
            }
            return lstSelectListItem;
        }
        /// <summary>
        /// GetReportTimeZoneList: Set time zones on the UI.
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private SelectList GetReportTimeZoneList()
        {
            SelectList timeZoneList = new SelectList(SchedulerFunctions.GetAllTimeZones(), Utility.Constants.Report_ReportTimeZoneList_Key, Utility.Constants.Report_ReportTimeZoneList_Key);
            return timeZoneList;
        }
        /// <summary>
        /// SaveReportDetails: Save scheduled and criteria information
        /// </summary>
        /// <param name="ReportScheduleDetailViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveReportDetails(ReportScheduleDetailViewModel reportScheduleDetailViewModel)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.InsertUpdateScheduledData(reportScheduleDetailViewModel, UserId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Sample_Report_Retailer_Product_Detail_Criteria: Action method for update Product Detail criteria.
        /// </summary>
        /// <param name="retailerProductDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Sample_Report_Retailer_Product_Detail_Criteria(RetailerProductDetailsEntity retailerProductDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdateProductDetailsCriteria(retailerProductDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Waste_Report_Criteria: Action method for update Waste Report Criteria.
        /// </summary>
        /// <param name="wasteReportDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Utility.Constants.Report_ActionName_Waste_Report_Criteria)]
        public ActionResult Waste_Report_Criteria(WasteReportEntity wasteReportDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdateWasteReportCriteria(wasteReportDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Pharmacy_Summary_Criteria: Action method for update Pharmacy Summary Criteria
        /// </summary>
        /// <param name="pharmacySummaryDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Utility.Constants.Report_ActionName_Pharmacy_Summary_Criteria)]
        public ActionResult Pharmacy_Summary_Criteria(PharmacySummaryEntity pharmacySummaryDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdatePharmacySummaryCriteria(pharmacySummaryDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Non_Creditable_Summary_Criteria: Action method for update Non Creditable Summary Criteria
        /// </summary>
        /// <param name="nonCreditableSummaryDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Utility.Constants.Report_ActionName_Non_Creditable_Summary_Criteria)]
        public ActionResult Non_Creditable_Summary_Criteria(NonCreditableSummaryEntity nonCreditableSummaryDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdateNonCreditableSummaryCriteria(nonCreditableSummaryDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Manufacturer_Summary_Criteria: Action method for update Manufacturer Summary Criteria
        /// </summary>
        /// <param name="manufacturerSummaryDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Utility.Constants.Report_ActionName_Manufacturer_Summary_Criteria)]
        public ActionResult Manufacturer_Summary_Criteria(ManufacturerSummaryEntity manufacturerSummaryDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdateManufacturerSummaryCriteria(manufacturerSummaryDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Return_To_Stock_Criteria: Action method for update Return To Stock Criteria
        /// </summary>
        /// <param name="returnToStockDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Utility.Constants.Report_ActionName_Return_To_Stock_Criteria)]
        public ActionResult Return_To_Stock_Criteria(ReturnToStockReportEntity returnToStockDetail, string ReportScheduleId)
        {
            Response objclsResponse = null;
            ReportDataAccess reportDataAccess = new ReportDataAccess();
            long reportScheduleId = Convert.ToInt64(ReportScheduleId);
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            objclsResponse = reportDataAccess.UpdateReturnToStockCriteria(returnToStockDetail, reportScheduleId);
            return Json(objclsResponse, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Sample_Report_Retailer_Product_Detail: Action method to generate Product Details report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Sample_Report_Retailer_Product_Detail(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                ProductDetailsEmailEntity objRetailerProductDetailsEntity = JsonConvert.DeserializeObject<ProductDetailsEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(Sample_Report_Retailer_Product_Detail).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, objRetailerProductDetailsEntity.NDCNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objRetailerProductDetailsEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objRetailerProductDetailsEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objRetailerProductDetailsEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objRetailerProductDetailsEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_StoreName, objRetailerProductDetailsEntity.StoreName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_VendorName, objRetailerProductDetailsEntity.VendorName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ProductDescription, objRetailerProductDetailsEntity.ProductDescription));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_LotNumber, objRetailerProductDetailsEntity.LotNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ExpDate, objRetailerProductDetailsEntity.ExpDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_FullQty, objRetailerProductDetailsEntity.FullQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PartialQty, objRetailerProductDetailsEntity.PartialQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceBefore, objRetailerProductDetailsEntity.UnitPriceBefore));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountBeforeMFGDiscount, objRetailerProductDetailsEntity.CreditableAmountBeforeMFGDiscount));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_NonCreditableAmount, objRetailerProductDetailsEntity.Creditable));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_OutofpolicyCodeDescription, objRetailerProductDetailsEntity.OutofpolicyDescription));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_Strength, objRetailerProductDetailsEntity.Strength));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ItemGuid, objRetailerProductDetailsEntity.ItemGuid));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_QoskProcessDate, objRetailerProductDetailsEntity.QoskProcessDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_ControlNumber, objRetailerProductDetailsEntity.ControlNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PackageSize, objRetailerProductDetailsEntity.PackageSize));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_UnitPriceAfter, objRetailerProductDetailsEntity.UnitPriceAfter));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_CreditableAmountAfter, objRetailerProductDetailsEntity.CreditableAmountAfter));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_RecalledProduct, objRetailerProductDetailsEntity.RecalledProduct));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_DiscontinuedProduct, objRetailerProductDetailsEntity.DiscontinuedProduct));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_RXorOTC, objRetailerProductDetailsEntity.RXorOTC));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_DosageForm, objRetailerProductDetailsEntity.DosageForm));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PackageForm, objRetailerProductDetailsEntity.PackageForm));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_PartialPercentage, objRetailerProductDetailsEntity.PartialPercentage));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_ProductDetail_Parameter_StoreNumber, objRetailerProductDetailsEntity.StoreNumber));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {

                    ReportRetailerProductDetails reportData = new ReportRetailerProductDetails();
                    List<ProductDetailsEmailEntity> listReportPreview = new List<ProductDetailsEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.GetRetailer_Product_Details(objRetailerProductDetailsEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ProductDetailListPanel, UserId);
                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);

                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        ///  WasteReport: Action method to generate Waste Report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_Waste_Report)]
        public ActionResult WasteReport(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                WasteReportEmailEntity objWasteReportEntity = JsonConvert.DeserializeObject<WasteReportEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(WasteReport).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NDCNumber, objWasteReportEntity.NDCNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objWasteReportEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objWasteReportEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objWasteReportEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objWasteReportEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_Description, objWasteReportEntity.ProductDescription));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_Strength, objWasteReportEntity.Strength));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_FullQuantity, objWasteReportEntity.FullQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_LotNumber, objWasteReportEntity.LotNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_SealedOpenCase, objWasteReportEntity.SealedOpenCase));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ItemGuid, objWasteReportEntity.ItemGuid));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_WasteCode, objWasteReportEntity.WasteCode));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_WasteStreamProfile, objWasteReportEntity.WasteStreamProfile));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PartialQuantity, objWasteReportEntity.PartialQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_StoreNumber, objWasteReportEntity.StoreNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_StoreName, objWasteReportEntity.StoreName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ExpDate, objWasteReportEntity.ExpDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PackageSize, objWasteReportEntity.PackageSize));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_ControlNumber, objWasteReportEntity.ControlNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_DosageForm, objWasteReportEntity.DosageForm));
                    Source.Parameters.Add(new Parameter(Utility.Constants.WasteReport_Parameter_PackageForm, objWasteReportEntity.PackageForm));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {
                    ReportRetailerWasteDetail reportData = new ReportRetailerWasteDetail();
                    List<WasteReportEmailEntity> listReportPreview = new List<WasteReportEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.Get_Waste_Report_Details(objWasteReportEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_WasteReportListPanel, UserId);
                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);
                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        /// PharmacySummary: Action method to generate Pharmacy Summary Report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_PharmacySummary)]
        public ActionResult PharmacySummary(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                PharmacyReportEmailEntity objPharmacyReportEmailEntity = JsonConvert.DeserializeObject<PharmacyReportEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(PharmacySummary).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_StoreNumber, objPharmacyReportEmailEntity.StoreNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objPharmacyReportEmailEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objPharmacyReportEmailEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objPharmacyReportEmailEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objPharmacyReportEmailEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_StoreName, objPharmacyReportEmailEntity.StoreName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_WholesalerCustomerNumber, objPharmacyReportEmailEntity.WholesalerCustomerNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RegionName, objPharmacyReportEmailEntity.RegionName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableValue, objPharmacyReportEmailEntity.ReturnCreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnCreditableQty, objPharmacyReportEmailEntity.ReturnCreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableValue, objPharmacyReportEmailEntity.RecallCreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_RecallCreditableQty, objPharmacyReportEmailEntity.RecallCreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableValue, objPharmacyReportEmailEntity.NonCreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_NonCreditableQty, objPharmacyReportEmailEntity.NonCreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalProductValue, objPharmacyReportEmailEntity.TotalProductValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalQty, objPharmacyReportEmailEntity.TotalQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ProcessingFeeBilled, objPharmacyReportEmailEntity.ProcessingFeeBilled));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalInvoiceAmount, objPharmacyReportEmailEntity.TotalInvoiceAmount));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_TotalDebitMemoQty, objPharmacyReportEmailEntity.TotalDebitMemoQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockValue, objPharmacyReportEmailEntity.ReturnToStockValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_PharmacySummary_ReturnToStockQty, objPharmacyReportEmailEntity.ReturnToStockQty));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {
                    ReportPharmacySummaryEntity reportData = new ReportPharmacySummaryEntity();
                    List<PharmacyReportEmailEntity> listReportPreview = new List<PharmacyReportEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.Get_Pharmacy_Summary_Details(objPharmacyReportEmailEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_PharmacySummaryListPanel, UserId);

                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);
                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        /// NonCreditableSummary: Action method to generate Non Creditable Summary Report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_NonCreditableSummary)]
        public ActionResult NonCreditableSummary(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                NonCreditableSummaryEmailEntity objNonCreditableSummaryEmailEntity = JsonConvert.DeserializeObject<NonCreditableSummaryEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(NonCreditableSummary).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreNumber, objNonCreditableSummaryEmailEntity.StoreNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objNonCreditableSummaryEmailEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objNonCreditableSummaryEmailEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objNonCreditableSummaryEmailEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objNonCreditableSummaryEmailEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_StoreName, objNonCreditableSummaryEmailEntity.StoreName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_RegionName, objNonCreditableSummaryEmailEntity.RegionName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableValue, objNonCreditableSummaryEmailEntity.NonCreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableQty, objNonCreditableSummaryEmailEntity.NonCreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_ManufacturerName, objNonCreditableSummaryEmailEntity.ManufacturerName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonCode, objNonCreditableSummaryEmailEntity.NonCreditableReasonCode));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_NonCreditableSummary_NonCreditableReasonDescription, objNonCreditableSummaryEmailEntity.NonCreditableReasonDescription));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {
                    ReportNonCreditableSummaryEntity reportData = new ReportNonCreditableSummaryEntity();
                    List<NonCreditableSummaryEmailEntity> listReportPreview = new List<NonCreditableSummaryEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.Get_Non_Creditable_Summary_Details(objNonCreditableSummaryEmailEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_NonCreditableSummaryListPanel, UserId);

                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);
                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        /// ManufacturerSummary: Action method to generate Manufacturer Summary Report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_ManufacturerSummary)]
        public ActionResult ManufacturerSummary(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                ManufacturerSummaryEmailEntity objManufacturerSummaryEmailEntity = JsonConvert.DeserializeObject<ManufacturerSummaryEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(ManufacturerSummary).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_MFGLabeler, objManufacturerSummaryEmailEntity.MFGLabeler));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objManufacturerSummaryEmailEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objManufacturerSummaryEmailEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objManufacturerSummaryEmailEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objManufacturerSummaryEmailEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_ManufacturerName, objManufacturerSummaryEmailEntity.ManufacturerName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RetailerVendorNumber, objManufacturerSummaryEmailEntity.RetailerVendorNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_DebitMemoInvoiceNo, objManufacturerSummaryEmailEntity.DebitMemoInvoiceNo));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableValue, objManufacturerSummaryEmailEntity.CreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_CreditableQty, objManufacturerSummaryEmailEntity.CreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableValue, objManufacturerSummaryEmailEntity.RecallCreditableValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_RecallCreditableQty, objManufacturerSummaryEmailEntity.RecallCreditableQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalProductValue, objManufacturerSummaryEmailEntity.TotalProductValue));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalQty, objManufacturerSummaryEmailEntity.TotalQty));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_ProcessingFee, objManufacturerSummaryEmailEntity.ProcessingFee));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ManufacturerSummary_TotalInvoiceAmount, objManufacturerSummaryEmailEntity.TotalInvoiceAmount));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {
                    ReportManufacturerSummaryEntity reportData = new ReportManufacturerSummaryEntity();
                    List<ManufacturerSummaryEmailEntity> listReportPreview = new List<ManufacturerSummaryEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.Get_Manufacturer_Summary_Details(objManufacturerSummaryEmailEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ManufacturerSummaryListPanel, UserId);

                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);
                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        /// ReturnToStock: Action method to generate Return To Stock Report in PDF,XLS and CSV format.
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="Isdownload"></param>
        /// <param name="reportCriteriaDetail"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Utility.Constants.Report_ActionName_ReturnToStock)]
        public ActionResult ReturnToStock(string FileType, bool Isdownload, string reportCriteriaDetail)
        {
            if (!string.IsNullOrEmpty(reportCriteriaDetail) || Isdownload)
            {
                long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                ReturnToStockEmailEntity objReturnToStockEmailEntity = JsonConvert.DeserializeObject<ReturnToStockEmailEntity>(reportCriteriaDetail);
                if (FileType == Utility.Constants.Report_FileType_PDF || Isdownload)
                {
                    TypeReportSource Source = new TypeReportSource();
                    Source.TypeName = typeof(ReturnToStock).AssemblyQualifiedName;
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_UserID, UserId));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_VendorName, objReturnToStockEmailEntity.VendorName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportDataTimeZone, objReturnToStockEmailEntity.ReportDataTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReportScheduleTimeZone, objReturnToStockEmailEntity.ReportScheduleTimeZone));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataStartDate, objReturnToStockEmailEntity.DataStartDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_DataEndDate, objReturnToStockEmailEntity.DataEndDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_FileType, FileType));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_StoreName, objReturnToStockEmailEntity.StoreName));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_StoreNumber, objReturnToStockEmailEntity.StoreNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_NDCNumber, objReturnToStockEmailEntity.NDCNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ProductDescription, objReturnToStockEmailEntity.ProductDescription));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_Strength, objReturnToStockEmailEntity.Strength));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ControlNumber, objReturnToStockEmailEntity.ControlNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_RXorOTC, objReturnToStockEmailEntity.RXorOTC));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_DosageForm, objReturnToStockEmailEntity.DosageForm));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_PackageForm, objReturnToStockEmailEntity.PackageForm));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_LotNumber, objReturnToStockEmailEntity.LotNumber));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_ExpDate, objReturnToStockEmailEntity.ExpDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_QoskProcessDate, objReturnToStockEmailEntity.QoskProcessDate));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_DateEligibleForCredit, objReturnToStockEmailEntity.DateEligibleForCredit));
                    Source.Parameters.Add(new Parameter(Utility.Constants.Report_Parameter_ReturnToStock_OutofpolicyDescription, objReturnToStockEmailEntity.OutofpolicyDescription));
                    if (Isdownload)
                    {
                        DownloadReportOnBrowser(Source, FileType);
                        return null;
                    }
                    else
                    {
                        ViewBag.ReportSource = Source;
                    }
                    return View(Utility.Constants.Report_ReportRender);
                }
                else
                {
                    ReportReturnToStockReportEntity reportData = new ReportReturnToStockReportEntity();
                    List<ReturnToStockEmailEntity> listReportPreview = new List<ReturnToStockEmailEntity>();
                    using (ReportDataContext reportDataContext = new ReportDataContext())
                    {
                        listReportPreview = reportDataContext.Get_Return_To_Stock_Details(objReturnToStockEmailEntity, FileType);
                    }
                    CommonDataAccess commonDataAccess = new CommonDataAccess();
                    List<GridColumns> gcList = commonDataAccess.GetUserSpecificColumnsForGrid(Utility.Constants.Report_ReturnToStockListPanel, UserId);

                    reportData.reportRetailerBoxDetails = listReportPreview;
                    reportData.gridColumns = gcList;
                    return Json(reportData, JsonRequestBehavior.AllowGet);
                }
            }
            return View(Utility.Constants.Report_ReportPreview);

        }
        /// <summary>
        /// Delete: Delete record from Report Scheduler Grid
        /// </summary>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Delete(int ReportScheduleId)
        {
            ReportDataAccess reportDataAccess = null;
            Response objclsResponse = null;
            try
            {
                reportDataAccess = new ReportDataAccess();
                string modifiedBy = User.Identity.Name;
                objclsResponse = reportDataAccess.DeleteReportSchedule(ReportScheduleId);
                switch (objclsResponse.Status)
                {
                    case Status.OK:
                        return Json(new { Result = QoskCloudWebResource.DeletedSuccessfully, Status = true }, JsonRequestBehavior.AllowGet);

                    case Status.Deleted:
                        return Json(new { Result = QoskCloudWebResource.Deleted, Status = false }, JsonRequestBehavior.AllowGet);

                    case Status.Fail:
                        return Json(new { Result = QoskCloudWebResource.DeletedFails, Status = false }, JsonRequestBehavior.AllowGet);
                }
            }

            finally
            {
                reportDataAccess = null;
                objclsResponse = null;
            }
            return Json(QoskCloudWebResource.DeletedFails);

        }
        /// <summary>
        /// SendEmail: Function for Send Email 
        /// </summary>
        /// <param name="EmailBody"></param>
        /// <param name="emailList"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendEmail(string EmailBody, string emailList, string Details)
        {
            bool isEmailSend = false;
            Dictionary<string, object> emailDetailList = null;
            try
            {
                if (Details != null)
                {
                    emailDetailList = JsonConvert.DeserializeObject<Dictionary<string, object>>(Details);
                    List<SasUriModel> _EmailBody = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType<List<SasUriModel>>(EmailBody, new List<SasUriModel>());
                    if (!string.IsNullOrEmpty(emailList))
                    {
                        string[] emails = emailList.Split(',');
                        List<string> _emailList = new List<string>(emails);
                        CommonDataAccess commonDataAccess = new CommonDataAccess();
                        isEmailSend = commonDataAccess.SendEmail(_EmailBody, _emailList, emailDetailList);
                    }
                    if (isEmailSend)
                    {
                        return Json(Utility.Constants.SendEmail_SuccessfullySend, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet);
                }
            }
            catch { return Json(Utility.Constants.SendEmail_Error, JsonRequestBehavior.AllowGet); }

        }
        /// <summary>
        /// DisplayImageAndVideo: Function for Display Image And Video in Popup
        /// </summary>
        /// <param name="ItemGuid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DisplayImageAndVideo(string ItemGuid)
        {
            try
            {
                //if QoskID and ItemIDForBarcode is null then return no image
                if (string.IsNullOrEmpty(ItemGuid))
                    return Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet);

                //Get images/Videos from azure blob.
                List<SasUriModel> imageList = AzureCommonUtility.DownloadImagesVideosFromAzure(ItemGuid);
                if (imageList != null && imageList.Count > 0)
                    return Json(imageList, JsonRequestBehavior.AllowGet);
                else return Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet);
            }
            catch { return Json(new[] { new SasUriModel() { ContentType = Utility.Constants.DisplayImageAndVideo_NoImage } }, JsonRequestBehavior.AllowGet); }

        }
        /// <summary>
        /// GetAutoCompleteDetails: Get Auto Complete Details for criteria
        /// </summary>
        /// <param name="terms"></param>
        /// <param name="controlID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAutoCompleteDetails(string terms, string controlID)
        {
            controlID = controlID.Replace(Utility.Constants.GetAutoCompleteDetails_textbox, string.Empty);
            ReportDataAccess objReportDataAccess = new ReportDataAccess();

            var profilelist = objReportDataAccess.GetDetailsUsingAutoCompleteBasedOnControlID(terms, controlID);
            if (profilelist != null)
            {
                var objProfile = profilelist
                                  .Select(n => new SelectListItem
                                  {
                                      Text = n.Text,
                                      Value = Convert.ToString(n.Value)
                                  });
                return Json(objProfile, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// BindColumnListing: method used to get column list save by specific user and show into custom grid popup.
        /// </summary>
        /// <param name="ReportName"></param>
        /// <returns></returns>
        [QoskAuthorizeAttribute]
        [HttpGet]
        public PartialViewResult BindColumnListing(string ReportName)
        {
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            ManageLayoutConfig objManageLayoutConfig = new ManageLayoutConfig();
            ColumnListEntity objListing = null;
            if (ReportName == Utility.Constants.Report_ActionName_RetailerProductDetail)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ProductDetailListPanel, UserId, true);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_RetailerWasteReport)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_WasteReportListPanel, UserId, true);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_PharmacySummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_PharmacySummaryListPanel, UserId, true);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_NonCreditableSummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_NonCreditableSummaryListPanel, UserId, true);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_ManufacturerSummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ManufacturerSummaryListPanel, UserId, true);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_ReturnToStock)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ReturnToStockListPanel, UserId, true);
            }
            return PartialView("_ReportColumnGridListPanel", objListing);
        }
        /// <summary>
        /// CreateSearchPanel: methods used to create criteria search panel dynamically based on report name.
        /// </summary>
        /// <param name="ReportName"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult CreateSearchPanel(string ReportName, int ReportScheduleId)
        {
            ManageLayoutConfig objManageLayoutConfig = new ManageLayoutConfig();
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            ColumnListEntity objListing = null;
            ReportDataAccess objReportDataAccess = new ReportDataAccess();
            string ReportCriteria = objReportDataAccess.GetReportCriteria(ReportScheduleId);
            Dictionary<string, object> parameterList = null;
            if (!string.IsNullOrEmpty(ReportCriteria))
            {
                parameterList = JsonConvert.DeserializeObject<Dictionary<string, object>>(ReportCriteria);
            }
            if (ReportName == Utility.Constants.Report_ActionName_RetailerProductDetail)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ProductDetailSearchPanel, UserId, false);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_RetailerWasteReport)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_WasteReportSearchPanel, UserId, false);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_PharmacySummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_PharmacySummarySearchPanel, UserId, false);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_NonCreditableSummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_NonCreditableSummarySearchPanel, UserId, false);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_ManufacturerSummary)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ManufacturerSummarySearchPanel, UserId, false);
            }
            else if (ReportName == Utility.Constants.Report_ActionName_ReturnToStock)
            {
                objListing = objManageLayoutConfig.GetColumnListingByLayout(Utility.Constants.Report_ReturnToStockSearchPanel, UserId, false);
            }
            ViewData["ControlListingForPopUp"] = objListing;
            List<ControlViewModel> objControlVMCollection = new List<ControlViewModel>();
            DynamicControlEntity ControlListing = new DynamicControlEntity();
            CommonDataAccess commonDataAccess = new CommonDataAccess();
            for (int i = 0; i < objListing.SubscribedColumn.Count; i++)
            {
                var controlName = objListing.SubscribedColumn[i].ColumnName;
                object controlValue = null;
                object AutoComplete = null;
                if (parameterList != null && controlName != Utility.Constants.CreateSearchPanel_DefaulColumnFlag)
                {
                    ReportDataContext objReportDataContext = new ReportDataContext();
                    if (parameterList.ContainsKey(controlName))
                    {
                        controlValue = parameterList.Single(x => x.Key == controlName).Value;
                    }
                }
                AutoComplete = objListing.SubscribedColumn[i].IsAutoComplete;
                objControlVMCollection.Add(commonDataAccess.CreateDynamicControlBasedOnValue(objListing.SubscribedColumn[i], controlValue, AutoComplete));
            }
            ControlListing.Controls = objControlVMCollection;

            if (ControlListing.Controls.Count > 0)
            {
                return PartialView("_ReportSearchCriteriaDisplay", ControlListing);
            }
            else
                //return PartialView("_SearchPanel");
                return null;
        }
        /// <summary>
        /// CreateColumnListingPopUp: method used to get column list save by specific user and show into custom search popup
        /// </summary>
        /// <param name="ReportName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateColumnListingPopUp(string ReportName)
        {
            ColumnListEntity objListing = null;
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());

            switch (ReportName)
            {
                case Utility.Constants.Report_ActionName_RetailerProductDetail:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ProductDetailSearchPanel);
                    }
                case Utility.Constants.Report_ActionName_RetailerWasteReport:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_WasteReportSearchPanel);
                    }
                case Utility.Constants.Report_ActionName_PharmacySummary:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_PharmacySummarySearchPanel);
                    }
                case Utility.Constants.Report_ActionName_NonCreditableSummary:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_NonCreditableSummarySearchPanel);
                    }
                case Utility.Constants.Report_ActionName_ManufacturerSummary:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ManufacturerSummarySearchPanel);
                    }
                case Utility.Constants.Report_ActionName_ReturnToStock:
                    {
                        return GetPartialViewForReportEntity(objListing, Utility.Constants.Report_ReturnToStockSearchPanel);
                    }
            }
            return null;
        }
        /// <summary>
        /// CommonFunctionForListingPopUp: Common Function For ListingPopUp.
        /// </summary>
        /// <param name="layoutName"></param>
        /// <returns></returns>
        [NonAction]
        public ColumnListEntity CommonFunctionForListingPopUp(string layoutName)
        {
            ColumnListEntity objListing = null;
            long UserId = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
            if (ViewData["ControlListingForPopUp"] == null)
            {
                ManageLayoutConfig objManageLayoutConfig = new ManageLayoutConfig();
                objListing = objManageLayoutConfig.GetColumnListingByLayout(layoutName, UserId, false);
            }
            return objListing;
        }
        /// <summary>
        /// GetPartialViewForReportEntity: Get Partial View For Report Entity.
        /// </summary>
        /// <param name="objListing"></param>
        /// <param name="layoutName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPartialViewForReportEntity(ColumnListEntity objListing, string layoutName)
        {
            objListing = CommonFunctionForListingPopUp(layoutName);
            if (objListing != null)
            {
                return PartialView("_ReportColumnSearchListPanel", objListing);
            }
            else
            {
                return PartialView("_ReportColumnSearchListPanel");
            }
        }

        /// <summary>
        /// SaveCustomSearchDetailsByUserName: method is used to save custom search and custom grid columns detail based on layout id.
        /// </summary>
        /// <param name="SearchList"></param>
        /// <param name="LayoutId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveCustomSearchDetailsByUserName(string SearchList, int LayoutId)
        {
            ColumnListEntity objList = new ColumnListEntity();
            objList.SubscribedColumn = new List<ColumnDetails>();
            ColumnDetails objColumnDetails = null;
            string[] sColumnList = SearchList.Split(',');
            if (sColumnList != null)
            {
                long UserID = CommonDataAccess.GetCurrentUserId(User.Identity.GetUserId());
                foreach (string objColumn in sColumnList)
                {
                    if (!string.IsNullOrEmpty(objColumn))
                    {
                        objColumnDetails = new ColumnDetails();
                        objColumnDetails.ColumnId = Convert.ToInt32(objColumn);
                        objColumnDetails.LayoutId = LayoutId;///For Testing Only
                        objColumnDetails.UserId = UserID;///For Testing Only
                        objList.SubscribedColumn.Add(objColumnDetails);
                    }

                }
                if (objList.SubscribedColumn != null)
                {
                    ManageLayoutConfig objCommon = new ManageLayoutConfig();
                    bool IsSave = objCommon.SaveSubscribedColumn(objList, UserID, LayoutId);
                    if (IsSave)
                        return Json("Custom_Save_Success");
                    else
                        return Json("UnDefined");
                }
                else
                    return Json("UnDefined");
            }
            else
                return Json("UnDefined");
        }
    }
}