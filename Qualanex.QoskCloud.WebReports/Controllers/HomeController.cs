﻿using Qualanex.QoskCloud.WebReports.Common;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.WebReports.Controllers
{
    [QoskAuthorizeAttribute]
    public class HomeController : Controller
    {
        /// <summary>
        /// Index: First action method which execute when navigated from Admin Website.
        /// </summary>
        /// <param name="ReportId"></param>
        /// <returns></returns>
        public ActionResult Index(string ReportId)
        {
            string queryStringValue = Request.Url.Query;
            ViewBag.ReportId = ReportId + queryStringValue;
            return View();
        }

    }
}