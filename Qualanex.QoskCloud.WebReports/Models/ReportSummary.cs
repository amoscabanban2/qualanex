﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// ReportSummary: This entity class is used to bind ReportStatus grid.
    /// </summary>
    public class ReportSummary
    {
        public long ReportId { get; set; }
        public string ReportTitle { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string RunTime { get; set; }
        public string BlobFile { get; set; }

    }
}