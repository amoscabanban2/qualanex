﻿using Qualanex.QoskCloud.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;

namespace Qualanex.QoskCloud.WebReports.Models
{
    public class ScheduleReportDataAccess
    {
        /// <summary>
        /// UpdateScheduleReportStatus: Function is used to update the status of the schedule report in database.
        /// </summary>
        /// <param name="ReportRunGuid"></param>
        /// <param name="status"></param>
        /// <param name="statusDescription"></param>
        /// <param name="BlobFilePath"></param>
        /// <param name="RunTime"></param>
        /// <returns></returns>
        public bool UpdateScheduleReportStatus(System.Guid ReportRunGuid, string status, string statusDescription, string BlobFilePath = null, DateTime? RunTime = null)
        {
            try
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    var objReportrun = (from reportrun in cloudModelEntities.ReportRun
                                        where reportrun.ReportRunGuid == ReportRunGuid
                                        select reportrun).FirstOrDefault();
                    if (objReportrun != null)
                    {
                        if (BlobFilePath != null)
                            objReportrun.BlobFile = BlobFilePath;
                        if (RunTime != null)
                            objReportrun.RunTime = RunTime;
                        objReportrun.Status = status.ToString();
                        objReportrun.StatusDescription = statusDescription;
                        objReportrun.StatusTime = DateTime.UtcNow;
                        cloudModelEntities.SaveChanges();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// QoskCloudConfigurationSetting: Function is used for set configuration setting
        /// </summary>
        /// <param name="Groupname"></param>
        /// <returns></returns>
        public List<AppConfigurationSetting> QoskCloudConfigurationSetting(string Groupname)
        {
            List<AppConfigurationSetting> objICollectionAppconfig = new List<AppConfigurationSetting>();
            try
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    objICollectionAppconfig = (from u in cloudModelEntities.AppConfig
                                               where u.GroupName == Groupname

                                               select new AppConfigurationSetting
                                               {
                                                   Value = u.Value,
                                                   keyName = u.KeyName,
                                                   GroupName = u.GroupName
                                               }).ToList();
                    return objICollectionAppconfig;
                }
            }
            catch
            {

            }
            return objICollectionAppconfig;
        }
    }
}
