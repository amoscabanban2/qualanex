﻿using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using System.Collections.Generic;
using System.Linq;
using System;
using Qualanex.QoskCloud.WebReports.Common;
using System.Web.Mvc;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;

namespace Qualanex.QoskCloud.WebReports.Models
{
    public class CommonDataAccess
    {
        private string NetworkUserEmail { get; set; }
        private string NetworkUserPassword { get; set; }
        private string NetworkSMTP { get; set; }
        private string ReportSubject { get; set; }
        private string MessageBody { get; set; }

        ///****************************************************************************
        /// <summary>
        /// GetCurrentUserId: Get Current User Id.
        /// </summary>
        /// <param name="GuidUserId"></param>
        /// <returns></returns>
        ///****************************************************************************
        public static long GetCurrentUserId(string GuidUserId)
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                long userId = -1;

                var objUser = (from user in cloudModelEntities.User
                               where user.Id.Equals(GuidUserId)
                               select user).FirstOrDefault();


                if (objUser != null)
                    userId = objUser.UserID;

                return userId;
            }
        }
        ///****************************************************************************
        /// <summary>
        ///   QoskCloudConfigurationSetting: used for set configuration setting.
        /// </summary>
        /// <param name="Groupname"></param>
        /// <returns></returns>
        ///****************************************************************************
        public List<AppConfigurationSetting> QoskCloudConfigurationSetting(string Groupname)
        {
            var objICollectionAppconfig = new List<AppConfigurationSetting>();

            try
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    objICollectionAppconfig = (from u in cloudModelEntities.AppConfig
                                               where string.Compare(u.GroupName, (Groupname), true) == 0

                                               select new AppConfigurationSetting
                                               {
                                                   Value = u.Value,
                                                   keyName = u.KeyName,
                                                   GroupName = u.GroupName
                                               }).ToList();
                    return objICollectionAppconfig;
                }
            }
            catch (Exception)
            {
            }

            return objICollectionAppconfig;
        }

        ///****************************************************************************
        /// <summary>
        ///  InitializeMailVariables: Initialize class variables.
        /// </summary>
        /// <returns></returns>
        ///****************************************************************************
        private void InitializeMailVariables()
        {
            try
            {
                var scheduleReportDataAccess = new ScheduleReportDataAccess();
                var lstAzureConfigDetails = scheduleReportDataAccess.QoskCloudConfigurationSetting(Constants.AppConfigFile_GroupKey_Qosk);

                if (lstAzureConfigDetails != null && lstAzureConfigDetails.Count > 0)
                {
                    ReportSubject = Constants.CommonDataAccess_ImageVideosLinks;
                    NetworkUserEmail = lstAzureConfigDetails.Where(x => x.keyName.Equals(Utility.Constants.AppConfigFile_Email_UserName)).FirstOrDefault().Value.ToString();
                    NetworkUserPassword = lstAzureConfigDetails.Where(x => x.keyName.Equals(Utility.Constants.AppConfigFile_Email_Password)).FirstOrDefault().Value.ToString();
                    NetworkSMTP = lstAzureConfigDetails.Where(x => x.keyName.Equals(Utility.Constants.AppConfigFile_Email_Host)).FirstOrDefault().Value.ToString();
                }

            }
            catch
            {
                throw;
            }
        }
        ///****************************************************************************
        /// <summary>
        /// SendEmail: To Send images/videos to users
        /// </summary>
        /// <param name="EmailBody"></param>
        /// <param name="emailList"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        ///****************************************************************************      
        public bool SendEmail(List<SasUriModel> EmailBody, List<string> emailList, Dictionary<string, object> details)
        {
            try
            {
                InitializeMailVariables();

                if (!string.IsNullOrEmpty(NetworkUserEmail)
                    && !string.IsNullOrEmpty(NetworkUserPassword)
                    && !string.IsNullOrEmpty(NetworkSMTP)
                    && emailList != null
                    && emailList.Count > 0)
                {
                    var emailbody = EmailBodyHtml(EmailBody, details);

                    if (string.IsNullOrWhiteSpace(emailbody))
                        return false;

                    return SendUserMail.SendEmail(emailList, "", null, ReportSubject, emailbody, NetworkUserEmail, NetworkUserPassword, NetworkSMTP);
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        ///****************************************************************************
        /// <summary>
        /// EmailBodyHtml: Format Email body in Html.
        /// </summary>
        /// <param name="EmailBody"></param>
        /// <param name="EmailDetails">
        /// <returns></returns>
        ///****************************************************************************
        public string EmailBodyHtml(List<SasUriModel> EmailBody, Dictionary<string, object> EmailDetails)
        {
            var quantity = string.Empty;
            var sealedOpenCase = string.Empty;
            var sbEmailBody = new System.Text.StringBuilder();

            if (EmailBody != null && EmailBody.Count > 0)
            {
                sbEmailBody.AppendLine("Hello,");
                sbEmailBody.AppendLine("");
                sbEmailBody.AppendLine("The following are the images/videos you requested-");
                sbEmailBody.AppendLine("<br>");
                sbEmailBody.AppendLine("<table");

                foreach (var content in EmailBody)
                {
                    sbEmailBody.AppendLine("<tr><td>" + content.ContentName + "</td><td><a href='" + content.ContentSasURI + "'>Click here</a></td></tr>");
                }

                sbEmailBody.AppendLine("<tr><td>" + "<b>Manufacturer/Vendor Name:</b> " + EmailDetails.FirstOrDefault(x => x.Key == "VendorName").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Credit requested by:</b> " + EmailDetails.Single(x => x.Key == "StoreName").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>City:</b> " + EmailDetails.Single(x => x.Key == "City").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>State:</b> " + EmailDetails.Single(x => x.Key == "State").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Zip:</b> " + EmailDetails.Single(x => x.Key == "ZipCode").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>DEA Number:</b> " + EmailDetails.Single(x => x.Key == "DEANumber").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Process Date:</b> " + EmailDetails.Single(x => x.Key == "QoskProcessDate").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Item ID:</b> " + EmailDetails.Single(x => x.Key == "ItemGuid").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Product Description:</b> " + EmailDetails.Single(x => x.Key == "ProductDescription").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>NDC Number:</b> " + EmailDetails.Single(x => x.Key == "NDCNumber").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Lot Number:</b> " + EmailDetails.Single(x => x.Key == "LotNumber").Value + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Expiration Date:</b> " + EmailDetails.Single(x => x.Key == "ExpDate").Value + "</td></tr>");
                sealedOpenCase = EmailDetails.SingleOrDefault(x => x.Key == "SealedOpenCase").Value != null ? EmailDetails.SingleOrDefault(x => x.Key == "SealedOpenCase").Value.ToString() : string.Empty;

                quantity = sealedOpenCase == string.Empty ? string.Empty : sealedOpenCase == "Opened" ? EmailDetails.Single(x => x.Key == "PartialQty").Value.ToString() : EmailDetails.Single(x => x.Key == "FullQty").Value.ToString();

                sbEmailBody.AppendLine("<tr><td>" + "<b>Quantity:</b> " + quantity + "</td></tr>");
                sbEmailBody.AppendLine("<tr><td>" + "<b>Container Type:</b> " + sealedOpenCase + "</td></tr>");
                sbEmailBody.AppendLine("</table>");
            }

            return sbEmailBody.ToString();
        }

        ///****************************************************************************
        /// <summary>
        /// GetAllColumnListing: Get All Columns List.
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        ///****************************************************************************
        public ColumnListEntity GetAllColumnListing(ColumnListRequest objRequest)
        {
            try
            {
                return GetSubscribedColumnListingByLayout(objRequest.LayoutName, objRequest.UserId, objRequest.ForGridColumnListing);
            }
            catch (Exception)
            {
                return null;
            }

        }

        ///****************************************************************************
        /// <summary>
        /// SaveSubscribedColumnListing: Save Subscribed Columns List based on layout id.
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        ///****************************************************************************
        public bool SaveSubscribedColumnListing(ColumnListEntity objRequest, long UserID, int LayoutID)
        {
            try
            {
                return SaveSubscribedColumnListForUser(objRequest, UserID, LayoutID);
            }
            catch (Exception)
            {
                return false;
            }
        }

        ///****************************************************************************
        /// <summary>
        /// GetSubscribedColumnListingByLayout: Get Subscribed Columns List By Layout.
        /// </summary>
        /// <param name="LayoutName"></param>
        /// <param name="UserID"></param>
        /// <param name="ForGridColumnListing"></param>
        /// <returns></returns>
        ///****************************************************************************
        public ColumnListEntity GetSubscribedColumnListingByLayout(string LayoutName, long UserID, bool ForGridColumnListing)
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var objSubscribedColumn = new List<ColumnDetails>();
                var objDefaultColumn = new List<ColumnDetails>();
                var objDefaultSubscribedColumn = new List<ColumnDetails>();

                objSubscribedColumn = (from UCM in cloudModelEntities.UserColumnMapping
                                       join MCL in cloudModelEntities.ReportMasterColumnList on UCM.ColumnId equals MCL.ReportMasterColumnListID
                                       join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                       join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                       join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                       where UCM.LayoutId == LTM.LayoutId && string.Compare(MLL.SectionName, LayoutName, true) == 0
                                             && UCM.UserId == UserID
                                             && MCL.IsDeleted != true
                                             && (!ForGridColumnListing || MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate)
                                       orderby UCM.OrderNumber ascending
                                       select new ColumnDetails
                                       {
                                           ColumnId = MCL.ReportMasterColumnListID,
                                           ColumnName = MCL.ColumnName,
                                           ColumnDescription = MCL.ColumnName,
                                           ColumnType = MCL.DataType,
                                           LayoutId = MLL.ReportMasterLayoutListID,
                                           TableName = MTL.TableName,
                                           FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                           UserMappingId = UCM.UserColumnMappingID,
                                           IsSearchPanelDefault = MCL.IsDefault,
                                           IsGridListingDefault = MCL.IsDefaultGrid,
                                           DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                           OrderNumber = UCM.OrderNumber,
                                           MappedTable = MCL.MappedTable,
                                           SearchOrder = MCL.SearchOrder,
                                           GridsearchOrder = MCL.GridsearchOrder,
                                           IsAutoComplete = MCL.IsAutoComplete

                                       }).ToList();

                objDefaultColumn = (from MCL in cloudModelEntities.ReportMasterColumnList
                                    join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                    join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                    join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                    where string.Compare(MLL.SectionName, LayoutName, true) == 0
                                          && MCL.IsDeleted != true
                                          && (!ForGridColumnListing || MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate)
                                          && MCL.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag
                                    select new ColumnDetails
                                    {
                                        ColumnId = MCL.ReportMasterColumnListID,
                                        ColumnName = MCL.ColumnName,
                                        ColumnDescription = MCL.ColumnName,
                                        ColumnType = MCL.DataType,
                                        LayoutId = MLL.ReportMasterLayoutListID,
                                        TableName = MTL.TableName,
                                        FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                        IsSearchPanelDefault = MCL.IsDefault,
                                        IsGridListingDefault = MCL.IsDefaultGrid,
                                        DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                        MappedTable = MCL.MappedTable,
                                        SearchOrder = MCL.SearchOrder,
                                        GridsearchOrder = MCL.GridsearchOrder,
                                        OrderNumber = 0,
                                        IsAutoComplete = MCL.IsAutoComplete
                                    }).Distinct().ToList();

                if (!objSubscribedColumn.Any())
                {
                    objSubscribedColumn = (from MCL in cloudModelEntities.ReportMasterColumnList
                                           join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                           join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                           join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                           where string.Compare(MLL.SectionName, LayoutName, true) == 0
                                                 && MCL.IsDeleted != true
                                                 && (MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate)
                                                 && MCL.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag
                                           select new ColumnDetails
                                           {
                                               ColumnId = MCL.ReportMasterColumnListID,
                                               ColumnName = MCL.ColumnName,
                                               ColumnDescription = MCL.ColumnName,
                                               ColumnType = MCL.DataType,
                                               LayoutId = MLL.ReportMasterLayoutListID,
                                               TableName = MTL.TableName,
                                               FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                               IsSearchPanelDefault = MCL.IsDefault,
                                               IsGridListingDefault = MCL.IsDefaultGrid,
                                               DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                               MappedTable = MCL.MappedTable,
                                               SearchOrder = MCL.SearchOrder,
                                               GridsearchOrder = MCL.GridsearchOrder,
                                               IsAutoComplete = MCL.IsAutoComplete
                                           }).ToList();
                }

                objDefaultColumn = ForGridColumnListing ? objDefaultColumn.Where(m => m.IsGridListingDefault == true).ToList() : objDefaultColumn.Where(m => m.IsSearchPanelDefault == true).ToList();

                for (var iCount = 0; iCount < objDefaultColumn.Count; iCount++)
                {
                    if (objSubscribedColumn.Any(m => m.ColumnId.Equals(objDefaultColumn[iCount].ColumnId)))
                    {
                        objDefaultColumn.RemoveAt(iCount);
                        iCount--;
                    }
                }

                objDefaultSubscribedColumn = (from MCL in cloudModelEntities.ReportMasterColumnList
                                              join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                              join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                              join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                              where string.Compare(MLL.SectionName, LayoutName, true) == 0
                                                    && MCL.IsDeleted != true
                                                    && (!ForGridColumnListing || MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate)
                                                    && MCL.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag
                                              select new ColumnDetails
                                              {
                                                  ColumnId = MCL.ReportMasterColumnListID,
                                                  ColumnName = MCL.ColumnName,
                                                  ColumnDescription = MCL.ColumnName,
                                                  ColumnType = MCL.DataType,
                                                  LayoutId = MLL.ReportMasterLayoutListID,
                                                  TableName = MTL.TableName,
                                                  FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                                  IsSearchPanelDefault = MCL.IsDefault,
                                                  IsGridListingDefault = MCL.IsDefaultGrid,
                                                  DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                                  MappedTable = MCL.MappedTable,
                                                  SearchOrder = MCL.SearchOrder,
                                                  GridsearchOrder = MCL.GridsearchOrder,
                                                  OrderNumber = 0,
                                              }).Distinct().ToList();

                objDefaultSubscribedColumn = ForGridColumnListing ? objDefaultSubscribedColumn.Where(m => m.IsGridListingDefault != null).ToList() : objDefaultSubscribedColumn.Where(m => m.IsSearchPanelDefault != null).ToList();

                objSubscribedColumn.AddRange(objDefaultColumn);

                for (var iCount = 0; iCount < objDefaultSubscribedColumn.Count; iCount++)
                {
                    if (objSubscribedColumn.Any(m => m.ColumnId.Equals(objDefaultSubscribedColumn[iCount].ColumnId)))
                    {
                        objDefaultSubscribedColumn.RemoveAt(iCount);
                        iCount--;
                    }
                }

                if (ForGridColumnListing)
                    return new ColumnListEntity
                    {
                        SubscribedColumn = objSubscribedColumn.OrderBy(p => p.GridsearchOrder).ThenBy(p => p.OrderNumber).ToList(),
                        DefaultColumn = objDefaultSubscribedColumn.OrderBy(p => p.GridsearchOrder).ThenBy(p => p.OrderNumber).ToList()
                    };

                return new ColumnListEntity
                {
                    SubscribedColumn = objSubscribedColumn.OrderBy(p => p.SearchOrder).ThenBy(p => p.OrderNumber).ToList(),
                    DefaultColumn = objDefaultSubscribedColumn.OrderBy(p => p.SearchOrder).ThenBy(p => p.OrderNumber).ToList(),
                };
            }
        }
        ///****************************************************************************
        /// <summary>
        ///  SaveSubscribedColumnListForUser: Save Subscribed Column List For User based on layout id.
        /// </summary>
        /// <param name="objColumn"></param>
        /// <returns></returns>
        ///****************************************************************************
        public bool SaveSubscribedColumnListForUser(ColumnListEntity objColumn, long UserID, int LayoutID)
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                UserColumnMapping objColumnMapping = null;
                long UserId = 0;
                var LayoutId = 0;

                if (objColumn.SubscribedColumn != null && objColumn.SubscribedColumn.Count > 0)
                {
                    UserId = objColumn.SubscribedColumn[0].UserId;
                    LayoutId = objColumn.SubscribedColumn[0].LayoutId;
                }

                var deleteOrderDetails = cloudModelEntities.UserColumnMapping.Where(m => m.LayoutId == (LayoutID)
                                                                                         && m.UserId == (UserID)).ToList();

                if (deleteOrderDetails.Count > 0)
                {
                    cloudModelEntities.UserColumnMapping.RemoveRange(deleteOrderDetails);
                    cloudModelEntities.SaveChanges();
                }

                if (objColumn.SubscribedColumn.Count == 0)
                {
                    var columnid = GetColumnFlag(LayoutID);

                    objColumnMapping = new UserColumnMapping
                    {
                        UserId = UserID,
                        LayoutId = LayoutID,
                        ColumnId = columnid,
                        CreatedDate = DateTime.UtcNow
                    };

                    cloudModelEntities.UserColumnMapping.Add(objColumnMapping);
                }

                for (var iCount = 0; iCount < objColumn.SubscribedColumn.Count; iCount++)
                {
                    UserId = objColumn.SubscribedColumn[iCount].UserId;
                    var ColumnId = objColumn.SubscribedColumn[iCount].ColumnId;
                    LayoutId = objColumn.SubscribedColumn[iCount].LayoutId;

                    if (!cloudModelEntities.UserColumnMapping.Any(m => m.ColumnId == (ColumnId)
                                                                       && m.LayoutId == (LayoutId)
                                                                       && m.UserId == (UserId)))
                    {
                        objColumnMapping = new UserColumnMapping
                        {
                            UserId = UserId,
                            LayoutId = LayoutId,
                            ColumnId = ColumnId,
                            CreatedBy = Constants.SaveSubscribedColumnListForUser_qoskdev,
                            CreatedDate = DateTime.UtcNow,
                            OrderNumber = iCount + 1
                        };
                        cloudModelEntities.UserColumnMapping.Add(objColumnMapping);
                    }
                }

                return cloudModelEntities.SaveChanges() > 0;
            }
        }
        ///****************************************************************************
        /// <summary>
        ///  GetOnlySubscribedColumns Get Only Subscribed Columns.
        /// </summary>
        /// <param name="LayoutName"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        ///****************************************************************************
        public List<ColumnDetails> GetOnlySubscribedColumns(string LayoutName, long UserID)
        {
            var objSubscribedColumn = new List<ColumnDetails>();

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                objSubscribedColumn = (from UCM in cloudModelEntities.UserColumnMapping
                                       join MCL in cloudModelEntities.ReportMasterColumnList on UCM.ColumnId equals MCL.ReportMasterColumnListID
                                       join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                       join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                       join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                       where UCM.LayoutId == LTM.LayoutId && string.Compare(MLL.SectionName, LayoutName, true) == 0
                                             && MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate
                                             && UCM.UserId == UserID
                                       orderby UCM.OrderNumber ascending
                                       select new ColumnDetails
                                       {
                                           ColumnId = MCL.ReportMasterColumnListID,
                                           ColumnName = MCL.ColumnName,
                                           ColumnDescription = MCL.ColumnName,
                                           ColumnType = MCL.DataType,
                                           LayoutId = MLL.ReportMasterLayoutListID,
                                           TableName = MTL.TableName,
                                           FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                           UserMappingId = UCM.UserColumnMappingID,
                                           IsSearchPanelDefault = MCL.IsDefault,
                                           IsGridListingDefault = MCL.IsDefaultGrid,
                                           DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                           OrderNumber = UCM.OrderNumber,
                                           MappedTable = MCL.MappedTable,
                                           SearchOrder = MCL.SearchOrder,
                                           GridsearchOrder = MCL.GridsearchOrder
                                       }).ToList();

                if (!objSubscribedColumn.Any())
                {
                    objSubscribedColumn = (from MCL in cloudModelEntities.ReportMasterColumnList
                                           join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                           join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                           join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                           where string.Compare(MLL.SectionName, LayoutName, true) == 0
                                                 && MCL.IsDeleted != true
                                                 && (MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate && MCL.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag)
                                           orderby MCL.GridsearchOrder ascending
                                           select new ColumnDetails
                                           {
                                               ColumnId = MCL.ReportMasterColumnListID,
                                               ColumnName = MCL.ColumnName,
                                               ColumnDescription = MCL.ColumnName,
                                               ColumnType = MCL.DataType,
                                               LayoutId = MLL.ReportMasterLayoutListID,
                                               TableName = MTL.TableName,
                                               FilterType = MCL.DataType == Constants.Model_CommonDataAccess_Bit ? Constants.Model_CommonDataAccess_Equal : Constants.Model_CommonDataAccess_Contains,
                                               IsSearchPanelDefault = MCL.IsDefault,
                                               IsGridListingDefault = MCL.IsDefaultGrid,
                                               DisplayText = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                               MappedTable = MCL.MappedTable,
                                               SearchOrder = MCL.SearchOrder,
                                               GridsearchOrder = MCL.GridsearchOrder,
                                               IsAutoComplete = MCL.IsAutoComplete
                                           }).ToList();
                }
            }

            return objSubscribedColumn.Where(x => x.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag).ToList();
        }
        ///****************************************************************************
        /// <summary>
        /// GetUserSpecificColumnsForGrid: Get User Specific Columns For Grid.
        /// </summary>
        /// <param name="LayoutName"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        ///****************************************************************************
        public List<GridColumns> GetUserSpecificColumnsForGrid(string LayoutName, long UserID)
        {
            var objSubscribedColumn = new List<GridColumns>();

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                objSubscribedColumn = (from UCM in cloudModelEntities.UserColumnMapping
                                       join MCL in cloudModelEntities.ReportMasterColumnList on UCM.ColumnId equals MCL.ReportMasterColumnListID
                                       join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                       join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                       join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                       where UCM.LayoutId == LTM.LayoutId && string.Compare(MLL.SectionName, LayoutName, true) == 0
                                             && UCM.UserId == UserID
                                       orderby UCM.OrderNumber ascending
                                       select new GridColumns
                                       {
                                           datafield = MCL.ColumnName,
                                           text = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                           width = Constants.GetUserSpecificColumnsForGrid_Width
                                       }).ToList();

                if (!objSubscribedColumn.Any())
                {
                    objSubscribedColumn = (from MCL in cloudModelEntities.ReportMasterColumnList
                                           join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                           join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                           join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                           where string.Compare(MLL.SectionName, LayoutName, true) == 0
                                                 && MCL.IsDeleted != true
                                                 && (MCL.ColumnName != Constants.CommonDataAccess_DataStartDate && MCL.ColumnName != Constants.CommonDataAccess_DataEndDate && MCL.ColumnName != Constants.CreateSearchPanel_DefaulColumnFlag)
                                           orderby MCL.GridsearchOrder ascending
                                           select new GridColumns
                                           {
                                               datafield = MCL.ColumnName,
                                               text = (string.IsNullOrEmpty(MCL.DisplayText) ? MCL.ColumnName : MCL.DisplayText),
                                               width = Constants.GetUserSpecificColumnsForGrid_Width
                                           }).ToList();
                }
            }

            return objSubscribedColumn.Where(x => x.datafield != Constants.CreateSearchPanel_DefaulColumnFlag).ToList();
        }
        ///****************************************************************************
        /// <summary>
        /// GetColumnFlag: Get default column flag based on layout
        /// </summary>
        /// <param name="layoutId"></param>
        /// <returns></returns>
        ///****************************************************************************
        public int GetColumnFlag(int layoutId)
        {
            var columnID = 0;

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var ColumnFlag = (from MCL in cloudModelEntities.ReportMasterColumnList
                                  join MTL in cloudModelEntities.ReportMasterTableList on MCL.TableId equals MTL.ReportMasterTableListID
                                  join LTM in cloudModelEntities.ReportLayoutTableMapping on MTL.ReportMasterTableListID equals LTM.TableId
                                  join MLL in cloudModelEntities.ReportMasterLayoutList on LTM.LayoutId equals MLL.ReportMasterLayoutListID
                                  where MCL.ColumnName == Constants.CreateSearchPanel_DefaulColumnFlag && LTM.LayoutId == layoutId
                                  select MCL.ReportMasterColumnListID).FirstOrDefault();

                columnID = ColumnFlag;
            }

            return columnID;
        }

        /// <summary>
        /// CreateDynamicControlBasedOnValue: method used to bind dynamic control based on data type into index search page.
        /// </summary>
        /// <param name="Details"></param>
        /// <param name="value"></param>
        /// <param name="AutoComplete"></param>
        /// <returns></returns>        
        public ControlViewModel CreateDynamicControlBasedOnValue(ColumnDetails Details, object value, object AutoComplete)
        {
            ControlViewModel obj = null;
            switch (Details.ColumnType.ToLower())
            {
                case Utility.Constants.LotNumberController_bit:
                    obj = new CheckBoxViewModel
                    {
                        Visible = true,
                        Label = string.IsNullOrEmpty(Details.DisplayText) ? Details.ColumnName : Details.DisplayText,
                        Name = Details.ColumnName,
                        Value = false,
                        AssociateTableName = Details.TableName,
                        DataType = Details.ColumnType,
                        OperatorType = Details.FilterType,
                    };
                    break;
                case Utility.Constants.LotNumberController_datetime1:

                    obj = new DateTimeViewModel
                    {
                        Visible = true,
                        Label = string.IsNullOrEmpty(Details.DisplayText) ? Details.ColumnName : Details.DisplayText,
                        Name = Details.ColumnName,
                        Value = value == null ? (DateTime?)value : Convert.ToDateTime(value).Date,
                        AssociateTableName = Details.TableName,
                        DataType = Details.ColumnType,
                        OperatorType = Details.FilterType,

                    };
                    break;
                case Utility.Constants.LotNumberController_string:
                case Utility.Constants.LotNumberController_bigint:
                case Utility.Constants.LotNumberController_int:
                case Utility.Constants.LotNumberController_decimal:
                default:
                    obj = new TextBoxViewModel
                    {
                        Visible = true,
                        Label = string.IsNullOrEmpty(Details.DisplayText) ? Details.ColumnName : Details.DisplayText,
                        Name = Details.ColumnName,
                        AssociateTableName = Details.TableName,
                        Value = value != null ? value.ToString() : null,
                        DataType = Details.ColumnType,
                        OperatorType = Details.FilterType,
                        IsAutoComplete = AutoComplete == null ? false : Convert.ToBoolean(AutoComplete)
                    };
                    break;
                case Utility.Constants.LotNumberController_select:

                    if (string.Compare(Details.ColumnName, Utility.Constants.LotNumberController_RXorOTC, true) == 0)
                    {
                        obj = new DropDownListViewModel
                        {
                            Visible = true,
                            Label = string.IsNullOrEmpty(Details.DisplayText) ? Details.ColumnName : Details.DisplayText,
                            Name = Details.ColumnName,
                            AssociateTableName = Details.TableName,

                            DataType = Details.ColumnType,
                            OperatorType = Details.FilterType,
                            Values = new SelectList(
                            new[]
                            {
                                new { Value = Utility.Constants.Model_ProfilesDataAccess_Zero, Text = Utility.Constants.ReportController_Select },
                                new { Value = Utility.Constants.ReportController_True, Text = Utility.Constants.ReportController_Rprescription },
                                new { Value = Utility.Constants.ReportController_False, Text = Utility.Constants.ReportController_OTC },
                            }, Utility.Constants.ReportController_Value, Utility.Constants.ReportController_Text, Utility.Constants.Model_ProfilesDataAccess_Zero
                                )
                        };

                    }
                    else
                    {
                        obj = new DropDownListViewModel
                        {
                            Visible = true,
                            Label = string.IsNullOrEmpty(Details.DisplayText) ? Details.ColumnName : Details.DisplayText,
                            Name = Details.ColumnName,
                            AssociateTableName = Details.TableName,
                            OperatorType = Details.FilterType,
                        };
                    }
                    break;
            }
            return obj;
        }

    }
}