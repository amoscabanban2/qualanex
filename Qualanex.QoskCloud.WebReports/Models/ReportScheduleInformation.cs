﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// ReportScheduleInformation: This entity class is used to bind ReportSchedule grid.
    /// </summary>
    public class ReportScheduleInformation
    {
        public long ReportScheduleID { get; set; }
        public long? ReportID { get; set; }
        public long? UserID { get; set; }
        public string ReportScheduleTitle { get; set; }
        public string Schedule { get; set; }
        public string Criteria { get; set; }
    }
}