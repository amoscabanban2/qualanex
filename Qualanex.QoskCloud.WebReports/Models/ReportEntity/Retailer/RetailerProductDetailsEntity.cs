﻿using Qualanex.QoskCloud.WebReports.ReportClasses;

namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// RetailerProductDetailsEntity: This Entity class is used to save and update RetailerProductDetails criteria.
    /// </summary>
    public class RetailerProductDetailsEntity : ReportCriteriaBaseClass
    {
        public string ExpDate { get; set; }
        public string LotNumber { get; set; }
        public string VendorName { get; set; }
        public string Creditable { get; set; }
        public string StoreName { get; set; }
        public string StoreNumber { get; set; }
        public string PartialQty { get; set; }
        public string Strength { get; set; }
        public string NDCNumber { get; set; }
        public string ItemGuid { get; set; }
        public string QoskProcessDate { get; set; }
        public string ProductDescription { get; set; }
        public int? ControlNumber { get; set; }
        public decimal? PackageSize { get; set; }
        public string FullQty { get; set; }
        public string RetailerVendorNumber { get; set; }
        public int? RetailerItem { get; set; }
        public string UnitPriceBefore { get; set; }
        public string UnitPriceAfter { get; set; }
        public string CreditableAmountBeforeMFGDiscount { get; set; }
        public string CreditableAmountAfter { get; set; }
        public string OutofpolicyDescription { get; set; }
        public string RecalledProduct { get; set; }
        public string DiscontinuedProduct { get; set; }
        public string RXorOTC { get; set; }
        public string DosageForm { get; set; }
        public string PackageForm { get; set; }
        public string PartialPercentage { get; set; }
    }
    /// <summary>
    /// ReportRetailerProductDetails
    /// </summary>
    public class ReportRetailerProductDetails
    {
        public System.Collections.Generic.List<ProductDetailsEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }

}
