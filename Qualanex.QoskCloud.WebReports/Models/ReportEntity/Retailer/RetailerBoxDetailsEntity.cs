﻿using System;
namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// RetailerBoxDetailsEntity: This Entity class is used to save and update RetailerBoxDetails criteria.
    /// </summary>
    public class RetailerBoxDetailsEntity
    {
        public string ExpirationDate { get; set; }
        public string LotNumbersID { get; set; }
        public string Strength { get; set; }
        public string DosageForm { get; set; }
        public long? NDC { get; set; }
        public Decimal? PackageSize { get; set; }
        public long? StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string RegionName { get; set; }
        public int? BoxNumber { get; set; }
        public int? ASN { get; set; }
        public string ProcessDate { get; set; }
        public string DisposalDate { get; set; }
        public int? VendorNumber { get; set; }
        public string VendorName { get; set; }
        public int? RetailerItem { get; set; }
        public string NDAName { get; set; }
        public int? DEAClass { get; set; }
        public double? FullQty { get; set; }
        public double? PartialQty { get; set; }
        public double? UnitPriceBefore { get; set; }
        public decimal? UnitPriceAfter { get; set; }
        public decimal? CreditableAmountBefore { get; set; }
        public decimal? CreditableAmountAfter { get; set; }
        public decimal? IndateAmount { get; set; }
        public decimal? OverrideAmount { get; set; }
        public decimal? WasteAmount { get; set; }
        public int? WasteReasonCode { get; set; }
        public string WasteReasonCodeDescription { get; set; }
    }
    /// <summary>
    /// ReportRetailerBoxDetails
    /// </summary>
    public class ReportRetailerBoxDetails
    {
        public System.Collections.Generic.List<RetailerBoxDetailsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }

    }

}
