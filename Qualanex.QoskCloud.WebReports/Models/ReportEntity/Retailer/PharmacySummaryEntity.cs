﻿using Qualanex.QoskCloud.WebReports.ReportClasses;
namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// PharmacySummaryEntity: This Entity class is used to save and update PharmacySummary criteria.
    /// </summary>
    public class PharmacySummaryEntity : ReportCriteriaBaseClass
    {
        public string StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string WholesalerCustomerNumber { get; set; }
        public string RegionName { get; set; }
        public string ReturnCreditableValue { get; set; }
        public string ReturnCreditableQty { get; set; }
        public string RecallCreditableValue { get; set; }
        public string RecallCreditableQty { get; set; }
        public string NonCreditableValue { get; set; }
        public string NonCreditableQty { get; set; }
        public string TotalProductValue { get; set; }
        public string TotalQty { get; set; }
        public string ProcessingFeeBilled { get; set; }
        public string TotalInvoiceAmount { get; set; }
        public string TotalDebitMemoQty { get; set; }
        public string ReturnToStockValue { get; set; }
        public string ReturnToStockQty { get; set; }
    }
    /// <summary>
    /// ReportPharmacySummaryEntity
    /// </summary>
    public class ReportPharmacySummaryEntity
    {
        public System.Collections.Generic.List<PharmacyReportEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }

}
