﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// RetailerBoxSummaryEntity: This Entity class is used to save and update RetailerBoxSummary criteria.
    /// </summary>
    public class RetailerBoxSummaryEntity
    {
        public long? WholesalerNumber { get; set; }
        public string ProcessDate { get; set; }
        public long? BoxNumber { get; set; }
        public string CreditableValue { get; set; }
        public string RecallCreditableValue { get; set; }
        public string RecallCreditableQuantity { get; set; }
        public int? StoreNumber { get; set; }
        public string StoreName { get; set; }
        public int? ASN { get; set; }
        public string DisposalDate { get; set; }
        public string IndateQuantity { get; set; }
        public string CreditablePieces { get; set; }
        public int? WasteQuantity { get; set; }
        public string IndataValue { get; set; }
        public double? TotalInvoiceAmount { get; set; }
        public double? ProcessingFeebilled { get; set; }
        public double? TotalProductvalue { get; set; }
        public string WasteValue { get; set; }
    }
    /// <summary>
    /// ReportRetailerBoxSummary
    /// </summary>
    public class ReportRetailerBoxSummary
    {
        public System.Collections.Generic.List<RetailerBoxSummaryEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
