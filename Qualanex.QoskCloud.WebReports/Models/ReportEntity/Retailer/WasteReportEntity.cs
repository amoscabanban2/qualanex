﻿using Qualanex.QoskCloud.WebReports.ReportClasses;
namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// WasteReportEntity: This Entity class is used to save and update WasteReport criteria.
    /// </summary>
    public class WasteReportEntity : ReportCriteriaBaseClass
    {
        public string ItemGuid { get; set; }
        public string SealedOpenCase { get; set; }
        public string LotNumber { get; set; }
        public string Strength { get; set; }
        public string WasteCode { get; set; }
        public string WasteStreamProfile { get; set; }
        public string NDCNumber { get; set; }
        public string ProductDescription { get; set; }
        public string FullQty { get; set; }
        public string PartialQty { get; set; }
        public string StoreName { get; set; }
        public string StoreNumber { get; set; }
        public decimal? PackageSize { get; set; }
        public string ExpDate { get; set; }
        public int? ControlNumber { get; set; }
        public string DosageForm { get; set; }
        public string PackageForm { get; set; }
    }
    /// <summary>
    /// ReportRetailerWasteDetail
    /// </summary>
    public class ReportRetailerWasteDetail
    {
        public System.Collections.Generic.List<WasteReportEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }

}
