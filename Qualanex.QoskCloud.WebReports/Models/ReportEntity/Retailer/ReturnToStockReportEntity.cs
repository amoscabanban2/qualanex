﻿using Qualanex.QoskCloud.WebReports.ReportClasses;
namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// ReturnToStockReportEntity: This Entity class is used to save and update ReturnToStockReport criteria.
    /// </summary>
    public class ReturnToStockReportEntity : ReportCriteriaBaseClass
    {
        public string VendorName { get; set; }
        public string StoreName { get; set; }
        public string StoreNumber { get; set; }
        public string NDCNumber { get; set; }
        public string ProductDescription { get; set; }
        public string Strength { get; set; }
        public int? ControlNumber { get; set; }
        public string RXorOTC { get; set; }
        public string DosageForm { get; set; }
        public string PackageForm { get; set; }
        public string LotNumber { get; set; }
        public string ExpDate { get; set; }
        public string QoskProcessDate { get; set; }
        public string DateEligibleForCredit { get; set; }
        public string OutofpolicyDescription { get; set; }
    }
    /// <summary>
    /// ReportReturnToStockReportEntity
    /// </summary>
    public class ReportReturnToStockReportEntity
    {
        public System.Collections.Generic.List<ReturnToStockEmailEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}