﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// WholesalerMFGSummaryEntity: This Entity class is used to save and update WholesalerMFGSummary criteria.
    /// </summary>
    public class WholesalerMFGSummaryEntity
    {
        public int? MFGLabeler { get; set; }
        public string MFGNumber { get; set; }
        public string MFGPhoneNumber { get; set; }
        public long? WholesalerVendorNumber { get; set; }
        public long? DebitMemoInvoiceNo { get; set; }
        public double? TotalInvoiceAmount { get; set; }
        public string CreditableValue { get; set; }
        public double? CreditableQty { get; set; }
        public string RecallCreditableValue { get; set; }
        public double? RecallCreditableQty { get; set; }
        public double? TotalProductvalue { get; set; }
        public double? TotalPieceQty { get; set; }
        public double? ProcessingFee { get; set; }
    }
    /// <summary>
    /// ReportWholesalerMFGSummary
    /// </summary>
    public class ReportWholesalerMFGSummary
    {
        public System.Collections.Generic.List<WholesalerMFGSummaryEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
