﻿using System;
namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// WholesalerReturnToStockDetailsEntity: This Entity class is used to save and update WholesalerReturnToStockDetails criteria.
    /// </summary>
    public class WholesalerReturnToStockDetailsEntity
    {
        public string IndateProcessingMonthyear { get; set; }
        public string ExpirationDate { get; set; }
        public long? DivisionNumber { get; set; }
        public string DivisionName { get; set; }
        public long? RegionNumber { get; set; }
        public string RegionName { get; set; }
        public string Vendor { get; set; }
        public int? Labeler { get; set; }
        public long? NDC { get; set; }
        public string Name { get; set; }
        public int? WholesalerItemsNumber { get; set; }
        public string Strength { get; set; }
        public string DEANumber { get; set; }
        public string Form { get; set; }
        public Decimal? PackageSize { get; set; }
        public double? FullQty { get; set; }
        public double? PartialQty { get; set; }
        public double? Partial { get; set; }
        public decimal? Indatevalue { get; set; }
        public string LotNumber { get; set; }
        public string PeriodProcessed { get; set; }
        public int? ASN { get; set; }
        public string Shipment { get; set; }
    }
    /// <summary>
    /// ReportWholesalerReturnToStockDetails
    /// </summary>
    public class ReportWholesalerReturnToStockDetails
    {
        public System.Collections.Generic.List<WholesalerReturnToStockDetailsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
