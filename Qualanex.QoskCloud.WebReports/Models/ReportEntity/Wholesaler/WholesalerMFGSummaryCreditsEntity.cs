﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// WholesalerMFGSummaryCreditsEntity: This Entity class is used to save and update WholesalerMFGSummaryCredits criteria.
    /// </summary>
    public class WholesalerMFGSummaryCreditsEntity
    {
        public int? MFGLabeler { get; set; }
        public string MFGNumber { get; set; }
        public int? MFGPhoneNumber { get; set; }
        public long? WholesalerVendorName { get; set; }
        public long? DebitMemoInvoiceNo { get; set; }
        public string CreditableValue { get; set; }
        public double? CreditableQty { get; set; }
        public string RecallCreditableValue { get; set; }
        public double? RecallCreditableQty { get; set; }
        public double? TotalProductvalue { get; set; }
        public double? TotalPieceQty { get; set; }
        public double? ProcessingFee { get; set; }
        public double? TotalInvoiceAmount { get; set; }
        public string MFGPMTCreditsChecks { get; set; }
        public double? VarianceFavUnfav { get; set; }
        public string Collection { get; set; }
    }
    /// <summary>
    /// ReportWholesalerMFGSummaryCredits
    /// </summary>
    public class ReportWholesalerMFGSummaryCredits
    {
        public System.Collections.Generic.List<WholesalerMFGSummaryCreditsEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
