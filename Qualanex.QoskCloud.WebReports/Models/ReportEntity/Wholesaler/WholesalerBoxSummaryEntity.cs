﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// WholesalerBoxSummaryEntity: This Entity class is used to save and update WholesalerBoxSummary criteria.
    /// </summary>
    public class WholesalerBoxSummaryEntity
    {
        public long? DivisionNumber { get; set; }
        public string DivisionName { get; set; }
        public long? BoxNumber { get; set; }
        public int? ASN { get; set; }
        public string ProcessDate { get; set; }
        public string DisposalDate { get; set; }
        public double? TotalInvoiceAmount { get; set; }
        public string CreditableValue { get; set; }
        public string CreditablePieces { get; set; }
        public string RecallCreditableValue { get; set; }
        public string RecallCreditableQty { get; set; }
        public string WasteValue { get; set; }
        public int? WasteQuantity { get; set; }
        public string IndateValue { get; set; }
        public string IndateQuantity { get; set; }
        public double? TotalProductvalue { get; set; }
        public double? TotalQty { get; set; }
        public double? ProcessingFee { get; set; }
    }
    /// <summary>
    /// ReportWholesalerBoxSummary
    /// </summary>
    public class ReportWholesalerBoxSummary
    {
        public System.Collections.Generic.List<WholesalerBoxSummaryEntity> reportRetailerBoxDetails { get; set; }
        public System.Collections.Generic.List<GridColumns> gridColumns { get; set; }
    }
}
