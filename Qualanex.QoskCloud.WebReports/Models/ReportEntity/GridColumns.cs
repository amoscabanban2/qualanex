﻿namespace Qualanex.QoskCloud.WebReports.Models
{
    /// <summary>
    /// GridColumns: This Entity class is used for binding jqx-grid property.
    /// </summary>
    public class GridColumns
    {
        public string text { get; set; }
        public string editable { get; set; }
        public string datafield { get; set; }
        public string width { get; set; }
    }

}