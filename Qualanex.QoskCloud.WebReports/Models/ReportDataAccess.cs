﻿using System.Collections.Generic;
using System.Linq;
using Qualanex.QoskCloud.Entity;
using Qualanex.QoskCloud.WebReports.ViewModel;
using Newtonsoft.Json;
using Qualanex.QoskCloud.Utility;
using System;
using Qualanex.QoskCloud.Utility.Common;
using Qualanex.QoskCloud.WebReports.Common;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;

namespace Qualanex.QoskCloud.WebReports.Models
{
    public class ReportDataAccess
    {
         /// <summary>
        /// GetRegionList: Get region list.
        /// </summary>
        /// <returns></returns>
        public List<StateDict> GetRegionList()
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var regions = (from r in cloudModelEntities.StateDict
                               select r).ToList();
                return regions;
            }
        }
        /// <summary>
        /// GetAllReport: Retrieve all the reports from report table.
        /// </summary>
        /// <returns></returns>
        public IList<ReportEntity> GetAllReport()
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reports = (from report in cloudModelEntities.Report
                               where report.IsDeleted == false
                               select new ReportEntity
                               {
                                   ReportID = report.ReportID,
                                   ReportTitle = report.ReportTitle,
                                   ReportModule = report.ReportModule
                               }).ToList();
                return reports;
            }
        }
        /// <summary>
        /// InsertUpdateScheduledData: Insert and update schedule data in report schedule table.
        /// </summary>
        /// <param name="reportScheduleDetailViewModel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Response InsertUpdateScheduledData(ReportScheduleDetailViewModel reportScheduleDetailViewModel, long userId)
        {
            Response objclsResponse = new Response();
            ReportSchedule objReportSchedule = null;
            long reportScheduleID = 0;
            try
            {
                if (reportScheduleDetailViewModel != null)
                {
                    using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                    {
                        ReportSchedulerEntity objReportSchedulerEntity = new ReportSchedulerEntity()
                        {
                            ScheduleType = (ScheduleType)reportScheduleDetailViewModel.ScheduleType,
                            FileType = reportScheduleDetailViewModel.FileType,
                            SMSTo = reportScheduleDetailViewModel.SMSTo,
                            Every = reportScheduleDetailViewModel.Every,
                            StartDate = CreateUTCDateTime(reportScheduleDetailViewModel.ReportScheduleTimeZone, reportScheduleDetailViewModel.StartDate, reportScheduleDetailViewModel.Hour, reportScheduleDetailViewModel.Minute, reportScheduleDetailViewModel.Meridieum.ToString()),
                            EndDate = CreateUTCDateTime(reportScheduleDetailViewModel.ReportScheduleTimeZone, reportScheduleDetailViewModel.EndDate, reportScheduleDetailViewModel.Hour, reportScheduleDetailViewModel.Minute, reportScheduleDetailViewModel.Meridieum.ToString()),
                            Hour = reportScheduleDetailViewModel.Hour,
                            Minute = reportScheduleDetailViewModel.Minute,
                            Meridieum = reportScheduleDetailViewModel.Meridieum,
                            ReportScheduleTimeZone = reportScheduleDetailViewModel.ReportScheduleTimeZone

                        };
                        string JsonSchedule = JsonConvert.SerializeObject(objReportSchedulerEntity);

                        //Code for update report schedule table

                        var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleDetailViewModel.ReportScheduleID);
                        int response = 0;
                        if (retailerReportSchedule != null)
                        {
                            retailerReportSchedule.ReportScheduleTitle = reportScheduleDetailViewModel.ScheduleReportTitle;
                            retailerReportSchedule.Schedule = JsonSchedule;
                            retailerReportSchedule.ModifiedDate = DateTime.UtcNow;
                            cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                            response = cloudModelEntities.SaveChanges();
                        }
                        else
                        {
                            objReportSchedule = new ReportSchedule()
                            {
                                ReportID = reportScheduleDetailViewModel.ReportID,
                                ReportScheduleTitle = reportScheduleDetailViewModel.ScheduleReportTitle,
                                UserID = userId,
                                Schedule = JsonSchedule,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow
                            };
                            cloudModelEntities.ReportSchedule.Add(objReportSchedule);
                            response = cloudModelEntities.SaveChanges();
                        }

                        if (response > 0 && !string.IsNullOrEmpty(reportScheduleDetailViewModel.EmailTo))
                        {
                            if (retailerReportSchedule != null)
                            {
                                var retailerReportScheduleData = cloudModelEntities.ReportRecipient.Where(c => c.ReportScheduleId == reportScheduleDetailViewModel.ReportScheduleID).ToList();
                                cloudModelEntities.ReportRecipient.RemoveRange(retailerReportScheduleData);
                                int responseResult = cloudModelEntities.SaveChanges();
                                reportScheduleID = reportScheduleDetailViewModel.ReportScheduleID;
                                string[] EmailAddresses = reportScheduleDetailViewModel.EmailTo.Split(Constants.Comma);
                                foreach (var email in EmailAddresses)
                                {
                                    cloudModelEntities.ReportRecipient.Add(new ReportRecipient()
                                    {
                                        ReportScheduleId = reportScheduleID,
                                        RecipientEmail = email
                                    });
                                }
                            }
                            else if (objReportSchedule != null)
                            {
                                reportScheduleID = objReportSchedule.ReportScheduleID;
                                string[] EmailAddresses = reportScheduleDetailViewModel.EmailTo.Split(Constants.Comma);
                                foreach (var email in EmailAddresses)
                                {
                                    cloudModelEntities.ReportRecipient.Add(new ReportRecipient()
                                    {
                                        ReportScheduleId = reportScheduleID,
                                        RecipientEmail = email
                                    });
                                }
                            }
                            int response1 = cloudModelEntities.SaveChanges();
                            if (response1 > 0)
                            {
                                objclsResponse.Status = Status.OK;
                                objclsResponse.ReportScheduleId = reportScheduleID;
                            }
                        }
                        else
                        { objclsResponse.Status = Status.Fail; }

                    }
                }
            }
            catch
            {
                objclsResponse.Status = Status.Fail;
            }
            return objclsResponse;
        }
        /// <summary>
        /// UpdateProductDetailsCriteria: Update Product Details criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="retailerProductDetailsEntity"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        public Response UpdateProductDetailsCriteria(RetailerProductDetailsEntity retailerProductDetailsEntity, long ReportScheduleId)
        {
            Response objclsResponse = new Response();
            if (retailerProductDetailsEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    RetailerProductDetailsEntity objRetailerProductDetailsEntity = new RetailerProductDetailsEntity()
                    {
                        ItemGuid = retailerProductDetailsEntity.ItemGuid,
                        Strength = retailerProductDetailsEntity.Strength,
                        QoskProcessDate = retailerProductDetailsEntity.QoskProcessDate,
                        ControlNumber = retailerProductDetailsEntity.ControlNumber,
                        PackageSize = retailerProductDetailsEntity.PackageSize,
                        UnitPriceAfter = retailerProductDetailsEntity.UnitPriceAfter,
                        CreditableAmountAfter = retailerProductDetailsEntity.CreditableAmountAfter,
                        RecalledProduct = retailerProductDetailsEntity.RecalledProduct,
                        DiscontinuedProduct = retailerProductDetailsEntity.DiscontinuedProduct,
                        RXorOTC = retailerProductDetailsEntity.RXorOTC,
                        DosageForm = retailerProductDetailsEntity.DosageForm,
                        PackageForm = retailerProductDetailsEntity.PackageForm,
                        PartialPercentage = retailerProductDetailsEntity.PartialPercentage,
                        NDCNumber = retailerProductDetailsEntity.NDCNumber,
                        ReportDataTimeZone = retailerProductDetailsEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = retailerProductDetailsEntity.ReportScheduleTimeZone,
                        StoreName = retailerProductDetailsEntity.StoreName,
                        StoreNumber = retailerProductDetailsEntity.StoreNumber,
                        VendorName = retailerProductDetailsEntity.VendorName,
                        ProductDescription = retailerProductDetailsEntity.ProductDescription,
                        LotNumber = retailerProductDetailsEntity.LotNumber,
                        ExpDate = retailerProductDetailsEntity.ExpDate,
                        FullQty = retailerProductDetailsEntity.FullQty,
                        PartialQty = retailerProductDetailsEntity.PartialQty,
                        UnitPriceBefore = retailerProductDetailsEntity.UnitPriceBefore,
                        CreditableAmountBeforeMFGDiscount = retailerProductDetailsEntity.CreditableAmountBeforeMFGDiscount,
                        Creditable = retailerProductDetailsEntity.Creditable,
                        OutofpolicyDescription = retailerProductDetailsEntity.OutofpolicyDescription,
                        DataStartDate = retailerProductDetailsEntity.DataStartDate,
                        DataEndDate = retailerProductDetailsEntity.DataEndDate
                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objRetailerProductDetailsEntity);

                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }
        /// <summary>
        /// UpdateWasteReportCriteria: Update Waste Report criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="wasteReportDetail"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        public Response UpdateWasteReportCriteria(WasteReportEntity wasteReportEntity, long ReportScheduleId)
        {
            Response objclsResponse = new Response();
            if (wasteReportEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    WasteReportEntity objWasteReportEntity = new WasteReportEntity()
                    {
                        NDCNumber = wasteReportEntity.NDCNumber,
                        StoreName = wasteReportEntity.StoreName,
                        StoreNumber = wasteReportEntity.StoreNumber,
                        ReportDataTimeZone = wasteReportEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = wasteReportEntity.ReportScheduleTimeZone,
                        ItemGuid = wasteReportEntity.ItemGuid,
                        ProductDescription = wasteReportEntity.ProductDescription,
                        LotNumber = wasteReportEntity.LotNumber,
                        Strength = wasteReportEntity.Strength,
                        FullQty = wasteReportEntity.FullQty,//Partial Quantity
                        ExpDate = wasteReportEntity.ExpDate,
                        PackageSize = wasteReportEntity.PackageSize,
                        PartialQty = wasteReportEntity.PartialQty,
                        WasteStreamProfile = wasteReportEntity.WasteStreamProfile,
                        WasteCode = wasteReportEntity.WasteCode,
                        SealedOpenCase = wasteReportEntity.SealedOpenCase,
                        DataStartDate = wasteReportEntity.DataStartDate,
                        DataEndDate = wasteReportEntity.DataEndDate,
                        ControlNumber = wasteReportEntity.ControlNumber,
                        DosageForm = wasteReportEntity.DosageForm,
                        PackageForm = wasteReportEntity.PackageForm

                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objWasteReportEntity);

                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }

        /// <summary>
        /// UpdatePharmacySummaryCriteria: Update Pharmacy Summary criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="pharmacySummaryEntity"></param>
        /// <param name="ReportScheduleId"></param>
        /// <returns></returns>
        public Response UpdatePharmacySummaryCriteria(PharmacySummaryEntity pharmacySummaryEntity, long ReportScheduleId)
        {
            Response objclsResponse = new Response();
            if (pharmacySummaryEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    PharmacySummaryEntity objPharmacySummaryEntity = new PharmacySummaryEntity()
                    {
                        StoreNumber = pharmacySummaryEntity.StoreNumber,
                        StoreName = pharmacySummaryEntity.StoreName,
                        WholesalerCustomerNumber = pharmacySummaryEntity.WholesalerCustomerNumber,
                        ReportDataTimeZone = pharmacySummaryEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = pharmacySummaryEntity.ReportScheduleTimeZone,
                        RegionName = pharmacySummaryEntity.RegionName,
                        ReturnCreditableValue = pharmacySummaryEntity.ReturnCreditableValue,
                        ReturnCreditableQty = pharmacySummaryEntity.ReturnCreditableQty,
                        RecallCreditableValue = pharmacySummaryEntity.RecallCreditableValue,
                        RecallCreditableQty = pharmacySummaryEntity.RecallCreditableQty,//Partial Quantity
                        NonCreditableValue = pharmacySummaryEntity.NonCreditableValue,
                        NonCreditableQty = pharmacySummaryEntity.NonCreditableQty,
                        TotalProductValue = pharmacySummaryEntity.TotalProductValue,
                        TotalQty = pharmacySummaryEntity.TotalQty,
                        ProcessingFeeBilled = pharmacySummaryEntity.ProcessingFeeBilled,
                        TotalInvoiceAmount = pharmacySummaryEntity.TotalInvoiceAmount,
                        TotalDebitMemoQty = pharmacySummaryEntity.TotalDebitMemoQty,
                        ReturnToStockValue = pharmacySummaryEntity.ReturnToStockValue,
                        ReturnToStockQty = pharmacySummaryEntity.ReturnToStockQty
                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objPharmacySummaryEntity);
                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == ReportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }
        /// <summary>
        /// UpdateNonCreditableSummaryCriteria: Update Non Creditable Summary criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="nonCreditableSummaryEntity"></param>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        public Response UpdateNonCreditableSummaryCriteria(NonCreditableSummaryEntity nonCreditableSummaryEntity, long reportScheduleId)
        {
            Response objclsResponse = new Response();
            if (nonCreditableSummaryEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    NonCreditableSummaryEntity objNonCreditableSummaryEntity = new NonCreditableSummaryEntity()
                    {
                        StoreNumber = nonCreditableSummaryEntity.StoreNumber,
                        StoreName = nonCreditableSummaryEntity.StoreName,
                        ReportDataTimeZone = nonCreditableSummaryEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = nonCreditableSummaryEntity.ReportScheduleTimeZone,
                        RegionName = nonCreditableSummaryEntity.RegionName,
                        NonCreditableValue = nonCreditableSummaryEntity.NonCreditableValue,
                        NonCreditableQty = nonCreditableSummaryEntity.NonCreditableQty,
                        ManufacturerName = nonCreditableSummaryEntity.ManufacturerName,
                        NonCreditableReasonCode = nonCreditableSummaryEntity.NonCreditableReasonCode,
                        NonCreditableReasonDescription = nonCreditableSummaryEntity.NonCreditableReasonDescription,

                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objNonCreditableSummaryEntity);

                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }
        /// <summary>
        /// UpdateManufacturerSummaryCriteria: Update Manufacturer Summary criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="manufacturerSummaryEntity"></param>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        public Response UpdateManufacturerSummaryCriteria(ManufacturerSummaryEntity manufacturerSummaryEntity, long reportScheduleId)
        {
            Response objclsResponse = new Response();
            if (manufacturerSummaryEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    ManufacturerSummaryEntity objManufacturerSummaryEntity = new ManufacturerSummaryEntity()
                    {
                        MFGLabeler = manufacturerSummaryEntity.MFGLabeler,
                        ManufacturerName = manufacturerSummaryEntity.ManufacturerName,
                        ReportDataTimeZone = manufacturerSummaryEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = manufacturerSummaryEntity.ReportScheduleTimeZone,
                        RetailerVendorNumber = manufacturerSummaryEntity.RetailerVendorNumber,
                        DebitMemoInvoiceNo = manufacturerSummaryEntity.DebitMemoInvoiceNo,
                        CreditableValue = manufacturerSummaryEntity.CreditableValue,
                        CreditableQty = manufacturerSummaryEntity.CreditableQty,
                        RecallCreditableValue = manufacturerSummaryEntity.RecallCreditableValue,
                        RecallCreditableQty = manufacturerSummaryEntity.RecallCreditableQty,
                        TotalProductValue = manufacturerSummaryEntity.TotalProductValue,
                        TotalQty = manufacturerSummaryEntity.TotalQty,
                        ProcessingFee = manufacturerSummaryEntity.ProcessingFee,
                        TotalInvoiceAmount = manufacturerSummaryEntity.TotalInvoiceAmount,

                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objManufacturerSummaryEntity);

                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }
        /// <summary>
        /// UpdateReturnToStockCriteria: Update Return To Stock criteria in ReportSchedule table in criteria column.
        /// </summary>
        /// <param name="returnToStockReportEntity"></param>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        public Response UpdateReturnToStockCriteria(ReturnToStockReportEntity returnToStockReportEntity, long reportScheduleId)
        {
            Response objclsResponse = new Response();
            if (returnToStockReportEntity != null)
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    ReturnToStockReportEntity objReturnToStockReportEntity = new ReturnToStockReportEntity()
                    {
                        VendorName = returnToStockReportEntity.VendorName,
                        StoreName = returnToStockReportEntity.StoreName,
                        ReportDataTimeZone = returnToStockReportEntity.ReportDataTimeZone,
                        ReportScheduleTimeZone = returnToStockReportEntity.ReportScheduleTimeZone,
                        StoreNumber = returnToStockReportEntity.StoreNumber,
                        NDCNumber = returnToStockReportEntity.NDCNumber,
                        ProductDescription = returnToStockReportEntity.ProductDescription,
                        Strength = returnToStockReportEntity.Strength,
                        ControlNumber = returnToStockReportEntity.ControlNumber,
                        RXorOTC = returnToStockReportEntity.RXorOTC,
                        DosageForm = returnToStockReportEntity.DosageForm,
                        PackageForm = returnToStockReportEntity.PackageForm,
                        LotNumber = returnToStockReportEntity.LotNumber,
                        ExpDate = returnToStockReportEntity.ExpDate,
                        QoskProcessDate = returnToStockReportEntity.QoskProcessDate,
                        DateEligibleForCredit = returnToStockReportEntity.DateEligibleForCredit,
                        OutofpolicyDescription = returnToStockReportEntity.OutofpolicyDescription,

                    };
                    string JsonCriteria = JsonConvert.SerializeObject(objReturnToStockReportEntity);

                    //Code for update report schedule table
                    var retailerReportSchedule = cloudModelEntities.ReportSchedule.FirstOrDefault(p => p.ReportScheduleID == reportScheduleId);
                    int response = 0;
                    if (retailerReportSchedule != null)
                    {
                        retailerReportSchedule.Criteria = JsonCriteria;
                        cloudModelEntities.Entry(retailerReportSchedule).State = System.Data.Entity.EntityState.Modified;
                        response = cloudModelEntities.SaveChanges();
                    }
                    if (response > 0)
                    {
                        objclsResponse.Status = Status.OK;
                    }
                    else
                    { objclsResponse.Status = Status.Fail; }

                }
            }
            return objclsResponse;
        }
        /// <summary>
        /// CreateUTCDateTime: Convert date into UTC datetime.
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <returns></returns>
        public DateTime? CreateUTCDateTime(string timezone, DateTime? datetime, int hour, int minute, string Meridieum)
        {
            if (datetime == null) return null;
            return SchedulerFunctions.ConvertTimeZoneDateTimeToUTC(timezone, datetime.Value, hour, minute, Meridieum);
        }

        /// <summary>
        /// GetReportRunDetails: Bind Report Status Grid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ReportScheduleID"></param>
        /// <returns></returns>
        public List<ReportSummary> GetReportRunDetails(long userId, long ReportScheduleID)
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reportDetails = (from report in cloudModelEntities.Report
                                     join reportSchedule in cloudModelEntities.ReportSchedule on
                                     report.ReportID equals reportSchedule.ReportID
                                     join reportRun in cloudModelEntities.ReportRun on
                                     reportSchedule.ReportScheduleID equals reportRun.ReportScheduleID
                                     where reportSchedule.UserID == userId && reportSchedule.ReportScheduleID == ReportScheduleID
                                     select new ReportSummary()
                                     {
                                         ReportId = report.ReportID,
                                         ReportTitle = report.ReportTitle,
                                         Status = reportRun.Status,
                                         StatusDescription = reportRun.StatusDescription,
                                         RunTime = reportRun.RunTime.ToString(),
                                         BlobFile = reportRun.BlobFile
                                     }).ToList();
                if (reportDetails != null)
                {
                    foreach (var item in reportDetails)
                    {
                        if (!string.IsNullOrEmpty(item.ReportTitle) && item.ReportTitle == Constants.Report_ActionName_RetailerProductDetail)
                        {
                            item.ReportTitle = Constants.Report_Name_Product_Detail;
                        }
                    }
                }
                return reportDetails;
            }
        }
        /// <summary>
        /// GetReportSchedulerDetails: Set data on Report Scheduler Grid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public List<ReportScheduleInformation> GetReportSchedulerDetails(long userId, int reportId)
        {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reportDetails = (from reportSchedule in cloudModelEntities.ReportSchedule
                                     where reportSchedule.ReportID == reportId && reportSchedule.UserID == userId && reportSchedule.IsDeleted != true
                                     orderby reportSchedule.ModifiedDate descending
                                     select new ReportScheduleInformation()
                                     {
                                         ReportScheduleID = reportSchedule.ReportScheduleID,
                                         ReportID = reportSchedule.ReportID,
                                         UserID = reportSchedule.UserID,
                                         ReportScheduleTitle = reportSchedule.ReportScheduleTitle,
                                         Schedule = reportSchedule.Schedule,
                                         Criteria = reportSchedule.Criteria

                                     }).ToList();
                if (reportDetails != null)
                {
                    foreach (var item in reportDetails)
                    {
                        item.Schedule = FormattedSchedule(item.Schedule);
                        item.Criteria = FormattedCriteria(item.Criteria);
                    }
                }
                return reportDetails;
            }
        }
        /// <summary>
        ///  FormattedSchedule: Function for showing Formatted Schedule.
        /// </summary>
        /// <param name="Schedule"></param>
        /// <returns></returns>
        public string FormattedSchedule(string Schedule)
        {
            string text = string.Empty;
            string scheduledType = string.Empty;
            ReportScheduleDetailViewModel objReportScheduleDetailViewModel = JsonConvert.DeserializeObject<ReportScheduleDetailViewModel>(Schedule);
            if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Hour)
            {
                scheduledType = Constants.FormattedSchedule_Hour;
            }
            else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Day)
            {
                scheduledType = Constants.FormattedSchedule_Day;
            }
            else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Week)
            {
                scheduledType = Constants.FormattedSchedule_Week;
            }
            else if (objReportScheduleDetailViewModel.ScheduleType == ScheduleType.Month)
            {
                scheduledType = Constants.FormattedSchedule_Month;
            }
            objReportScheduleDetailViewModel.StartDate = objReportScheduleDetailViewModel.StartDate != null ? SchedulerFunctions.ConvertUTCToTimeZoneDate(objReportScheduleDetailViewModel.ReportScheduleTimeZone, objReportScheduleDetailViewModel.StartDate).Value : objReportScheduleDetailViewModel.StartDate;
            SetTimeAndMeridieum(objReportScheduleDetailViewModel);

            text = Constants.FormattedSchedule_Every + objReportScheduleDetailViewModel.Every + Constants.FormattedSchedule_Space + scheduledType + (objReportScheduleDetailViewModel.Every == 1 ? Constants.FormattedSchedule_Space : Constants.FormattedSchedule_s) + Constants.FormattedSchedule_At + objReportScheduleDetailViewModel.Hour + Constants.FormattedSchedule_Colon + objReportScheduleDetailViewModel.Minute.ToString().PadLeft(2,'0') + Constants.FormattedSchedule_Space + objReportScheduleDetailViewModel.Meridieum;

            return text;
        }
        /// <summary>
        /// FormattedCriteria: Function for showing single and multiple criteria.
        /// </summary>
        /// <param name="Criteria"></param>
        /// <returns></returns>
        private string FormattedCriteria(string Criteria)
        {
            string criteria = string.Empty;
            int count = 0;
            try
            {
                Dictionary<string, object> parameterList = JsonConvert.DeserializeObject<Dictionary<string, object>>(Criteria);
                foreach (KeyValuePair<string, object> parameter in parameterList)
                {
                    if (parameter.Value != null && parameter.Key != Constants.Report_Parameter_ReportScheduleTimeZone && parameter.Key != Constants.Report_Parameter_ReportDataTimeZone && parameter.Key != Constants.FormattedSchedule_QoskID && parameter.Key != Constants.Report_Parameter_FileType)
                    {
                        criteria = parameter.Key.ToString().Replace(Constants.Report_Parameter_Initial, string.Empty) + Constants.FormattedSchedule_Colon + parameter.Value;
                        count = count + 1;
                    }

                }
                if (count > 1)
                {
                    criteria = Constants.FormattedSchedule_MultipleCriteria;
                }
                else
                {
                    return criteria;
                }
                return criteria;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// DeleteReportSchedule: Delete records from Report Scheduler Grid 
        /// </summary>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        internal Response DeleteReportSchedule(int reportScheduleId)
        {
            Response objclsResponse = new Response();
            try
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {

                    ReportSchedule reportScheduleItem = (from reportSchedule in cloudModelEntities.ReportSchedule
                                                         where reportSchedule.ReportScheduleID == reportScheduleId
                                                         select reportSchedule).SingleOrDefault();

                    if (reportScheduleItem != null)
                    {
                        reportScheduleItem.IsDeleted = true;
                        int status = cloudModelEntities.SaveChanges();
                        if (status > 0)
                        {
                            objclsResponse.Status = Status.OK;
                        }
                        else
                        {
                            objclsResponse.Status = Status.Fail;
                        }

                    }
                    else
                    {
                        objclsResponse.Status = Status.Deleted;
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                objclsResponse.Status = Status.Fail;
            }
            return objclsResponse;
        }
        /// <summary>
        /// GetReportScheduleInformation: Set data on Edit UI
        /// </summary>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        public ReportScheduleDetailViewModel GetReportScheduleInformation(int reportScheduleId)
        {
            ReportScheduleDetailViewModel retailerBoxDetailReportViewModel = null;
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reportScheduleDetail = (from reportSchedule in cloudModelEntities.ReportSchedule
                                            where reportSchedule.ReportScheduleID == reportScheduleId
                                            select reportSchedule).FirstOrDefault();
                if (reportScheduleDetail != null)
                {
                    ReportSchedulerEntity objReportSchedulerEntity = JsonConvert.DeserializeObject<ReportSchedulerEntity>(reportScheduleDetail.Schedule);
                    RetailerProductDetailsEntity objRetailerProductDetailsEntity = JsonConvert.DeserializeObject<RetailerProductDetailsEntity>(reportScheduleDetail.Criteria);
                    if (objReportSchedulerEntity != null && objRetailerProductDetailsEntity != null)
                    {
                        retailerBoxDetailReportViewModel = new ReportScheduleDetailViewModel()
                        {
                            ScheduleType = objReportSchedulerEntity.ScheduleType,
                            FileType = objReportSchedulerEntity.FileType,
                            ReportID = reportScheduleDetail.ReportID,
                            ReportScheduleID = reportScheduleDetail.ReportScheduleID,
                            Every = objReportSchedulerEntity.Every,
                            SMSTo = objReportSchedulerEntity.SMSTo,
                            StartDate = objReportSchedulerEntity.StartDate != null ? SchedulerFunctions.ConvertUTCToTimeZoneDate(objReportSchedulerEntity.ReportScheduleTimeZone, objReportSchedulerEntity.StartDate).Value : objReportSchedulerEntity.StartDate,
                            EndDate = objReportSchedulerEntity.EndDate != null ? SchedulerFunctions.ConvertUTCToTimeZoneDate(objReportSchedulerEntity.ReportScheduleTimeZone, objReportSchedulerEntity.EndDate).Value : objReportSchedulerEntity.EndDate,
                            ReportScheduleTimeZone = objReportSchedulerEntity.ReportScheduleTimeZone,
                            ReportDataTimeZone = objRetailerProductDetailsEntity.ReportDataTimeZone,
                            ScheduleReportTitle = reportScheduleDetail.ReportScheduleTitle,
                            EmailTo = GetEmailAddress((from reportRecipient in cloudModelEntities.ReportRecipient where reportRecipient.ReportScheduleId == reportScheduleId select reportRecipient.RecipientEmail).Distinct().ToList()),
                            Hour = objReportSchedulerEntity.Hour,
                            Minute = objReportSchedulerEntity.Minute,
                            Meridieum = objReportSchedulerEntity.Meridieum
                        };
                        SetTimeAndMeridieum(retailerBoxDetailReportViewModel);

                    }
                }
                return retailerBoxDetailReportViewModel;
            }
        }

        /// <summary>
        /// GetEmailAddress: Set email address in Edit UI
        /// </summary>
        /// <param name="emailList"></param>
        /// <returns></returns>
        public string GetEmailAddress(List<string> emailList)
        {
            string emailAddress = string.Empty;
            if (emailList != null && emailList.Count > 0)
            {
                emailAddress = string.Join(Constants.Model_ReportDataAccess_Comma, emailList);
            }
            return emailAddress;
        }

        /// <summary>
        /// SetTimeAndMeridieum: Set AM, PM and date on Edit UI
        /// </summary>
        /// <param name="retailerBoxDetailReportViewModel"></param>
        public void SetTimeAndMeridieum(ReportScheduleDetailViewModel retailerBoxDetailReportViewModel)
        {
            if (retailerBoxDetailReportViewModel != null && retailerBoxDetailReportViewModel.StartDate != null)
            {
                int hour = retailerBoxDetailReportViewModel.StartDate.Value.Hour;
                if (hour >= 12)
                {
                    if (hour > 12)
                    {
                        hour = hour - 12;
                    }
                    retailerBoxDetailReportViewModel.Meridieum = Meridieum.PM;
                }
                else
                {
                    if (hour == 0)
                    {
                        hour = 12;
                    }
                    retailerBoxDetailReportViewModel.Meridieum = Meridieum.AM;
                }
                retailerBoxDetailReportViewModel.Hour = hour;
                retailerBoxDetailReportViewModel.Minute = retailerBoxDetailReportViewModel.StartDate.Value.Minute;
            }
        }
        /// <summary>
        /// GetReportName: Get Report Name from Report table.
        /// </summary>
        /// <param name="reportID"></param>
        /// <returns></returns>
        public string GetReportName(long reportID)
        {
            string name = string.Empty;

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reportName = (from report in cloudModelEntities.Report
                                  where report.ReportID == reportID && report.IsDeleted == false
                                  select report.ReportTitle).FirstOrDefault();
                name = reportName;
            }
            return name;
        }
        /// <summary>
        /// GetReportModule: Get Report Module from Report table.
        /// </summary>
        /// <param name="reportID"></param>
        /// <returns></returns>
        public string GetReportModule(long reportID)
        {
            string ReportModule = string.Empty;

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var Module = (from report in cloudModelEntities.Report
                              where report.ReportID == reportID
                              select report.ReportModule).FirstOrDefault();

                ReportModule = Module;
            }
            return ReportModule;
        }
        /// <summary>
        /// GetPanelLayoutId: Get layout id based on report name.
        /// </summary>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public List<ReportMasterLayoutList> GetPanelLayoutId(string reportName)
        {

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var query = (from reportLayoutTableMapping in cloudModelEntities.ReportLayoutTableMapping
                             join reportMasterTableList in cloudModelEntities.ReportMasterTableList on
                             reportLayoutTableMapping.TableId equals reportMasterTableList.ReportMasterTableListID
                             join reportMasterLayoutList in cloudModelEntities.ReportMasterLayoutList on
                             reportLayoutTableMapping.LayoutId equals reportMasterLayoutList.ReportMasterLayoutListID

                             where reportMasterTableList.TableName == reportName
                             select reportMasterLayoutList);
                return query.ToList();
            }
        }
        /// <summary>
        /// GetReportCriteria: Get Report Criteria based on reportScheduleId.
        /// </summary>
        /// <param name="reportScheduleId"></param>
        /// <returns></returns>
        public string GetReportCriteria(int reportScheduleId)
        {
            string criteria = string.Empty;

            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
                var reportCriteria = (from reportSchedule in cloudModelEntities.ReportSchedule
                                      where reportSchedule.ReportScheduleID == reportScheduleId
                                      select reportSchedule.Criteria).FirstOrDefault();

                criteria = reportCriteria;
            }
            return criteria;
        }
        /// <summary>
        /// GetDetailsUsingAutoCompleteBasedOnControlID: Get Details Auto Complete Based On ControlID
        /// </summary>
        /// <param name="terms"></param>
        /// <param name="controlID"></param>
        /// <returns></returns>
        internal ICollection<AutoCompleteEntity> GetDetailsUsingAutoCompleteBasedOnControlID(string terms, string controlID)
        {

            ICollection<AutoCompleteEntity> lstAutoCompleteDetails = null;
            try
            {
                using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
                {
                    switch (controlID)
                    {
                        case Constants.GetDetails_AutoComplete_VendorName:
                            {
                                lstAutoCompleteDetails = (from profile in cloudModelEntities.Profile.Where(p => p.Name.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = profile.ProfileCode.ToString(),
                                                              Text = profile.Name,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;

                            }
                        case Constants.GetDetails_AutoComplete_NDCNumber:
                            {
                                lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.NDC.ToString().Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = product.ProductID.ToString(),
                                                              Text = product.NDC.ToString(),
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_ProductDescription:
                            {
                                lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.Description.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = product.ProductID.ToString(),
                                                              Text = product.Description,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_LotNumber:
                            {
                                lstAutoCompleteDetails = (from lotNumber in cloudModelEntities.Lot.Where(p => p.LotNumber.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = lotNumber.LotNumberID.ToString(),
                                                              Text = lotNumber.LotNumber,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_StoreName:
                            {
                                lstAutoCompleteDetails = (from item in cloudModelEntities.Item
                                                          join qosk in cloudModelEntities.Qosk
                                                              on item.QoskID equals qosk.QoskID

                                                          join qoskprofile in cloudModelEntities.Profile
                                                          on qosk.ProfileCode equals qoskprofile.ProfileCode
                                                          where qoskprofile.Name.Contains(terms)
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = qoskprofile.ProfileCode.ToString(),
                                                              Text = qoskprofile.Name,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_OutofPolicyCode:
                            {
                                lstAutoCompleteDetails = (from item in cloudModelEntities.Item.Where(p => p.NonReturnableReason.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = item.ItemGUID.ToString(),
                                                              Text = item.NonReturnableReason,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_OutofpolicyCodeDescription:
                            {
                                lstAutoCompleteDetails = (from item in cloudModelEntities.Item.Where(p => p.NonReturnableReasonDetail.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = item.ItemGUID.ToString(),
                                                              Text = item.NonReturnableReasonDetail,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_Description:
                            {
                                lstAutoCompleteDetails = (from product in cloudModelEntities.Product.Where(p => p.Description.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = product.ProductID.ToString(),
                                                              Text = product.Description,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_SealedOpenCase:
                            {
                                lstAutoCompleteDetails = (from itemStateDict in cloudModelEntities.ItemStateDict.Where(p => p.Description.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = itemStateDict.Code,
                                                              Text = itemStateDict.Description,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                        case Constants.GetDetails_AutoComplete_ManufacturerName:
                            {
                                lstAutoCompleteDetails = (from profile in cloudModelEntities.Profile.Where(p => p.Name.Contains(terms))
                                                          select new AutoCompleteEntity
                                                          {
                                                              Value = profile.ProfileCode.ToString(),
                                                              Text = profile.Name,
                                                          }).Distinct().Take(15).ToList();

                                return lstAutoCompleteDetails;
                            }
                    }

                }
                return lstAutoCompleteDetails;
            }
            catch
            {

                return lstAutoCompleteDetails;
            }
        }


    }
}