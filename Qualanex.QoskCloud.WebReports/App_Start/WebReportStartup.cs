﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Qualanex.QoskCloud.WebReports.WebReportStartup))]
namespace Qualanex.QoskCloud.WebReports
{
    /// <summary>
    /// WebReportStartup
    /// </summary>
    public partial class WebReportStartup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
