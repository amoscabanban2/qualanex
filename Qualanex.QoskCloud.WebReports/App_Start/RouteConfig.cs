﻿using System.Web.Mvc;
using System.Web.Routing;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.WebReports
{
    /// <summary>
    /// RouteConfig
    /// </summary>
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute(Constants.RouteConfig_DefaulUrl);

            routes.MapRoute(
               name: Constants.RouteConfig_ReportUrl_Name,
               url: Constants.RouteConfig_ReportUrl_Url,
               defaults: new { controller = Constants.RouteConfig_ReportUrl_Controller, action = Constants.RouteConfig_ReportUrl_Action, ReportId = UrlParameter.Optional }
           );


        }
    }
}
