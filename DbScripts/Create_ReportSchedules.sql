USE [QIPSReporting]
GO

/****** Object:  Table [dbo].[ReportSchedules]    Script Date: 4/18/2019 10:00:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReportSchedules](
	[ReportScheduleID] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[UserID] [bigint] NOT NULL,
	[ScheduleName] [nvarchar](255) NOT NULL,
	[ReportUniqueName] [nvarchar](50) NOT NULL,
	[ScheduleTime] [time](0) NOT NULL,
	[ScheduleFrequency] [nvarchar](50) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[EmailTo] [nvarchar](255) NOT NULL,
	[TargetTimeZone] [nvarchar](100) NULL,
	[ReportFileType] [nvarchar](50) NOT NULL,
	[ScheduleParamsXml] [nvarchar](max) NULL,
	[ReportColumnsXml] [nvarchar](max) NULL,
	[ReportParamsXml] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO