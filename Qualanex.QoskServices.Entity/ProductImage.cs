﻿using System;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class ProductImage
   {
      [DataMember]
      public long ProductImageID { get; set; }

      [DataMember]
      public long ProductID { get; set; }

      [DataMember]
      public string FileName { get; set; }

      [DataMember]
      public byte ProductImageTypeDictID { get; set; }

      [DataMember]
      public byte ProductImageSourceDictID { get; set; }

      [DataMember]
      public byte ConditionDictID { get; set; }

      [DataMember]
      public byte ItemStateDictID { get; set; }

      [DataMember]
      public byte PackageOrContainerTypeDictID { get; set; }

      [DataMember]
      public int Version { get; set; }

      [DataMember]
      public bool IsDeleted { get; set; }

      [DataMember]
      public System.DateTime? EffectiveStartDate { get; set; }

      [DataMember]
      public System.DateTime? EffectiveEndDate { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public System.DateTime? CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public System.DateTime? ModifiedDate { get; set; }
   }
}
