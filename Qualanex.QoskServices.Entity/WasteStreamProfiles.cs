﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class WasteStreamProfiles
   {
      [DataMember]
      public long WasteStreamProfileID { get; set; }

      [DataMember]
      public string Description { get; set; }

      [DataMember]
      public int ProfileCode { get; set; }

      [DataMember]
      public string ApprovalCode { get; set; }

      [DataMember]
      public bool IsHazardous { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public System.DateTime CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public System.DateTime ModifiedDate { get; set; }

      [DataMember]
      public decimal? CostPerPound { get; set; }

      [DataMember]
      public decimal? MinimumPoundsPerPallet { get; set; }

      [DataMember]
      public decimal? MinimumPoundsPerDrum { get; set; }

      [DataMember]
      public decimal? MinimumPoundsPerBox { get; set; }

   }
}
