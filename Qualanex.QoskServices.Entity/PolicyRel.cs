﻿using System;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class PolicyRel
   {
      [DataMember]
      public int PolicyRelationID { get; set; }

      [DataMember]
      public string PolicyCode { get; set; }

      [DataMember]
      public int? ProfileCode { get; set; }

      [DataMember]
      public int ManufacturerProfileCode { get; set; }

      [DataMember]
      public long? ProductID { get; set; }

      [DataMember]
      public string LotNumber { get; set; }

      [DataMember]
      public int Version { get; set; }

      [DataMember]
      public bool IsDeleted { get; set; }

      [DataMember]
      public System.DateTime? EffectiveStartDate { get; set; }

      [DataMember]
      public System.DateTime? EffectiveEndDate { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public System.DateTime? CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public System.DateTime? ModifiedDate { get; set; }
   }
}
