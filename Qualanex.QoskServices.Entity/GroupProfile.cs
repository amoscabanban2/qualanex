﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.Qosk.ServiceEntities
{
     public class GroupProfile 
    {
        public long ID { get; set; }
        public int ProfileGroupID { get; set; }
        public int ProfileCode { get; set; }
        public string Created { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDateTime { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<bool> IsAssigned { get; set; }    
    }
}
