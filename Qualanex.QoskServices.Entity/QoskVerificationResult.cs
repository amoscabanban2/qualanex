﻿using System;
using System.Collections.Generic;
namespace Qualanex.Qosk.ServiceEntities
{
    public class QoskVerificationResult
    {
        public string QoskID { get; set; }
        public DateTime RequestedDate { get; set; }
        public bool IsValidQosk { get; set; }
        public List<Users> QoskUser { get; set; }
        public Qosk QoskDetail { get; set; }
        public string Message { get; set; }
    }
}
