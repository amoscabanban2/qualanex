﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.Qosk.ServiceEntities
{
   public class Qosk
   {
      public string QoskID { get; set; }
      public string MachineName { get; set; }
      public bool? Active { get; set; }
      public int ProfileCode { get; set; }
      public string CustomerSupportNumber { get; set; }
      public string CreatedBy { get; set; }
      public DateTime? CreatedDate { get; set; }
      public string ModifiedBy { get; set; }
      public DateTime? ModifiedDate { get; set; }
      public bool? IsVerified { get; set; }
      public DateTime? VerificationDate { get; set; }
      public DateTime? PasswordExpirationDate { get; set; }
   }
}
