﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class Products
   {
      [DataMember]
      public long ProductID { get; set; }

      [DataMember]
      public long? RollupProductID { get; set; }

      [DataMember]
      public int ProfileCode { get; set; }

      [DataMember]
      public DateTime? ProductStartDate { get; set; }

      [DataMember]
      public DateTime? ProductEndDate { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public DateTime? CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public DateTime? ModifiedDate { get; set; }

      [DataMember]
      public int? MFGLabelerCode { get; set; }

      [DataMember]
      public long? NDC { get; set; }

      [DataMember]
      public string NDCUPCWithDashes { get; set; }

      [DataMember]
      public long? UPC { get; set; }

      [DataMember]
      public string EDINumber { get; set; }

      [DataMember]
      public string MFGProductNumber { get; set; }

      [DataMember]
      public bool PhysicianSample { get; set; }

      [DataMember]
      public string PhysicianSampleProductNumber { get; set; }

      [DataMember]
      public decimal PackageSize { get; set; }

      [DataMember]
      public int UnitsPerPackage { get; set; }

      [DataMember]
      public int? CaseSize3 { get; set; }

      [DataMember]
      public int? CaseSize2 { get; set; }

      [DataMember]
      public int? CaseSize1 { get; set; }

      [DataMember]
      public string UnitOfMeasure { get; set; }

      [DataMember]
      public string Description { get; set; }

      [DataMember]
      public string DosageCode { get; set; }

      [DataMember]
      public byte ControlNumber { get; set; }

      [DataMember]
      public decimal? IndividualCountWeight { get; set; }

      [DataMember]
      public decimal? ContainerWeight { get; set; }

      [DataMember]
      public decimal? FullContainerWeightWithContents { get; set; }

      [DataMember]
      public string Strength { get; set; }

      [DataMember]
      public bool UnitDose { get; set; }

      [DataMember]
      public bool RXorOTC { get; set; }

      [DataMember]
      public string BrandorGeneric { get; set; }

      [DataMember]
      public string TherapeuticClass { get; set; }

      [DataMember]
      public string ShortSupply { get; set; }

      [DataMember]
      public bool TamperResistantSealExists { get; set; }

      [DataMember]
      public string SpecialHandlingInstructions { get; set; }

      [DataMember]
      public bool DEAWatchList { get; set; }

      [DataMember]
      public bool? ARCOSReportable { get; set; }

      [DataMember]
      public bool? Withdrawn { get; set; }

      [DataMember]
      public bool? ExcludeFromExternalInterface { get; set; }

      [DataMember]
      public bool? ExcludeFromDropDown { get; set; }

      [DataMember]
      public bool AlwaysSortToQuarantine { get; set; }

      [DataMember]
      public bool ForceQuantityCount { get; set; }

      [DataMember]
      public string ForceQuantityCountNotes { get; set; }

      [DataMember]
      public bool? IsRepackager { get; set; }

      [DataMember]
      public string WholesalerNumber { get; set; }

      [DataMember]
      public bool? Refrigerated { get; set; }

      [DataMember]
      public bool? DivestedByLot { get; set; }

      [DataMember]
      public int? DiverseManufacturerProfileCode { get; set; }

      [DataMember]
      public bool? IsDeleted { get; set; }

      [DataMember]
      public long Version { get; set; }

      [DataMember]
      public bool? IsOriginal { get; set; }

      [DataMember]
      public string PillType { get; set; }

   }
}
