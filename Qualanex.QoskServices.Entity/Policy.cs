﻿using System;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
    [DataContract]
    public class Policy
    {
        
        [DataMember]
        public long PolicyID { get; set; }
        [DataMember]
        public string PolicyName { get; set; }
        [DataMember]
        public string PolicyTypeName { get; set; }
        [DataMember]
        public Nullable<byte> PolicyType { get; set; }
        [DataMember]
        public Nullable<int> ProfileCode { get; set; }
        [DataMember]
        public Nullable<long> ProductID { get; set; }
        [DataMember]
        public Nullable<int> ManufacturerProfileCode { get; set; }
        [DataMember]
        public Nullable<int> RepackagerProfileCode { get; set; }
        [DataMember]
        public Nullable<int> WholesalerProfileCode { get; set; }
        [DataMember]
        public string LotNumber { get; set; }
        [DataMember]
        public Nullable<bool> DirectAccount { get; set; }
        [DataMember]
        public Nullable<System.DateTime> PolicyStartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> PolicyEndDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public string AuthorizedForCreditConsideration { get; set; }
        [DataMember]
        public Nullable<byte> NumberOfDaysToReturnProductReceivedInError { get; set; }
        [DataMember]
        public Nullable<bool> ExpiresOnFirstOfCurrentMonth { get; set; }
        [DataMember]
        public Nullable<byte> NumberOfMonthsAfterExpirationReturnable { get; set; }
        [DataMember]
        public Nullable<byte> NumberOfMonthsBeforeExpirationReturnableSealed { get; set; }
        [DataMember]
        public Nullable<byte> NumberOfMonthsBeforeExpirationReturnableOpened { get; set; }
        [DataMember]
        public Nullable<bool> BrokenSealReturnable { get; set; }
        [DataMember]
        public string PartialsReturnable { get; set; }
        [DataMember]
        public string StateLawPartialOverride { get; set; }
        [DataMember]
        public Nullable<decimal> PartialPercent1 { get; set; }
        [DataMember]
        public Nullable<decimal> CreditPercent1 { get; set; }
        [DataMember]
        public Nullable<decimal> PartialPercent2 { get; set; }
        [DataMember]
        public Nullable<decimal> CreditPercent2 { get; set; }
        [DataMember]
        public Nullable<decimal> PartialPercent3 { get; set; }
        [DataMember]
        public Nullable<decimal> CreditPercent3 { get; set; }
        [DataMember]
        public Nullable<decimal> MaximumPartialCredit { get; set; }
        [DataMember]
        public bool PrescriptionVialsReturnable { get; set; }
        [DataMember]
        public bool RepackagedIntoUnitDoseReturnable { get; set; }
        [DataMember]
        public Nullable<bool> MissingLotNumberorExpDateNotReturnable { get; set; }
        [DataMember]
        public Nullable<decimal> CustomerReimbursementAmount { get; set; }
        [DataMember]
        public string UseDebitPricing { get; set; }
        [DataMember]
        public bool DoNotUseFallThroughPrice { get; set; }
        [DataMember]
        public short MatchAccuracyPrecision { get; set; }
        [DataMember]
        public decimal MinimumDollarAmountReturnable { get; set; }
        [DataMember]
        public bool UseWholeSalerDirectPriceWhenNoIndirect { get; set; }
        [DataMember]
        public bool AllowQuantityOverPackageSize { get; set; }
        [DataMember]
        public Nullable<decimal> OrderErrorAdministrativeFee { get; set; }
        [DataMember]
        public Nullable<decimal> HandlingFee { get; set; }
        [DataMember]
        public Nullable<decimal> HandlingFeeExceptionForStatePartialPolicyLaw { get; set; }
        [DataMember]
        public Nullable<decimal> CreditIssuedAtWACLess { get; set; }
        [DataMember]
        public Nullable<decimal> CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw { get; set; }
        [DataMember]
        public string NonDirectCreditIssuedBy { get; set; }
        [DataMember]
        public Nullable<int> ReturnAuthorizationExpirationDays { get; set; }
        [DataMember]
        public Nullable<decimal> ReturnAuthorizationCompletionPercent { get; set; }
        [DataMember]
        public Nullable<bool> OverstockApprovalRequired { get; set; }
        [DataMember]
        public Nullable<bool> Form41Allowed { get; set; }
        [DataMember]
        public Nullable<bool> AcceptNonReturnableProductForDestruction { get; set; }
         [DataMember]
        public string InnerPack { get; set; }
        [DataMember]
        public decimal ReturnAuthorizationApprovalThreshold { get; set; }
        [DataMember]
        public bool PriorReturnAuthorizationRequired { get; set; }
        [DataMember]
        public Nullable<bool> IsDeleted { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public long Version { get; set; }

        
    }
}
