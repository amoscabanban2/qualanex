﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
    [DataContract]
    public class Recalls
    {
        
        [DataMember]
        public long RecallID { get; set; }
        [DataMember]
        public Nullable<System.DateTime> RecallStartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> RecallEndDate { get; set; }
        [DataMember]
        public Nullable<int> RecallNumber { get; set; }
        [DataMember]
        public int ManufacturerProfileCode { get; set; }
        [DataMember]
        public Nullable<decimal> ShippingAndHandling { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreditEndDate { get; set; }
        [DataMember]
        public bool NotifyDirectAccounts { get; set; }
        [DataMember]
        public bool NotifyAllClassesOfTrade { get; set; }
        [DataMember]
        public string RecallClass { get; set; }
        [DataMember]
        public string ReimbusementProvider { get; set; }
        [DataMember]
        public bool AllowReimbursementOverShippingAllowance { get; set; }
        [DataMember]
        public bool ApprovedForDestruction { get; set; }
        [DataMember]
        public bool QualanexManaging { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<bool> CaptureBRCCases { get; set; }
        [DataMember]
        public Nullable<bool> CaptureBRCOpenContainers { get; set; }
        [DataMember]
        public Nullable<bool> CaptureBRCSealedContainers { get; set; }
        [DataMember]
        public Nullable<bool> CaptureBRCContentsCount { get; set; }
        [DataMember]
        public Nullable<bool> IsActiveRecalll { get; set; }
        
    }
}
