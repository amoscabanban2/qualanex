﻿using System;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class ProductWasteStreamProfiles
   {
      [DataMember]
      public long ProductsWasteStreamProfileID { get; set; }

      [DataMember]
      public long WasteStreamProfileID { get; set; }

      [DataMember]
      public long ProductID { get; set; }

      [DataMember]
      public int Priority { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public System.DateTime CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public System.DateTime ModifiedDate { get; set; }

   }
}
