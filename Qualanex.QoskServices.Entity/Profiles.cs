﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
   [DataContract]
   public class Profiles
   {

      [DataMember]
      public int ProfileCode { get; set; }

      [DataMember]
      public string Name { get; set; }

      [DataMember]
      public string Type { get; set; }

      [DataMember]
      public byte DisplayOrder { get; set; }

      [DataMember]
      public byte[] Logo { get; set; }

      [DataMember]
      public string DEANumber { get; set; }

      [DataMember]
      public DateTime? DEAExpirationDate { get; set; }

      [DataMember]
      public string EmailAddress { get; set; }

      [DataMember]
      public string Address1 { get; set; }

      [DataMember]
      public string Address2 { get; set; }

      [DataMember]
      public string Address3 { get; set; }

      [DataMember]
      public string City { get; set; }

      [DataMember]
      public string State { get; set; }

      [DataMember]
      public string RegionCode { get; set; }

      [DataMember]
      public string ZipCode { get; set; }

      [DataMember]
      public string PhoneNumber { get; set; }

      [DataMember]
      public string ContactFirstName { get; set; }

      [DataMember]
      public string ContactLastName { get; set; }

      [DataMember]
      public string ContactPhoneNumber { get; set; }

      [DataMember]
      public string ContactEmailAddress { get; set; }

      [DataMember]
      public string ContactFaxNumber { get; set; }

      [DataMember]
      public byte[] PolicySnapshot { get; set; }

      [DataMember]
      public bool DirectAccountsOnly { get; set; }

      [DataMember]
      public int? RollupProfileCode { get; set; }

      [DataMember]
      public string TollFreePhoneNumber { get; set; }

      [DataMember]
      public string Notes { get; set; }

      [DataMember]
      public string ReturnProcedures { get; set; }

      [DataMember]
      public string RepresentativeNumber { get; set; }

      [DataMember]
      public string WholesalerAccountNumber { get; set; }

      [DataMember]
      public string FaxNumber { get; set; }

      [DataMember]
      public string LotNumberExpirationDateValidationMethod { get; set; }

      [DataMember]
      public string CreatedBy { get; set; }

      [DataMember]
      public System.DateTime CreatedDate { get; set; }

      [DataMember]
      public string ModifiedBy { get; set; }

      [DataMember]
      public System.DateTime ModifiedDate { get; set; }

      [DataMember]
      public bool AlertPendingReturnAuthorizations { get; set; }

      [DataMember]
      public bool AlertCompletedReturnAuthorizations { get; set; }

      [DataMember]
      public string Status { get; set; }

      [DataMember]
      public long? VendorID { get; set; }

      [DataMember]
      public DateTime? VendorLoadDate { get; set; }

      [DataMember]
      public string SalesOrg { get; set; }

      [DataMember]
      public bool? QualanexComDisplay { get; set; }

      [DataMember]
      public string PDMACoordinatorEmailAddress { get; set; }

      [DataMember]
      public string PDMACoordinatorEmailAddressProcessCompleted { get; set; }

      [DataMember]
      public long Version { get; set; }

   }
}
