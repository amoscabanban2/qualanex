﻿using System;
using System.Runtime.Serialization;

namespace Qualanex.Qosk.ServiceEntities
{
    [DataContract]
    public class LotNumbers
    {
        [DataMember]
        public long LotNumbersID { get; set; }
        [DataMember]
        public Nullable<long> ProductID { get; set; }
        [DataMember]
        public string LotNumber { get; set; }
        [DataMember]
        public Nullable<int> RepackagerProfileCode { get; set; }
        [DataMember]
        public Nullable<int> DiverseManufacturerProfileCode { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        [DataMember]
        public Nullable<long> RecallID { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public Nullable<bool> NoReturnsAllowed { get; set; }
        [DataMember]
        public Nullable<bool> IsDeleted { get; set; }
        [DataMember]
        public long Version { get; set; }

    }
}
