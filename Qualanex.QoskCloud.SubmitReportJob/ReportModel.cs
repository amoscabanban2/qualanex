﻿using Newtonsoft.Json;
using System;
using Qualanex.QoskCloud.Entity;
using System.Linq;
using Qualanex.QoskCloud.Utility;
using System.Collections.Generic;
using Qualanex.QoskCloud.Utility.Common;
using System.IO;
using Qualanex.Qosk.Library.Common.CodeCorrectness;
using Qualanex.Qosk.Library.Model.DBModel;
using Qualanex.Qosk.Library.Model.QoskCloud;

namespace Qualanex.QoskCloud.SubmitReportJob
{
   public class ReportModel
   {
      TextWriter log;
      public ReportModel(TextWriter Log)
      {
         this.log = Log;

      }

      /// <summary>
      /// Get all schedule information from report schedule Table
      /// </summary>
      /// <returns></returns>
      public void GetSchedulingInformation()
      {
         Console.WriteLine("Entered in method: GetSchedulingInformation");

         try
         {
            using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               DateTime CurrentUTCDateTime = DateTime.UtcNow;
               Console.WriteLine("Scheduled started for UTC datetime: " + CurrentUTCDateTime);
               Console.WriteLine("Fetching available report schedule from database table.");

               foreach (ReportSchedule objReportSchedule in cloudModelEntities.ReportSchedule)
               {
                  if (objReportSchedule != null && !string.IsNullOrEmpty(objReportSchedule.Schedule)
                      && !objReportSchedule.Schedule.Replace(Constants.Single_Space, string.Empty).Trim().Equals(Constants.Json_Braces) && !objReportSchedule.IsDeleted)
                  {
                     ReportSchedulerEntity objReportSchedulerEntity = JsonConvert.DeserializeObject<ReportSchedulerEntity>(objReportSchedule.Schedule);

                     if (objReportSchedulerEntity != null)
                     {
                        if (Runnable(objReportSchedulerEntity, objReportSchedule.LastRunTime, CurrentUTCDateTime))
                        {
                           Console.WriteLine("Starting report generation for ReportScheduleId: " + objReportSchedule.ReportScheduleID);
                           ProcessRunnableReport(objReportSchedulerEntity, objReportSchedule, CurrentUTCDateTime);
                           Console.WriteLine("Completed report generation for ReportScheduleId: " + objReportSchedule.ReportScheduleID);
                        }
                     }
                  }
               }

               Console.WriteLine("Scheduled ended for UTC datetime: " + CurrentUTCDateTime);
            }
         }
         catch (System.Exception ex)
         {
            Console.WriteLine("Error occured:" + ex.Message);
            throw;
         }
         finally
         {
            Console.WriteLine("Out from method: GetSchedulingInformation");
         }

      }

      bool Runnable(ReportSchedulerEntity objReportSchedulerEntity, DateTime? lastRunTime, DateTime CurrentUTCDateTime)
      {
         bool timeForNextRun;

         if (lastRunTime != null)
         {
            var bufferedTime = CurrentUTCDateTime.AddMinutes(Constants.DataRange_Buffer);

            switch (objReportSchedulerEntity.ScheduleType)
            {
               case ScheduleType.Hour:
                  timeForNextRun = lastRunTime.Value.AddHours(objReportSchedulerEntity.Every) < bufferedTime;
                  break;
               case ScheduleType.Day:
                  timeForNextRun = lastRunTime.Value.AddDays(objReportSchedulerEntity.Every) < bufferedTime;
                  break;
               case ScheduleType.Week:
                  timeForNextRun = lastRunTime.Value.AddDays(objReportSchedulerEntity.Every * Constants.DataRange_Weekly) < bufferedTime;
                  break;
               case ScheduleType.Month:
                  timeForNextRun = lastRunTime.Value.AddDays(objReportSchedulerEntity.Every * Constants.DataRange_Monthly) < bufferedTime;
                  break;
               default:
                  return false;
            }
         }
         else
         {
            timeForNextRun = true;
         }

         return (timeForNextRun &&
                 CurrentUTCDateTime >= (objReportSchedulerEntity.StartDate ?? CurrentUTCDateTime.AddDays(-1)) &&
                 CurrentUTCDateTime <= (objReportSchedulerEntity.EndDate ?? CurrentUTCDateTime.AddDays(1)));
      }

      /// <summary>
      /// Verify and Add Hourly report into report run table.
      /// </summary>
      /// <param name="objReportSchedulerEntity"></param>
      /// <param name="objReportSchedule"></param>
      /// <param name="CurrentUTCDateTime"></param>
      void ProcessRunnableReport(ReportSchedulerEntity objReportSchedulerEntity, ReportSchedule objReportSchedule, DateTime CurrentUTCDateTime)
      {
         Console.WriteLine("Entered in method: ProcessRunnableReport");
         Console.WriteLine("Batch report started for schedule type: " + objReportSchedulerEntity.ScheduleType.ToString() + "and ReportScheduleId:" + objReportSchedule.ReportScheduleID);

         using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            //Report will be generated on the basis of schedule type
            //Hour: Data on the report will be within (one hour + every) range, i.e. if every=2 then data on report will be (every*hour) 2 hours back from current date.
            //Day: Data on the report will be within (one day + every) range, i.e. if every=2 then data on report will be (every*day [1 day]) 2 days back from current date.
            //Week: Data on the report will be within (one week + every) range, i.e. if every=2 then data on report will be (every*week [7 days] ) 14 days back from current date.
            //Month: Data on the report will be within (one month + every) range, i.e. if every=2 then data on report will be (every*month[30 days]) 2 months back from current date.
            DateTime DataStartDate = new DateTime();
            DateTime DataEndDate = CurrentUTCDateTime;

            switch (objReportSchedulerEntity.ScheduleType)
            {
               case ScheduleType.Hour:
                  DataStartDate = CurrentUTCDateTime.AddHours(-objReportSchedulerEntity.Every);
                  break;
               case ScheduleType.Day:
                  DataStartDate = CurrentUTCDateTime.AddDays(-objReportSchedulerEntity.Every * Constants.DataRange_Daily);
                  break;
               case ScheduleType.Week:
                  DataStartDate = CurrentUTCDateTime.AddDays(-objReportSchedulerEntity.Every * Constants.DataRange_Weekly);
                  break;
               case ScheduleType.Month:
                  DataStartDate = CurrentUTCDateTime.AddDays(-objReportSchedulerEntity.Every * Constants.DataRange_Monthly);
                  break;
            }

            objReportSchedule.Criteria = AddDataRangeInCriteria(objReportSchedule.Criteria, DataStartDate, DataEndDate);
            ProcessScheduleReport(objReportSchedule.ReportScheduleID, objReportSchedule.Criteria, CurrentUTCDateTime, objReportSchedulerEntity.FileType);
         }

         Console.WriteLine("Batch report ended for schedule type: " + objReportSchedulerEntity.ScheduleType.ToString() + "and ReportScheduleId:" + objReportSchedule.ReportScheduleID);
         Console.WriteLine("Out from method: ProcessRunnableReport");
      }

      /// <summary>
      /// Add schedule report into report run table and add same report in queue.
      /// </summary>
      /// <param name="ReportScheduleID"></param>
      /// <param name="Criteria"></param>
      /// <param name="StartDateTime"></param>
      void ProcessScheduleReport(long ReportScheduleID, string Criteria, DateTime StartDateTime, FileType filetype)
      {
         Console.WriteLine("Entered in method: ProcessScheduleReport");

         using (var cloudModelEntities = new Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            if (ReportScheduleID > 0)
            {
               //Update Report Schedule Table.
               ReportSchedule reportScheduleEntity = (from report in cloudModelEntities.Report
                                                      join reportSchedule in cloudModelEntities.ReportSchedule
                                                      on report.ReportID equals reportSchedule.ReportID
                                                      where reportSchedule.ReportScheduleID == ReportScheduleID
                                                      select reportSchedule).FirstOrDefault();
               if (reportScheduleEntity != null)
               {
                  ReportRun reportRun = new ReportRun();
                  reportRun.ReportRunGuid = Guid.NewGuid();
                  reportRun.ReportScheduleID = ReportScheduleID;
                  reportRun.RunCriteria = Criteria;
                  cloudModelEntities.ReportRun.Add(reportRun);
                  cloudModelEntities.SaveChanges();
                  Console.WriteLine("Report run item inserted for ReportRunGuid: " + reportRun.ReportScheduleID);

                  reportScheduleEntity.LastRunTime = StartDateTime;
                  cloudModelEntities.SaveChanges();
                  Console.WriteLine("ReportSchedule table is updated with lastRuntime for ReportScheduleID: " + reportRun.ReportRunGuid);

                  //Get all the email for this schedule from the database.
                  List<string> objReportRecipientEmails = (from reportRecipient in cloudModelEntities.ReportRecipient
                                                           where reportRecipient.ReportScheduleId == reportScheduleEntity.ReportScheduleID
                                                           select reportRecipient.RecipientEmail).ToList();

                  //Populating ReportRequest object to add into queue.
                  ReportRequest objReportRequest = new ReportRequest();
                  objReportRequest.ReportID = reportScheduleEntity.ReportID.Value;
                  var name = (from report in cloudModelEntities.Report
                              where report.ReportID == objReportRequest.ReportID
                              select report.ReportTitle).SingleOrDefault();
                  objReportRequest.ReportName = name == "Sample_Report_Retailer_Product_Detail"
                     ? "Product Details"
                     : name;
                  objReportRequest.ReportModule = (from report in cloudModelEntities.Report
                                                   where report.ReportID == objReportRequest.ReportID
                                                   select report.ReportModule).SingleOrDefault();
                  objReportRequest.ReportRunGuid = reportRun.ReportRunGuid;
                  objReportRequest.ReportScheduleTitle = reportScheduleEntity.ReportScheduleTitle;
                  objReportRequest.Criteria = reportRun.RunCriteria;
                  objReportRequest.UserEmailList = objReportRecipientEmails;
                  objReportRequest.FileType = filetype;
                  objReportRequest.UserID = (from reportSchedule in cloudModelEntities.ReportSchedule
                                             where reportSchedule.ReportScheduleID == ReportScheduleID
                                             select reportSchedule.UserID).SingleOrDefault();

                  //Add ReportRequest object into the ReportRequestQueue.
                  Console.WriteLine("Report request is being added to the Queue named:ReportRequestQueue for ReportRunGuid: " + reportRun.ReportRunGuid);
                  AddInNotificationQueue(objReportRequest);
                  Console.WriteLine("Report request is added to the Queue named:ReportRequestQueue for ReportRunGuid: " + reportRun.ReportRunGuid);
               }
            }
         }

         Console.WriteLine("Out from method: ProcessScheduleReport");
      }

      /// <summary>
      /// Add ReportRequest object in to the ReportQueue
      /// </summary>
      /// <param name="objReportRequest"></param>
      void AddInNotificationQueue(ReportRequest objReportRequest)
      {
         Console.WriteLine("Entered in method: AddInNotificationQueue");
         QueueResource.AddItemInQueue(Constants.AzureAccount_KioskSync, Constants.Azure_Queue_ReportRequestQueue, objReportRequest);
         Console.WriteLine("Our from method: AddInNotificationQueue");
      }
      /// <summary>
      /// Add data start date and end date in criteria.
      /// </summary>
      /// <param name="criteria"></param>
      /// <param name="dataStartDate"></param>
      /// <param name="dataEndDate"></param>
      /// <returns></returns>
      string AddDataRangeInCriteria(string criteria, DateTime dataStartDate, DateTime dataEndDate)
      {
         Console.WriteLine("Entered in method: AddDataRangeInCriteria");

         if (!string.IsNullOrEmpty(criteria))
         {
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(criteria);
            jsonObj[Constants.Report_Parameter_DataStartDate] = dataStartDate;
            jsonObj[Constants.Report_Parameter_DataEndDate] = dataEndDate;
            criteria = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj);
         }

         Console.WriteLine("Out from method: AddDataRangeInCriteria");
         return criteria;
      }

      /// <summary>
      /// Calculate the next runtime of a report.
      /// </summary>
      /// <param name="LastRunTime"></param>
      /// <param name="CurrentUTCTime"></param>
      /// <param name="Every"></param>
      /// <param name="scheduleType"></param>
      /// <returns></returns>
      DateTime? GetNextRunTime(DateTime LastRunTime, DateTime CurrentUTCTime, double Every, ScheduleType scheduleType)
      {
         Console.WriteLine("Entered in method: GetNextRunTime");
         DateTime? NextRunTime = CurrentUTCTime;

         if (scheduleType == ScheduleType.Hour)
         {
            DateTime CheckNextRunTime = new DateTime();

            CheckNextRunTime = LastRunTime.AddHours(Every);

            if (CheckNextRunTime <= CurrentUTCTime)
            {
               int hour = GetHourIn24HrsClockFormatFromDate(CurrentUTCTime, CurrentUTCTime.ToString("tt"));
               CurrentUTCTime = new DateTime(CurrentUTCTime.Year, CurrentUTCTime.Month, CurrentUTCTime.Day, hour, LastRunTime.Minute, 0);
               NextRunTime = CurrentUTCTime;
            }
            else
            {
               NextRunTime = null;
            }
         }
         else if (scheduleType == ScheduleType.Day)
         {
            NextRunTime = CalculateNextRunTime(LastRunTime, CurrentUTCTime, Every);
         }
         else if (scheduleType == ScheduleType.Week)
         {
            NextRunTime = CalculateNextRunTime(LastRunTime, CurrentUTCTime, (Every * Constants.DataRange_Weekly));
         }
         else if (scheduleType == ScheduleType.Month)
         {
            NextRunTime = CalculateNextRunTime(LastRunTime, CurrentUTCTime, (Every * Constants.DataRange_Monthly));
         }

         Console.WriteLine("Out from method: GetNextRunTime");
         return NextRunTime;

      }

      /// <summary>
      /// Calculate NextRunTime for report
      /// </summary>
      /// <param name="LastRunTime"></param>
      /// <param name="CurrentUTCTime"></param>
      /// <param name="CalculatedOnDays"></param>
      /// <returns></returns>
      DateTime? CalculateNextRunTime(DateTime LastRunTime, DateTime CurrentUTCTime, double CalculatedOnDays)
      {
         Console.WriteLine("Entered in method: CalculateNextRunTime");
         double dateDiff = (CurrentUTCTime.Date - LastRunTime.Date).TotalDays;
         DateTime? NextRunTime = null;
         DateTime CheckNextRunTime = LastRunTime.AddDays(dateDiff);//To check time

         if (CalculatedOnDays <= dateDiff && CheckNextRunTime <= CurrentUTCTime) // to check "Every" field.
         {
            int hour = GetHourIn24HrsClockFormatFromDate(CheckNextRunTime, CheckNextRunTime.ToString("tt"));
            CurrentUTCTime = new DateTime(CurrentUTCTime.Year, CurrentUTCTime.Month, CurrentUTCTime.Day, hour, LastRunTime.Minute, 0);
            NextRunTime = CurrentUTCTime;
         }

         Console.WriteLine("Out from method: CalculateNextRunTime");
         return NextRunTime;
      }

      /// <summary>
      /// Set date time into schedule timezone and then convert into UTC time zone.
      /// </summary>
      /// <param name="Utcdatetime"></param>
      /// <param name="Timezone"></param>
      /// <param name="Hour"></param>
      /// <param name="Minute"></param>
      /// <param name="Meridieum"></param>
      /// <returns></returns>
      DateTime? SetTimezoneDate(DateTime Utcdatetime, string Timezone, int Hour, int Minute, Meridieum Meridieum)
      {
         Console.WriteLine("Entered in method: SetTimezoneDate");
         //Convert utc date time in schedule timezone date time
         DateTime? timezoneDateTime = SchedulerFunctions.ConvertUTCToTimeZoneDate(Timezone, Utcdatetime);

         if (timezoneDateTime != null)
         {
            //Set schedule hour and minute into schedule timezone date time.
            int hour = GetHourIn24HrsClockFormatFromDate(Hour, Meridieum.ToString());
            DateTime setStoredDateTime = timezoneDateTime.Value.Date + new TimeSpan(hour, Minute, 0);

            //Convert schedule time zone into utc time zone.
            return SchedulerFunctions.ConvertTimeZoneDateTimeToUTC(Timezone, setStoredDateTime);
         }

         Console.WriteLine("Out from method: SetTimezoneDate");
         return null;
      }

      /// <summary>
      /// Get Hour from a date in 24hrs clock format
      /// </summary>
      /// <param name="date"></param>
      /// <param name="meridieum"></param>
      /// <returns></returns>
      int GetHourIn24HrsClockFormatFromDate(DateTime date, string meridieum)
      {
         return GetHourIn24HrsClockFormatFromDate(date.Hour, meridieum);
      }

      /// <summary>
      /// Get Hour from a date in 24hrs clock format
      /// </summary>
      /// <param name="hour"></param>
      /// <param name="meridieum"></param>
      /// <returns></returns>
      int GetHourIn24HrsClockFormatFromDate(int hour, string meridieum)
      {
         return DateTime.Parse((hour + meridieum).ToString()).Hour;
      }

   }
}