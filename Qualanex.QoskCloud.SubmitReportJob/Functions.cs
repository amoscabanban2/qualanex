﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;
namespace Qualanex.QoskCloud.SubmitReportJob
{
    public static class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        [NoAutomaticTrigger]
        public static void RunReportSchedule(TextWriter log)
        {
           try
           {
              Console.WriteLine("RunReportSchedule started");
              ReportModel reportModel = new ReportModel(log);
              Console.WriteLine("ReportModel Initialized");
              reportModel.GetSchedulingInformation();
              Console.WriteLine("RunReportSchedule finished");
           }
           catch (System.Exception ex)
           {
              Console.WriteLine("Error = {0}", ex.Message);
           }
           finally
           {
              Console.WriteLine("Finally Block");
           }
        }
    }
}
