﻿namespace Qualanex.QoskCloud.Entity
{
    public class UserType
    {
        public string UserTypeCode { get; set; }
        public string UserTypeName { get; set; }

    }
}
