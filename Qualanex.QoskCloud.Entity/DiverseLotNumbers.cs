﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class DiverseLotNumbers
    {
        public int ProfileCode { get; set; }
        public long ProductID { get; set; }
        public string Name { get; set; }
        public List<LotNumbersDataEntity> LotNumbersList { get; set; }

    }
}
