﻿namespace Qualanex.QoskCloud.Entity
{   
   public class LeftMenuLinks
    {      
        public string PageName { get; set; }     
        /// <summary>
        /// this class is given by the client architect we dont need to modify that.
        /// </summary>
        public string PageURL { get; set; }         
        /// <summary>
        /// this class is given by the client architect we dont need to modify that.
        /// </summary>
        public string PageURLClass { get; set; } 
        public string Application { get; set; }       
        public int ParentMenuID { get; set; }       
        public string IsActive { get; set; } 
    }
}
