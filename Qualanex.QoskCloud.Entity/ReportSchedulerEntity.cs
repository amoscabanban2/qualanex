﻿using Qualanex.QoskCloud.Utility;
using System;

namespace Qualanex.QoskCloud.Entity
{
    public class ReportSchedulerEntity
    {
        public ScheduleType ScheduleType { get; set; }
        public double Every { get; set; }
        public FileType FileType { get; set; }       
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public Meridieum Meridieum { get; set; }
        public string SMSTo { get; set; }
        public string ReportScheduleTimeZone { get; set; }

    }
}
