﻿using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class ProductsEntity
    {
        public List<ProductsML> lstManufactureProduct { get; set; }
        public List<ProductsML> lstLotProduct { get; set; }
        public List<Dosage> lstProductDosageType { get; set; }
        public List<string> ColumnName { get; set; }
        private int _SelectedRecords;
        public int SelectedRecords
        {
            get
            {
                if (_SelectedRecords == default(Int32))
                {
                    return 10;
                }
                else
                {
                    return _SelectedRecords;
                }
            }
            set
            {
                _SelectedRecords = value;
            }
        }
        public DynamicControlEntity ControlListing { get; set; }
        /// <summary>
        /// Property that will Hold the List of Subscribed/Default Column
        /// </summary>
        public ColumnListEntity ColumnListing { get; set; }

        /// <summary>
        /// Property that will Hold the List of Subscribed/Default Column
        /// </summary>
        public ColumnListEntity GridColumnListing { get; set; }
        public int? CurrentPageNo { get; set; }
    }
}
