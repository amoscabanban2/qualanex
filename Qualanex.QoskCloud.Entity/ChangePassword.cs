﻿using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
    public class ChangeUserPassword
    {
        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = Constants.Model_FormattedString_Message_String_Must_Be_Character_Long, MinimumLength = 6)]
        [Display(Name = Constants.Model_Current_Password)]
        [RegularExpression(Constants.Model_RegularExpression_Password, ErrorMessage = Constants.Model_Message_Password_Must_Contains)]
        public string OldPassword { get; set; }
              
        public long UserID { get; set; }
       
        [Required]
        [StringLength(20, ErrorMessage = Constants.Model_FormattedString_Message_String_Must_Be_Character_Long, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = Constants.Model_New_Password)]
        [RegularExpression(Constants.Model_RegularExpression_Password, ErrorMessage = Constants.Model_Message_Password_Must_Contains)]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = Constants.Model_Confirm_New_Password)]
        [Compare(Constants.Model_New_Password, ErrorMessage = Constants.Model_Message_New_Confirm_Password_Not_Matched)]
        public string ConfirmPassword { get; set; }

        public string UserInfo { get; set; } 
       
    }
}
