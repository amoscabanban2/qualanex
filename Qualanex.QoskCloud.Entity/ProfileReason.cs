﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProfileReason
    {
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public bool IsAssigned { get; set; }
    }
}
