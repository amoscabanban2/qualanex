﻿using System;

namespace Qualanex.QoskCloud.Entity
{
   public class ProductWasteInfo
   {
      public string WasteClassification { get; set; }
      public string WasteStream { get; set; }
      public string Highlights { get; set; }
      public string AdditionalInformation { get; set; }
   }
}