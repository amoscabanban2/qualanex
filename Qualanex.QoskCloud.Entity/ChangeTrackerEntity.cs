﻿using System;
namespace Qualanex.QoskCloud.Entity
{
    public class ChangeTrackerEntity
    {
        public string PrimaryColumnId { get; set; }

        public string TableName { get; set; }

        public string ColumnName { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public DateTime CurrentDateTime { get; set; }

        public string OperationType { get; set; }

        public long Version { get; set; }
    }
}
