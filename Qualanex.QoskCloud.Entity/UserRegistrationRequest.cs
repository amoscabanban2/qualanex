﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
   public class UserRegistrationRequest
   {
      [Key]
      public long? UserID { get; set; }

      [Required(ErrorMessage = Constants.Model_Please_Select_Type)]
      public string ddType { get; set; }

      [Display(Name = Constants.Model_Profile_Name)]
      public string Name { get; set; }

      [Display(Name = Constants.Model_User_Name)]
      public string UserName { get; set; }

      [Required]
      [StringLength(20, MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = Constants.Model_Password)]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = Constants.Model_Confirm_Password)]
      [System.ComponentModel.DataAnnotations.CompareAttribute(Constants.Model_Password, ErrorMessage = Constants.Model_Password_Not_Matched)]
      public string ConfirmPassword { get; set; }

      [Display(Name = Constants.Model_First_Name)]
      public string FirstName { get; set; }

      [Display(Name = Constants.Model_Last_Name)]
      public string LastName { get; set; }

      [Display(Name = Constants.Model_Profile_Name)]
      [Required(ErrorMessage = Constants.Model_Please_Select_Profile_Name)]
      public int? ProfileCode { get; set; }

      [Display(Name = Constants.Model_Email_Address)]
      public string Email { get; set; }

      public string CreatedBy { get; set; }
      public DateTime? CreatedDate { get; set; }
      public string ModifiedBy { get; set; }
      public DateTime? ModifiedDate { get; set; }
      public bool? Enabled { get; set; }

      [Display(Name = Constants.Model_Phone_Number)]
      public string PhoneNumber { get; set; }

      [Display(Name = Constants.Model_Fax_Number)]
      public string FaxNumber { get; set; }

      [Required(ErrorMessage = Constants.Model_Please_Select_User_Type)]
      public string UserTypeCode { get; set; }

      public string UserProfileType { get; set; }

      [Display(Name = Constants.Model_User_Type)]
      public string UserTypeName { get; set; }

      public bool IsTemporaryPassword { get; set; }
      public virtual ICollection<MLGroup> MLUserGroups { get; set; }
      public int? RollupProfileCode { get; set; }
      public ICollection<AssignedProfileGroup> MLProfileGroup { get; set; }
      public string SelectedgroupProfiles { get; set; }
      public string Selectedgroups { get; set; }

      #region Added Field For AspNet Identity

      public string PasswordHash { get; set; }
      public long? Version { get; set; }

      #endregion

   }
}