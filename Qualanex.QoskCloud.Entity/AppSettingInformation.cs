﻿using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public class AppSettingInformation
    {
        public static IList<AppConfigurationSetting> lstQoskCloudAppInfo { get; set; }
    }
}
