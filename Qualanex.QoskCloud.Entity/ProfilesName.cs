﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProfilesName
    {
        public int? ProfileCode { get; set; }
        public int? SelectedProfileCode { get; set; }
        public string Name { get; set; }
        public long ProductID { get; set; }
        //for bind manufacture group
        public int ProfileGroupID { get; set; }
        public string GroupName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
