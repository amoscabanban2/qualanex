﻿namespace Qualanex.QoskCloud.Entity
{
    
    public class ItemImageDetails
    {
        public string ItemIDForBarcode { get; set; }
        public string QoskID { get; set; }
        public string FirstProductImage { get; set; }
        public string SecondProductImage { get; set; }
        public string PillCounterVideoFileName { get; set; }
        public string TransactionViedeoFileName { get; set; }

        public string FirstProductImageSasURL { get; set; }
        public string SecondProductImageSasURL { get; set; }
        public string PillCounterVideoFileNameSasURL { get; set; }
        public string TransactionViedeoFileNameSasURL { get; set; }


        public void DownloadReturnedImagesAndVideos( )
        {
            /* This needs to be updated - grf */
            this.FirstProductImageSasURL = Utility.Common.BlobResource.GetSASUrl("qoskimages", "images/" + this.QoskID + "/" + this.ItemIDForBarcode, this.FirstProductImage , "Read", 75);
            this.SecondProductImageSasURL = Utility.Common.BlobResource.GetSASUrl("qoskimages", "images/" + this.QoskID + "/" + this.ItemIDForBarcode, this.SecondProductImage , "Read", 75);
            this.PillCounterVideoFileNameSasURL = Utility.Common.BlobResource.GetSASUrl("qoskvideo", "videos/" + this.QoskID + "/" + this.ItemIDForBarcode, this.PillCounterVideoFileName , "Read", 75);
            this.TransactionViedeoFileNameSasURL = Utility.Common.BlobResource.GetSASUrl("qoskvideo", "videos/" + this.QoskID + "/" + this.ItemIDForBarcode, this.TransactionViedeoFileName , "Read", 75);
        }
    }

    public class QoskDetails
    {
        public int QoskId { get; set; }
        public string machineName { get; set; }
    }
}
