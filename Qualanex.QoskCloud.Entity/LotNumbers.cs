﻿using System;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
   public class LotNumbersDataEntity
   {

      [Display(Name = Constants.Model_Divested_Own_Name)]
      public string ProfileName { get; set; }

      [Display(Name = Constants.Model_Lot_Number_ID)]
      public long LotID { get; set; }

      [Display(Name = Constants.Model_Product)]
      public int ProductID { get; set; }

      public long NDC { get; set; }
      public int ProfileCode { get; set; }

      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Description { get; set; }

      [Required]
      [StringLength(20)]
      [Display(Name = Constants.Model_Lot_Number)]
      public string LotNumber { get; set; }

      [Display(Name = Constants.Model_Repackager)]
      public int? RepackagerProfileCode { get; set; }

      public string RepackagerName { get; set; }

      [Display(Name = Constants.Model_Select)]
      public bool IsDiverseLotNumber { get; set; }

      public string DivestedName { get; set; }

      [Display(Name = Constants.Model_Divested_Manufacture_Name)]
      public int? DiverseManufactureProfileCode { get; set; }

      [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = Constants.DefaultDataFormatString)]
      [Display(Name = Constants.Model_Expiration_Date)]
      public DateTime? ExpirationDate { get; set; }

      public bool IsRepackaged { get; set; }

      [Display(Name = Constants.Model_Recall)]
      public long RecallID { get; set; }

      [Display(Name = Constants.Model_Credit_Allowed)]
      public bool? ReturnAllowed { get; set; }

      public bool Assigned { get; set; }
      public string SelectedLOtNumber { get; set; }
      public bool disabledcb { get; set; }
      public int Version { get; set; }
      public string ModifiedBy { get; set; }
      public string CreatedBy { get; set; }

      [Display(Name = Constants.Model_Is_Recall)]
      public bool? IsActiveRecall { get; set; }

      public long? DivestProductID { get; set; }

   }

}
