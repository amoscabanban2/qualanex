﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public class ColumnListEntity
    {
       /// <summary>
       /// SubscribedColumn - subscribed columns grid in the Custom Grid options
       /// </summary>
       public List<ColumnDetails> SubscribedColumn { get; set; }
       
       /// <summary>
       /// DefaultColumn - default columns grid in the Custom Grid options
       /// </summary>
       public List<ColumnDetails> DefaultColumn { get; set; }
       
       /// <summary>
       /// CriteriaColumn - Get all columns tied to report for criteria i.e. search purposes
       /// </summary>
       public List<ColumnDetails> CriteriaColumn { get; set; }

   }
}
