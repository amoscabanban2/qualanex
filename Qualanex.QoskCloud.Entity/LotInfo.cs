﻿using System;

namespace Qualanex.QoskCloud.Entity
{
   public class LotInfo
   {
      public string LotNumber { get; set; }
      public DateTime? ExpirationDate { get; set; }
      public bool Recall { get; set; }
   }
}