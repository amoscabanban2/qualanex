﻿using System.Collections.Generic;
using Qualanex.Qosk.Library.Model.DBModel;

namespace Qualanex.QoskCloud.Entity
{
    public class MLAssignedGroupProfiles
    {
        public ProfileGroup profileGroup { get; set; }
        public List<AssignedProfileGroup> groupDetails { get; set; }
    }
}
