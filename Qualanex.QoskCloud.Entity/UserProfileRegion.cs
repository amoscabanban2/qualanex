﻿namespace Qualanex.QoskCloud.Entity
{
    public class UserProfileRegion
    {
        public virtual string ProfileName { get; set; }
        public virtual long userID { get; set; }
        public virtual string RegionCode { get; set; }
        public virtual bool IsSelected { get; set; }
        public virtual int ProfileCode { get; set; }

    }
}
