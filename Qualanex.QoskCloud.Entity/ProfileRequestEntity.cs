﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class ProfileRequestEntity
    {
        public List<ProfileRequest> profilesLists = new List<ProfileRequest>();
        public List<ProfileType> TypeList = new List<ProfileType>();
        public MLGroup Profile_Group { get; set; }
        public int? CurrentPageNo { get; set; }
    }
}
