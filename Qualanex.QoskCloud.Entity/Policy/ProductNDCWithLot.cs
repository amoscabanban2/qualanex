﻿using System;

namespace Qualanex.QoskCloud.Entity
{
   public class ProductNDCWithLot
   {
      public long ProductID { get; set; }
      public int ProfileCode { get; set; }
      public string Name { get; set; }
      public Nullable<int> MFGLabelerCode { get; set; }
      public Nullable<long> NDC { get; set; }
      public string NDCUPCWithDashes { get; set; }
      public string MFGProductNumber { get; set; }
      public string Description { get; set; }
      public ProductNDCLOTData lotDetails { get; set; }
   }
}
