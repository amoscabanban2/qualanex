﻿using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class ProductNDCLOTData
    {
        public List<LotNumbersDataEntity> lotNumberDetails { get; set; }
    }
}
