﻿using System.Collections.Generic;
using Qualanex.Qosk.Library.Model.DBModel;

namespace Qualanex.QoskCloud.Entity
{
   public class ProductWithLot
    {
        public Product Order { get; set; }
        public List<Lot> OrderDetails { get; set; }
        public ProfileTypeDict ProfileTypeDict { get; set; }
        public List<Profile> PrfDetails { get; set; }
    }

}
