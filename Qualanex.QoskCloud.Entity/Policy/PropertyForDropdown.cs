﻿namespace Qualanex.QoskCloud.Entity
{
    public class PropertyForDropdown
    {
        public PolicyTypeEnum ddlPolicyTypeEnum { get; set; }
        public PolicyNameEnum ddlPolicyNameEnum { get; set; }
        public ProfileTypeEnum ddlProfileTypeForPublishedEnum { get; set; }
        public ProfileTypeForCustomEnum ddlProfileTypeForCustomEnum { get; set; }
    }
}
