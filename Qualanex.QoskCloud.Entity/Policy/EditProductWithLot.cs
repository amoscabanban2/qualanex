﻿using System.Collections.Generic;
using Qualanex.Qosk.Library.Model.DBModel;

namespace Qualanex.QoskCloud.Entity
{
    public class EditProductWithLot
    {
        public Product Order { get; set; }
        public List<LotNumbersDataEntity> OrderDetails { get; set; }
        public ProfileTypeDict ProfileTypeDict { get; set; }
        public List<ProfileRequest> PrfDetails { get; set; }
    }
}
