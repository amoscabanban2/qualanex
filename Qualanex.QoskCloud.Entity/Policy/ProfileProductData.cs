﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProfileProductData
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int ProfileCode { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string RegionCode { get; set; }
        public ProductData products { get; set; }
    }
}
