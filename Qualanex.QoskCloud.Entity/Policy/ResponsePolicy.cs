﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
   public class ResponsePolicy : PropertyForDropdown
   {
      public long PolicyID { get; set; }
      public byte? PolicyType { get; set; }

      [Display(Name = Constants.PolicyName)]
      public string PolicyName { get; set; }

      public int? ProfileCode { get; set; }
      public long? ProductID { get; set; }
      public int? ManufacturerProfileCode { get; set; }
      public int RepackagerProfileCode { get; set; }
      public int WholesalerProfileCode { get; set; }
      public string LotNumber { get; set; }
      public bool? DirectAccount { get; set; }

      [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = Constants.DefaultDataFormatString)]
      public DateTime? PolicyStartDate { get; set; }

      [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = Constants.DefaultDataFormatString)]
      public DateTime? PolicyEndDate { get; set; }

      public string AuthorizedForCreditConsideration { get; set; }
      public byte NumberOfDaysToReturnProductReceivedInError { get; set; }
      public bool? ExpiresOnFirstOfCurrentMonth { get; set; }
      public byte? NumberOfMonthsAfterExpirationReturnable { get; set; }
      public byte? NumberOfMonthsBeforeExpirationReturnableSealed { get; set; }
      public byte? NumberOfMonthsBeforeExpirationReturnableOpened { get; set; }
      public bool? BrokenSealReturnable { get; set; }
      public string PartialsReturnable { get; set; }
      public bool StateLawPartialOverride { get; set; }
      public decimal? PartialPercent1 { get; set; }
      public decimal? CreditPercent1 { get; set; }
      public decimal? PartialPercent2 { get; set; }
      public decimal? CreditPercent2 { get; set; }
      public decimal? PartialPercent3 { get; set; }
      public decimal? CreditPercent3 { get; set; }
      public decimal? MaximumPartialCredit { get; set; }
      public bool PrescriptionVialsReturnable { get; set; }
      public bool RepackagedIntoUnitDoseReturnable { get; set; }
      public bool MissingLotNumberReturnable { get; set; }
      public bool MissingExpDateReturnable { get; set; }
      public decimal? CustomerReimbursementAmount { get; set; }
      public string UseDebitPricing { get; set; }
      public bool DoNotUseFallThroughPrice { get; set; }
      public short MatchAccuracyPrecision { get; set; }
      public decimal? MinimumDollarAmountReturnable { get; set; }
      public bool UseWholeSalerDirectPriceWhenNoIndirect { get; set; }
      public bool AllowQuantityOverPackageSize { get; set; }
      public bool OverstockApprovalRequired { get; set; }
      public decimal? OrderErrorAdministrativeFee { get; set; }
      public decimal? HandlingFee { get; set; }
      public decimal? HandlingFeeExceptionForStatePartialPolicyLaw { get; set; }
      public decimal? CreditIssuedAtWACLess { get; set; }
      public decimal? CreditIssuedAtWACLessExceptionForStatePartialPolicyLaw { get; set; }
      public string NonDirectCreditIssuedBy { get; set; }
      public int ReturnAuthorizationExpirationDays { get; set; }
      public decimal? ReturnAuthorizationCompletionPercent { get; set; }
      public bool PriorReturnAuthorizationRequired { get; set; }
      public IList<ProfilesName> lstProfileBind { get; set; }
      public IEnumerable<string> lstType { get; set; }
      public IEnumerable<string> SelectedProfileType { get; set; }
      public ICollection<ProfileRequest> SelectedProfileDetails { get; set; }

      [Display(Name = Constants.PolicyType)]
      public string SelectedPolicyType { get; set; }

      public string PolicyTypeName { get; set; }
      public IEnumerable<ProductWithLot> listProductLot { get; set; }
      public string ManufactureName { get; set; }
      public string PolicyforProfileCode { get; set; }
      public Nullable<long> NDC { get; set; }
      public string NDCUPCWithDashes { get; set; }
      public IEnumerable<MLGroup> lstGroupType { get; set; }
      public bool Isselectedlot { get; set; }
      public string PolicyTypeFlag { get; set; }
      public string ProfileGroupSelectionType { get; set; }
      public string InnerPack { get; set; }
      public bool AcceptNonReturnableProductForDestruction { get; set; }
      public bool Form41Allowed { get; set; }
      public string SelectedMWRType { get; set; }
      public int CurrentPageNoforPharmacy { get; set; }
      public int PolicyGroupType { get; set; }
      public string PolicyProfileType { get; set; }
      public int? CurrentPageNo { get; set; }
      public int? NDCCurrentPageNo { get; set; }
      public int? NDCLOTCurrentPageNo { get; set; }
      public string PharmacyName { get; set; }
      public string PolicyNameforGrid { get; set; }
      public string ModifiedBy { get; set; }
      public long? Version { get; set; }
      public DateTime ModifiedDate { get; set; }
      public bool OverWrite { get; set; }
      public string Description { get; set; }
      public string GroupName { get; set; }
      public string PharmacyProfileGroupType { get; set; }
      public string ProfileType { get; set; }
      public Nullable<int> PolicyForGroupProfileID { get; set; }
      public string Address1 { get; set; }
      public string City { get; set; }
      public string State { get; set; }
      public string RegionCode { get; set; }
      public string ZipCode { get; set; }
   }
}
