﻿namespace Qualanex.QoskCloud.Entity
{ /// <summary>
  /// View Policy Details
  /// </summary>
    public class ViewDeletePolicy
    {
        public long PolicyID { get; set; }
        public byte? PolicyType { get; set; }
        public string PolicyName { get; set; }
        public string ddPolicyProfileType { get; set; }
        public int ddPolicyProfileName { get; set; }
    }
}
