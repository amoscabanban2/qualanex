﻿
namespace Qualanex.QoskCloud.Entity
{
   public class ViewEdit
    {
       public long PolicyID { get; set; }
        public string SelectedPolicyType { get; set; }
        public int PolicyTypeName { get; set; }
        public string ProfileGroupSelectionType { get; set; }
        public string SelectedPharmacyGroupType { get; set; }
        public string SelectedPharmacyGroupName { get; set; }
        public int? SelectedPharmacyGroupProfileCode { get; set; }
        public byte?  PolicyType { get; set; }      
        public string PolicyTypeText { get; set; }
        public string SelectedMWWRProfileType { get; set; }
        public int  MWWRProfileType { get; set; }
        public int? SelectedMWWRProfileCode { get; set; }
        public string MWRName { get; set; }
       
    }
}
