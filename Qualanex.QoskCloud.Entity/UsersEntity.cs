﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class UsersEntity
    {
        public IEnumerable<UserRegistrationRequest> UsersReturnData { get; set; }
        public List<UserType> UserType { get; set; }
        public string UserTypeCode { get; set; }
        public int? CurrentPageNo { get; set; }
    }
}
