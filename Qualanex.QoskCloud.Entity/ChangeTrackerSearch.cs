﻿using System;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Entity
{
   public class ChangeTrackerSearch
   {
      public long ID { get; set; }
      public string NDCUPCWithDashes { get; set; }
      public Nullable<long> NDC { get; set; }
      public Nullable<long> UPC { get; set; }
      public string Strength { get; set; }
      public string ProfileTypeName { get; set; }
      public string ManufactureRepackagerName { get; set; }

      public string Description { get; set; }
      public string LotNumber { get; set; }
      public string PageIdentification { get; set; }
      public DateTime? FromRange { get; set; }
      public DateTime? ToRange { get; set; }
      public string SelectedTableName { get; set; }
      public string ColumnName { get; set; }
      public List<ChangeTrackerEntity> lstChangeTrackentity { get; set; }
   }
}
