﻿
namespace Qualanex.QoskCloud.Entity
{
    public class AssignedUserProfilesData
    {
        public int profileCode { get; set; }
        public string ProfileName { get; set; }
        public bool Assigned { get; set; }
    }
}
