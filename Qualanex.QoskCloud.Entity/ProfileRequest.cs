﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
   public class ProfileRequest
   {
      [Key]
      public int ProfileCode { get; set; }
      [Required, StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Name { get; set; }
      [Required, StringLength(30)]
      public string Type { get; set; }
      [Display(Name = Constants.Model_Display_Order)]
      public int DisplayOrder { get; set; }
      public byte[] Logo { get; set; }
      [StringLength(10)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string DEANumber { get; set; }
      [Display(Name = Constants.Model_DEA_Expiration_Date)]
      public Nullable<System.DateTime> DEAExpirationDate { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_Email_Address)]
      public string EmailAddress { get; set; }
      [Required, StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Address1 { get; set; }
      [StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Address2 { get; set; }
      [StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Address3 { get; set; }
      [Required, StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string City { get; set; }
      [Required, StringLength(2)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string State { get; set; }
      [Required, StringLength(5)]
      [Display(Name = Constants.Model_Zip_Code)]
      [RegularExpression(Constants.Model_RegularExpression_Zipcode)]
      public string ZipCode { get; set; }
      [Required, StringLength(50)]
      [Display(Name = Constants.Model_Phone_Number)]
      public string PhoneNumber { get; set; }
      [Required, StringLength(50)]
      [Display(Name = Constants.Model_Contact_First_Name)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string ContactFirstName { get; set; }
      [Required, StringLength(50)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      [Display(Name = Constants.Model_Contact_Last_Name)]
      public string ContactLastName { get; set; }
      [Display(Name = Constants.Model_Contact_Phone_Number)]
      public string ContactPhoneNumber { get; set; }
      [Display(Name = Constants.Model_Contact_Email_Address)]
      public string ContactEmailAddress { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_Contact_Fax_Number)]
      public string ContactFaxNumber { get; set; }
      [Display(Name = Constants.Model_Policy_Snapshot)]
      public byte[] PolicySnapshot { get; set; }
      [Required]
      [Display(Name = Constants.Model_Direct_Accounts_Only)]
      public bool DirectAccountsOnly { get; set; }
      [Display(Name = Constants.Model_Rollup_Profile_Code)]
      public int RollupProfileCode { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_Toll_Free_Phone_Number)]
      public string TollFreePhoneNumber { get; set; }
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string Notes { get; set; }
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      [Display(Name = Constants.Model_Return_Procedures)]
      public string ReturnProcedures { get; set; }
      [StringLength(10)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      [Display(Name = Constants.Model_Representative_Number)]
      public string RepresentativeNumber { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_Wholesaler_Account_Number)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string WholesalerAccountNumber { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_Contact_Fax_Number)]
      public string FaxNumber { get; set; }
      [StringLength(14)]
      [Display(Name = Constants.Model_LotNumber_ExpirationDate_Validation_Method)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string LotNumberExpirationDateValidationMethod { get; set; }
      [StringLength(20)]
      public string CreatedBy { get; set; }
      [Display(Name = Constants.Model_Created_Date)]
      public DateTime CreatedDate { get; set; }
      [StringLength(20)]
      public string ModifiedBy { get; set; }
      public DateTime ModifiedDate { get; set; }
      [Required]

      [Display(Name = Constants.Model_Alert_Pending_Return_Authorizations)]
      public bool AlertPendingReturnAuthorizations { get; set; }
      [Required]
      [Display(Name = Constants.Model_Alert_Completed_Return_Authorizations)]
      public bool AlertCompletedReturnAuthorizations { get; set; }
      [StringLength(10)]
      public string Status { get; set; }
      [Display(Name = Constants.Model_Vendor_ID)]
      public long VendorID { get; set; }
      [Display(Name = Constants.Model_Vendor_Load_Date)]
      public DateTime? VendorLoadDate { get; set; }
      [StringLength(20)]
      [Display(Name = Constants.Model_Sales_Org)]
      public string SalesOrg { get; set; }
      [Display(Name = Constants.Model_Qualanex_Display)]
      public bool QualanexComDisplay { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_PDMA_Coordinator_Email_Address)]
      public string PDMACoordinatorEmailAddress { get; set; }
      [StringLength(50)]
      [Display(Name = Constants.Model_PDMA_Coordinator_Email_Address_Process_Completed)]
      public string PDMACoordinatorEmailAddressProcessCompleted { get; set; }
      public int SectionNumber { get; set; }
      public int ProfileGroupID { get; set; }
      public IEnumerable<SelectListItem> ProfileGroup { get; set; }
      public IEnumerable<SelectListItem> AccountProfileGroupType { get; set; }
      [StringLength(3)]
      [Display(Name = Constants.Model_Region_Code)]
      public string RegionCode { get; set; }
      public bool Assigned { get; set; }
      public string ProfileMsg { get; set; }
      [StringLength(30)]
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string AccountProfileType { get; set; }
      [RegularExpression(Constants.Model_RegularExpression_Description)]
      public string AccountProfileCode { get; set; }
      public List<ProfilesName> lstmanufacturer { get; set; }
      public int? CurrentPageNo { get; set; }
      public long? Version { get; set; }
      public bool SpecialHandling { get; set; }
   }

}