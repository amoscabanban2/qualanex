﻿namespace Qualanex.QoskCloud.Entity
{
    public class ColumnListRequest
    {
       public string LayoutName { get; set; }
       public long UserId { get; set; }
       public bool ForGridColumnListing { get; set; }
       public bool Criteria { get; set; }
   }
}
