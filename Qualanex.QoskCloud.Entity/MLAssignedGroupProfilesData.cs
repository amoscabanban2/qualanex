﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class MLAssignedGroupProfilesData
    {
        public List<AssignedProfileGroup> groupDetails { get; set; }
    }
}
