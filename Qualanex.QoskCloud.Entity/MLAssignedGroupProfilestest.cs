﻿namespace Qualanex.QoskCloud.Entity
{
    public class MLAssignedGroupProfilestest
    {
        public int ProfileGroupID { get; set; }
        public string GroupName { get; set; }
        public int ProfileCode { get; set; }
        public string GroupType { get; set; }
        public string ProfileName { get; set; }
        public string RegionCode { get; set; }
        public bool IsAssigned { get; set; }
        public MLAssignedGroupProfilesData groupDetail { get; set; }
    }
}
