﻿namespace Qualanex.QoskCloud.Entity
{
   public class ColumnDetails
   {
      public int ColumnId { get; set; }
      public string ColumnName { get; set; }
      public string ColumnType { get; set; }
      public string ColumnDescription { get; set; }
      public long UserId { get; set; }
      public int LayoutId { get; set; }
      public string TableName { get; set; }
      public int TableId { get; set; }
      public string FilterType { get; set; }
      public int UserMappingId { get; set; }
      public bool? IsSearchPanelDefault { get; set; }
      public bool? IsGridListingDefault { get; set; }
      public string DisplayText { get; set; }
      public string MappedTable { get; set; }
      public int? OrderNumber { get; set; }
      public int? SearchOrder { get; set; }
      public int? GridsearchOrder { get; set; }
      public string SearchColumnName { get; set; }
      public bool? IsAutoComplete { get; set; }
   }
}
