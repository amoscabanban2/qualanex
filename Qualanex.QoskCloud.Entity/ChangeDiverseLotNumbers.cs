﻿namespace Qualanex.QoskCloud.Entity
{
    public class ChangeDiverseLotNumbers
    {
        public int DiverseOwnshipProfileCode { get; set; }
        public string LotIDs { get; set; }
        public long ProductID { get; set; }
        public bool Updatestatus { get; set; }
    }
}
