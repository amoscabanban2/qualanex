﻿using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class LotNumbersReturns
    {
        public IEnumerable<LotNumbersDataEntity> listLotNumbers { get; set; }
        public int? CurrentLotPageNo { get; set; }
    }
}
