﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public class DynamicControlEntity
    {
        /// <summary>
        /// Define Model/Entity Property To Generate Dynamic Control On Form
        /// </summary>
        public List<ControlViewModel> Controls { get; set; }

    }
}
