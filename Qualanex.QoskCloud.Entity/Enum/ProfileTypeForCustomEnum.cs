﻿using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
    public enum ProfileTypeForCustomEnum
    {
        [Display(Name = Constants.ProfileTypeForCustom_Manufacturer)]
        Manufacturer=1,
        [Display(Name = Constants.ProfileTypeForCustom_ManufacturerGroup)]
        ManufacturerGroup=2,
        [Display(Name = Constants.ProfileTypeForCustom_Wholesaler)]
        Wholesaler=3,
        [Display(Name = Constants.ProfileTypeForCustom_WholesalerGroup)]
        WholesalerGroup=4,
        [Display(Name = Constants.ProfileTypeForCustom_Repackager)]
        Repackager=5
    }
}
