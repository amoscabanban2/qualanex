﻿using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
     /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public enum PolicyTypeEnum
    {
        [Display(Name = Constants.PolicyType_AllProducts)]
         AllProducts = 1,
        [Display(Name = Constants.PolicyType_ProductsSpecificToNDCUPC)]
        ProductsSpecifictoNDCUPC = 2,
        [Display(Name = Constants.PolicyType_ProductsSpecificToNDCUPCAndLotNumber)]
        ProductsSpecifictoNDCUPCandLotNumber = 3
    }
}
