﻿using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
    public enum ProfileTypeEnum
        {
            [Display(Name = Constants.ProfileType_Manufacturer)]
            Manufacturer=1,
            
            [Display(Name = Constants.ProfileType_Wholesaler)]
            Wholesaler=3,
            
            [Display(Name = Constants.ProfileType_Repackager)]
            Repackager=5
        }
   
}
