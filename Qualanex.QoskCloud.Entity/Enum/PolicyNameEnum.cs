﻿using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
     /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
   public enum PolicyNameEnum
    {
        [Display(Name = Constants.ProfileType_PublishedPolicy)]
        Published = 1,

        [Display(Name = Constants.ProfileType_CustomerSpecific)]
        Custom = 2 
    }
}
