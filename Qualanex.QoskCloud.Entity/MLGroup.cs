﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
   public class MLGroup
    {
       public int ProfileGroupID { get; set; }
       [Display(Name =Constants.Model_Group_Name)]
       [RegularExpression(Constants.Model_RegularExpression_Description)]
       public string GroupName { get; set; }
       [Display(Name =Constants.Model_Profile_Name)]
       public string GroupProfilesName { get; set; } 
       public int ProfileCode { get; set; }
       public int ChildProfileCode { get; set; }
       [Display(Name = Constants.Model_Group_Type)]
       public string GroupType { get; set; }
       public string ProfileName { get; set; }
       public bool IsDelete { get; set; }  
       public string Created { get; set; }
       public DateTime CreatedDateTime { get; set; }
       public string ModifiedBy { get; set; }
       public DateTime? ModifiedDateTime { get; set; }
       public IEnumerable<SelectListItem> ProductProfileList { get; set; }
       public bool IsAssigned { get; set; }   
    }
}
