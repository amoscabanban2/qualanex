﻿namespace Qualanex.QoskCloud.Entity
{
   public class LotNumberSearch
   {
      public long UserID { get; set; }
      public int ProfileCode { get; set; }
      public int UserProfileCode { get; set; }
      public int RollupProfileCode { get; set; }
      public string UserTypeCode { get; set; }
      public long LotNumberID { get; set; }
      public string LotNumber { get; set; }
      public long NDC { get; set; }
      public string Description { get; set; }
      public string UserTypeName { get; set; }
      public string ProfileType { get; set; }
   }
}
