﻿using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    public class AccountProfileRequestEntity
    {
        public List<ProfileAccountResponse> AccountProfileLists = new List<ProfileAccountResponse>();
        public int? CurrentPageNo { get; set; }

    }
}
