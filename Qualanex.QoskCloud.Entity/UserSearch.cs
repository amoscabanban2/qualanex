﻿namespace Qualanex.QoskCloud.Entity
{
    public class UserSearch
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserTypeCode { get; set; }
        public string UserSearchTypeCode { get; set; }
        public int ProfileCode { get; set; }
        public string ProfileType { get; set; }
        public string UserTypeName { get; set; }
        public string RegionCode { get; set; }
        public string NumberOfRecords { get; set; }
        public int RollupProfileCode { get; set; }
    }
}
