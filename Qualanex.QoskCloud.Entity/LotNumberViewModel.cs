﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Entity
{
    public class LotNumberViewModel
    {
        public LotNumbersDataEntity LotNumbersData { get; set; }

        public IEnumerable<SelectListItem> RepackagerProfileList { get; set; }

        public IEnumerable<SelectListItem> ProductProfileList { get; set; }

        public IEnumerable<SelectListItem> ProductRecallList { get; set; }

        public ProductsML ProductDetails { get; set; }

        public bool IsPopupCreateLotNumber { get; set; }

        public string LblMsg { get; set; }
    }
}
