﻿using System.Configuration;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
   public class LoginViewModel
   {
      [Required(ErrorMessage = Constants.Model_Message_UserName_Is_Required), StringLength(20)]
      [Display(Name = Constants.Model_User_Name)]
      public string UserName { get; set; }
      [Required(ErrorMessage = Constants.Model_Message_Password_Is_Required)]
      [StringLength(20, ErrorMessage = Constants.Model_Message_Password_Is_Required, MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = Constants.Model_Password)]
      [PlaceHolder(Constants.Model_Message_Enter_Passowrd)]
      [RegularExpression(Constants.Model_RegularExpression_Password, ErrorMessage = Constants.Model_Message_Password_Is_Required)]
      public string Password { get; set; }
      public bool RememberMe { get; set; }
      public bool IsTemporaryPassword { get; set; }
      public string Version { get { return ConfigurationManager.AppSettings["trayservice:Version"]; }}
   }
}
