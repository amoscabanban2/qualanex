﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;

namespace Qualanex.QoskCloud.Entity
{
   public class ProductsML
   {
      public string Name { get; set; }
      public int ProductID { get; set; }
      public int? RollupProductID { get; set; }
      public int ProfileCode { get; set; }
      public DateTime? ProductStartDate { get; set; }
      public DateTime? ProductEndDate { get; set; }
      public string ModifiedBy { get; set; }
      public int? MFGLabelerCode { get; set; }
      public long? NDC { get; set; }
      public long? NDCforImageTab { get; set; }
      public string NDCUPCWithDashesforImageTab { get; set; }
      public string DescriptionforImageTab { get; set; }
      private string _NDCUPCWithDashes;

      public string NDCUPCWithDashes
      {
         get { return _NDCUPCWithDashes; }
         set { _NDCUPCWithDashes = value.Trim(); }
      }

      public long? UPC { get; set; }
      public string MFGProductNumber { get; set; }
      public bool PhysicianSample { get; set; }
      public string PhysicianSampleProductNumber { get; set; }
      public decimal PackageSize { get; set; }
      public int UnitsPerPackage { get; set; }
      public string UnitOfMeasure { get; set; }
      public string Description { get; set; }
      public string DosageCode { get; set; }
      public byte ControlNumber { get; set; }
      public decimal? IndividualCountWeight { get; set; }
      public decimal? ContainerWeight { get; set; }
      public decimal? FullContainerWeightWithContents { get; set; }
      public string Strength { get; set; }
      public bool UnitDose { get; set; }
      public bool RXorOTC { get; set; }
      public string BrandorGeneric { get; set; }
      public string TherapeuticClass { get; set; }
      public string ShortSupply { get; set; }
      public bool TamperResistantSealExists { get; set; }
      public string SpecialHandlingInstructions { get; set; }
      public bool DEAWatchList { get; set; }
      public bool? ARCOSReportable { get; set; }
      public bool? Withdrawn { get; set; }
      public bool? ExcludeFromExternalInterface { get; set; }
      public bool? ExcludeFromDropDown { get; set; }
      public bool AlwaysSortToQuarantine { get; set; }
      public bool ForceQuantityCount { get; set; }
      public string ForceQuantityCountNotes { get; set; }
      public bool? IsRepackager { get; set; }
      public int? DiverseManufacturerProfileCode { get; set; }
      public string DiverseManufacturerName { get; set; }
      public List<Dosage> lstProductDosageType { get; set; }
      public List<Dosage> lstManufacturelist { get; set; }
      public int SectionNumber { get; set; }
      public List<ProductsML> lstLotProduct { get; set; }

      [Display(Name = Constants.Model_Is_Divested_Product)]
      public bool IsDiverseProduct { get; set; }

      public string Image { get; set; }
      public string PackagingOrContainerType { get; set; }
      public string OpenSealedCaseType { get; set; }
      public string httppostedfile { get; set; }
      public string httppostedContentfile { get; set; }
      public string browseforSeparateContainer { get; set; }
      public string browseforSeparateContent { get; set; }
      public string ContentImage { get; set; }
      public List<ImageDetails> lstProductImage { get; set; }
      public long HiddenProductImageDetailID { get; set; }
      public DiverseLotNumbersDetails diverseLotNumbersDetails { get; set; }
      //New Column Suggested by Dan
      public int? CaseSize1 { get; set; }
      public int? CaseSize2 { get; set; }
      public int? CaseSize3 { get; set; }
      public bool? Refrigerated { get; set; }
      public bool? DivestedByLot { get; set; }
      public string WholesalerNumber { get; set; }
      public string EDINumber { get; set; }
      public string CreatedBy { get; set; }
      public string ManufactureRepackagerName { get; set; }
      public DateTime? ModifiedDate { get; set; }
      public string lstOwnerShipManufacture { get; set; }
      public LotNumberViewModel objLotNumbersDataEntity { get; set; }
      public List<ProfileRequest> lstWholesaler { get; set; }
      public List<ProfilesName> lstManufacturer { get; set; }
      public bool DeleteLotNumber { get; set; }
      public string LotNumber { get; set; }
      public DateTime? ExpirationDate { get; set; }
      public long? RecallId { get; set; }
      public long? lotNumberID { get; set; }
      public bool disabledcb { get; set; }
      public bool Assigned { get; set; }
      public long? Version { get; set; }
      public long VersionForProductImage { get; set; }
      public bool LocalMedia { get; set; }
      public string AzureImageContainer { get; set; }
      public string AzureImageContent { get; set; }
      public string DosagesDescription { get; set; }
      public string ProfileTypeName { get; set; }
      public long? DivestProductID { get; set; }
      public bool? IsOriginal { get; set; }
      public string PillType { get; set; }
   }
}
