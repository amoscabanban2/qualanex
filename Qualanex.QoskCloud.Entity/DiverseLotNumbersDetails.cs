﻿namespace Qualanex.QoskCloud.Entity
{
    public class DiverseLotNumbersDetails
    {
        public DiverseLotNumbers OriginalManufactureLotNumbers { get; set; }
        public DiverseLotNumbers DiverseManufactureLotNumbers { get; set; }
        public int? DivestedProfileCode { get; set; }
    }
}
