﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Qualanex.QoskCloud.Entity
{
    public class QoskRequest
    {
        [Required]
        public string QoskID { get; set; }
        [Required]
        public string MachineName { get; set; }
        public bool Active { get; set; }
        [Required]
        public int ProfileCode { get; set; }
        public string Profile { get; set; }
        public string CustomerSupportNumber { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Password { get; set; }
        [Required]
        public string State { get; set; }
        public string PharmacyName { get; set; }
    }
}
