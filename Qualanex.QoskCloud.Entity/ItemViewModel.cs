﻿using System.Web.Mvc;
namespace Qualanex.QoskCloud.Entity
{
    public class ItemViewModel
    {
        public SelectList RegionDistrict { get; set; }
        public SelectList Qosk { get; set; }
        public SelectList lstManufacturer { get; set; }

    }
}
