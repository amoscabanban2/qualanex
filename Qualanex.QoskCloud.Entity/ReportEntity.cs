﻿namespace Qualanex.QoskCloud.Entity
{
   public class ReportEntity
    {
        public long ReportID { get; set; }
        public string ReportTitle { get; set; }
        public string ReportModule { get; set; }
    }
}
