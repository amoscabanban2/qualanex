﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProfileType
    {
        public string ID { get; set; }
        public string Type { get; set; }
    }
}
