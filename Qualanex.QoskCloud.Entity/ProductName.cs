﻿namespace Qualanex.QoskCloud.Entity
{
   public class ProductName
   {
      public long? NDC { get; set; }
      public long? UPC { get; set; }
      public string ManufacturerName { get; set; }
      public string ProductDescription { get; set; }
      public string GenericName { get; set; }
   }
}