﻿using System.Collections.Generic;
namespace Qualanex.QoskCloud.Entity
{
    /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public class ReturnLoginUserEntity
    {
       public UserRegistrationRequest UserReturnData { get; set; }
       public List<LeftMenuLinks> UserLeftLinks  { get; set; } 
       public string AdminApplicationUrl { get; set; }
       public string ReportApplicationUrl { get; set; }
       public List<ReportEntity> ReportsNameLinks { get; set; }
    }
      
}
