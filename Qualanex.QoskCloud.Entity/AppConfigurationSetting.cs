﻿using System;
namespace Qualanex.QoskCloud.Entity
{
   public class AppConfigurationSetting
    {
        
            public int ID { get; set; }
            public string KioskId { get; set; }
            public string Value { get; set; }
            public string keyName { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public string GroupName { get; set; }
            public string Type { get; set; }
            public bool Active { get; set; }
    }

}
