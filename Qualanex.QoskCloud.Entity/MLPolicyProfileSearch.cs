﻿
namespace Qualanex.QoskCloud.Entity
{
   public class MLPolicyProfileSearch
    {
        public long PolicyID { get; set; }
        public int PolicyProfileCode { get; set; }
        public long PolicyAppliedForProfileCode { get; set; } 
        public int GroupID { get; set; } 
        public long NDCUPC { get; set; } 
        public string LotNumber { get; set; }
        public bool IsSerach { get; set; }
    }
}
