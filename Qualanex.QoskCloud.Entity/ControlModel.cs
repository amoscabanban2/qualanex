﻿namespace Qualanex.QoskCloud.Entity
{
    public abstract class ControlViewModel
    {
        public abstract string Type { get; }
        public bool Visible { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Value1 { get; set; }
        public string AssociateTableName { get; set; }
        public string DataType { get; set; }
        public string OperatorType { get; set; }
        public bool? IsAutoComplete { get; set; }
        public int FieldLength { get; set; }
    }

}