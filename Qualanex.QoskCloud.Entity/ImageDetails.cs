﻿namespace Qualanex.QoskCloud.Entity
{
   public class ImageDetails
   {
      public long ProductImageDetailID { get; set; }
      public string Image { get; set; }
      public string ContentImage { get; set; }
      public string Container { get; set; }
      public long? ProductID { get; set; }
      public long? NDCforImage { get; set; }
      public string NDCUPCWithDashesforImage { get; set; }
      public string DescriptionforImage { get; set; }
      public string AzureContainer { get; set; }
      public string AzureContent { get; set; }
      public long? Version { get; set; }
   }
}
