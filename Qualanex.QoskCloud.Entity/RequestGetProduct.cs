﻿namespace Qualanex.QoskCloud.Entity
{
    public class RequestGetProduct
    {
        public long UserID { get; set; }
        public int ProfileCode { get; set; }
        public long NDCUPC { get; set; }
        public string LotNumber { get; set; }
        public long ProductID { get; set; }
        public string Description { get; set; }
        public string UserTypeName { get; set; }
        public int RecordNumber { get; set; }
    }
}
