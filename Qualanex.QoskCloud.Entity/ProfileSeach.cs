﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProfileSeach
    {
        public long UserID { get; set; }
        public int UserProfileCode { get; set; }
        public int ProfileCode { get; set; }
        public int RollupProfileCode { get; set; }
        public string SerachProfileType { get; set; }
        public string ProfileType { get; set; }
        public string UserTypeCode { get; set; }
        public string UserTypeName { get; set; }
        public string City { get; set; }

    }
}
