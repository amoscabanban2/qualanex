﻿using System;

namespace Qualanex.QoskCloud.Entity
{
   public class ProductRecall
   {
      public long? NDC { get; set; }
      public long? UPC { get; set; }
      public string LotNumber { get; set; }
      public string RecallClass { get; set; }
      public DateTime? EffectiveStartDate { get;set; }
      public DateTime? EffectiveEndDate { get; set; }
      public DateTime? RecallStartDate { get; set; }
      public DateTime? RecallEndDate { get; set; }
   }
}