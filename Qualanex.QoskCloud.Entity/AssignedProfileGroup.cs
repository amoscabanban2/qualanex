﻿namespace Qualanex.QoskCloud.Entity
{
    public class AssignedProfileGroup
    {
        public int ProfileGroupID { get; set; }
        public long userID { get; set; }
        public int GroupProfileCode { get; set; }
        public int PProfileCode { get; set; }
        public string GroupProfileName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Region { get; set; }
        public string GroupName { get; set; }
        public bool Assigned { get; set; }
        public string GroupType { get; set; }

    }
}
