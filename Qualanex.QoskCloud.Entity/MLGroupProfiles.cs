﻿using System;
using System.ComponentModel.DataAnnotations;
using Qualanex.QoskCloud.Utility;
namespace Qualanex.QoskCloud.Entity
{
   public class MLGroupProfiles
    {
       public long ID { get; set; }
       [Display(Name =Constants.Model_Group_Name)]
       public string ProfileGroupName { get; set; }
       [Display(Name = Constants.Model_Profile_Group_ID)]
       public int ProfileGroupID { get; set; }
       [Display(Name = Constants.Model_Profile_Type)]
       public string ProfileType { get; set; }
       [Display(Name = Constants.Model_Profile_Name)]
       public string ProfileName { get; set; }
       [Display(Name = Constants.Model_Group_Type)]
       public string GroupType { get; set; } 
       [Display(Name = Constants.Model_Profile_Name)]
       public int ProfileCode { get; set; }
       [Display(Name = Constants.Model_Region_Code)]
       public string RegionCode { get; set; }
       [Display(Name = Constants.Model_City)]
       public string City { get; set; }
       [Display(Name = Constants.Model_State)]
       public string State { get; set; } 
       [Display(Name = Constants.Model_Created_By)]
       public string CreatedBy { get; set; }
       [Display(Name = Constants.Model_Created_DateTime)]
       public DateTime CreatedDateTime { get; set; }
       [Display(Name = Constants.Model_Modified_By)]
       public string ModifiedBy { get; set; }
       [Display(Name = Constants.Model_Modified_DateTime)]
       public DateTime? ModifiedDateTime { get; set; }
       [Display(Name = Constants.Model_Is_Assigned)]
       public bool IsAssigned { get; set; } 
    }
}
