﻿using System;
using Qualanex.QoskCloud.Utility;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Entity
{
    /// <summary>
    /// this class is given by the client architect we dont need to modify that.
    /// </summary>
    public sealed class PlaceHolderAttribute : Attribute, IMetadataAware
    {
        private readonly string _placeholder;

        public PlaceHolderAttribute(string placeholder)
        {
            _placeholder = placeholder;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues[Constants.Placeholder] = _placeholder;
        }

    }
}
