﻿namespace Qualanex.QoskCloud.Entity
{
   public class ProfileAccountResponse
    {
       public string PharmacyName { get; set; } 
       public string Type { get; set; }
       public string AccountType { get; set; }     
       public string City { get; set; }
       public string  State{ get; set; }
       public string AccountNumber { get; set; } 
    }

  }
