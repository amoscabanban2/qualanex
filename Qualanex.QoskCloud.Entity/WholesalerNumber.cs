﻿using System;
using System.Web.Mvc;

namespace Qualanex.QoskCloud.Entity
{
   public class AddWholesalerNumber
   {
      public long WholesalerNumberID { get; set; }
      public long ProductID { get; set; }
      public string NDCUPCWithDashes { get; set; }
      public string Number { get; set; }
      public int ProfileCode { get; set; }
      public string Description { get; set; }
      public string WholesalerName { get; set; }
      public string ModifiedBy { get; set; }
      public DateTime ModifiedDate { get; set; }
      public string CreatedBY { get; set; }
      public DateTime CreatedDate { get; set; }
      public string OperationType { get; set; }
      public SelectList WholesalerProfileList { get; set; }
      public long WholesalerVersion { get; set; }
      public string NDCUPCInputName { get; set; }
      public int NDCUPCInputvalue { get; set; }
      public string ManufacturerRepackagerName { get; set; }
      public string Strength { get; set; }
      public long NDC { get; set; }
      public long UPC { get; set; }
   }
}
