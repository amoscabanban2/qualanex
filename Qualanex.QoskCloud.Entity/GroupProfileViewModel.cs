﻿using System.Collections.Generic;
using System.Web.Mvc;
namespace Qualanex.QoskCloud.Entity
{
   public class GroupProfileViewModel
    {
       //For Group Filter Profiles Reason Code list
       public IEnumerable<SelectListItem> GroupProfileReason { get; set; }
       //For Group Filter Profiles City list 
       public IEnumerable<SelectListItem> GroupProfileCity { get; set; }
       //For Group Filter  ProfileType selected  Profiles Code
       public IEnumerable<SelectListItem> GroupProfileType { get; set; }
       public IEnumerable<SelectListItem> GroupProfileState { get; set; } 
       //For Group Filter Profiles Group  list Name
       public IEnumerable<SelectListItem> GroupList { get; set; }
       //get all  Group Profile Data 
       public IEnumerable<MLGroupProfiles> GroupProfileData { get; set; }
       //For Group Filter Profiles Group Data for show list of group 
       public IEnumerable<MLGroup> GroupData { get; set; }
       //For MLGroup class for create group in Popup
       public MLGroup GroupDataEntity { get; set; }   
       // For Profile Type like-Manufacture,Whoalasler,Retailer Pharamcy,Chain Phanmcy
       public IEnumerable<SelectListItem> ProfileTpe { get; set; }
       //For Get Profile Code based on ProfileTpe
       public IEnumerable<SelectListItem> Profile { get; set; }
       //For New Group Name 
       public string GroupName { get; set; }
       public int? CurrentPageNo { get; set; }

    } 
}
