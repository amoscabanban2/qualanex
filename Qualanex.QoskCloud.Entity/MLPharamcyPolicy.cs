﻿using System;

namespace Qualanex.QoskCloud.Entity
{
   public class MLPharamcyPolicy
   {
      public long PolicyProfileID { get; set; }
      public long PolicyID { get; set; }
      public string PolicyType { get; set; }
      public long PolicyName { get; set; }
      public int PharamacyProfileCode { get; set; }
      public int PolicyForProfileCode { get; set; }
      public string PolicyForProfileName { get; set; }
      public string ProfileName { get; set; }
      public string Address { get; set; }
      public string Type { get; set; }
      public string City { get; set; }
      public string State { get; set; }
      public string RegionCode { get; set; }
      public string ZipCode { get; set; }
      public long ProductID { get; set; }
      public string Description { get; set; }
      public string Strength { get; set; }
      public long NDC { get; set; }
      public long UPC { get; set; }
      public string NDCUPCWithDashes { get; set; }
      public string LotNumber { get; set; }
      public DateTime? ExpirationDate { get; set; }
      public int GroupID { get; set; }
   }
}
