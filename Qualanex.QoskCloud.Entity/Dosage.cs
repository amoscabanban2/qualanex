﻿namespace Qualanex.QoskCloud.Entity
{
    public class Dosage
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
