﻿namespace Qualanex.QoskCloud.Entity
{
    public class ProductIDWithName
    {
        public long ProductID { get; set; }
        public string ProductName { get; set; }
    }
}
