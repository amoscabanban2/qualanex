// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.ProcessCreateAVRO.#CreateAVROFiles()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.ProcessCreateAVRO.#CreateZipFile()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetProductListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetProductImageDetailListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetLotNumbersListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetProfilesListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetPolicyListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetPolicyProfileTypeListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetRecallsListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetRecallsDetailsListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetDirectIndirectAccountsListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetReturnAuthorizationsReasonsListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetProductsWasteStreamProfilesListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetWasteStreamProfilesListOfData()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Qualanex.QoskCloud.FullDataSyncCreateAVRO.CreateAVROData.#GetPurchaseDetailsListOfData()")]
