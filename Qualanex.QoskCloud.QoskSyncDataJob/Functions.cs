﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Qualanex.QoskCloud.QoskSyncDataJob
{
    public class Functions
    {
        private static FullSyncRequest fullSyncRequest;

        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called fullsync.
        public static void ProcessQueueMessage([QueueTrigger(Constants.Azure_Queue_FullSync)] string message, TextWriter log)
        {
            log.Write("Processing Queue Message {0}", message);
            try
            {
                fullSyncRequest = JsonConvert.DeserializeObject<FullSyncRequest>(message);
                var kioskDataFullSync = new KioskDataFullSync();
                kioskDataFullSync.KioskDataProcessFullSync(fullSyncRequest);
                log.WriteLine("Processing Complete for Message {0}", message);
            }
            catch (System.Exception ex)
            {
                log.Write("Error: {0}", ex.ToString());
                throw;
            }
        }
    }
}
