﻿using Newtonsoft.Json;
using Qualanex.QoskCloud.Utility;
using Qualanex.QoskCloud.Utility.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qualanex.Qosk.Library.Model.DBModel;

namespace Qualanex.QoskCloud.QoskSyncDataJob
{
   public class KioskDataFullSync
   {
      BlobStorageHandler blobStorageHandler;
      /// <summary>
      /// Create the kiosk specific data for a full synchronization
      /// </summary>
      /// <param name="syncRequest">The Synchronization Request details.</param>
      /// <returns>Task completion status</returns>
      public void KioskDataProcessFullSync(FullSyncRequest syncRequest)
      {
         try
         {
            Logger.Info("ExecuteFullDataSync::KioskDataProcessFullSync() started");

            blobStorageHandler = new BlobStorageHandler(Constants.AzureAccount_KioskSync, syncRequest.RequestId.ToLower());

            using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
            {
               var appConfigs = (from appConfig in cloudEntities.AppConfig
                                 where (appConfig.QoskMachineID == null) || (appConfig.QoskMachineID == syncRequest.QoskMachineID)
                                 select appConfig);
               SaveModelData<AppConfig>(appConfigs);

               var qoskId = (from qosk in cloudEntities.Qosk
                             where qosk.MachineID == syncRequest.QoskMachineID
                             select qosk).SingleOrDefault().QoskID;

               var items = (from item in cloudEntities.Item
                            where item.QoskID == qoskId
                            select item);
               SaveModelData<Item>(items);

               var users = (from user in cloudEntities.QoskUser
                            where user.QoskID == qoskId
                            select user);
               SaveModelData<QoskUser>(users);

               var userRoleRels = (from userRoleRel in cloudEntities.QoskUserRoleRel
                                   where userRoleRel.QoskID == qoskId
                                   select userRoleRel);
               SaveModelData<QoskUserRoleRel>(userRoleRels);
            }
         }
         catch (Exception ex)
         {
            Logger.Error(ex);
            throw;
         }
         finally
         {
            Logger.Info("KioskDataProcessFullSync Complete.");
         }
      }

      /// <summary>
      /// Save the model data to a json file in blob storage for use by the Qosk
      /// </summary>
      /// <typeparam name="T">The data type for json to serialize</typeparam>
      /// <param name="dataList">the list of data items to serialize</param>
      private void SaveModelData<T>(IQueryable<T> dataList)
      {
         Logger.Info("ExecuteFullDataSync::SaveModelData()");

         try
         {
            var type = typeof(T);
            var json = JsonConvert.SerializeObject(dataList, type, Formatting.None,
               new JsonSerializerSettings()
               {
                  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });

            var typeName = type.Name;
            Logger.Info(string.Format("Saving data for type {0}", typeName));

            using (var stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)))
            {
               blobStorageHandler.UploadStreamAsync(stream, typeName + Constants.FileExtension_Json).Wait();
            }

            Logger.Info("ExecuteFullDataSync::SaveModelData() completed succesfully");
         }
         catch (Exception error)
         {
            Logger.Error(error);
            throw;
         }
         finally
         {
            Logger.Info("End SaveLocalData.");
         }

      }
   }
}
