﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;

namespace Qualanex.QoskCloud.ReturnJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called batchreturns.
        public static void ProcessQueueMessage([QueueTrigger(Constants.Azure_Queue_BatchReturns)] string message, TextWriter log)
        {
            log.WriteLine("Processing Queue Message {0}",message);

            try
            {
                QueueProcessing queueprocessing = new QueueProcessing();

                BatchReturnsRequest batchReturnsRequest = JsonConvert.DeserializeObject<BatchReturnsRequest>(message);
                log.WriteLine("Processing Returns for batch:{0} Kiosk:{1}", batchReturnsRequest.BatchId, batchReturnsRequest.KioskID);
                queueprocessing.ProcessReturn(batchReturnsRequest, log).Wait();
                log.WriteLine("Processing Complete for Message {0}", message);
            }
            catch (AggregateException ae)
            {
                foreach (var x in ae.InnerExceptions)
                {
                    log.WriteLine("Error: {0}", x.ToString());
                }
                throw ae.Flatten();
            }
            catch (Exception ex)
            {
                log.WriteLine("Error: {0}", ex.ToString());
                throw;
            }
        }
    }
}
