﻿using Microsoft.Azure.WebJobs;
using System.IO;

namespace Qualanex.QoskCloud.ReturnJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage for tracing
        static void Main()
        {
            System.Console.WriteLine("Main() function called in ReturnJob");
            using (JobHost host = new JobHost())
            {
                host.RunAndBlock();
            }
        }

        /*
        //For testing in local
        public static void ProcessQueueMessage([QueueTrigger("test")] string msg, TextWriter log)
        {
            System.Console.WriteLine("ProcessQueueMessage called for " + msg);
            log.WriteLine("ProcessQueueMessage called for " + msg);
        }
         */
        
    }
}
