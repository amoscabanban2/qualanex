﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Qualanex.Qosk.Library.Model.DBModel;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.ReturnJob
{
   class QueueProcessing
   {
      /// <summary>
      /// Insert Return Item into cloud database, move image/video files and remove .json file.
      /// </summary>
      /// <param name="returnRequest"></param>
      /// <param name="log"></param>
      /// <returns>Task</returns>
      public async Task ProcessReturn(BatchReturnsRequest returnRequest, TextWriter log)
      {
         try
         {
            string batchfolderName = returnRequest.BatchId.ToLower();  // Same as <itemguid>

            // The returns json, images and video files are placed in <kioskid> container prefixed with <itemguid> and the destination folder
            // is stored in the images | video with <itemguid> container.
            var imageBlobHandler = new BlobStorageHandler(Constants.AzureAccount_KioskSync, returnRequest.KioskID,
                Constants.AzureAccount_Image, returnRequest.BatchId.ToLower());
            var videoBlobHandler = new BlobStorageHandler(Constants.AzureAccount_KioskSync, returnRequest.KioskID,
                Constants.AzureAccount_Video, returnRequest.BatchId.ToLower());

            var item = await InsertReturnItemDataAsync(returnRequest, log, imageBlobHandler, videoBlobHandler);
            await MoveReturnItemMediaAsync(returnRequest, item, log, imageBlobHandler, videoBlobHandler);

            // Cleanup upload folder
            await videoBlobHandler.DeleteBlobAsync(item.ItemGUID.ToString().ToLower(), Constants.FileExtension_Json, log);
         }
         catch (AggregateException ae)
         {
            log.WriteLine($"Aggregate Error encountered processing return item");
            foreach (var e in ae.InnerExceptions)
               log.WriteLine($"Error: {e.ToString()}");
            throw ae.Flatten();
         }
         catch (Exception e)
         {
            log.WriteLine($"Error encountered processing return item");
            log.WriteLine(e.ToString());
            throw;
         }
      }

      private async Task<Item> InsertReturnItemDataAsync(BatchReturnsRequest returnRequest, TextWriter log,
          BlobStorageHandler imageBlobHandler, BlobStorageHandler videoBlobHandler)
      {
         string batchfolderName = returnRequest.BatchId.ToLower();  // Same as <itemguid>
         string fileName = $"{batchfolderName}{Constants.FileExtension_Json}";
         log.WriteLine($"Downloading file:{fileName}");

         Item item = null;
         try
         {
            var jsonItem = imageBlobHandler.DownloadString($"{fileName}");
            item = JsonConvert.DeserializeObject<Item>(jsonItem);
            if (item == null) throw new ApplicationException($"{fileName} cannot be deserialized!");
         }
         catch { throw new ApplicationException($"Return Item file:{fileName} doesn't exist!"); }

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            // TODO: Should we do a merge instead of an insert?  If the record exists, we are skipping for now... grf
            log.WriteLine($"Inserting Return item:{returnRequest.BatchId} from kiosk:{returnRequest.KioskID} into QoskCloud database");

            // Insert Return Item
            var resultItem = (from p in cloudEntities.Item where p.ItemGUID == item.ItemGUID select p).FirstOrDefault();
            if (resultItem == null)
            {
               cloudEntities.Item.Add(item);
               if ((await cloudEntities.SaveChangesAsync()) <= 0)
                  throw new ApplicationException($"Return item:{returnRequest.BatchId} data NOT stored in database!");
            }
            else log.WriteLine($"Return item {item.ItemGUID} exists in Items table, skipping insert or update");

            // Insert Return Item Non-credits
            fileName = $"{batchfolderName }$NCs{ Constants.FileExtension_Json}";
            log.WriteLine($"Downloading file:{fileName}");
            try
            {
               var jsonNonCredit = imageBlobHandler.DownloadString($"{fileName}");
               await InsertReturnItemNonCreditsAsync(item, jsonNonCredit, cloudEntities, log);
            }
            catch { log.WriteLine($"File:{fileName} doesn't exist, skipping Non-credits"); }

            // Insert Return Item Waste Codes
            fileName = $"{batchfolderName }$WCs{ Constants.FileExtension_Json}";
            log.WriteLine($"Downloading file:{fileName}");
            try
            {
               var jsonWasteCode = imageBlobHandler.DownloadString($"{fileName}");
               await InsertReturnItemWasteCodesAsync(item, jsonWasteCode, cloudEntities, log);
            }
            catch { log.WriteLine($"File:{fileName} doesn't exist, skipping Waste Codes"); }
         }
         return item;
      }

      private async Task InsertReturnItemNonCreditsAsync(Item item, string jsonNonCredit,
              Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud cloudEntities, TextWriter log)
      {
         if (jsonNonCredit != string.Empty)
         {
            var itemNonCredits = JsonConvert.DeserializeObject<List<ItemNonCreditRel>>(jsonNonCredit);
            log.WriteLine($"Inserting {itemNonCredits.Count} Return item:{item.BatchId} Non Credits into QoskCloud database");

            foreach (ItemNonCreditRel itemNonCredit in itemNonCredits)
            {
               var resultNonCredit = (from p in cloudEntities.ItemNonCreditRel
                                      where p.ItemGUID == itemNonCredit.ItemGUID && p.NonCreditCode == itemNonCredit.NonCreditCode
                                      select p).FirstOrDefault();
               if (resultNonCredit == null)
               {
                  cloudEntities.ItemNonCreditRel.Add(itemNonCredit);
                  if ((await cloudEntities.SaveChangesAsync()) <= 0)
                     throw new ApplicationException($"Return item:{item.BatchId} NonCredit:{itemNonCredit.NonCreditCode} data NOT stored in database!");
               }
               else log.WriteLine($"Return item {item.ItemGUID} NonCredit:{itemNonCredit.NonCreditCode} exists in ItemNonCreditRel table, skipping insert or update");
            }
         }
      }

      private async Task InsertReturnItemWasteCodesAsync(Item item, string jsonWasteCode,
              Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud cloudEntities, TextWriter log)
      {
         if (jsonWasteCode != string.Empty)
         {
            var itemWasteCodes = JsonConvert.DeserializeObject<List<ItemWasteCode>>(jsonWasteCode);
            log.WriteLine($"Inserting {itemWasteCodes.Count} Return item:{item.BatchId} Waste Codes into QoskCloud database");

            foreach (ItemWasteCode itemWasteCode in itemWasteCodes)
            {
               var resultNonCredit = (from p in cloudEntities.ItemWasteCode
                                      where p.ItemGUID == itemWasteCode.ItemGUID &&
                                           p.WasteCode == itemWasteCode.WasteCode &&
                                           p.StateCode == itemWasteCode.StateCode
                                      select p).FirstOrDefault();
               if (resultNonCredit == null)
               {
                  cloudEntities.ItemWasteCode.Add(itemWasteCode);
                  if ((await cloudEntities.SaveChangesAsync()) <= 0)
                     throw new ApplicationException($"Return item:{item.BatchId} WasteCode:{itemWasteCode.WasteCode} for " +
                         $"{itemWasteCode.StateCode} data NOT stored in database!");
               }
               else log.WriteLine($"Return item {item.ItemGUID} WasteCode:{itemWasteCode.WasteCode} for " +
                   $"{itemWasteCode.StateCode} exists in ItemWasteCode table, skipping insert or update");
            }
         }
      }

      private async Task MoveReturnItemMediaAsync(BatchReturnsRequest returnRequest, Item item, TextWriter log,
          BlobStorageHandler imageBlobHandler, BlobStorageHandler videoBlobHandler)
      {
         List<Task> taskList = new List<Task>();
         try
         {
            log.WriteLine($"Moving return media for item:{returnRequest.BatchId} to images and video blob");

            taskList.Add(imageBlobHandler.CopyBlobListAsync(item.ItemGUID.ToString().ToUpper(), Constants.FileExtension_JPG, log));
            taskList.Add(videoBlobHandler.CopyBlobListAsync(item.ItemGUID.ToString().ToUpper(), Constants.FileExtension_MP4, log));

            await Task.WhenAll(taskList);
            log.WriteLine($"Return media for item:{returnRequest.BatchId} moved successfully to images and video blob");
         }
         catch (AggregateException ae)
         {
            log.WriteLine($"Aggregate Error encountered for MoveReturnItemMediaAsync");
            foreach (var e in ae.InnerExceptions)
               log.WriteLine($"Error: {e.ToString()}");
            throw ae.Flatten();
         }
         catch (Exception x)
         {
            log.WriteLine($"Error encountered moving return media.");
            foreach (var task in taskList.Where(t => t.IsFaulted))
               log.WriteLine(task.Exception.ToString());
            log.WriteLine(x.ToString());
            throw;
         }
      }
   }
}