﻿using System;

namespace Qualanex.QoskCloud.ReturnJob
{
    internal class BatchReturnsRequest
    {
        public string RequestId { get; set; }
        public DateTime RequestTime { get; set; }
        public string RunMode { get; set; }
        public string KioskID { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string ProgramVersion { get; set; }
        public string BatchId { get; set; }
        public int ReturnCount { get; set; }
        public string VerificationHash { get; set; }
    }
}