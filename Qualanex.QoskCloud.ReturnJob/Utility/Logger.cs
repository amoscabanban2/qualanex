﻿using System;
using System.Diagnostics;

namespace Qualanex.QoskCloud.ReturnJob
{
  public static class Logger
    {
        public static void Error(string msg)
        {
            Trace.TraceError(msg);
        }

        public static void Error(string msg, Exception ex)
        {
            Trace.TraceError(msg);
            Trace.TraceError(ex.ToString());
        }

        public static void Error(Exception ex)
        {
            Trace.TraceError(ex.ToString());
        }

        public static void Error(string fmt, params object[] vars)
        {
            string msg = string.Format(fmt, vars);
            Trace.TraceError(msg.ToString());
        }

        public static void Info(string msg)
        {
            Trace.TraceInformation(msg);
        }

        public static void Warning(string msg)
        {
            Trace.TraceWarning(msg);
        }
    }
}
