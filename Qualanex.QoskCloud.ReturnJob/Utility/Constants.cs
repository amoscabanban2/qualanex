﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.ReturnJob
{
    public class Constants
    {
        public const string Double_Backward_Slash = "\\";
        public const string Single_Forward_Slash = "/";

        public const string FileExtension_Zip = ".zip";
        public const string FileExtension_Json = ".json";
        public const string FileExtension_JPG = ".jpg";
        public const string FileExtension_MP4 = ".mp4";

        public const string AzureContainer_Returns = "returns";
        public const string Azure_Queue_BatchReturns = "batchreturns";
        public const string Azure_ConnectionString_Format = "Storage.{0}.ConnectionString";
        public const string AzureAccount_Image = "qoskimages";
        public const string AzureAccount_Video = "qoskvideo";
        public const string AzureAccount_KioskSync = "qoskkiosksync";

        public const string DirectoryName_Images = "images";
        public const string DirectoryName_Videos = "videos";
    }
}
