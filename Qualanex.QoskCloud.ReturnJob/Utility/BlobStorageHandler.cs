﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.DataMovement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.ReturnJob
{
    public class BlobStorageHandler
    {
        /// <summary>
        /// Source container for blob operations
        /// </summary>
        [CLSCompliant(false)]
        public CloudBlobContainer SourceBlobContainer { get; private set; }

        /// <summary>
        /// Destination container for blob operations
        /// </summary>
        [CLSCompliant(false)]
        public CloudBlobContainer DestinationBlobContainer { get; private set; }

        /// <summary>
        /// Default constructor for the BlobStorageHandler class
        /// </summary>
        public BlobStorageHandler()
        {
            TransferManager.Configurations.ParallelOperations = 64;
        }

        /// <summary>
        /// Constructor for the BlobStorageHandler class
        /// </summary>
        /// <param name="sourceAccount">Source blob account string</param>
        /// <param name="sourceContainerName">Source blob container name</param>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public BlobStorageHandler(string sourceAccount, string sourceContainerName, string destinationAccount, string destinationContainerName) : base()
        {
            this.SourceBlobContainer = GetCloudBlobContainer(sourceContainerName, sourceAccount);
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// Constructor for the BlobStorageHandler class
        /// </summary>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public BlobStorageHandler(string destinationAccount, string destinationContainerName) : base()
        {
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// Set the source container
        /// </summary>
        /// <param name="sourceAccount">Source blob account string</param>
        /// <param name="sourceContainerName">Source blob container name</param>
        public void SetSourceContainer(string sourceAccount, string sourceContainerName)
        {
            this.SourceBlobContainer = GetCloudBlobContainer(sourceContainerName, sourceAccount);
        }

        /// <summary>
        /// Set the destination container
        /// </summary>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public void SetDestinationContainer(string destinationAccount, string destinationContainerName)
        {
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        public async Task CopyBlobListAsync(string prefix, string fileExtension, TextWriter log, bool overwrite = true)
        {
            List<Task> copytaskList = new List<Task>();
            List<Task> deltaskList = new List<Task>();
            var blobList = SourceBlobContainer.ListBlobs();

            try
            {
                foreach (CloudBlockBlob blob in blobList)
                {
                    if (blob.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase) == true && 
                        blob.Name.EndsWith(fileExtension, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        log.WriteLine($"Copying {blob.Name} from {SourceBlobContainer.Name} to {DestinationBlobContainer.Name}");

                        var sourceBlob = GetCloudBlob(SourceBlobContainer, blob.Name, blob.BlobType);
                        var destBlob = GetCloudBlob(DestinationBlobContainer, blob.Name, blob.BlobType);
                        if (await destBlob.ExistsAsync() == true)
                        {
                            if (overwrite == false)
                            {
                                log.WriteLine($"Media already exists in destination! Overwrite is set to false, skipping {blob.Name}");
                                continue; // skip this entry...
                            }
                            log.WriteLine($"Media already exists in destination! Deleting {blob.Name}");
                            await destBlob.DeleteAsync();
                        }

                        copytaskList.Add(TransferManager.CopyAsync(sourceBlob, destBlob, true));
                    }
                }
                await Task.WhenAll(copytaskList);
            }
            catch (AggregateException ae)
            {
                log.WriteLine($"Aggregate Error encountered for CopyBlobListAsync");
                foreach (var e in ae.InnerExceptions)
                    log.WriteLine($"Error: {e.ToString()}");
                throw ae.Flatten();
            }
            catch (Exception x)
            {
                foreach (var task in copytaskList.Where(t => t.IsFaulted))
                {
                    Logger.Error(task.Exception.ToString());
                }
                Logger.Error(x);
                throw;
            }

            try {
                // Now Remove all of the files in the source
                foreach (CloudBlockBlob blob in blobList)
                {
                    if (blob.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase) == true && 
                        blob.Name.EndsWith(fileExtension, StringComparison.OrdinalIgnoreCase) == true)
                    {

                        var sourceBlob = GetCloudBlob(SourceBlobContainer, blob.Name, blob.BlobType);
                        var destBlob = GetCloudBlob(DestinationBlobContainer, blob.Name, blob.BlobType);
                        if (await destBlob.ExistsAsync() == true)
                        {
                            log.WriteLine("Removing {0} from {1}", blob.Name, SourceBlobContainer.Name);
                            deltaskList.Add(sourceBlob.DeleteAsync());
                        }
                    }
                }
                await Task.WhenAll(deltaskList.Cast<Task>().ToArray());

            }
            catch (Exception x)
            {
                foreach (var task in deltaskList.Where(t => t.IsFaulted))
                {
                    Logger.Error(task.Exception.ToString());
                }
                Logger.Error(x);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        public async Task DeleteBlobAsync(string prefix, string fileExtension, TextWriter log)
        {
            try
            {
                var blobList = SourceBlobContainer.ListBlobs();
                foreach (CloudBlockBlob blob in blobList)
                {
                    if (blob.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase) == true && 
                        blob.Name.EndsWith(fileExtension, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        log.WriteLine("Removing {0} from {1}", blob.Name, SourceBlobContainer.Name);

                        var sourceBlob = GetCloudBlob(SourceBlobContainer, blob.Name, blob.BlobType);
                        await sourceBlob.DeleteAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// CopyDirectoryAsync does not appear to work.  Leaving code here for now in case we can figure it out.
        /// </summary>
        /// <param name="destinationDirectory"></param>
        /// <param name="sourceDirectory"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public async Task CopyDirectoryAsync(string destinationDirectory, string sourceDirectory, string pattern)
        {
            var sourceBlobDirectory = SourceBlobContainer.GetDirectoryReference(sourceDirectory);
            var destinationBlobDirectory = DestinationBlobContainer.GetDirectoryReference(destinationDirectory);

            CopyDirectoryOptions options = new CopyDirectoryOptions()
            {
                BlobType = BlobType.BlockBlob,
                SearchPattern = pattern,
            };

            await TransferManager.CopyDirectoryAsync(sourceBlobDirectory, destinationBlobDirectory, true, options, null);
        }

        /// <summary>
        /// Download a blob file to a byte stream.
        /// 
        /// NOTE: Call SetSourceContainer before calling this method.
        /// </summary>
        /// <param name="source">File name in blob storage to download.</param>
        /// <returns>Byte buffer containing downloaded file</returns>
        public byte[] DownloadStream(string source)
        {
            
            try
            {
                CloudBlob sourceBlob = GetCloudBlob(SourceBlobContainer, source, BlobType.BlockBlob);
                using (MemoryStream destinationStream = new MemoryStream())
                {
                    TransferManager.DownloadAsync(sourceBlob, destinationStream).Wait();
                    return destinationStream.GetBuffer();
                }
            }
            catch (Exception x)
            {
                Logger.Error(x);
                throw;
            }
        }

        /// <summary>
        /// Download a blob file to a byte stream.
        /// 
        /// NOTE: Call SetSourceContainer before calling this method.
        /// </summary>
        /// <param name="source">File name in blob storage to download.</param>
        /// <returns>String containing downloaded file</returns>
        public string DownloadString(string source)
        {
            try
            {
                var blobBytes = DownloadStream(source);
                return blobBytes.Length == 0 ? string.Empty : Encoding.UTF8.GetString(blobBytes);
            }
            catch (Exception x)
            {
                Logger.Error(x);
                throw;
            }
        }

        /// <summary>
        /// Gets a reference to a blob
        /// </summary>
        /// <param name="container">The CloudBlobContainer instance</param>
        /// <param name="blobName">The file name of the blob to reference</param>
        /// <param name="blobType">The blob type</param>
        /// <returns></returns>
        private CloudBlob GetCloudBlob(CloudBlobContainer container, string blobName, BlobType blobType)
        {
            CloudBlob cloudBlob;
            switch (blobType)
            {
                case BlobType.AppendBlob:
                    cloudBlob = container.GetAppendBlobReference(blobName);
                    break;
                case BlobType.BlockBlob:
                    cloudBlob = container.GetBlockBlobReference(blobName);
                    break;
                case BlobType.PageBlob:
                    cloudBlob = container.GetPageBlobReference(blobName);
                    break;
                case BlobType.Unspecified:
                default:
                    throw new ArgumentException(string.Format("Invalid blob type {0}", blobType.ToString()), "blobType");
            }

            return cloudBlob;
        }


        /// <summary>
        /// Get the cloud blob container instance and creates it if it does not exist
        /// </summary>
        /// <param name="container">The container name</param>
        /// <param name="accountName">The account name</param>
        /// <returns></returns>
        private CloudBlobContainer GetCloudBlobContainer(string container, string accountName)
        {
            var connectString = ConfigurationManager.GetConnectionString(accountName);
            CloudStorageAccount account = CloudStorageAccount.Parse(connectString);
            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer sampleContainer = client.GetContainerReference(container.ToLower());
            sampleContainer.CreateIfNotExists();
            return sampleContainer;
        }
    }
}
