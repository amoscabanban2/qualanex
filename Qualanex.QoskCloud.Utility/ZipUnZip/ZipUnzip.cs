﻿using System;
using System.Text;
using System.IO.Compression;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{
   public class ZipUnzip
    {
        /// <summary>
        /// UnZip EncryptedZip based on selected folder
        /// </summary>
        /// <param name="zipFileToUnzip"></param>
        /// <param name="outputFolder"></param>
        public void UnZipContent(string zipFolderToUnzip, ref string unzipFolderName)
        {
            StringBuilder traceInformation = new StringBuilder();
            traceInformation.Append("UnZipEncryptedZip method is called");
            try
            {
                ZipFile.ExtractToDirectory(zipFolderToUnzip, unzipFolderName);
            }
            catch (Exception error) { Logger.Error(error.Message); throw; }
            finally
            {
                Logger.Info(traceInformation.ToString());
                traceInformation = null;
            }
        }

        /// <summary>
        /// Create a compressed content file from the source file
        /// </summary>
        /// <param name="sourecfileDirectory">Source file directory name to compress</param>
        /// <param name="filename">Name of the resulting compressed file</param>
        /// <returns>Boolean indicating success</returns>
        public bool CreateZipFolder(string sourecfileDirectory,string filename)
       {
           try
           {
               ZipFile.CreateFromDirectory(sourecfileDirectory, filename + Constants.FileExtension_Zip);
               return true;
           }
           catch (Exception Ex)
           {
               Logger.Error(Ex);
               return false;
           }
          
       }

        /// <summary>
        /// Compress a string and return the resulting array of bytes
        /// </summary>
        /// <param name="script">String to Compress</param>
        /// <returns>Array of bytes containing compressed string</returns>
        public byte[] ZipString(string script)
        {
            BinaryFormatter bf = new BinaryFormatter();

            MemoryStream resultStream = new MemoryStream();
            using (DeflateStream compressStream = new DeflateStream(resultStream, CompressionMode.Compress, true))
            {
                bf.Serialize(compressStream, script);
                return resultStream.ToArray();
            }
        }

        /// <summary>
        /// Compress a single file
        /// </summary>
        /// <param name="path">Path to file to be compressed including file name and extension</param>
        /// <returns>Path to compressed file</returns>
        public string ZipSingleFile(string path)
        {
            var zipFile = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + Constants.FileExtension_Zip);
            var fileName = Path.GetFileName(path);

            using (var zip = ZipFile.Open(zipFile, ZipArchiveMode.Create))
            {
                zip.CreateEntryFromFile(path, fileName);
            }

            return zipFile;
        }
    }
}
