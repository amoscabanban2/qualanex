﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{
    /// <summary>
    /// Handler for various direct SQL Server operations
    /// </summary>
    public class DatabaseHandler
    {
        private string connectString;
        private FileHandler fileHandler = new FileHandler();

        /// <summary>
        /// Constructor DatabaseHandler
        /// </summary>
        /// <param name="ConnectString">The Entity Framework connection string.</param>
        public DatabaseHandler(string ConnectString)
        {
            connectString = ConnectString;
        }

        /// <summary>
        /// Create a script file containing the database structure and data depending on the SyncTable settings
        /// </summary>
        /// <param name="path">Temporary path to store script files</param>
        /// <param name="syncTable">Synchronization table options</param>
        /// <returns>Path to the script file</returns>
        public string CreateScriptToFile(string path, SyncTable syncTable)
        {
            using (var ec = new EntityConnection(connectString))
            {
                using (var connection = ec.StoreConnection as SqlConnection)
                {
                    try
                    {
                        connection.Open();
                        var serverConnection = new ServerConnection(connection);
                        var server = new Server(serverConnection);
                        var scripter = new Scripter(server);

                        var database = server.Databases[Constants.DBModel_ConnectionString_DefaultInstance];

                        scripter.Options.ScriptData = syncTable.SyncType == SyncTypeEnum.SharedTable;
                        scripter.Options.ToFileOnly = true;
                        scripter.Options.FileName = fileHandler.GetTempFileName(path, syncTable.TableName, Constants.FileExtension_Script);

                        if (database.Tables.Contains(syncTable.TableName) == false)
                        {
                            Logger.Warning(string.Format("Table {0} does not exist on server database.", syncTable.TableName));
                            return scripter.Options.FileName;
                        }

                        var table = database.Tables[syncTable.TableName];

                        scripter.EnumScript(new SqlSmoObject[] { table });

                        return scripter.Options.FileName;
                    }
                    catch (Exception x)
                    {
                        Logger.Error(x.ToString());
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Create a file containing Kiosk specific data
        /// </summary>
        /// <param name="path">Temporary path to store script files</param>
        /// <param name="syncTable">Synchronization table options</param>
        /// <param name="syncRequest">Synchronization request object containing the Kiosk Identifier</param>
        /// <returns>Path to the Kiosk specific data file</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public string CreateKioskSpecificDataToFile(string path, SyncTable syncTable, SyncRequest syncRequest)
        {
            if (syncTable.SyncType != SyncTypeEnum.KioskSpecificTable)
            {
                throw new ArgumentException("Synchronization type must be SyncTypeEnum.KioskSpecificTable: " + syncTable.TableName, "syncTable.SyncType");
            }

            if (syncTable.KioskIdColumnName == null)
            {
                throw new ArgumentException("Synchronization KioskIDColumnName cannot be null: " + syncTable.TableName, "syncTable.KioskIDColumnName");
            }

            if (syncRequest.QoskMachineID == null)
            {
                throw new ArgumentException("KioskID cannot be null", "syncRequest.KioskID");
            }

            using (var ec = new EntityConnection(connectString))
            {
                using (var connection = ec.StoreConnection as SqlConnection)
                {
                    try
                    {
                        var sql = string.Format("select * from {0} where @KioskIdColumnName = @KioskId for JSON Path", syncTable.TableName);
                        using (var command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@KioskIdColumnName", syncTable.KioskIdColumnName);
                            command.Parameters.AddWithValue("@KioskId", syncRequest.QoskMachineID);
                            connection.Open();
                            using (var reader = command.ExecuteReader())
                            {
                                var fileName = Path.Combine(path, syncRequest.QoskMachineID + syncTable.TableName + "Data" + Constants.FileExtension_Data);
                                using (StreamWriter writer = new StreamWriter(fileName))
                                {
                                    while (reader.Read())
                                    {
                                        writer.Write(reader[0]);
                                    }
                                }
                                return fileName;
                            }
                        }
                    }
                    catch (Exception x)
                    {
                        Logger.Error(x.ToString());
                        throw;
                    }
                }
            }
        }
    }
}
