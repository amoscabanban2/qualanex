﻿
namespace Qualanex.QoskCloud.Utility
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   using AutoMapper;

   using Qualanex.Qosk.ServiceEntities;
   using Qualanex.Qosk.Library.Model.DBModel;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public sealed class CreateAVROData
   {

      ///****************************************************************************
      /// <summary>
      ///   Will get Product information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Products> GetProductListOfData()
      {
         var listProducts = new List<Qualanex.Qosk.ServiceEntities.Products>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var products = (from l in cloudEntities.Product select l);

            listProducts.AddRange(products.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.Products())));
         }

         return listProducts;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get ProductImageDetail information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.ProductImage> GetProductImageListOfData()
      {
         var listProductImages = new List<Qualanex.Qosk.ServiceEntities.ProductImage>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var productImage = (from l in cloudEntities.ProductImage select l);

            listProductImages.AddRange(productImage.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.ProductImage())));
         }

         return listProductImages;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get LotNumbers information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.LotNumbers> GetLotNumbersListOfData()
      {
         var listLotNumbers = new List<Qualanex.Qosk.ServiceEntities.LotNumbers>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var lotNumbers = (from l in cloudEntities.Lot select l);

            listLotNumbers.AddRange(lotNumbers.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.LotNumbers())));
         }

         return listLotNumbers;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Profiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Profiles> GetProfilesListOfData()
      {
         var listProfiles = new List<Qualanex.Qosk.ServiceEntities.Profiles>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var profiles = (from l in cloudEntities.Profile select l);

            listProfiles.AddRange(profiles.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.Profiles())));
         }

         return listProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Policy information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Policy> GetPolicyListOfData()
      {
         var listPolicies = new List<Qualanex.Qosk.ServiceEntities.Policy>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var policies = (from l in cloudEntities.Policy select l);

            listPolicies.AddRange(policies.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.Policy())));
         }

         return listPolicies;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get PolicyProfileType information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.PolicyRel> GetPolicyProfileTypeListOfData()
      {
         var listPolicyRelations = new List<Qualanex.Qosk.ServiceEntities.PolicyRel>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var policyProfileTypes = (from l in cloudEntities.PolicyRel select l);

            listPolicyRelations.AddRange(policyProfileTypes.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.PolicyRel())));
         }

         return listPolicyRelations;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Recalls information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Recalls> GetRecallsListOfData()
      {
         var listRecalls = new List<Qualanex.Qosk.ServiceEntities.Recalls>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var recalls = (from l in cloudEntities.Recall select l);

            listRecalls.AddRange(recalls.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.Recalls())));
         }

         return listRecalls;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get ProductsWasteStreamProfiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles> GetProductsWasteStreamProfilesListOfData()
      {
         var listProductWasteStreamProfiles = new List<Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var productWasteStreamProfiles = (from l in cloudEntities.ProductWasteStreamProfile select l);

            foreach (var item in productWasteStreamProfiles.ToList())
            {
               listProductWasteStreamProfiles.Add(Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles()));
            }
         }

         return listProductWasteStreamProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get WasteStreamProfiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.WasteStreamProfiles> GetWasteStreamProfilesListOfData()
      {
         var listWasteStreamProfiles = new List<Qualanex.Qosk.ServiceEntities.WasteStreamProfiles>();

         //using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   var wasteStreamProfiles = (from l in cloudEntities.WasteStreamProfile select l);

         //   listWasteStreamProfiles.AddRange(wasteStreamProfiles.ToList().Select(item => Utility.CommonFunctions.MapListObject(item, new Qualanex.Qosk.ServiceEntities.WasteStreamProfiles())));
         //}

         return listWasteStreamProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="syncRequest"></param>
      ///****************************************************************************
      public static void StartPartialSync(SyncRequest syncRequest)
      {
         syncRequest.Status = SyncStatus.Pending;

         // TODO: Fix commented code
         //using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   cloudEntities.PartialSync.Add(new PartialSync()
         //   {
         //      QoskID = syncRequest.KioskID,
         //      RequestedTime = syncRequest.RequestTime,
         //      RequestId = new Guid(syncRequest.RequestId),
         //      Status = syncRequest.Status.ToString(),
         //      ProgramVersion = syncRequest.ProgramVersion,
         //      LastUpdateDate = syncRequest.LastUpdateDate
         //   });

         //   cloudEntities.SaveChanges();
         //}
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="syncRequest"></param>
      ///****************************************************************************
      public static void UpdatePartialSyncStatus(SyncRequest syncRequest)
      {
         // TODO: Fix commented code
         //using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   var syncData = (from partialSync in cloudEntities.PartialSync
         //                   where partialSync.RequestId == new Guid(syncRequest.RequestId) && partialSync.QoskID == syncRequest.KioskID
         //                   select partialSync).FirstOrDefault();

         //   syncData.Status = syncRequest.Status.ToString();
         //   cloudEntities.SaveChanges();
         //}
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Product information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Products> GetProductListOfDataPartially(SyncRequest syncRequest)
      {
         var listProductImageDetails = new List<Qualanex.Qosk.ServiceEntities.Products>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var products = (from l in cloudEntities.Product
                            where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                  && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                            select l);

            listProductImageDetails.AddRange(products.ToList().Select(item => CreateAVROData.MapProductObject(item, new Qualanex.Qosk.ServiceEntities.Products())));
         }

         return listProductImageDetails;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.Products MapProductObject(Qualanex.Qosk.Library.Model.DBModel.Product source, Qualanex.Qosk.ServiceEntities.Products destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.Product, Qualanex.Qosk.ServiceEntities.Products>();

         return Mapper.Map<Qualanex.Qosk.ServiceEntities.Products>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get ProductImageDetail information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.ProductImage> GetProductImageListOfDataPartially(SyncRequest syncRequest)
      {
         var listProductImages = new List<Qualanex.Qosk.ServiceEntities.ProductImage>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var productImageDetails = (from l in cloudEntities.ProductImage
                                       where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                             && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                                       select l);

            listProductImages.AddRange(productImageDetails.ToList().Select(item => MapProductImageObject(item, new Qualanex.Qosk.ServiceEntities.ProductImage())));
         }

         return listProductImages;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.ProductImage MapProductImageObject(Qualanex.Qosk.Library.Model.DBModel.ProductImage source, Qualanex.Qosk.ServiceEntities.ProductImage destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.ProductImage, Qualanex.Qosk.ServiceEntities.ProductImage>();

         return Mapper.Map<Qualanex.Qosk.ServiceEntities.ProductImage>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get LotNumbers information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.LotNumbers> GetLotNumbersListOfDataPartially(SyncRequest syncRequest)
      {
         var listLotNumbers = new List<Qualanex.Qosk.ServiceEntities.LotNumbers>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var lotNumbers = (from l in cloudEntities.Lot
                              where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                    && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                              select l);

            listLotNumbers.AddRange(lotNumbers.ToList().Select(item => MapLotNumbersObject(item, new LotNumbers())));
         }

         return listLotNumbers;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.LotNumbers MapLotNumbersObject(Lot source, Qualanex.Qosk.ServiceEntities.LotNumbers destination)
      {
         Mapper.CreateMap<Lot, Qualanex.Qosk.ServiceEntities.LotNumbers>();

         return Mapper.Map<Qualanex.Qosk.ServiceEntities.LotNumbers>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Profiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Profiles> GetProfilesListOfDataPartially(SyncRequest syncRequest)
      {
         var listProfiles = new List<Qualanex.Qosk.ServiceEntities.Profiles>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var profiles = (from l in cloudEntities.Profile
                            where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                  && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                            select l);

            listProfiles.AddRange(profiles.ToList().Select(item => MapProfilesObject(item, new Qualanex.Qosk.ServiceEntities.Profiles())));
         }

         return listProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.Profiles MapProfilesObject(Qualanex.Qosk.Library.Model.DBModel.Profile source, Qualanex.Qosk.ServiceEntities.Profiles destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.Profile, Qualanex.Qosk.ServiceEntities.Profiles>();

         return Mapper.Map<Qualanex.Qosk.ServiceEntities.Profiles>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Policy information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Policy> GetPolicyListOfDataPartially(SyncRequest syncRequest)
      {
         var listPolicies = new List<Qualanex.Qosk.ServiceEntities.Policy>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var policies = (from l in cloudEntities.Policy
                            where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                  && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                            select l);

            listPolicies.AddRange(policies.ToList().Select(item => MapPolicyObject(item, new Qualanex.Qosk.ServiceEntities.Policy())));
         }

         return listPolicies;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.Policy MapPolicyObject(Qualanex.Qosk.Library.Model.DBModel.Policy source, Qualanex.Qosk.ServiceEntities.Policy destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.Policy, Qualanex.Qosk.ServiceEntities.Policy>();
         return Mapper.Map<Qualanex.Qosk.ServiceEntities.Policy>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get PolicyProfileType information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.PolicyRel> GetPolicyProfileTypeListOfDataPartially(SyncRequest syncRequest)
      {
         var listPolicyRelations = new List<Qualanex.Qosk.ServiceEntities.PolicyRel>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var policyRelations = (from l in cloudEntities.PolicyRel
                                   where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                         && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                                   select l);

            listPolicyRelations.AddRange(policyRelations.ToList().Select(item => MapPolicyRelObject(item, new Qualanex.Qosk.ServiceEntities.PolicyRel())));
         }

         return listPolicyRelations;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.PolicyRel MapPolicyRelObject(Qualanex.Qosk.Library.Model.DBModel.PolicyRel source, Qualanex.Qosk.ServiceEntities.PolicyRel destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.PolicyRel, Qualanex.Qosk.ServiceEntities.PolicyRel>();
         return Mapper.Map<Qualanex.Qosk.ServiceEntities.PolicyRel>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get Recalls information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.Recalls> GetRecallsListOfDataPartially(SyncRequest syncRequest)
      {
         var listRecalls = new List<Qualanex.Qosk.ServiceEntities.Recalls>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var recalls = (from l in cloudEntities.Recall
                           where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                 && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                           select l);

            listRecalls.AddRange(recalls.ToList().Select(item => MapRecallsObject(item, new Qualanex.Qosk.ServiceEntities.Recalls())));
         }

         return listRecalls;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.Recalls MapRecallsObject(Qualanex.Qosk.Library.Model.DBModel.Recall source, Qualanex.Qosk.ServiceEntities.Recalls destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.Recall, Qualanex.Qosk.ServiceEntities.Recalls>();
         return Mapper.Map<Qualanex.Qosk.ServiceEntities.Recalls>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get ProductsWasteStreamProfiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles> GetProductsWasteStreamProfilesListOfDataPartially(SyncRequest syncRequest)
      {
         var listProductsWasteStreamProfiles = new List<Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles>();

         using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         {
            var requestDateTime = syncRequest.RequestTime;

            var productWasteStreamProfiles = (from l in cloudEntities.ProductWasteStreamProfile
                                              where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
                                                    && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
                                              select l);

            listProductsWasteStreamProfiles.AddRange(productWasteStreamProfiles.ToList().Select(item => MapProductsWasteStreamProfilesObject(item, new Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles())));
         }

         return listProductsWasteStreamProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <param name="destination"></param>
      /// <returns></returns>
      ///****************************************************************************
      private static Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles MapProductsWasteStreamProfilesObject(Qualanex.Qosk.Library.Model.DBModel.ProductWasteStreamProfile source, Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles destination)
      {
         Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.ProductWasteStreamProfile, Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles>();
         return Mapper.Map<Qualanex.Qosk.ServiceEntities.ProductWasteStreamProfiles>(source);
      }

      ///****************************************************************************
      /// <summary>
      ///   Will get WasteStreamProfiles information from database
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public static List<Qualanex.Qosk.ServiceEntities.WasteStreamProfiles> GetWasteStreamProfilesListOfDataPartially(SyncRequest syncRequest)
      {
         var listWasteStreamProfiles = new List<Qualanex.Qosk.ServiceEntities.WasteStreamProfiles>();

         //using (var cloudEntities = new Qualanex.Qosk.Library.Model.QoskCloud.QoskCloud())
         //{
         //   var requestDateTime = syncRequest.RequestTime;

         //   var wasteStreamProfiles = (from l in cloudEntities.WasteStreamProfile
         //                              where l.CreatedDate > syncRequest.LastUpdateDate || l.ModifiedDate > syncRequest.LastUpdateDate
         //                                    && (l.CreatedDate <= syncRequest.RequestTime || l.ModifiedDate <= syncRequest.RequestTime)
         //                              select l);

         //   listWasteStreamProfiles.AddRange(wasteStreamProfiles.ToList().Select(item => MapWasteStreamProfilesObject(item, new Qualanex.Qosk.ServiceEntities.WasteStreamProfiles())));
         //}

         return listWasteStreamProfiles;
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="source"></param>
      /// <returns></returns>
      ///****************************************************************************
      //private static Qualanex.Qosk.ServiceEntities.WasteStreamProfiles MapWasteStreamProfilesObject(Qualanex.Qosk.Library.Model.DBModel.WasteStreamProfile source, Qualanex.Qosk.ServiceEntities.WasteStreamProfiles destination)
      //{
      //   Mapper.CreateMap<Qualanex.Qosk.Library.Model.DBModel.WasteStreamProfile, Qualanex.Qosk.ServiceEntities.WasteStreamProfiles>();
      //   return Mapper.Map<Qualanex.Qosk.ServiceEntities.WasteStreamProfiles>(source);
      //}

   }
}
