﻿using System;
using System.Collections.Generic;
using System.Text;
using Qualanex.Qosk.ServiceEntities;
using System.IO;

namespace Qualanex.QoskCloud.Utility
{
   public class ProcessCreateAVRO
   {
      private string avroFilePath;
      private string zipFileName;
      private string contentImageFolder;
      private string originalImageFolder;

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public string ProductImageFolderPath { get; private set; }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public ProcessCreateAVRO()
      {
         string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

         avroFilePath = Path.Combine(ApplicationPath, Constants.DirectoryName_AVROFiles);

         if (!Directory.Exists(avroFilePath))
         {
            Directory.CreateDirectory(avroFilePath);
         }
         zipFileName = Path.Combine(ApplicationPath, Constants.FileName_Qosklatest);

         ProductImageFolderPath = Path.Combine(avroFilePath, Constants.DirectoryName_ProductImages);
         if (!Directory.Exists(ProductImageFolderPath))
         {
            Directory.CreateDirectory(ProductImageFolderPath);
         }

         originalImageFolder = Path.Combine(ProductImageFolderPath, Constants.DirectoryName_Images, Constants.DirectoryName_ProductImage, Constants.DirectoryName_Original);
         if (!Directory.Exists(originalImageFolder))
         {
            Directory.CreateDirectory(originalImageFolder);
         }

         contentImageFolder = Path.Combine(ProductImageFolderPath, Constants.DirectoryName_Images, Constants.DirectoryName_ProductImage, Constants.DirectoryName_Content);
         if (!Directory.Exists(contentImageFolder))
         {
            Directory.CreateDirectory(contentImageFolder);
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Create avro files on server
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public Responses<Status> CreateAVROFiles(ref List<ProductImage> objProductImageDetails)
      {
         Responses<Status> responses = new Responses<Status>();
         StringBuilder traceInformation = new StringBuilder();

         try
         {
            traceInformation.AppendLine("Start Method CreateAVROFiles ProcessCreateAVRO.");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Products + ".avro");
            List<Products> objProducts = CreateAVROData.GetProductListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(objProducts, avroFilePath, FilesToProcess.Products + Constants.FileExtension_Avro);
            objProducts.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Products + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.ProductImageDetail + ".avro");
            objProductImageDetails = CreateAVROData.GetProductImageListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(objProductImageDetails, avroFilePath, FilesToProcess.ProductImageDetail + Constants.FileExtension_Avro);

            traceInformation.AppendLine("Created file " + FilesToProcess.ProductImageDetail + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.LotNumbers + ".avro");
            List<LotNumbers> lstLotNumbers = CreateAVROData.GetLotNumbersListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstLotNumbers, avroFilePath, FilesToProcess.LotNumbers + Constants.FileExtension_Avro);
            lstLotNumbers.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.LotNumbers + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Profiles + ".avro");
            List<Profiles> lstProfiles = CreateAVROData.GetProfilesListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstProfiles, avroFilePath, FilesToProcess.Profiles + Constants.FileExtension_Avro);
            lstProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Profiles + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Policy + ".avro");
            List<Policy> lstPolicy = CreateAVROData.GetPolicyListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstPolicy, avroFilePath, FilesToProcess.Policy + Constants.FileExtension_Avro);
            lstPolicy.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Policy + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.PolicyProfileType + ".avro");
            List<PolicyRel> lstPolicyProfileType = CreateAVROData.GetPolicyProfileTypeListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstPolicyProfileType, avroFilePath, FilesToProcess.PolicyProfileType + Constants.FileExtension_Avro);
            lstPolicyProfileType.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.PolicyProfileType + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Recalls + ".avro");
            List<Recalls> lstRecalls = CreateAVROData.GetRecallsListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstRecalls, avroFilePath, FilesToProcess.Recalls + Constants.FileExtension_Avro);
            lstRecalls.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Recalls + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.ProductsWasteStreamProfiles + ".avro");
            List<ProductWasteStreamProfiles> lstProductsWasteStreamProfiles = CreateAVROData.GetProductsWasteStreamProfilesListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstProductsWasteStreamProfiles, avroFilePath, FilesToProcess.ProductsWasteStreamProfiles + Constants.FileExtension_Avro);
            lstProductsWasteStreamProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.ProductsWasteStreamProfiles + ".avro");

            traceInformation.AppendLine("Creating file " + FilesToProcess.WasteStreamProfiles + ".avro");
            List<WasteStreamProfiles> lstWasteStreamProfiles = CreateAVROData.GetWasteStreamProfilesListOfData();
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstWasteStreamProfiles, avroFilePath, FilesToProcess.WasteStreamProfiles + Constants.FileExtension_Avro);
            lstWasteStreamProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.WasteStreamProfiles + ".avro");

            responses.status = Status.OK;
         }
         catch (Exception error)
         {
            Logger.Error(error.Message);
            responses.status = Status.Fail;
         }
         finally
         {
            traceInformation.AppendLine("End Method CreateAVROFiles ProcessCreateAVRO.");
            Logger.Info(traceInformation.ToString());
         }
         return responses;
      }

      ///****************************************************************************
      /// <summary>
      ///   Creating Zip for avro files
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public Responses<string> CreateZipFile()
      {
         Responses<string> responses = new Responses<string>();
         StringBuilder traceInformation = new StringBuilder();

         try
         {
            traceInformation.AppendLine("Start Method CreateZipFile ProcessCreateAVRO.");
            ZipUnzip objZipUnzip = new ZipUnzip();
            objZipUnzip.CreateZipFolder(avroFilePath, zipFileName);
            responses.status = Status.OK;
            responses.responses = zipFileName + Constants.FileExtension_Zip;
         }
         catch (Exception error)
         {
            Logger.Error(error.Message);
            responses.status = Status.Fail;
         }
         finally
         {
            traceInformation.AppendLine("End Method CreateZipFile ProcessCreateAVRO.");
            Logger.Info(traceInformation.ToString());
         }
         return responses;
      }

   }
}
