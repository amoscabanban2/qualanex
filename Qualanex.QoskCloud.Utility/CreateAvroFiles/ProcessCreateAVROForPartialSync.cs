﻿using System;
using System.Collections.Generic;
using System.Text;
using Qualanex.Qosk.ServiceEntities;
using System.IO;

namespace Qualanex.QoskCloud.Utility
{
   public class ProcessCreateAVROForPartialSync
   {
      private string avroFilePathForPartialSync;
      private string zipFileNameForPartialSync;
      private string contentImageFolder;
      private string originalImageFolder;

      public string ProductImageFolderPath { get; private set; }

      private SyncRequest syncRequest = null;

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///
      /// </summary>
      /// <param name="SyncRequest"></param>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public ProcessCreateAVROForPartialSync(SyncRequest SyncRequest)
      {

         syncRequest = SyncRequest;
         string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

         avroFilePathForPartialSync = Path.Combine(ApplicationPath, Constants.DirectoryName_AVROFilesForPartialSync);
         if (!Directory.Exists(avroFilePathForPartialSync))
         {
            Directory.CreateDirectory(avroFilePathForPartialSync);
         }

         zipFileNameForPartialSync = Path.Combine(ApplicationPath, Constants.DirectoryName_ZipFileForPartialSync, Constants.FileName_QosklatestForPartialSync);
         if (!Directory.Exists(zipFileNameForPartialSync))
         {
            Directory.CreateDirectory(zipFileNameForPartialSync);
         }

         ProductImageFolderPath = Path.Combine(avroFilePathForPartialSync, Constants.DirectoryName_ProductImages);
         if (!Directory.Exists(ProductImageFolderPath))
         {
            Directory.CreateDirectory(ProductImageFolderPath);
         }

         originalImageFolder = Path.Combine(ProductImageFolderPath, Constants.DirectoryName_Images, Constants.DirectoryName_ProductImage, Constants.DirectoryName_Original);
         if (!Directory.Exists(originalImageFolder))
         {
            Directory.CreateDirectory(originalImageFolder);
         }

         contentImageFolder = Path.Combine(ProductImageFolderPath, Constants.DirectoryName_Images, Constants.DirectoryName_ProductImage, Constants.DirectoryName_Content);
         if (!Directory.Exists(contentImageFolder))
         {
            Directory.CreateDirectory(contentImageFolder);
         }

      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="utcDate"></param>
      /// <returns></returns>
      ///****************************************************************************
      static long GetEpocTime(DateTime utcDate)
      {
         return (long)(utcDate - new DateTime(1970, 1, 1)).TotalSeconds;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public bool IsUpdateAvailable()
      {
         List<Products> lstProducts = CreateAVROData.GetProductListOfDataPartially(syncRequest);
         if (lstProducts.Count > 0)
         {
            return true;
         }
         List<ProductImage> lstProductImageDetails = CreateAVROData.GetProductImageListOfDataPartially(syncRequest);
         if (lstProductImageDetails.Count > 0)
         {
            return true;
         }
         List<LotNumbers> lstLotNumbers = CreateAVROData.GetLotNumbersListOfDataPartially(syncRequest);
         if (lstLotNumbers.Count > 0)
         {
            return true;
         }
         List<Profiles> lstProfiles = CreateAVROData.GetProfilesListOfDataPartially(syncRequest);
         if (lstProfiles.Count > 0)
         {
            return true;
         }
         List<Policy> lstPolicy = CreateAVROData.GetPolicyListOfDataPartially(syncRequest);
         if (lstPolicy.Count > 0)
         {
            return true;
         }
         List<PolicyRel> lstPolicyProfileType = CreateAVROData.GetPolicyProfileTypeListOfDataPartially(syncRequest);
         if (lstPolicyProfileType.Count > 0)
         {
            return true;
         }
         List<Recalls> lstRecalls = CreateAVROData.GetRecallsListOfDataPartially(syncRequest);
         if (lstRecalls.Count > 0)
         {
            return true;
         }
         List<ProductWasteStreamProfiles> lstProductsWasteStreamProfiles = CreateAVROData.GetProductsWasteStreamProfilesListOfDataPartially(syncRequest);
         if (lstProductsWasteStreamProfiles.Count > 0)
         {
            return true;
         }
         List<WasteStreamProfiles> lstWasteStreamProfiles = CreateAVROData.GetWasteStreamProfilesListOfDataPartially(syncRequest);
         if (lstWasteStreamProfiles.Count > 0)
         {
            return true;
         }
         return false;
      }

      ///****************************************************************************
      /// <summary>
      ///   Create avro files on server
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public Responses<Status> CreateAVROFilesForPartial(ref List<ProductImage> objProductImageDetails)
      {
         Responses<Status> responses = new Responses<Status>();
         StringBuilder traceInformation = new StringBuilder();

         try
         {

            CreateAVROData.StartPartialSync(syncRequest);

            traceInformation.AppendLine("Start Method CreateAVROFiles ProcessCreateAVRO.");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Products + ".avro" + "for Partial Sync");
            List<Products> objProducts = CreateAVROData.GetProductListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(objProducts, avroFilePathForPartialSync, FilesToProcess.Products + Constants.FileExtension_Avro);
            objProducts.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Products + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.ProductImageDetail + ".avro");
            objProductImageDetails = CreateAVROData.GetProductImageListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(objProductImageDetails, avroFilePathForPartialSync, FilesToProcess.ProductImageDetail + Constants.FileExtension_Avro);

            traceInformation.AppendLine("Created file " + FilesToProcess.ProductImageDetail + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.LotNumbers + ".avro" + "for Partial Sync");
            List<LotNumbers> lstLotNumbers = CreateAVROData.GetLotNumbersListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstLotNumbers, avroFilePathForPartialSync, FilesToProcess.LotNumbers + Constants.FileExtension_Avro);
            lstLotNumbers.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.LotNumbers + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Profiles + ".avro" + "for Partial Sync");
            List<Profiles> lstProfiles = CreateAVROData.GetProfilesListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstProfiles, avroFilePathForPartialSync, FilesToProcess.Profiles + Constants.FileExtension_Avro);
            lstProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Profiles + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Policy + ".avro" + "for Partial Sync");
            List<Policy> lstPolicy = CreateAVROData.GetPolicyListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstPolicy, avroFilePathForPartialSync, FilesToProcess.Policy + Constants.FileExtension_Avro);
            lstPolicy.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Policy + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.PolicyProfileType + ".avro" + "for Partial Sync");
            List<PolicyRel> lstPolicyProfileType = CreateAVROData.GetPolicyProfileTypeListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstPolicyProfileType, avroFilePathForPartialSync, FilesToProcess.PolicyProfileType + Constants.FileExtension_Avro);
            lstPolicyProfileType.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.PolicyProfileType + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.Recalls + ".avro" + "for Partial Sync");
            List<Recalls> lstRecalls = CreateAVROData.GetRecallsListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstRecalls, avroFilePathForPartialSync, FilesToProcess.Recalls + Constants.FileExtension_Avro);
            lstRecalls.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.Recalls + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.ProductsWasteStreamProfiles + ".avro" + "for Partial Sync");
            List<ProductWasteStreamProfiles> lstProductsWasteStreamProfiles = CreateAVROData.GetProductsWasteStreamProfilesListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstProductsWasteStreamProfiles, avroFilePathForPartialSync, FilesToProcess.ProductsWasteStreamProfiles + Constants.FileExtension_Avro);
            lstProductsWasteStreamProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.ProductsWasteStreamProfiles + ".avro" + "for Partial Sync");

            traceInformation.AppendLine("Creating file " + FilesToProcess.WasteStreamProfiles + ".avro" + "for Partial Sync");
            List<WasteStreamProfiles> lstWasteStreamProfiles = CreateAVROData.GetWasteStreamProfilesListOfDataPartially(syncRequest);
            AvroConverstion.SerializeAppConfigUsingObjectContainersReflection(lstWasteStreamProfiles, avroFilePathForPartialSync, FilesToProcess.WasteStreamProfiles + Constants.FileExtension_Avro);
            lstWasteStreamProfiles.Clear();
            traceInformation.AppendLine("Created file " + FilesToProcess.WasteStreamProfiles + ".avro" + "for Partial Sync");

            responses.status = Status.OK;
         }
         catch (Exception error)
         {
            Logger.Error(error.Message);
            responses.status = Status.Fail;
         }
         finally
         {
            traceInformation.AppendLine("End Method CreateAVROFiles ProcessCreateAVRO.");
            Logger.Info(traceInformation.ToString());
         }

         return responses;
      }

      ///****************************************************************************
      /// <summary>
      ///   Creating Zip for avro files
      /// </summary>
      /// <returns></returns>
      ///****************************************************************************
      public Responses<string> CreateZipFile()
      {
         Responses<string> responses = new Responses<string>();
         responses.status = Status.Fail;
         StringBuilder traceInformation = new StringBuilder();

         try
         {
            traceInformation.AppendLine("Start Method CreateZipFile for ProcessCreateAVRO for partial sync");
            ZipUnzip objZipUnzip = new ZipUnzip();
            zipFileNameForPartialSync = syncRequest.QoskMachineID.ToLower() + Constants.Single_Dot + GetEpocTime(syncRequest.RequestTime) + Constants.Single_Dot + syncRequest.RequestId;
            if (objZipUnzip.CreateZipFolder(avroFilePathForPartialSync, zipFileNameForPartialSync))
               responses.status = Status.OK;
            responses.responses = zipFileNameForPartialSync + Constants.FileExtension_Zip;
         }
         catch (Exception error)
         {
            Logger.Error(error.Message);
            responses.status = Status.Fail;
         }
         finally
         {
            traceInformation.AppendLine("End Method CreateZipFile ProcessCreateAVRO.");
            Logger.Info(traceInformation.ToString());
         }

         return responses;
      }

   }
}
