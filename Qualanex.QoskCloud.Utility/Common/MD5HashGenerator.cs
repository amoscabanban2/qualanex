﻿using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Qualanex.QoskCloud.Utility
{
    /// <summary>
    /// Contain functions to generate hash for a particular file.
    /// </summary>
    public class MD5HashGenerator
    {
        /// <summary>
        /// Generate the hash code for the given file.
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public string GenerateHash(string FilePath)
        {
            string HashCode = string.Empty;
            if (IsFileExists(FilePath))
            {
                //Read file contents in bytes.          
                byte[] FileData = File.ReadAllBytes(FilePath);
                //Compute hash of file contents using MD5.
                using (MD5 objMD5 = MD5.Create())
                {
                    byte[] MD5HashCode = objMD5.ComputeHash(FileData);
                    //Encrypt hash based on MD5CryptoServiceProvider class.
                    using (MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider())
                    {
                        byte[] EncryptedHashCode = mD5CryptoServiceProvider.ComputeHash(MD5HashCode);
                        HashCode = ByteArrayToString(EncryptedHashCode);
                    }
                }
            }
            return HashCode;
        }

        /// <summary>
        /// Converts the byte[] array into a Hexadecimal string format.
        /// </summary>
        /// <param name="arrInput"></param>
        /// <returns>bytes in string.</returns>
        string ByteArrayToString(byte[] arrInput)
        {
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (int i = 0; i < arrInput.Length - 1; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }

        /// <summary>
        /// Check the existance of the file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>if file exists return true other file false.</returns>
        bool IsFileExists(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
