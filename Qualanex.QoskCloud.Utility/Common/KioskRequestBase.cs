﻿using System;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Utility
{
   /// <summary>
   /// Base class for all the requests
   /// </summary>
   public class KioskRequestBase
   {
      public string RequestId { get; set; }
      public DateTime RequestTime { get; set; }
      public string RunMode { get; set; }
      public string QoskMachineID { get; set; }
      public DateTime LastUpdateDate { get; set; }
      public string ProgramVersion { get; set; }
      public string Operation { get; set; }
      public KioskRequestBase GetBase(string operation)
      {
         return new KioskRequestBase
         {
            RequestId = this.RequestId,
            RequestTime = this.RequestTime,
            RunMode = this.RunMode,
            QoskMachineID = this.QoskMachineID,
            LastUpdateDate = this.LastUpdateDate,
            ProgramVersion = this.ProgramVersion,
            Operation = operation
         };
      }
   }
   public class BatchReturnsRequest : KioskRequestBase
   {
      public string BatchId { get; set; }
      public int ReturnCount { get; set; }
      public string VerificationHash { get; set; }
   }

   public class ResourceRequest : KioskRequestBase
   {
      public ResourceRequest()
      {
         ResourceAccess = Constants.FileAccess_Methods_Read;
         Timeout = 60; // default timeout in minutes
         ResourceName = string.Empty;
      }
      public string ResourceName { get; set; }
      public string ResourceAccess { get; set; }
      public int Timeout { get; set; }
   }

   public class ResourceResponse
   {
      public ResourceResponse()
      {
         ResourceURI = string.Empty;
      }
      public string ResourceURI { get; set; }
   }
   public class SyncRequest : KioskRequestBase
   {
      public SyncStatus Status { get; set; }
   }

   public class FullSyncRequest : KioskRequestBase
   {

   }

   public class FullSyncResponse
   {
      public FullSyncResponse()
      {
         FullSyncFiles = new List<string>();
      }

      // SASUris to the Full Sync files on Blob Storage
      public List<string> FullSyncFiles { get; set; }
   }

   public class SyncResponse
   {
      public bool UpdatesAvailable { get; set; }
      public SyncStatus Status { get; set; }
   }
   public enum SyncStatus
   {
      Requested = 1,
      Pending = 2,
      Downloaded = 3,
      Downloading = 4,
      Uploaded = 5,
      Uploading = 6,
      Error = 7,
      Finished = 8
   }
   public class CheckVersionResponse
   {
      public string Type { get; set; }
      public string Version { get; set; }
      public string Path { get; set; }
      public bool Active { get; set; }
      public DateTime? ReleaseDate { get; set; }
      public bool? IsImmediate { get; set; }
   }
   public class CheckVersionRequest : KioskRequestBase
   {
      public string Type { get; set; }
      public string Version { get; set; }
      public bool? IsImmediate { get; set; }
   }
   public class UpdateResponse
   {
      public bool IsUpdateSuccess { get; set; }
   }

   public class AzureAccounts
   {
      public string AccountName { get; set; }
      public string AccountKey { get; set; }
      public bool IsValidAccount { get; set; }
   }

}
