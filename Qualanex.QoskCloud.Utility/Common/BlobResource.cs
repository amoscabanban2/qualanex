﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qualanex.QoskCloud.Utility.Common
{
   public class BlobResource
   {
      /// <summary>
      /// Generates a Signed URL for access to private blob storage
      /// </summary>
      /// <param name="account">The Account (without DV)</param>
      /// <param name="containerName">The Container</param>
      /// <param name="fileName">The file with optional /directories</param>
      /// <param name="access">Read or Write</param>
      /// <param name="timeout">Timeout in minutes</param>
      /// <returns></returns>
      public static string GetSASUrl(string account, string containerName, string fileName, string access = "Read", int timeout = 60)
      {
         string sasBlobToken = string.Empty;

         var connectString = ConfigurationManager.GetConnectionString(account);
         var storageAccount = CloudStorageAccount.Parse(connectString);

         var blobClient = storageAccount.CreateCloudBlobClient();
         var serviceProperties = blobClient.GetServiceProperties();

         serviceProperties.Cors.CorsRules.Clear();

         serviceProperties.Cors.CorsRules.Add(new CorsRule()
         {
            AllowedHeaders = { Constants.Triple_Dot },
            AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
            AllowedOrigins = { Constants.Triple_Dot },
            ExposedHeaders = { Constants.Triple_Dot },
            MaxAgeInSeconds = 600,

         });

         blobClient.SetServiceProperties(serviceProperties);

         var container = blobClient.GetContainerReference(containerName.ToLower());

         // If access requested is write but the container doesn't exist, then create it
         if (access.ToLower() == Constants.FileAccess_Methods_Write)
            container.CreateIfNotExists();

         var sasConstraints = new SharedAccessBlobPolicy
         {
            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
            SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(timeout),
            Permissions = access.ToLower() == Constants.FileAccess_Methods_Write ? SharedAccessBlobPermissions.Read |
                 SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List :
                 SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
         };

         if (string.IsNullOrEmpty(fileName)) // Directory blob
         {
            sasBlobToken = container.Uri + container.GetSharedAccessSignature(sasConstraints) +
                Constants.HTTPS_Header_Custom_Contents_CompAndRestType;
         }
         else
         {
            var blob = container.GetBlockBlobReference(fileName);
            sasBlobToken = blob.Uri + blob.GetSharedAccessSignature(sasConstraints);
         }
         return sasBlobToken;
      }

      /// <summary>
      /// To find account key by account name
      /// </summary>
      /// <param name="accountName"></param>
      /// <returns></returns>
      public static AzureAccounts GetAccountCredentials(string accountName)
      {
         AzureAccounts _azureAccounts = new AzureAccounts();
         string AccountString = ConfigurationManager.GetConnectionString(accountName);
         if (string.IsNullOrEmpty(AccountString))
         {
            List<string> list = new List<string>(AccountString.Split(Constants.Semicolon));
            if (list.Count > 0)
            {
               foreach (string accInfo in list)
               {
                  if (accInfo.Contains(Constants.AccountKeyAttribute))
                  {
                     _azureAccounts.AccountKey = accInfo.Replace(Constants.AccountNameAttribute, string.Empty);
                  }
                  if (string.IsNullOrEmpty(_azureAccounts.AccountKey) && accInfo.Contains(Constants.AccountNameAttribute))
                  {
                     _azureAccounts.AccountName = accInfo.Replace(Constants.AccountNameAttribute, string.Empty);
                     _azureAccounts.IsValidAccount = !string.IsNullOrEmpty(_azureAccounts.AccountName);
                  }

               }
            }

         }
         return _azureAccounts;
      }

      /// <summary>
      /// Generates SasURI for all the blobs searched by.
      /// </summary>
      /// <param name="account">The Account (without DV)</param>
      /// <param name="containerName">The Container</param>
      /// <param name="directoryName">The Directory Name , i.e. "FL10100" or "FL10100/000001"</param>        
      /// <param name="fileName">The file with optional /directories</param>
      /// <param name="searchCriteria">Search within directory with this criteria (comma separated) i.e. topImage,sideImage. list will return containing items that contains topimage, sideimage in its name.</param>        
      /// <param name="access">Read or Write</param>
      /// <param name="timeout">Timeout in days</param>
      /// <returns></returns>
      public static List<SasUriModel> GetFolderItemsListSASUrlBasedOnSearchCriteria(string account, string containerName, string contentType, string ItemGuid, string access = "Read", int timeoutInDays = 10)
      {
         if (ConfigurationManager.GetAppSettingValue("UseLocalImages") == "true")
         {

            var directoryPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
                                @"/ProductImages/saved-images/";
            var images = new List<SasUriModel>();
            var files = System.IO.Directory.EnumerateFiles(directoryPath);
            foreach (var image in files)
            {
               if (image.Contains(ItemGuid) && contentType == "Images")
               {
                  var name = image.Remove(0, directoryPath.Length);
                  images.Add(new SasUriModel()
                  {
                     ContentSasTimeStamp = System.IO.Directory.GetLastWriteTime(directoryPath+ name).ToLocalTime().ToString(),
                     ContentName = name,
                     ContentSasURI = @"/ProductImages/saved-images/" + name,
                     ContentType = "Images"
                  });
               }
            }
            return images;
         }

         string sasBlobToken = string.Empty;

         var connectString = ConfigurationManager.GetConnectionString(account);
         var storageAccount = CloudStorageAccount.Parse(connectString);

         var blobClient = storageAccount.CreateCloudBlobClient();
         var serviceProperties = blobClient.GetServiceProperties();

         serviceProperties.Cors.CorsRules.Clear();

         serviceProperties.Cors.CorsRules.Add(new CorsRule()
         {
            AllowedHeaders = { Constants.Triple_Dot },
            AllowedMethods = CorsHttpMethods.Get | CorsHttpMethods.Head,
            AllowedOrigins = { Constants.Triple_Dot },
            ExposedHeaders = { Constants.Triple_Dot },
            MaxAgeInSeconds = 600,

         });

         blobClient.SetServiceProperties(serviceProperties);
         //var container = blobClient.GetContainerReference(containerName.ToLower());
         var container = blobClient.GetContainerReference(ItemGuid.ToLower());

         // If access requested is write but the container doesn't exist, then create it
         if (access.ToLower() == Constants.FileAccess_Methods_Write)
            container.CreateIfNotExists();

         var sasConstraints = new SharedAccessBlobPolicy
         {
            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
            SharedAccessExpiryTime = DateTime.UtcNow.AddDays(timeoutInDays),
            Permissions = access.ToLower() == Constants.FileAccess_Methods_Write ? SharedAccessBlobPermissions.Read |
                 SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List :
                 SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List
         };

         //Get list of files containing in this directory
         //CloudBlobDirectory blobDirectory = container.GetDirectoryReference(ItemGuid);
         CloudBlobContainer blobDirectory = container;
         var listOfBlobs = blobDirectory.ListBlobs();
         List<SasUriModel> SearchedBlobUri = new List<SasUriModel>();
         if (blobDirectory.Exists() && listOfBlobs.Count() > 0)
         {
            SasUriModel sasUriModel = null;
            foreach (var blobItem in listOfBlobs)
            {
               if (blobItem != null && blobItem.Uri.AbsoluteUri.ToLower().Contains(ItemGuid.ToLower()))
               {
                  sasUriModel = new SasUriModel();
                  CloudBlockBlob blob = (CloudBlockBlob)blobItem;
                  sasBlobToken = blob.Uri + blob.GetSharedAccessSignature(sasConstraints);
                  sasUriModel.ContentSasTimeStamp = blob.Properties.LastModified.Value.LocalDateTime.ToString();
                  sasUriModel.ContentSasURI = sasBlobToken;
                  sasUriModel.ContentName = blob.Name;
                  sasUriModel.ContentType = contentType == "Videos" ? Utility.Constants.ContentType_Videos : Utility.Constants.ContentType_Images;
                  SearchedBlobUri.Add(sasUriModel);
               }
            }

         }
         return SearchedBlobUri;
      }

   }
}