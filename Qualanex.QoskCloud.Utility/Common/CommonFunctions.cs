﻿using AutoMapper;
using System;
using System.Collections.Generic;
namespace Qualanex.QoskCloud.Utility
{
    public class CommonFunctions
    {
        public static T MapListObject<T>(object source, T target)
        {
            Type sourceType = source.GetType();           
            Mapper.CreateMap(sourceType, typeof(T));
            return Mapper.Map<T>(source);
        }
        public static Dictionary<string, string> GetState()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("AL", "Alabama");
            dict.Add("AK", "Alaska");
            dict.Add("AZ", "Arizona");
            dict.Add("AR", "Arkansas");
            dict.Add("CA", "California");
            dict.Add("CO", "Colorado");
            dict.Add("CT", "Connecticut");
            dict.Add("DE", "Delaware");
            dict.Add("FL", "Florida");
            dict.Add("GA", "Georgia");
            dict.Add("HI", "Hawaii");
            dict.Add("IL", "Illinois");
            dict.Add("IN", "Indiana");
            dict.Add("IA", "Iowa");
            dict.Add("KS", "Kansas");
            dict.Add("KY", "Kentucky");
            dict.Add("LA", "Louisiana");
            dict.Add("ME", "Maine");
            dict.Add("MD", "Maryland");
            dict.Add("MA", "Massachusetts");
            dict.Add("MI", "Michigan");
            dict.Add("MN", "Minnesota");
            dict.Add("MS", "Mississippi");
            dict.Add("MO", "Missouri");
            dict.Add("MT", "Montana");
            dict.Add("NE", "Nebraska");
            dict.Add("NV", "Nevada");
            dict.Add("NH", "New Hampshire");
            dict.Add("NJ", "New Jersey");
            dict.Add("NM", "New Mexico");
            dict.Add("NY", "New York");
            dict.Add("NC", "North Carolina");
            dict.Add("ND", "North Dakota");
            dict.Add("OH", "Ohio");
            dict.Add("OK", "Oklahoma");
            dict.Add("OR", "Oregon");
            dict.Add("PA", "Pennsylvania");
            dict.Add("RI", "Rhode Island");
            dict.Add("SC", "South Carolina");
            dict.Add("SD", "South Dakota");
            dict.Add("TN", "Tennessee");
            dict.Add("TX", "Texas");
            dict.Add("UT", "Utah");
            dict.Add("VT", "Vermont");
            dict.Add("VA", "Virginia");
            dict.Add("WA", "Washington");
            dict.Add("WV", "West Virginia");
            dict.Add("WI", "Wisconsin");
            dict.Add("WY", "Wyoming");
            return dict;

        }
    }
}
