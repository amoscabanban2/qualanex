﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Azure;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace Qualanex.QoskCloud.Utility.Common
{
   public class QueueResource
   {
      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="account"></param>
      /// <param name="queueName"></param>
      /// <param name="access"></param>
      /// <param name="timeout"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static string GetSASUrl(string account, string queueName, string access = Constants.Azure_Queue_Permission_Add, int timeout = 60)
      {
         var sasQueueToken = string.Empty;

         var connectString = ConfigurationManager.GetConnectionString(account);
         var storageAccount = CloudStorageAccount.Parse(connectString);

         var queueClient = storageAccount.CreateCloudQueueClient();
         var queue = queueClient.GetQueueReference(queueName);

         var permissions = SharedAccessQueuePermissions.None;
         switch (access.ToLower())
         {
            case Constants.Azure_Queue_Permission_Add:
               permissions = SharedAccessQueuePermissions.Add;
               break;
            case Constants.Azure_Queue_Permission_Read:
               permissions = SharedAccessQueuePermissions.Read;
               break;
            case Constants.Azure_Queue_Permission_Processmessages:
               permissions = SharedAccessQueuePermissions.ProcessMessages;
               break;
            case Constants.Azure_Queue_Permission_Update:
               permissions = SharedAccessQueuePermissions.Update;
               break;
         }

         var sasConstraints = new SharedAccessQueuePolicy
         {
            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
            SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(timeout),
            Permissions = permissions
         };

         sasQueueToken = queue.Uri + queue.GetSharedAccessSignature(sasConstraints);
         return sasQueueToken;
      }

      ///****************************************************************************
      /// <summary>
      ///
      /// </summary>
      /// <param name="account"></param>
      /// <param name="queueName"></param>
      /// <param name="message"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static async Task AddItemAsync(string account, string queueName, object message)
      {
         var configVal = ConfigurationManager.GetConnectionString(account);

         if (string.IsNullOrEmpty(configVal))
            throw new ApplicationException(string.Format("Configuration Setting for {0} not defined!", account));

         var storageAccount = CloudStorageAccount.Parse(configVal);
         var client = storageAccount.CreateCloudQueueClient();
         var queue = client.GetQueueReference(queueName);

         if (!queue.Exists())
         {
            Trace.TraceInformation("Attempting to Creating Queue: " + queueName + " for Account: " + account);
            await queue.CreateIfNotExistsAsync();
         }

         Trace.TraceInformation("Adding Message to Queue: " + queueName + " for Account: " + account);
         await queue.AddMessageAsync(new CloudQueueMessage(JsonConvert.SerializeObject(message,
            new JsonSerializerSettings() {NullValueHandling = NullValueHandling.Ignore})));
      }

      ///****************************************************************************
      /// <summary>
      ///   Add item in queue. Sync Function.
      /// </summary>
      /// <param name="account"></param>
      /// <param name="queueName"></param>
      /// <param name="message"></param>
      /// <returns></returns>
      ///****************************************************************************
      public static void AddItemInQueue(string account, string queueName, object message)
      {
         var configVal = ConfigurationManager.GetConnectionString(account);
         Console.WriteLine("Config val get");
         if (string.IsNullOrEmpty(configVal))
         {
            Console.WriteLine("Config val empty!");
            throw new ApplicationException(string.Format("Configuration Setting for {0} not defined!", account));
         }

         var storageAccount = CloudStorageAccount.Parse(configVal);
         Console.WriteLine("Storage account get");
         var client = storageAccount.CreateCloudQueueClient();
         Console.WriteLine("Client get");
         var queue = client.GetQueueReference(queueName);
         Console.WriteLine("Queue (" + queueName + ") get");

         if (!queue.Exists())
         {
            Console.WriteLine("Queue doesn't exist - creating...");
            queue.CreateIfNotExists();
         }
         Console.WriteLine("Message Added: " + JsonConvert.SerializeObject(message));
         Trace.TraceInformation("Adding Message to Queue: " + queueName + " for Account: " + account);
         queue.AddMessage(new CloudQueueMessage(JsonConvert.SerializeObject(message)));
      }
   }
}
