﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility.Common
{
    public class SyncTableHelper
    {

        /// <summary>
        /// Read the database tables to be used to synchronize to the local database
        /// </summary>
        /// <returns>A list of SyncTable objects detailing how to script each table</returns>
        public IList<SyncTable> ReadTables()
        {
            var blobSourceHandler = new BlobStorageHandler();
            blobSourceHandler.SetSourceContainer(Constants.AzureAccount_SysUpdates, Constants.AzureContainer_Full);
            MemoryStream mStream = new MemoryStream(blobSourceHandler.DownloadStream("SyncTables.json"));
            using (var reader = new StreamReader(mStream, Encoding.UTF8))
            {
                string json = reader.ReadToEnd();
                var tableList = JsonConvert.DeserializeObject<List<SyncTable>>(json);
                return tableList;
            }
        }
    }
}
