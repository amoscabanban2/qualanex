﻿using System;
using System.Collections.Generic;

namespace Qualanex.QoskCloud.Utility.Common
{
    public class ReportRequest
    {
        public long ReportID { get; set; }
        public string ReportModule { get; set; }
        public string ReportName { get; set; }
        public string ReportScheduleTitle { get; set; }
        public Guid ReportRunGuid { get; set; }
        public long ReportScheduleID { get; set; }
        public FileType FileType { get; set; }
        public string Criteria { get; set; }
        public string BlobFilePath { get; set; }
        public List<string> UserEmailList { get; set; }
        public long? UserID { get; set; }

    }
}
