﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Qualanex.Qosk.ServiceEntities;
using Microsoft.Azure;
using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Qualanex.QoskCloud.Utility
{
   public  class ImageProcessing
    {
        /// <summary>
        /// Location to store product images temporary.
        /// </summary>
        private string TemporaryImageStorageLocation { get; set; }      
        /// <summary>
        /// Name of Zip file to store all images.
        /// </summary>
        private string ImageZipFileName { get; set; }
        /// <summary>
        /// Information tracer.
        /// </summary>
        StringBuilder traceInformation = null;

        public ImageProcessing()
        {
            string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            TemporaryImageStorageLocation = Path.Combine(ApplicationPath, "TemporaryProductImages");    
            if (!Directory.Exists(TemporaryImageStorageLocation))
            {
                Directory.CreateDirectory(TemporaryImageStorageLocation);
            }
           
        }


        public void CopyProductImagesIntoTemporaryLocation(SyncRequest syncRequest)
        {
            Responses<Status> responses = new Responses<Status>();
            traceInformation = new StringBuilder();
            traceInformation.AppendLine("Start Method CopyProductImagesIntoTemporaryLocation ImageProcessing.");

            List<ProductImageDetail> objProductImageDetails = CreateAVROData.GetProductImageDetailListOfDataPartially(syncRequest);
            foreach (ProductImageDetail imageItem in objProductImageDetails)
            {
               
            }

        }

    
    }
}
