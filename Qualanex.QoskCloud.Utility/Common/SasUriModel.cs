﻿namespace Qualanex.QoskCloud.Utility.Common
{
   public class SasUriModel
   {
      public string ContentType { get; set; }
      public string ContentName { get; set; }
      public string ContentSasURI { get; set; }
      public string ContentSasTimeStamp { get; set; }
   }
}