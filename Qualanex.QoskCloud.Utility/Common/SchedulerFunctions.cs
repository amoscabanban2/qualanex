﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qualanex.QoskCloud.Utility.Common
{ 

    public class SchedulerFunctions
    {
        private static Dictionary<string, string> WindowsTimeZones;
        /// <summary>
        /// To get all the time zones.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetAllTimeZones()
        {
            if (WindowsTimeZones == null)
            {
                WindowsTimeZones = new Dictionary<string, string>()
                 {
                    { "(UTC-10:00) Hawaii", "Hawaiian Standard Time" },
                    { "(UTC-09:00) Alaska","Alaskan Standard Time" },
                    { "(UTC-08:00) Pacific Time (US & Canada)","Pacific Standard Time" },
                    { "(UTC-07:00) Mountain Time (US & Canada)","Mountain Standard Time" },
                    { "(UTC-06:00) Central Time (US & Canada)","Central Standard Time" },
                    { "(UTC-05:00) Eastern Time (US & Canada)","Eastern Standard Time" }
                 };
            }
            return WindowsTimeZones;
        }

       public static string GetTimeZoneFromJs(string jsTimeZone)
       {
         var temp = jsTimeZone.Substring(jsTimeZone.IndexOf("(") + 1);
         temp = temp.Remove(temp.IndexOf(")")).Replace("Daylight", "Standard");
         var timeZone = GetAllTimeZones().SingleOrDefault(x => x.Value.Contains(temp)).Key;
          return timeZone;
       }

      public static DateTime? ConvertUTCtojsTimeZone(string jsTimeZone, DateTime? UTCDateTime)
      {
         try
         {
            if (UTCDateTime != null && !string.IsNullOrEmpty(jsTimeZone) && UTCDateTime.Value > DateTime.MinValue && UTCDateTime.Value < DateTime.MaxValue)
            {
               var timeZone = jsTimeZone.Substring(jsTimeZone.IndexOf("(") + 1);
               timeZone = timeZone.Remove(timeZone.IndexOf(")")).Replace("Daylight", "Standard");
               UTCDateTime = DateTime.SpecifyKind(UTCDateTime.Value, DateTimeKind.Utc);
               return TimeZoneInfo.ConvertTimeFromUtc(UTCDateTime.Value, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
            }
         }
         catch { }
         return null;
      }

      /// <summary>
      ///  Convert Specific time zone Date time into UTC time, based on hour, Minute and Meridiem.
      /// </summary>
      /// <param name="TimeZone"></param>
      /// <param name="datetime"></param>
      /// <param name="Hours"></param>
      /// <param name="Meridiem"></param>
      /// <returns></returns>
      public static DateTime ConvertTimeZoneDateTimeToUTC(string TimeZone, DateTime datetime, int Hours, int Minute, string Meridiem = "AM")
        {
            if (Hours > 12 || Hours < 1) throw new Exception("Hour must be between 1 and 12.");
            int hour = DateTime.Parse((Hours + Meridiem).ToString()).Hour;           
            DateTime timeZoneDateTime = new DateTime(year: datetime.Year, month: datetime.Month, day: datetime.Day, hour: hour, minute: Minute, second: 0);
            //DateTime UTCDateTime = TimeZoneInfo.ConvertTimeToUtc(timeZoneDateTime, TimeZoneInfo.FindSystemTimeZoneById(TimeZone));
            return ConvertTimeZoneDateTimeToUTC(TimeZone,timeZoneDateTime);
            // UTCDateTime;
        }

        /// <summary>
        /// Overloaded function: Convert Specific time zone Date time into UTC time.
        /// </summary>
        /// <param name="TimeZone"></param>
        /// <param name="datetime"></param>
        /// <param name="Hours"></param>
        /// <param name="Meridiem"></param>
        /// <returns></returns>
       public static DateTime ConvertTimeZoneDateTimeToUTC(string TimeZone, DateTime datetime)
        {
            TimeZone = GetAllTimeZones()[TimeZone];
            DateTime timeZoneDateTime = datetime;
            DateTime UTCDateTime = TimeZoneInfo.ConvertTimeToUtc(timeZoneDateTime, TimeZoneInfo.FindSystemTimeZoneById(TimeZone));
            return UTCDateTime;
        }

       /// <summary>
       /// Convert date into one timezone to another timezone
       /// </summary>
       /// <param name="datetime"></param>
       /// <param name="sourceTimezone"></param>
       /// <param name="destinationTimeZone"></param>
       /// <returns></returns>
        static DateTime ConvertDateIntoOneTimeZoneToAnotherTimezone(DateTime datetime,string sourceTimezone,string destinationTimeZone)
        {           
            DateTime UTCDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(datetime, sourceTimezone,destinationTimeZone);
            return UTCDateTime;
        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="datetime"></param>
      /// <param name="destinationTimeZone"></param>
      /// <param name="Hours"></param>
      /// <param name="Minute"></param>
      /// <param name="Meridiem"></param>
      /// <returns></returns>
      static DateTime SetDateIntoSpecificTimeZone(DateTime datetime,string destinationTimeZone, int Hours, int Minute, string Meridiem = "AM")
        {
            int hour = DateTime.Parse((Hours + Meridiem).ToString()).Hour;
            DateTime timeZoneDateTime = new DateTime(year: datetime.Year, month: datetime.Month, day: datetime.Day, hour: hour, minute: Minute, second: 0);
            DateTime UTCDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(datetime, destinationTimeZone);
            return UTCDateTime;
        }

        /// <summary>
        /// Convert UTC datetime in specified time zone datetime.
        /// </summary>
        /// <param name="TimeZone"></param>
        /// <param name="UTCDateTime"></param>
        /// <returns></returns>
        public static DateTime? ConvertUTCToTimeZoneDate(string TimeZone, DateTime? UTCDateTime)
        {
            try
            {
                if (UTCDateTime != null && !string.IsNullOrEmpty(TimeZone) && UTCDateTime.Value > DateTime.MinValue && UTCDateTime.Value < DateTime.MaxValue)
                {
                    TimeZone = GetAllTimeZones()[TimeZone];
                    UTCDateTime = DateTime.SpecifyKind(UTCDateTime.Value, DateTimeKind.Utc);
                    return TimeZoneInfo.ConvertTimeFromUtc(UTCDateTime.Value, TimeZoneInfo.FindSystemTimeZoneById(TimeZone));
                }
            }
            catch {  }
            return null;
        }

        /// <summary>
        /// Get Local System TimeZone
        /// </summary>
        /// <returns></returns>
        public static string GetUTCTimeZone()
        {
            string localTimezone = string.Empty;
            try
            {
                localTimezone = "Etc/GMT";
            }
            catch { }
            return localTimezone;
        }
    }
}
