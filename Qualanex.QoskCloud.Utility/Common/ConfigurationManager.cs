﻿using Microsoft.Azure;
using System;
using System.Configuration;

namespace Qualanex.QoskCloud.Utility.Common
{
    public class ConfigurationManager
    {
        public static string GetConnectionString(string account)
        {
            var connectString = string.Empty;

         connectString = System.Configuration.ConfigurationManager.ConnectionStrings[$"Storage.{account}"].ConnectionString;
         if (string.IsNullOrEmpty(connectString))
                throw new ApplicationException($"Configuration Setting for {account} not defined!");
            return connectString;
        }


        public static string GetAppSettingValue(string Key)
        {
            var appSettings = System.Configuration.ConfigurationManager.AppSettings;
            string result = appSettings[Key] ?? string.Empty;
            return result;
        }
        public static string GetConnectionStringByName(string name)
        {
            // Assume failure.
            string returnValue = null;

            // Look for the name in the connectionStrings section.
            ConnectionStringSettings settings =
                System.Configuration.ConfigurationManager.ConnectionStrings[name];

            // If found, return the connection string.
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;

        }
    }
}
