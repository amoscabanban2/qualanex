﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{

    /// <summary>
    /// Utilities to provided a hiearchical grouping of any list of objects
    /// </summary>
    public class TopologyUtilities
    {

        /// <summary>
        /// Get a collection of grouped objects based on a Topological sort
        /// </summary>
        /// <typeparam name="T">Generic type to group</typeparam>
        /// <typeparam name="TKey">Generic type of the group key</typeparam>
        /// <param name="source">An enumerable collection of objects (T)</param>
        /// <param name="getDependencies">A function provided to return an enumerable collection of dependencies for the object</param>
        /// <param name="getKey">A function provided to return the key of the object</param>
        /// <returns>A collection of grouped objects</returns>
        public IList<ICollection<T>> Group<T, TKey>(IEnumerable<T> source, Func<T, IEnumerable<TKey>> getDependencies, Func<T, TKey> getKey)
        {
            return Group<T>(source, RemapDependencies<T, TKey>(source, getDependencies, getKey), new GenericEqualityComparer<T, TKey>(getKey));
        }

        /// <summary>
        /// Get a collection of grouped objects based on a Topological sort
        /// </summary>
        /// <typeparam name="T">Generic type to group</typeparam>
        /// <param name="source">An enumerable collection of objects (T)</param>
        /// <param name="getDependencies">A function provided to return an enumerable collection of dependencies for the object</param>
        /// <param name="comparer">Generic comparer to use in the sort comparison</param>
        /// <returns>A collection of grouped objects</returns>
        public IList<ICollection<T>> Group<T>(IEnumerable<T> source, Func<T, IEnumerable<T>> getDependencies, IEqualityComparer<T> comparer = null)
        {
            var sorted = new List<ICollection<T>>();
            var visited = new Dictionary<T, int>(comparer);

            foreach (var item in source)
            {
                Visit(item, getDependencies, sorted, visited);
            }

            return sorted;
        }

        /// <summary>
        /// Remap the dependencies to include the actual object instead of just the name of the object
        /// </summary>
        /// <typeparam name="T">Generic type to group</typeparam>
        /// <typeparam name="TKey">Generic type of the group key</typeparam>
        /// <param name="source">An enumerable collection of objects (T)</param>
        /// <param name="getDependencies">A function provided to return an enumerable collection of dependencies for the object</param>
        /// <param name="getKey">A function provided to return the key of the object</param>
        /// <returns>A function to retrieve the remapped dependencies</returns>
        private static Func<T, IEnumerable<T>> RemapDependencies<T, TKey>(IEnumerable<T> source, Func<T, IEnumerable<TKey>> getDependencies, Func<T, TKey> getKey)
        {
            var map = source.ToDictionary(getKey);
            return item =>
            {
                var dependencies = getDependencies(item);
                return dependencies != null
                    ? dependencies.Select(key => map[key])
                    : null;
            };
        }

        /// <summary>
        /// Visit each item and determine it's status
        /// </summary>
        /// <typeparam name="T">Generic type to group</typeparam>
        /// <param name="item">The object being visited</param>
        /// <param name="getDependencies">A function provided to return an enumerable collection of dependencies for the object</param>
        /// <param name="sorted">A collection of already sorted items</param>
        /// <param name="visited">A dictionary of already visited items</param>
        /// <returns>An integer indicating the group level of the object</returns>
        public int Visit<T>(T item, Func<T, IEnumerable<T>> getDependencies, List<ICollection<T>> sorted, Dictionary<T, int> visited)
        {
            const int inProcess = -1;
            int level;
            var alreadyVisited = visited.TryGetValue(item, out level);

            if (alreadyVisited)
            {
                if (level == inProcess)
                {
                    throw new ArgumentException("Cyclic dependency found.");
                }
            }
            else
            {
                visited[item] = (level = inProcess);

                var dependencies = getDependencies(item);
                if (dependencies != null)
                {
                    foreach (var dependency in dependencies)
                    {
                        var depLevel = Visit(dependency, getDependencies, sorted, visited);
                        level = Math.Max(level, depLevel);
                    }
                }

                visited[item] = ++level;
                while (sorted.Count <= level)
                {
                    sorted.Add(new Collection<T>());
                }
                sorted[level].Add(item);
            }

            return level;
        }
    }
}
