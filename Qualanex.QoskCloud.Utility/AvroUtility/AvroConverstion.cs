﻿using Microsoft.Hadoop.Avro;
using Microsoft.Hadoop.Avro.Container;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Qualanex.QoskCloud.Utility
{
    public sealed class AvroConverstion
    {
        public static void SerializeAppConfigUsingObjectContainersReflection<T>(List<T> listAppConfigData, string path, string fileName)
        {
            StringBuilder traceInformation = traceInformation = new StringBuilder();
            traceInformation.AppendLine("SERIALIZATION USING REFLECTION AND AVRO OBJECT CONTAINER FILES");
            try
            {
                //Create a data set using sample Class and struct
                var testData = listAppConfigData;

                //Path for Avro Object Container File
                string path1 = Constants.Double_Backward_Slash + fileName;
                path = path + path1;
                //Path for Avro Object Container File

                //string pathtest = fileName;
                RemoveFile(path, traceInformation);
                //Serializing and saving data to file

                //Creating a Memory Stream buffer
                using (var buffer = new MemoryStream())
                {
                    traceInformation.AppendLine("Serializing Sample Data Set...");

                    var avroSerializerSettings = new AvroSerializerSettings();
                    avroSerializerSettings.Resolver = new AvroDataContractResolver(allowNullable: true);

                    //Create a SequentialWriter instance for type SensorData which can serialize a sequence of SensorData objects to stream
                    //Data will be compressed using Deflate codec
                    var w = AvroContainer.CreateWriter<T>(buffer, true, avroSerializerSettings, Codec.Deflate);

                    using (var writer = new SequentialWriter<T>(w, 1))
                    {
                        // Serialize the data to stream using the sequential writer
                        if (writer != null)
                        {
                            testData.ForEach(writer.Write);
                        }
                        //Save stream to file

                        traceInformation.AppendLine("Saving serialized data to file...");

                        if (!WriteFile(buffer, path, traceInformation))
                        {
                            traceInformation.AppendLine("Error during file operation. Quitting method");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
            finally
            {
                Logger.Info(traceInformation.ToString());                
                traceInformation = null;
            }

        }


        /// <summary>
        /// Write File on Configuration path
        /// </summary>
        /// <param name="InputStream"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private static bool WriteFile(MemoryStream InputStream, string path, StringBuilder traceInformation)
        {
            if (!File.Exists(path))
            {
                try
                {
                    using (FileStream fs = File.Create(path))
                    {
                        InputStream.Seek(0, SeekOrigin.Begin);
                        InputStream.CopyTo(fs);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    return false;
                }

            }
            else
            {
                traceInformation.AppendLine(string.Format("Can not create file \"{0}\". File already exists", path));
                return false;

            }
        }
       
        //Deleting file using given path
        public static void RemoveFile(string path, StringBuilder traceInformation)
        {
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception ex) { 
                    Logger.Error(ex);
                }
            }
            else
            {
                traceInformation.AppendLine(string.Format("Can not delete file \"{0}\". File does not exists", path));   
            }
        }

        public static void MoveFile(string source, string target, StringBuilder traceInformation)
        {
            if (File.Exists(source))
            {
                try
                {
                    File.Move(source, target);
                }
                catch (Exception ex) {
                    Logger.Error(ex);
                    throw;
                }
            }
            else
            {
                traceInformation.AppendLine(string.Format("Can not move file \"{0}\". File does not exists", source));   
            }
        }
    }

}
