﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{

    public class FileHandler
    {
        private string temporaryFilePath = string.Empty;
        /// <summary>
        /// The temporary file path to use for temporary files
        /// </summary>
        public string TemporaryFilePath
        {
            get
            {
                if (temporaryFilePath == String.Empty)
                {
                    temporaryFilePath = this.GetTempFolder();
                }
                return temporaryFilePath;
            }
        }

        /// <summary>
        /// Gets a temporary file folder
        /// </summary>
        /// <returns>File folder including temporary path, default Qualanex temporary folder and a unique identifier</returns>
        public string GetTempFolder()
        {
            var path = Path.Combine(Path.GetTempPath(), Constants.DirectoryName_TempFiles, Guid.NewGuid().ToString());
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        /// <summary>
        /// Creates a temporary file based on path, file name and file extension.
        /// </summary>
        /// <param name="path">The path for the temporary file.  NOTE:  This path is assumed valid.</param>
        /// <param name="fileName">The temporary file name.</param>
        /// <param name="fileExtension">The file extension</param>
        /// <returns></returns>
        public string GetTempFileName(string path, string fileName, string fileExtension)
        {
            return Path.Combine(path, fileName + fileExtension);
        }
    }
}
