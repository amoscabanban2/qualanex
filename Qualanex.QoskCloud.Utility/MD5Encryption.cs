﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Qualanex.QoskCloud.Utility
{
    public class MD5Encryption
    {
        public string EncryptPassword(string Password)
        {
            string hash = string.Empty;
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    hash = GetMd5Hash(md5Hash, Password);

                    if (VerifyMd5Hash(md5Hash, Password, hash))
                    {
                        return hash;
                    }
                    else
                    {
                        Console.WriteLine("The hashes are not same.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
            return hash;

        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            try
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            try
            {
                // Hash the input. 
                string hashOfInput = GetMd5Hash(md5Hash, input);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// This method has been used to get the Encrypetd string for the
        /// passed string
        /// </summary>
        /// <param name="strText">
        /// <param name="strEncrypt">
        /// <returns>
        public string Encrypt(string strText, string strEncrypt = Constants.Default_Encryption_String)
        {
            // Convert strings into byte arrays.
            byte[] byKey = new byte[20];
            byte[] dv = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            string result = string.Empty;
            try
            {
                //trace Error start
                // Convert our plaintext into a byte array.
                // Let us assume that plaintext contains UTF8-encoded characters.
                byKey = System.Text.Encoding.UTF8.GetBytes(strEncrypt.Substring(0, 8));
                // Create DESCryptoServiceProvider object
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    // Convert our plaintext into a byte array.
                    byte[] inputArray = System.Text.Encoding.UTF8.GetBytes(strText);
                    // Define memory stream which will be used to hold encrypted data
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Define cryptographic stream (always use Write mode for encryption).
                        CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, dv),
                            CryptoStreamMode.Write);
                        // Start encrypting.
                        cs.Write(inputArray, 0, inputArray.Length);
                        // Finish encrypting.
                        cs.FlushFinalBlock();
                        // Convert encrypted data into a base64-encoded string and
                        // Return encrypted string.

                        //trace Error End
                        result = Convert.ToBase64String(ms.ToArray());
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                // Write Error Log
                Logger.Error(ex);
                return null;
            }

        }

        /// <summary>
        /// This method has been used to Decrypt the Encrypted String
        /// </summary>
        /// <param name="strText">
        /// <param name="strEncrypt">
        /// <returns>
        public string Decrypt(string strText, string strEncrypt = Constants.Default_Encryption_String)
        {
            // Convert strings into byte arrays.
            byte[] bKey = new byte[20];
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            string result = string.Empty;
            try
            {
                //trace Error start

                // Convert our plaintext into a byte array.
                // Let us assume that plaintext contains UTF8-encoded characters.
                bKey = System.Text.Encoding.UTF8.GetBytes(strEncrypt.Substring(0, 8));
                // Create DESCryptoServiceProvider object
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    // Convert our plaintext into a byte array.
                    Byte[] inputByteArray = inputByteArray = Convert.FromBase64String(strText);
                    // Define memory stream which will be used to hold encrypted data
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Define cryptographic stream (always use Write mode for encryption).
                        CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(bKey, IV), CryptoStreamMode.Write);
                        // Start decrypting.
                        cs.Write(inputByteArray, 0, inputByteArray.Length);
                        // Finish decrypting.
                        cs.FlushFinalBlock();
                        // Convert decrypted data into a string. 
                        System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                        // Return decrypted string.

                        //trace Error End
                        result = encoding.GetString(ms.ToArray());
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                // Write Error Log
                Logger.Error(ex);
                return null;
            }
        }

    }
}

