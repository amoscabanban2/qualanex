﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qualanex.QoskCloud.Utility
{

    /// <summary>
    /// Enumerated Synchronization Table Types
    /// </summary>
    public enum SyncTypeEnum
    {
        SharedTable,
        KioskSpecificTable,
        LocalOnlyTable
    }

    /// <summary>
    /// Synchronization Table class used to store synchronization data about individual tables
    /// </summary>
    public class SyncTable
    {
        /// <summary>
        /// Table name to synchronize
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// The Kiosk identifier column name
        /// </summary>
        public string KioskIdColumnName { get; set; }

        /// <summary>
        /// Synchronization type
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public SyncTypeEnum SyncType { get; set; }

        /// <summary>
        /// This table depends on this list of tables based on foreign key restraints
        /// </summary>
        public IList<string> DependsOnTables { get; set; }
    }
}
