﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Text;
using Microsoft.Azure;

namespace Qualanex.QoskCloud.Utility
{
   public class AzureStorageCommunication : IDisposable
   {
      /// <summary>
      /// Account Name details
      /// </summary>
      private readonly string _accountName;

      /// <summary>
      /// Account Key details
      /// </summary>
      private readonly string _accountKey;

      private readonly object _disposedLock = new object();

      private bool _isDisposed;

      #region Constructors

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="accountName"></param>
      /// <param name="accountKey"></param>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      public AzureStorageCommunication(string accountName, string accountKey)
      {
         this._accountName = accountName;
         this._accountKey = accountKey;
      }

      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ~AzureStorageCommunication()
      {
         this.Dispose(false);
      }

      #endregion

      #region Properties

      ///////////////////////////////////////////////////////////////////////////////
      /// <summary>
      ///   Summary not available.
      /// </summary>
      ///////////////////////////////////////////////////////////////////////////////
      public bool IsDisposed
      {
         get
         {
            lock (this._disposedLock)
            {
               return this._isDisposed;
            }
         }
      }

      #endregion

      #region Disposal

      ///****************************************************************************
      /// <summary>
      ///   Checks this object's Disposed flag for state.
      /// </summary>
      ///****************************************************************************
      private void CheckDisposal()
      {
         ; //          TO BE IMPLEMENTED LATER
         ; //ParameterValidation.Begin().IsNotDisposed(this.IsDisposed);
         ;
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs the object specific tasks for resource disposal.
      /// </summary>
      ///****************************************************************************
      public void Dispose()
      {
         this.Dispose(true);
         GC.SuppressFinalize(this);
      }

      ///****************************************************************************
      /// <summary>
      ///   Performs object-defined tasks associated with freeing, releasing, or
      ///   resetting unmanaged resources.
      /// </summary>
      /// <param name="programmaticDisposal">
      ///   If true, the method has been called directly or indirectly.  Managed
      ///   and unmanaged resources can be disposed.  When false, the method has
      ///   been called by the runtime from inside the finalizer and should not
      ///   reference other objects.  Only unmanaged resources can be disposed.
      /// </param>
      ///****************************************************************************
      private void Dispose(bool programmaticDisposal)
      {
         if (!this._isDisposed)
         {
            lock (this._disposedLock)
            {
               if (programmaticDisposal)
               {

                  try
                  {
                     ;
                     ;
                     ;
                  }
                  catch (Exception ex)
                  {
                     Logger.Error(ex.Message);
                  }
                  finally
                  {
                     this._isDisposed = true;
                  }
               }
            }
         }
      }

      #endregion

      #region Methods

      ///****************************************************************************
      /// <summary>
      ///   Upload file to Azure server based on HttpPostedFileBase.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="file"></param>
      /// <param name="fileName"></param>
      /// <returns></returns>
      ///****************************************************************************
      public bool UploadFile(string container, System.Web.HttpPostedFileBase file, string fileName)
      {
         this.CheckDisposal();

         var traceInformation = new StringBuilder();
         bool result;

         try
         {
            traceInformation.AppendLine("UploadFile Method start...");

            // Create the instance for StorageCredentials .
            var creds = new StorageCredentials(this._accountName, this._accountKey);

            // Create the blob client.
            var account = new CloudStorageAccount(creds, useHttps: true);
            traceInformation.AppendLine("Authenticating account...");
            var client = account.CreateCloudBlobClient();

            // Retrieve reference to a previously created container
            var sampleContainer = client.GetContainerReference(container.ToLower());

            sampleContainer.CreateIfNotExists();
            traceInformation.AppendLine("Create Container if not Exists.");
            sampleContainer.SetPermissions(new BlobContainerPermissions()
            {
               PublicAccess = BlobContainerPublicAccessType.Container
            });

            traceInformation.AppendLine("Given public access permission.");

            // Retrieve reference to a blob named
            var blob = sampleContainer.GetBlockBlobReference(fileName);

            blob.Properties.ContentType = file.ContentType;
            // upload image  on Azure .
            blob.UploadFromStream(file.InputStream);

            result = true;
         }
         catch
         {
            throw;
         }
         finally
         {
            Logger.Info(traceInformation.ToString());
         }

         return result;
      }

      ///****************************************************************************
      /// <summary>
      ///   Delete the Image from cloud storage.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="blobfilereference"></param>
      /// <returns></returns>
      ///****************************************************************************
      public bool DeleteImagefromAzureblob(string container, string blobfilereference)
      {
         this.CheckDisposal();

         var creds = new StorageCredentials(this._accountName, this._accountKey);
         var account = new CloudStorageAccount(creds, useHttps: true);

         // Create the blob client.
         var client = account.CreateCloudBlobClient();

         // Retrieve reference to a previously created container
         var sampleContainer = client.GetContainerReference(container);

         // Retrieve reference to a blob named
         var blockBlob = sampleContainer.GetBlockBlobReference(blobfilereference);

         // Delete the blob.
         if (blockBlob.Exists())
         {
            blockBlob.Delete();
            return true;
         }

         return false;
      }

      #endregion
   }
}
