﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.DataMovement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qualanex.QoskCloud.Utility.Common;

namespace Qualanex.QoskCloud.Utility
{
    public class BlobStorageHandler
    {
        /// <summary>
        /// Source container for blob operations
        /// </summary>
        [CLSCompliant(false)]
        public CloudBlobContainer SourceBlobContainer { get; private set; }

        /// <summary>
        /// Destination container for blob operations
        /// </summary>
        [CLSCompliant(false)]
        public CloudBlobContainer DestinationBlobContainer { get; private set; }

        /// <summary>
        /// Default constructor for the BlobStorageHandler class
        /// </summary>
        public BlobStorageHandler()
        {
            TransferManager.Configurations.ParallelOperations = 64;
        }

        /// <summary>
        /// Constructor for the BlobStorageHandler class
        /// </summary>
        /// <param name="sourceAccount">Source blob account string</param>
        /// <param name="sourceContainerName">Source blob container name</param>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public BlobStorageHandler(string sourceAccount, string sourceContainerName, string destinationAccount, string destinationContainerName) : base()
        {
            this.SourceBlobContainer = GetCloudBlobContainer(sourceContainerName, sourceAccount);
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// Constructor for the BlobStorageHandler class
        /// </summary>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public BlobStorageHandler(string destinationAccount, string destinationContainerName) : base()
        {
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// Set the source container
        /// </summary>
        /// <param name="sourceAccount">Source blob account string</param>
        /// <param name="sourceContainerName">Source blob container name</param>
        public void SetSourceContainer(string sourceAccount, string sourceContainerName)
        {
            this.SourceBlobContainer = GetCloudBlobContainer(sourceContainerName, sourceAccount);
        }

        /// <summary>
        /// Set the destination container
        /// </summary>
        /// <param name="destinationAccount">Destination blob account string</param>
        /// <param name="destinationContainerName">Destination blob container name</param>
        public void SetDestinationContainer(string destinationAccount, string destinationContainerName)
        {
            this.DestinationBlobContainer = GetCloudBlobContainer(destinationContainerName, destinationAccount);
            DestinationBlobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
        }

        /// <summary>
        /// Copy blob file from source file to destination file on the server
        /// </summary>
        /// <param name="sourceFile">Source blob</param>
        /// <param name="destinationFile">Destination blob</param>
        /// <returns></returns>
        public async Task CopyBlob(string sourceFile, string destinationFile)
        {
            try
            {
                CloudBlob sourceBlob = GetCloudBlob(SourceBlobContainer, sourceFile, BlobType.BlockBlob);
                if (sourceBlob.Exists())
                {
                    CloudBlob destinationBlob = GetCloudBlob(DestinationBlobContainer, destinationFile, BlobType.BlockBlob);
                    if (destinationBlob.Exists() == false)
                    {
                        await TransferManager.CopyAsync(sourceBlob, destinationBlob, true);
                    }
                }
            }
            catch (Exception x)
            {
                Logger.Error(x);
                throw x;  //Implement retry logic here?
            }
        }

        /// <summary>
        /// Upload a stream to blob storage
        /// </summary>
        /// <param name="stream">The stream to be uploaded</param>
        /// <param name="destinationFile">The destination blob</param>
        /// <returns>Task completion status</returns>
        public async Task UploadStreamAsync(Stream stream, string destinationFile)
        {
            try
            {
                CloudBlob destinationBlob = GetCloudBlob(DestinationBlobContainer, destinationFile, BlobType.BlockBlob);
                await destinationBlob.DeleteIfExistsAsync();
                await TransferManager.UploadAsync(stream, destinationBlob);
            }
            catch (Exception x)
            {
                Logger.Error(x);
                throw x;
            }
        }

        /// <summary>
        /// Download a blob file to a byte stream.
        /// 
        /// NOTE: Call SetSourceContainer before calling this method.
        /// </summary>
        /// <param name="source">File name in blob storage to download.</param>
        /// <returns>Byte buffer containing downloaded file</returns>
        public byte[] DownloadStream(string source)
        {
            
            try
            {
                CloudBlob sourceBlob = GetCloudBlob(SourceBlobContainer, source, BlobType.BlockBlob);
                using (MemoryStream destinationStream = new MemoryStream())
                {
                    TransferManager.DownloadAsync(sourceBlob, destinationStream).Wait();
                    return destinationStream.GetBuffer();
                }
            }
            catch (Exception x)
            {
                Logger.Error(x);
                throw;
            }
        }

        /// <summary>
        /// Gets a reference to a blob
        /// </summary>
        /// <param name="container">The CloudBlobContainer instance</param>
        /// <param name="blobName">The file name of the blob to reference</param>
        /// <param name="blobType">The blob type</param>
        /// <returns></returns>
        private CloudBlob GetCloudBlob(CloudBlobContainer container, string blobName, BlobType blobType)
        {
            CloudBlob cloudBlob;
            switch (blobType)
            {
                case BlobType.AppendBlob:
                    cloudBlob = container.GetAppendBlobReference(blobName);
                    break;
                case BlobType.BlockBlob:
                    cloudBlob = container.GetBlockBlobReference(blobName);
                    break;
                case BlobType.PageBlob:
                    cloudBlob = container.GetPageBlobReference(blobName);
                    break;
                case BlobType.Unspecified:
                default:
                    throw new ArgumentException(string.Format("Invalid blob type {0}", blobType.ToString()), "blobType");
            }

            return cloudBlob;
        }

        /// <summary>
        /// Get the cloud connection string for the specified account.
        /// </summary>
        /// <param name="account">Cloud container account</param>
        /// <returns>Connection string for the cloud blob storage container</returns>
        private string GetConnectionString(string account)
        {
            var connectString = ConfigurationManager.GetConnectionString(account);
            if (string.IsNullOrEmpty(connectString))
                throw new ApplicationException(string.Format("Configuration Setting for {0} not defined!", account));
            return connectString;
        }

        /// <summary>
        /// Get the cloud blob container instance
        /// </summary>
        /// <param name="containerName">The container name</param>
        /// <param name="accountName">The account name</param>
        /// <returns></returns>
        private CloudBlobContainer GetCloudBlobContainer(string containerName, string accountName)
        {
            var connectString = GetConnectionString(accountName);
            CloudStorageAccount account = CloudStorageAccount.Parse(connectString);
            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = client.GetContainerReference(containerName);
            cloudBlobContainer.CreateIfNotExists();
            return cloudBlobContainer;
        }

        /// <summary>
        /// Download a file to a folder path
        /// </summary>
        /// <param name="folderPath">Destination path for fle</param>
        /// <param name="sourcePath">Source path for file</param>
        /// <param name="fileName">Name of file</param>
        public void DownloadFile(string folderPath, string sourcePath, string fileName)
        {
            try
            {
                if (SourceBlobContainer.Exists())
                {
                    CloudBlockBlob blob = SourceBlobContainer.GetBlockBlobReference(sourcePath + Utility.Constants.Single_Forward_Slash + fileName);
                    blob.FetchAttributes();
                    blob.SetProperties();
                    string downloadFileWithPath = folderPath + Utility.Constants.Double_Backward_Slash + fileName;
                    using (Stream outputFile = new FileStream(downloadFileWithPath, FileMode.Create))
                    {
                        blob.DownloadToStream(outputFile);
                    }
                }
            }
            catch (Exception x)
            {
                Logger.Error(x);
            }

        }

        /// <summary>
        /// BlobFileUpload uploads a single file to the Azure Blob target folder
        /// </summary>
        /// <param name="sourceFile">Full path to source file for upload</param>
        /// <param name="targetFolder">Azure blob folder for file destination</param>
        /// <returns>Task object containing status/results of the asynchronous task</returns>
        public async Task BlobFileUpload(string sourceFile, string targetFolder = default(string))
        {
            CloudBlob destBlob;
            if (targetFolder == null)
            {
                destBlob = DestinationBlobContainer.GetBlockBlobReference(Path.GetFileName(sourceFile));

            }
            else
            {
                destBlob = DestinationBlobContainer.GetBlockBlobReference(targetFolder + "/" + Path.GetFileName(sourceFile));
            }

            if (await destBlob.ExistsAsync() == true)
            {
                await destBlob.DeleteAsync();
            }
            await TransferManager.UploadAsync(sourceFile, destBlob);
        }


        /// <summary>
        /// Upload table script asynchrounously.  This method will zip the script string before uploading to a binary file.
        /// </summary>
        /// <param name="file">The file of script currently being uploaded</param>
        /// <returns>Task completion status</returns>
        public async Task UploadTableAsync(string file)
        {
            try
            {
                if (File.Exists(file) == true)
                {
                    var zippedFile = await Task.Factory.StartNew(() =>
                    {
                        var zipper = new ZipUnzip();
                        return zipper.ZipSingleFile(file);
                    });

                    await BlobFileUpload(zippedFile);
                }
            } 
            catch(Exception x)
            {
                Logger.Error(x);
                throw;
            }
        }
    }
}
