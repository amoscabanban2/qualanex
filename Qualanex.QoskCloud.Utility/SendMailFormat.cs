﻿using System.IO;
using Microsoft.SqlServer.Management.Sdk.Sfc;

namespace Qualanex.QoskCloud.Utility
{
   using System;
   using System.Text;
   using System.Net.Mail;
   using System.Net;
   using System.Collections.Generic;

   /// <summary>
   /// Mail format is class for sending email with or without attachment.
   /// </summary>
   /// <Developer>Irshad Ansari</Developer>
   /// <Date>Nov 11, 2014</Date>
   public class SendUserMail : IDisposable
   {
      // Flag: Has Dispose already been called? 
      bool disposed = false;

      // Public implementation of Dispose pattern callable by consumers. 
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      // Protected implementation of Dispose pattern. 
      protected virtual void Dispose(bool disposing)
      {
         if (disposed)
            return;

         if (disposing)
         {
            // Free any other managed objects here. 
            //

         }

         // Free any unmanaged objects here. 
         //
         disposed = true;
      }

      ~SendUserMail()
      {
         Dispose(false);
      }


      public static string AutogenratePassword()
      {
         string strRandom = string.Empty;
         string[] strArray = new string[40];
         strArray = new string[] { Constants.Alphabets_a, Constants.Alphabets_b, Constants.Alphabets_c,
                Constants.Alphabets_d, Constants.Alphabets_e, Constants.Alphabets_f, Constants.Alphabets_g,
                Constants.Alphabets_h, Constants.Alphabets_i, Constants.Alphabets_j, Constants.Alphabets_k,
                Constants.Alphabets_l, Constants.Alphabets_m, Constants.Alphabets_n, Constants.Alphabets_o,
                Constants.Alphabets_p, Constants.Alphabets_q, Constants.Alphabets_r, Constants.Alphabets_s,
                Constants.Alphabets_t, Constants.Alphabets_u, Constants.Alphabets_v, Constants.Alphabets_w,
                Constants.SpecialChar_AT, Constants.SpecialChar_AND, Constants.SpecialChar_DOLLAR, Constants.SpecialChar_HASH,
                Constants.SpecialChar_PERCENTILE, Constants.Alphabets_x, Constants.Alphabets_y, Constants.Alphabets_z, Constants.Nubmer_0,
                Constants.Nubmer_1, Constants.Nubmer_2, Constants.Nubmer_3, Constants.Nubmer_4, Constants.Nubmer_5, Constants.Nubmer_6,
                Constants.Nubmer_7, Constants.Nubmer_8, Constants.Nubmer_9 };
         Random autoRand = new Random();
         int x;
         for (x = 0; x < 6; x++)
         {
            int i = Convert.ToInt32(autoRand.Next(0, 36));
            strRandom += strArray[i].ToString();
         }
         return strRandom + Constants.Random_Password_Suffix;

      }

      /// <summary>
      /// Send Email without attachment
      /// </summary>
      /// <param name="FromMsg">From Email Address</param>
      /// <param name="ToMsg">To Email Address</param>
      /// <param name="bcc">Bcc Email Address</param>
      /// <param name="cc">Cc Email Address</param>
      /// <param name="subject">Subject of the email</param>
      /// <param name="body">Email Message</param>
      /// <param name="fromContact"></param>
      /// <returns>Mail Send or not</returns>
      /// <Developer>Irshad</Developer>
      public static bool SendEmail(List<string> toUserEmailList, string cc, List<string> toBCCUserEmailList, string subject, string msgBody,
         string NtwrkCardinalUserName, string NtworkPass, string smtpSettings, TextWriter log = null)
      {
         if (log == null)
         {
            log = TextWriter.Null;
         }

         StringBuilder stBuild = new StringBuilder();
         string ntwrkCardinalUserName = NtwrkCardinalUserName;
         string smtpSetting = smtpSettings;
         string ntwrkCardinalPWD = NtworkPass;
         stBuild.AppendLine(" Start SendMailMessage from: " + ntwrkCardinalUserName + "subject: " + subject);
         log.WriteLine("Creating MailMessage");

         // Create a System.Net.Mail.MailMessage object
         using (MailMessage message = new MailMessage())
         {
            if (toUserEmailList != null && toUserEmailList.Count > 0)
            {
               log.WriteLine("Adding 'to' emails");
               foreach (string to in toUserEmailList)
               {
                  // Add a recipient
                  message.To.Add(to);
               }
            }

            log.WriteLine("Adding subject");
            // Add a message subject
            message.Subject = subject;

            if (toBCCUserEmailList != null && toBCCUserEmailList.Count > 0)
            {
               log.WriteLine("Adding 'bcc' emails");
               foreach (string bcc in toBCCUserEmailList)
               {
                  // Add bcc recipients
                  message.Bcc.Add(new MailAddress(bcc));
               }
            }

            // Check if the cc value is null or an empty value
            if (!string.IsNullOrEmpty(cc))
            {
               log.WriteLine("Adding 'cc' emails");
               message.CC.Add(new MailAddress(cc));
            }

            log.WriteLine("Adding message body");
            // Add a message body
            message.Body = msgBody;
            // Set the format of the mail message body as HTML
            log.WriteLine("Formatting as HTML");
            message.IsBodyHtml = true;
            // Set the priority of the mail message to normal
            log.WriteLine("normal priority");
            message.Priority = MailPriority.Normal;
            // Create a System.Net.Mail.MailAddress object and 
            // set the sender email address and display name.
            log.WriteLine("Setting from and sender");
            message.From = new MailAddress(ntwrkCardinalUserName, Constants.Qualanex);
            message.Sender = new MailAddress(ntwrkCardinalUserName, Constants.Qualanex);
            // Create a System.Net.Mail.SmtpClient object
            // and set the SMTP host and port number

            // Instantiate a new instance of SmtpClient
            log.WriteLine("Initialize smtp client");
            using (SmtpClient smtp = new SmtpClient(smtpSetting, Constants.SmtpClient_PORTNUMBER))
            {
               // If your server requires authentication add the below code
               // =========================================================
               // Enable Secure Socket Layer (SSL) for connection encryption
               smtp.EnableSsl = true;
               // Do not send the DefaultCredentials with requests
               smtp.UseDefaultCredentials = true;
               smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
               // Create a System.Net.NetworkCredential object and set
               // the username and password required by your SMTP account
               smtp.Credentials = new NetworkCredential(ntwrkCardinalUserName, ntwrkCardinalPWD);
               // =========================================================
               try
               {
                  // Send the message
                  log.WriteLine("Attempting send");
                  smtp.Send(message);
                  stBuild.AppendLine("Email sent successfully ");
                  log.WriteLine("Successfully sent");
                  return true;
               }
               catch (Exception ex)
               {
                  log.WriteLine("Error: " + ex.Message);
                  stBuild.AppendLine("Error while sending mail " + ex.Message.ToString());
                  Logger.Error(stBuild.ToString());
                  return false;
               }
               finally
               {
                  Logger.Info(stBuild.ToString());
               }
            }
         }
      }
   }
}
