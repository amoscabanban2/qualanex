﻿
using System.Collections.Generic;
using System.ComponentModel;

namespace Qualanex.QoskCloud.Utility
{

   /// <summary>
   /// User types
   /// </summary>
   public enum UserTypes
   {
      ADMIN,
      USER,
      OTHERS
   }
   /// <summary>
   /// Container
   /// </summary>
   public enum Container
   {
      None = 0,
      changeset = 1,
      full = 2,
      notifications = 3,
      returnmedia = 4,
      notificationmedia = 5
   }
   /// <summary>
   /// ImageTypes
   /// </summary>
   public enum ImageTypes
   {
      None = 0,
      Original = 1,
      Repackager = 2
   }
   /// <summary>
   /// FilesToProcess
   /// </summary>
   public enum FilesToProcess
   {
      None = 0,
      PolicyProfileType = 1,
      Policy = 2,
      ReturnAuthorizationsReasons = 3,
      DirectIndirectAccounts = 4,
      ProductImageDetail = 5,
      RecallsDetails = 6,
      LotNumbers = 7,
      Recalls = 8,
      PurchaseDetails = 9,
      ProductsWasteStreamProfiles = 10,
      WasteStreamProfiles = 11,
      Products = 12,
      Profiles = 13
   }
   /// <summary>
   /// Status
   /// </summary>
   public enum Status
   {
      None = -1,
      OK = 0,
      Fail = 1,
      Rollback = 2
   }
   /// <summary>
   /// Meridieum
   /// </summary>
   public enum Meridieum
   {
      AM = 0,
      PM = 1
   }


   /// <summary>
   /// Scheduled report can be scheduled in following ways.
   /// </summary>
   public enum ScheduleType
   {
      Hour = 0,
      Day = 1,
      Week = 2,
      Month = 3
   }

   /// <summary>
   /// Telrik report file types.
   /// </summary>
   public enum FileType
   {
      PDF = 1,
      XLS = 2,
      CSV = 3
   }

   /// <summary>
   /// Define the status of the batch report.
   /// </summary>
   public enum ReportRunningStatus
   {

      [Description(Constants.ReportMessage_Added_To_Report_Queue)]
      AddedToReportQueue = 0,

      [Description(Constants.ReportMessage_Picked_By_Schedule_Job)]
      Running = 1,

      [Description(Constants.ReportMessage_Report_Exported_Successfully)]
      Generated = 2,

      [Description(Constants.ReportMessage_Uploading_To_Blob)]
      Uploading = 3,

      [Description(Constants.ReportMessage_Uploaded_To_Blob)]
      Uploaded = 4,

      [Description(Constants.ReportMessage_Placed_In_NotificationQueue)]
      AddedToNotificationQueue = 5,

      [Description(Constants.ReportMessage_Notification_Sending)]
      NotificationSending = 6,

      [Description(Constants.ReportMessage_Notification_Sent)]
      NotificationSent = 7,

      [Description(Constants.ReportMessage_Finished)]
      Finished = 8,

      [Description(Constants.ReportMessage_Error_Occurred)]
      Error = 9
   }
   /// <summary>
   /// Responses
   /// </summary>
   /// <typeparam name="T"></typeparam>
   public class Responses<T>
   {
      public T responses { get; set; }
      public Status status { get; set; }
   }

   /// <summary>
   /// Common Constant
   /// </summary>
   public sealed class Constants
   {
      #region  Miscellaneous 

      public const string productOriginal = "images/ProductImage/Original/";
      public const string productRepackager = "images/ProductImage/Repackager/";
      public const string productContent = "images/ProductImage/Content/";


      public const string Manufacturer = "Manufacturer";
      public const string Repackager = "Repackager";
      public const string ManufacturerGroup = "ManufacturerGroup";
      public const string WholesalerGroup = "WholesalerGroup";
      public const string Wholesaler = "Wholesaler";

      public const string MFGRGRP = "MFGR";
      public const string WHLSGRP = "WHLS";
      public const string PHARGRP = "PHAR";

      public const string GroupType = "GroupType";
      public const string ProfileType = "ProfileType";

      /// <summary>
      /// Strings for sidebar containers
      /// </summary>
      public const string UpaApplicationKey = "UPA_APPLICATION";

      public const string UpaSidebar = "Sidebar";
      public const string Tote = "Tote";
      public const string Bin = "Bin";
      public const string Gaylord = "Gaylord";



      public const string BlackSortBin = "BLK_BIN";
      public const string BlueSortBin = "BLU_BIN";
      public const string ClearSortBin = "CLR_BIN";
      public const string GreenSortBin = "GRN_BIN";
      public const string RedSortBin = "RED_BIN";
      public const string WhiteSortBin = "WHT_BIN";
      public const string YellowSortBin = "YLW_BIN";

      public const string Property_SValue_Required = "R";
      public const string Property_SValue_ManualOverride = "M";
      public const string Property_Code_ProductContainerImage = "CNTRIMG";
      public const string Property_Code_ProductContentImage = "CONTIMG";
      public const string Property_Code_Location = "LOC";
      public const string Property_Code_LocationRoute = "LOCRTE";
      public const string Property_Code_LotExpiration = "LOTEXP";
      public const string Property_Code_ProductWeight = "PRODWGT";
      public const string Property_Code_ProductWaste = "PRODWST";
      public const string Property_Code_Quarantine = "QUARTNE";
      public const string Property_Code_Unknown = "UNKN";

      public static readonly List<string> BinTypes = new List<string>
      {
         BlackSortBin,BlueSortBin,GreenSortBin,RedSortBin,WhiteSortBin,YellowSortBin
      };

      public const string BlueInduction = "BLU_INDCTN";
      public const string GrayInduction = "GRY_INDCTN";

      public static readonly List<string> ToteTypes = new List<string>
      {
         BlueInduction,GrayInduction,ClearSortBin
      };

      public const string LargeGaylord = "LRG_GAYLRD";
      public const string OrangeGaylord = "ORG_GAYLRD";
      public const string RecallGaylord = "RCL_GAYLRD";
      public const string SmallGaylord = "SML_GAYLRD";
      public const string WasteGaylord = "WST_GAYLRD";
      public const string VirtualGaylord = "VRT_GAYLRD";
      public const string BlackGaylord = "BLK_GAYLRD";

      public static readonly List<string> GaylordTypes = new List<string>
      {
         LargeGaylord,OrangeGaylord,RecallGaylord,SmallGaylord,WasteGaylord,VirtualGaylord,BlackGaylord
      };
      ///////////////////////////////////////////////////////////

      public const string Group = "Group";
      public const string Profile = "Profile";
      public const string PublishedProfileType = "PublishedProfileType";
      public const string CustomerSpecific = "Customer Specific";
      public const string PublishedPolicy = "Published Policy";

      public const string AllProducts = "All Products";
      public const string ProductsSpecifictoNDCUPC = "Products Specific to NDC/UPC";
      public const string ProductsSpecifictoNDCUPCandLotNumber = "Products Specific to NDC/UPC and Lot Number";

      public const string UserRoleAdmin = "Admin";

      public const string Javascript_Popup_Method = "<a class = 'clsPopUp' href='javascript:openPopup({0});'  target='_blank'>{1}</a>";
      #endregion

      #region File Extensions        
      public const string FileExtension_Zip = ".zip";
      public const string FileExtension_Bin = ".bin";
      public const string FileExtension_Script = ".scr";
      public const string FileExtension_Data = ".dat";
      public const string FileExtension_Json = ".json";
      public const string FileExtension_Avro = ".avro";
      public const string FileExtension_MP4 = ".mp4";
      #endregion

      #region DB Model Connection Strings
      public const string DBModel_ConnectionString_DefaultConnection = "DefaultConnection";
      public const string DBModel_ConnectionString_DefaultInstance = "dvqoskdb";
      #endregion

      #region DB Model DbModelBuilder Entities/Columns
      public const string DbModelBuilder_Entity_ApplicationUser = "User";
      public const string DbModelBuilder_Entity_IdentityUserRole = "UserRole";
      public const string DbModelBuilder_Entity_IdentityUserLogin = "UserLogin";
      public const string DbModelBuilder_Entity_IdentityUserClaim = "UserClaim";
      public const string DbModelBuilder_Entity_Role = "Role";

      public const string DbModelBuilder_Column_UserClaimId = "UserClaimId";
      public const string DbModelBuilder_Column_RoleId = "RoleId";

      #endregion

      #region Route Prefix
      public const string RoutePrefix_ApiAccount = "api/Account";
      public const string RoutePrefix_ApiKiosk = "api/Kiosk";
      public const string RoutePrefix_ApiNotification = "api/Notification";
      public const string RoutePrefix_ApiReport = "api/Report";
      #endregion

      #region Route Templates
      public const string RouteTemplate_ExternalLogin = "ExternalLogin";
      public const string RouteTemplate_Sync = "Sync";
      public const string RouteTemplate_FullSync = "FullSync";
      public const string RouteTemplate_PartialSync = "PartialSync";
      public const string RouteTemplate_CheckPartialSyncStatus = "CheckPartialSyncStatus";
      public const string RouteTemplate_UpdateStatus = "UpdateStatus";
      public const string RouteTemplate_BatchReturns = "BatchReturns";
      public const string RouteTemplate_ResourceURI = "ResourceURI";
      public const string RouteTemplate_SysExeSync = "SysExeSync";
      public const string RouteTemplate_UpdateSysExeImmediateStatus = "UpdateSysExeImmediateStatus";
      public const string RouteTemplate_RequestOTP = "RequestOTP";
      public const string RouteTemplate_UpdateQoskVerificationResult = "UpdateQoskVerificationResult";
      public const string RouteTemplate_FullDataSyncMasterDB = "FullDataSyncMasterDB";
      public const string RouteTemplate_FullDataSyncQoskSpecific = "FullDataSyncQoskSpecific";

      #endregion

      #region Common 
      public const string PolicyName = "Policy Name";
      public const string PolicyType = "Policy Type";
      public const string DefaultDataFormatString = "{0:MM/dd/yyyy}";
      public const string Placeholder = "placeholder";
      public const string Default_Encryption_String = "MAKV2SPBNI99212111";
      public const string Default_Access = "*";
      public const string Relative_Path_Prefix = "~/";
      public const string Key_Error = "#error=";
      public const string Qosk_Machine_Is_UpToDate = "Qosk machine is up-to date";
      public const string VerificationStatus_Fail = "Fail";
      public const string VerificationStatus_Success = "Success";
      public const string UserName = "userName";
      public const string Invalid_Grant = "invalid_grant";
      public const string Random_Password_Suffix = "@9Q";
      public const string Qualanex = "Qualanex";
      public const int SmtpClient_PORTNUMBER = 587;
      public const string AccountNameAttribute = "AccountName=";
      public const string AccountKeyAttribute = "AccountKey=";
      public const string DefaultUTC8601Format = "yyyy-MM-ddTHH:mm:ssZ";
      public const string ContentType_Images = "Images";
      public const string ContentType_Videos = "Videos";
      public const string Default_Product_Image_Url = @"\Areas\Product\Images\Qosk_Logo.jpg";

      #endregion

      #region Alphabets
      public const string Alphabets_a = "a";
      public const string Alphabets_b = "b";
      public const string Alphabets_c = "c";
      public const string Alphabets_d = "d";
      public const string Alphabets_e = "e";
      public const string Alphabets_f = "f";
      public const string Alphabets_g = "g";
      public const string Alphabets_h = "h";
      public const string Alphabets_i = "i";
      public const string Alphabets_j = "j";
      public const string Alphabets_k = "k";
      public const string Alphabets_l = "l";
      public const string Alphabets_m = "m";
      public const string Alphabets_n = "n";
      public const string Alphabets_o = "o";
      public const string Alphabets_p = "p";
      public const string Alphabets_q = "q";
      public const string Alphabets_r = "r";
      public const string Alphabets_s = "s";
      public const string Alphabets_t = "t";
      public const string Alphabets_u = "u";
      public const string Alphabets_v = "v";
      public const string Alphabets_w = "w";
      public const string Alphabets_x = "x";
      public const string Alphabets_y = "y";
      public const string Alphabets_z = "z";
      #endregion

      #region Number
      public const string Nubmer_0 = "0";
      public const string Nubmer_1 = "1";
      public const string Nubmer_2 = "2";
      public const string Nubmer_3 = "3";
      public const string Nubmer_4 = "4";
      public const string Nubmer_5 = "5";
      public const string Nubmer_6 = "6";
      public const string Nubmer_7 = "7";
      public const string Nubmer_8 = "8";
      public const string Nubmer_9 = "9";
      #endregion

      #region Special Characters
      public const string SpecialChar_AT = "@";
      public const string SpecialChar_AND = "&";
      public const string SpecialChar_DOLLAR = "$";
      public const string SpecialChar_HASH = "#";
      public const string SpecialChar_PERCENTILE = "%";
      #endregion

      #region Https Methods/Values 
      public const string Https_Method_Get = "GET";
      public const string Https_Method_Post = "POST";
      public const string Https_Values_Authorization = "authorization";
      public const string Https_Values_ContentType = "content-type";
      public const string Https_Default_Route_Name = "DefaultApi";
      public const string Https_Default_Route_Template = "api/{controller}/{id}";
      public const string Https_MediaType = "text/html";
      public const string Https_ResponseMessage_ReasonPhrase = "HTTPS Required";
      #endregion

      #region Weight Condition Segment
      public const decimal WeightTolerance = (decimal) .10;
      #endregion

      #region Authorization constants
      public const string PublicClientId = "self";
      public const string TokenEndpointPath = "/Token";
      public const string AuthorizeEndpointPath = "/api/Account/ExternalLogin";
      public const int AccessTokenExpireTimeSpan = 14;
      public const string Origin = "Origin";
      public const string IOwinResponse_Header_Access_Control_Allow_Origin = "Access-Control-Allow-Origin";
      public const string IOwinRequest_Method_OPTIONS = "OPTIONS";
      public const string IOwinRequest_Key_Access_Control_Allow_Methods = "Access-Control-Allow-Methods";
      public const string IOwinRequest_Key_Access_Control_Allow_Headers = "Access-Control-Allow-Headers";


      #endregion

      #region Stored Procedure Parameter
      public const string StoredProcedure_Parameter_TotalRecord = "totalRecord";
      public const string StoredProcedure_Parameter_PageNameforSearch = "Product";
      public const string StoredProcedure_Parameter_WhereCondition = " where  1=1 ";
      public const string StoredProcedure_Parameter_ProductID = "ProductID";
      public const string StoredProcedure_Parameter_SortDataField = "sortdatafield";
      public const string StoredProcedure_Parameter_SortOrder = "sortorder";
      #endregion

      #region Report Parameter
      public const string Report_Parameter_Month = "month";
      public const string Report_Parameter_ProductId = "productId";
      public const string Report_Parameter_Search = "search";

      public const string Report_Parameter_Entity_Prefix = "RP_";

      #endregion

      #region ProfileType
      public const string ProfileType_PublishedPolicy = "Published Policy";
      public const string ProfileType_CustomerSpecific = "Customer Specific";
      #endregion

      #region PolicyType
      public const string PolicyType_AllProducts = "All Products";
      public const string PolicyType_ProductsSpecificToNDCUPC = "Products Specific to NDC/UPC";
      public const string PolicyType_ProductsSpecificToNDCUPCAndLotNumber = "Products Specific to NDC/UPC and Lot Number";
      #endregion

      #region Profile Type For Custom
      public const string ProfileTypeForCustom_Manufacturer = "Manufacturer";
      public const string ProfileTypeForCustom_ManufacturerGroup = "ManufacturerGroup";
      public const string ProfileTypeForCustom_Wholesaler = "Wholesaler";
      public const string ProfileTypeForCustom_WholesalerGroup = "WholesalerGroup";
      public const string ProfileTypeForCustom_Repackager = "Repackager";
      #endregion

      #region Profile Type For Custom
      public const string ProfileType_Manufacturer = "Manufacturer";
      public const string ProfileType_Wholesaler = "Wholesaler";
      public const string ProfileType_Repackager = "Repackager";
      #endregion

      #region Model Annotation Fields
      public const string Model_User_Name = "User Name";
      public const string Model_Password = "Password";
      public const string Model_Divested_Own_Name = "Divested Own Name";
      public const string Model_Lot_Number_ID = "Lot Number ID";
      public const string Model_Product = "Product";
      public const string Model_Lot_Number = "Lot Number";
      public const string Model_Repackager = "Repackager";
      public const string Model_Select = "Select";
      public const string Model_Divested_Manufacture_Name = "Divested Manufacture Name";
      public const string Model_Expiration_Date = "Expiration Date";
      public const string Model_Recall = "Recall";
      public const string Model_Credit_Allowed = "Credit Allowed";
      public const string Model_Is_Recall = "Is Recall";
      public const string Model_Group_Name = "Group Name";
      public const string Model_Group_Type = "Group Type";
      public const string Model_Profile_Name = "Profile Name";
      public const string Model_Profile_Group_ID = "Profile Group ID";
      public const string Model_Profile_Type = "Profile Type";
      public const string Model_Region_Code = "Region Code";
      public const string Model_City = "City";
      public const string Model_State = "State";
      public const string Model_Created_By = "Created By";
      public const string Model_Created_DateTime = "Created DateTime";
      public const string Model_Modified_By = "Modified By";
      public const string Model_Modified_DateTime = "Modified DateTime";
      public const string Model_Is_Assigned = "Is Assigned";
      public const string Model_Is_Divested_Product = "IsDivestedProduct";
      public const string Model_Display_Order = "Display Order";
      public const string Model_DEA_Expiration_Date = "DEA Expiration Date";
      public const string Model_Email_Address = "Email Address";
      public const string Model_Zip_Code = "Zip Code";
      public const string Model_Phone_Number = "Phone Number";
      public const string Model_Contact_First_Name = "Contact First Name";
      public const string Model_Contact_Last_Name = "Contact Last Name";
      public const string Model_Contact_Phone_Number = "Contact Phone Number";
      public const string Model_Contact_Email_Address = "Contact Email Address";
      public const string Model_Contact_Fax_Number = "Contact_Fax_Number";
      public const string Model_Policy_Snapshot = "Policy_Snapshot";
      public const string Model_Direct_Accounts_Only = "Direct_Accounts_Only";
      public const string Model_Rollup_Profile_Code = "Rollup Profile Code";
      public const string Model_Toll_Free_Phone_Number = "Toll Free Phone Number";
      public const string Model_Return_Procedures = "Return Procedures";
      public const string Model_Representative_Number = "Representative Number";
      public const string Model_Wholesaler_Account_Number = "Wholesaler Account Number";
      public const string Model_Fax_Number = "Fax Number";
      public const string Model_LotNumber_ExpirationDate_Validation_Method = "LotNumber ExpirationDate Validation Method";
      public const string Model_Created_Date = "Created Date";
      public const string Model_Alert_Pending_Return_Authorizations = "Alert Pending Return Authorizations";
      public const string Model_Alert_Completed_Return_Authorizations = "Alert Completed Return Authorizations";
      public const string Model_Vendor_ID = "Vendor ID";
      public const string Model_Vendor_Load_Date = "Vendor Load Date";
      public const string Model_Sales_Org = "Sales Org";
      public const string Model_Qualanex_Display = "Qualanex.com Display";
      public const string Model_PDMA_Coordinator_Email_Address = "PDMA Coordinator Email Address";
      public const string Model_PDMA_Coordinator_Email_Address_Process_Completed = "PDMA Coordinator Email Address Process Completed";
      public const string Model_Please_Select_Type = "Please select Type";
      public const string Model_Confirm_Password = "Confirm Password";
      public const string Model_Password_Not_Matched = " password not matched";
      public const string Model_First_Name = "First Name";
      public const string Model_Last_Name = "Last Name";
      public const string Model_Please_Select_Profile_Name = "Please select Profile Name";
      public const string Model_Please_Select_User_Type = "Please select User Type";
      public const string Model_User_Type = "User Type";
      public const string Model_Current_Password = "Current password";
      public const string Model_New_Password = "New password";
      public const string Model_Confirm_New_Password = "Confirm new password";


      public const string Model_RegularExpression_Description = "^[^<>,<|>]+$";
      public const string Model_RegularExpression_Password = "((?=.*\\d)(?=.*[a-z])(?=.*[-$@$!%*#?&]).{6,20})";
      public const string Model_RegularExpression_Zipcode = "^(\\d{5}-\\d{4}|\\d{5}|\\d{9})$|^([a-zA-Z]\\d[a-zA-Z] \\d[a-zA-Z]\\d)$";


      public const string Model_Message_UserName_Is_Required = "User Name is required";
      public const string Model_Message_Password_Is_Required = "Password is required";
      public const string Model_Message_Enter_Passowrd = "Enter Passowrd";
      public const string Model_FormattedString_Message_String_Must_Be_Character_Long = "The {0} must be at least {2} characters long.";
      public const string Model_Message_Password_Must_Contains = "The must be at least Capital Lettet,digit and special symbol";
      public const string Model_Message_New_Confirm_Password_Not_Matched = "The new password and confirmation password do not match.";


      #endregion


      #region HTTPS Header Custom Contents
      public const string HTTPS_Header_Custom_Contents_CompAndRestType = "&comp=list&restype=container";
      #endregion

      #region File Access Modes
      public const string FileAccess_Methods_Read = "read";
      public const string FileAccess_Methods_Write = "write";

      #endregion

      #region Operators and Characters
      public const char Semicolon = ';';
      public const string Colon = ":";
      public const string Left_Round_Bracket = " (";
      public const string Right_Round_Bracket = " )";
      public const string Double_Backward_Slash = "\\";
      public const string Double_Forward_Slash = "//";
      public const string Single_Forward_Slash = "/";
      public const string Single_Backward_Slash = @"\";
      public const string Single_Dot = ".";
      public const string Triple_Dot = "...";
      public const char Comma = ',';
      public const string Single_Space = " ";
      public const string Json_Braces = "{}";
      #endregion

      #region Web Jobs Fields 
      #region common fields
      public const string Original_Product_Images_Folder = "images/ProductImage/Original/";
      public const string Content_Product_Images_Folder = "images/ProductImage/Content/";
      public const string FormatString_CreatingFile_For_Sync = "Creating file {0}.avro for {1}";
      #endregion

      #region Resource Names
      public const string ResourceName_Blob = "blob";
      public const string ResourceName_Queue = "queue";
      public const string ResourceName_Table = "table";
      public const string ResourceName_Blob_FirstLetterInCap = "Blob";
      public const string ResourceName_Queue_FirstLetterInCap = "Queue";
      public const string ResourceName_Table_FirstLetterInCap = "Table";
      #endregion

      #region Azure Queue Access Permission
      public const string Azure_Queue_Permission_Add = "add";
      public const string Azure_Queue_Permission_Read = "read";
      public const string Azure_Queue_Permission_Processmessages = "processmessages";
      public const string Azure_Queue_Permission_Update = "update";

      #endregion

      #region Azure config fields
      public const string Azure_ConnectionString_Format = "Storage.{0}.ConnectionString";
      public const string Azure_ConnectionString_NotDefined_Format = " Configuration Setting for {0} not defined!";

      #endregion

      #region Azure Queue Names
      public const string Azure_Queue_PartialSync = "partialsync";
      public const string Azure_Queue_BatchReturns = "batchreturns";
      public const string Azure_Queue_Queue = "queue";
      public const string Azure_Queue_FullSync = "fullsync";
      public const string Azure_Queue_Communications = "communications";
      //Need to create
      public const string Azure_Queue_ReportRequestQueue = "reportrequestqueue";
      public const string Azure_Queue_ReportNotificationQueue = "reportnotificationqueue";
      //Need to create

      #endregion

      #region Azure Account Names        
      public const string AzureAccount_Image = "qoskimages";
      public const string AzureAccount_Video = "qoskvideo";
      public const string AzureAccount_KioskSync = "qoskkiosksync";
      public const string AzureAccount_SysUpdates = "qosksysupdates";
      public const string AzureAccount_Products = "qoskproduct";
      #endregion

      #region Azure Container Names 
      public const string AzureContainer_Changeset = "changeset";
      public const string AzureContainer_Full = "full";
      public const string AzureContainer_SysExeUpdate = "sysexeupdate";
      public const string AzureContainer_Returns = "returns";
      public const string AzureContainer_ReturnMedia = "returnmedia";
      public const string AzureContainer_GeneratedReports = "generatedreports";
      public const string AzureContainer_AttachedFiles = "attachedfiles";
      public const string AzureContainer_Attached = "attached";
      public const string AzureContainer_Images = "images";
      public const string AzureContainer_Videos = "videos";
      #endregion

      #region File and Directory Names
      public const string DirectoryName_DownLoadZip = "DownLoadZip";
      public const string DirectoryName_Images = "images";
      public const string DirectoryName_Videos = "videos";
      public const string DirectoryName_AVROFiles = "AVROFiles";
      public const string DirectoryName_ProductImages = "ProductImages";
      public const string DirectoryName_ProductImage = "ProductImage";
      public const string DirectoryName_Original = "Original";
      public const string DirectoryName_Content = "Content";
      public const string DirectoryName_ScheduledReports = "ScheduledReports";


      public const string DirectoryName_AVROFilesForPartialSync = "AVROFilesForPartialSync";
      public const string DirectoryName_ZipFileForPartialSync = "ZipFileForPartialSync";
      public const string DirectoryName_TempFiles = "QTemp";


      public const string FileName_ReturnItem = "returnItemFile";
      public const string FileName_Qosklatest = "qosklatest";
      public const string FileName_QosklatestForPartialSync = "qosklatestForPartialSync";

      #endregion

      #region Sync Data Job Main Method
      public const string SyncDataJob_Main_Method = "WriteSyncDataToBlob";
      #endregion
      #endregion


      #region Constants
      public const string EmailService_credentialUserName = "rajkishore838@gmail.com";
      public const string EmailService_sentFrom = "rajkishore838@gmail.com";
      public const string EmailService_pwd = "mybirthplacebihar";
      public const string EmailService_SmtpClient = "smtp.gmail.com";
      public const string EmailService_PhoneCode = "Phone Code";
      public const string EmailService_SecurityCode = "Security Code";
      public const string EmailService_Identity = "ASP.NET Identity";
      public const string EmailService_MessageAndBodyFormat = "Your security code is {0}";
      public const string EmailService_EmailCode = "Email Code";


      public const string RouteConfig_DefaulUrl = "{resource}.axd/{*pathInfo}";

      #region Web Application Routes
      public const string RouteConfig_DefaulUrl_Name = "Default";
      public const string RouteConfig_DefaulUrl_Url = "{controller}/{action}/{id}";
      public const string RouteConfig_DefaulUrl_Controller = "User";
      public const string RouteConfig_DefaulUrl_Action = "Login";

      public const string ActionOutputCacheAttribute_Rd = "rd{0}_{1}_";
      public const string ActionOutputCacheAttribute_Ap = "ap{0}_{1}_";
      #endregion

      #region Report Application Routes
      public const string RouteConfig_ReportUrl_OptionalParameter = "ReportId";
      public const string RouteConfig_ReportUrl_Name = "Report";
      public const string RouteConfig_ReportUrl_Url = "{controller}/{action}/{ReportId}";
      public const string RouteConfig_ReportUrl_Controller = "Report";
      public const string RouteConfig_ReportUrl_Action = "Index";
      #endregion

      #region Reprot common constants
      public const string ReturnUrl_Content_Report = "/report/";
      public const string ReturnUrl_Content_Home = "Home/";
      #endregion

      #region Startup Urls
      public const string Web_Startup_Url = "/User/Login";
      public const string Report_Startup_Url = "/Report/Index";
      #endregion

      #region Shared Cookie Name and Domain
      public const string SharedCookieName = "Qualanex-HC";
      public const string DeployedDomain = "DeployedDomain";
      //public const string SharedCookieDomain = ".dvqosk.com";
      #endregion


      public const string WebApiConfig_Url = "DefaultApi";
      public const string WebApiConfig_RouteTemplate = "api/{controller}/{id}";

      public const string QualanexQoskGlobalFilterAttribute_controller = "controller";
      public const string QualanexQoskGlobalFilterAttribute_User = "User";
      public const string QualanexQoskGlobalFilterAttribute_Action = "action";
      public const string QualanexQoskGlobalFilterAttribute_Login = "Login";


      public const string ActionOutputCacheAttribute_SwitchWriter = "SwitchWriter";
      public const string ControlModelBinder_Type = ".Type";
      public const string ControlModelBinder_textbox = "textbox";
      public const string ControlModelBinder_checkbox = "checkbox";
      public const string ControlModelBinder_ddl = "ddl";
      public const string ControlModelBinder_datepicker = "datepicker";
      public const string ExceptionFilterAttribute_EnableError = "EnableErrorlogging";
      public const string ExceptionFilterAttribute_yes = "yes";
      public const string ManageLayoutConfig_UserDataWithLink = "UserDataWithLink";
      public const string QoskAuthorizeAttribute_View = "UnauthorizedAccess";
      public const string TraceFilterAttribute_EnableTracing = "EnableTracing";
      public const string TraceFilterAttribute_FormCollection = "FormCollection";
      public const string Role_QoskLiteUser = "QLUser";
      public const string Role_AdminCuser = "Admin,CUser";
      public const string Role_AdminRuser = "Admin,RUser";
      public const string Role_AllUser = "Admin,RUser,User,CUser,QLUser";
      public const string HomeController_Manufacture = "Manufacture";
      public const string HomeController_ADMIN = "ADMIN";
      public const string HomeController_OTHERS = "OTHERS";
      public const string LotNumberController_Lotmessage = "Lotmessage";
      public const string LotNumberController_AllUser = "Admin,RUser,CUser";
      public const string LotNumberController_LotSearchPanel = "LotSearchPanel";
      public const string LotNumberController_bit = "bit";
      public const string LotNumberController_datetime1 = "datetime1";
      public const string LotNumberController_string = "string";
      public const string LotNumberController_bigint = "bigint";
      public const string LotNumberController_int = "int";
      public const string LotNumberController_decimal = "decimal";
      public const string LotNumberController_select = "select";
      public const string LotNumberController_RXorOTC = "RXorOTC";
      public const string LotNumberController_LotListing = "LotListing";
      public const string LotNumberController_ProductListing = "ProductListing";
      public const string LotNumberController_CaseSize1 = "CaseSize1";
      public const string LotNumberController_CaseSize2 = "CaseSize2";
      public const string LotNumberController_CaseSize3 = "CaseSize3";
      public const string LotNumberController_Strength = "Strength";
      public const string LotNumberController_UnitDose = "UnitDose";
      public const string LotNumberController_Refrigerated = "Refrigerated";
      public const string LotNumberController_DivestedBYLot = "DivestedBYLot";
      public const string LotNumberController_IsRepackager = "IsRepackager";
      public const string LotNumberController_ControlNumber = "ControlNumber";
      public const string LotNumberController_DosageCode = "DosageCode";
      public const string LotNumberController_UnitOfMeasure = "UnitOfMeasure";
      public const string LotNumberController_RecallID = "RecallID";
      public const string LotNumberController_Profiles = "Profiles";
      public const string LotNumberController_ManufacturerRepackager = "Manufacturer,Repackager";
      public const string LotNumberController_value = "value";
      public const string LotNumberController_text = "text";
      public const string PolicyController_NDC = "NDC";
      public const string PolicyController_NDCLOT = "NDCLOT";
      public const string PolicyController_Select = "Select";
      public const string PolicyController_asc = "asc";
      public const string PolicyController_NOT_EMPTY = "NOT_EMPTY";
      public const string PolicyController_NOT_NULL = "NOT_NULL";
      public const string PolicyController_EMPTY = "EMPTY";
      public const string PolicyController_NULL = "NULL";
      public const string PolicyController_CONTAINS_CASE_SENSITIVE = "CONTAINS_CASE_SENSITIVE";
      public const string PolicyController_CONTAINS = "CONTAINS";
      public const string PolicyController_DOES_NOT_CONTAIN_CASE_SENSITIVE = "DOES_NOT_CONTAIN_CASE_SENSITIVE";
      public const string PolicyController_DOES_NOT_CONTAIN = "DOES_NOT_CONTAIN";
      public const string PolicyController_EQUAL_CASE_SENSITIVE = "EQUAL_CASE_SENSITIVE";
      public const string PolicyController_NOT_EQUAL = "NOT_EQUAL";
      public const string PolicyController_GREATER_THAN = "GREATER_THAN";
      public const string PolicyController_LESS_THAN = "LESS_THAN";
      public const string PolicyController_GREATER_THAN_OR_EQUAL = "GREATER_THAN_OR_EQUAL";
      public const string PolicyController_LESS_THAN_OR_EQUAL = "LESS_THAN_OR_EQUAL";
      public const string PolicyController_STARTS_WITH_CASE_SENSITIVE = "STARTS_WITH_CASE_SENSITIVE";
      public const string PolicyController_STARTS_WITH = "STARTS_WITH";
      public const string PolicyController_ENDS_WITH_CASE_SENSITIVE = "ENDS_WITH_CASE_SENSITIVE";
      public const string PolicyController_ENDS_WITH = "ENDS_WITH";
      public const string PolicyController_EQUAL = "EQUAL";
      public const string PolicyController_NOT_EQUAL_CASE_SENSITIVE = "NOT_EQUAL_CASE_SENSITIVE";
      public const string PolicyController_Space = " ";
      public const string PolicyController_NOTLIKE = " NOT LIKE '" + "" + "'";
      public const string PolicyController_LIKE = " LIKE '" + "" + "'";
      public const string PolicyController_LIKEPERCENT = " LIKE '%";
      public const string PolicyController_PERCENT = "%'";
      public const string PolicyController_SQLLATIN1 = " COLLATE SQL_Latin1_General_CP1_CS_AS";
      public const string PolicyController_NOTLIKEPERCENT = " NOT LIKE '%";
      public const string PolicyController_EQUALWITHCOMMA = " = '";
      public const string PolicyController_COMMA = "'";
      public const string PolicyController_BINARY = " BINARY ";
      public const string PolicyController_NOTEQUALWITHCOMMA = " <> '";
      public const string PolicyController_GRATERTHENWITHCOMMA = " > '";
      public const string PolicyController_LESSTHENWITHCOMMA = " < '";
      public const string PolicyController_GRATERTHENEQUALWITHCOMMA = " >= '";
      public const string PolicyController_LESSTHENEQUALWITHCOMMA = " <= '";
      public const string PolicyController_LIKEWITHCOMMA = " LIKE '";
      public const string PolicyController_WHERE = " WHERE (";
      public const string PolicyController_ANDWITHBREKET = ") AND (";
      public const string PolicyController_ANDWITHSPACE = " AND ";
      public const string PolicyController_OR = " OR ";
      public const string PolicyController_BREKET = ")";
      public const string PolicyController_filterscount = "filterscount";
      public const string PolicyController_filtervalue = "filtervalue";
      public const string PolicyController_filtercondition = "filtercondition";
      public const string PolicyController_filterdatafield = "filterdatafield";
      public const string PolicyController_filteroperator = "filteroperator";

      public const string ProductController_ProductSearchPanel = "ProductSearchPanel";
      public const string ProductController_Manufacturer = "Manufacturer";
      public const string ProductController_ProductListing = "ProductListing";
      public const string ProductController_Wholesaler = "Wholesaler";
      public const string ProductController_ProductID = "ProductID";
      public const string ProductController_User = "User";
      public const string ProductController_Mode = "mode";
      public const string ProductController_Edit = "edit";
      public const string ProductController_AzureStorage = "AzureStorage";
      public const string ProductController_LocalMedia = "LocalMedia";
      public const string ProductController_BREKET_LEFTWITHSPACE = "( ";
      public const string ProductController_BREKET_RIGHTWITHSPACE = " )";
      public const string ProductController_SelectedProfileID = "SelectedProfileID";
      public const string ProductController_Contentfile = "Contentfile";
      public const string ProductController_Productimgfile = "productimgfile";
      public const string ProductController_NDCUPCWithDashes = "NDCUPCWithDashes";
      public const string ProductController_Description = "Description";
      public const string ProductController_ContainerType = "ContainerType";
      public const string ProductController_Original = "Original";
      public const string ProductController_OriginalPtah = "../images/ProductImage/Original";
      public const string ProductController_RepackagerPath = "../images/ProductImage/Repackager";
      public const string ProductController_Underscore = "_";
      public const string ProductController_ContentPath = "../images/ProductImage/Content";
      public const string ProductController_Account = "Account";
      public const string ProductController_AccessKey = "AccessKey";
      public const string ProductController_AzureMediaName = "AzureMediaName";
      public const string ProductController_Qoskkiosksync = "qoskkiosksync";
      public const string ProductController_Returnmedia = "returnmedia";
      public const string ProductController_Read = "Read";
      public const string ProductController_productcontainerimgfile = "productcontainerimgfile";
      public const string ProductController_HiddenProductImageDetailID = "HiddenProductImageDetailID";
      public const string ProductController_Version = "Version";
      public const string ProductController_Repackager = "Repackager";
      public const string ProductController_Profiles = "Profiles";
      public const string ProductController_Select = "Select";
      public const string ProductController_Value = "value";
      public const string ProductController_Text = "text";
      public const string ProductController_Asc = "asc";
      public const string ProductController_Name = "Name";
      public const string ProductController_ProfileCode = "ProfileCode";


      public const string ProfileController_ManufacturerWPT = "Manufacturer - WPT Only";
      public const string ProfileController_LeftBreket = "(";
      public const string ProfileController_RightBreket = ")";
      public const string ProfileController_Grp = "-GRP";
      public const string ProfileController_Sucesss = "Sucesss";
      public const string ProfileController_Fail = "Fail";
      public const string ProfileController_SelectGroup = "Select Group";
      public const string ProfileController_Unsuccess = "Unsuccess";
      public const string ProfileController_NA = "NA";
      public const string ProfileController_NotExist = "NotExist";
      public const string ProfileController_Exist = "Exist";
      public const string ProfileController_ToBeDeleted = "To Be Deleted";


      public const string QoskController_Small_AtoZ = @"abcdefghijklmnopqrstuvwxyz";
      public const string QoskController_Capital_AtoZ = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      public const string QoskController_Number = @"0123456789";
      public const string QoskController_Pattern = @" !""#$%&'()*+,./:;<>?@[\]^_{|}~";
      public const string QoskController_Length = "length";


      public const string ReportController_Manufacturer = "Manufacturer";
      public const string ReportController_SelectRegion = "Select Region";
      public const string ReportController_RegionCode = "RegionCode";
      public const string ReportController_RegionDescription = "RegionDescription";
      public const string ReportController_SelectQosk = "Select Qosk";
      public const string ReportController_QoskId = "QoskId";
      public const string ReportController_MachineName = "MachineName";
      public const string ReportController_Select = "Select";
      public const string ReportController_ProfileCode = "ProfileCode";
      public const string ReportController_Name = "Name";
      public const string ReportController_PlayVideo = "PlayVideo";
      public const string ReportController_ProductSearchPanel = "ProductSearchPanel";
      public const string ReportController_RXorOTC = "RXorOTC";
      public const string ReportController_Rprescription = "Rx (Prescription)";
      public const string ReportController_OTC = "OTC (Over the Counter)";
      public const string ReportController_True = "true";
      public const string ReportController_False = "false";
      public const string ReportController_Value = "Value";
      public const string ReportController_Text = "Text";
      public const string ReportController_ProductListing = "ProductListing";

      public const string TelerikReportsController_MvcDemoApp = "MvcDemoApp";
      public const string TelerikReportsController_MapPath = "~/";
      public const string TelerikReportsController_TelerikReports = @"TelerikReports";



      public const string UserController_UserTypeCode = "UserTypeCode";
      public const string UserController_UserTypeName = "UserTypeName";
      public const string UserController_EnteredUserName = "EnteredUserName";
      public const string UserController_Password = "Password";
      public const string UserController_IncorrectCredential = "User name or password is not correct";
      public const string UserController_Fail = "Fail";
      public const string UserController_SelectedProfileID = "SelectedProfileID";
      public const string UserController_User = "User";
      public const string UserController_CUser = "CUser";
      public const string UserController_RUser = "RUser";
      public const string UserController_Admin = "Admin";
      public const string UserController_Success = "SUCCESS";
      public const string UserController_FailResult = "FAIL";
      public const string UserController_ForgotPassword = "FgtPwd";
      public const string UserController_NetworkCredentialUserName = "userName";
      public const string UserController_NetworkCredentialPassword = "password";
      public const string UserController_CorporateSMTP = "host";
      public const string UserController_NewPassword = "New Password";
      public const string UserController_Ok = "OK";
      public const string UserController_Fails = "Fails";
      public const string UserController_MailNotSent = "MAILNOTSENT";
      public const string UserController_ValidEmail = "ValidEmail";
      public const string UserController_GroupProfiles = "_GroupProfiles";
      public const string UserController_ProfileGroups = "_ProfileGroups";
      public const string UserController_SuccessResult = "Success";
      public const string UserController_UnsuccessResult = "Insuccess";


      public const string Model_ApplicationUser_DefaultConnection = "DefaultConnection";
      public const string Model_ApplicationUser_User = "User";
      public const string Model_ApplicationUser_UserRole = "UserRole";
      public const string Model_ApplicationUser_UserLogin = "UserLogin";
      public const string Model_ApplicationUser_UserClaim = "UserClaim";
      public const string Model_ApplicationUser_UserClaimId = "UserClaimId";
      public const string Model_ApplicationUser_Role = "Role";
      public const string Model_ApplicationUser_RoleId = "RoleId";

      public const string Model_CommonDataAccess_Bit = "bit";
      public const string Model_CommonDataAccess_Equal = "Equal";
      public const string Model_CommonDataAccess_Contains = "Contains";
      public const string Model_CommonDataAccess_Products = "Products";
      public const string Model_CommonDataAccess_LotNumbers = "LotNumbers";
      public const string Model_CommonDataAccess_ProfileCode = "ProfileCode";
      public const string Model_CommonDataAccess_In = "IN";
      public const string Model_CommonDataAccess_ProductID = "ProductID";


      public const string Model_CommonDataAccess_NDCUPC = "NDCUPC";
      public const string Model_CommonDataAccess_NDCUPCWithDashes = "NDCUPCWithDashes LIKE '%";
      public const string Model_CommonDataAccess_NDCLikePercent = "%' OR NDC LIKE '%";
      public const string Model_CommonDataAccess_UPCLikePercent = "%' OR UPC LIKE '%";
      public const string Model_CommonDataAccess_INWithLeftBraket = " IN (";
      public const string Model_CommonDataAccess_RightBraket = ")";


      public const string Model_GroupDataAccess_SelectGroup = "Selectw Group";
      public const string Model_GroupDataAccess_ChainPharmacy = "Chain Pharmacy";
      public const string Model_GroupDataAccess_Manufacturer = "Manufacturer";
      public const string Model_GroupDataAccess_RetailPharmacy = "Retail Pharmacy";
      public const string Model_GroupDataAccess_Repackager = "Repackager";
      public const string Model_GroupDataAccess_Wholesaler = "Wholesaler";
      public const string Model_GroupDataAccess_ProfilesProfileCode = " [Profiles].ProfileCode = ";
      public const string Model_GroupDataAccess_OneEqualOne = "1=1";
      public const string Model_GroupDataAccess_ProfileGroup = "ProfileGroup";
      public const string Model_GroupDataAccess_ProfilesRollupProfileCode = " [Profiles].RollupProfileCode = ";
      public const string Model_GroupDataAccess_GroupProfileData = "GroupProfileData";
      public const string Model_GroupDataAccess_AddNewGroup = "Add New Group";
      public const string Model_GroupDataAccess_NA = "NA";
      public const string Model_GroupDataAccess_GProfileCode = " g.ProfileCode = {0}";
      public const string Model_GroupDataAccess_AndGProfileCode = " AND g.ProfileGroupID in ( {0} ) ";
      public const string Model_GroupDataAccess_Asc = "asc";
      public const string Model_GroupDataAccess_GroupProfileCode = " [Group].ProfileCode = {0}";
      public const string Model_GroupDataAccess_AndGroupProfileCode = " AND [Group].ProfileGroupID in ( {0} ) ";
      public const string Model_GroupDataAccess_Where = " WHERE ";


      public const string Model_LotNumbersDataAccess_Insert = "Insert";
      public const string Model_LotNumbersDataAccess_Update = "Update";
      public const string Model_LotNumbersDataAccess_BigLeftBraket = "[";
      public const string Model_LotNumbersDataAccess_BigRightBraket = "]";
      public const string Model_LotNumbersDataAccess_Delete = "Delete";


      public const string Model_PolicyDataAccess_NA = "N/A";
      public const string Model_PolicyDataAccess_L = "L";
      public const string Model_PolicyDataAccess_Insert = "Insert";
      public const string Model_PolicyDataAccess_LeftBraketWithSpace = " ( ";
      public const string Model_PolicyDataAccess_RightBraketWithSpace = " ) ";
      public const string Model_PolicyDataAccess_Type = " Type = '";
      public const string Model_PolicyDataAccess_Comma = "'";
      public const string Model_PolicyDataAccess_RetailPharmacy = "Retail Pharmacy";
      public const string Model_PolicyDataAccess_Wholesaler = "Wholesaler";
      public const string Model_PolicyDataAccess_ChainPharmacy = "Chain Pharmacy";
      public const string Model_PolicyDataAccess_HospitalClinic = "Hospital/Clinic";
      public const string Model_PolicyDataAccess_ProfileCode = "ProfileCode";
      public const string Model_PolicyDataAccess_Equal = "EQUAL";
      public const string Model_PolicyDataAccess_PolicyType = "PolicyType";
      public const string Model_PolicyDataAccess_Update = "Update";
      public const string Model_PolicyDataAccess_Delete = "Delete";
      public const string Model_PolicyDataAccess_ProfileGroupID = " ProfileGroupID in ( {0} ) ";
      public const string Model_PolicyDataAccess_ProductsProfileCode = " Products.ProfileCode in ( {0} ) ";
      public const string Model_PolicyDataAccess_PolicyID = "PolicyID";
      public const string Model_PolicyDataAccess_ProductID = " ProductID = ";
      public const string Model_PolicyDataAccess_DiverseMsg = " AND DiverseManufacturerProfileCode is null AND RepackagerProfileCode is null ";
      public const string Model_PolicyDataAccess_PolicyIDWithEqual = " PolicyID = ";
      public const string Model_PolicyDataAccess_NDC = "NDC";
      public const string Model_PolicyDataAccess_ProfileCodeInZero = " ProfileCode in ( {0} ) ";
      public const string Model_PolicyDataAccess_PolicyProfileTypeProfileCode = " [PolicyProfileType].ProfileCode = ";
      public const string Model_PolicyDataAccess_ProfileCodeWithEqual = " ProfileCode = ";
      public const string Model_PolicyDataAccess_PolicyProfileTypeProfileCodeIn = " PolicyProfileType.ProfileCode in ( {0} ) ";
      public const string Model_PolicyDataAccess_wrw = "Wrw";
      public const string Model_PolicyDataAccess_PolicyProfileTypeWithMsg = " AND ( PolicyProfileType.IsDeleted is null OR PolicyProfileType.IsDeleted = 'false')";
      public const string Model_PolicyDataAccess_NDCView = "NDCView";
      public const string Model_PolicyDataAccess_PolicyProfileTypeProductID = " PolicyProfileType.ProductID = ";
      public const string Model_PolicyDataAccess_PolicyProfileTypePolicyID = " PolicyProfileType.PolicyID = ";
      public const string Model_PolicyDataAccess_PolicyTypeOne = "1";
      public const string Model_PolicyDataAccess_PolicyTypeTwo = "2";
      public const string Model_PolicyDataAccess_PolicyTypeThree = "3";


      public const string Model_ProductDataAccess_TotalRecord = "totalRecord";
      public const string Model_ProductDataAccess_ModifiedDate = "ModifiedDate";

      public const string Model_ProfilesDataAccess_City = "City";
      public const string Model_ProfilesDataAccess_Active = "Active";
      public const string Model_ProfilesDataAccess_LeftBraces = "(";
      public const string Model_ProfilesDataAccess_RightBraces = ")";
      public const string Model_ProfilesDataAccess_QualanexInternal = "QualanexInternal";
      public const string Model_ProfilesDataAccess_Deleted = "Deleted";
      public const string Model_ProfilesDataAccess_RetailPharmacy = "Retail Pharmacy";
      public const string Model_ProfilesDataAccess_ManufacturerRepackagerProfileCode = " ManufacturerRepackagerProfileCode = ";
      public const string Model_ProfilesDataAccess_AccountType = " AccountType = '";
      public const string Model_ProfilesDataAccess_AccountNumber = " AccountNumber = '";
      public const string Model_ProfilesDataAccess_AccountNumberWithoutComma = " AccountNumber = ";
      public const string Model_ProfilesDataAccess_GRP = "-GRP";
      public const string Model_ProfilesDataAccess_Profile = "Profile";
      public const string Model_ProfilesDataAccess_ProfileGroup = "ProfileGroup";
      public const string Model_ProfilesDataAccess_TypeWithEqual = " Type = '";
      public const string Model_ProfilesDataAccess_Zero = "0";

      public const string Model_ReportDataAccess_Comma = ",";

      public const string Model_QoskDataAccess_Number = "10100";

      public const string Model_SaveChangeTracker_Version = "Version";
      public const string Model_SaveChangeTracker_ModifiedBy = "ModifiedBy";
      public const string Model_SaveChangeTracker_Update = "Update";


      public const string Model_UsersDataAccess_User_UserName = " [User].UserName LIKE '%";
      public const string Model_UsersDataAccess_User_UserType = " [User].UserType = '";
      public const string Model_UsersDataAccess_User_UserMsg = " AND( [User].IsDeleted is null OR [User].IsDeleted=0)";
      public const string Model_UsersDataAccess_User_FourHundred = "400";
      public const string Model_UsersDataAccess_User_TwoHundred = "200";
      public const string Model_UsersDataAccess_User_Hundred = "100";
      public const string Model_UsersDataAccess_User_MinusOne = "-1";

      public const string Startup_QoskCloud = "QoskCloud";
      public const string Settings_QoskCloud = "Qosk";


      public const string EmailFormat_OpenTable = "<table>";
      public const string EmailFormat_OpenTableRaw = "<tr><td>Hi,</td></tr>";
      public const string EmailFormat_EndTableRaw = "<tr><td>Your Tempory Password is:{0}</td></tr>";
      public const string EmailFormat_OpenTD = "<tr><td>You can  generate new password  by login with temporary password</td></tr>";
      public const string EmailFormat_EndTD = "<tr><td>Thanks</td></tr>";
      public const string EmailFormat_EndTable = "</table>";
      public const string EmailFormat_QualanexQosk = "<tr><td>Qualanex Qosk</td></tr>";

      #region Reports
      public const string Report_Retailer_Box_Detail_Report = "Sample_Report_Retailer_Box_Detail";
      #endregion
      #endregion

      #region Report Constants
      public const string Report_Parameter_Initial = "RP_";

      public const string Report_AssemblyName = "Qualanex.QoskCloud.Web.Areas.Report";
      public const string Report_Class_Full_QualifiedName = Report_AssemblyName + ".{0}.{1}";
      public const string Report_Module_Retailer = "Retailer";
      public const string Report_Module_Wholesaler = "Wholesaler";
      public const int Report_SASUrl_24HrTimeout = 24 * 60;

      public const string Report_ReportFormDataXmlPath = "~/App_Data/ReportFormData.xml";
      public const string Report_ReportFormDataXmlNode_Region = "Region";
      public const string Report_ReportFormDataXmlNode_FileType = "FileType";
      public const string Report_ReportFormDataXmlNode_Hour = "Hour";
      public const string Report_ReportFormDataXmlNode_Minute = "Minute";
      public const string Report_ReportFormDataXmlNode_ScheduleType = "ScheduleType";
      public const string Report_ReportFormDataXmlNode_Meridieum = "Meridieum";
      public const string Report_ReportFormDataXmlNode_ID = "ID";
      public const string Report_ReportFormDataXmlNode_Name = "Name";
      public const string Report_ReportTimeZoneList_Key = "key";
      public const string Report_EveryList_Key = "Key";
      public const string Report_EveryList_Value = "Value";

      public const string Report_ReportParameter = "_Parameter";

      public const string Report_WholesalerParameter = "Wholesaler/Parameters/";
      public const string Report_RetailerParameter = "Retailer/Parameters/";

      public const string Report_Parameter_UserID = "UserID";
      public const string Report_Parameter_NDCNumber = "NDCNumber";
      public const string Report_Parameter_DataStartDate = "DataStartDate";
      public const string Report_Parameter_DataEndDate = "DataEndDate";
      public const string Report_Parameter_FileType = "FileType";
      public const string Report_Parameter_ReportDataTimeZone = "ReportDataTimeZone";
      public const string Report_Parameter_ReportScheduleTimeZone = "ReportScheduleTimeZone";
      public const string Report_Parameter_Timezone = "Timezone";


      public const string Report_Parameter_BoxNumber = "BoxNumber";
      public const string Report_ReportRender = "ReportRender";
      public const string Report_ReportScheduler = "ReportScheduler";


      public const string Report_ReportPreview = "ReportPreview";

      public const string Report_ActionName_RetailerBoxSummary = "Sample Reports_Retailer_Box Summary";
      public const string Report_ActionName_RetailerMFGSummary = "Sample Reports_Retailer_MFG Summary";
      public const string Report_ActionName_RetailerMFGCreditSummary = "Sample Reports_Retailer_MFG Summary_w Credits";
      public const string Report_ActionName_RetailerReturnToStock = "Sample Reports_Retailer_Return to Stock Details";
      public const string Report_ActionName_RetailerReturnToStockSummary = "Sample Reports_Retailer_Return to Stock Summary";
      public const string Report_ActionName_WholesalerBoxDetail = "Sample Reports_Wholesaler_Box Detail";
      public const string Report_ActionName_WholesalerBoxSummary = "Sample Reports_Wholesaler_Box Summary";
      public const string Report_ActionName_WholesalerMFGCreditSummary = "Sample Reports_Wholesaler_MFG Summary_w Credits";
      public const string Report_ActionName_WholesalerMFGSummary = "Sample Reports_Wholesaler_MFG Summary";
      public const string Report_ActionName_WholesalerReturnToStock = "Sample Reports_Wholesaler_Return to Stock Details";

      public const string Report_ActionName_RetailerProductDetail = "Product Details";
      public const string Report_ActionName_RetailerWasteReport = "Waste Report";
      public const string Report_ActionName_PharmacySummary = "Pharmacy Summary";
      public const string Report_ActionName_NonCreditableSummary = "Non Creditable Summary";
      public const string Report_ActionName_ManufacturerSummary = "Manufacturer Summary";
      public const string Report_ActionName_ReturnToStock = "Return To Stock";

      public const string Report_ActionName_RetailerProduct_Detail_Criteria = "Product Details_Criteria";
      public const string Report_ActionName_Waste_Report_Criteria = "Waste Report_Criteria";
      public const string Report_ActionName_Pharmacy_Summary_Criteria = "Pharmacy Summary_Criteria";
      public const string Report_ActionName_Non_Creditable_Summary_Criteria = "Non Creditable Summary_Criteria";
      public const string Report_ActionName_Manufacturer_Summary_Criteria = "Manufacturer Summary_Criteria";
      public const string Report_ActionName_Return_To_Stock_Criteria = "Return To Stock_Criteria";
      public const string Report_ActionName_Waste_Report = "Waste Report";

      public const string Report_ProductDetailSearchPanel = "ProductDetailSearchPanel";
      public const string Report_WasteReportSearchPanel = "WasteReportSearchPanel";
      public const string Report_ProductDetailListPanel = "ProductDetailListPanel";
      public const string Report_WasteReportListPanel = "WasteReportListPanel";
      public const string Report_PharmacySummarySearchPanel = "PharmacySummarySearchPanel";
      public const string Report_PharmacySummaryListPanel = "PharmacySummaryListPanel";
      public const string Report_NonCreditableSummarySearchPanel = "NonCreditableSummarySearchPanel";
      public const string Report_NonCreditableSummaryListPanel = "NonCreditableSummaryListPanel";
      public const string Report_ManufacturerSummarySearchPanel = "ManufacturerSummarySearchPanel";
      public const string Report_ManufacturerSummaryListPanel = "ManufacturerSummaryListPanel";
      public const string Report_ReturnToStockSearchPanel = "ReturnToStockSearchPanel";
      public const string Report_ReturnToStockListPanel = "ReturnToStockListPanel";


      public const string Report_FileType_PDF = "PDF";
      public const string Report_Using_FileType_pdf = "pdf";
      public const string Report_Extension_Dot = ".";
      public const string Report_Json_File_Path_RetailerBoxDetail = "~/ReportColumns/RetailerBoxDetail.json";
      public const string Report_Json_File_Path_RetailerBoxSummary = "~/ReportColumns/RetailerBoxSummary.json";
      public const string Report_Json_File_Path_RetailerMFGSummary = "~/ReportColumns/RetailerMFGSummary.json";
      public const string Report_Json_File_Path_RetailerMFGSummaryCredits = "~/ReportColumns/RetailerMFGSummaryCredits.json";
      public const string Report_Json_File_Path_RetailerReturnToStockDetails = "~/ReportColumns/RetailerReturnToStockDetails.json";
      public const string Report_Json_File_Path_WholesalerBoxDetails = "~/ReportColumns/WholesalerBoxDetails.json";
      public const string Report_Json_File_Path_WholesalerBoxSummary = "~/ReportColumns/WholesalerBoxSummary.json";
      public const string Report_Json_File_Path_WholesalerMFGSummaryCredits = "~/ReportColumns/WholesalerMFGSummaryCredits.json";
      public const string Report_Json_File_Path_WholesalerMFGSummary = "~/ReportColumns/WholesalerMFGSummary.json";
      public const string Report_Json_File_Path_WholesalerReturnToStockDetails = "~/ReportColumns/WholesalerReturnToStockDetails.json";
      public const string Report_Json_File_Path_RetailerProductDetail = "~/ReportColumns/RetailerProductDetail.json";
      public const string Report_Json_File_Path_WasteReport = "~/ReportColumns/WasteReport.json";

      //Product detail report parameters
      public const string Report_ProductDetail_Parameter_StoreNumber = "StoreNumber";
      public const string Report_ProductDetail_Parameter_StoreName = "StoreName";
      public const string Report_ProductDetail_Parameter_VendorName = "VendorName";
      public const string Report_ProductDetail_Parameter_ProductDescription = "ProductDescription";
      public const string Report_ProductDetail_Parameter_LotNumber = "LotNumber";
      public const string Report_ProductDetail_Parameter_ExpDate = "ExpDate";
      public const string Report_ProductDetail_Parameter_FullQty = "FullQty";
      public const string Report_ProductDetail_Parameter_PartialQty = "PartialQty";
      public const string Report_ProductDetail_Parameter_UnitPriceBefore = "UnitPriceBefore";
      public const string Report_ProductDetail_Parameter_CreditableAmountBeforeMFGDiscount = "CreditableAmountBeforeMFGDiscount";
      public const string Report_ProductDetail_Parameter_IndateAmount = "IndateAmount";
      public const string Report_ProductDetail_Parameter_StoreTransferAmount = "StoreTransferAmount";
      public const string Report_ProductDetail_Parameter_ReturnStatus = "ReturnStatus";
      public const string Report_ProductDetail_Parameter_OutofPolicyCode = "OutofPolicyCode";
      public const string Report_ProductDetail_Parameter_OutofpolicyCodeDescription = "OutofpolicyDescription";
      public const string Report_ProductDetail_Parameter_Strength = "Strength";
      public const string Report_ProductDetail_Parameter_ItemGuid = "ItemGuid";
      public const string Report_ProductDetail_Parameter_QoskProcessDate = "QoskProcessDate";
      public const string Report_ProductDetail_Parameter_ControlNumber = "ControlNumber";
      public const string Report_ProductDetail_Parameter_PackageSize = "PackageSize";
      public const string Report_ProductDetail_Parameter_UnitPriceAfter = "UnitPriceAfter";
      public const string Report_ProductDetail_Parameter_CreditableAmountAfter = "CreditableAmountAfter";
      public const string Report_ProductDetail_Parameter_RecalledProduct = "RecalledProduct";
      public const string Report_ProductDetail_Parameter_DiscontinuedProduct = "DiscontinuedProduct";
      public const string Report_ProductDetail_Parameter_RXorOTC = "RXorOTC";
      public const string Report_ProductDetail_Parameter_DosageForm = "DosageForm";
      public const string Report_ProductDetail_Parameter_PackageForm = "PackageForm";
      public const string Report_ProductDetail_Parameter_PartialPercentage = "PartialPercentage";
      public const string Report_ProductDetail_Parameter_ExtendedDiscountPercent = "ExtendedDiscountPercent";


      //Waste report parameters

      public const string WasteReport_Parameter_ItemGuid = "ItemGuid";
      public const string WasteReport_Parameter_Description = "ProductDescription";
      public const string WasteReport_Parameter_Strength = "Strength";
      public const string WasteReport_Parameter_FullQuantity = "FullQty";
      public const string WasteReport_Parameter_LotNumber = "LotNumber";
      public const string WasteReport_Parameter_SealedOpenCase = "SealedOpenCase";
      public const string WasteReport_Parameter_WasteCode = "WasteCode";
      public const string WasteReport_Parameter_WasteStreamProfile = "WasteStreamProfile";
      public const string WasteReport_Parameter_PartialQuantity = "PartialQty";

      public const string WasteReport_Parameter_StoreNumber = "StoreNumber";
      public const string WasteReport_Parameter_StoreName = "StoreName";
      public const string WasteReport_Parameter_ExpDate = "ExpDate";
      public const string WasteReport_Parameter_PackageSize = "PackageSize";
      public const string WasteReport_Parameter_ControlNumber = "ControlNumber";
      public const string WasteReport_Parameter_DosageForm = "DosageForm";
      public const string WasteReport_Parameter_PackageForm = "PackageForm";

      public const string GetDetails_AutoComplete_VendorName = "VendorName";
      public const string GetDetails_AutoComplete_NDCNumber = "NDCNumber";
      public const string GetDetails_AutoComplete_ProductDescription = "ProductDescription";
      public const string GetDetails_AutoComplete_LotNumber = "LotNumber";
      public const string GetDetails_AutoComplete_StoreName = "StoreName";
      public const string GetDetails_AutoComplete_OutofPolicyCode = "OutofPolicyCode";
      public const string GetDetails_AutoComplete_OutofpolicyCodeDescription = "OutofpolicyCodeDescription";
      public const string GetDetails_AutoComplete_ItemID = "#ItemID";
      public const string GetDetails_AutoComplete_Description = "Description";
      public const string GetDetails_AutoComplete_SealedOpenCase = "SealedOpenCase";
      public const string GetDetails_AutoComplete_ManufacturerName = "ManufacturerName";

      //Pharmacy Summary Parameters
      public const string Report_Parameter_PharmacySummary_StoreNumber = "StoreNumber";
      public const string Report_Parameter_PharmacySummary_StoreName = "StoreName";
      public const string Report_Parameter_PharmacySummary_WholesalerCustomerNumber = "WholesalerCustomerNumber";
      public const string Report_Parameter_PharmacySummary_RegionName = "RegionName";
      public const string Report_Parameter_PharmacySummary_ReturnCreditableValue = "ReturnCreditableValue";
      public const string Report_Parameter_PharmacySummary_ReturnCreditableQty = "ReturnCreditableQty";
      public const string Report_Parameter_PharmacySummary_RecallCreditableValue = "RecallCreditableValue";
      public const string Report_Parameter_PharmacySummary_RecallCreditableQty = "RecallCreditableQty";
      public const string Report_Parameter_PharmacySummary_NonCreditableValue = "NonCreditableValue";
      public const string Report_Parameter_PharmacySummary_NonCreditableQty = "NonCreditableQty";
      public const string Report_Parameter_PharmacySummary_TotalProductValue = "TotalProductValue";
      public const string Report_Parameter_PharmacySummary_TotalQty = "TotalQty";
      public const string Report_Parameter_PharmacySummary_ProcessingFeeBilled = "ProcessingFeeBilled";
      public const string Report_Parameter_PharmacySummary_TotalInvoiceAmount = "TotalInvoiceAmount";
      public const string Report_Parameter_PharmacySummary_TotalDebitMemoQty = "TotalDebitMemoQty";
      public const string Report_Parameter_PharmacySummary_ReturnToStockValue = "ReturnToStockValue";
      public const string Report_Parameter_PharmacySummary_ReturnToStockQty = "ReturnToStockQty";

      //Non Creditable Summary Parameters
      public const string Report_Parameter_NonCreditableSummary_StoreNumber = "StoreNumber";
      public const string Report_Parameter_NonCreditableSummary_StoreName = "StoreName";
      public const string Report_Parameter_NonCreditableSummary_RegionName = "RegionName";
      public const string Report_Parameter_NonCreditableSummary_NonCreditableValue = "NonCreditableValue";
      public const string Report_Parameter_NonCreditableSummary_NonCreditableQty = "NonCreditableQty";
      public const string Report_Parameter_NonCreditableSummary_ManufacturerName = "ManufacturerName";
      public const string Report_Parameter_NonCreditableSummary_NonCreditableReasonCode = "NonCreditableReasonCode";
      public const string Report_Parameter_NonCreditableSummary_NonCreditableReasonDescription = "NonCreditableReasonDescription";

      //Manufacturer Summary Parameters
      public const string Report_Parameter_ManufacturerSummary_MFGLabeler = "MFGLabeler";
      public const string Report_Parameter_ManufacturerSummary_ManufacturerName = "ManufacturerName";
      public const string Report_Parameter_ManufacturerSummary_RetailerVendorNumber = "RetailerVendorNumber";
      public const string Report_Parameter_ManufacturerSummary_DebitMemoInvoiceNo = "DebitMemoInvoiceNo";
      public const string Report_Parameter_ManufacturerSummary_CreditableValue = "CreditableValue";
      public const string Report_Parameter_ManufacturerSummary_CreditableQty = "CreditableQty";
      public const string Report_Parameter_ManufacturerSummary_RecallCreditableValue = "RecallCreditableValue";
      public const string Report_Parameter_ManufacturerSummary_RecallCreditableQty = "RecallCreditableQty";
      public const string Report_Parameter_ManufacturerSummary_TotalProductValue = "TotalProductValue";
      public const string Report_Parameter_ManufacturerSummary_TotalQty = "TotalQty";
      public const string Report_Parameter_ManufacturerSummary_ProcessingFee = "ProcessingFee";
      public const string Report_Parameter_ManufacturerSummary_TotalInvoiceAmount = "TotalInvoiceAmount";

      //Return To Stock Parameters
      public const string Report_Parameter_ReturnToStock_VendorName = "VendorName";
      public const string Report_Parameter_ReturnToStock_StoreName = "StoreName";
      public const string Report_Parameter_ReturnToStock_StoreNumber = "StoreNumber";
      public const string Report_Parameter_ReturnToStock_NDCNumber = "NDCNumber";
      public const string Report_Parameter_ReturnToStock_ProductDescription = "ProductDescription";
      public const string Report_Parameter_ReturnToStock_Strength = "Strength";
      public const string Report_Parameter_ReturnToStock_ControlNumber = "ControlNumber";
      public const string Report_Parameter_ReturnToStock_RXorOTC = "RXorOTC";
      public const string Report_Parameter_ReturnToStock_DosageForm = "DosageForm";
      public const string Report_Parameter_ReturnToStock_PackageForm = "PackageForm";
      public const string Report_Parameter_ReturnToStock_LotNumber = "LotNumber";
      public const string Report_Parameter_ReturnToStock_ExpDate = "ExpDate";
      public const string Report_Parameter_ReturnToStock_QoskProcessDate = "QoskProcessDate";
      public const string Report_Parameter_ReturnToStock_DateEligibleForCredit = "DateEligibleForCredit";
      public const string Report_Parameter_ReturnToStock_OutofpolicyDescription = "OutofpolicyDescription";

      public const string ReportDataContext_Yes = "Yes";
      public const string ReportDataContext_No = "No";
      public const string ReportDataContext_Aging = "Aging";
      public const string ReportDataContext_Stock = "Stock";
      public const string ReportDataContext_OTC = "OTC";
      public const string ReportDataContext_RX = "RX";
      public const string ReportDataContext_Sealed = "Sealed";
      public const string ReportDataContext_Opened = "Opened";
      public const string ReportDataContext_Case = "Case";
      public const string ReportDataContext_DateFormat = "MM/dd/yyyy";
      public const string ReportDataContext_Space = " ";
      public const string ReportDataContext_Empty = "";
      public const string ReportDataContext_Comma = ",";
      public const string ReportDataContext_NA = "N/A";
      public const string ReportDataContext_Dash = "-";
      public const string ReportDataContext_ZeroAfterDecimal = "0.00";

      public const string ReportDataContext_ItemStateDict_Code_Initial_S = "S";
      public const string ReportDataContext_ItemStateDict_Code_Initial_C = "C";
      public const string ReportDataContext_ItemStateDict_Code_Initial_O = "O";

      public const string GetAutoCompleteDetails_textbox = "textbox-";
      public const string CreateSearchPanel_DefaulColumnFlag = "DefaulColumnFlag";
      public const string CommonDataAccess_ImageVideosLinks = "Images/Videos links";
      public const string CommonDataAccess_DataStartDate = "DataStartDate";
      public const string CommonDataAccess_DataEndDate = "DataEndDate";
      public const string GetUserSpecificColumnsForGrid_Width = "200px";
      public const string SaveSubscribedColumnListForUser_qoskdev = "qoskdev";

      public const string FormattedSchedule_Hour = "Hour";
      public const string FormattedSchedule_Day = "Day";
      public const string FormattedSchedule_Week = "Week";
      public const string FormattedSchedule_Month = "Month";
      public const string FormattedSchedule_s = "s ";
      public const string FormattedSchedule_At = "At ";
      public const string FormattedSchedule_Colon = ":";
      public const string FormattedSchedule_Space = " ";
      public const string FormattedSchedule_Every = "Every ";
      public const string FormattedSchedule_QoskID = "QoskID";
      public const string FormattedSchedule_ItemGuid = "ItemGuid";
      public const string FormattedSchedule_MultipleCriteria = "Multiple Criteria";

      public const string SendEmail_SuccessfullySend = "Email sent successfully.";
      public const string SendEmail_Error = "Error occurred while sending email.";
      public const string DisplayImageAndVideo_NoImage = "NoImage";

      public const string ResponseHeader_Content_Disposition = "Content-Disposition";
      public const string ResponseHeader_FileName_Format = "{0};FileName=\"{1}\"";
      public const string ResponseHeader_Attachment = "attachment";

      public const string GetSearchPanelLayoutId_SearchPanel = "SearchPanel";
      public const string GetListPanelLayoutId_ListPanel = "ListPanel";




      #region ReportMessages
      public const string ReportMessage_Added_To_Report_Queue = "Added to report queue to be processed.";
      public const string ReportMessage_Picked_By_Schedule_Job = "Picked by Schedule web job to process.";
      public const string ReportMessage_Report_Exported_Successfully = "Report exported successfully.";
      public const string ReportMessage_Uploading_To_Blob = "Schedule web job uploading generated report to the blob storage.";
      public const string ReportMessage_Uploaded_To_Blob = "Schedule web job successfully uploaded on the blob storage.";
      public const string ReportMessage_Placed_In_NotificationQueue = "Schedule web job placed message in the notification queue after uploading.";
      public const string ReportMessage_Notification_Sending = "Notification web job is sending notification to user.";
      public const string ReportMessage_Notification_Sent = "Notification web job sent notification to user.";
      public const string ReportMessage_Finished = "Scheduled report generated successfully and notification sent to user.";
      public const string ReportMessage_Error_Occurred = "Error occurred during the execution.";
      #endregion

      #region Time Zone Names
      public const string TimeZone_Eastern_Standard_Time = "Eastern Standard Time";
      public const string TimeZone_Central_Standard_Time = "Central Standard Time";
      public const string TimeZone_Mountain_Standard_Time = "Mountain Standard Time";
      public const string TimeZone_Pacific_Standard_Time = "Pacific Standard Time";

      public const int DataRange_Daily = 1;
      public const int DataRange_Weekly = 7;
      public const int DataRange_Monthly = 30;
      public const int DataRange_Buffer = 10;

      #endregion
      #endregion

      #region For partial view 
      public const int GetReportView_ReportOneParameter = 1;
      public const int GetReportView_ReportTwoParameter = 2;
      public const int GetReportView_ReportThreeParameter = 3;
      public const int GetReportView_ReportFourParameter = 4;
      public const int GetReportView_ReportFiveParameter = 5;
      public const int GetReportView_ReportSixParameter = 6;
      public const int GetReportView_ReportSevenParameter = 7;
      public const int GetReportView_ReportEightParameter = 8;
      public const int GetReportView_ReportNineParameter = 9;
      public const int GetReportView_ReportTenParameter = 10;
      public const int GetReportView_ReportElevenParameter = 11;
      public const int GetReportView_ReportTwelveParameter = 12;

      #endregion

      #region App Config File Keys       
      public const string AdminWebsiteUrl = "AdminWebsiteUrl";
      public const string ReportWebsiteUrl = "ReportWebsiteUrl";
      #endregion

      #region AppConfig Table Keys (Email Configuration)
      public const string AppConfigFile_GroupKey_Qosk = "Qosk";
      public const string AppConfigFile_Email_UserName = "userName";
      public const string AppConfigFile_Email_Password = "password";
      public const string AppConfigFile_Email_Host = "host";
      public const string AppConfigFile_Email_Port = "port";
      public const string AppConfigFile_Email_EnableSsl = "enableSsl";
      #endregion

      #region Websites default timeout
      public const double DefaultTimeOut = 120.0;
      #endregion
   }

}
