﻿///-----------------------------------------------------------------------------------------------
/// <copyright file="Logger.cs">
///   Copyright (c) 2015 - 2016, Qualanex LLC
///   All rights reserved.
///
///   This software is provided 'as-is', without any express or implied warranty.  In no event
///   will the authors be held liable for any damages arising from the use of this software.
/// </copyright>
/// <summary>
///   Summary not available.
/// </summary>
/// <remarks>
///   None.
/// </remarks>
///-----------------------------------------------------------------------------------------------

namespace Qualanex.QoskCloud.Utility
{
   using System;
   using System.Data.Entity.Validation;
   using System.Diagnostics;

   ///============================================================================
   /// <summary>
   ///   Summary not available.
   /// </summary>
   ///----------------------------------------------------------------------------
   public static class Logger
   {
      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="msg">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Error(string msg)
      {
         Trace.TraceError(msg);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="msg">
      ///   Description not available.
      /// </param>
      /// <param name="ex">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Error(string msg, Exception ex)
      {
         Trace.TraceError(msg);
         Trace.TraceError(ex.ToString());
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="ex">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Error(Exception ex)
      {
         Trace.TraceError(ex.ToString());
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="fmt">
      ///   Description not available.
      /// </param>
      /// <param name="vars">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Error(string fmt, params object[] vars)
      {
         var msg = string.Format(fmt, vars);
         Trace.TraceError(msg);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="dbEx">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void DBError(DbEntityValidationException dbEx)
      {
         foreach (var validationErrors in dbEx.EntityValidationErrors)
         {
            foreach (var validationError in validationErrors.ValidationErrors)
            {
               Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                                       validationErrors.Entry.Entity.GetType().FullName,
                                        validationError.PropertyName,
                                         validationError.ErrorMessage);
            }
         }
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="msg">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Info(string msg)
      {
         Trace.TraceInformation(msg);
      }

      ///****************************************************************************
      /// <summary>
      ///   Summary not available.
      /// </summary>
      /// <param name="msg">
      ///   Description not available.
      /// </param>
      ///****************************************************************************
      public static void Warning(string msg)
      {
         Trace.TraceWarning(msg);
      }

   }
}
